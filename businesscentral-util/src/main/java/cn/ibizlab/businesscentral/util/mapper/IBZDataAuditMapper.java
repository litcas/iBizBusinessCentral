package cn.ibizlab.businesscentral.util.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.ibizlab.businesscentral.util.domain.IBZDataAudit;

public interface IBZDataAuditMapper extends BaseMapper<IBZDataAudit> {

}
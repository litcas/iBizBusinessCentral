window.Environment = {
    // 是否为开发模式
    devMode: true,
    // 项目模板地址
    ProjectUrl: "https://gitee.com/ibizlab/iBizBusinessCentral",
    debugOpenMode:'mos',
    // 配置平台地址
    StudioUrl: "http://mos.ibizlab.cn/mos/",
    // 方案标识
    SlnId: "54CBED3A-E1C8-4489-8CB8-899DBCDE48C8",
    // 系统标识
    SysId: "75356053-28BD-4651-B2C8-C228F6F9B588",
    // 前端应用标识
    AppId: "ff4d565b24c39640d06905adfa175d5e"
}
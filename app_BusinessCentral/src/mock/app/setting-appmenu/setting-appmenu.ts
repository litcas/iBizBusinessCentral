import { MockAdapter } from '@/mock/mock-adapter';
const mock = MockAdapter.getInstance();

import Mock from 'mockjs'
const Random = Mock.Random;

// 获取应用数据
mock.onGet('v7/settingappmenu').reply((config: any) => {
    let status = MockAdapter.mockStatus(config);
    return [status, {
        name: 'db_setting_appmenu',
        items:  [
            {
	id: 'a988b425462b1e6864b8a87790c0ad2b',
	name: 'menuitem1',
	text: '链接角色',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '链接角色',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'fa fa-connectdevelop',
	icon: '',
	textcls: '',
	appfunctag: 'Auto27',
	resourcetag: '',
},
        ],
    }];
});


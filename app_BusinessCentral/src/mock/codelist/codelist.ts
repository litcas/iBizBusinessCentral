import { MockAdapter } from '../mock-adapter';
const mock = MockAdapter.getInstance();

import Mock from 'mockjs'

// 获取全部数组
mock.onGet('./assets/json/data-dictionary.json').reply((config: any) => {
    let status = MockAdapter.mockStatus(config);
    return [status, [
        {
        srfkey: "List__CreatedFromCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "1",
                label: "帐户",
                text: "帐户",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "4",
                label: "潜在顾客",
                text: "潜在顾客",
                "data":"",
                "codename":"Item_4",
                value: "4",
                
                disabled: false,
            },
            {
                id: "2",
                label: "联系人",
                text: "联系人",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Campaignactivity__ChannelTypeCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "5",
                label: "传真",
                text: "传真",
                "data":"",
                "codename":"Item_5",
                value: "5",
                
                disabled: false,
            },
            {
                id: "3",
                label: "信件",
                text: "信件",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
            {
                id: "9",
                label: "其他",
                text: "其他",
                "data":"",
                "codename":"Item_9",
                value: "9",
                
                disabled: false,
            },
            {
                id: "7",
                label: "电子邮件",
                text: "电子邮件",
                "data":"",
                "codename":"Item_7",
                value: "7",
                
                disabled: false,
            },
            {
                id: "1",
                label: "电话",
                text: "电话",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "2",
                label: "约会",
                text: "约会",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "6",
                label: "通过邮件合并的传真",
                text: "通过邮件合并的传真",
                "data":"",
                "codename":"Item_6",
                value: "6",
                
                disabled: false,
            },
            {
                id: "4",
                label: "通过邮件合并的信件",
                text: "通过邮件合并的信件",
                "data":"",
                "codename":"Item_4",
                value: "4",
                
                disabled: false,
            },
            {
                id: "8",
                label: "通过邮件合并的电子邮件",
                text: "通过邮件合并的电子邮件",
                "data":"",
                "codename":"Item_8",
                value: "8",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Salesorder__StateCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "2",
                label: "已取消",
                text: "已取消",
                "data":"",
                "codename":"Item_2",
                value: 2,
                
                disabled: false,
            },
            {
                id: "3",
                label: "已完成",
                text: "已完成",
                "data":"",
                "codename":"Item_3",
                value: 3,
                
                disabled: false,
            },
            {
                id: "4",
                label: "已开发票",
                text: "已开发票",
                "data":"",
                "codename":"Item_4",
                value: 4,
                
                disabled: false,
            },
            {
                id: "1",
                label: "已提交",
                text: "已提交",
                "data":"",
                "codename":"Item_1",
                value: 1,
                
                disabled: false,
            },
            {
                id: "0",
                label: "有效",
                text: "有效",
                "data":"",
                "codename":"Item_0",
                value: 0,
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Connection__StateCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "1",
                label: "停用",
                text: "停用",
                "data":"",
                "codename":"Item_1",
                value: 1,
                
                disabled: false,
            },
            {
                id: "0",
                label: "可用",
                text: "可用",
                "data":"",
                "codename":"Item_0",
                value: 0,
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Account__StateCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "1",
                label: "停用",
                text: "停用",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "0",
                label: "可用",
                text: "可用",
                "data":"",
                "codename":"Item_0",
                value: "0",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "CodeListJobStatus",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "0",
                label: "ENABLED",
                text: "ENABLED",
                "data":"",
                "codename":"Item_0",
                "color": "rgba(58, 116, 7, 1)",
                value: 0,
                
                disabled: false,
            },
            {
                id: "1",
                label: "DISABLED",
                text: "DISABLED",
                "data":"",
                "codename":"Item_1",
                "color": "rgba(67, 65, 65, 1)",
                value: 1,
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Account__StatusCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "2",
                label: "停用",
                text: "停用",
                "data":"",
                "codename":"Item_2",
                "color": "rgba(255, 42, 0, 1)",
                value: "2",
                
                disabled: false,
            },
            {
                id: "1",
                label: "可用",
                text: "可用",
                "data":"",
                "codename":"Item_1",
                "color": "rgba(0, 255, 64, 1)",
                value: "1",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Contact__PaymentTermsCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "2",
                label: "2/10 N30",
                text: "2/10 N30",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "1",
                label: "N30",
                text: "N30",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "3",
                label: "N45",
                text: "N45",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
            {
                id: "4",
                label: "N60",
                text: "N60",
                "data":"",
                "codename":"Item_4",
                value: "4",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "AccountClassificationCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "1",
                label: "默认值",
                text: "默认值",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Connection__Record2ObjectTypeCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "1",
                label: "Account",
                text: "Account",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "4200",
                label: "Activity",
                text: "Activity",
                "data":"",
                "codename":"Item_4200",
                value: "4200",
                
                disabled: false,
            },
            {
                id: "10156",
                label: "Agreement",
                text: "Agreement",
                "data":"",
                "codename":"Item_10156",
                value: "10156",
                
                disabled: false,
            },
            {
                id: "10157",
                label: "Agreement Booking Date",
                text: "Agreement Booking Date",
                "data":"",
                "codename":"Item_10157",
                value: "10157",
                
                disabled: false,
            },
            {
                id: "10158",
                label: "Agreement Booking Incident",
                text: "Agreement Booking Incident",
                "data":"",
                "codename":"Item_10158",
                value: "10158",
                
                disabled: false,
            },
            {
                id: "10159",
                label: "Agreement Booking Product",
                text: "Agreement Booking Product",
                "data":"",
                "codename":"Item_10159",
                value: "10159",
                
                disabled: false,
            },
            {
                id: "10160",
                label: "Agreement Booking Service",
                text: "Agreement Booking Service",
                "data":"",
                "codename":"Item_10160",
                value: "10160",
                
                disabled: false,
            },
            {
                id: "10161",
                label: "Agreement Booking Service Task",
                text: "Agreement Booking Service Task",
                "data":"",
                "codename":"Item_10161",
                value: "10161",
                
                disabled: false,
            },
            {
                id: "10162",
                label: "Agreement Booking Setup",
                text: "Agreement Booking Setup",
                "data":"",
                "codename":"Item_10162",
                value: "10162",
                
                disabled: false,
            },
            {
                id: "10163",
                label: "Agreement Invoice Date",
                text: "Agreement Invoice Date",
                "data":"",
                "codename":"Item_10163",
                value: "10163",
                
                disabled: false,
            },
            {
                id: "10164",
                label: "Agreement Invoice Product",
                text: "Agreement Invoice Product",
                "data":"",
                "codename":"Item_10164",
                value: "10164",
                
                disabled: false,
            },
            {
                id: "10165",
                label: "Agreement Invoice Setup",
                text: "Agreement Invoice Setup",
                "data":"",
                "codename":"Item_10165",
                value: "10165",
                
                disabled: false,
            },
            {
                id: "4201",
                label: "Appointment",
                text: "Appointment",
                "data":"",
                "codename":"Item_4201",
                value: "4201",
                
                disabled: false,
            },
            {
                id: "10042",
                label: "Booking Alert",
                text: "Booking Alert",
                "data":"",
                "codename":"Item_10042",
                value: "10042",
                
                disabled: false,
            },
            {
                id: "10043",
                label: "Booking Alert Status",
                text: "Booking Alert Status",
                "data":"",
                "codename":"Item_10043",
                value: "10043",
                
                disabled: false,
            },
            {
                id: "10045",
                label: "Booking Rule",
                text: "Booking Rule",
                "data":"",
                "codename":"Item_10045",
                value: "10045",
                
                disabled: false,
            },
            {
                id: "10168",
                label: "Booking Timestamp",
                text: "Booking Timestamp",
                "data":"",
                "codename":"Item_10168",
                value: "10168",
                
                disabled: false,
            },
            {
                id: "4400",
                label: "Campaign",
                text: "Campaign",
                "data":"",
                "codename":"Item_4400",
                value: "4400",
                
                disabled: false,
            },
            {
                id: "4402",
                label: "Campaign Activity",
                text: "Campaign Activity",
                "data":"",
                "codename":"Item_4402",
                value: "4402",
                
                disabled: false,
            },
            {
                id: "112",
                label: "Case",
                text: "Case",
                "data":"",
                "codename":"Item_112",
                value: "112",
                
                disabled: false,
            },
            {
                id: "9400",
                label: "Channel Access Profile Rule",
                text: "Channel Access Profile Rule",
                "data":"",
                "codename":"Item_9400",
                value: "9400",
                
                disabled: false,
            },
            {
                id: "123",
                label: "Competitor",
                text: "Competitor",
                "data":"",
                "codename":"Item_123",
                value: "123",
                
                disabled: false,
            },
            {
                id: "2",
                label: "Contact",
                text: "Contact",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "1010",
                label: "Contract",
                text: "Contract",
                "data":"",
                "codename":"Item_1010",
                value: "1010",
                
                disabled: false,
            },
            {
                id: "10173",
                label: "Customer Asset",
                text: "Customer Asset",
                "data":"",
                "codename":"Item_10173",
                value: "10173",
                
                disabled: false,
            },
            {
                id: "4202",
                label: "Email",
                text: "Email",
                "data":"",
                "codename":"Item_4202",
                value: "4202",
                
                disabled: false,
            },
            {
                id: "9700",
                label: "Entitlement",
                text: "Entitlement",
                "data":"",
                "codename":"Item_9700",
                value: "9700",
                
                disabled: false,
            },
            {
                id: "9701",
                label: "Entitlement Channel",
                text: "Entitlement Channel",
                "data":"",
                "codename":"Item_9701",
                value: "9701",
                
                disabled: false,
            },
            {
                id: "9703",
                label: "Entitlement Template Channel",
                text: "Entitlement Template Channel",
                "data":"",
                "codename":"Item_9703",
                value: "9703",
                
                disabled: false,
            },
            {
                id: "4000",
                label: "Facility/Equipment",
                text: "Facility/Equipment",
                "data":"",
                "codename":"Item_4000",
                value: "4000",
                
                disabled: false,
            },
            {
                id: "4204",
                label: "Fax",
                text: "Fax",
                "data":"",
                "codename":"Item_4204",
                value: "4204",
                
                disabled: false,
            },
            {
                id: "10065",
                label: "Fulfillment Preference",
                text: "Fulfillment Preference",
                "data":"",
                "codename":"Item_10065",
                value: "10065",
                
                disabled: false,
            },
            {
                id: "9600",
                label: "Goal",
                text: "Goal",
                "data":"",
                "codename":"Item_9600",
                value: "9600",
                
                disabled: false,
            },
            {
                id: "10181",
                label: "Incident Type Characteristic",
                text: "Incident Type Characteristic",
                "data":"",
                "codename":"Item_10181",
                value: "10181",
                
                disabled: false,
            },
            {
                id: "10182",
                label: "Incident Type Product",
                text: "Incident Type Product",
                "data":"",
                "codename":"Item_10182",
                value: "10182",
                
                disabled: false,
            },
            {
                id: "10183",
                label: "Incident Type Service",
                text: "Incident Type Service",
                "data":"",
                "codename":"Item_10183",
                value: "10183",
                
                disabled: false,
            },
            {
                id: "10187",
                label: "Inventory Adjustment",
                text: "Inventory Adjustment",
                "data":"",
                "codename":"Item_10187",
                value: "10187",
                
                disabled: false,
            },
            {
                id: "10188",
                label: "Inventory Adjustment Product",
                text: "Inventory Adjustment Product",
                "data":"",
                "codename":"Item_10188",
                value: "10188",
                
                disabled: false,
            },
            {
                id: "10189",
                label: "Inventory Journal",
                text: "Inventory Journal",
                "data":"",
                "codename":"Item_10189",
                value: "10189",
                
                disabled: false,
            },
            {
                id: "10190",
                label: "Inventory Transfer",
                text: "Inventory Transfer",
                "data":"",
                "codename":"Item_10190",
                value: "10190",
                
                disabled: false,
            },
            {
                id: "1090",
                label: "Invoice",
                text: "Invoice",
                "data":"",
                "codename":"Item_1090",
                value: "1090",
                
                disabled: false,
            },
            {
                id: "10241",
                label: "IoT Alert",
                text: "IoT Alert",
                "data":"",
                "codename":"Item_10241",
                value: "10241",
                
                disabled: false,
            },
            {
                id: "10242",
                label: "IoT Device",
                text: "IoT Device",
                "data":"",
                "codename":"Item_10242",
                value: "10242",
                
                disabled: false,
            },
            {
                id: "10243",
                label: "IoT Device Category",
                text: "IoT Device Category",
                "data":"",
                "codename":"Item_10243",
                value: "10243",
                
                disabled: false,
            },
            {
                id: "10244",
                label: "IoT Device Command",
                text: "IoT Device Command",
                "data":"",
                "codename":"Item_10244",
                value: "10244",
                
                disabled: false,
            },
            {
                id: "10248",
                label: "IoT Device Registration History",
                text: "IoT Device Registration History",
                "data":"",
                "codename":"Item_10248",
                value: "10248",
                
                disabled: false,
            },
            {
                id: "9953",
                label: "Knowledge Article",
                text: "Knowledge Article",
                "data":"",
                "codename":"Item_9953",
                value: "9953",
                
                disabled: false,
            },
            {
                id: "9930",
                label: "Knowledge Base Record",
                text: "Knowledge Base Record",
                "data":"",
                "codename":"Item_9930",
                value: "9930",
                
                disabled: false,
            },
            {
                id: "4",
                label: "Lead",
                text: "Lead",
                "data":"",
                "codename":"Item_4",
                value: "4",
                
                disabled: false,
            },
            {
                id: "4207",
                label: "Letter",
                text: "Letter",
                "data":"",
                "codename":"Item_4207",
                value: "4207",
                
                disabled: false,
            },
            {
                id: "4300",
                label: "Marketing List",
                text: "Marketing List",
                "data":"",
                "codename":"Item_4300",
                value: "4300",
                
                disabled: false,
            },
            {
                id: "3",
                label: "Opportunity",
                text: "Opportunity",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
            {
                id: "1088",
                label: "Order",
                text: "Order",
                "data":"",
                "codename":"Item_1088",
                value: "1088",
                
                disabled: false,
            },
            {
                id: "10195",
                label: "Payment",
                text: "Payment",
                "data":"",
                "codename":"Item_10195",
                value: "10195",
                
                disabled: false,
            },
            {
                id: "10196",
                label: "Payment Detail",
                text: "Payment Detail",
                "data":"",
                "codename":"Item_10196",
                value: "10196",
                
                disabled: false,
            },
            {
                id: "10197",
                label: "Payment Method",
                text: "Payment Method",
                "data":"",
                "codename":"Item_10197",
                value: "10197",
                
                disabled: false,
            },
            {
                id: "10198",
                label: "Payment Term",
                text: "Payment Term",
                "data":"",
                "codename":"Item_10198",
                value: "10198",
                
                disabled: false,
            },
            {
                id: "4210",
                label: "Phone Call",
                text: "Phone Call",
                "data":"",
                "codename":"Item_4210",
                value: "4210",
                
                disabled: false,
            },
            {
                id: "50",
                label: "Position",
                text: "Position",
                "data":"",
                "codename":"Item_50",
                value: "50",
                
                disabled: false,
            },
            {
                id: "10199",
                label: "Postal Code",
                text: "Postal Code",
                "data":"",
                "codename":"Item_10199",
                value: "10199",
                
                disabled: false,
            },
            {
                id: "1022",
                label: "Price List",
                text: "Price List",
                "data":"",
                "codename":"Item_1022",
                value: "1022",
                
                disabled: false,
            },
            {
                id: "10108",
                label: "Process Notes",
                text: "Process Notes",
                "data":"",
                "codename":"Item_10108",
                value: "10108",
                
                disabled: false,
            },
            {
                id: "4710",
                label: "Process Session",
                text: "Process Session",
                "data":"",
                "codename":"Item_4710",
                value: "4710",
                
                disabled: false,
            },
            {
                id: "1024",
                label: "Product",
                text: "Product",
                "data":"",
                "codename":"Item_1024",
                value: "1024",
                
                disabled: false,
            },
            {
                id: "10200",
                label: "Product Inventory",
                text: "Product Inventory",
                "data":"",
                "codename":"Item_10200",
                value: "10200",
                
                disabled: false,
            },
            {
                id: "10027",
                label: "Profile Album",
                text: "Profile Album",
                "data":"",
                "codename":"Item_10027",
                value: "10027",
                
                disabled: false,
            },
            {
                id: "10109",
                label: "Project",
                text: "Project",
                "data":"",
                "codename":"Item_10109",
                value: "10109",
                
                disabled: false,
            },
            {
                id: "10070",
                label: "Project Service Approval",
                text: "Project Service Approval",
                "data":"",
                "codename":"Item_10070",
                value: "10070",
                
                disabled: false,
            },
            {
                id: "10117",
                label: "Project Team Member",
                text: "Project Team Member",
                "data":"",
                "codename":"Item_10117",
                value: "10117",
                
                disabled: false,
            },
            {
                id: "10201",
                label: "Purchase Order",
                text: "Purchase Order",
                "data":"",
                "codename":"Item_10201",
                value: "10201",
                
                disabled: false,
            },
            {
                id: "10202",
                label: "Purchase Order Bill",
                text: "Purchase Order Bill",
                "data":"",
                "codename":"Item_10202",
                value: "10202",
                
                disabled: false,
            },
            {
                id: "10203",
                label: "Purchase Order Product",
                text: "Purchase Order Product",
                "data":"",
                "codename":"Item_10203",
                value: "10203",
                
                disabled: false,
            },
            {
                id: "10204",
                label: "Purchase Order Receipt",
                text: "Purchase Order Receipt",
                "data":"",
                "codename":"Item_10204",
                value: "10204",
                
                disabled: false,
            },
            {
                id: "10205",
                label: "Purchase Order Receipt Product",
                text: "Purchase Order Receipt Product",
                "data":"",
                "codename":"Item_10205",
                value: "10205",
                
                disabled: false,
            },
            {
                id: "10206",
                label: "Purchase Order SubStatus",
                text: "Purchase Order SubStatus",
                "data":"",
                "codename":"Item_10206",
                value: "10206",
                
                disabled: false,
            },
            {
                id: "1084",
                label: "Quote",
                text: "Quote",
                "data":"",
                "codename":"Item_1084",
                value: "1084",
                
                disabled: false,
            },
            {
                id: "10207",
                label: "Quote Booking Incident",
                text: "Quote Booking Incident",
                "data":"",
                "codename":"Item_10207",
                value: "10207",
                
                disabled: false,
            },
            {
                id: "10208",
                label: "Quote Booking Product",
                text: "Quote Booking Product",
                "data":"",
                "codename":"Item_10208",
                value: "10208",
                
                disabled: false,
            },
            {
                id: "10209",
                label: "Quote Booking Service",
                text: "Quote Booking Service",
                "data":"",
                "codename":"Item_10209",
                value: "10209",
                
                disabled: false,
            },
            {
                id: "10210",
                label: "Quote Booking Service Task",
                text: "Quote Booking Service Task",
                "data":"",
                "codename":"Item_10210",
                value: "10210",
                
                disabled: false,
            },
            {
                id: "4251",
                label: "Recurring Appointment",
                text: "Recurring Appointment",
                "data":"",
                "codename":"Item_4251",
                value: "4251",
                
                disabled: false,
            },
            {
                id: "4007",
                label: "Resource Group",
                text: "Resource Group",
                "data":"",
                "codename":"Item_4007",
                value: "4007",
                
                disabled: false,
            },
            {
                id: "10235",
                label: "Resource Restriction (Deprecated)",
                text: "Resource Restriction (Deprecated)",
                "data":"",
                "codename":"Item_10235",
                value: "10235",
                
                disabled: false,
            },
            {
                id: "10061",
                label: "Resource Territory",
                text: "Resource Territory",
                "data":"",
                "codename":"Item_10061",
                value: "10061",
                
                disabled: false,
            },
            {
                id: "10215",
                label: "RMA",
                text: "RMA",
                "data":"",
                "codename":"Item_10215",
                value: "10215",
                
                disabled: false,
            },
            {
                id: "10216",
                label: "RMA Product",
                text: "RMA Product",
                "data":"",
                "codename":"Item_10216",
                value: "10216",
                
                disabled: false,
            },
            {
                id: "10217",
                label: "RMA Receipt",
                text: "RMA Receipt",
                "data":"",
                "codename":"Item_10217",
                value: "10217",
                
                disabled: false,
            },
            {
                id: "10218",
                label: "RMA Receipt Product",
                text: "RMA Receipt Product",
                "data":"",
                "codename":"Item_10218",
                value: "10218",
                
                disabled: false,
            },
            {
                id: "10219",
                label: "RMA SubStatus",
                text: "RMA SubStatus",
                "data":"",
                "codename":"Item_10219",
                value: "10219",
                
                disabled: false,
            },
            {
                id: "10220",
                label: "RTV",
                text: "RTV",
                "data":"",
                "codename":"Item_10220",
                value: "10220",
                
                disabled: false,
            },
            {
                id: "10221",
                label: "RTV Product",
                text: "RTV Product",
                "data":"",
                "codename":"Item_10221",
                value: "10221",
                
                disabled: false,
            },
            {
                id: "10222",
                label: "RTV Substatus",
                text: "RTV Substatus",
                "data":"",
                "codename":"Item_10222",
                value: "10222",
                
                disabled: false,
            },
            {
                id: "4005",
                label: "Scheduling Group",
                text: "Scheduling Group",
                "data":"",
                "codename":"Item_4005",
                value: "4005",
                
                disabled: false,
            },
            {
                id: "4214",
                label: "Service Activity",
                text: "Service Activity",
                "data":"",
                "codename":"Item_4214",
                value: "4214",
                
                disabled: false,
            },
            {
                id: "10224",
                label: "Ship Via",
                text: "Ship Via",
                "data":"",
                "codename":"Item_10224",
                value: "10224",
                
                disabled: false,
            },
            {
                id: "4216",
                label: "Social Activity",
                text: "Social Activity",
                "data":"",
                "codename":"Item_4216",
                value: "4216",
                
                disabled: false,
            },
            {
                id: "99",
                label: "Social Profile",
                text: "Social Profile",
                "data":"",
                "codename":"Item_99",
                value: "99",
                
                disabled: false,
            },
            {
                id: "10036",
                label: "Survey invite",
                text: "Survey invite",
                "data":"",
                "codename":"Item_10036",
                value: "10036",
                
                disabled: false,
            },
            {
                id: "10037",
                label: "Survey response",
                text: "Survey response",
                "data":"",
                "codename":"Item_10037",
                value: "10037",
                
                disabled: false,
            },
            {
                id: "10064",
                label: "System User Scheduler Setting",
                text: "System User Scheduler Setting",
                "data":"",
                "codename":"Item_10064",
                value: "10064",
                
                disabled: false,
            },
            {
                id: "4212",
                label: "Task",
                text: "Task",
                "data":"",
                "codename":"Item_4212",
                value: "4212",
                
                disabled: false,
            },
            {
                id: "10225",
                label: "Tax Code",
                text: "Tax Code",
                "data":"",
                "codename":"Item_10225",
                value: "10225",
                
                disabled: false,
            },
            {
                id: "9",
                label: "Team",
                text: "Team",
                "data":"",
                "codename":"Item_9",
                value: "9",
                
                disabled: false,
            },
            {
                id: "2013",
                label: "Territory",
                text: "Territory",
                "data":"",
                "codename":"Item_2013",
                value: "2013",
                
                disabled: false,
            },
            {
                id: "10066",
                label: "Time Group Detail",
                text: "Time Group Detail",
                "data":"",
                "codename":"Item_10066",
                value: "10066",
                
                disabled: false,
            },
            {
                id: "10227",
                label: "Time Off Request",
                text: "Time Off Request",
                "data":"",
                "codename":"Item_10227",
                value: "10227",
                
                disabled: false,
            },
            {
                id: "8",
                label: "User",
                text: "User",
                "data":"",
                "codename":"Item_8",
                value: "8",
                
                disabled: false,
            },
            {
                id: "10229",
                label: "Warehouse",
                text: "Warehouse",
                "data":"",
                "codename":"Item_10229",
                value: "10229",
                
                disabled: false,
            },
            {
                id: "10230",
                label: "Work Order",
                text: "Work Order",
                "data":"",
                "codename":"Item_10230",
                value: "10230",
                
                disabled: false,
            },
            {
                id: "10231",
                label: "Work Order Characteristic (Deprecated)",
                text: "Work Order Characteristic (Deprecated)",
                "data":"",
                "codename":"Item_10231",
                value: "10231",
                
                disabled: false,
            },
            {
                id: "10233",
                label: "Work Order Incident",
                text: "Work Order Incident",
                "data":"",
                "codename":"Item_10233",
                value: "10233",
                
                disabled: false,
            },
            {
                id: "10234",
                label: "Work Order Product",
                text: "Work Order Product",
                "data":"",
                "codename":"Item_10234",
                value: "10234",
                
                disabled: false,
            },
            {
                id: "10236",
                label: "Work Order Service",
                text: "Work Order Service",
                "data":"",
                "codename":"Item_10236",
                value: "10236",
                
                disabled: false,
            },
            {
                id: "10237",
                label: "Work Order Service Task",
                text: "Work Order Service Task",
                "data":"",
                "codename":"Item_10237",
                value: "10237",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Quote__StateCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "3",
                label: "已关闭",
                text: "已关闭",
                "data":"",
                "codename":"Item_3",
                value: 3,
                
                disabled: false,
            },
            {
                id: "1",
                label: "有效",
                text: "有效",
                "data":"",
                "codename":"Item_1",
                value: 1,
                
                disabled: false,
            },
            {
                id: "0",
                label: "草稿",
                text: "草稿",
                "data":"",
                "codename":"Item_0",
                value: 0,
                
                disabled: false,
            },
            {
                id: "2",
                label: "赢单",
                text: "赢单",
                "data":"",
                "codename":"Item_2",
                value: 2,
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Productsubstitute__SalesRelationshipType",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "1",
                label: "交叉销售",
                text: "交叉销售",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "3",
                label: "替代品",
                text: "替代品",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
            {
                id: "0",
                label: "追加销售",
                text: "追加销售",
                "data":"",
                "codename":"Item_0",
                value: "0",
                
                disabled: false,
            },
            {
                id: "2",
                label: "配件",
                text: "配件",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Opportunity__PriorityCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "1",
                label: "默认值",
                text: "默认值",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Invoice__StatusCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "7",
                label: "全额付款(已弃用)",
                text: "全额付款(已弃用)",
                "data":"",
                "codename":"Item_7",
                value: "7",
                
                disabled: false,
            },
            {
                id: "100001",
                label: "完成",
                text: "完成",
                "data":"",
                "codename":"Item_100001",
                value: "100001",
                
                disabled: false,
            },
            {
                id: "100003",
                label: "已取消",
                text: "已取消",
                "data":"",
                "codename":"Item_100003",
                value: "100003",
                
                disabled: false,
            },
            {
                id: "3",
                label: "已取消(已弃用)",
                text: "已取消(已弃用)",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
            {
                id: "6",
                label: "已安装(适用于服务)",
                text: "已安装(适用于服务)",
                "data":"",
                "codename":"Item_6",
                value: "6",
                
                disabled: false,
            },
            {
                id: "4",
                label: "已记帐",
                text: "已记帐",
                "data":"",
                "codename":"Item_4",
                value: "4",
                
                disabled: false,
            },
            {
                id: "5",
                label: "已预定(适用于服务)",
                text: "已预定(适用于服务)",
                "data":"",
                "codename":"Item_5",
                value: "5",
                
                disabled: false,
            },
            {
                id: "1",
                label: "新建",
                text: "新建",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "100002",
                label: "部分",
                text: "部分",
                "data":"",
                "codename":"Item_100002",
                value: "100002",
                
                disabled: false,
            },
            {
                id: "2",
                label: "部分发货",
                text: "部分发货",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Contact__StateCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "1",
                label: "停用",
                text: "停用",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "0",
                label: "可用",
                text: "可用",
                "data":"",
                "codename":"Item_0",
                value: "0",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Knowledgearticleincident__StatusCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "2",
                label: "停用",
                text: "停用",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "1",
                label: "有效",
                text: "有效",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Lead__PurchaseTimeFrame",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "2",
                label: "下一季度",
                text: "下一季度",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "3",
                label: "今年",
                text: "今年",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
            {
                id: "4",
                label: "未知",
                text: "未知",
                "data":"",
                "codename":"Item_4",
                value: "4",
                
                disabled: false,
            },
            {
                id: "1",
                label: "本季度",
                text: "本季度",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "0",
                label: "立即",
                text: "立即",
                "data":"",
                "codename":"Item_0",
                value: "0",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Campaign__TypeCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "3",
                label: "事件",
                text: "事件",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
            {
                id: "5",
                label: "其他",
                text: "其他",
                "data":"",
                "codename":"Item_5",
                value: "5",
                
                disabled: false,
            },
            {
                id: "1",
                label: "广告",
                text: "广告",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "2",
                label: "直销",
                text: "直销",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "4",
                label: "联合品牌",
                text: "联合品牌",
                "data":"",
                "codename":"Item_4",
                value: "4",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Invoice__StateCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "3",
                label: "已取消",
                text: "已取消",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
            {
                id: "2",
                label: "已支付",
                text: "已支付",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "1",
                label: "已结束(已弃用)",
                text: "已结束(已弃用)",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "0",
                label: "有效",
                text: "有效",
                "data":"",
                "codename":"Item_0",
                value: "0",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "CLAuthCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "200",
                label: "成功",
                text: "成功",
                "data":"",
                "codename":"Item_200",
                "color": "rgba(0, 255, 0, 0)",
                value: "200",
                
                disabled: false,
            },
            {
                id: "400",
                label: "用户不存在",
                text: "用户不存在",
                "data":"",
                "codename":"Item_400",
                value: "400",
                
                disabled: false,
            },
            {
                id: "401.1",
                label: "密码错误",
                text: "密码错误",
                "data":"",
                "codename":"Item_3",
                value: "401.1",
                
                disabled: false,
            },
            {
                id: "401.2",
                label: "配置错误",
                text: "配置错误",
                "data":"",
                "codename":"Item_4",
                "color": "rgba(22, 9, 170, 1)",
                value: "401.2",
                
                disabled: false,
            },
            {
                id: "403.6",
                label: "地址被拒绝",
                text: "地址被拒绝",
                "data":"",
                "codename":"Item_5",
                "color": "rgba(0, 72, 255, 1)",
                value: "403.6",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Lead__LeadSourceCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "8",
                label: "Web",
                text: "Web",
                "data":"",
                "codename":"Item_8",
                value: "8",
                
                disabled: false,
            },
            {
                id: "5",
                label: "公共关系",
                text: "公共关系",
                "data":"",
                "codename":"Item_5",
                value: "5",
                
                disabled: false,
            },
            {
                id: "10",
                label: "其他",
                text: "其他",
                "data":"",
                "codename":"Item_10",
                value: "10",
                
                disabled: false,
            },
            {
                id: "9",
                label: "口碑",
                text: "口碑",
                "data":"",
                "codename":"Item_9",
                value: "9",
                
                disabled: false,
            },
            {
                id: "4",
                label: "合作伙伴",
                text: "合作伙伴",
                "data":"",
                "codename":"Item_4",
                value: "4",
                
                disabled: false,
            },
            {
                id: "2",
                label: "员工推荐",
                text: "员工推荐",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "3",
                label: "外部推荐",
                text: "外部推荐",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
            {
                id: "7",
                label: "展销会",
                text: "展销会",
                "data":"",
                "codename":"Item_7",
                value: "7",
                
                disabled: false,
            },
            {
                id: "1",
                label: "广告",
                text: "广告",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "6",
                label: "研讨会",
                text: "研讨会",
                "data":"",
                "codename":"Item_6",
                value: "6",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Contact__StatusCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "2",
                label: "停用",
                text: "停用",
                "data":"",
                "codename":"Item_2",
                "color": "rgba(255, 25, 0, 1)",
                value: "2",
                
                disabled: false,
            },
            {
                id: "1",
                label: "可用",
                text: "可用",
                "data":"",
                "codename":"Item_1",
                "color": "rgba(0, 255, 72, 1)",
                value: "1",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Salesliterature__LiteratureTypeCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "1",
                label: "产品表单",
                text: "产品表单",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "7",
                label: "价格表单",
                text: "价格表单",
                "data":"",
                "codename":"Item_7",
                value: "7",
                
                disabled: false,
            },
            {
                id: "9",
                label: "公司背景",
                text: "公司背景",
                "data":"",
                "codename":"Item_9",
                value: "9",
                
                disabled: false,
            },
            {
                id: "6",
                label: "公告",
                text: "公告",
                "data":"",
                "codename":"Item_6",
                value: "6",
                
                disabled: false,
            },
            {
                id: "100001",
                label: "市场营销宣传材料",
                text: "市场营销宣传材料",
                "data":"",
                "codename":"Item_100001",
                value: "100001",
                
                disabled: false,
            },
            {
                id: "8",
                label: "手册",
                text: "手册",
                "data":"",
                "codename":"Item_8",
                value: "8",
                
                disabled: false,
            },
            {
                id: "5",
                label: "新闻",
                text: "新闻",
                "data":"",
                "codename":"Item_5",
                value: "5",
                
                disabled: false,
            },
            {
                id: "0",
                label: "演示文稿",
                text: "演示文稿",
                "data":"",
                "codename":"Item_0",
                value: "0",
                
                disabled: false,
            },
            {
                id: "4",
                label: "电子表格",
                text: "电子表格",
                "data":"",
                "codename":"Item_4",
                value: "4",
                
                disabled: false,
            },
            {
                id: "2",
                label: "策略和步骤",
                text: "策略和步骤",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "3",
                label: "销售宣传资料",
                text: "销售宣传资料",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
        ]
    },
    {
        "srfkey": "SysOperator",
        "emptytext": "未定义",
        "codelisttype":"dynamic",
        "appdataentity":"",
        "appdedataset":"",
        "items": []
    },
    {
        srfkey: "CL_HR_0009",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "IDCARD",
                label: "身份证",
                text: "身份证",
                "data":"",
                "codename":"Idcard",
                value: "IDCARD",
                
                disabled: false,
            },
            {
                id: "PASSPORT",
                label: "护照",
                text: "护照",
                "data":"",
                "codename":"Passport",
                value: "PASSPORT",
                
                disabled: false,
            },
            {
                id: "OTHER",
                label: "其他",
                text: "其他",
                "data":"",
                "codename":"Other",
                value: "OTHER",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Account__OwnershipCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "1",
                label: "公共事业",
                text: "公共事业",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "4",
                label: "其他",
                text: "其他",
                "data":"",
                "codename":"Item_4",
                value: "4",
                
                disabled: false,
            },
            {
                id: "3",
                label: "子公司",
                text: "子公司",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
            {
                id: "2",
                label: "私人公司",
                text: "私人公司",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "AccountCategoryCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "2",
                label: "标准",
                text: "标准",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "1",
                label: "首选客户",
                text: "首选客户",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Connectionrole__ComponentState",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "2",
                label: "已删除",
                text: "已删除",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "3",
                label: "已删除未发布内容",
                text: "已删除未发布内容",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
            {
                id: "0",
                label: "已发布",
                text: "已发布",
                "data":"",
                "codename":"Item_0",
                value: "0",
                
                disabled: false,
            },
            {
                id: "1",
                label: "未发布",
                text: "未发布",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Campaign__StatusCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "6",
                label: "停用",
                text: "停用",
                "data":"",
                "codename":"Item_6",
                value: "6",
                
                disabled: false,
            },
            {
                id: "1",
                label: "准备启动",
                text: "准备启动",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "4",
                label: "已取消",
                text: "已取消",
                "data":"",
                "codename":"Item_4",
                value: "4",
                
                disabled: false,
            },
            {
                id: "2",
                label: "已启动",
                text: "已启动",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "3",
                label: "已完成",
                text: "已完成",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
            {
                id: "0",
                label: "已建议",
                text: "已建议",
                "data":"",
                "codename":"Item_0",
                value: "0",
                
                disabled: false,
            },
            {
                id: "5",
                label: "已暂停",
                text: "已暂停",
                "data":"",
                "codename":"Item_5",
                value: "5",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Lead__StateCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "2",
                label: "不合格",
                text: "不合格",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "1",
                label: "已合格",
                text: "已合格",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "0",
                label: "已开始",
                text: "已开始",
                "data":"",
                "codename":"Item_0",
                value: "0",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "YesNo",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "1",
                label: "是",
                text: "是",
                "data":"",
                "codename":"Item_1",
                value: 1,
                
                disabled: false,
            },
            {
                id: "0",
                label: "否",
                text: "否",
                "data":"",
                "codename":"Item_0",
                value: 0,
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Task__PriorityCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "0",
                label: "低",
                text: "低",
                "data":"",
                "codename":"Item_0",
                value: "0",
                
                disabled: false,
            },
            {
                id: "1",
                label: "正常",
                text: "正常",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "2",
                label: "高",
                text: "高",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "AccountRatingCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "1",
                label: "默认值",
                text: "默认值",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Productsubstitute__Direction",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "0",
                label: "单向",
                text: "单向",
                "data":"",
                "codename":"Item_0",
                value: "0",
                
                disabled: false,
            },
            {
                id: "1",
                label: "双向",
                text: "双向",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Campaign__StateCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "1",
                label: "停用",
                text: "停用",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "0",
                label: "有效",
                text: "有效",
                "data":"",
                "codename":"Item_0",
                value: "0",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "PickDataType_AC",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "ACCOUNT",
                label: "客户",
                text: "客户",
                "data":{"n_pickdatatype_eq":"ACCOUNT"},
                "codename":"Account",
                value: "ACCOUNT",
                
                disabled: false,
            },
            {
                id: "CONTACT",
                label: "联系人",
                text: "联系人",
                "data":{"n_pickdatatype_eq":"CONTACT"},
                "codename":"Contact",
                value: "CONTACT",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Salesorder__PaymentTermsCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "2",
                label: "2/10 N30",
                text: "2/10 N30",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "1",
                label: "N30",
                text: "N30",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "3",
                label: "N45",
                text: "N45",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
            {
                id: "4",
                label: "N60",
                text: "N60",
                "data":"",
                "codename":"Item_4",
                value: "4",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Account__CustomerTypeCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "11",
                label: "供应商",
                text: "供应商",
                "data":"",
                "codename":"Item_11",
                value: "11",
                
                disabled: false,
            },
            {
                id: "10",
                label: "供货商",
                text: "供货商",
                "data":"",
                "codename":"Item_10",
                value: "10",
                
                disabled: false,
            },
            {
                id: "12",
                label: "其他",
                text: "其他",
                "data":"",
                "codename":"Item_12",
                value: "12",
                
                disabled: false,
            },
            {
                id: "5",
                label: "合作伙伴",
                text: "合作伙伴",
                "data":"",
                "codename":"Item_5",
                value: "5",
                
                disabled: false,
            },
            {
                id: "3",
                label: "客户",
                text: "客户",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
            {
                id: "6",
                label: "影响者",
                text: "影响者",
                "data":"",
                "codename":"Item_6",
                value: "6",
                
                disabled: false,
            },
            {
                id: "4",
                label: "投资者",
                text: "投资者",
                "data":"",
                "codename":"Item_4",
                value: "4",
                
                disabled: false,
            },
            {
                id: "7",
                label: "新闻界",
                text: "新闻界",
                "data":"",
                "codename":"Item_7",
                value: "7",
                
                disabled: false,
            },
            {
                id: "8",
                label: "目标客户",
                text: "目标客户",
                "data":"",
                "codename":"Item_8",
                value: "8",
                
                disabled: false,
            },
            {
                id: "1",
                label: "竞争对手",
                text: "竞争对手",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "9",
                label: "经销商",
                text: "经销商",
                "data":"",
                "codename":"Item_9",
                value: "9",
                
                disabled: false,
            },
            {
                id: "2",
                label: "顾问",
                text: "顾问",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "AppType",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "INNER",
                label: "内置应用",
                text: "内置应用",
                "data":"",
                "codename":"Inner",
                value: "INNER",
                
                disabled: false,
            },
            {
                id: "THIRD-PARTY",
                label: "第三方应用",
                text: "第三方应用",
                "data":"",
                "codename":"Third_SUB_party",
                value: "THIRD-PARTY",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Product__StateCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "1",
                label: "已停用",
                text: "已停用",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "0",
                label: "有效",
                text: "有效",
                "data":"",
                "codename":"Item_0",
                value: "0",
                
                disabled: false,
            },
            {
                id: "3",
                label: "正在修订",
                text: "正在修订",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
            {
                id: "2",
                label: "草稿",
                text: "草稿",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "PickDataType",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "ACCOUNT",
                label: "客户",
                text: "客户",
                "data":"",
                "codename":"Account",
                value: "ACCOUNT",
                
                disabled: false,
            },
            {
                id: "CONTACT",
                label: "联系人",
                text: "联系人",
                "data":"",
                "codename":"Contact",
                value: "CONTACT",
                
                disabled: false,
            },
            {
                id: "LEAD",
                label: "潜在客户",
                text: "潜在客户",
                "data":"",
                "codename":"Lead",
                value: "LEAD",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Lead__PurchaseProcess",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "0",
                label: "个人",
                text: "个人",
                "data":"",
                "codename":"Item_0",
                value: "0",
                
                disabled: false,
            },
            {
                id: "1",
                label: "委员会",
                text: "委员会",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "2",
                label: "未知",
                text: "未知",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Salesorder__StatusCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "100001",
                label: "完成",
                text: "完成",
                "data":"",
                "codename":"Item_100001",
                value: "100001",
                
                disabled: false,
            },
            {
                id: "100003",
                label: "已开发票",
                text: "已开发票",
                "data":"",
                "codename":"Item_100003",
                value: "100003",
                
                disabled: false,
            },
            {
                id: "2",
                label: "待定",
                text: "待定",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "1",
                label: "新建",
                text: "新建",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "4",
                label: "无现金",
                text: "无现金",
                "data":"",
                "codename":"Item_4",
                value: "4",
                
                disabled: false,
            },
            {
                id: "3",
                label: "正在进行",
                text: "正在进行",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
            {
                id: "100002",
                label: "部分",
                text: "部分",
                "data":"",
                "codename":"Item_100002",
                value: "100002",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "CL_HR_0008",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "汉",
                label: "汉",
                text: "汉",
                "data":"",
                "codename":"Item_1",
                value: "汉",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Lead__StatusCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "6",
                label: "不再感兴趣",
                text: "不再感兴趣",
                "data":"",
                "codename":"Item_6",
                value: "6",
                
                disabled: false,
            },
            {
                id: "4",
                label: "丢单",
                text: "丢单",
                "data":"",
                "codename":"Item_4",
                value: "4",
                
                disabled: false,
            },
            {
                id: "7",
                label: "已取消",
                text: "已取消",
                "data":"",
                "codename":"Item_7",
                value: "7",
                
                disabled: false,
            },
            {
                id: "3",
                label: "已合格",
                text: "已合格",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
            {
                id: "2",
                label: "已联系",
                text: "已联系",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "1",
                label: "新建",
                text: "新建",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "5",
                label: "无法联系",
                text: "无法联系",
                "data":"",
                "codename":"Item_5",
                value: "5",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Incident__StateCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "2",
                label: "已取消",
                text: "已取消",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "1",
                label: "已解决",
                text: "已解决",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "0",
                label: "有效",
                text: "有效",
                "data":"",
                "codename":"Item_0",
                value: "0",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Opportunity__PurchaseProcess",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "0",
                label: "个人",
                text: "个人",
                "data":"",
                "codename":"Item_0",
                value: "0",
                
                disabled: false,
            },
            {
                id: "1",
                label: "委员会",
                text: "委员会",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "2",
                label: "未知",
                text: "未知",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Activitypointer__StateCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "2",
                label: "已取消",
                text: "已取消",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "1",
                label: "已完成",
                text: "已完成",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "0",
                label: "已开启",
                text: "已开启",
                "data":"",
                "codename":"Item_0",
                value: "0",
                
                disabled: false,
            },
            {
                id: "3",
                label: "已计划",
                text: "已计划",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Contact__CustomerSizeCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "1",
                label: "默认值",
                text: "默认值",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Goal__FiscalYear",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "1970",
                label: "1970 财年",
                text: "1970 财年",
                "data":"",
                "codename":"Item_1970",
                value: "1970",
                
                disabled: false,
            },
            {
                id: "1971",
                label: "1971 财年",
                text: "1971 财年",
                "data":"",
                "codename":"Item_1971",
                value: "1971",
                
                disabled: false,
            },
            {
                id: "1972",
                label: "1972 财年",
                text: "1972 财年",
                "data":"",
                "codename":"Item_1972",
                value: "1972",
                
                disabled: false,
            },
            {
                id: "1973",
                label: "1973 财年",
                text: "1973 财年",
                "data":"",
                "codename":"Item_1973",
                value: "1973",
                
                disabled: false,
            },
            {
                id: "1974",
                label: "1974 财年",
                text: "1974 财年",
                "data":"",
                "codename":"Item_1974",
                value: "1974",
                
                disabled: false,
            },
            {
                id: "1975",
                label: "1975 财年",
                text: "1975 财年",
                "data":"",
                "codename":"Item_1975",
                value: "1975",
                
                disabled: false,
            },
            {
                id: "1976",
                label: "1976 财年",
                text: "1976 财年",
                "data":"",
                "codename":"Item_1976",
                value: "1976",
                
                disabled: false,
            },
            {
                id: "1977",
                label: "1977 财年",
                text: "1977 财年",
                "data":"",
                "codename":"Item_1977",
                value: "1977",
                
                disabled: false,
            },
            {
                id: "1978",
                label: "1978 财年",
                text: "1978 财年",
                "data":"",
                "codename":"Item_1978",
                value: "1978",
                
                disabled: false,
            },
            {
                id: "1979",
                label: "1979 财年",
                text: "1979 财年",
                "data":"",
                "codename":"Item_1979",
                value: "1979",
                
                disabled: false,
            },
            {
                id: "1980",
                label: "1980 财年",
                text: "1980 财年",
                "data":"",
                "codename":"Item_1980",
                value: "1980",
                
                disabled: false,
            },
            {
                id: "1981",
                label: "1981 财年",
                text: "1981 财年",
                "data":"",
                "codename":"Item_1981",
                value: "1981",
                
                disabled: false,
            },
            {
                id: "1982",
                label: "1982 财年",
                text: "1982 财年",
                "data":"",
                "codename":"Item_1982",
                value: "1982",
                
                disabled: false,
            },
            {
                id: "1983",
                label: "1983 财年",
                text: "1983 财年",
                "data":"",
                "codename":"Item_1983",
                value: "1983",
                
                disabled: false,
            },
            {
                id: "1984",
                label: "1984 财年",
                text: "1984 财年",
                "data":"",
                "codename":"Item_1984",
                value: "1984",
                
                disabled: false,
            },
            {
                id: "1985",
                label: "1985 财年",
                text: "1985 财年",
                "data":"",
                "codename":"Item_1985",
                value: "1985",
                
                disabled: false,
            },
            {
                id: "1986",
                label: "1986 财年",
                text: "1986 财年",
                "data":"",
                "codename":"Item_1986",
                value: "1986",
                
                disabled: false,
            },
            {
                id: "1987",
                label: "1987 财年",
                text: "1987 财年",
                "data":"",
                "codename":"Item_1987",
                value: "1987",
                
                disabled: false,
            },
            {
                id: "1988",
                label: "1988 财年",
                text: "1988 财年",
                "data":"",
                "codename":"Item_1988",
                value: "1988",
                
                disabled: false,
            },
            {
                id: "1989",
                label: "1989 财年",
                text: "1989 财年",
                "data":"",
                "codename":"Item_1989",
                value: "1989",
                
                disabled: false,
            },
            {
                id: "1990",
                label: "1990 财年",
                text: "1990 财年",
                "data":"",
                "codename":"Item_1990",
                value: "1990",
                
                disabled: false,
            },
            {
                id: "1991",
                label: "1991 财年",
                text: "1991 财年",
                "data":"",
                "codename":"Item_1991",
                value: "1991",
                
                disabled: false,
            },
            {
                id: "1992",
                label: "1992 财年",
                text: "1992 财年",
                "data":"",
                "codename":"Item_1992",
                value: "1992",
                
                disabled: false,
            },
            {
                id: "1993",
                label: "1993 财年",
                text: "1993 财年",
                "data":"",
                "codename":"Item_1993",
                value: "1993",
                
                disabled: false,
            },
            {
                id: "1994",
                label: "1994 财年",
                text: "1994 财年",
                "data":"",
                "codename":"Item_1994",
                value: "1994",
                
                disabled: false,
            },
            {
                id: "1995",
                label: "1995 财年",
                text: "1995 财年",
                "data":"",
                "codename":"Item_1995",
                value: "1995",
                
                disabled: false,
            },
            {
                id: "1996",
                label: "1996 财年",
                text: "1996 财年",
                "data":"",
                "codename":"Item_1996",
                value: "1996",
                
                disabled: false,
            },
            {
                id: "1997",
                label: "1997 财年",
                text: "1997 财年",
                "data":"",
                "codename":"Item_1997",
                value: "1997",
                
                disabled: false,
            },
            {
                id: "1998",
                label: "1998 财年",
                text: "1998 财年",
                "data":"",
                "codename":"Item_1998",
                value: "1998",
                
                disabled: false,
            },
            {
                id: "1999",
                label: "1999 财年",
                text: "1999 财年",
                "data":"",
                "codename":"Item_1999",
                value: "1999",
                
                disabled: false,
            },
            {
                id: "2000",
                label: "2000 财年",
                text: "2000 财年",
                "data":"",
                "codename":"Item_2000",
                value: "2000",
                
                disabled: false,
            },
            {
                id: "2001",
                label: "2001 财年",
                text: "2001 财年",
                "data":"",
                "codename":"Item_2001",
                value: "2001",
                
                disabled: false,
            },
            {
                id: "2002",
                label: "2002 财年",
                text: "2002 财年",
                "data":"",
                "codename":"Item_2002",
                value: "2002",
                
                disabled: false,
            },
            {
                id: "2003",
                label: "2003 财年",
                text: "2003 财年",
                "data":"",
                "codename":"Item_2003",
                value: "2003",
                
                disabled: false,
            },
            {
                id: "2004",
                label: "2004 财年",
                text: "2004 财年",
                "data":"",
                "codename":"Item_2004",
                value: "2004",
                
                disabled: false,
            },
            {
                id: "2005",
                label: "2005 财年",
                text: "2005 财年",
                "data":"",
                "codename":"Item_2005",
                value: "2005",
                
                disabled: false,
            },
            {
                id: "2006",
                label: "2006 财年",
                text: "2006 财年",
                "data":"",
                "codename":"Item_2006",
                value: "2006",
                
                disabled: false,
            },
            {
                id: "2007",
                label: "2007 财年",
                text: "2007 财年",
                "data":"",
                "codename":"Item_2007",
                value: "2007",
                
                disabled: false,
            },
            {
                id: "2008",
                label: "2008 财年",
                text: "2008 财年",
                "data":"",
                "codename":"Item_2008",
                value: "2008",
                
                disabled: false,
            },
            {
                id: "2009",
                label: "2009 财年",
                text: "2009 财年",
                "data":"",
                "codename":"Item_2009",
                value: "2009",
                
                disabled: false,
            },
            {
                id: "2010",
                label: "2010 财年",
                text: "2010 财年",
                "data":"",
                "codename":"Item_2010",
                value: "2010",
                
                disabled: false,
            },
            {
                id: "2011",
                label: "2011 财年",
                text: "2011 财年",
                "data":"",
                "codename":"Item_2011",
                value: "2011",
                
                disabled: false,
            },
            {
                id: "2012",
                label: "2012 财年",
                text: "2012 财年",
                "data":"",
                "codename":"Item_2012",
                value: "2012",
                
                disabled: false,
            },
            {
                id: "2013",
                label: "2013 财年",
                text: "2013 财年",
                "data":"",
                "codename":"Item_2013",
                value: "2013",
                
                disabled: false,
            },
            {
                id: "2014",
                label: "2014 财年",
                text: "2014 财年",
                "data":"",
                "codename":"Item_2014",
                value: "2014",
                
                disabled: false,
            },
            {
                id: "2015",
                label: "2015 财年",
                text: "2015 财年",
                "data":"",
                "codename":"Item_2015",
                value: "2015",
                
                disabled: false,
            },
            {
                id: "2016",
                label: "2016 财年",
                text: "2016 财年",
                "data":"",
                "codename":"Item_2016",
                value: "2016",
                
                disabled: false,
            },
            {
                id: "2017",
                label: "2017 财年",
                text: "2017 财年",
                "data":"",
                "codename":"Item_2017",
                value: "2017",
                
                disabled: false,
            },
            {
                id: "2018",
                label: "2018 财年",
                text: "2018 财年",
                "data":"",
                "codename":"Item_2018",
                value: "2018",
                
                disabled: false,
            },
            {
                id: "2019",
                label: "2019 财年",
                text: "2019 财年",
                "data":"",
                "codename":"Item_2019",
                value: "2019",
                
                disabled: false,
            },
            {
                id: "2020",
                label: "2020 财年",
                text: "2020 财年",
                "data":"",
                "codename":"Item_2020",
                value: "2020",
                
                disabled: false,
            },
            {
                id: "2021",
                label: "2021 财年",
                text: "2021 财年",
                "data":"",
                "codename":"Item_2021",
                value: "2021",
                
                disabled: false,
            },
            {
                id: "2022",
                label: "2022 财年",
                text: "2022 财年",
                "data":"",
                "codename":"Item_2022",
                value: "2022",
                
                disabled: false,
            },
            {
                id: "2023",
                label: "2023 财年",
                text: "2023 财年",
                "data":"",
                "codename":"Item_2023",
                value: "2023",
                
                disabled: false,
            },
            {
                id: "2024",
                label: "2024 财年",
                text: "2024 财年",
                "data":"",
                "codename":"Item_2024",
                value: "2024",
                
                disabled: false,
            },
            {
                id: "2025",
                label: "2025 财年",
                text: "2025 财年",
                "data":"",
                "codename":"Item_2025",
                value: "2025",
                
                disabled: false,
            },
            {
                id: "2026",
                label: "2026 财年",
                text: "2026 财年",
                "data":"",
                "codename":"Item_2026",
                value: "2026",
                
                disabled: false,
            },
            {
                id: "2027",
                label: "2027 财年",
                text: "2027 财年",
                "data":"",
                "codename":"Item_2027",
                value: "2027",
                
                disabled: false,
            },
            {
                id: "2028",
                label: "2028 财年",
                text: "2028 财年",
                "data":"",
                "codename":"Item_2028",
                value: "2028",
                
                disabled: false,
            },
            {
                id: "2029",
                label: "2029 财年",
                text: "2029 财年",
                "data":"",
                "codename":"Item_2029",
                value: "2029",
                
                disabled: false,
            },
            {
                id: "2030",
                label: "2030 财年",
                text: "2030 财年",
                "data":"",
                "codename":"Item_2030",
                value: "2030",
                
                disabled: false,
            },
            {
                id: "2031",
                label: "2031 财年",
                text: "2031 财年",
                "data":"",
                "codename":"Item_2031",
                value: "2031",
                
                disabled: false,
            },
            {
                id: "2032",
                label: "2032 财年",
                text: "2032 财年",
                "data":"",
                "codename":"Item_2032",
                value: "2032",
                
                disabled: false,
            },
            {
                id: "2033",
                label: "2033 财年",
                text: "2033 财年",
                "data":"",
                "codename":"Item_2033",
                value: "2033",
                
                disabled: false,
            },
            {
                id: "2034",
                label: "2034 财年",
                text: "2034 财年",
                "data":"",
                "codename":"Item_2034",
                value: "2034",
                
                disabled: false,
            },
            {
                id: "2035",
                label: "2035 财年",
                text: "2035 财年",
                "data":"",
                "codename":"Item_2035",
                value: "2035",
                
                disabled: false,
            },
            {
                id: "2036",
                label: "2036 财年",
                text: "2036 财年",
                "data":"",
                "codename":"Item_2036",
                value: "2036",
                
                disabled: false,
            },
            {
                id: "2037",
                label: "2037 财年",
                text: "2037 财年",
                "data":"",
                "codename":"Item_2037",
                value: "2037",
                
                disabled: false,
            },
            {
                id: "2038",
                label: "2038 财年",
                text: "2038 财年",
                "data":"",
                "codename":"Item_2038",
                value: "2038",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Connectionrole__Category",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "1",
                label: "业务",
                text: "业务",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "5",
                label: "其他",
                text: "其他",
                "data":"",
                "codename":"Item_5",
                value: "5",
                
                disabled: false,
            },
            {
                id: "1000",
                label: "利益干系人",
                text: "利益干系人",
                "data":"",
                "codename":"Item_1000",
                value: "1000",
                
                disabled: false,
            },
            {
                id: "2",
                label: "家庭",
                text: "家庭",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "1002",
                label: "服务",
                text: "服务",
                "data":"",
                "codename":"Item_1002",
                value: "1002",
                
                disabled: false,
            },
            {
                id: "3",
                label: "社团组织",
                text: "社团组织",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
            {
                id: "4",
                label: "销售",
                text: "销售",
                "data":"",
                "codename":"Item_4",
                value: "4",
                
                disabled: false,
            },
            {
                id: "1001",
                label: "销售团队",
                text: "销售团队",
                "data":"",
                "codename":"Item_1001",
                value: "1001",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "FreightTermsCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "1",
                label: "FOB",
                text: "FOB",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "2",
                label: "免收费用",
                text: "免收费用",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Connectionrole__StatusCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "2",
                label: "停用",
                text: "停用",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "1",
                label: "可用",
                text: "可用",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Productpricelevel__PricingMethodCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "2",
                label: "定价百分比",
                text: "定价百分比",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "5",
                label: "成本加成百分比 - 标准成本",
                text: "成本加成百分比 - 标准成本",
                "data":"",
                "codename":"Item_5",
                value: "5",
                
                disabled: false,
            },
            {
                id: "3",
                label: "成本加成百分比 – 当前成本",
                text: "成本加成百分比 – 当前成本",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
            {
                id: "1",
                label: "货币金额",
                text: "货币金额",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "4",
                label: "边际百分比 – 当前成本",
                text: "边际百分比 – 当前成本",
                "data":"",
                "codename":"Item_4",
                value: "4",
                
                disabled: false,
            },
            {
                id: "6",
                label: "边际百分比 – 标准成本",
                text: "边际百分比 – 标准成本",
                "data":"",
                "codename":"Item_6",
                value: "6",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Productpricelevel__QuantitySellingCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "2",
                label: "整数",
                text: "整数",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "3",
                label: "整数和小数",
                text: "整数和小数",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
            {
                id: "1",
                label: "无控制",
                text: "无控制",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "IndustryCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "2",
                label: "Agriculture and Non-petrol Natural Resource Extraction",
                text: "Agriculture and Non-petrol Natural Resource Extraction",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "3",
                label: "Broadcasting Printing and Publishing",
                text: "Broadcasting Printing and Publishing",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
            {
                id: "4",
                label: "Brokers",
                text: "Brokers",
                "data":"",
                "codename":"Item_4",
                value: "4",
                
                disabled: false,
            },
            {
                id: "5",
                label: "Building Supply Retail",
                text: "Building Supply Retail",
                "data":"",
                "codename":"Item_5",
                value: "5",
                
                disabled: false,
            },
            {
                id: "6",
                label: "Business Services",
                text: "Business Services",
                "data":"",
                "codename":"Item_6",
                value: "6",
                
                disabled: false,
            },
            {
                id: "7",
                label: "Consulting",
                text: "Consulting",
                "data":"",
                "codename":"Item_7",
                value: "7",
                
                disabled: false,
            },
            {
                id: "8",
                label: "Consumer Services",
                text: "Consumer Services",
                "data":"",
                "codename":"Item_8",
                value: "8",
                
                disabled: false,
            },
            {
                id: "9",
                label: "Design, Direction and Creative Management",
                text: "Design, Direction and Creative Management",
                "data":"",
                "codename":"Item_9",
                value: "9",
                
                disabled: false,
            },
            {
                id: "10",
                label: "Distributors, Dispatchers and Processors",
                text: "Distributors, Dispatchers and Processors",
                "data":"",
                "codename":"Item_10",
                value: "10",
                
                disabled: false,
            },
            {
                id: "11",
                label: "Doctors Offices and Clinics",
                text: "Doctors Offices and Clinics",
                "data":"",
                "codename":"Item_11",
                value: "11",
                
                disabled: false,
            },
            {
                id: "12",
                label: "Durable Manufacturing",
                text: "Durable Manufacturing",
                "data":"",
                "codename":"Item_12",
                value: "12",
                
                disabled: false,
            },
            {
                id: "13",
                label: "Eating and Drinking Places",
                text: "Eating and Drinking Places",
                "data":"",
                "codename":"Item_13",
                value: "13",
                
                disabled: false,
            },
            {
                id: "14",
                label: "Entertainment Retail",
                text: "Entertainment Retail",
                "data":"",
                "codename":"Item_14",
                value: "14",
                
                disabled: false,
            },
            {
                id: "15",
                label: "Equipment Rental and Leasing",
                text: "Equipment Rental and Leasing",
                "data":"",
                "codename":"Item_15",
                value: "15",
                
                disabled: false,
            },
            {
                id: "17",
                label: "Food and Tobacco Processing",
                text: "Food and Tobacco Processing",
                "data":"",
                "codename":"Item_17",
                value: "17",
                
                disabled: false,
            },
            {
                id: "18",
                label: "Inbound Capital Intensive Processing",
                text: "Inbound Capital Intensive Processing",
                "data":"",
                "codename":"Item_18",
                value: "18",
                
                disabled: false,
            },
            {
                id: "19",
                label: "Inbound Repair and Services",
                text: "Inbound Repair and Services",
                "data":"",
                "codename":"Item_19",
                value: "19",
                
                disabled: false,
            },
            {
                id: "20",
                label: "Insurance",
                text: "Insurance",
                "data":"",
                "codename":"Item_20",
                value: "20",
                
                disabled: false,
            },
            {
                id: "21",
                label: "Legal Services",
                text: "Legal Services",
                "data":"",
                "codename":"Item_21",
                value: "21",
                
                disabled: false,
            },
            {
                id: "22",
                label: "Non-Durable Merchandise Retail",
                text: "Non-Durable Merchandise Retail",
                "data":"",
                "codename":"Item_22",
                value: "22",
                
                disabled: false,
            },
            {
                id: "23",
                label: "Outbound Consumer Service",
                text: "Outbound Consumer Service",
                "data":"",
                "codename":"Item_23",
                value: "23",
                
                disabled: false,
            },
            {
                id: "24",
                label: "Petrochemical Extraction and Distribution",
                text: "Petrochemical Extraction and Distribution",
                "data":"",
                "codename":"Item_24",
                value: "24",
                
                disabled: false,
            },
            {
                id: "25",
                label: "Service Retail",
                text: "Service Retail",
                "data":"",
                "codename":"Item_25",
                value: "25",
                
                disabled: false,
            },
            {
                id: "26",
                label: "SIG Affiliations",
                text: "SIG Affiliations",
                "data":"",
                "codename":"Item_26",
                value: "26",
                
                disabled: false,
            },
            {
                id: "27",
                label: "Social Services",
                text: "Social Services",
                "data":"",
                "codename":"Item_27",
                value: "27",
                
                disabled: false,
            },
            {
                id: "28",
                label: "Special Outbound Trade Contractors",
                text: "Special Outbound Trade Contractors",
                "data":"",
                "codename":"Item_28",
                value: "28",
                
                disabled: false,
            },
            {
                id: "29",
                label: "Specialty Realty",
                text: "Specialty Realty",
                "data":"",
                "codename":"Item_29",
                value: "29",
                
                disabled: false,
            },
            {
                id: "30",
                label: "Transportation",
                text: "Transportation",
                "data":"",
                "codename":"Item_30",
                value: "30",
                
                disabled: false,
            },
            {
                id: "31",
                label: "Utility Creation and Distribution",
                text: "Utility Creation and Distribution",
                "data":"",
                "codename":"Item_31",
                value: "31",
                
                disabled: false,
            },
            {
                id: "32",
                label: "Vehicle Retail",
                text: "Vehicle Retail",
                "data":"",
                "codename":"Item_32",
                value: "32",
                
                disabled: false,
            },
            {
                id: "33",
                label: "Wholesale",
                text: "Wholesale",
                "data":"",
                "codename":"Item_33",
                value: "33",
                
                disabled: false,
            },
            {
                id: "1",
                label: "会计",
                text: "会计",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "16",
                label: "金融业",
                text: "金融业",
                "data":"",
                "codename":"Item_16",
                value: "16",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Activitypointer__StatusCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "3",
                label: "已取消",
                text: "已取消",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
            {
                id: "2",
                label: "已完成",
                text: "已完成",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "1",
                label: "已开启",
                text: "已开启",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "4",
                label: "已计划",
                text: "已计划",
                "data":"",
                "codename":"Item_4",
                value: "4",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Opportunity__StatusCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "5",
                label: "售完",
                text: "售完",
                "data":"",
                "codename":"Item_5",
                value: "5",
                
                disabled: false,
            },
            {
                id: "4",
                label: "已取消",
                text: "已取消",
                "data":"",
                "codename":"Item_4",
                value: "4",
                
                disabled: false,
            },
            {
                id: "2",
                label: "暂候",
                text: "暂候",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "1",
                label: "正在进行",
                text: "正在进行",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "3",
                label: "赢单",
                text: "赢单",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Activitypointer__PriorityCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "0",
                label: "低",
                text: "低",
                "data":"",
                "codename":"Item_0",
                value: "0",
                
                disabled: false,
            },
            {
                id: "1",
                label: "正常",
                text: "正常",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "2",
                label: "高",
                text: "高",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Incident__CaseTypeCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "3",
                label: "请求",
                text: "请求",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
            {
                id: "1",
                label: "问题",
                text: "问题",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "2",
                label: "难题",
                text: "难题",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "List__MemberType",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "Account",
                label: "客户",
                text: "客户",
                "data":"",
                "codename":"Account",
                value: "Account",
                
                disabled: false,
            },
            {
                id: "Lead",
                label: "潜在客户",
                text: "潜在客户",
                "data":"",
                "codename":"Lead",
                value: "Lead",
                
                disabled: false,
            },
            {
                id: "Contact",
                label: "联系人",
                text: "联系人",
                "data":"",
                "codename":"Contact",
                value: "Contact",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "CLIBZSex",
        emptytext: "",
        "codelisttype":"static",
        items: [
            {
                id: "男",
                label: "男性",
                text: "男性",
                "data":"",
                "codename":"Item_1",
                value: "男",
                
                disabled: false,
            },
            {
                id: "女",
                label: "女性",
                text: "女性",
                "data":"",
                "codename":"Item_2",
                value: "女",
                
                disabled: false,
            },
            {
                id: "性别不详",
                label: "性别不详",
                text: "性别不详",
                "data":"",
                "codename":"Item_3",
                value: "性别不详",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "ActivityTypeCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "TASK",
                label: "任务",
                text: "任务",
                "data":{"N_ACTIVITYTYPECODE_EQ":"TASK"},
                "codename":"Task",
                value: "TASK",
                
                disabled: false,
            },
            {
                id: "EMAIL",
                label: "电子邮件",
                text: "电子邮件",
                "data":{"N_ACTIVITYTYPECODE_EQ":"EMAIL"},
                "codename":"Email",
                value: "EMAIL",
                
                disabled: false,
            },
            {
                id: "APPOINTMENT",
                label: "约会",
                text: "约会",
                "data":{"N_ACTIVITYTYPECODE_EQ":"APPOINTMENT"},
                "codename":"Appointment",
                value: "APPOINTMENT",
                
                disabled: false,
            },
            {
                id: "PHONECALL",
                label: "电话联络",
                text: "电话联络",
                "data":{"N_ACTIVITYTYPECODE_EQ":"PHONECALL"},
                "codename":"Phonecall",
                value: "PHONECALL",
                
                disabled: false,
            },
            {
                id: "LETTER",
                label: "信件",
                text: "信件",
                "data":{"N_ACTIVITYTYPECODE_EQ":"LETTER"},
                "codename":"Letter",
                value: "LETTER",
                
                disabled: false,
            },
            {
                id: "FAX",
                label: "传真",
                text: "传真",
                "data":"",
                "codename":"Fax",
                value: "FAX",
                
                disabled: false,
            },
            {
                id: "SERVICEAPPOINTMENT",
                label: "服务活动",
                text: "服务活动",
                "data":{"N_ACTIVITYTYPECODE_EQ":"SERVICEAPPOINTMENT"},
                "codename":"Serviceappointment",
                value: "SERVICEAPPOINTMENT",
                
                disabled: false,
            },
            {
                id: "CAMPAIGNRESPONSE",
                label: "市场活动响应",
                text: "市场活动响应",
                "data":{"N_ACTIVITYTYPECODE_EQ":"CAMPAIGNRESPONSE"},
                "codename":"Campaignresponse",
                value: "CAMPAIGNRESPONSE",
                
                disabled: false,
            },
            {
                id: "CAMPAIGNACTIVITY",
                label: "市场活动项目",
                text: "市场活动项目",
                "data":"",
                "codename":"Campaignactivity",
                value: "CAMPAIGNACTIVITY",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Pricelevel__PaymentMethodCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "1",
                label: "默认值",
                text: "默认值",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Goal__FiscalPeriod",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "1",
                label: "1 季度",
                text: "1 季度",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "2",
                label: "2 季度",
                text: "2 季度",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "3",
                label: "3 季度",
                text: "3 季度",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
            {
                id: "4",
                label: "4 季度",
                text: "4 季度",
                "data":"",
                "codename":"Item_4",
                value: "4",
                
                disabled: false,
            },
            {
                id: "401",
                label: "P1",
                text: "P1",
                "data":"",
                "codename":"Item_401",
                value: "401",
                
                disabled: false,
            },
            {
                id: "410",
                label: "P10",
                text: "P10",
                "data":"",
                "codename":"Item_410",
                value: "410",
                
                disabled: false,
            },
            {
                id: "411",
                label: "P11",
                text: "P11",
                "data":"",
                "codename":"Item_411",
                value: "411",
                
                disabled: false,
            },
            {
                id: "412",
                label: "P12",
                text: "P12",
                "data":"",
                "codename":"Item_412",
                value: "412",
                
                disabled: false,
            },
            {
                id: "413",
                label: "P13",
                text: "P13",
                "data":"",
                "codename":"Item_413",
                value: "413",
                
                disabled: false,
            },
            {
                id: "402",
                label: "P2",
                text: "P2",
                "data":"",
                "codename":"Item_402",
                value: "402",
                
                disabled: false,
            },
            {
                id: "403",
                label: "P3",
                text: "P3",
                "data":"",
                "codename":"Item_403",
                value: "403",
                
                disabled: false,
            },
            {
                id: "404",
                label: "P4",
                text: "P4",
                "data":"",
                "codename":"Item_404",
                value: "404",
                
                disabled: false,
            },
            {
                id: "405",
                label: "P5",
                text: "P5",
                "data":"",
                "codename":"Item_405",
                value: "405",
                
                disabled: false,
            },
            {
                id: "406",
                label: "P6",
                text: "P6",
                "data":"",
                "codename":"Item_406",
                value: "406",
                
                disabled: false,
            },
            {
                id: "407",
                label: "P7",
                text: "P7",
                "data":"",
                "codename":"Item_407",
                value: "407",
                
                disabled: false,
            },
            {
                id: "408",
                label: "P8",
                text: "P8",
                "data":"",
                "codename":"Item_408",
                value: "408",
                
                disabled: false,
            },
            {
                id: "409",
                label: "P9",
                text: "P9",
                "data":"",
                "codename":"Item_409",
                value: "409",
                
                disabled: false,
            },
            {
                id: "101",
                label: "一月",
                text: "一月",
                "data":"",
                "codename":"Item_101",
                value: "101",
                
                disabled: false,
            },
            {
                id: "107",
                label: "七月",
                text: "七月",
                "data":"",
                "codename":"Item_107",
                value: "107",
                
                disabled: false,
            },
            {
                id: "103",
                label: "三月",
                text: "三月",
                "data":"",
                "codename":"Item_103",
                value: "103",
                
                disabled: false,
            },
            {
                id: "201",
                label: "上半年",
                text: "上半年",
                "data":"",
                "codename":"Item_201",
                value: "201",
                
                disabled: false,
            },
            {
                id: "202",
                label: "下半年",
                text: "下半年",
                "data":"",
                "codename":"Item_202",
                value: "202",
                
                disabled: false,
            },
            {
                id: "109",
                label: "九月",
                text: "九月",
                "data":"",
                "codename":"Item_109",
                value: "109",
                
                disabled: false,
            },
            {
                id: "102",
                label: "二月",
                text: "二月",
                "data":"",
                "codename":"Item_102",
                value: "102",
                
                disabled: false,
            },
            {
                id: "105",
                label: "五月",
                text: "五月",
                "data":"",
                "codename":"Item_105",
                value: "105",
                
                disabled: false,
            },
            {
                id: "108",
                label: "八月",
                text: "八月",
                "data":"",
                "codename":"Item_108",
                value: "108",
                
                disabled: false,
            },
            {
                id: "106",
                label: "六月",
                text: "六月",
                "data":"",
                "codename":"Item_106",
                value: "106",
                
                disabled: false,
            },
            {
                id: "111",
                label: "十一月",
                text: "十一月",
                "data":"",
                "codename":"Item_111",
                value: "111",
                
                disabled: false,
            },
            {
                id: "112",
                label: "十二月",
                text: "十二月",
                "data":"",
                "codename":"Item_112",
                value: "112",
                
                disabled: false,
            },
            {
                id: "110",
                label: "十月",
                text: "十月",
                "data":"",
                "codename":"Item_110",
                value: "110",
                
                disabled: false,
            },
            {
                id: "104",
                label: "四月",
                text: "四月",
                "data":"",
                "codename":"Item_104",
                value: "104",
                
                disabled: false,
            },
            {
                id: "301",
                label: "年度",
                text: "年度",
                "data":"",
                "codename":"Item_301",
                value: "301",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Transactioncurrency__StatusCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "2",
                label: "停用",
                text: "停用",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "1",
                label: "可用",
                text: "可用",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Phonecall__PriorityCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "0",
                label: "低",
                text: "低",
                "data":"",
                "codename":"Item_0",
                value: "0",
                
                disabled: false,
            },
            {
                id: "1",
                label: "正常",
                text: "正常",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "2",
                label: "高",
                text: "高",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "IncidentCustomer",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "ACCOUNT",
                label: "客户",
                text: "客户",
                "data":{"n_customeridtype_eq":"ACCOUNT"},
                "codename":"Account",
                value: "ACCOUNT",
                
                disabled: false,
            },
            {
                id: "CONTACT",
                label: "联系人",
                text: "联系人",
                "data":{"n_customeridtype_eq":"CONTACT"},
                "codename":"Contact",
                value: "CONTACT",
                
                disabled: false,
            },
        ]
    },
    {
        "srfkey": "AddressTypeCode",
        "emptytext": "未定义",
        "codelisttype":"dynamic",
        "appdataentity":"",
        "appdedataset":"",
        "items": []
    },
    {
        srfkey: "ContentType",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "CONTENT",
                label: "内容",
                text: "内容",
                "data":"",
                "codename":"Content",
                value: "CONTENT",
                
                disabled: false,
            },
            {
                id: "LINK",
                label: "链接",
                text: "链接",
                "data":"",
                "codename":"Link",
                value: "LINK",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "CL_HR_0001",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "COSTCENTER",
                label: "成本中心",
                text: "成本中心",
                "data":"",
                "codename":"Costcenter",
                value: "COSTCENTER",
                
                disabled: false,
            },
            {
                id: "BUSINESSUNIT",
                label: "业务单位",
                text: "业务单位",
                "data":"",
                "codename":"Businessunit",
                value: "BUSINESSUNIT",
                
                disabled: false,
            },
            {
                id: "DEPARTMENT",
                label: "部门",
                text: "部门",
                "data":"",
                "codename":"Department",
                value: "DEPARTMENT",
                
                disabled: false,
            },
            {
                id: "COMMERCIAL",
                label: "商业渠道",
                text: "商业渠道",
                "data":"",
                "codename":"Commercial",
                value: "COMMERCIAL",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Knowledgearticleincident__StateCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "1",
                label: "停用",
                text: "停用",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "0",
                label: "有效",
                text: "有效",
                "data":"",
                "codename":"Item_0",
                value: "0",
                
                disabled: false,
            },
        ]
    },
    {
        "srfkey": "RelationshipsType",
        "emptytext": "未定义",
        "codelisttype":"dynamic",
        "appdataentity":"",
        "appdedataset":"",
        "items": []
    },
    {
        srfkey: "CL_HR_0010",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "MALE",
                label: "男",
                text: "男",
                "data":"",
                "codename":"Male",
                value: "MALE",
                
                disabled: false,
            },
            {
                id: "FEMALE",
                label: "女",
                text: "女",
                "data":"",
                "codename":"Female",
                value: "FEMALE",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Transactioncurrency__StateCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "1",
                label: "停用",
                text: "停用",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "0",
                label: "可用",
                text: "可用",
                "data":"",
                "codename":"Item_0",
                value: "0",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Connectionrole__StateCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "1",
                label: "停用",
                text: "停用",
                "data":"",
                "codename":"Item_1",
                value: 1,
                
                disabled: false,
            },
            {
                id: "0",
                label: "可用",
                text: "可用",
                "data":"",
                "codename":"Item_0",
                value: 0,
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Contact__FamilyStatusCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "4",
                label: "丧偶",
                text: "丧偶",
                "data":"",
                "codename":"Item_4",
                value: "4",
                
                disabled: false,
            },
            {
                id: "1",
                label: "单身",
                text: "单身",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "2",
                label: "已婚",
                text: "已婚",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "3",
                label: "离异",
                text: "离异",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Incident__CaseOriginCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "2483",
                label: "Facebook",
                text: "Facebook",
                "data":"",
                "codename":"Item_2483",
                value: "2483",
                
                disabled: false,
            },
            {
                id: "3986",
                label: "Twitter",
                text: "Twitter",
                "data":"",
                "codename":"Item_3986",
                value: "3986",
                
                disabled: false,
            },
            {
                id: "2",
                label: "电子邮件",
                text: "电子邮件",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "1",
                label: "电话",
                text: "电话",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "3",
                label: "网站/网页",
                text: "网站/网页",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Quote__PaymentTermsCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "2",
                label: "2/10 N30",
                text: "2/10 N30",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "1",
                label: "N30",
                text: "N30",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "3",
                label: "N45",
                text: "N45",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
            {
                id: "4",
                label: "N60",
                text: "N60",
                "data":"",
                "codename":"Item_4",
                value: "4",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Knowledgearticle__StatusCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "4",
                label: "审阅中",
                text: "审阅中",
                "data":"",
                "codename":"Item_4",
                value: "4",
                
                disabled: false,
            },
            {
                id: "7",
                label: "已发布",
                text: "已发布",
                "data":"",
                "codename":"Item_7",
                value: "7",
                
                disabled: false,
            },
            {
                id: "12",
                label: "已存档",
                text: "已存档",
                "data":"",
                "codename":"Item_12",
                value: "12",
                
                disabled: false,
            },
            {
                id: "5",
                label: "已审批",
                text: "已审批",
                "data":"",
                "codename":"Item_5",
                value: "5",
                
                disabled: false,
            },
            {
                id: "14",
                label: "已拒绝",
                text: "已拒绝",
                "data":"",
                "codename":"Item_14",
                value: "14",
                
                disabled: false,
            },
            {
                id: "11",
                label: "已拒绝",
                text: "已拒绝",
                "data":"",
                "codename":"Item_11",
                value: "11",
                
                disabled: false,
            },
            {
                id: "1",
                label: "已拟定",
                text: "已拟定",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "13",
                label: "已放弃",
                text: "已放弃",
                "data":"",
                "codename":"Item_13",
                value: "13",
                
                disabled: false,
            },
            {
                id: "6",
                label: "已计划",
                text: "已计划",
                "data":"",
                "codename":"Item_6",
                value: "6",
                
                disabled: false,
            },
            {
                id: "10",
                label: "已过期",
                text: "已过期",
                "data":"",
                "codename":"Item_10",
                value: "10",
                
                disabled: false,
            },
            {
                id: "9",
                label: "正在更新",
                text: "正在更新",
                "data":"",
                "codename":"Item_9",
                value: "9",
                
                disabled: false,
            },
            {
                id: "2",
                label: "草稿",
                text: "草稿",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "3",
                label: "需要审阅",
                text: "需要审阅",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
            {
                id: "8",
                label: "需要审阅",
                text: "需要审阅",
                "data":"",
                "codename":"Item_8",
                value: "8",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "FQG_ActivityPointer",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "ALL",
                label: "全部活动",
                text: "全部活动",
                "data":{},
                "codename":"All",
                value: "ALL",
                
                disabled: false,
            },
            {
                id: "TASK",
                label: "任务",
                text: "任务",
                "data":{"n_activitytypecode_eq":"TASK"},
                "codename":"Task",
                value: "TASK",
                
                disabled: false,
            },
            {
                id: "EMAIL",
                label: "电子邮件",
                text: "电子邮件",
                "data":{"n_activitytypecode_eq":"EMAIL"},
                "codename":"Email",
                value: "EMAIL",
                
                disabled: false,
            },
            {
                id: "APPOINTMENT",
                label: "约会",
                text: "约会",
                "data":{"n_activitytypecode_eq":"APPOINTMENT"},
                "codename":"Appointment",
                value: "APPOINTMENT",
                
                disabled: false,
            },
            {
                id: "PHONECALL",
                label: "电话联络",
                text: "电话联络",
                "data":{"n_activitytypecode_eq":"PHONECALL"},
                "codename":"Phonecall",
                value: "PHONECALL",
                
                disabled: false,
            },
            {
                id: "OTHER",
                label: "其他",
                text: "其他",
                "data":"",
                "codename":"Other",
                value: "OTHER",
                
                disabled: false,
            },
            {
                id: "LETTER",
                label: "信件",
                text: "信件",
                "data":{"n_activitytypecode_eq":"LETTER"},
                "codename":"Letter",
                value: "LETTER",
                "pvalue": "OTHER",
                disabled: false,
            },
            {
                id: "FAX",
                label: "传真",
                text: "传真",
                "data":{"n_activitytypecode_eq":"FAX"},
                "codename":"Fax",
                value: "FAX",
                "pvalue": "OTHER",
                disabled: false,
            },
            {
                id: "SERVICEAPPOINTMENT",
                label: "服务活动",
                text: "服务活动",
                "data":{"n_activitytypecode_eq":"SERVICEAPPOINTMENT"},
                "codename":"Serviceappointment",
                value: "SERVICEAPPOINTMENT",
                "pvalue": "OTHER",
                disabled: false,
            },
            {
                id: "CAMPAIGNRESPONSE",
                label: "市场活动响应",
                text: "市场活动响应",
                "data":{"n_activitytypecode_eq":"CAMPAIGNRESPONSE"},
                "codename":"Campaignresponse",
                value: "CAMPAIGNRESPONSE",
                "pvalue": "OTHER",
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Contact__LeadSourceCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "1",
                label: "默认值",
                text: "默认值",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Connection__StatusCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "2",
                label: "停用",
                text: "停用",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "1",
                label: "可用",
                text: "可用",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Contact__GenderCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "2",
                label: "女",
                text: "女",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "1",
                label: "男",
                text: "男",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Knowledgearticle__StateCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "3",
                label: "已发布",
                text: "已发布",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
            {
                id: "5",
                label: "已存档",
                text: "已存档",
                "data":"",
                "codename":"Item_5",
                value: "5",
                
                disabled: false,
            },
            {
                id: "1",
                label: "已审批",
                text: "已审批",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "6",
                label: "已放弃",
                text: "已放弃",
                "data":"",
                "codename":"Item_6",
                value: "6",
                
                disabled: false,
            },
            {
                id: "2",
                label: "已计划",
                text: "已计划",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "4",
                label: "已过期",
                text: "已过期",
                "data":"",
                "codename":"Item_4",
                value: "4",
                
                disabled: false,
            },
            {
                id: "0",
                label: "草稿",
                text: "草稿",
                "data":"",
                "codename":"Item_0",
                value: "0",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Metric__AmountDataType",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "1",
                label: "十进制",
                text: "十进制",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "2",
                label: "整数",
                text: "整数",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "0",
                label: "金钱",
                text: "金钱",
                "data":"",
                "codename":"Item_0",
                value: "0",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Incident__PriorityCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "3",
                label: "低",
                text: "低",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
            {
                id: "2",
                label: "标准",
                text: "标准",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "1",
                label: "高",
                text: "高",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Opportunity__StateCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "2",
                label: "丢单",
                text: "丢单",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "0",
                label: "已开始",
                text: "已开始",
                "data":"",
                "codename":"Item_0",
                value: "0",
                
                disabled: false,
            },
            {
                id: "1",
                label: "赢单",
                text: "赢单",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "ShippingMethodCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "1",
                label: "默认值",
                text: "默认值",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Invoice__PaymentTermsCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "2",
                label: "2/10 N30",
                text: "2/10 N30",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "1",
                label: "N30",
                text: "N30",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "3",
                label: "N45",
                text: "N45",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
            {
                id: "4",
                label: "N60",
                text: "N60",
                "data":"",
                "codename":"Item_4",
                value: "4",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Account__PreferredContactMethodCode",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "1",
                label: "任何方式",
                text: "任何方式",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "4",
                label: "传真",
                text: "传真",
                "data":"",
                "codename":"Item_4",
                value: "4",
                
                disabled: false,
            },
            {
                id: "2",
                label: "电子邮件",
                text: "电子邮件",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "3",
                label: "电话",
                text: "电话",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
            {
                id: "5",
                label: "邮件",
                text: "邮件",
                "data":"",
                "codename":"Item_5",
                value: "5",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Opportunity__PurchaseTimeframe",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "2",
                label: "下一季度",
                text: "下一季度",
                "data":"",
                "codename":"Item_2",
                value: "2",
                
                disabled: false,
            },
            {
                id: "3",
                label: "今年",
                text: "今年",
                "data":"",
                "codename":"Item_3",
                value: "3",
                
                disabled: false,
            },
            {
                id: "4",
                label: "未知",
                text: "未知",
                "data":"",
                "codename":"Item_4",
                value: "4",
                
                disabled: false,
            },
            {
                id: "1",
                label: "本季度",
                text: "本季度",
                "data":"",
                "codename":"Item_1",
                value: "1",
                
                disabled: false,
            },
            {
                id: "0",
                label: "立即",
                text: "立即",
                "data":"",
                "codename":"Item_0",
                value: "0",
                
                disabled: false,
            },
        ]
    }
    ]];
});


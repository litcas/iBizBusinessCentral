import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import PriceLevelService from '@/service/price-level/price-level-service';
import PriceLevelAuthService from '@/authservice/price-level/price-level-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import PriceLevelUIService from '@/uiservice/price-level/price-level-ui-service';

/**
 * 价目表数据选择视图视图基类
 *
 * @export
 * @class PriceLevelPickupViewBase
 * @extends {PickupViewBase}
 */
export class PriceLevelPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof PriceLevelPickupViewBase
     */
    protected appDeName: string = 'pricelevel';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof PriceLevelPickupViewBase
     */
    protected appDeKey: string = 'pricelevelid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof PriceLevelPickupViewBase
     */
    protected appDeMajor: string = 'pricelevelname';

    /**
     * 实体服务对象
     *
     * @type {PriceLevelService}
     * @memberof PriceLevelPickupViewBase
     */
    protected appEntityService: PriceLevelService = new PriceLevelService;

    /**
     * 实体权限服务对象
     *
     * @type PriceLevelUIService
     * @memberof PriceLevelPickupViewBase
     */
    public appUIService: PriceLevelUIService = new PriceLevelUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof PriceLevelPickupViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof PriceLevelPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.pricelevel.views.pickupview.caption',
        srfTitle: 'entities.pricelevel.views.pickupview.title',
        srfSubTitle: 'entities.pricelevel.views.pickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof PriceLevelPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'c2915614947c18118d7d66a67ec6a35d';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof PriceLevelPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof PriceLevelPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'pricelevel',
            majorPSDEField: 'pricelevelname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PriceLevelPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PriceLevelPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PriceLevelPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}
import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import ProductService from '@/service/product/product-service';
import ProductAuthService from '@/authservice/product/product-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import ProductUIService from '@/uiservice/product/product-ui-service';

/**
 * 头部信息编辑视图基类
 *
 * @export
 * @class ProductEdit_DataPanelViewBase
 * @extends {EditViewBase}
 */
export class ProductEdit_DataPanelViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof ProductEdit_DataPanelViewBase
     */
    protected appDeName: string = 'product';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof ProductEdit_DataPanelViewBase
     */
    protected appDeKey: string = 'productid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof ProductEdit_DataPanelViewBase
     */
    protected appDeMajor: string = 'productname';

    /**
     * 实体服务对象
     *
     * @type {ProductService}
     * @memberof ProductEdit_DataPanelViewBase
     */
    protected appEntityService: ProductService = new ProductService;

    /**
     * 实体权限服务对象
     *
     * @type ProductUIService
     * @memberof ProductEdit_DataPanelViewBase
     */
    public appUIService: ProductUIService = new ProductUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof ProductEdit_DataPanelViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof ProductEdit_DataPanelViewBase
     */
    protected model: any = {
        srfCaption: 'entities.product.views.edit_datapanelview.caption',
        srfTitle: 'entities.product.views.edit_datapanelview.title',
        srfSubTitle: 'entities.product.views.edit_datapanelview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof ProductEdit_DataPanelViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '5fb6ebc7acda3197226c0dcd9216c7d1';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof ProductEdit_DataPanelViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof ProductEdit_DataPanelViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'product',
            majorPSDEField: 'productname',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ProductEdit_DataPanelViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ProductEdit_DataPanelViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ProductEdit_DataPanelViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}
import { Subject } from 'rxjs';
import { TabExpViewBase } from '@/studio-core';
import ProductService from '@/service/product/product-service';
import ProductAuthService from '@/authservice/product/product-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import ProductUIService from '@/uiservice/product/product-ui-service';

/**
 * 产品信息视图基类
 *
 * @export
 * @class ProductStateTabViewBase
 * @extends {TabExpViewBase}
 */
export class ProductStateTabViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof ProductStateTabViewBase
     */
    protected appDeName: string = 'product';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof ProductStateTabViewBase
     */
    protected appDeKey: string = 'productid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof ProductStateTabViewBase
     */
    protected appDeMajor: string = 'productname';

    /**
     * 实体服务对象
     *
     * @type {ProductService}
     * @memberof ProductStateTabViewBase
     */
    protected appEntityService: ProductService = new ProductService;

    /**
     * 实体权限服务对象
     *
     * @type ProductUIService
     * @memberof ProductStateTabViewBase
     */
    public appUIService: ProductUIService = new ProductUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof ProductStateTabViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof ProductStateTabViewBase
     */
    protected model: any = {
        srfCaption: 'entities.product.views.statetabview.caption',
        srfTitle: 'entities.product.views.statetabview.title',
        srfSubTitle: 'entities.product.views.statetabview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof ProductStateTabViewBase
     */
    protected containerModel: any = {
        view_tabexppanel: { name: 'tabexppanel', type: 'TABEXPPANEL' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '486b2cf6464c79f2410ceec8c278d3bc';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof ProductStateTabViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof ProductStateTabViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'product',
            majorPSDEField: 'productname',
            isLoadDefault: true,
        });
    }


}
import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import PriceLevelService from '@/service/price-level/price-level-service';
import PriceLevelAuthService from '@/authservice/price-level/price-level-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import PriceLevelUIService from '@/uiservice/price-level/price-level-ui-service';

/**
 * 价目表选择表格视图视图基类
 *
 * @export
 * @class PriceLevelPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class PriceLevelPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof PriceLevelPickupGridViewBase
     */
    protected appDeName: string = 'pricelevel';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof PriceLevelPickupGridViewBase
     */
    protected appDeKey: string = 'pricelevelid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof PriceLevelPickupGridViewBase
     */
    protected appDeMajor: string = 'pricelevelname';

    /**
     * 实体服务对象
     *
     * @type {PriceLevelService}
     * @memberof PriceLevelPickupGridViewBase
     */
    protected appEntityService: PriceLevelService = new PriceLevelService;

    /**
     * 实体权限服务对象
     *
     * @type PriceLevelUIService
     * @memberof PriceLevelPickupGridViewBase
     */
    public appUIService: PriceLevelUIService = new PriceLevelUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof PriceLevelPickupGridViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof PriceLevelPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.pricelevel.views.pickupgridview.caption',
        srfTitle: 'entities.pricelevel.views.pickupgridview.title',
        srfSubTitle: 'entities.pricelevel.views.pickupgridview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof PriceLevelPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: { name: 'grid', type: 'GRID' },
        view_searchform: { name: 'searchform', type: 'SEARCHFORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '7d1ebe0e88a3c5564455651bd2ed8703';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof PriceLevelPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof PriceLevelPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'pricelevel',
            majorPSDEField: 'pricelevelname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PriceLevelPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PriceLevelPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PriceLevelPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PriceLevelPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PriceLevelPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PriceLevelPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PriceLevelPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof PriceLevelPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}
import { Subject } from 'rxjs';
import { PickupGridViewBase } from '@/studio-core';
import IncidentCustomerService from '@/service/incident-customer/incident-customer-service';
import IncidentCustomerAuthService from '@/authservice/incident-customer/incident-customer-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import IncidentCustomerUIService from '@/uiservice/incident-customer/incident-customer-ui-service';

/**
 * 案例客户选择表格视图视图基类
 *
 * @export
 * @class IncidentCustomerPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class IncidentCustomerPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof IncidentCustomerPickupGridViewBase
     */
    protected appDeName: string = 'incidentcustomer';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof IncidentCustomerPickupGridViewBase
     */
    protected appDeKey: string = 'customerid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof IncidentCustomerPickupGridViewBase
     */
    protected appDeMajor: string = 'customername';

    /**
     * 实体服务对象
     *
     * @type {IncidentCustomerService}
     * @memberof IncidentCustomerPickupGridViewBase
     */
    protected appEntityService: IncidentCustomerService = new IncidentCustomerService;

    /**
     * 实体权限服务对象
     *
     * @type IncidentCustomerUIService
     * @memberof IncidentCustomerPickupGridViewBase
     */
    public appUIService: IncidentCustomerUIService = new IncidentCustomerUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof IncidentCustomerPickupGridViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof IncidentCustomerPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.incidentcustomer.views.pickupgridview.caption',
        srfTitle: 'entities.incidentcustomer.views.pickupgridview.title',
        srfSubTitle: 'entities.incidentcustomer.views.pickupgridview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof IncidentCustomerPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: { name: 'grid', type: 'GRID' },
        view_searchform: { name: 'searchform', type: 'SEARCHFORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '193cc211e5bf7fa2d238bafc7a4fbe93';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof IncidentCustomerPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof IncidentCustomerPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'incidentcustomer',
            majorPSDEField: 'customername',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IncidentCustomerPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IncidentCustomerPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IncidentCustomerPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IncidentCustomerPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IncidentCustomerPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IncidentCustomerPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IncidentCustomerPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof IncidentCustomerPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}
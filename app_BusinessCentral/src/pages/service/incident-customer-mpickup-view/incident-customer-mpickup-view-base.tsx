import { Subject } from 'rxjs';
import { MPickupViewBase } from '@/studio-core';
import IncidentCustomerService from '@/service/incident-customer/incident-customer-service';
import IncidentCustomerAuthService from '@/authservice/incident-customer/incident-customer-auth-service';
import MPickupViewEngine from '@engine/view/mpickup-view-engine';
import IncidentCustomerUIService from '@/uiservice/incident-customer/incident-customer-ui-service';

/**
 * 案例客户数据多项选择视图视图基类
 *
 * @export
 * @class IncidentCustomerMPickupViewBase
 * @extends {MPickupViewBase}
 */
export class IncidentCustomerMPickupViewBase extends MPickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof IncidentCustomerMPickupViewBase
     */
    protected appDeName: string = 'incidentcustomer';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof IncidentCustomerMPickupViewBase
     */
    protected appDeKey: string = 'customerid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof IncidentCustomerMPickupViewBase
     */
    protected appDeMajor: string = 'customername';

    /**
     * 实体服务对象
     *
     * @type {IncidentCustomerService}
     * @memberof IncidentCustomerMPickupViewBase
     */
    protected appEntityService: IncidentCustomerService = new IncidentCustomerService;

    /**
     * 实体权限服务对象
     *
     * @type IncidentCustomerUIService
     * @memberof IncidentCustomerMPickupViewBase
     */
    public appUIService: IncidentCustomerUIService = new IncidentCustomerUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof IncidentCustomerMPickupViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof IncidentCustomerMPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.incidentcustomer.views.mpickupview.caption',
        srfTitle: 'entities.incidentcustomer.views.mpickupview.title',
        srfSubTitle: 'entities.incidentcustomer.views.mpickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof IncidentCustomerMPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'da70cb6f06104242301260e8ac00d713';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof IncidentCustomerMPickupViewBase
     */
    public engine: MPickupViewEngine = new MPickupViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof IncidentCustomerMPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'incidentcustomer',
            majorPSDEField: 'customername',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IncidentCustomerMPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IncidentCustomerMPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IncidentCustomerMPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }



    /**
     * 添加左侧面板所有数据到右侧
     *
     * @memberof IncidentCustomerMPickupView
     */
    public onCLickAllRight(): void {
        Object.values(this.containerModel).forEach((model: any) => {
            if (!Object.is(model.type, 'PICKUPVIEWPANEL')) {
                return;
            }
            if (model.datas.length > 0) {
                model.datas.forEach((data: any, index: any) => {
                    Object.assign(data, { srfmajortext: data['customername'] });
                })
            }
            model.datas.forEach((item: any) => {
                const index: number = this.viewSelections.findIndex((selection: any) => Object.is(item.srfkey, selection.srfkey));
                if (index === -1) {
                    item._select = false;
                    this.viewSelections.push(item);
                }
            });
        });
        this.selectedData = JSON.stringify(this.viewSelections);
    }


}
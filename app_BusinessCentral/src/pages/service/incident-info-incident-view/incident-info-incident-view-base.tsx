import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import IncidentService from '@/service/incident/incident-service';
import IncidentAuthService from '@/authservice/incident/incident-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import IncidentUIService from '@/uiservice/incident/incident-ui-service';

/**
 * 案例信息视图基类
 *
 * @export
 * @class IncidentInfo_IncidentViewBase
 * @extends {EditViewBase}
 */
export class IncidentInfo_IncidentViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof IncidentInfo_IncidentViewBase
     */
    protected appDeName: string = 'incident';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof IncidentInfo_IncidentViewBase
     */
    protected appDeKey: string = 'incidentid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof IncidentInfo_IncidentViewBase
     */
    protected appDeMajor: string = 'title';

    /**
     * 实体服务对象
     *
     * @type {IncidentService}
     * @memberof IncidentInfo_IncidentViewBase
     */
    protected appEntityService: IncidentService = new IncidentService;

    /**
     * 实体权限服务对象
     *
     * @type IncidentUIService
     * @memberof IncidentInfo_IncidentViewBase
     */
    public appUIService: IncidentUIService = new IncidentUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof IncidentInfo_IncidentViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof IncidentInfo_IncidentViewBase
     */
    protected model: any = {
        srfCaption: 'entities.incident.views.info_incidentview.caption',
        srfTitle: 'entities.incident.views.info_incidentview.title',
        srfSubTitle: 'entities.incident.views.info_incidentview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof IncidentInfo_IncidentViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '59afbe7ce81dcef92047e59031769a52';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof IncidentInfo_IncidentViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof IncidentInfo_IncidentViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'incident',
            majorPSDEField: 'title',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IncidentInfo_IncidentViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IncidentInfo_IncidentViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IncidentInfo_IncidentViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}
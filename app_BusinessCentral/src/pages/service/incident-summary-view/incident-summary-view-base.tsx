import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { DashboardViewBase } from '@/studio-core';
import IncidentService from '@/service/incident/incident-service';
import IncidentAuthService from '@/authservice/incident/incident-auth-service';
import PortalViewEngine from '@engine/view/portal-view-engine';
import IncidentUIService from '@/uiservice/incident/incident-ui-service';

/**
 * 案例数据看板视图视图基类
 *
 * @export
 * @class IncidentSummaryViewBase
 * @extends {DashboardViewBase}
 */
export class IncidentSummaryViewBase extends DashboardViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof IncidentSummaryViewBase
     */
    protected appDeName: string = 'incident';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof IncidentSummaryViewBase
     */
    protected appDeKey: string = 'incidentid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof IncidentSummaryViewBase
     */
    protected appDeMajor: string = 'title';

    /**
     * 实体服务对象
     *
     * @type {IncidentService}
     * @memberof IncidentSummaryViewBase
     */
    protected appEntityService: IncidentService = new IncidentService;

    /**
     * 实体权限服务对象
     *
     * @type IncidentUIService
     * @memberof IncidentSummaryViewBase
     */
    public appUIService: IncidentUIService = new IncidentUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof IncidentSummaryViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof IncidentSummaryViewBase
     */
    protected model: any = {
        srfCaption: 'entities.incident.views.summaryview.caption',
        srfTitle: 'entities.incident.views.summaryview.title',
        srfSubTitle: 'entities.incident.views.summaryview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof IncidentSummaryViewBase
     */
    protected containerModel: any = {
        view_dashboard: { name: 'dashboard', type: 'DASHBOARD' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '9a21ee1d10cdcd6c1b3823fd8fbcf669';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof IncidentSummaryViewBase
     */
    public engine: PortalViewEngine = new PortalViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof IncidentSummaryViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            dashboard: this.$refs.dashboard,
            keyPSDEField: 'incident',
            majorPSDEField: 'title',
            isLoadDefault: true,
        });
    }

    /**
     * dashboard 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IncidentSummaryViewBase
     */
    public dashboard_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('dashboard', 'load', $event);
    }


}
import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import IncidentService from '@/service/incident/incident-service';
import IncidentAuthService from '@/authservice/incident/incident-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import IncidentUIService from '@/uiservice/incident/incident-ui-service';

/**
 * 头部信息编辑视图基类
 *
 * @export
 * @class IncidentEdit_DataPanelViewBase
 * @extends {EditViewBase}
 */
export class IncidentEdit_DataPanelViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof IncidentEdit_DataPanelViewBase
     */
    protected appDeName: string = 'incident';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof IncidentEdit_DataPanelViewBase
     */
    protected appDeKey: string = 'incidentid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof IncidentEdit_DataPanelViewBase
     */
    protected appDeMajor: string = 'title';

    /**
     * 实体服务对象
     *
     * @type {IncidentService}
     * @memberof IncidentEdit_DataPanelViewBase
     */
    protected appEntityService: IncidentService = new IncidentService;

    /**
     * 实体权限服务对象
     *
     * @type IncidentUIService
     * @memberof IncidentEdit_DataPanelViewBase
     */
    public appUIService: IncidentUIService = new IncidentUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof IncidentEdit_DataPanelViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof IncidentEdit_DataPanelViewBase
     */
    protected model: any = {
        srfCaption: 'entities.incident.views.edit_datapanelview.caption',
        srfTitle: 'entities.incident.views.edit_datapanelview.title',
        srfSubTitle: 'entities.incident.views.edit_datapanelview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof IncidentEdit_DataPanelViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '7b74f6733aeff35c8bc965af197ea90d';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof IncidentEdit_DataPanelViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof IncidentEdit_DataPanelViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'incident',
            majorPSDEField: 'title',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IncidentEdit_DataPanelViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IncidentEdit_DataPanelViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IncidentEdit_DataPanelViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}
import { Subject } from 'rxjs';
import { PickupViewBase } from '@/studio-core';
import IncidentCustomerService from '@/service/incident-customer/incident-customer-service';
import IncidentCustomerAuthService from '@/authservice/incident-customer/incident-customer-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import IncidentCustomerUIService from '@/uiservice/incident-customer/incident-customer-ui-service';

/**
 * 客户视图基类
 *
 * @export
 * @class IncidentCustomerPickupViewBase
 * @extends {PickupViewBase}
 */
export class IncidentCustomerPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof IncidentCustomerPickupViewBase
     */
    protected appDeName: string = 'incidentcustomer';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof IncidentCustomerPickupViewBase
     */
    protected appDeKey: string = 'customerid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof IncidentCustomerPickupViewBase
     */
    protected appDeMajor: string = 'customername';

    /**
     * 实体服务对象
     *
     * @type {IncidentCustomerService}
     * @memberof IncidentCustomerPickupViewBase
     */
    protected appEntityService: IncidentCustomerService = new IncidentCustomerService;

    /**
     * 实体权限服务对象
     *
     * @type IncidentCustomerUIService
     * @memberof IncidentCustomerPickupViewBase
     */
    public appUIService: IncidentCustomerUIService = new IncidentCustomerUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof IncidentCustomerPickupViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof IncidentCustomerPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.incidentcustomer.views.pickupview.caption',
        srfTitle: 'entities.incidentcustomer.views.pickupview.title',
        srfSubTitle: 'entities.incidentcustomer.views.pickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof IncidentCustomerPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '78831ef897ed4ce7d38788ded1b2c536';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof IncidentCustomerPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof IncidentCustomerPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'incidentcustomer',
            majorPSDEField: 'customername',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IncidentCustomerPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IncidentCustomerPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IncidentCustomerPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}
import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import IBZEmployeeService from '@/service/ibzemployee/ibzemployee-service';
import IBZEmployeeAuthService from '@/authservice/ibzemployee/ibzemployee-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import IBZEmployeeUIService from '@/uiservice/ibzemployee/ibzemployee-ui-service';

/**
 * 人员数据选择视图视图基类
 *
 * @export
 * @class IBZEmployeePickupViewBase
 * @extends {PickupViewBase}
 */
export class IBZEmployeePickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof IBZEmployeePickupViewBase
     */
    protected appDeName: string = 'ibzemployee';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof IBZEmployeePickupViewBase
     */
    protected appDeKey: string = 'userid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof IBZEmployeePickupViewBase
     */
    protected appDeMajor: string = 'personname';

    /**
     * 实体服务对象
     *
     * @type {IBZEmployeeService}
     * @memberof IBZEmployeePickupViewBase
     */
    protected appEntityService: IBZEmployeeService = new IBZEmployeeService;

    /**
     * 实体权限服务对象
     *
     * @type IBZEmployeeUIService
     * @memberof IBZEmployeePickupViewBase
     */
    public appUIService: IBZEmployeeUIService = new IBZEmployeeUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof IBZEmployeePickupViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof IBZEmployeePickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.ibzemployee.views.pickupview.caption',
        srfTitle: 'entities.ibzemployee.views.pickupview.title',
        srfSubTitle: 'entities.ibzemployee.views.pickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof IBZEmployeePickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '067b7105c6d7b908acbb7c7738c7cbb9';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof IBZEmployeePickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof IBZEmployeePickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'ibzemployee',
            majorPSDEField: 'personname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZEmployeePickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZEmployeePickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZEmployeePickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}
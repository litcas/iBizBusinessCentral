import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import IBZDepartmentService from '@/service/ibzdepartment/ibzdepartment-service';
import IBZDepartmentAuthService from '@/authservice/ibzdepartment/ibzdepartment-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import IBZDepartmentUIService from '@/uiservice/ibzdepartment/ibzdepartment-ui-service';

/**
 * 部门选择视图视图基类
 *
 * @export
 * @class IBZDepartmentPickupViewBase
 * @extends {PickupViewBase}
 */
export class IBZDepartmentPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof IBZDepartmentPickupViewBase
     */
    protected appDeName: string = 'ibzdepartment';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof IBZDepartmentPickupViewBase
     */
    protected appDeKey: string = 'deptid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof IBZDepartmentPickupViewBase
     */
    protected appDeMajor: string = 'deptname';

    /**
     * 实体服务对象
     *
     * @type {IBZDepartmentService}
     * @memberof IBZDepartmentPickupViewBase
     */
    protected appEntityService: IBZDepartmentService = new IBZDepartmentService;

    /**
     * 实体权限服务对象
     *
     * @type IBZDepartmentUIService
     * @memberof IBZDepartmentPickupViewBase
     */
    public appUIService: IBZDepartmentUIService = new IBZDepartmentUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof IBZDepartmentPickupViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof IBZDepartmentPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.ibzdepartment.views.pickupview.caption',
        srfTitle: 'entities.ibzdepartment.views.pickupview.title',
        srfSubTitle: 'entities.ibzdepartment.views.pickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof IBZDepartmentPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '59e18200dd83f3db1fb0ab1080594212';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof IBZDepartmentPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof IBZDepartmentPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'ibzdepartment',
            majorPSDEField: 'deptname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZDepartmentPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZDepartmentPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZDepartmentPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}
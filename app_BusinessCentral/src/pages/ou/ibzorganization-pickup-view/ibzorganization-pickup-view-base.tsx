import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import IBZOrganizationService from '@/service/ibzorganization/ibzorganization-service';
import IBZOrganizationAuthService from '@/authservice/ibzorganization/ibzorganization-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import IBZOrganizationUIService from '@/uiservice/ibzorganization/ibzorganization-ui-service';

/**
 * 单位机构数据选择视图视图基类
 *
 * @export
 * @class IBZOrganizationPickupViewBase
 * @extends {PickupViewBase}
 */
export class IBZOrganizationPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof IBZOrganizationPickupViewBase
     */
    protected appDeName: string = 'ibzorganization';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof IBZOrganizationPickupViewBase
     */
    protected appDeKey: string = 'orgid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof IBZOrganizationPickupViewBase
     */
    protected appDeMajor: string = 'orgname';

    /**
     * 实体服务对象
     *
     * @type {IBZOrganizationService}
     * @memberof IBZOrganizationPickupViewBase
     */
    protected appEntityService: IBZOrganizationService = new IBZOrganizationService;

    /**
     * 实体权限服务对象
     *
     * @type IBZOrganizationUIService
     * @memberof IBZOrganizationPickupViewBase
     */
    public appUIService: IBZOrganizationUIService = new IBZOrganizationUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof IBZOrganizationPickupViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof IBZOrganizationPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.ibzorganization.views.pickupview.caption',
        srfTitle: 'entities.ibzorganization.views.pickupview.title',
        srfSubTitle: 'entities.ibzorganization.views.pickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof IBZOrganizationPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '2644137e77038f3666fcea7258e6953d';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof IBZOrganizationPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof IBZOrganizationPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'ibzorganization',
            majorPSDEField: 'orgname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZOrganizationPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZOrganizationPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZOrganizationPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}
import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import IBZPostService from '@/service/ibzpost/ibzpost-service';
import IBZPostAuthService from '@/authservice/ibzpost/ibzpost-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import IBZPostUIService from '@/uiservice/ibzpost/ibzpost-ui-service';

/**
 * 岗位选择表格视图视图基类
 *
 * @export
 * @class IBZPostPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class IBZPostPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof IBZPostPickupGridViewBase
     */
    protected appDeName: string = 'ibzpost';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof IBZPostPickupGridViewBase
     */
    protected appDeKey: string = 'postid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof IBZPostPickupGridViewBase
     */
    protected appDeMajor: string = 'postname';

    /**
     * 实体服务对象
     *
     * @type {IBZPostService}
     * @memberof IBZPostPickupGridViewBase
     */
    protected appEntityService: IBZPostService = new IBZPostService;

    /**
     * 实体权限服务对象
     *
     * @type IBZPostUIService
     * @memberof IBZPostPickupGridViewBase
     */
    public appUIService: IBZPostUIService = new IBZPostUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof IBZPostPickupGridViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof IBZPostPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.ibzpost.views.pickupgridview.caption',
        srfTitle: 'entities.ibzpost.views.pickupgridview.title',
        srfSubTitle: 'entities.ibzpost.views.pickupgridview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof IBZPostPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: { name: 'grid', type: 'GRID' },
        view_searchform: { name: 'searchform', type: 'SEARCHFORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'd099f56cc52d60591db78b1ed53f4305';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof IBZPostPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof IBZPostPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'ibzpost',
            majorPSDEField: 'postname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZPostPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZPostPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZPostPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZPostPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZPostPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZPostPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZPostPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof IBZPostPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}
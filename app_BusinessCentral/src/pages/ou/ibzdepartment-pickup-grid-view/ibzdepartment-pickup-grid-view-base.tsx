import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import IBZDepartmentService from '@/service/ibzdepartment/ibzdepartment-service';
import IBZDepartmentAuthService from '@/authservice/ibzdepartment/ibzdepartment-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import IBZDepartmentUIService from '@/uiservice/ibzdepartment/ibzdepartment-ui-service';

/**
 * 部门选择表格视图视图基类
 *
 * @export
 * @class IBZDepartmentPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class IBZDepartmentPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof IBZDepartmentPickupGridViewBase
     */
    protected appDeName: string = 'ibzdepartment';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof IBZDepartmentPickupGridViewBase
     */
    protected appDeKey: string = 'deptid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof IBZDepartmentPickupGridViewBase
     */
    protected appDeMajor: string = 'deptname';

    /**
     * 实体服务对象
     *
     * @type {IBZDepartmentService}
     * @memberof IBZDepartmentPickupGridViewBase
     */
    protected appEntityService: IBZDepartmentService = new IBZDepartmentService;

    /**
     * 实体权限服务对象
     *
     * @type IBZDepartmentUIService
     * @memberof IBZDepartmentPickupGridViewBase
     */
    public appUIService: IBZDepartmentUIService = new IBZDepartmentUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof IBZDepartmentPickupGridViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof IBZDepartmentPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.ibzdepartment.views.pickupgridview.caption',
        srfTitle: 'entities.ibzdepartment.views.pickupgridview.title',
        srfSubTitle: 'entities.ibzdepartment.views.pickupgridview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof IBZDepartmentPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: { name: 'grid', type: 'GRID' },
        view_searchform: { name: 'searchform', type: 'SEARCHFORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '5def5b47040f10f4e797df178afb972e';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof IBZDepartmentPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof IBZDepartmentPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'ibzdepartment',
            majorPSDEField: 'deptname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZDepartmentPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZDepartmentPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZDepartmentPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZDepartmentPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZDepartmentPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZDepartmentPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZDepartmentPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof IBZDepartmentPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}
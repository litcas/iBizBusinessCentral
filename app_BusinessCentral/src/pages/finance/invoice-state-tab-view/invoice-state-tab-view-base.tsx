import { Subject } from 'rxjs';
import { TabExpViewBase } from '@/studio-core';
import InvoiceService from '@/service/invoice/invoice-service';
import InvoiceAuthService from '@/authservice/invoice/invoice-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import InvoiceUIService from '@/uiservice/invoice/invoice-ui-service';

/**
 * 发票信息视图基类
 *
 * @export
 * @class InvoiceStateTabViewBase
 * @extends {TabExpViewBase}
 */
export class InvoiceStateTabViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof InvoiceStateTabViewBase
     */
    protected appDeName: string = 'invoice';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof InvoiceStateTabViewBase
     */
    protected appDeKey: string = 'invoiceid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof InvoiceStateTabViewBase
     */
    protected appDeMajor: string = 'invoicename';

    /**
     * 实体服务对象
     *
     * @type {InvoiceService}
     * @memberof InvoiceStateTabViewBase
     */
    protected appEntityService: InvoiceService = new InvoiceService;

    /**
     * 实体权限服务对象
     *
     * @type InvoiceUIService
     * @memberof InvoiceStateTabViewBase
     */
    public appUIService: InvoiceUIService = new InvoiceUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof InvoiceStateTabViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof InvoiceStateTabViewBase
     */
    protected model: any = {
        srfCaption: 'entities.invoice.views.statetabview.caption',
        srfTitle: 'entities.invoice.views.statetabview.title',
        srfSubTitle: 'entities.invoice.views.statetabview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof InvoiceStateTabViewBase
     */
    protected containerModel: any = {
        view_tabexppanel: { name: 'tabexppanel', type: 'TABEXPPANEL' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '73324c8d27828ead87dcca4fdbc8fb19';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof InvoiceStateTabViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof InvoiceStateTabViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'invoice',
            majorPSDEField: 'invoicename',
            isLoadDefault: true,
        });
    }


}
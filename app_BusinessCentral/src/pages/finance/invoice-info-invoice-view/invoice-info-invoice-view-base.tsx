import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import InvoiceService from '@/service/invoice/invoice-service';
import InvoiceAuthService from '@/authservice/invoice/invoice-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import InvoiceUIService from '@/uiservice/invoice/invoice-ui-service';

/**
 * 发票编辑视图视图基类
 *
 * @export
 * @class InvoiceInfo_InvoiceViewBase
 * @extends {EditViewBase}
 */
export class InvoiceInfo_InvoiceViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof InvoiceInfo_InvoiceViewBase
     */
    protected appDeName: string = 'invoice';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof InvoiceInfo_InvoiceViewBase
     */
    protected appDeKey: string = 'invoiceid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof InvoiceInfo_InvoiceViewBase
     */
    protected appDeMajor: string = 'invoicename';

    /**
     * 实体服务对象
     *
     * @type {InvoiceService}
     * @memberof InvoiceInfo_InvoiceViewBase
     */
    protected appEntityService: InvoiceService = new InvoiceService;

    /**
     * 实体权限服务对象
     *
     * @type InvoiceUIService
     * @memberof InvoiceInfo_InvoiceViewBase
     */
    public appUIService: InvoiceUIService = new InvoiceUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof InvoiceInfo_InvoiceViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof InvoiceInfo_InvoiceViewBase
     */
    protected model: any = {
        srfCaption: 'entities.invoice.views.info_invoiceview.caption',
        srfTitle: 'entities.invoice.views.info_invoiceview.title',
        srfSubTitle: 'entities.invoice.views.info_invoiceview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof InvoiceInfo_InvoiceViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'a9c176524018ca1aac5e64ef5a112255';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof InvoiceInfo_InvoiceViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof InvoiceInfo_InvoiceViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'invoice',
            majorPSDEField: 'invoicename',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof InvoiceInfo_InvoiceViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof InvoiceInfo_InvoiceViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof InvoiceInfo_InvoiceViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}
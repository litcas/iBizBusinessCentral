import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import DictCatalogService from '@/service/dict-catalog/dict-catalog-service';
import DictCatalogAuthService from '@/authservice/dict-catalog/dict-catalog-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import DictCatalogUIService from '@/uiservice/dict-catalog/dict-catalog-ui-service';

/**
 * 字典数据选择视图视图基类
 *
 * @export
 * @class DictCatalogPickupViewBase
 * @extends {PickupViewBase}
 */
export class DictCatalogPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof DictCatalogPickupViewBase
     */
    protected appDeName: string = 'dictcatalog';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof DictCatalogPickupViewBase
     */
    protected appDeKey: string = 'cid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof DictCatalogPickupViewBase
     */
    protected appDeMajor: string = 'cname';

    /**
     * 实体服务对象
     *
     * @type {DictCatalogService}
     * @memberof DictCatalogPickupViewBase
     */
    protected appEntityService: DictCatalogService = new DictCatalogService;

    /**
     * 实体权限服务对象
     *
     * @type DictCatalogUIService
     * @memberof DictCatalogPickupViewBase
     */
    public appUIService: DictCatalogUIService = new DictCatalogUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof DictCatalogPickupViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof DictCatalogPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.dictcatalog.views.pickupview.caption',
        srfTitle: 'entities.dictcatalog.views.pickupview.title',
        srfSubTitle: 'entities.dictcatalog.views.pickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof DictCatalogPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'bd6f149583c60a4f5f5f671334a71959';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof DictCatalogPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof DictCatalogPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'dictcatalog',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof DictCatalogPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof DictCatalogPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof DictCatalogPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}
import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import SysUserService from '@/service/sys-user/sys-user-service';
import SysUserAuthService from '@/authservice/sys-user/sys-user-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import SysUserUIService from '@/uiservice/sys-user/sys-user-ui-service';

/**
 * 用户表数据选择视图视图基类
 *
 * @export
 * @class SysUserPickupViewBase
 * @extends {PickupViewBase}
 */
export class SysUserPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof SysUserPickupViewBase
     */
    protected appDeName: string = 'sysuser';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof SysUserPickupViewBase
     */
    protected appDeKey: string = 'userid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof SysUserPickupViewBase
     */
    protected appDeMajor: string = 'personname';

    /**
     * 实体服务对象
     *
     * @type {SysUserService}
     * @memberof SysUserPickupViewBase
     */
    protected appEntityService: SysUserService = new SysUserService;

    /**
     * 实体权限服务对象
     *
     * @type SysUserUIService
     * @memberof SysUserPickupViewBase
     */
    public appUIService: SysUserUIService = new SysUserUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof SysUserPickupViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof SysUserPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.sysuser.views.pickupview.caption',
        srfTitle: 'entities.sysuser.views.pickupview.title',
        srfSubTitle: 'entities.sysuser.views.pickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof SysUserPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '753b710282b0332c2d6418a1d6c7594e';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof SysUserPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof SysUserPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'sysuser',
            majorPSDEField: 'personname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SysUserPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SysUserPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SysUserPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}
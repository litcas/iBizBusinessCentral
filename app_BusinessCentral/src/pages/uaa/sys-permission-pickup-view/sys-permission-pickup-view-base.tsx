import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import SysPermissionService from '@/service/sys-permission/sys-permission-service';
import SysPermissionAuthService from '@/authservice/sys-permission/sys-permission-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import SysPermissionUIService from '@/uiservice/sys-permission/sys-permission-ui-service';

/**
 * 权限表数据选择视图视图基类
 *
 * @export
 * @class SysPermissionPickupViewBase
 * @extends {PickupViewBase}
 */
export class SysPermissionPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof SysPermissionPickupViewBase
     */
    protected appDeName: string = 'syspermission';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof SysPermissionPickupViewBase
     */
    protected appDeKey: string = 'sys_permissionid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof SysPermissionPickupViewBase
     */
    protected appDeMajor: string = 'sys_permissionname';

    /**
     * 实体服务对象
     *
     * @type {SysPermissionService}
     * @memberof SysPermissionPickupViewBase
     */
    protected appEntityService: SysPermissionService = new SysPermissionService;

    /**
     * 实体权限服务对象
     *
     * @type SysPermissionUIService
     * @memberof SysPermissionPickupViewBase
     */
    public appUIService: SysPermissionUIService = new SysPermissionUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof SysPermissionPickupViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof SysPermissionPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.syspermission.views.pickupview.caption',
        srfTitle: 'entities.syspermission.views.pickupview.title',
        srfSubTitle: 'entities.syspermission.views.pickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof SysPermissionPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'd9006107ae68d070380f333abf2c9ea9';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof SysPermissionPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof SysPermissionPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'syspermission',
            majorPSDEField: 'permissionname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SysPermissionPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SysPermissionPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SysPermissionPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}
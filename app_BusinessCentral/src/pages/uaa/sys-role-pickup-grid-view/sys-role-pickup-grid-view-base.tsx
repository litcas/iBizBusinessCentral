import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import SysRoleService from '@/service/sys-role/sys-role-service';
import SysRoleAuthService from '@/authservice/sys-role/sys-role-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import SysRoleUIService from '@/uiservice/sys-role/sys-role-ui-service';

/**
 * 角色选择表格视图视图基类
 *
 * @export
 * @class SysRolePickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class SysRolePickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof SysRolePickupGridViewBase
     */
    protected appDeName: string = 'sysrole';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof SysRolePickupGridViewBase
     */
    protected appDeKey: string = 'sys_roleid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof SysRolePickupGridViewBase
     */
    protected appDeMajor: string = 'sys_rolename';

    /**
     * 实体服务对象
     *
     * @type {SysRoleService}
     * @memberof SysRolePickupGridViewBase
     */
    protected appEntityService: SysRoleService = new SysRoleService;

    /**
     * 实体权限服务对象
     *
     * @type SysRoleUIService
     * @memberof SysRolePickupGridViewBase
     */
    public appUIService: SysRoleUIService = new SysRoleUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof SysRolePickupGridViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof SysRolePickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.sysrole.views.pickupgridview.caption',
        srfTitle: 'entities.sysrole.views.pickupgridview.title',
        srfSubTitle: 'entities.sysrole.views.pickupgridview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof SysRolePickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: { name: 'grid', type: 'GRID' },
        view_searchform: { name: 'searchform', type: 'SEARCHFORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'f92fa90f9731add4b4ab1d5e7d15bfb6';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof SysRolePickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof SysRolePickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'sysrole',
            majorPSDEField: 'rolename',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SysRolePickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SysRolePickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SysRolePickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SysRolePickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SysRolePickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SysRolePickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SysRolePickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof SysRolePickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}
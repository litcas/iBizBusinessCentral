import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { MPickupViewBase } from '@/studio-core';
import SysPermissionService from '@/service/sys-permission/sys-permission-service';
import SysPermissionAuthService from '@/authservice/sys-permission/sys-permission-auth-service';
import MPickupViewEngine from '@engine/view/mpickup-view-engine';
import SysPermissionUIService from '@/uiservice/sys-permission/sys-permission-ui-service';

/**
 * 权限表数据多项选择视图视图基类
 *
 * @export
 * @class SysPermissionMPickupViewBase
 * @extends {MPickupViewBase}
 */
export class SysPermissionMPickupViewBase extends MPickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof SysPermissionMPickupViewBase
     */
    protected appDeName: string = 'syspermission';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof SysPermissionMPickupViewBase
     */
    protected appDeKey: string = 'sys_permissionid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof SysPermissionMPickupViewBase
     */
    protected appDeMajor: string = 'sys_permissionname';

    /**
     * 实体服务对象
     *
     * @type {SysPermissionService}
     * @memberof SysPermissionMPickupViewBase
     */
    protected appEntityService: SysPermissionService = new SysPermissionService;

    /**
     * 实体权限服务对象
     *
     * @type SysPermissionUIService
     * @memberof SysPermissionMPickupViewBase
     */
    public appUIService: SysPermissionUIService = new SysPermissionUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof SysPermissionMPickupViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof SysPermissionMPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.syspermission.views.mpickupview.caption',
        srfTitle: 'entities.syspermission.views.mpickupview.title',
        srfSubTitle: 'entities.syspermission.views.mpickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof SysPermissionMPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'a35e5430216e8f5524548b2d79cb4c07';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof SysPermissionMPickupViewBase
     */
    public engine: MPickupViewEngine = new MPickupViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof SysPermissionMPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'syspermission',
            majorPSDEField: 'permissionname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SysPermissionMPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SysPermissionMPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SysPermissionMPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }



    /**
     * 添加左侧面板所有数据到右侧
     *
     * @memberof SysPermissionMPickupView
     */
    public onCLickAllRight(): void {
        Object.values(this.containerModel).forEach((model: any) => {
            if (!Object.is(model.type, 'PICKUPVIEWPANEL')) {
                return;
            }
            if (model.datas.length > 0) {
                model.datas.forEach((data: any, index: any) => {
                    Object.assign(data, { srfmajortext: data['permissionname'] });
                })
            }
            model.datas.forEach((item: any) => {
                const index: number = this.viewSelections.findIndex((selection: any) => Object.is(item.srfkey, selection.srfkey));
                if (index === -1) {
                    item._select = false;
                    this.viewSelections.push(item);
                }
            });
        });
        this.selectedData = JSON.stringify(this.viewSelections);
    }


}
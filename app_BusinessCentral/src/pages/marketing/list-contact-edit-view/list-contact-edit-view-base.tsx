import { Subject } from 'rxjs';
import { EditViewBase } from '@/studio-core';
import ListContactService from '@/service/list-contact/list-contact-service';
import ListContactAuthService from '@/authservice/list-contact/list-contact-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import ListContactUIService from '@/uiservice/list-contact/list-contact-ui-service';

/**
 * 营销列表-联系人编辑视图视图基类
 *
 * @export
 * @class ListContactEditViewBase
 * @extends {EditViewBase}
 */
export class ListContactEditViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof ListContactEditViewBase
     */
    protected appDeName: string = 'listcontact';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof ListContactEditViewBase
     */
    protected appDeKey: string = 'relationshipsid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof ListContactEditViewBase
     */
    protected appDeMajor: string = 'relationshipsname';

    /**
     * 实体服务对象
     *
     * @type {ListContactService}
     * @memberof ListContactEditViewBase
     */
    protected appEntityService: ListContactService = new ListContactService;

    /**
     * 实体权限服务对象
     *
     * @type ListContactUIService
     * @memberof ListContactEditViewBase
     */
    public appUIService: ListContactUIService = new ListContactUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof ListContactEditViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof ListContactEditViewBase
     */
    protected model: any = {
        srfCaption: 'entities.listcontact.views.editview.caption',
        srfTitle: 'entities.listcontact.views.editview.title',
        srfSubTitle: 'entities.listcontact.views.editview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof ListContactEditViewBase
     */
    protected containerModel: any = {
        view_toolbar: { name: 'toolbar', type: 'TOOLBAR' },
        view_form: { name: 'form', type: 'FORM' },
    };

    /**
     * 工具栏模型
     *
     * @type {*}
     * @memberof ListContactEditView
     */
    public toolBarModels: any = {
        tbitem12: { name: 'tbitem12', caption: '关闭', 'isShowCaption': true, 'isShowIcon': true, tooltip: '关闭', iconcls: 'fa fa-sign-out', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Exit', target: '', class: '' } },

    };



	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'cd36a4d5e19fd5523b22ee0568ed7bd8';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof ListContactEditViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof ListContactEditViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'listcontact',
            majorPSDEField: 'relationshipsname',
            isLoadDefault: true,
        });
    }

    /**
     * toolbar 部件 click 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ListContactEditViewBase
     */
    public toolbar_click($event: any, $event2?: any): void {
        if (Object.is($event.tag, 'tbitem12')) {
            this.toolbar_tbitem12_click(null, '', $event2);
        }
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ListContactEditViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ListContactEditViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ListContactEditViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem12_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this.$refs.form;
        if (xData.getDatas && xData.getDatas instanceof Function) {
            datas = [...xData.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        this.Exit(datas, contextJO,paramJO,  $event, xData,this,"ListContact");
    }

    /**
     * 关闭
     *
     * @param {any[]} args 当前数据
     * @param {any} contextJO 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @memberof ListContactEditViewBase
     */
    public Exit(args: any[],contextJO?:any, params?: any, $event?: any, xData?: any,actionContext?:any,srfParentDeName?:string) {
        this.closeView(args);
        if(window.parent){
            window.parent.postMessage([{ ...args }],'*');
        }
    }


}
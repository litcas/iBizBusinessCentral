import { Subject } from 'rxjs';
import { EditViewBase } from '@/studio-core';
import IBizListService from '@/service/ibiz-list/ibiz-list-service';
import IBizListAuthService from '@/authservice/ibiz-list/ibiz-list-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import IBizListUIService from '@/uiservice/ibiz-list/ibiz-list-ui-service';

/**
 * 市场营销列表编辑视图视图基类
 *
 * @export
 * @class IBizListInfo_AbstractBase
 * @extends {EditViewBase}
 */
export class IBizListInfo_AbstractBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof IBizListInfo_AbstractBase
     */
    protected appDeName: string = 'ibizlist';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof IBizListInfo_AbstractBase
     */
    protected appDeKey: string = 'listid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof IBizListInfo_AbstractBase
     */
    protected appDeMajor: string = 'listname';

    /**
     * 实体服务对象
     *
     * @type {IBizListService}
     * @memberof IBizListInfo_AbstractBase
     */
    protected appEntityService: IBizListService = new IBizListService;

    /**
     * 实体权限服务对象
     *
     * @type IBizListUIService
     * @memberof IBizListInfo_AbstractBase
     */
    public appUIService: IBizListUIService = new IBizListUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof IBizListInfo_AbstractBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof IBizListInfo_AbstractBase
     */
    protected model: any = {
        srfCaption: 'entities.ibizlist.views.info_abstract.caption',
        srfTitle: 'entities.ibizlist.views.info_abstract.title',
        srfSubTitle: 'entities.ibizlist.views.info_abstract.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof IBizListInfo_AbstractBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'b16d4453e74dc9e530b8d46fcf0f7814';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof IBizListInfo_AbstractBase
     */
    public engine: EditViewEngine = new EditViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof IBizListInfo_AbstractBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'ibizlist',
            majorPSDEField: 'listname',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBizListInfo_AbstractBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBizListInfo_AbstractBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBizListInfo_AbstractBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}
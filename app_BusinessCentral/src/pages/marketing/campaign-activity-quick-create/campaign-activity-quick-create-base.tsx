import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { OptionViewBase } from '@/studio-core';
import CampaignActivityService from '@/service/campaign-activity/campaign-activity-service';
import CampaignActivityAuthService from '@/authservice/campaign-activity/campaign-activity-auth-service';
import OptionViewEngine from '@engine/view/option-view-engine';
import CampaignActivityUIService from '@/uiservice/campaign-activity/campaign-activity-ui-service';

/**
 * 快速新建视图基类
 *
 * @export
 * @class CampaignActivityQuickCreateBase
 * @extends {OptionViewBase}
 */
export class CampaignActivityQuickCreateBase extends OptionViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof CampaignActivityQuickCreateBase
     */
    protected appDeName: string = 'campaignactivity';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof CampaignActivityQuickCreateBase
     */
    protected appDeKey: string = 'activityid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof CampaignActivityQuickCreateBase
     */
    protected appDeMajor: string = 'subject';

    /**
     * 实体服务对象
     *
     * @type {CampaignActivityService}
     * @memberof CampaignActivityQuickCreateBase
     */
    protected appEntityService: CampaignActivityService = new CampaignActivityService;

    /**
     * 实体权限服务对象
     *
     * @type CampaignActivityUIService
     * @memberof CampaignActivityQuickCreateBase
     */
    public appUIService: CampaignActivityUIService = new CampaignActivityUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof CampaignActivityQuickCreateBase
     */    
    protected counterServiceArray: Array<any> = [];

	/**
	 * 自定义视图导航参数集合
	 *
     * @protected
	 * @type {*}
	 * @memberof CampaignActivityQuickCreateBase
	 */
    protected customViewParams: any = {
        'regardingobjectid': { isRawValue: false, value: 'campaign' }
    };

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof CampaignActivityQuickCreateBase
     */
    protected model: any = {
        srfCaption: 'entities.campaignactivity.views.quickcreate.caption',
        srfTitle: 'entities.campaignactivity.views.quickcreate.title',
        srfSubTitle: 'entities.campaignactivity.views.quickcreate.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof CampaignActivityQuickCreateBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'a12f1de520040f893da6aedd43eb4961';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof CampaignActivityQuickCreateBase
     */
    public engine: OptionViewEngine = new OptionViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof CampaignActivityQuickCreateBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'campaignactivity',
            majorPSDEField: 'subject',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof CampaignActivityQuickCreateBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof CampaignActivityQuickCreateBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof CampaignActivityQuickCreateBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}
import { Subject } from 'rxjs';
import { TabExpViewBase } from '@/studio-core';
import CampaignService from '@/service/campaign/campaign-service';
import CampaignAuthService from '@/authservice/campaign/campaign-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import CampaignUIService from '@/uiservice/campaign/campaign-ui-service';

/**
 * 市场活动状态分页视图视图基类
 *
 * @export
 * @class CampaignStateTabViewBase
 * @extends {TabExpViewBase}
 */
export class CampaignStateTabViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof CampaignStateTabViewBase
     */
    protected appDeName: string = 'campaign';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof CampaignStateTabViewBase
     */
    protected appDeKey: string = 'campaignid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof CampaignStateTabViewBase
     */
    protected appDeMajor: string = 'campaignname';

    /**
     * 实体服务对象
     *
     * @type {CampaignService}
     * @memberof CampaignStateTabViewBase
     */
    protected appEntityService: CampaignService = new CampaignService;

    /**
     * 实体权限服务对象
     *
     * @type CampaignUIService
     * @memberof CampaignStateTabViewBase
     */
    public appUIService: CampaignUIService = new CampaignUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof CampaignStateTabViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof CampaignStateTabViewBase
     */
    protected model: any = {
        srfCaption: 'entities.campaign.views.statetabview.caption',
        srfTitle: 'entities.campaign.views.statetabview.title',
        srfSubTitle: 'entities.campaign.views.statetabview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof CampaignStateTabViewBase
     */
    protected containerModel: any = {
        view_tabexppanel: { name: 'tabexppanel', type: 'TABEXPPANEL' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'e0b474e2c960a07f02f5a0714a683634';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof CampaignStateTabViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof CampaignStateTabViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'campaign',
            majorPSDEField: 'campaignname',
            isLoadDefault: true,
        });
    }


}
import { Subject } from 'rxjs';
import { TabExpViewBase } from '@/studio-core';
import IBizListService from '@/service/ibiz-list/ibiz-list-service';
import IBizListAuthService from '@/authservice/ibiz-list/ibiz-list-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import IBizListUIService from '@/uiservice/ibiz-list/ibiz-list-ui-service';

/**
 * 市场营销列表状态分页视图视图基类
 *
 * @export
 * @class IBizListStateTabViewBase
 * @extends {TabExpViewBase}
 */
export class IBizListStateTabViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof IBizListStateTabViewBase
     */
    protected appDeName: string = 'ibizlist';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof IBizListStateTabViewBase
     */
    protected appDeKey: string = 'listid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof IBizListStateTabViewBase
     */
    protected appDeMajor: string = 'listname';

    /**
     * 实体服务对象
     *
     * @type {IBizListService}
     * @memberof IBizListStateTabViewBase
     */
    protected appEntityService: IBizListService = new IBizListService;

    /**
     * 实体权限服务对象
     *
     * @type IBizListUIService
     * @memberof IBizListStateTabViewBase
     */
    public appUIService: IBizListUIService = new IBizListUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof IBizListStateTabViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof IBizListStateTabViewBase
     */
    protected model: any = {
        srfCaption: 'entities.ibizlist.views.statetabview.caption',
        srfTitle: 'entities.ibizlist.views.statetabview.title',
        srfSubTitle: 'entities.ibizlist.views.statetabview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof IBizListStateTabViewBase
     */
    protected containerModel: any = {
        view_tabexppanel: { name: 'tabexppanel', type: 'TABEXPPANEL' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '29b0091518850a55f7a722a43e49c24f';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof IBizListStateTabViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof IBizListStateTabViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'ibizlist',
            majorPSDEField: 'listname',
            isLoadDefault: true,
        });
    }


}
import { Subject } from 'rxjs';
import { DashboardViewBase } from '@/studio-core';
import IBizListService from '@/service/ibiz-list/ibiz-list-service';
import IBizListAuthService from '@/authservice/ibiz-list/ibiz-list-auth-service';
import PortalViewEngine from '@engine/view/portal-view-engine';
import IBizListUIService from '@/uiservice/ibiz-list/ibiz-list-ui-service';

/**
 * 市场营销列表概览视图基类
 *
 * @export
 * @class IBizListSummaryBase
 * @extends {DashboardViewBase}
 */
export class IBizListSummaryBase extends DashboardViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof IBizListSummaryBase
     */
    protected appDeName: string = 'ibizlist';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof IBizListSummaryBase
     */
    protected appDeKey: string = 'listid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof IBizListSummaryBase
     */
    protected appDeMajor: string = 'listname';

    /**
     * 实体服务对象
     *
     * @type {IBizListService}
     * @memberof IBizListSummaryBase
     */
    protected appEntityService: IBizListService = new IBizListService;

    /**
     * 实体权限服务对象
     *
     * @type IBizListUIService
     * @memberof IBizListSummaryBase
     */
    public appUIService: IBizListUIService = new IBizListUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof IBizListSummaryBase
     */    
    protected counterServiceArray: Array<any> = [];

	/**
	 * 自定义视图导航上下文集合
	 *
     * @protected
	 * @type {*}
	 * @memberof IBizListSummaryBase
	 */
    protected customViewNavContexts: any = {
        'REGARDINGOBJECTID': { isRawValue: false, value: 'ibizlist' },
        'REGARDINGOBJECTTYPECODE': { isRawValue: true, value: 'IBIZLIST' }
    };

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof IBizListSummaryBase
     */
    protected model: any = {
        srfCaption: 'entities.ibizlist.views.summary.caption',
        srfTitle: 'entities.ibizlist.views.summary.title',
        srfSubTitle: 'entities.ibizlist.views.summary.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof IBizListSummaryBase
     */
    protected containerModel: any = {
        view_dashboard: { name: 'dashboard', type: 'DASHBOARD' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '78abe6f7e960e0e5d5df0b18b319c241';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof IBizListSummaryBase
     */
    public engine: PortalViewEngine = new PortalViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof IBizListSummaryBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            dashboard: this.$refs.dashboard,
            keyPSDEField: 'ibizlist',
            majorPSDEField: 'listname',
            isLoadDefault: true,
        });
    }

    /**
     * dashboard 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBizListSummaryBase
     */
    public dashboard_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('dashboard', 'load', $event);
    }


}
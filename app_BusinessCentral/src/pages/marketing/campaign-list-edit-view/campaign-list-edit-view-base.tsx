import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import CampaignListService from '@/service/campaign-list/campaign-list-service';
import CampaignListAuthService from '@/authservice/campaign-list/campaign-list-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import CampaignListUIService from '@/uiservice/campaign-list/campaign-list-ui-service';

/**
 * 市场活动-营销列表编辑视图视图基类
 *
 * @export
 * @class CampaignListEditViewBase
 * @extends {EditViewBase}
 */
export class CampaignListEditViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof CampaignListEditViewBase
     */
    protected appDeName: string = 'campaignlist';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof CampaignListEditViewBase
     */
    protected appDeKey: string = 'relationshipsid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof CampaignListEditViewBase
     */
    protected appDeMajor: string = 'relationshipsname';

    /**
     * 实体服务对象
     *
     * @type {CampaignListService}
     * @memberof CampaignListEditViewBase
     */
    protected appEntityService: CampaignListService = new CampaignListService;

    /**
     * 实体权限服务对象
     *
     * @type CampaignListUIService
     * @memberof CampaignListEditViewBase
     */
    public appUIService: CampaignListUIService = new CampaignListUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof CampaignListEditViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof CampaignListEditViewBase
     */
    protected model: any = {
        srfCaption: 'entities.campaignlist.views.editview.caption',
        srfTitle: 'entities.campaignlist.views.editview.title',
        srfSubTitle: 'entities.campaignlist.views.editview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof CampaignListEditViewBase
     */
    protected containerModel: any = {
        view_toolbar: { name: 'toolbar', type: 'TOOLBAR' },
        view_form: { name: 'form', type: 'FORM' },
    };

    /**
     * 工具栏模型
     *
     * @type {*}
     * @memberof CampaignListEditView
     */
    public toolBarModels: any = {
        tbitem12: { name: 'tbitem12', caption: '关闭', 'isShowCaption': true, 'isShowIcon': true, tooltip: '关闭', iconcls: 'fa fa-sign-out', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Exit', target: '', class: '' } },

    };



	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '4b2d33e743a8625d5a167e9feb58c3e0';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof CampaignListEditViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof CampaignListEditViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'campaignlist',
            majorPSDEField: 'relationshipsname',
            isLoadDefault: true,
        });
    }

    /**
     * toolbar 部件 click 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof CampaignListEditViewBase
     */
    public toolbar_click($event: any, $event2?: any): void {
        if (Object.is($event.tag, 'tbitem12')) {
            this.toolbar_tbitem12_click(null, '', $event2);
        }
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof CampaignListEditViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof CampaignListEditViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof CampaignListEditViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem12_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this.$refs.form;
        if (xData.getDatas && xData.getDatas instanceof Function) {
            datas = [...xData.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        this.Exit(datas, contextJO,paramJO,  $event, xData,this,"CampaignList");
    }

    /**
     * 关闭
     *
     * @param {any[]} args 当前数据
     * @param {any} contextJO 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @memberof CampaignListEditViewBase
     */
    public Exit(args: any[],contextJO?:any, params?: any, $event?: any, xData?: any,actionContext?:any,srfParentDeName?:string) {
        this.closeView(args);
        if(window.parent){
            window.parent.postMessage([{ ...args }],'*');
        }
    }


}
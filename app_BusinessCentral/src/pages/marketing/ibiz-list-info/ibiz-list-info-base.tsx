import { Subject } from 'rxjs';
import { TabExpViewBase } from '@/studio-core';
import IBizListService from '@/service/ibiz-list/ibiz-list-service';
import IBizListAuthService from '@/authservice/ibiz-list/ibiz-list-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import DataPanelEngine from '@engine/ctrl/data-panel-engine';
import IBizListUIService from '@/uiservice/ibiz-list/ibiz-list-ui-service';

/**
 * 市场营销列表视图基类
 *
 * @export
 * @class IBizListInfoBase
 * @extends {TabExpViewBase}
 */
export class IBizListInfoBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof IBizListInfoBase
     */
    protected appDeName: string = 'ibizlist';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof IBizListInfoBase
     */
    protected appDeKey: string = 'listid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof IBizListInfoBase
     */
    protected appDeMajor: string = 'listname';

    /**
     * 实体服务对象
     *
     * @type {IBizListService}
     * @memberof IBizListInfoBase
     */
    protected appEntityService: IBizListService = new IBizListService;

    /**
     * 实体权限服务对象
     *
     * @type IBizListUIService
     * @memberof IBizListInfoBase
     */
    public appUIService: IBizListUIService = new IBizListUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof IBizListInfoBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof IBizListInfoBase
     */
    protected model: any = {
        srfCaption: 'entities.ibizlist.views.info.caption',
        srfTitle: 'entities.ibizlist.views.info.title',
        srfSubTitle: 'entities.ibizlist.views.info.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof IBizListInfoBase
     */
    protected containerModel: any = {
        view_datapanel: { name: 'datapanel', type: 'FORM' },
        view_toolbar: { name: 'toolbar', type: 'TOOLBAR' },
        view_tabexppanel: { name: 'tabexppanel', type: 'TABEXPPANEL' },
    };

    /**
     * 工具栏模型
     *
     * @type {*}
     * @memberof IBizListInfo
     */
    public toolBarModels: any = {
        tbitem1_openmaineditview: { name: 'tbitem1_openmaineditview', caption: '编辑', 'isShowCaption': true, 'isShowIcon': true, tooltip: '编辑', iconcls: 'fa fa-edit', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'OpenMainEditView', target: 'SINGLEKEY', class: '' } },

        tbitem1_remove_sep: {  name: 'tbitem1_remove_sep', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem1_remove: { name: 'tbitem1_remove', caption: '删除并关闭', 'isShowCaption': true, 'isShowIcon': true, tooltip: '删除并关闭', iconcls: 'fa fa-remove', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Remove', target: 'SINGLEKEY', class: '' } },

        seperator4: {  name: 'seperator4', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem17_active: { name: 'tbitem17_active', caption: '激活', 'isShowCaption': true, 'isShowIcon': true, tooltip: '激活', iconcls: 'fa fa-play-circle-o', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Active', target: 'SINGLEKEY', class: '' } },

        tbitem17_stop_sep: {  name: 'tbitem17_stop_sep', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem17_stop: { name: 'tbitem17_stop', caption: '停用', 'isShowCaption': true, 'isShowIcon': true, tooltip: '停用', iconcls: 'fa fa-minus-circle', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Stop', target: 'SINGLEKEY', class: '' } },

        tbitem2: {  name: 'tbitem2', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem12: { name: 'tbitem12', caption: '关闭', 'isShowCaption': true, 'isShowIcon': true, tooltip: '关闭', iconcls: 'fa fa-sign-out', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Exit', target: '', class: '' } },

    };



	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '1a3ced3f8b4a2e927a9e4ae11a1d92f5';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof IBizListInfoBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();

    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof IBizListInfoBase
     */
    public datapanel: DataPanelEngine = new DataPanelEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof IBizListInfoBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'ibizlist',
            majorPSDEField: 'listname',
            isLoadDefault: true,
        });
        this.datapanel.init({
            view: this,
            datapanel: this.$refs.datapanel,
            keyPSDEField: 'ibizlist',
            majorPSDEField: 'listname',
            isLoadDefault: true,
        });
    }

    /**
     * toolbar 部件 click 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBizListInfoBase
     */
    public toolbar_click($event: any, $event2?: any): void {
        if (Object.is($event.tag, 'tbitem1_openmaineditview')) {
            this.toolbar_tbitem1_openmaineditview_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem1_remove')) {
            this.toolbar_tbitem1_remove_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem17_active')) {
            this.toolbar_tbitem17_active_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem17_stop')) {
            this.toolbar_tbitem17_stop_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem12')) {
            this.toolbar_tbitem12_click(null, '', $event2);
        }
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem1_openmaineditview_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:IBizListUIService  = new IBizListUIService();
        curUIService.IBizList_OpenMainEditView(datas,contextJO, paramJO,  $event, xData,this,"IBizList");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem1_remove_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:IBizListUIService  = new IBizListUIService();
        curUIService.IBizList_Remove(datas,contextJO, paramJO,  $event, xData,this,"IBizList");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem17_active_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:IBizListUIService  = new IBizListUIService();
        curUIService.IBizList_Active(datas,contextJO, paramJO,  $event, xData,this,"IBizList");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem17_stop_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:IBizListUIService  = new IBizListUIService();
        curUIService.IBizList_Stop(datas,contextJO, paramJO,  $event, xData,this,"IBizList");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem12_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        this.Exit(datas, contextJO,paramJO,  $event, xData,this,"IBizList");
    }

    /**
     * 关闭
     *
     * @param {any[]} args 当前数据
     * @param {any} contextJO 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @memberof IBizListInfoBase
     */
    public Exit(args: any[],contextJO?:any, params?: any, $event?: any, xData?: any,actionContext?:any,srfParentDeName?:string) {
        this.closeView(args);
        if(window.parent){
            window.parent.postMessage([{ ...args }],'*');
        }
    }


}
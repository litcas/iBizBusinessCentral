import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import CampaignService from '@/service/campaign/campaign-service';
import CampaignAuthService from '@/authservice/campaign/campaign-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import CampaignUIService from '@/uiservice/campaign/campaign-ui-service';

/**
 * 活动信息视图基类
 *
 * @export
 * @class CampaignInfo_CampaginBase
 * @extends {EditViewBase}
 */
export class CampaignInfo_CampaginBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof CampaignInfo_CampaginBase
     */
    protected appDeName: string = 'campaign';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof CampaignInfo_CampaginBase
     */
    protected appDeKey: string = 'campaignid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof CampaignInfo_CampaginBase
     */
    protected appDeMajor: string = 'campaignname';

    /**
     * 实体服务对象
     *
     * @type {CampaignService}
     * @memberof CampaignInfo_CampaginBase
     */
    protected appEntityService: CampaignService = new CampaignService;

    /**
     * 实体权限服务对象
     *
     * @type CampaignUIService
     * @memberof CampaignInfo_CampaginBase
     */
    public appUIService: CampaignUIService = new CampaignUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof CampaignInfo_CampaginBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof CampaignInfo_CampaginBase
     */
    protected model: any = {
        srfCaption: 'entities.campaign.views.info_campagin.caption',
        srfTitle: 'entities.campaign.views.info_campagin.title',
        srfSubTitle: 'entities.campaign.views.info_campagin.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof CampaignInfo_CampaginBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '5073ab2b1f8d4bc34669e325bcd8726c';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof CampaignInfo_CampaginBase
     */
    public engine: EditViewEngine = new EditViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof CampaignInfo_CampaginBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'campaign',
            majorPSDEField: 'campaignname',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof CampaignInfo_CampaginBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof CampaignInfo_CampaginBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof CampaignInfo_CampaginBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}
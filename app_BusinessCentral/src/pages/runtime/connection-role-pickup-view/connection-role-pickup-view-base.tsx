import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import ConnectionRoleService from '@/service/connection-role/connection-role-service';
import ConnectionRoleAuthService from '@/authservice/connection-role/connection-role-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import ConnectionRoleUIService from '@/uiservice/connection-role/connection-role-ui-service';

/**
 * 连接角色数据选择视图视图基类
 *
 * @export
 * @class ConnectionRolePickupViewBase
 * @extends {PickupViewBase}
 */
export class ConnectionRolePickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof ConnectionRolePickupViewBase
     */
    protected appDeName: string = 'connectionrole';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof ConnectionRolePickupViewBase
     */
    protected appDeKey: string = 'connectionroleid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof ConnectionRolePickupViewBase
     */
    protected appDeMajor: string = 'connectionrolename';

    /**
     * 实体服务对象
     *
     * @type {ConnectionRoleService}
     * @memberof ConnectionRolePickupViewBase
     */
    protected appEntityService: ConnectionRoleService = new ConnectionRoleService;

    /**
     * 实体权限服务对象
     *
     * @type ConnectionRoleUIService
     * @memberof ConnectionRolePickupViewBase
     */
    public appUIService: ConnectionRoleUIService = new ConnectionRoleUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof ConnectionRolePickupViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof ConnectionRolePickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.connectionrole.views.pickupview.caption',
        srfTitle: 'entities.connectionrole.views.pickupview.title',
        srfSubTitle: 'entities.connectionrole.views.pickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof ConnectionRolePickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '8c76486988140b7c6a9f958785991366';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof ConnectionRolePickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof ConnectionRolePickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'connectionrole',
            majorPSDEField: 'connectionrolename',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ConnectionRolePickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ConnectionRolePickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ConnectionRolePickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}
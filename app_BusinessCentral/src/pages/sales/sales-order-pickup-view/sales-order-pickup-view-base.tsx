import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import SalesOrderService from '@/service/sales-order/sales-order-service';
import SalesOrderAuthService from '@/authservice/sales-order/sales-order-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import SalesOrderUIService from '@/uiservice/sales-order/sales-order-ui-service';

/**
 * 订单数据选择视图视图基类
 *
 * @export
 * @class SalesOrderPickupViewBase
 * @extends {PickupViewBase}
 */
export class SalesOrderPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof SalesOrderPickupViewBase
     */
    protected appDeName: string = 'salesorder';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof SalesOrderPickupViewBase
     */
    protected appDeKey: string = 'salesorderid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof SalesOrderPickupViewBase
     */
    protected appDeMajor: string = 'salesordername';

    /**
     * 实体服务对象
     *
     * @type {SalesOrderService}
     * @memberof SalesOrderPickupViewBase
     */
    protected appEntityService: SalesOrderService = new SalesOrderService;

    /**
     * 实体权限服务对象
     *
     * @type SalesOrderUIService
     * @memberof SalesOrderPickupViewBase
     */
    public appUIService: SalesOrderUIService = new SalesOrderUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof SalesOrderPickupViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof SalesOrderPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.salesorder.views.pickupview.caption',
        srfTitle: 'entities.salesorder.views.pickupview.title',
        srfSubTitle: 'entities.salesorder.views.pickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof SalesOrderPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '62c5a10bec3f9a6f8b3b766aaf8ee07a';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof SalesOrderPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof SalesOrderPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'salesorder',
            majorPSDEField: 'salesordername',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SalesOrderPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SalesOrderPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SalesOrderPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}
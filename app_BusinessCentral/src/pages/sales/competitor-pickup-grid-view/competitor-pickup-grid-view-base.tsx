import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import CompetitorService from '@/service/competitor/competitor-service';
import CompetitorAuthService from '@/authservice/competitor/competitor-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import CompetitorUIService from '@/uiservice/competitor/competitor-ui-service';

/**
 * 竞争对手选择表格视图视图基类
 *
 * @export
 * @class CompetitorPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class CompetitorPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof CompetitorPickupGridViewBase
     */
    protected appDeName: string = 'competitor';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof CompetitorPickupGridViewBase
     */
    protected appDeKey: string = 'competitorid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof CompetitorPickupGridViewBase
     */
    protected appDeMajor: string = 'competitorname';

    /**
     * 实体服务对象
     *
     * @type {CompetitorService}
     * @memberof CompetitorPickupGridViewBase
     */
    protected appEntityService: CompetitorService = new CompetitorService;

    /**
     * 实体权限服务对象
     *
     * @type CompetitorUIService
     * @memberof CompetitorPickupGridViewBase
     */
    public appUIService: CompetitorUIService = new CompetitorUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof CompetitorPickupGridViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof CompetitorPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.competitor.views.pickupgridview.caption',
        srfTitle: 'entities.competitor.views.pickupgridview.title',
        srfSubTitle: 'entities.competitor.views.pickupgridview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof CompetitorPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: { name: 'grid', type: 'GRID' },
        view_searchform: { name: 'searchform', type: 'SEARCHFORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '73f0d5fb33479d51bdf3153b5f7a5b3f';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof CompetitorPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof CompetitorPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'competitor',
            majorPSDEField: 'competitorname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof CompetitorPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof CompetitorPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof CompetitorPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof CompetitorPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof CompetitorPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof CompetitorPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof CompetitorPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof CompetitorPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}
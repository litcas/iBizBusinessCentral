import { Subject } from 'rxjs';
import { DashboardViewBase } from '@/studio-core';
import CompetitorService from '@/service/competitor/competitor-service';
import CompetitorAuthService from '@/authservice/competitor/competitor-auth-service';
import PortalViewEngine from '@engine/view/portal-view-engine';
import CompetitorUIService from '@/uiservice/competitor/competitor-ui-service';

/**
 * 竞争对手概览视图基类
 *
 * @export
 * @class CompetitorSummaryBase
 * @extends {DashboardViewBase}
 */
export class CompetitorSummaryBase extends DashboardViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof CompetitorSummaryBase
     */
    protected appDeName: string = 'competitor';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof CompetitorSummaryBase
     */
    protected appDeKey: string = 'competitorid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof CompetitorSummaryBase
     */
    protected appDeMajor: string = 'competitorname';

    /**
     * 实体服务对象
     *
     * @type {CompetitorService}
     * @memberof CompetitorSummaryBase
     */
    protected appEntityService: CompetitorService = new CompetitorService;

    /**
     * 实体权限服务对象
     *
     * @type CompetitorUIService
     * @memberof CompetitorSummaryBase
     */
    public appUIService: CompetitorUIService = new CompetitorUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof CompetitorSummaryBase
     */    
    protected counterServiceArray: Array<any> = [];

	/**
	 * 自定义视图导航上下文集合
	 *
     * @protected
	 * @type {*}
	 * @memberof CompetitorSummaryBase
	 */
    protected customViewNavContexts: any = {
        'REGARDINGOBJECTID': { isRawValue: false, value: 'competitor' },
        'REGARDINGOBJECTTYPECODE': { isRawValue: true, value: 'COMPETITOR' }
    };

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof CompetitorSummaryBase
     */
    protected model: any = {
        srfCaption: 'entities.competitor.views.summary.caption',
        srfTitle: 'entities.competitor.views.summary.title',
        srfSubTitle: 'entities.competitor.views.summary.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof CompetitorSummaryBase
     */
    protected containerModel: any = {
        view_dashboard: { name: 'dashboard', type: 'DASHBOARD' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '7be3b24d82f203e004d99ec9ed57e7bd';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof CompetitorSummaryBase
     */
    public engine: PortalViewEngine = new PortalViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof CompetitorSummaryBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            dashboard: this.$refs.dashboard,
            keyPSDEField: 'competitor',
            majorPSDEField: 'competitorname',
            isLoadDefault: true,
        });
    }

    /**
     * dashboard 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof CompetitorSummaryBase
     */
    public dashboard_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('dashboard', 'load', $event);
    }


}
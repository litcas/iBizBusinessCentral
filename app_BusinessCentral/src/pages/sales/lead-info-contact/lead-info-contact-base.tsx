import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import LeadService from '@/service/lead/lead-service';
import LeadAuthService from '@/authservice/lead/lead-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import LeadUIService from '@/uiservice/lead/lead-ui-service';

/**
 * 潜在顾客信息视图基类
 *
 * @export
 * @class LeadInfo_ContactBase
 * @extends {EditViewBase}
 */
export class LeadInfo_ContactBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof LeadInfo_ContactBase
     */
    protected appDeName: string = 'lead';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof LeadInfo_ContactBase
     */
    protected appDeKey: string = 'leadid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof LeadInfo_ContactBase
     */
    protected appDeMajor: string = 'fullname';

    /**
     * 实体服务对象
     *
     * @type {LeadService}
     * @memberof LeadInfo_ContactBase
     */
    protected appEntityService: LeadService = new LeadService;

    /**
     * 实体权限服务对象
     *
     * @type LeadUIService
     * @memberof LeadInfo_ContactBase
     */
    public appUIService: LeadUIService = new LeadUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof LeadInfo_ContactBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof LeadInfo_ContactBase
     */
    protected model: any = {
        srfCaption: 'entities.lead.views.info_contact.caption',
        srfTitle: 'entities.lead.views.info_contact.title',
        srfSubTitle: 'entities.lead.views.info_contact.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof LeadInfo_ContactBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '66b1b29633cd8c496c3938902fb4a2ec';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof LeadInfo_ContactBase
     */
    public engine: EditViewEngine = new EditViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof LeadInfo_ContactBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'lead',
            majorPSDEField: 'fullname',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof LeadInfo_ContactBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof LeadInfo_ContactBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof LeadInfo_ContactBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}
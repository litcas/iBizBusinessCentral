import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import QuoteService from '@/service/quote/quote-service';
import QuoteAuthService from '@/authservice/quote/quote-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import QuoteUIService from '@/uiservice/quote/quote-ui-service';

/**
 * 头部信息编辑视图基类
 *
 * @export
 * @class QuoteEdit_DataPanelViewBase
 * @extends {EditViewBase}
 */
export class QuoteEdit_DataPanelViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof QuoteEdit_DataPanelViewBase
     */
    protected appDeName: string = 'quote';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof QuoteEdit_DataPanelViewBase
     */
    protected appDeKey: string = 'quoteid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof QuoteEdit_DataPanelViewBase
     */
    protected appDeMajor: string = 'quotename';

    /**
     * 实体服务对象
     *
     * @type {QuoteService}
     * @memberof QuoteEdit_DataPanelViewBase
     */
    protected appEntityService: QuoteService = new QuoteService;

    /**
     * 实体权限服务对象
     *
     * @type QuoteUIService
     * @memberof QuoteEdit_DataPanelViewBase
     */
    public appUIService: QuoteUIService = new QuoteUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof QuoteEdit_DataPanelViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof QuoteEdit_DataPanelViewBase
     */
    protected model: any = {
        srfCaption: 'entities.quote.views.edit_datapanelview.caption',
        srfTitle: 'entities.quote.views.edit_datapanelview.title',
        srfSubTitle: 'entities.quote.views.edit_datapanelview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof QuoteEdit_DataPanelViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '567e0b1363f5ebe1169cd805cc22ab73';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof QuoteEdit_DataPanelViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof QuoteEdit_DataPanelViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'quote',
            majorPSDEField: 'quotename',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof QuoteEdit_DataPanelViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof QuoteEdit_DataPanelViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof QuoteEdit_DataPanelViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}
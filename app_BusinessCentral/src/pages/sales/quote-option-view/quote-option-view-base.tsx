import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { OptionViewBase } from '@/studio-core';
import QuoteService from '@/service/quote/quote-service';
import QuoteAuthService from '@/authservice/quote/quote-auth-service';
import OptionViewEngine from '@engine/view/option-view-engine';
import QuoteUIService from '@/uiservice/quote/quote-ui-service';

/**
 * 报价单选项操作视图视图基类
 *
 * @export
 * @class QuoteOptionViewBase
 * @extends {OptionViewBase}
 */
export class QuoteOptionViewBase extends OptionViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof QuoteOptionViewBase
     */
    protected appDeName: string = 'quote';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof QuoteOptionViewBase
     */
    protected appDeKey: string = 'quoteid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof QuoteOptionViewBase
     */
    protected appDeMajor: string = 'quotename';

    /**
     * 实体服务对象
     *
     * @type {QuoteService}
     * @memberof QuoteOptionViewBase
     */
    protected appEntityService: QuoteService = new QuoteService;

    /**
     * 实体权限服务对象
     *
     * @type QuoteUIService
     * @memberof QuoteOptionViewBase
     */
    public appUIService: QuoteUIService = new QuoteUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof QuoteOptionViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof QuoteOptionViewBase
     */
    protected model: any = {
        srfCaption: 'entities.quote.views.optionview.caption',
        srfTitle: 'entities.quote.views.optionview.title',
        srfSubTitle: 'entities.quote.views.optionview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof QuoteOptionViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'fd1e3a15b1d164bfe5c7ec4ef0002bdc';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof QuoteOptionViewBase
     */
    public engine: OptionViewEngine = new OptionViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof QuoteOptionViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'quote',
            majorPSDEField: 'quotename',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof QuoteOptionViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof QuoteOptionViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof QuoteOptionViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}
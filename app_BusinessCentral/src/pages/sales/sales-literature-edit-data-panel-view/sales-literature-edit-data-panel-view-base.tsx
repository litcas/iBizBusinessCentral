import { Subject } from 'rxjs';
import { EditViewBase } from '@/studio-core';
import SalesLiteratureService from '@/service/sales-literature/sales-literature-service';
import SalesLiteratureAuthService from '@/authservice/sales-literature/sales-literature-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import SalesLiteratureUIService from '@/uiservice/sales-literature/sales-literature-ui-service';

/**
 * 头部信息编辑视图基类
 *
 * @export
 * @class SalesLiteratureEdit_DataPanelViewBase
 * @extends {EditViewBase}
 */
export class SalesLiteratureEdit_DataPanelViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof SalesLiteratureEdit_DataPanelViewBase
     */
    protected appDeName: string = 'salesliterature';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof SalesLiteratureEdit_DataPanelViewBase
     */
    protected appDeKey: string = 'salesliteratureid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof SalesLiteratureEdit_DataPanelViewBase
     */
    protected appDeMajor: string = 'salesliteraturename';

    /**
     * 实体服务对象
     *
     * @type {SalesLiteratureService}
     * @memberof SalesLiteratureEdit_DataPanelViewBase
     */
    protected appEntityService: SalesLiteratureService = new SalesLiteratureService;

    /**
     * 实体权限服务对象
     *
     * @type SalesLiteratureUIService
     * @memberof SalesLiteratureEdit_DataPanelViewBase
     */
    public appUIService: SalesLiteratureUIService = new SalesLiteratureUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof SalesLiteratureEdit_DataPanelViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof SalesLiteratureEdit_DataPanelViewBase
     */
    protected model: any = {
        srfCaption: 'entities.salesliterature.views.edit_datapanelview.caption',
        srfTitle: 'entities.salesliterature.views.edit_datapanelview.title',
        srfSubTitle: 'entities.salesliterature.views.edit_datapanelview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof SalesLiteratureEdit_DataPanelViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'deaff04cc5731f73a124da5a1ce84d06';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof SalesLiteratureEdit_DataPanelViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof SalesLiteratureEdit_DataPanelViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'salesliterature',
            majorPSDEField: 'salesliteraturename',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SalesLiteratureEdit_DataPanelViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SalesLiteratureEdit_DataPanelViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SalesLiteratureEdit_DataPanelViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}
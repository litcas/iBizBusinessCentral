import { Subject } from 'rxjs';
import { OptionViewBase } from '@/studio-core';
import GoalService from '@/service/goal/goal-service';
import GoalAuthService from '@/authservice/goal/goal-auth-service';
import OptionViewEngine from '@engine/view/option-view-engine';
import GoalUIService from '@/uiservice/goal/goal-ui-service';

/**
 * 快速新建视图基类
 *
 * @export
 * @class GoalQuickCreateViewBase
 * @extends {OptionViewBase}
 */
export class GoalQuickCreateViewBase extends OptionViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof GoalQuickCreateViewBase
     */
    protected appDeName: string = 'goal';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof GoalQuickCreateViewBase
     */
    protected appDeKey: string = 'goalid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof GoalQuickCreateViewBase
     */
    protected appDeMajor: string = 'title';

    /**
     * 实体服务对象
     *
     * @type {GoalService}
     * @memberof GoalQuickCreateViewBase
     */
    protected appEntityService: GoalService = new GoalService;

    /**
     * 实体权限服务对象
     *
     * @type GoalUIService
     * @memberof GoalQuickCreateViewBase
     */
    public appUIService: GoalUIService = new GoalUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof GoalQuickCreateViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof GoalQuickCreateViewBase
     */
    protected model: any = {
        srfCaption: 'entities.goal.views.quickcreateview.caption',
        srfTitle: 'entities.goal.views.quickcreateview.title',
        srfSubTitle: 'entities.goal.views.quickcreateview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof GoalQuickCreateViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'd7e5cc68f308f44c8f2dd1a7f51d3bdc';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof GoalQuickCreateViewBase
     */
    public engine: OptionViewEngine = new OptionViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof GoalQuickCreateViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'goal',
            majorPSDEField: 'title',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof GoalQuickCreateViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof GoalQuickCreateViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof GoalQuickCreateViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}
import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import OpportunityService from '@/service/opportunity/opportunity-service';
import OpportunityAuthService from '@/authservice/opportunity/opportunity-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import OpportunityUIService from '@/uiservice/opportunity/opportunity-ui-service';

/**
 * 商机数据选择视图视图基类
 *
 * @export
 * @class OpportunityPickupViewBase
 * @extends {PickupViewBase}
 */
export class OpportunityPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof OpportunityPickupViewBase
     */
    protected appDeName: string = 'opportunity';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof OpportunityPickupViewBase
     */
    protected appDeKey: string = 'opportunityid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof OpportunityPickupViewBase
     */
    protected appDeMajor: string = 'opportunityname';

    /**
     * 实体服务对象
     *
     * @type {OpportunityService}
     * @memberof OpportunityPickupViewBase
     */
    protected appEntityService: OpportunityService = new OpportunityService;

    /**
     * 实体权限服务对象
     *
     * @type OpportunityUIService
     * @memberof OpportunityPickupViewBase
     */
    public appUIService: OpportunityUIService = new OpportunityUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof OpportunityPickupViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof OpportunityPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.opportunity.views.pickupview.caption',
        srfTitle: 'entities.opportunity.views.pickupview.title',
        srfSubTitle: 'entities.opportunity.views.pickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof OpportunityPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '15b10fa0deea0c5b96c1e498d79b69e5';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof OpportunityPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof OpportunityPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'opportunity',
            majorPSDEField: 'opportunityname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof OpportunityPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof OpportunityPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof OpportunityPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}
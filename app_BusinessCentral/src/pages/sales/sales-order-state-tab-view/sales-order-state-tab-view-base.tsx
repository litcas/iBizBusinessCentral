import { Subject } from 'rxjs';
import { TabExpViewBase } from '@/studio-core';
import SalesOrderService from '@/service/sales-order/sales-order-service';
import SalesOrderAuthService from '@/authservice/sales-order/sales-order-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import SalesOrderUIService from '@/uiservice/sales-order/sales-order-ui-service';

/**
 * 订单信息视图基类
 *
 * @export
 * @class SalesOrderStateTabViewBase
 * @extends {TabExpViewBase}
 */
export class SalesOrderStateTabViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof SalesOrderStateTabViewBase
     */
    protected appDeName: string = 'salesorder';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof SalesOrderStateTabViewBase
     */
    protected appDeKey: string = 'salesorderid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof SalesOrderStateTabViewBase
     */
    protected appDeMajor: string = 'salesordername';

    /**
     * 实体服务对象
     *
     * @type {SalesOrderService}
     * @memberof SalesOrderStateTabViewBase
     */
    protected appEntityService: SalesOrderService = new SalesOrderService;

    /**
     * 实体权限服务对象
     *
     * @type SalesOrderUIService
     * @memberof SalesOrderStateTabViewBase
     */
    public appUIService: SalesOrderUIService = new SalesOrderUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof SalesOrderStateTabViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof SalesOrderStateTabViewBase
     */
    protected model: any = {
        srfCaption: 'entities.salesorder.views.statetabview.caption',
        srfTitle: 'entities.salesorder.views.statetabview.title',
        srfSubTitle: 'entities.salesorder.views.statetabview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof SalesOrderStateTabViewBase
     */
    protected containerModel: any = {
        view_tabexppanel: { name: 'tabexppanel', type: 'TABEXPPANEL' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '166048749d603698637c9f8a207ddcb5';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof SalesOrderStateTabViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof SalesOrderStateTabViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'salesorder',
            majorPSDEField: 'salesordername',
            isLoadDefault: true,
        });
    }


}
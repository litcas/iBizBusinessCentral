import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { OptionViewBase } from '@/studio-core';
import LeadService from '@/service/lead/lead-service';
import LeadAuthService from '@/authservice/lead/lead-auth-service';
import OptionViewEngine from '@engine/view/option-view-engine';
import LeadUIService from '@/uiservice/lead/lead-ui-service';

/**
 * 授予资格视图基类
 *
 * @export
 * @class LeadQualificationBase
 * @extends {OptionViewBase}
 */
export class LeadQualificationBase extends OptionViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof LeadQualificationBase
     */
    protected appDeName: string = 'lead';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof LeadQualificationBase
     */
    protected appDeKey: string = 'leadid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof LeadQualificationBase
     */
    protected appDeMajor: string = 'fullname';

    /**
     * 实体服务对象
     *
     * @type {LeadService}
     * @memberof LeadQualificationBase
     */
    protected appEntityService: LeadService = new LeadService;

    /**
     * 实体权限服务对象
     *
     * @type LeadUIService
     * @memberof LeadQualificationBase
     */
    public appUIService: LeadUIService = new LeadUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof LeadQualificationBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof LeadQualificationBase
     */
    protected model: any = {
        srfCaption: 'entities.lead.views.qualification.caption',
        srfTitle: 'entities.lead.views.qualification.title',
        srfSubTitle: 'entities.lead.views.qualification.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof LeadQualificationBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'a51e70f47a9d85290602ad0c81e6b243';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof LeadQualificationBase
     */
    public engine: OptionViewEngine = new OptionViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof LeadQualificationBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'lead',
            majorPSDEField: 'fullname',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof LeadQualificationBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof LeadQualificationBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof LeadQualificationBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}
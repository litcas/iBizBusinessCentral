import { Subject } from 'rxjs';
import { EditViewBase } from '@/studio-core';
import CompetitorService from '@/service/competitor/competitor-service';
import CompetitorAuthService from '@/authservice/competitor/competitor-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import CompetitorUIService from '@/uiservice/competitor/competitor-ui-service';

/**
 * 竞争对手编辑视图视图基类
 *
 * @export
 * @class CompetitorInfo_AbstractBase
 * @extends {EditViewBase}
 */
export class CompetitorInfo_AbstractBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof CompetitorInfo_AbstractBase
     */
    protected appDeName: string = 'competitor';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof CompetitorInfo_AbstractBase
     */
    protected appDeKey: string = 'competitorid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof CompetitorInfo_AbstractBase
     */
    protected appDeMajor: string = 'competitorname';

    /**
     * 实体服务对象
     *
     * @type {CompetitorService}
     * @memberof CompetitorInfo_AbstractBase
     */
    protected appEntityService: CompetitorService = new CompetitorService;

    /**
     * 实体权限服务对象
     *
     * @type CompetitorUIService
     * @memberof CompetitorInfo_AbstractBase
     */
    public appUIService: CompetitorUIService = new CompetitorUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof CompetitorInfo_AbstractBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof CompetitorInfo_AbstractBase
     */
    protected model: any = {
        srfCaption: 'entities.competitor.views.info_abstract.caption',
        srfTitle: 'entities.competitor.views.info_abstract.title',
        srfSubTitle: 'entities.competitor.views.info_abstract.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof CompetitorInfo_AbstractBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'be78ca037813e48a849b10b91e72f406';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof CompetitorInfo_AbstractBase
     */
    public engine: EditViewEngine = new EditViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof CompetitorInfo_AbstractBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'competitor',
            majorPSDEField: 'competitorname',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof CompetitorInfo_AbstractBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof CompetitorInfo_AbstractBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof CompetitorInfo_AbstractBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}
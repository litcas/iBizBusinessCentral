import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { DashboardViewBase } from '@/studio-core';
import LeadService from '@/service/lead/lead-service';
import LeadAuthService from '@/authservice/lead/lead-auth-service';
import PortalViewEngine from '@engine/view/portal-view-engine';
import LeadUIService from '@/uiservice/lead/lead-ui-service';

/**
 * 潜在顾客数据看板视图视图基类
 *
 * @export
 * @class LeadDashboardViewBase
 * @extends {DashboardViewBase}
 */
export class LeadDashboardViewBase extends DashboardViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof LeadDashboardViewBase
     */
    protected appDeName: string = 'lead';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof LeadDashboardViewBase
     */
    protected appDeKey: string = 'leadid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof LeadDashboardViewBase
     */
    protected appDeMajor: string = 'fullname';

    /**
     * 实体服务对象
     *
     * @type {LeadService}
     * @memberof LeadDashboardViewBase
     */
    protected appEntityService: LeadService = new LeadService;

    /**
     * 实体权限服务对象
     *
     * @type LeadUIService
     * @memberof LeadDashboardViewBase
     */
    public appUIService: LeadUIService = new LeadUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof LeadDashboardViewBase
     */    
    protected counterServiceArray: Array<any> = [];

	/**
	 * 自定义视图导航上下文集合
	 *
     * @protected
	 * @type {*}
	 * @memberof LeadDashboardViewBase
	 */
    protected customViewNavContexts: any = {
        'REGARDINGOBJECTID': { isRawValue: false, value: 'lead' },
        'REGARDINGOBJECTTYPECODE': { isRawValue: true, value: 'LEAD' }
    };

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof LeadDashboardViewBase
     */
    protected model: any = {
        srfCaption: 'entities.lead.views.dashboardview.caption',
        srfTitle: 'entities.lead.views.dashboardview.title',
        srfSubTitle: 'entities.lead.views.dashboardview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof LeadDashboardViewBase
     */
    protected containerModel: any = {
        view_dashboard: { name: 'dashboard', type: 'DASHBOARD' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '29bfc8028805f4fa7c7aca9ce667b084';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof LeadDashboardViewBase
     */
    public engine: PortalViewEngine = new PortalViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof LeadDashboardViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            dashboard: this.$refs.dashboard,
            keyPSDEField: 'lead',
            majorPSDEField: 'fullname',
            isLoadDefault: true,
        });
    }

    /**
     * dashboard 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof LeadDashboardViewBase
     */
    public dashboard_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('dashboard', 'load', $event);
    }


}
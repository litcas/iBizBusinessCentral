import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { MPickupViewBase } from '@/studio-core';
import CompetitorService from '@/service/competitor/competitor-service';
import CompetitorAuthService from '@/authservice/competitor/competitor-auth-service';
import MPickupViewEngine from '@engine/view/mpickup-view-engine';
import CompetitorUIService from '@/uiservice/competitor/competitor-ui-service';

/**
 * 竞争对手数据多项选择视图视图基类
 *
 * @export
 * @class CompetitorMPickupViewBase
 * @extends {MPickupViewBase}
 */
export class CompetitorMPickupViewBase extends MPickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof CompetitorMPickupViewBase
     */
    protected appDeName: string = 'competitor';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof CompetitorMPickupViewBase
     */
    protected appDeKey: string = 'competitorid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof CompetitorMPickupViewBase
     */
    protected appDeMajor: string = 'competitorname';

    /**
     * 实体服务对象
     *
     * @type {CompetitorService}
     * @memberof CompetitorMPickupViewBase
     */
    protected appEntityService: CompetitorService = new CompetitorService;

    /**
     * 实体权限服务对象
     *
     * @type CompetitorUIService
     * @memberof CompetitorMPickupViewBase
     */
    public appUIService: CompetitorUIService = new CompetitorUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof CompetitorMPickupViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof CompetitorMPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.competitor.views.mpickupview.caption',
        srfTitle: 'entities.competitor.views.mpickupview.title',
        srfSubTitle: 'entities.competitor.views.mpickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof CompetitorMPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '4d07a7a728748c14e057d047a6abe42c';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof CompetitorMPickupViewBase
     */
    public engine: MPickupViewEngine = new MPickupViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof CompetitorMPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'competitor',
            majorPSDEField: 'competitorname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof CompetitorMPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof CompetitorMPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof CompetitorMPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }



    /**
     * 添加左侧面板所有数据到右侧
     *
     * @memberof CompetitorMPickupView
     */
    public onCLickAllRight(): void {
        Object.values(this.containerModel).forEach((model: any) => {
            if (!Object.is(model.type, 'PICKUPVIEWPANEL')) {
                return;
            }
            if (model.datas.length > 0) {
                model.datas.forEach((data: any, index: any) => {
                    Object.assign(data, { srfmajortext: data['competitorname'] });
                })
            }
            model.datas.forEach((item: any) => {
                const index: number = this.viewSelections.findIndex((selection: any) => Object.is(item.srfkey, selection.srfkey));
                if (index === -1) {
                    item._select = false;
                    this.viewSelections.push(item);
                }
            });
        });
        this.selectedData = JSON.stringify(this.viewSelections);
    }


}
import { Subject } from 'rxjs';
import { PickupViewBase } from '@/studio-core';
import GoalService from '@/service/goal/goal-service';
import GoalAuthService from '@/authservice/goal/goal-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import GoalUIService from '@/uiservice/goal/goal-ui-service';

/**
 * 目标数据选择视图视图基类
 *
 * @export
 * @class GoalPickupViewBase
 * @extends {PickupViewBase}
 */
export class GoalPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof GoalPickupViewBase
     */
    protected appDeName: string = 'goal';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof GoalPickupViewBase
     */
    protected appDeKey: string = 'goalid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof GoalPickupViewBase
     */
    protected appDeMajor: string = 'title';

    /**
     * 实体服务对象
     *
     * @type {GoalService}
     * @memberof GoalPickupViewBase
     */
    protected appEntityService: GoalService = new GoalService;

    /**
     * 实体权限服务对象
     *
     * @type GoalUIService
     * @memberof GoalPickupViewBase
     */
    public appUIService: GoalUIService = new GoalUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof GoalPickupViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof GoalPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.goal.views.pickupview.caption',
        srfTitle: 'entities.goal.views.pickupview.title',
        srfSubTitle: 'entities.goal.views.pickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof GoalPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '0ac803c2ada4edab5985c5c5a42ff4f3';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof GoalPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof GoalPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'goal',
            majorPSDEField: 'title',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof GoalPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof GoalPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof GoalPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}
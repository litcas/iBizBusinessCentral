import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import WFGroupService from '@/service/wfgroup/wfgroup-service';
import WFGroupAuthService from '@/authservice/wfgroup/wfgroup-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import WFGroupUIService from '@/uiservice/wfgroup/wfgroup-ui-service';

/**
 * 角色/用户组选择表格视图视图基类
 *
 * @export
 * @class WFGroupPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class WFGroupPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof WFGroupPickupGridViewBase
     */
    protected appDeName: string = 'wfgroup';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof WFGroupPickupGridViewBase
     */
    protected appDeKey: string = 'groupid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof WFGroupPickupGridViewBase
     */
    protected appDeMajor: string = 'groupname';

    /**
     * 实体服务对象
     *
     * @type {WFGroupService}
     * @memberof WFGroupPickupGridViewBase
     */
    protected appEntityService: WFGroupService = new WFGroupService;

    /**
     * 实体权限服务对象
     *
     * @type WFGroupUIService
     * @memberof WFGroupPickupGridViewBase
     */
    public appUIService: WFGroupUIService = new WFGroupUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof WFGroupPickupGridViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof WFGroupPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.wfgroup.views.pickupgridview.caption',
        srfTitle: 'entities.wfgroup.views.pickupgridview.title',
        srfSubTitle: 'entities.wfgroup.views.pickupgridview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof WFGroupPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: { name: 'grid', type: 'GRID' },
        view_searchform: { name: 'searchform', type: 'SEARCHFORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '00c544eb087eb999754c164cc4522767';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof WFGroupPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof WFGroupPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'wfgroup',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof WFGroupPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof WFGroupPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof WFGroupPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof WFGroupPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof WFGroupPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof WFGroupPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof WFGroupPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof WFGroupPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}
import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import WFUserService from '@/service/wfuser/wfuser-service';
import WFUserAuthService from '@/authservice/wfuser/wfuser-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import WFUserUIService from '@/uiservice/wfuser/wfuser-ui-service';

/**
 * 用户数据选择视图视图基类
 *
 * @export
 * @class WFUserPickupViewBase
 * @extends {PickupViewBase}
 */
export class WFUserPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof WFUserPickupViewBase
     */
    protected appDeName: string = 'wfuser';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof WFUserPickupViewBase
     */
    protected appDeKey: string = 'userid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof WFUserPickupViewBase
     */
    protected appDeMajor: string = 'personname';

    /**
     * 实体服务对象
     *
     * @type {WFUserService}
     * @memberof WFUserPickupViewBase
     */
    protected appEntityService: WFUserService = new WFUserService;

    /**
     * 实体权限服务对象
     *
     * @type WFUserUIService
     * @memberof WFUserPickupViewBase
     */
    public appUIService: WFUserUIService = new WFUserUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof WFUserPickupViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof WFUserPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.wfuser.views.pickupview.caption',
        srfTitle: 'entities.wfuser.views.pickupview.title',
        srfSubTitle: 'entities.wfuser.views.pickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof WFUserPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '8462425aecb08651249f9bfd31f96736';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof WFUserPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof WFUserPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'wfuser',
            majorPSDEField: 'displayname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof WFUserPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof WFUserPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof WFUserPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}
import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import WFGroupService from '@/service/wfgroup/wfgroup-service';
import WFGroupAuthService from '@/authservice/wfgroup/wfgroup-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import WFGroupUIService from '@/uiservice/wfgroup/wfgroup-ui-service';

/**
 * 角色/用户组数据选择视图视图基类
 *
 * @export
 * @class WFGroupPickupViewBase
 * @extends {PickupViewBase}
 */
export class WFGroupPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof WFGroupPickupViewBase
     */
    protected appDeName: string = 'wfgroup';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof WFGroupPickupViewBase
     */
    protected appDeKey: string = 'groupid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof WFGroupPickupViewBase
     */
    protected appDeMajor: string = 'groupname';

    /**
     * 实体服务对象
     *
     * @type {WFGroupService}
     * @memberof WFGroupPickupViewBase
     */
    protected appEntityService: WFGroupService = new WFGroupService;

    /**
     * 实体权限服务对象
     *
     * @type WFGroupUIService
     * @memberof WFGroupPickupViewBase
     */
    public appUIService: WFGroupUIService = new WFGroupUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof WFGroupPickupViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof WFGroupPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.wfgroup.views.pickupview.caption',
        srfTitle: 'entities.wfgroup.views.pickupview.title',
        srfSubTitle: 'entities.wfgroup.views.pickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof WFGroupPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'e5f7c32a3dfcaa46be1d1719743825ea';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof WFGroupPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof WFGroupPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'wfgroup',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof WFGroupPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof WFGroupPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof WFGroupPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}
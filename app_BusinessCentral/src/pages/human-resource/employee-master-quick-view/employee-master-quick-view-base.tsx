import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { OptionViewBase } from '@/studio-core';
import EmployeeService from '@/service/employee/employee-service';
import EmployeeAuthService from '@/authservice/employee/employee-auth-service';
import OptionViewEngine from '@engine/view/option-view-engine';
import EmployeeUIService from '@/uiservice/employee/employee-ui-service';

/**
 * 快速新建视图视图基类
 *
 * @export
 * @class EmployeeMasterQuickViewBase
 * @extends {OptionViewBase}
 */
export class EmployeeMasterQuickViewBase extends OptionViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EmployeeMasterQuickViewBase
     */
    protected appDeName: string = 'employee';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EmployeeMasterQuickViewBase
     */
    protected appDeKey: string = 'employeeid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EmployeeMasterQuickViewBase
     */
    protected appDeMajor: string = 'employeename';

    /**
     * 实体服务对象
     *
     * @type {EmployeeService}
     * @memberof EmployeeMasterQuickViewBase
     */
    protected appEntityService: EmployeeService = new EmployeeService;

    /**
     * 实体权限服务对象
     *
     * @type EmployeeUIService
     * @memberof EmployeeMasterQuickViewBase
     */
    public appUIService: EmployeeUIService = new EmployeeUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof EmployeeMasterQuickViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EmployeeMasterQuickViewBase
     */
    protected model: any = {
        srfCaption: 'entities.employee.views.masterquickview.caption',
        srfTitle: 'entities.employee.views.masterquickview.title',
        srfSubTitle: 'entities.employee.views.masterquickview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EmployeeMasterQuickViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '6cf74fb83871413d04f09231b1559fe0';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EmployeeMasterQuickViewBase
     */
    public engine: OptionViewEngine = new OptionViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EmployeeMasterQuickViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'employee',
            majorPSDEField: 'employeename',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EmployeeMasterQuickViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EmployeeMasterQuickViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EmployeeMasterQuickViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}
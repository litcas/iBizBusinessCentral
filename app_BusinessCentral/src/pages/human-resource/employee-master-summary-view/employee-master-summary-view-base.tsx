import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { DashboardViewBase } from '@/studio-core';
import EmployeeService from '@/service/employee/employee-service';
import EmployeeAuthService from '@/authservice/employee/employee-auth-service';
import PortalViewEngine from '@engine/view/portal-view-engine';
import EmployeeUIService from '@/uiservice/employee/employee-ui-service';

/**
 * 主信息概览看板视图视图基类
 *
 * @export
 * @class EmployeeMasterSummaryViewBase
 * @extends {DashboardViewBase}
 */
export class EmployeeMasterSummaryViewBase extends DashboardViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EmployeeMasterSummaryViewBase
     */
    protected appDeName: string = 'employee';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EmployeeMasterSummaryViewBase
     */
    protected appDeKey: string = 'employeeid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EmployeeMasterSummaryViewBase
     */
    protected appDeMajor: string = 'employeename';

    /**
     * 实体服务对象
     *
     * @type {EmployeeService}
     * @memberof EmployeeMasterSummaryViewBase
     */
    protected appEntityService: EmployeeService = new EmployeeService;

    /**
     * 实体权限服务对象
     *
     * @type EmployeeUIService
     * @memberof EmployeeMasterSummaryViewBase
     */
    public appUIService: EmployeeUIService = new EmployeeUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof EmployeeMasterSummaryViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EmployeeMasterSummaryViewBase
     */
    protected model: any = {
        srfCaption: 'entities.employee.views.mastersummaryview.caption',
        srfTitle: 'entities.employee.views.mastersummaryview.title',
        srfSubTitle: 'entities.employee.views.mastersummaryview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EmployeeMasterSummaryViewBase
     */
    protected containerModel: any = {
        view_dashboard: { name: 'dashboard', type: 'DASHBOARD' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '238378e8fcc75e3b771a92aef1d7b3d2';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EmployeeMasterSummaryViewBase
     */
    public engine: PortalViewEngine = new PortalViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EmployeeMasterSummaryViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            dashboard: this.$refs.dashboard,
            keyPSDEField: 'employee',
            majorPSDEField: 'employeename',
            isLoadDefault: true,
        });
    }

    /**
     * dashboard 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EmployeeMasterSummaryViewBase
     */
    public dashboard_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('dashboard', 'load', $event);
    }


}
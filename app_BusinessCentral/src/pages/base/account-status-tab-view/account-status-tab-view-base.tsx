import { Subject } from 'rxjs';
import { TabExpViewBase } from '@/studio-core';
import AccountService from '@/service/account/account-service';
import AccountAuthService from '@/authservice/account/account-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import AccountUIService from '@/uiservice/account/account-ui-service';

/**
 * 客户视图基类
 *
 * @export
 * @class AccountStatusTabViewBase
 * @extends {TabExpViewBase}
 */
export class AccountStatusTabViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof AccountStatusTabViewBase
     */
    protected appDeName: string = 'account';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof AccountStatusTabViewBase
     */
    protected appDeKey: string = 'accountid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof AccountStatusTabViewBase
     */
    protected appDeMajor: string = 'accountname';

    /**
     * 实体服务对象
     *
     * @type {AccountService}
     * @memberof AccountStatusTabViewBase
     */
    protected appEntityService: AccountService = new AccountService;

    /**
     * 实体权限服务对象
     *
     * @type AccountUIService
     * @memberof AccountStatusTabViewBase
     */
    public appUIService: AccountUIService = new AccountUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof AccountStatusTabViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof AccountStatusTabViewBase
     */
    protected model: any = {
        srfCaption: 'entities.account.views.statustabview.caption',
        srfTitle: 'entities.account.views.statustabview.title',
        srfSubTitle: 'entities.account.views.statustabview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof AccountStatusTabViewBase
     */
    protected containerModel: any = {
        view_tabexppanel: { name: 'tabexppanel', type: 'TABEXPPANEL' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'ff391715ae77b5b4434d466a3fc51001';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof AccountStatusTabViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof AccountStatusTabViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'account',
            majorPSDEField: 'accountname',
            isLoadDefault: true,
        });
    }


}
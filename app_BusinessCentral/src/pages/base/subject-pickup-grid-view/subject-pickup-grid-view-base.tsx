import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import SubjectService from '@/service/subject/subject-service';
import SubjectAuthService from '@/authservice/subject/subject-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import SubjectUIService from '@/uiservice/subject/subject-ui-service';

/**
 * 主题选择表格视图视图基类
 *
 * @export
 * @class SubjectPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class SubjectPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof SubjectPickupGridViewBase
     */
    protected appDeName: string = 'subject';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof SubjectPickupGridViewBase
     */
    protected appDeKey: string = 'subjectid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof SubjectPickupGridViewBase
     */
    protected appDeMajor: string = 'title';

    /**
     * 实体服务对象
     *
     * @type {SubjectService}
     * @memberof SubjectPickupGridViewBase
     */
    protected appEntityService: SubjectService = new SubjectService;

    /**
     * 实体权限服务对象
     *
     * @type SubjectUIService
     * @memberof SubjectPickupGridViewBase
     */
    public appUIService: SubjectUIService = new SubjectUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof SubjectPickupGridViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof SubjectPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.subject.views.pickupgridview.caption',
        srfTitle: 'entities.subject.views.pickupgridview.title',
        srfSubTitle: 'entities.subject.views.pickupgridview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof SubjectPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: { name: 'grid', type: 'GRID' },
        view_searchform: { name: 'searchform', type: 'SEARCHFORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '55bbb00957affb96575a785c6c0b7a71';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof SubjectPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof SubjectPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'subject',
            majorPSDEField: 'title',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SubjectPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SubjectPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SubjectPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SubjectPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SubjectPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SubjectPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SubjectPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof SubjectPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}
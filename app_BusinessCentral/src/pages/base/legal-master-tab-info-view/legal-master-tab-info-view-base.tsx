import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { TabExpViewBase } from '@/studio-core';
import LegalService from '@/service/legal/legal-service';
import LegalAuthService from '@/authservice/legal/legal-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import LegalUIService from '@/uiservice/legal/legal-ui-service';

/**
 * 主信息总览视图基类
 *
 * @export
 * @class LegalMasterTabInfoViewBase
 * @extends {TabExpViewBase}
 */
export class LegalMasterTabInfoViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof LegalMasterTabInfoViewBase
     */
    protected appDeName: string = 'legal';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof LegalMasterTabInfoViewBase
     */
    protected appDeKey: string = 'legalid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof LegalMasterTabInfoViewBase
     */
    protected appDeMajor: string = 'legalname';

    /**
     * 实体服务对象
     *
     * @type {LegalService}
     * @memberof LegalMasterTabInfoViewBase
     */
    protected appEntityService: LegalService = new LegalService;

    /**
     * 实体权限服务对象
     *
     * @type LegalUIService
     * @memberof LegalMasterTabInfoViewBase
     */
    public appUIService: LegalUIService = new LegalUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof LegalMasterTabInfoViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof LegalMasterTabInfoViewBase
     */
    protected model: any = {
        srfCaption: 'entities.legal.views.mastertabinfoview.caption',
        srfTitle: 'entities.legal.views.mastertabinfoview.title',
        srfSubTitle: 'entities.legal.views.mastertabinfoview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof LegalMasterTabInfoViewBase
     */
    protected containerModel: any = {
        view_tabexppanel: { name: 'tabexppanel', type: 'TABEXPPANEL' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '950a978df8865658d89e6c1b458757a9';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof LegalMasterTabInfoViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof LegalMasterTabInfoViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'legal',
            majorPSDEField: 'legalname',
            isLoadDefault: true,
        });
    }


}
import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import TransactionCurrencyService from '@/service/transaction-currency/transaction-currency-service';
import TransactionCurrencyAuthService from '@/authservice/transaction-currency/transaction-currency-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import TransactionCurrencyUIService from '@/uiservice/transaction-currency/transaction-currency-ui-service';

/**
 * 货币选择表格视图视图基类
 *
 * @export
 * @class TransactionCurrencyPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class TransactionCurrencyPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof TransactionCurrencyPickupGridViewBase
     */
    protected appDeName: string = 'transactioncurrency';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof TransactionCurrencyPickupGridViewBase
     */
    protected appDeKey: string = 'transactioncurrencyid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof TransactionCurrencyPickupGridViewBase
     */
    protected appDeMajor: string = 'currencyname';

    /**
     * 实体服务对象
     *
     * @type {TransactionCurrencyService}
     * @memberof TransactionCurrencyPickupGridViewBase
     */
    protected appEntityService: TransactionCurrencyService = new TransactionCurrencyService;

    /**
     * 实体权限服务对象
     *
     * @type TransactionCurrencyUIService
     * @memberof TransactionCurrencyPickupGridViewBase
     */
    public appUIService: TransactionCurrencyUIService = new TransactionCurrencyUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof TransactionCurrencyPickupGridViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof TransactionCurrencyPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.transactioncurrency.views.pickupgridview.caption',
        srfTitle: 'entities.transactioncurrency.views.pickupgridview.title',
        srfSubTitle: 'entities.transactioncurrency.views.pickupgridview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof TransactionCurrencyPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: { name: 'grid', type: 'GRID' },
        view_searchform: { name: 'searchform', type: 'SEARCHFORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '80f6269047dcded88d0fb2022c268dd1';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof TransactionCurrencyPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof TransactionCurrencyPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'transactioncurrency',
            majorPSDEField: 'currencyname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof TransactionCurrencyPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof TransactionCurrencyPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof TransactionCurrencyPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof TransactionCurrencyPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof TransactionCurrencyPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof TransactionCurrencyPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof TransactionCurrencyPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof TransactionCurrencyPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}
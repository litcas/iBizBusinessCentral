import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { DashboardViewBase } from '@/studio-core';
import AccountService from '@/service/account/account-service';
import AccountAuthService from '@/authservice/account/account-auth-service';
import PortalViewEngine from '@engine/view/portal-view-engine';
import AccountUIService from '@/uiservice/account/account-ui-service';

/**
 * 客户概览视图基类
 *
 * @export
 * @class AccountSummaryBase
 * @extends {DashboardViewBase}
 */
export class AccountSummaryBase extends DashboardViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof AccountSummaryBase
     */
    protected appDeName: string = 'account';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof AccountSummaryBase
     */
    protected appDeKey: string = 'accountid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof AccountSummaryBase
     */
    protected appDeMajor: string = 'accountname';

    /**
     * 实体服务对象
     *
     * @type {AccountService}
     * @memberof AccountSummaryBase
     */
    protected appEntityService: AccountService = new AccountService;

    /**
     * 实体权限服务对象
     *
     * @type AccountUIService
     * @memberof AccountSummaryBase
     */
    public appUIService: AccountUIService = new AccountUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof AccountSummaryBase
     */    
    protected counterServiceArray: Array<any> = [];

	/**
	 * 自定义视图导航上下文集合
	 *
     * @protected
	 * @type {*}
	 * @memberof AccountSummaryBase
	 */
    protected customViewNavContexts: any = {
        'REGARDINGOBJECTID': { isRawValue: false, value: 'account' },
        'REGARDINGOBJECTTYPECODE': { isRawValue: true, value: 'ACCOUNT' }
    };

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof AccountSummaryBase
     */
    protected model: any = {
        srfCaption: 'entities.account.views.summary.caption',
        srfTitle: 'entities.account.views.summary.title',
        srfSubTitle: 'entities.account.views.summary.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof AccountSummaryBase
     */
    protected containerModel: any = {
        view_dashboard: { name: 'dashboard', type: 'DASHBOARD' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '03006e40067bad32bb63465fbf24239f';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof AccountSummaryBase
     */
    public engine: PortalViewEngine = new PortalViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof AccountSummaryBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            dashboard: this.$refs.dashboard,
            keyPSDEField: 'account',
            majorPSDEField: 'accountname',
            isLoadDefault: true,
        });
    }

    /**
     * dashboard 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof AccountSummaryBase
     */
    public dashboard_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('dashboard', 'load', $event);
    }


}
import { Subject } from 'rxjs';
import { PickupViewBase } from '@/studio-core';
import LanguageLocaleService from '@/service/language-locale/language-locale-service';
import LanguageLocaleAuthService from '@/authservice/language-locale/language-locale-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import LanguageLocaleUIService from '@/uiservice/language-locale/language-locale-ui-service';

/**
 * 语言数据选择视图视图基类
 *
 * @export
 * @class LanguageLocalePickupViewBase
 * @extends {PickupViewBase}
 */
export class LanguageLocalePickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof LanguageLocalePickupViewBase
     */
    protected appDeName: string = 'languagelocale';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof LanguageLocalePickupViewBase
     */
    protected appDeKey: string = 'languagelocaleid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof LanguageLocalePickupViewBase
     */
    protected appDeMajor: string = 'languagelocalename';

    /**
     * 实体服务对象
     *
     * @type {LanguageLocaleService}
     * @memberof LanguageLocalePickupViewBase
     */
    protected appEntityService: LanguageLocaleService = new LanguageLocaleService;

    /**
     * 实体权限服务对象
     *
     * @type LanguageLocaleUIService
     * @memberof LanguageLocalePickupViewBase
     */
    public appUIService: LanguageLocaleUIService = new LanguageLocaleUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof LanguageLocalePickupViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof LanguageLocalePickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.languagelocale.views.pickupview.caption',
        srfTitle: 'entities.languagelocale.views.pickupview.title',
        srfSubTitle: 'entities.languagelocale.views.pickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof LanguageLocalePickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '7e7edefa5b3a142319f4c0fdc1056325';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof LanguageLocalePickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof LanguageLocalePickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'languagelocale',
            majorPSDEField: 'languagelocalename',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof LanguageLocalePickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof LanguageLocalePickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof LanguageLocalePickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}
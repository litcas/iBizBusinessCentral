import { Subject } from 'rxjs';
import { DashboardViewBase } from '@/studio-core';
import KnowledgeArticleService from '@/service/knowledge-article/knowledge-article-service';
import KnowledgeArticleAuthService from '@/authservice/knowledge-article/knowledge-article-auth-service';
import PortalViewEngine from '@engine/view/portal-view-engine';
import KnowledgeArticleUIService from '@/uiservice/knowledge-article/knowledge-article-ui-service';

/**
 * 知识文章数据看板视图视图基类
 *
 * @export
 * @class KnowledgeArticleSummaryViewBase
 * @extends {DashboardViewBase}
 */
export class KnowledgeArticleSummaryViewBase extends DashboardViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof KnowledgeArticleSummaryViewBase
     */
    protected appDeName: string = 'knowledgearticle';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof KnowledgeArticleSummaryViewBase
     */
    protected appDeKey: string = 'knowledgearticleid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof KnowledgeArticleSummaryViewBase
     */
    protected appDeMajor: string = 'title';

    /**
     * 实体服务对象
     *
     * @type {KnowledgeArticleService}
     * @memberof KnowledgeArticleSummaryViewBase
     */
    protected appEntityService: KnowledgeArticleService = new KnowledgeArticleService;

    /**
     * 实体权限服务对象
     *
     * @type KnowledgeArticleUIService
     * @memberof KnowledgeArticleSummaryViewBase
     */
    public appUIService: KnowledgeArticleUIService = new KnowledgeArticleUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof KnowledgeArticleSummaryViewBase
     */    
    protected counterServiceArray: Array<any> = [];

	/**
	 * 自定义视图导航上下文集合
	 *
     * @protected
	 * @type {*}
	 * @memberof KnowledgeArticleSummaryViewBase
	 */
    protected customViewNavContexts: any = {
        'REGARDINGOBJECTID': { isRawValue: false, value: 'knowledgearticle' },
        'REGARDINGOBJECTTYPECODE': { isRawValue: true, value: 'KNOWLEDGEARTICLE' }
    };

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof KnowledgeArticleSummaryViewBase
     */
    protected model: any = {
        srfCaption: 'entities.knowledgearticle.views.summaryview.caption',
        srfTitle: 'entities.knowledgearticle.views.summaryview.title',
        srfSubTitle: 'entities.knowledgearticle.views.summaryview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof KnowledgeArticleSummaryViewBase
     */
    protected containerModel: any = {
        view_dashboard: { name: 'dashboard', type: 'DASHBOARD' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'a2f266aef39a0417b5565e9943689f46';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof KnowledgeArticleSummaryViewBase
     */
    public engine: PortalViewEngine = new PortalViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof KnowledgeArticleSummaryViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            dashboard: this.$refs.dashboard,
            keyPSDEField: 'knowledgearticle',
            majorPSDEField: 'title',
            isLoadDefault: true,
        });
    }

    /**
     * dashboard 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof KnowledgeArticleSummaryViewBase
     */
    public dashboard_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('dashboard', 'load', $event);
    }


}
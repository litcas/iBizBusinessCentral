import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { WizardViewBase } from '@/studio-core';
import ActivityPointerService from '@/service/activity-pointer/activity-pointer-service';
import ActivityPointerAuthService from '@/authservice/activity-pointer/activity-pointer-auth-service';
import ActivityPointerUIService from '@/uiservice/activity-pointer/activity-pointer-ui-service';
import qs from 'qs';


/**
 * 活动数据重定向视图视图基类
 *
 * @export
 * @class ActivityPointerRedirectViewBase
 * @extends {WizardViewBase}
 */
export class ActivityPointerRedirectViewBase extends WizardViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof ActivityPointerRedirectViewBase
     */
    protected appDeName: string = 'activitypointer';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof ActivityPointerRedirectViewBase
     */
    protected appDeKey: string = 'activityid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof ActivityPointerRedirectViewBase
     */
    protected appDeMajor: string = 'subject';

    /**
     * 实体服务对象
     *
     * @type {ActivityPointerService}
     * @memberof ActivityPointerRedirectViewBase
     */
    protected appEntityService: ActivityPointerService = new ActivityPointerService;

    /**
     * 实体权限服务对象
     *
     * @type ActivityPointerUIService
     * @memberof ActivityPointerRedirectViewBase
     */
    public appUIService: ActivityPointerUIService = new ActivityPointerUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof ActivityPointerRedirectViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof ActivityPointerRedirectViewBase
     */
    protected model: any = {
        srfCaption: 'entities.activitypointer.views.redirectview.caption',
        srfTitle: 'entities.activitypointer.views.redirectview.title',
        srfSubTitle: 'entities.activitypointer.views.redirectview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof ActivityPointerRedirectViewBase
     */
    protected containerModel: any = {
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'cbbd3c8d0c31669905963452e6e5e87c';


    /**
     * 引擎初始化
     *
     * @public
     * @memberof ActivityPointerRedirectViewBase
     */
    public engineInit(): void {
    }



    /**
     * 初始化视图
     *
     * @memberof ActivityPointerRedirectViewBase
     */    
    public async viewInit(){
        let srfkey:any = this.context.activitypointer;
        this.appUIService.getRDAppView(srfkey,false).then((res:any) =>{
            if(res && res.viewname && res.srfappde){
                let indexPath:string = ViewTool.getIndexRoutePath(this.$route);
                const path:string =`${indexPath}/${res.srfappde}/${srfkey}/${res.viewname}?${qs.stringify(this.viewparams, { delimiter: ';' })}`;
                this.$router.replace({path:path});
            }else{
                console.error("未查找到重定向视图")
            }
        })
    }


}
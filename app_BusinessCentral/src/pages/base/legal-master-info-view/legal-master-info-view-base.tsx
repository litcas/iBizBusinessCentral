import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import LegalService from '@/service/legal/legal-service';
import LegalAuthService from '@/authservice/legal/legal-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import LegalUIService from '@/uiservice/legal/legal-ui-service';

/**
 * 主信息概览视图视图基类
 *
 * @export
 * @class LegalMasterInfoViewBase
 * @extends {EditViewBase}
 */
export class LegalMasterInfoViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof LegalMasterInfoViewBase
     */
    protected appDeName: string = 'legal';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof LegalMasterInfoViewBase
     */
    protected appDeKey: string = 'legalid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof LegalMasterInfoViewBase
     */
    protected appDeMajor: string = 'legalname';

    /**
     * 实体服务对象
     *
     * @type {LegalService}
     * @memberof LegalMasterInfoViewBase
     */
    protected appEntityService: LegalService = new LegalService;

    /**
     * 实体权限服务对象
     *
     * @type LegalUIService
     * @memberof LegalMasterInfoViewBase
     */
    public appUIService: LegalUIService = new LegalUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof LegalMasterInfoViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof LegalMasterInfoViewBase
     */
    protected model: any = {
        srfCaption: 'entities.legal.views.masterinfoview.caption',
        srfTitle: 'entities.legal.views.masterinfoview.title',
        srfSubTitle: 'entities.legal.views.masterinfoview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof LegalMasterInfoViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'fd51b5cc0295272326340415414865ee';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof LegalMasterInfoViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof LegalMasterInfoViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'legal',
            majorPSDEField: 'legalname',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof LegalMasterInfoViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof LegalMasterInfoViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof LegalMasterInfoViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}
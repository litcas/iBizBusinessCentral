import { Subject } from 'rxjs';
import { PickupViewBase } from '@/studio-core';
import MetricService from '@/service/metric/metric-service';
import MetricAuthService from '@/authservice/metric/metric-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import MetricUIService from '@/uiservice/metric/metric-ui-service';

/**
 * 目标度量数据选择视图视图基类
 *
 * @export
 * @class MetricPickupViewBase
 * @extends {PickupViewBase}
 */
export class MetricPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MetricPickupViewBase
     */
    protected appDeName: string = 'metric';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof MetricPickupViewBase
     */
    protected appDeKey: string = 'metricid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof MetricPickupViewBase
     */
    protected appDeMajor: string = 'metricname';

    /**
     * 实体服务对象
     *
     * @type {MetricService}
     * @memberof MetricPickupViewBase
     */
    protected appEntityService: MetricService = new MetricService;

    /**
     * 实体权限服务对象
     *
     * @type MetricUIService
     * @memberof MetricPickupViewBase
     */
    public appUIService: MetricUIService = new MetricUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof MetricPickupViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof MetricPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.metric.views.pickupview.caption',
        srfTitle: 'entities.metric.views.pickupview.title',
        srfSubTitle: 'entities.metric.views.pickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof MetricPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '7ff410fd38e3eaac8b9b1ce504dac1a0';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof MetricPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof MetricPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'metric',
            majorPSDEField: 'metricname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof MetricPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof MetricPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof MetricPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}
import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import MultiPickDataService from '@/service/multi-pick-data/multi-pick-data-service';
import MultiPickDataAuthService from '@/authservice/multi-pick-data/multi-pick-data-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import MultiPickDataUIService from '@/uiservice/multi-pick-data/multi-pick-data-ui-service';

/**
 * 负责人视图基类
 *
 * @export
 * @class MultiPickDataACBase
 * @extends {PickupViewBase}
 */
export class MultiPickDataACBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MultiPickDataACBase
     */
    protected appDeName: string = 'multipickdata';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof MultiPickDataACBase
     */
    protected appDeKey: string = 'pickdataid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof MultiPickDataACBase
     */
    protected appDeMajor: string = 'pickdataname';

    /**
     * 实体服务对象
     *
     * @type {MultiPickDataService}
     * @memberof MultiPickDataACBase
     */
    protected appEntityService: MultiPickDataService = new MultiPickDataService;

    /**
     * 实体权限服务对象
     *
     * @type MultiPickDataUIService
     * @memberof MultiPickDataACBase
     */
    public appUIService: MultiPickDataUIService = new MultiPickDataUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof MultiPickDataACBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof MultiPickDataACBase
     */
    protected model: any = {
        srfCaption: 'entities.multipickdata.views.ac.caption',
        srfTitle: 'entities.multipickdata.views.ac.title',
        srfSubTitle: 'entities.multipickdata.views.ac.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof MultiPickDataACBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'c4beabed09dcf58537c6a3394b50899f';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof MultiPickDataACBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof MultiPickDataACBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'multipickdata',
            majorPSDEField: 'pickdataname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof MultiPickDataACBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof MultiPickDataACBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof MultiPickDataACBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}
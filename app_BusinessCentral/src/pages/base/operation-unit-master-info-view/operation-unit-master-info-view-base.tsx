import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import OperationUnitService from '@/service/operation-unit/operation-unit-service';
import OperationUnitAuthService from '@/authservice/operation-unit/operation-unit-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import OperationUnitUIService from '@/uiservice/operation-unit/operation-unit-ui-service';

/**
 * 主信息概览视图视图基类
 *
 * @export
 * @class OperationUnitMasterInfoViewBase
 * @extends {EditViewBase}
 */
export class OperationUnitMasterInfoViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof OperationUnitMasterInfoViewBase
     */
    protected appDeName: string = 'operationunit';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof OperationUnitMasterInfoViewBase
     */
    protected appDeKey: string = 'operationunitid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof OperationUnitMasterInfoViewBase
     */
    protected appDeMajor: string = 'operationunitname';

    /**
     * 实体服务对象
     *
     * @type {OperationUnitService}
     * @memberof OperationUnitMasterInfoViewBase
     */
    protected appEntityService: OperationUnitService = new OperationUnitService;

    /**
     * 实体权限服务对象
     *
     * @type OperationUnitUIService
     * @memberof OperationUnitMasterInfoViewBase
     */
    public appUIService: OperationUnitUIService = new OperationUnitUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof OperationUnitMasterInfoViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof OperationUnitMasterInfoViewBase
     */
    protected model: any = {
        srfCaption: 'entities.operationunit.views.masterinfoview.caption',
        srfTitle: 'entities.operationunit.views.masterinfoview.title',
        srfSubTitle: 'entities.operationunit.views.masterinfoview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof OperationUnitMasterInfoViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'a20eacba1e00028e490c46d7a4c67b18';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof OperationUnitMasterInfoViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof OperationUnitMasterInfoViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'operationunit',
            majorPSDEField: 'operationunitname',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof OperationUnitMasterInfoViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof OperationUnitMasterInfoViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof OperationUnitMasterInfoViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}
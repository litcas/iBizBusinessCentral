import { Subject } from 'rxjs';
import { OptionViewBase } from '@/studio-core';
import KnowledgeArticleService from '@/service/knowledge-article/knowledge-article-service';
import KnowledgeArticleAuthService from '@/authservice/knowledge-article/knowledge-article-auth-service';
import OptionViewEngine from '@engine/view/option-view-engine';
import KnowledgeArticleUIService from '@/uiservice/knowledge-article/knowledge-article-ui-service';

/**
 * 快速新建视图基类
 *
 * @export
 * @class KnowledgeArticleQuickCreateViewBase
 * @extends {OptionViewBase}
 */
export class KnowledgeArticleQuickCreateViewBase extends OptionViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof KnowledgeArticleQuickCreateViewBase
     */
    protected appDeName: string = 'knowledgearticle';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof KnowledgeArticleQuickCreateViewBase
     */
    protected appDeKey: string = 'knowledgearticleid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof KnowledgeArticleQuickCreateViewBase
     */
    protected appDeMajor: string = 'title';

    /**
     * 实体服务对象
     *
     * @type {KnowledgeArticleService}
     * @memberof KnowledgeArticleQuickCreateViewBase
     */
    protected appEntityService: KnowledgeArticleService = new KnowledgeArticleService;

    /**
     * 实体权限服务对象
     *
     * @type KnowledgeArticleUIService
     * @memberof KnowledgeArticleQuickCreateViewBase
     */
    public appUIService: KnowledgeArticleUIService = new KnowledgeArticleUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof KnowledgeArticleQuickCreateViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof KnowledgeArticleQuickCreateViewBase
     */
    protected model: any = {
        srfCaption: 'entities.knowledgearticle.views.quickcreateview.caption',
        srfTitle: 'entities.knowledgearticle.views.quickcreateview.title',
        srfSubTitle: 'entities.knowledgearticle.views.quickcreateview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof KnowledgeArticleQuickCreateViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '6218e1b557bb8d653fd979d0b2063062';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof KnowledgeArticleQuickCreateViewBase
     */
    public engine: OptionViewEngine = new OptionViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof KnowledgeArticleQuickCreateViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'knowledgearticle',
            majorPSDEField: 'title',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof KnowledgeArticleQuickCreateViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof KnowledgeArticleQuickCreateViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof KnowledgeArticleQuickCreateViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}
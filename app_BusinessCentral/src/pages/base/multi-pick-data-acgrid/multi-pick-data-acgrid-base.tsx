import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import MultiPickDataService from '@/service/multi-pick-data/multi-pick-data-service';
import MultiPickDataAuthService from '@/authservice/multi-pick-data/multi-pick-data-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import MultiPickDataUIService from '@/uiservice/multi-pick-data/multi-pick-data-ui-service';

/**
 * 负责人（客户、联系人）表格视图基类
 *
 * @export
 * @class MultiPickDataACGridBase
 * @extends {PickupGridViewBase}
 */
export class MultiPickDataACGridBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MultiPickDataACGridBase
     */
    protected appDeName: string = 'multipickdata';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof MultiPickDataACGridBase
     */
    protected appDeKey: string = 'pickdataid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof MultiPickDataACGridBase
     */
    protected appDeMajor: string = 'pickdataname';

    /**
     * 实体服务对象
     *
     * @type {MultiPickDataService}
     * @memberof MultiPickDataACGridBase
     */
    protected appEntityService: MultiPickDataService = new MultiPickDataService;

    /**
     * 实体权限服务对象
     *
     * @type MultiPickDataUIService
     * @memberof MultiPickDataACGridBase
     */
    public appUIService: MultiPickDataUIService = new MultiPickDataUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof MultiPickDataACGridBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof MultiPickDataACGridBase
     */
    protected model: any = {
        srfCaption: 'entities.multipickdata.views.acgrid.caption',
        srfTitle: 'entities.multipickdata.views.acgrid.title',
        srfSubTitle: 'entities.multipickdata.views.acgrid.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof MultiPickDataACGridBase
     */
    protected containerModel: any = {
        view_grid: { name: 'grid', type: 'GRID' },
        view_searchform: { name: 'searchform', type: 'SEARCHFORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '731def77005293813123c3d71ff10f66';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof MultiPickDataACGridBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof MultiPickDataACGridBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'multipickdata',
            majorPSDEField: 'pickdataname',
            isLoadDefault: false,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof MultiPickDataACGridBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof MultiPickDataACGridBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof MultiPickDataACGridBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof MultiPickDataACGridBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof MultiPickDataACGridBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof MultiPickDataACGridBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof MultiPickDataACGridBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof MultiPickDataACGridBase
     */
    protected isExpandSearchForm: boolean = true;


}
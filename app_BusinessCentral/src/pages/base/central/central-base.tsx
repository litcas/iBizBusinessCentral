import { Vue } from 'vue-property-decorator';
import { FooterItemsService } from '@/studio-core/service/FooterItemsService';
import { AppService } from '@/studio-core/service/app-service/AppService';
import AppMenusModel from '@/widgets/app/central-appmenu/central-appmenu-model';
import { Environment } from '@/environments/environment';

/**
 * 应用首页基类
 */
export class CentralBase extends Vue {
  /**
   * 计数器服务对象集合
   *
   * @type {any[]}
   * @memberof CentralBase
   */
  protected counterServiceArray: any[] = [];

  /**
   * 应用服务
   *
   * @protected
   * @type {AppService}
   * @memberof CentralBase
   */
  protected appService: AppService = new AppService();

  /**
   * 应用菜单集合
   *
   * @type {AppMenusModel}
   * @memberof CentralBase
   */
  protected appMenuModel: AppMenusModel = new AppMenusModel();

  /**
   * 左侧导航菜单
   *
   * @type {*}
   * @memberof CentralBase
   */
  protected left_exp: any = this.appMenuModel.getMenuGroup('left_exp') || {};

  /**
   * 底部导航菜单
   *
   * @type {*}
   * @memberof CentralBase
   */
  protected bottom_exp: any = this.appMenuModel.getMenuGroup('bottom_exp') || {};
 
  /**
   * 标题栏菜单
   *
   * @type {*}
   * @memberof CentralBase
   */
  protected top_menus: any = this.appMenuModel.getMenuGroup('top_menus') || {};
 
  /**
   * 用户菜单
   *
   * @type {*}
   * @memberof CentralBase
   */
  protected user_menus: any = this.appMenuModel.getMenuGroup('user_menus') || {};

  /**
   * 底部项绘制服务
   *
   * @type {FooterItemsService}
   * @memberof CentralBase
   */
  protected footerItemsService: FooterItemsService = new FooterItemsService();

  /**
   * 视图标识
   *
   * @type {string}
   * @memberof CentralBase
   */
  protected viewtag: string = '36502c7cedb99f1a7d45fc5651242ac1';

  /**
   * 视图模型数据
   *
   * @type {*}
   * @memberof CentralBase
   */
  protected model: any = {
      srfCaption: 'app.views.central.caption',
      srfTitle: 'app.views.central.title',
      srfSubTitle: 'app.views.central.subtitle',
      dataInfo: ''
  }

  /**
   * 应用上下文
   *
   * @type {*}
   * @memberof CentralBase
   */
  protected context: any = {};

  /**
   * 视图参数
   *
   * @type {*}
   * @memberof CentralBase
   */
  protected viewparams: any = {};

  /**
   * 注册底部项
   *
   * @memberof CentralBase
   */
  protected registerFooterItems(): void {
    const leftItems: any = this.appMenuModel.getMenuGroup('footer_left');
    const centerItems: any = this.appMenuModel.getMenuGroup('footer_center');
    const rightItems: any = this.appMenuModel.getMenuGroup('footer_right');
    if (leftItems && leftItems.items) {
      leftItems.items.forEach((item: any) => {
        this.footerItemsService.registerLeftItem((h: any) => {
          return <div class='action-item' title={item.tooltip} on-click={() => this.click(item)}>
            <menu-icon item={item}/>
            {item.text}
          </div>;
        });
      });
    }
    if (centerItems && centerItems.items) {
      centerItems.items.forEach((item: any) => {
        this.footerItemsService.registerCenterItem((h: any) => {
          return <div class='action-item' title={item.tooltip} on-click={() => this.click(item)}>
            <menu-icon item={item}/>
            {item.text}
          </div>;
        });
      });
    }
    if (rightItems && rightItems.items) {
      rightItems.items.forEach((item: any) => {
        this.footerItemsService.registerRightItem((h: any) => {
          return <div class='action-item' title={item.tooltip} on-click={() => this.click(item)}>
            <menu-icon item={item}/>
            {item.text}
          </div>;
        });
      });
    }
  }

  /**
   * 项点击触发界面行为
   *
   * @protected
   * @param {*} item
   * @memberof CentralBase
   */
  protected click(item: any): void {
    const appMenu: any = this.$refs.appmenu;
    if (appMenu) {
      appMenu.click(item);
    }
  }

  /**
   * 组件创建完毕
   *
   * @memberof CentralBase
   */
  protected created() {
    this.left_exp = this.handleMenusResource(this.left_exp);
    this.bottom_exp = this.handleMenusResource(this.bottom_exp);
    this.top_menus = this.handleMenusResource(this.top_menus);
    this.user_menus = this.handleMenusResource(this.user_menus);
    const secondtag = this.$util.createUUID();
    this.$store.commit("viewaction/createdView", {
      viewtag: this.viewtag,
      secondtag: secondtag
    });
    this.viewtag = secondtag;
    this.parseViewParam();
    this.$uiState.changeLayoutState({
      styleMode: 'STYLE2'
    });
    this.registerFooterItems();
  }

  /**
   * 销毁之前
   *
   * @memberof CentralBase
   */
  protected beforeDestroy() {
    this.$store.commit("viewaction/removeView", this.viewtag);
  }

  /**
   * Vue声明周期(组件初始化完毕)
   *
   * @memberof CentralBase
   */
  protected mounted() {
    this.$viewTool.setIndexParameters([
      { pathName: 'central', parameterName: 'central' }
    ]);
    this.$viewTool.setIndexViewParam(this.context);
    setTimeout(() => {
      const el = document.getElementById('app-loading-x');
      if (el) {
        el.style.display = 'none';
      }
    }, 300);
  }

  /**
   * 解析视图参数
   *
   * @private
   * @memberof CentralBase
   */
  private parseViewParam(): void {
    const params = this.$route.params;
    if (params?.central) {
      this.context.central = params.central;
    }
    const context = this.$appService.contextStore.appContext;
    if (context) {
      Object.assign(this.context, context);
      context.clearAll();
      Object.assign(context, this.context);
    }
  }

  /**
   * 通过统一资源标识计算菜单
   *
   * @param {*} data
   * @memberof CentralBase
   */
  public handleMenusResource(inputMenus: Array<any>) {
    if (Environment.enablePermissionValid) {
      this.computedEffectiveMenus(inputMenus);
      this.computeParentMenus(inputMenus);
    }
    return inputMenus;
  }

  /**
   * 计算父项菜单项是否隐藏
   *
   * @param {*} inputMenus
   * @memberof CentralBase
   */
  public computeParentMenus(inputMenus: Array<any>) {
    if (inputMenus && inputMenus.length > 0) {
      inputMenus.forEach((item: any) => {
        if (item.hidden && item.items && item.items.length > 0) {
          item.items.map((singleItem: any) => {
            if (!singleItem.hidden) {
              item.hidden = false;
            }
            if (singleItem.items && singleItem.items.length > 0) {
              this.computeParentMenus(singleItem.items);
            }
          })
        }
      })
    }
  }

  /**
   * 计算有效菜单项
   *
   * @param {*} inputMenus
   * @memberof CentralBase
   */
  public computedEffectiveMenus(inputMenus: Array<any>) {
    inputMenus.forEach((_item: any) => {
      if (!this.$store.getters['authresource/getAuthMenu'](_item)) {
        _item.hidden = true;
        if (_item.items && _item.items.length > 0) {
          this.computedEffectiveMenus(_item.items);
        }
      }
    })
  }


  /**
   * 绘制内容
   */
  public render(): any {
    const styleMode = this.$uiState.layoutState.styleMode;
    let leftContent: any;
    switch (styleMode) {
      case 'DEFAULT':
        leftContent = <app-content-left-exp ref="leftExp" ctrlName="central" menus={this.left_exp.items} />;
        break;
      case 'STYLE2':
        leftContent = <app-content-left-nav-menu ref="leftNavMenu" ctrlName="central" menus={this.left_exp.items} on-menu-click={(item: any) => this.click(item)}/>;
    }
    return (
      <app-layout ref="appLayout">
        <template slot="header">
          <app-header>
            <template slot="header_left">
              <div class="title">
                企业中心
              </div>
            </template>
            <template slot="header_right">
              <app-header-menus ref="headerMenus" ctrlName="central" menus={this.top_menus.items} on-menu-click={(item: any) => this.click(item)}/>
              <app-lang style='font-size: 15px;padding: 0 10px;'></app-lang>
              <user-info ref="userInfo" ctrlName="central" menus={this.user_menus.items} on-menu-click={(item: any) => this.click(item)}/>
            </template>
          </app-header>
          <view_appmenu ref='appmenu'/>
        </template>
        <app-content>
          {this.left_exp.items ? <template slot="content_left">
            {leftContent}
          </template> : null}
          {styleMode === 'DEFAULT' ? <tab-page-exp ref="tabExp"></tab-page-exp> : null}
          <div class="view-warp">
            <app-keep-alive routerList={this.appService.navHistory.historyList}>
              <router-view key={this.$route.fullPath}></router-view>
            </app-keep-alive>
          </div>
          {this.bottom_exp.items ? <template slot="content_bottom">
            <app-content-bottom-exp ref="bootomExp" ctrlName="central" menus={this.bottom_exp.items} />
          </template> : null}
        </app-content>
        <template slot="footer">
          <app-footer ref="footer"/>
        </template>
      </app-layout>
    );
  }
}
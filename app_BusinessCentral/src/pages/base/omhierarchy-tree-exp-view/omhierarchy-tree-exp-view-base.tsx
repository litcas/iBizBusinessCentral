import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { TreeExpViewBase } from '@/studio-core';
import OMHierarchyService from '@/service/omhierarchy/omhierarchy-service';
import OMHierarchyAuthService from '@/authservice/omhierarchy/omhierarchy-auth-service';
import TreeExpViewEngine from '@engine/view/tree-exp-view-engine';
import OMHierarchyUIService from '@/uiservice/omhierarchy/omhierarchy-ui-service';

/**
 * 组织层次结构树导航视图视图基类
 *
 * @export
 * @class OMHierarchyTreeExpViewBase
 * @extends {TreeExpViewBase}
 */
export class OMHierarchyTreeExpViewBase extends TreeExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof OMHierarchyTreeExpViewBase
     */
    protected appDeName: string = 'omhierarchy';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof OMHierarchyTreeExpViewBase
     */
    protected appDeKey: string = 'omhierarchyid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof OMHierarchyTreeExpViewBase
     */
    protected appDeMajor: string = 'omhierarchyname';

    /**
     * 实体服务对象
     *
     * @type {OMHierarchyService}
     * @memberof OMHierarchyTreeExpViewBase
     */
    protected appEntityService: OMHierarchyService = new OMHierarchyService;

    /**
     * 实体权限服务对象
     *
     * @type OMHierarchyUIService
     * @memberof OMHierarchyTreeExpViewBase
     */
    public appUIService: OMHierarchyUIService = new OMHierarchyUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof OMHierarchyTreeExpViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof OMHierarchyTreeExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.omhierarchy.views.treeexpview.caption',
        srfTitle: 'entities.omhierarchy.views.treeexpview.title',
        srfSubTitle: 'entities.omhierarchy.views.treeexpview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof OMHierarchyTreeExpViewBase
     */
    protected containerModel: any = {
        view_treeexpbar: { name: 'treeexpbar', type: 'TREEEXPBAR' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'd38dc7b197b7c56ad6342d3d0d2f5b5a';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof OMHierarchyTreeExpViewBase
     */
    public engine: TreeExpViewEngine = new TreeExpViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof OMHierarchyTreeExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            treeexpbar: this.$refs.treeexpbar,
            keyPSDEField: 'omhierarchy',
            majorPSDEField: 'omhierarchyname',
            isLoadDefault: true,
        });
    }

    /**
     * treeexpbar 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof OMHierarchyTreeExpViewBase
     */
    public treeexpbar_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'selectionchange', $event);
    }

    /**
     * treeexpbar 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof OMHierarchyTreeExpViewBase
     */
    public treeexpbar_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'activated', $event);
    }

    /**
     * treeexpbar 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof OMHierarchyTreeExpViewBase
     */
    public treeexpbar_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'load', $event);
    }

    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof OMHierarchyTreeExpView
     */
    public newdata(args: any[],fullargs?:any[], params?: any, $event?: any, xData?: any) {
        let localContext:any = null;
        let localViewParam:any =null;
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }


    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof OMHierarchyTreeExpView
     */
    public opendata(args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) {
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }



    /**
     * 视图唯一标识
     *
     * @type {string}
     * @memberof OMHierarchyTreeExpView
     */
    public viewUID: string = 'base-omhierarchy-tree-exp-view';


}
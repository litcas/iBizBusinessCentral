import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { OptionViewBase } from '@/studio-core';
import OperationUnitService from '@/service/operation-unit/operation-unit-service';
import OperationUnitAuthService from '@/authservice/operation-unit/operation-unit-auth-service';
import OptionViewEngine from '@engine/view/option-view-engine';
import OperationUnitUIService from '@/uiservice/operation-unit/operation-unit-ui-service';

/**
 * 部门快速新建视图视图基类
 *
 * @export
 * @class OperationUnitDeptMasterQuickViewBase
 * @extends {OptionViewBase}
 */
export class OperationUnitDeptMasterQuickViewBase extends OptionViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof OperationUnitDeptMasterQuickViewBase
     */
    protected appDeName: string = 'operationunit';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof OperationUnitDeptMasterQuickViewBase
     */
    protected appDeKey: string = 'operationunitid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof OperationUnitDeptMasterQuickViewBase
     */
    protected appDeMajor: string = 'operationunitname';

    /**
     * 实体服务对象
     *
     * @type {OperationUnitService}
     * @memberof OperationUnitDeptMasterQuickViewBase
     */
    protected appEntityService: OperationUnitService = new OperationUnitService;

    /**
     * 实体权限服务对象
     *
     * @type OperationUnitUIService
     * @memberof OperationUnitDeptMasterQuickViewBase
     */
    public appUIService: OperationUnitUIService = new OperationUnitUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof OperationUnitDeptMasterQuickViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof OperationUnitDeptMasterQuickViewBase
     */
    protected model: any = {
        srfCaption: 'entities.operationunit.views.deptmasterquickview.caption',
        srfTitle: 'entities.operationunit.views.deptmasterquickview.title',
        srfSubTitle: 'entities.operationunit.views.deptmasterquickview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof OperationUnitDeptMasterQuickViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'ba058a45bb360b241a8015800951cb80';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof OperationUnitDeptMasterQuickViewBase
     */
    public engine: OptionViewEngine = new OptionViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof OperationUnitDeptMasterQuickViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'operationunit',
            majorPSDEField: 'operationunitname',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof OperationUnitDeptMasterQuickViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof OperationUnitDeptMasterQuickViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof OperationUnitDeptMasterQuickViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}
import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { TabExpViewBase } from '@/studio-core';
import ContactService from '@/service/contact/contact-service';
import ContactAuthService from '@/authservice/contact/contact-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import DataPanelEngine from '@engine/ctrl/data-panel-engine';
import ContactUIService from '@/uiservice/contact/contact-ui-service';

/**
 * 联系人信息视图基类
 *
 * @export
 * @class ContactInfoBase
 * @extends {TabExpViewBase}
 */
export class ContactInfoBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof ContactInfoBase
     */
    protected appDeName: string = 'contact';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof ContactInfoBase
     */
    protected appDeKey: string = 'contactid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof ContactInfoBase
     */
    protected appDeMajor: string = 'fullname';

    /**
     * 实体服务对象
     *
     * @type {ContactService}
     * @memberof ContactInfoBase
     */
    protected appEntityService: ContactService = new ContactService;

    /**
     * 实体权限服务对象
     *
     * @type ContactUIService
     * @memberof ContactInfoBase
     */
    public appUIService: ContactUIService = new ContactUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof ContactInfoBase
     */    
    protected counterServiceArray: Array<any> = [];

	/**
	 * 自定义视图导航上下文集合
	 *
     * @protected
	 * @type {*}
	 * @memberof ContactInfoBase
	 */
    protected customViewNavContexts: any = {
        'CUSTOMERTYPE': { isRawValue: true, value: 'CONTACT' },
        'CUSTOMERNAME': { isRawValue: false, value: 'contact' },
        'CUSTOMERID': { isRawValue: false, value: 'contact' }
    };

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof ContactInfoBase
     */
    protected model: any = {
        srfCaption: 'entities.contact.views.info.caption',
        srfTitle: 'entities.contact.views.info.title',
        srfSubTitle: 'entities.contact.views.info.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof ContactInfoBase
     */
    protected containerModel: any = {
        view_toolbar: { name: 'toolbar', type: 'TOOLBAR' },
        view_datapanel: { name: 'datapanel', type: 'FORM' },
        view_tabexppanel: { name: 'tabexppanel', type: 'TABEXPPANEL' },
    };

    /**
     * 工具栏模型
     *
     * @type {*}
     * @memberof ContactInfo
     */
    public toolBarModels: any = {
        tbitem1_editmain: { name: 'tbitem1_editmain', caption: '编辑', 'isShowCaption': true, 'isShowIcon': true, tooltip: '编辑', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'EditMain', target: 'SINGLEKEY', class: '' } },

        tbitem1_remove_sep: {  name: 'tbitem1_remove_sep', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem1_remove: { name: 'tbitem1_remove', caption: '删除并关闭', 'isShowCaption': true, 'isShowIcon': true, tooltip: '删除并关闭', iconcls: 'fa fa-remove', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Remove', target: 'SINGLEKEY', class: '' } },

        seperator4: {  name: 'seperator4', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem17_inactive: { name: 'tbitem17_inactive', caption: '停用', 'isShowCaption': true, 'isShowIcon': true, tooltip: '停用', iconcls: 'fa fa-minus-circle', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Inactive', target: 'SINGLEKEY', class: '' } },

        tbitem17_active_sep: {  name: 'tbitem17_active_sep', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem17_active: { name: 'tbitem17_active', caption: '激活', 'isShowCaption': true, 'isShowIcon': true, tooltip: '激活', iconcls: 'fa fa-play-circle-o', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Active', target: 'SINGLEKEY', class: '' } },

        tbitem17_addlist_sep: {  name: 'tbitem17_addlist_sep', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem17_addlist: { name: 'tbitem17_addlist', caption: '添加到市场营销列表', 'isShowCaption': true, 'isShowIcon': true, tooltip: '添加到市场营销列表', iconcls: 'fa fa-plus', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'AddList', target: 'SINGLEKEY', class: '' } },

        tbitem2: {  name: 'tbitem2', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem12: { name: 'tbitem12', caption: '关闭', 'isShowCaption': true, 'isShowIcon': true, tooltip: '关闭', iconcls: 'fa fa-sign-out', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Exit', target: '', class: '' } },

    };



	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'b25520cf5b51b60a1d66ec4190713529';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof ContactInfoBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();

    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof ContactInfoBase
     */
    public datapanel: DataPanelEngine = new DataPanelEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof ContactInfoBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'contact',
            majorPSDEField: 'fullname',
            isLoadDefault: true,
        });
        this.datapanel.init({
            view: this,
            datapanel: this.$refs.datapanel,
            keyPSDEField: 'contact',
            majorPSDEField: 'fullname',
            isLoadDefault: true,
        });
    }

    /**
     * toolbar 部件 click 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ContactInfoBase
     */
    public toolbar_click($event: any, $event2?: any): void {
        if (Object.is($event.tag, 'tbitem1_editmain')) {
            this.toolbar_tbitem1_editmain_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem1_remove')) {
            this.toolbar_tbitem1_remove_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem17_inactive')) {
            this.toolbar_tbitem17_inactive_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem17_active')) {
            this.toolbar_tbitem17_active_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem17_addlist')) {
            this.toolbar_tbitem17_addlist_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem12')) {
            this.toolbar_tbitem12_click(null, '', $event2);
        }
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem1_editmain_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:ContactUIService  = new ContactUIService();
        curUIService.Contact_EditMain(datas,contextJO, paramJO,  $event, xData,this,"Contact");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem1_remove_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:ContactUIService  = new ContactUIService();
        curUIService.Contact_Remove(datas,contextJO, paramJO,  $event, xData,this,"Contact");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem17_inactive_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:ContactUIService  = new ContactUIService();
        curUIService.Contact_Inactive(datas,contextJO, paramJO,  $event, xData,this,"Contact");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem17_active_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:ContactUIService  = new ContactUIService();
        curUIService.Contact_Active(datas,contextJO, paramJO,  $event, xData,this,"Contact");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem17_addlist_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:ContactUIService  = new ContactUIService();
        curUIService.Contact_AddList(datas,contextJO, paramJO,  $event, xData,this,"Contact");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem12_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        this.Exit(datas, contextJO,paramJO,  $event, xData,this,"Contact");
    }

    /**
     * 关闭
     *
     * @param {any[]} args 当前数据
     * @param {any} contextJO 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @memberof ContactInfoBase
     */
    public Exit(args: any[],contextJO?:any, params?: any, $event?: any, xData?: any,actionContext?:any,srfParentDeName?:string) {
        this.closeView(args);
        if(window.parent){
            window.parent.postMessage([{ ...args }],'*');
        }
    }


}
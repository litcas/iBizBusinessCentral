import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import UomService from '@/service/uom/uom-service';
import UomAuthService from '@/authservice/uom/uom-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import UomUIService from '@/uiservice/uom/uom-ui-service';

/**
 * 计价单位选择表格视图视图基类
 *
 * @export
 * @class UomPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class UomPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof UomPickupGridViewBase
     */
    protected appDeName: string = 'uom';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof UomPickupGridViewBase
     */
    protected appDeKey: string = 'uomid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof UomPickupGridViewBase
     */
    protected appDeMajor: string = 'uomname';

    /**
     * 实体服务对象
     *
     * @type {UomService}
     * @memberof UomPickupGridViewBase
     */
    protected appEntityService: UomService = new UomService;

    /**
     * 实体权限服务对象
     *
     * @type UomUIService
     * @memberof UomPickupGridViewBase
     */
    public appUIService: UomUIService = new UomUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof UomPickupGridViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof UomPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.uom.views.pickupgridview.caption',
        srfTitle: 'entities.uom.views.pickupgridview.title',
        srfSubTitle: 'entities.uom.views.pickupgridview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof UomPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: { name: 'grid', type: 'GRID' },
        view_searchform: { name: 'searchform', type: 'SEARCHFORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'b12ba9db37cce82d2d8f9e8149cc371a';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof UomPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof UomPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'uom',
            majorPSDEField: 'uomname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof UomPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof UomPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof UomPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof UomPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof UomPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof UomPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof UomPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof UomPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}
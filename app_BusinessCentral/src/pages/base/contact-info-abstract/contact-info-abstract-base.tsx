import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import ContactService from '@/service/contact/contact-service';
import ContactAuthService from '@/authservice/contact/contact-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import ContactUIService from '@/uiservice/contact/contact-ui-service';

/**
 * 联系人编辑视图视图基类
 *
 * @export
 * @class ContactInfo_AbstractBase
 * @extends {EditViewBase}
 */
export class ContactInfo_AbstractBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof ContactInfo_AbstractBase
     */
    protected appDeName: string = 'contact';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof ContactInfo_AbstractBase
     */
    protected appDeKey: string = 'contactid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof ContactInfo_AbstractBase
     */
    protected appDeMajor: string = 'fullname';

    /**
     * 实体服务对象
     *
     * @type {ContactService}
     * @memberof ContactInfo_AbstractBase
     */
    protected appEntityService: ContactService = new ContactService;

    /**
     * 实体权限服务对象
     *
     * @type ContactUIService
     * @memberof ContactInfo_AbstractBase
     */
    public appUIService: ContactUIService = new ContactUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof ContactInfo_AbstractBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof ContactInfo_AbstractBase
     */
    protected model: any = {
        srfCaption: 'entities.contact.views.info_abstract.caption',
        srfTitle: 'entities.contact.views.info_abstract.title',
        srfSubTitle: 'entities.contact.views.info_abstract.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof ContactInfo_AbstractBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '6e994c3354fcbeb06f40febdc0fc4c36';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof ContactInfo_AbstractBase
     */
    public engine: EditViewEngine = new EditViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof ContactInfo_AbstractBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'contact',
            majorPSDEField: 'fullname',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ContactInfo_AbstractBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ContactInfo_AbstractBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ContactInfo_AbstractBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}
import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { OptionViewBase } from '@/studio-core';
import AccountService from '@/service/account/account-service';
import AccountAuthService from '@/authservice/account/account-auth-service';
import OptionViewEngine from '@engine/view/option-view-engine';
import AccountUIService from '@/uiservice/account/account-ui-service';

/**
 * 快速新建视图基类
 *
 * @export
 * @class AccountQuickCreateBase
 * @extends {OptionViewBase}
 */
export class AccountQuickCreateBase extends OptionViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof AccountQuickCreateBase
     */
    protected appDeName: string = 'account';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof AccountQuickCreateBase
     */
    protected appDeKey: string = 'accountid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof AccountQuickCreateBase
     */
    protected appDeMajor: string = 'accountname';

    /**
     * 实体服务对象
     *
     * @type {AccountService}
     * @memberof AccountQuickCreateBase
     */
    protected appEntityService: AccountService = new AccountService;

    /**
     * 实体权限服务对象
     *
     * @type AccountUIService
     * @memberof AccountQuickCreateBase
     */
    public appUIService: AccountUIService = new AccountUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof AccountQuickCreateBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof AccountQuickCreateBase
     */
    protected model: any = {
        srfCaption: 'entities.account.views.quickcreate.caption',
        srfTitle: 'entities.account.views.quickcreate.title',
        srfSubTitle: 'entities.account.views.quickcreate.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof AccountQuickCreateBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'e30a2916b3054875db8d057cf4116df7';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof AccountQuickCreateBase
     */
    public engine: OptionViewEngine = new OptionViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof AccountQuickCreateBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'account',
            majorPSDEField: 'accountname',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof AccountQuickCreateBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof AccountQuickCreateBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof AccountQuickCreateBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}
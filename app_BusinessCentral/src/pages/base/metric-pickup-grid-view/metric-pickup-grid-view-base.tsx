import { Subject } from 'rxjs';
import { PickupGridViewBase } from '@/studio-core';
import MetricService from '@/service/metric/metric-service';
import MetricAuthService from '@/authservice/metric/metric-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import MetricUIService from '@/uiservice/metric/metric-ui-service';

/**
 * 目标度量选择表格视图视图基类
 *
 * @export
 * @class MetricPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class MetricPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MetricPickupGridViewBase
     */
    protected appDeName: string = 'metric';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof MetricPickupGridViewBase
     */
    protected appDeKey: string = 'metricid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof MetricPickupGridViewBase
     */
    protected appDeMajor: string = 'metricname';

    /**
     * 实体服务对象
     *
     * @type {MetricService}
     * @memberof MetricPickupGridViewBase
     */
    protected appEntityService: MetricService = new MetricService;

    /**
     * 实体权限服务对象
     *
     * @type MetricUIService
     * @memberof MetricPickupGridViewBase
     */
    public appUIService: MetricUIService = new MetricUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof MetricPickupGridViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof MetricPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.metric.views.pickupgridview.caption',
        srfTitle: 'entities.metric.views.pickupgridview.title',
        srfSubTitle: 'entities.metric.views.pickupgridview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof MetricPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: { name: 'grid', type: 'GRID' },
        view_searchform: { name: 'searchform', type: 'SEARCHFORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'a7e6b6721c505c18c29e33c83bcfbc4b';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof MetricPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof MetricPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'metric',
            majorPSDEField: 'metricname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof MetricPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof MetricPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof MetricPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof MetricPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof MetricPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof MetricPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof MetricPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof MetricPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}
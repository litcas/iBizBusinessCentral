import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PortalViewBase } from '@/studio-core';

/**
 * 设置视图基类
 *
 * @export
 * @class SettingBase
 * @extends {PortalViewBase}
 */
export class SettingBase extends PortalViewBase {


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof SettingBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof SettingBase
     */
    protected model: any = {
        srfCaption: 'app.views.setting.caption',
        srfTitle: 'app.views.setting.title',
        srfSubTitle: 'app.views.setting.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof SettingBase
     */
    protected containerModel: any = {
        view_dashboard: { name: 'dashboard', type: 'DASHBOARD' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '127c15f76b66ca89e790d974f63abb89';


    /**
     * 引擎初始化
     *
     * @public
     * @memberof SettingBase
     */
    public engineInit(): void {
    }




}
import { Subject } from 'rxjs';
import { PortalViewBase } from '@/studio-core';

/**
 * 仪表盘视图基类
 *
 * @export
 * @class CenteralPortalBase
 * @extends {PortalViewBase}
 */
export class CenteralPortalBase extends PortalViewBase {


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof CenteralPortalBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof CenteralPortalBase
     */
    protected model: any = {
        srfCaption: 'app.views.centeralportal.caption',
        srfTitle: 'app.views.centeralportal.title',
        srfSubTitle: 'app.views.centeralportal.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof CenteralPortalBase
     */
    protected containerModel: any = {
        view_dashboard: { name: 'dashboard', type: 'DASHBOARD' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '58199705a616a93d810b2730ae13bc8d';


    /**
     * 引擎初始化
     *
     * @public
     * @memberof CenteralPortalBase
     */
    public engineInit(): void {
    }




}
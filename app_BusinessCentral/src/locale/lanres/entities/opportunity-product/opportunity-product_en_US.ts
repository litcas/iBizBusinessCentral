
export default {
  fields: {
    priceoverridden: "自定义的价格",
    tax_base: "税 (Base)",
    skippricecalculation: "SkipPriceCalculation",
    volumediscountamount_base: "批发折扣 (Base)",
    manualdiscountamount: "零售折扣金额",
    entityimage_timestamp: "EntityImage_Timestamp",
    opportunitystatecode: "商机状态",
    ownertype: "负责人类型",
    volumediscountamount: "批发折扣",
    extendedamount_base: "应收净额 (Base)",
    ownername: "负责人",
    priceperunit_base: "单价 (Base)",
    opportunityproductid: "商机产品",
    versionnumber: "Version Number",
    createman: "建立人",
    entityimageid: "EntityImageId",
    description: "说明",
    manualdiscountamount_base: "零售折扣金额 (Base)",
    sequencenumber: "序号",
    productdescription: "目录外产品",
    productoverridden: "选择产品",
    productname: "Product Name",
    exchangerate: "汇率",
    opportunityproductname: "名称",
    propertyconfigurationstatus: "属性配置",
    tax: "税",
    priceperunit: "单价",
    parentbundleid: "父捆绑销售",
    utcconversiontimezonecode: "UTC Conversion Time Zone Code",
    productassociationid: "捆绑销售项关联",
    baseamount: "金额",
    producttypecode: "产品类型",
    updatedate: "更新时间",
    timezoneruleversionnumber: "Time Zone Rule Version Number",
    quantity: "数量",
    lineitemnumber: "明细项目编号",
    overriddencreatedon: "Record Created On",
    createdate: "建立时间",
    extendedamount: "应收净额",
    updateman: "更新人",
    entityimage_url: "EntityImage_URL",
    pricingerrorcode: "定价错误",
    entityimage: "实体图像",
    baseamount_base: "金额 (Base)",
    importsequencenumber: "Import Sequence Number",
    productid: "现有产品",
    parentbundleidref: "Parent bundle product",
    uomid: "计价单位",
    transactioncurrencyid: "货币",
    opportunityid: "商机",
  },
	views: {
		editview: {
			caption: "商机产品",
      		title: "商机产品编辑视图",
		},
		opp_oppprogridview: {
			caption: "商机产品",
      		title: "商机产品表格视图",
		},
	},
	main_form: {
		details: {
			group1: "opportunityproduct基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "商机产品", 
			srfmajortext: "名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			opportunityproductname: "名称", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			opportunityproductid: "商机产品", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			opportunityproductname: "名称",
			updateman: "更新人",
			updatedate: "更新时间",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	opp_oppprogridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem6: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "Save",
			tip: "Save",
		},
		tbitem4: {
			caption: "Save And New",
			tip: "Save And New",
		},
		tbitem5: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "Remove And Close",
			tip: "Remove And Close Window",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "New",
			tip: "New",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "Copy",
			tip: "Copy {0}",
		},
	},
};
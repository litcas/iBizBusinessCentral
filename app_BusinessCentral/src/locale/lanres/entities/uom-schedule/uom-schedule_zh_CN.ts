export default {
  fields: {
    importsequencenumber: "Import Sequence Number",
    timezoneruleversionnumber: "Time Zone Rule Version Number",
    createman: "建立人",
    description: "说明",
    baseuomname: "基础单位名称",
    updateman: "更新人",
    updatedate: "更新时间",
    versionnumber: "Version Number",
    createdate: "建立时间",
    utcconversiontimezonecode: "UTC Conversion Time Zone Code",
    statecode: "状态",
    statuscode: "状态描述",
    overriddencreatedon: "Record Created On",
    uomschedulename: "单位进度表名称",
    uomscheduleid: "计价单位组",
  },
	views: {
		pickupview: {
			caption: "计价单位组",
      		title: "计价单位组数据选择视图",
		},
		editview: {
			caption: "计价单位组",
      		title: "计价单位组",
		},
		pickupgridview: {
			caption: "计价单位组",
      		title: "计价单位组选择表格视图",
		},
		gridview: {
			caption: "计价单位组",
      		title: "计价单位组",
		},
	},
	main_form: {
		details: {
			group1: "uomschedule基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "计价单位组", 
			srfmajortext: "单位进度表名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			uomschedulename: "单位进度表名称", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			uomscheduleid: "计价单位组", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			uomschedulename: "单位进度表名称",
			updateman: "更新人",
			updatedate: "更新时间",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem6: {
			caption: "拷贝",
			tip: "拷贝",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "保存",
			tip: "保存",
		},
		tbitem4: {
			caption: "保存并新建",
			tip: "保存并新建",
		},
		tbitem5: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "删除并关闭",
			tip: "删除并关闭",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "新建",
			tip: "新建",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "拷贝",
			tip: "拷贝",
		},
	},
};
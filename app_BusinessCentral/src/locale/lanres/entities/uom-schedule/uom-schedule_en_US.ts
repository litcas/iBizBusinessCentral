
export default {
  fields: {
    importsequencenumber: "Import Sequence Number",
    timezoneruleversionnumber: "Time Zone Rule Version Number",
    createman: "建立人",
    description: "说明",
    baseuomname: "基础单位名称",
    updateman: "更新人",
    updatedate: "更新时间",
    versionnumber: "Version Number",
    createdate: "建立时间",
    utcconversiontimezonecode: "UTC Conversion Time Zone Code",
    statecode: "状态",
    statuscode: "状态描述",
    overriddencreatedon: "Record Created On",
    uomschedulename: "单位进度表名称",
    uomscheduleid: "计价单位组",
  },
	views: {
		pickupview: {
			caption: "计价单位组",
      		title: "计价单位组数据选择视图",
		},
		editview: {
			caption: "计价单位组",
      		title: "计价单位组",
		},
		pickupgridview: {
			caption: "计价单位组",
      		title: "计价单位组选择表格视图",
		},
		gridview: {
			caption: "计价单位组",
      		title: "计价单位组",
		},
	},
	main_form: {
		details: {
			group1: "uomschedule基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "计价单位组", 
			srfmajortext: "单位进度表名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			uomschedulename: "单位进度表名称", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			uomscheduleid: "计价单位组", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			uomschedulename: "单位进度表名称",
			updateman: "更新人",
			updatedate: "更新时间",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem6: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "Save",
			tip: "Save",
		},
		tbitem4: {
			caption: "Save And New",
			tip: "Save And New",
		},
		tbitem5: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "Remove And Close",
			tip: "Remove And Close Window",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "New",
			tip: "New",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "Copy",
			tip: "Copy {0}",
		},
	},
};
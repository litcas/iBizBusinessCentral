export default {
  fields: {
    createman: "建立人",
    statuscode: "状态描述",
    amount: "度量类型",
    timezoneruleversionnumber: "时区规则版本号",
    updateman: "更新人",
    description: "说明",
    utcconversiontimezonecode: "UTC 转换时区代码",
    statecode: "状态",
    overriddencreatedon: "创建记录的时间",
    metricname: "公制名称",
    importsequencenumber: "导入序列号",
    metricid: "目标度量",
    amountdatatype: "金额数据类型",
    createdate: "建立时间",
    updatedate: "更新时间",
    versionnumber: "版本号",
    stretchtracked: "跟踪扩展目标值",
  },
	views: {
		gridview: {
			caption: "目标度量",
      		title: "目标度量信息",
		},
		pickupview: {
			caption: "目标度量",
      		title: "目标度量数据选择视图",
		},
		pickupgridview: {
			caption: "目标度量",
      		title: "目标度量选择表格视图",
		},
		editview: {
			caption: "目标度量",
      		title: "目标度量信息",
		},
	},
	main_form: {
		details: {
			group1: "metric基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "目标度量", 
			srfmajortext: "公制名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			metricname: "公制名称", 
			amount: "度量类型", 
			amountdatatype: "金额数据类型", 
			stretchtracked: "跟踪扩展目标值", 
			description: "说明", 
			metricid: "目标度量", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			metricname: "公制名称",
			updateman: "更新人",
			updatedate: "更新时间",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem6: {
			caption: "拷贝",
			tip: "拷贝",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "保存",
			tip: "保存",
		},
		tbitem4: {
			caption: "保存并新建",
			tip: "保存并新建",
		},
		tbitem5: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "删除并关闭",
			tip: "删除并关闭",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "新建",
			tip: "新建",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "拷贝",
			tip: "拷贝",
		},
	},
};
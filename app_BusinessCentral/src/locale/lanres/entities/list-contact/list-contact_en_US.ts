
export default {
  fields: {
    relationshipsid: "关系标识",
    updatedate: "更新时间",
    createdate: "建立时间",
    relationshipsname: "关系名称",
    updateman: "更新人",
    relationshipstype: "关系类型",
    createman: "建立人",
    emailaddress1: "电子邮件",
    statecode: "状态",
    entity2name: "联系人",
    parentcustomerid: "公司名称",
    entityname: "营销列表",
    telephone1: "商务电话",
    entity2id: "联系人",
    entityid: "列表",
  },
	views: {
		inner: {
			caption: "联系人",
      		title: "营销列表-联系人表格视图",
		},
		editview: {
			caption: "营销列表-联系人",
      		title: "营销列表-联系人编辑视图",
		},
		gridview: {
			caption: "营销列表-联系人",
      		title: "营销列表-联系人表格视图",
		},
	},
	main_form: {
		details: {
			group1: "营销列表-联系人基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "关系标识", 
			srfmajortext: "关系名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			relationshipsid: "关系标识", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			entity2name: "联系人",
			emailaddress1: "电子邮件",
			parentcustomerid: "公司名称",
			telephone1: "商务电话",
			statecode: "状态",
		},
		uiactions: {
		},
	},
	inner_grid: {
		columns: {
			entity2name: "联系人",
			emailaddress1: "电子邮件",
			parentcustomerid: "公司名称",
			telephone1: "商务电话",
			statecode: "状态",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem6: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem12: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	innertoolbar_toolbar: {
	},
};
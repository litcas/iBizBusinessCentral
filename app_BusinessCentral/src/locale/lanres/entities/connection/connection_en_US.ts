
export default {
  fields: {
    record2objecttypecode: "类型(目标)",
    updatedate: "更新时间",
    entityimage_timestamp: "EntityImage_Timestamp",
    createdate: "建立时间",
    createman: "建立人",
    record1objecttypecode: "类型(源)",
    record2id: "已连接到",
    master: "主记录",
    record1idobjecttypecode: "连接自",
    ownerid: "负责人",
    updateman: "更新人",
    effectivestart: "正在启动",
    record1id: "连接自",
    overriddencreatedon: "创建记录的时间",
    importsequencenumber: "导入序列号",
    ownername: "负责人",
    ownertype: "负责人类型",
    record2idobjecttypecode: "已连接到",
    exchangerate: "汇率",
    entityimageid: "实体图像 ID",
    versionnumber: "版本号",
    effectiveend: "正在结束",
    entityimage_url: "EntityImage_URL",
    connectionname: "关联名称",
    statuscode: "状态描述",
    connectionid: "连接",
    statecode: "状态",
    description: "说明",
    entityimage: "实体图像",
    record1rolename: "角色",
    record2rolename: "角色（目标）",
    record1roleid: "角色(源)",
    transactioncurrencyid: "货币",
    record2roleid: "角色(目标)",
  },
	views: {
		byparentkey: {
			caption: "连接",
      		title: "连接表格视图",
		},
		editview: {
			caption: "连接",
      		title: "连接编辑视图",
		},
	},
	main_form: {
		details: {
			group1: "connection基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "连接", 
			srfmajortext: "关联名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			connectionname: "关联名称", 
			record1id: "连接自", 
			record1rolename: "角色", 
			record2rolename: "角色（目标）", 
			record2id: "已连接到", 
			record2idobjecttypecode: "已连接到", 
			record2objecttypecode: "类型(目标)", 
			statecode: "状态", 
			statuscode: "状态描述", 
			effectiveend: "正在结束", 
			effectivestart: "正在启动", 
			description: "说明", 
			ownerid: "负责人", 
			ownertype: "负责人类型", 
			ownername: "负责人", 
			connectionid: "连接", 
			record1roleid: "角色(源)", 
			record2roleid: "角色(目标)", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			record1rolename: "角色",
			record2idobjecttypecode: "已连接到",
			statecode: "状态",
			statuscode: "状态描述",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	byparentkeytoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem6: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "Save",
			tip: "Save",
		},
		tbitem4: {
			caption: "Save And New",
			tip: "Save And New",
		},
		tbitem5: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "Remove And Close",
			tip: "Remove And Close Window",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "New",
			tip: "New",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "Copy",
			tip: "Copy {0}",
		},
	},
};
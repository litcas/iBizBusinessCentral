export default {
  fields: {
    createdate: "建立时间",
    relationshipsname: "关系名称",
    relationshipstype: "关系类型",
    createman: "建立人",
    updatedate: "更新时间",
    relationshipsid: "关系标识",
    updateman: "更新人",
    statecode: "状态",
    primarycontactid: "主要联系人",
    telephone1: "主要电话",
    primarycontactname: "主要联系人",
    entityname: "营销列表",
    entity2name: "选择客户：",
    entityid: "列表",
    entity2id: "客户",
  },
	views: {
		inner: {
			caption: "营销列表-账户",
      		title: "营销列表-账户表格视图",
		},
		quickcreatebylist: {
			caption: "查找客户",
      		title: "查找客户",
		},
		bylist: {
			caption: "营销列表-账户",
      		title: "营销列表-账户表格视图",
		},
		editview: {
			caption: "营销列表-账户",
      		title: "营销列表-账户编辑视图",
		},
	},
	quickcreatebylist_form: {
		details: {
			group1: "营销列表-账户基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "关系标识", 
			srfmajortext: "关系名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			entity2name: "选择客户：", 
			entity2id: "客户", 
			relationshipsid: "关系标识", 
		},
		uiactions: {
		},
	},
	main_form: {
		details: {
			group1: "营销列表-账户基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "关系标识", 
			srfmajortext: "关系名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			relationshipsid: "关系标识", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			entity2name: "选择客户：",
			telephone1: "主要电话",
			primarycontactname: "主要联系人",
			statecode: "状态",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	bylisttoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem6: {
			caption: "拷贝",
			tip: "拷贝",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem12: {
			caption: "关闭",
			tip: "关闭",
		},
	},
};
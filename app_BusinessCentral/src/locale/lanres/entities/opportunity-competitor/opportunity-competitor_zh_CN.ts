export default {
  fields: {
    updatedate: "更新时间",
    createman: "建立人",
    createdate: "建立时间",
    relationshipsid: "关系标识",
    updateman: "更新人",
    relationshipstype: "关系类型",
    relationshipsname: "关系名称",
    originatingleadid: "潜在顾客",
    entityname: "商机",
    estimatedvalue: "预计收入",
    originatingleadname: "潜在顾客",
    estimatedclosedate: "预计结束日期",
    actualvalue: "实际收入",
    statecode: "状态",
    entity2name: "竞争对手",
    entity2id: "竞争对手",
    entityid: "商机",
  },
	views: {
		oppcompgridview: {
			caption: "商机对手",
      		title: "商机对手表格视图",
		},
		editview: {
			caption: "商机对手",
      		title: "商机对手编辑视图",
		},
		edit_compoppview: {
			caption: "商机对手",
      		title: "竞争对手商机信息",
		},
		compoppgridview: {
			caption: "商机对手",
      		title: "竞争对手商机信息",
		},
	},
	main_form: {
		details: {
			group1: "商机对手基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "关系标识", 
			srfmajortext: "关系名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			entityid: "商机", 
			entity2name: "竞争对手", 
			estimatedvalue: "预计收入", 
			estimatedclosedate: "预计结束日期", 
			statecode: "状态", 
			entity2id: "竞争对手", 
			relationshipsid: "关系标识", 
		},
		uiactions: {
		},
	},
	edit_compopp_form: {
		details: {
			group1: "商机对手基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "关系标识", 
			srfmajortext: "关系名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			entity2id: "竞争对手", 
			entityname: "商机", 
			estimatedvalue: "预计收入", 
			estimatedclosedate: "预计结束日期", 
			statecode: "状态", 
			relationshipsid: "关系标识", 
			entityid: "商机", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			entityname: "商机",
			entity2name: "竞争对手",
			originatingleadname: "潜在顾客",
			estimatedvalue: "预计收入",
			estimatedclosedate: "预计结束日期",
			actualvalue: "实际收入",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	oppcompgridviewtoolbar_toolbar: {
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem6: {
			caption: "拷贝",
			tip: "拷贝",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem12: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	compoppgridviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "新建",
			tip: "新建",
		},
		tbitem2: {
			caption: "-",
			tip: "",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	edit_compoppviewtoolbar_toolbar: {
		tbitem1: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
};

export default {
  fields: {
    updateman: "更新人",
    createdate: "建立时间",
    relationshipstype: "关系类型",
    relationshipsid: "关系标识",
    createman: "建立人",
    updatedate: "更新时间",
    relationshipsname: "关系名称",
    entityname: "营销列表",
    ownerid: "负责人",
    entity2name: "潜在顾客",
    subject: "主题",
    ownername: "负责人",
    statuscode: "状态描述",
    entityid: "列表",
    entity2id: "潜在顾客",
  },
	views: {
		quickcreatebylist: {
			caption: "快速新建",
      		title: "快速新建（根据营销列表）",
		},
		bylist: {
			caption: "潜在客户",
      		title: "营销列表-潜在客户（根据营销列表）",
		},
	},
	quickcreatebylist_form: {
		details: {
			group1: "营销列表-潜在客户基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "关系标识", 
			srfmajortext: "关系名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			entity2name: "潜在顾客", 
			entity2id: "潜在顾客", 
			relationshipsid: "关系标识", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			entity2name: "潜在顾客",
			subject: "主题",
			ownername: "负责人",
			statuscode: "状态描述",
		},
		uiactions: {
		},
	},
	bylisttoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem6: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
	},
};
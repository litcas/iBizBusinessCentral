
export default {
  fields: {
    createdate: "建立时间",
    omhierarchyid: "组织层次结构标识",
    omhierarchyname: "组织层次结构名称",
    updatedate: "更新时间",
    updateman: "更新人",
    createman: "建立人",
    organizationid: "组织",
    pomhierarchyid: "父组织层次结构标识",
    omhierarchycatid: "结构层次类别标识",
    valid: "是否有效",
    displayname: "显示名称",
    sn: "序号",
    codevalue: "代码",
    showorder: "排序号",
    shortname: "组织简称",
    organizationname: "组织",
    omhierarchycatname: "层次类别",
    pomhierarchyname: "上级组织",
  },
	views: {
		gridview: {
			caption: "组织层次结构",
      		title: "组织层次结构表格视图",
		},
		editview: {
			caption: "组织层次结构",
      		title: "组织层次结构编辑视图",
		},
		treeexpview: {
			caption: "组织层级结构",
      		title: "组织层次结构树导航视图",
		},
	},
	main_form: {
		details: {
			group1: "组织层次结构基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "组织层次结构标识", 
			srfmajortext: "组织层次结构名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			omhierarchyid: "组织层次结构标识", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			organizationname: "组织",
			shortname: "组织简称",
			pomhierarchyname: "上级组织",
			omhierarchycatname: "层次类别",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem6: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "Save",
			tip: "Save",
		},
		tbitem4: {
			caption: "Save And New",
			tip: "Save And New",
		},
		tbitem5: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "Remove And Close",
			tip: "Remove And Close Window",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "New",
			tip: "New",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "Copy",
			tip: "Copy {0}",
		},
	},
	tree_001_treeview: {
		nodes: {
			root: "全部组织",
		},
		uiactions: {
		},
	},
};
export default {
  fields: {
    id: "组标识",
    name: "组名称",
    groupscope: "范围",
  },
	views: {
		pickupgridview: {
			caption: "角色/用户组",
      		title: "角色/用户组选择表格视图",
		},
		editview: {
			caption: "流程角色",
      		title: "流程角色",
		},
		pickupview: {
			caption: "角色/用户组",
      		title: "角色/用户组数据选择视图",
		},
		gridview: {
			caption: "流程角色",
      		title: "流程角色",
		},
	},
	main_form: {
		details: {
			druipart1: "成员", 
			group1: "角色/用户组基本信息", 
			formpage1: "基本信息", 
			srforikey: "", 
			srfkey: "组标识", 
			srfmajortext: "组名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			groupname: "组名称", 
			groupscope: "范围", 
			groupid: "组标识", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			groupid: "组标识",
			groupname: "组名称",
			groupscope: "范围",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
			n_groupname_like: "组名称(文本包含(%))", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem6: {
			caption: "拷贝",
			tip: "拷贝",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "保存",
			tip: "保存",
		},
		tbitem4: {
			caption: "保存并新建",
			tip: "保存并新建",
		},
		tbitem5: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "删除并关闭",
			tip: "删除并关闭",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "新建",
			tip: "新建",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "拷贝",
			tip: "拷贝",
		},
	},
};
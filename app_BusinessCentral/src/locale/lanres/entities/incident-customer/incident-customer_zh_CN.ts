export default {
  fields: {
    customerid: "客户",
    businesstypecode: "商业类型",
    customertype: "客户类型",
    customername: "客户",
  },
	views: {
		pickupgridview: {
			caption: "案例客户",
      		title: "案例客户选择表格视图",
		},
		indexpickupview: {
			caption: "案例客户",
      		title: "案例客户数据选择视图",
		},
		pickupview: {
			caption: "客户",
      		title: "客户",
		},
		gridview: {
			caption: "案例客户",
      		title: "案例客户表格视图",
		},
		editview: {
			caption: "案例客户",
      		title: "案例客户编辑视图",
		},
		mpickupview: {
			caption: "案例客户",
      		title: "案例客户数据多项选择视图",
		},
		indexpickupdataview: {
			caption: "案例客户",
      		title: "案例客户索引关系选择数据视图",
		},
	},
	main_form: {
		details: {
			group1: "案例客户基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srforikey: "", 
			srfkey: "客户", 
			srfmajortext: "客户", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			businesstypecode: "商业类型", 
			customerid: "客户", 
			customername: "客户", 
			customertype: "客户类型", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			customername: "客户",
			customertype: "客户类型",
			businesstypecode: "商业类型",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
			n_customertype_eq: "客户类型(等于(=))", 
			n_customername_like: "客户(文本包含(%))", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem6: {
			caption: "拷贝",
			tip: "拷贝",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "保存",
			tip: "保存",
		},
		tbitem4: {
			caption: "保存并新建",
			tip: "保存并新建",
		},
		tbitem5: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "删除并关闭",
			tip: "删除并关闭",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "新建",
			tip: "新建",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "拷贝",
			tip: "拷贝",
		},
	},
};
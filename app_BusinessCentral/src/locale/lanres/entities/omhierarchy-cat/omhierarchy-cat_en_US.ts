
export default {
  fields: {
    updatedate: "更新时间",
    updateman: "更新人",
    omhierarchycatname: " 结构层次类别名称",
    createman: "建立人",
    omhierarchycatid: " 结构层次类别标识",
    createdate: "建立时间",
    memo: "备注",
  },
	views: {
		editview: {
			caption: "结构层次类别",
      		title: "实体编辑视图",
		},
		gridview: {
			caption: "层次类别",
      		title: "实体表格视图",
		},
		v_002: {
			caption: "结构层次类别",
      		title: "V_002",
		},
		dashboardview: {
			caption: "结构层次类别",
      		title: "结构层次类别看板",
		},
	},
	main_form: {
		details: {
			group1: "实体基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: " 结构层次类别标识", 
			srfmajortext: " 结构层次类别名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			omhierarchycatid: " 结构层次类别标识", 
		},
		uiactions: {
		},
	},
	ef_001_form: {
		details: {
			group1: "用途", 
			druipart1: "", 
			grouppanel1: "层次结构", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: " 结构层次类别标识", 
			srfmajortext: " 结构层次类别名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			omhierarchycatname: "用途", 
			memo: "备注", 
			omhierarchycatid: " 结构层次类别标识", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			omhierarchycatname: "用途",
			memo: "备注",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "Save",
			tip: "Save",
		},
		tbitem4: {
			caption: "Save And New",
			tip: "Save And New",
		},
		tbitem5: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "Remove And Close",
			tip: "Remove And Close Window",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "New",
			tip: "New",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "Copy",
			tip: "Copy {0}",
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem6: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
};
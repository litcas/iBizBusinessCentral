export default {
  fields: {
    updatedate: "更新时间",
    updateman: "更新人",
    omhierarchycatname: " 结构层次类别名称",
    createman: "建立人",
    omhierarchycatid: " 结构层次类别标识",
    createdate: "建立时间",
    memo: "备注",
  },
	views: {
		editview: {
			caption: "结构层次类别",
      		title: "实体编辑视图",
		},
		gridview: {
			caption: "层次类别",
      		title: "实体表格视图",
		},
		v_002: {
			caption: "结构层次类别",
      		title: "V_002",
		},
		dashboardview: {
			caption: "结构层次类别",
      		title: "结构层次类别看板",
		},
	},
	main_form: {
		details: {
			group1: "实体基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: " 结构层次类别标识", 
			srfmajortext: " 结构层次类别名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			omhierarchycatid: " 结构层次类别标识", 
		},
		uiactions: {
		},
	},
	ef_001_form: {
		details: {
			group1: "用途", 
			druipart1: "", 
			grouppanel1: "层次结构", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: " 结构层次类别标识", 
			srfmajortext: " 结构层次类别名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			omhierarchycatname: "用途", 
			memo: "备注", 
			omhierarchycatid: " 结构层次类别标识", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			omhierarchycatname: "用途",
			memo: "备注",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "保存",
			tip: "保存",
		},
		tbitem4: {
			caption: "保存并新建",
			tip: "保存并新建",
		},
		tbitem5: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "删除并关闭",
			tip: "删除并关闭",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "新建",
			tip: "新建",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "拷贝",
			tip: "拷贝",
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem6: {
			caption: "拷贝",
			tip: "拷贝",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
};
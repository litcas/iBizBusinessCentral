
export default {
  fields: {
    overriddencreatedon: "Record Created On",
    baseuomname: "BaseUoMName",
    updateman: "更新人",
    createman: "建立人",
    versionnumber: "Version Number",
    quantity: "数量",
    uomid: "计价单位",
    schedulebaseuom: "为计划基础单位",
    organizationid: "组织",
    createdate: "建立时间",
    updatedate: "更新时间",
    uomname: "计量单位名称",
    utcconversiontimezonecode: "UTC Conversion Time Zone Code",
    timezoneruleversionnumber: "Time Zone Rule Version Number",
    importsequencenumber: "Import Sequence Number",
    uomscheduleid: "单位计划",
    baseuom: "基础单位",
  },
	views: {
		editview: {
			caption: "计价单位",
      		title: "计价单位",
		},
		gridview: {
			caption: "计价单位",
      		title: "计价单位",
		},
		pickupview: {
			caption: "计价单位",
      		title: "计价单位数据选择视图",
		},
		pickupgridview: {
			caption: "计价单位",
      		title: "计价单位选择表格视图",
		},
	},
	main_form: {
		details: {
			group1: "uom基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "计价单位", 
			srfmajortext: "计量单位名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			uomname: "计量单位名称", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			uomid: "计价单位", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			uomname: "计量单位名称",
			updateman: "更新人",
			updatedate: "更新时间",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem6: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "Save",
			tip: "Save",
		},
		tbitem4: {
			caption: "Save And New",
			tip: "Save And New",
		},
		tbitem5: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "Remove And Close",
			tip: "Remove And Close Window",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "New",
			tip: "New",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "Copy",
			tip: "Copy {0}",
		},
	},
};
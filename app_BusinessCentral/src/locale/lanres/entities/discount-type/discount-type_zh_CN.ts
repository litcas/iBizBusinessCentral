export default {
  fields: {
    timezoneruleversionnumber: "Time Zone Rule Version Number",
    statuscode: "状态描述",
    discounttypeid: "折扣表",
    createdate: "建立时间",
    updateman: "更新人",
    amounttype: "类型",
    versionnumber: "Version Number",
    createman: "建立人",
    discounttypename: "折扣类型名称",
    updatedate: "更新时间",
    description: "说明",
    utcconversiontimezonecode: "UTC Conversion Time Zone Code",
    overriddencreatedon: "Record Created On",
    importsequencenumber: "Import Sequence Number",
    statecode: "状态",
    transactioncurrencyid: "货币",
  },
	views: {
		editview: {
			caption: "折扣表",
      		title: "折扣表",
		},
		gridview: {
			caption: "折扣表",
      		title: "折扣表",
		},
		pickupgridview: {
			caption: "折扣表",
      		title: "折扣表选择表格视图",
		},
		pickupview: {
			caption: "折扣表",
      		title: "折扣表数据选择视图",
		},
	},
	main_form: {
		details: {
			group1: "discounttype基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "折扣表", 
			srfmajortext: "折扣类型名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			discounttypename: "折扣类型名称", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			discounttypeid: "折扣表", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			discounttypename: "折扣类型名称",
			updateman: "更新人",
			updatedate: "更新时间",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "保存",
			tip: "保存",
		},
		tbitem4: {
			caption: "保存并新建",
			tip: "保存并新建",
		},
		tbitem5: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "删除并关闭",
			tip: "删除并关闭",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "新建",
			tip: "新建",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "拷贝",
			tip: "拷贝",
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem6: {
			caption: "拷贝",
			tip: "拷贝",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
};
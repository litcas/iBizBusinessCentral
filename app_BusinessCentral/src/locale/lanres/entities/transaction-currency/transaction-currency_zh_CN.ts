export default {
  fields: {
    updatedate: "更新时间",
    entityimageid: "实体图像 ID",
    transactioncurrencyid: "交易币种",
    versionnumber: "版本号",
    entityimage_url: "EntityImage_URL",
    exchangerate: "汇率",
    overriddencreatedon: "创建记录的时间",
    createdate: "建立时间",
    uniquedscid: "UniqueDscId",
    statecode: "状态",
    statuscode: "状态描述",
    updateman: "更新人",
    currencyprecision: "货币精度",
    currencyname: "货币名称",
    createman: "建立人",
    entityimage: "实体图像",
    isocurrencycode: "货币代码",
    entityimage_timestamp: "EntityImage_Timestamp",
    currencysymbol: "货币符号",
    importsequencenumber: "导入序列号",
  },
	views: {
		pickupgridview: {
			caption: "货币",
      		title: "货币选择表格视图",
		},
		editview: {
			caption: "货币",
      		title: "货币",
		},
		gridview: {
			caption: "货币",
      		title: "货币",
		},
		pickupview: {
			caption: "货币",
      		title: "货币数据选择视图",
		},
	},
	main_form: {
		details: {
			group1: "货币基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "交易币种", 
			srfmajortext: "货币名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			currencyname: "货币名称", 
			currencyprecision: "货币精度", 
			currencysymbol: "货币符号", 
			exchangerate: "汇率", 
			statecode: "状态", 
			statuscode: "状态描述", 
			isocurrencycode: "货币代码", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			transactioncurrencyid: "交易币种", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			currencyname: "货币名称",
			currencyprecision: "货币精度",
			currencysymbol: "货币符号",
			exchangerate: "汇率",
			isocurrencycode: "货币代码",
			statecode: "状态",
			statuscode: "状态描述",
			updatedate: "更新时间",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem6: {
			caption: "拷贝",
			tip: "拷贝",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "保存",
			tip: "保存",
		},
		tbitem4: {
			caption: "保存并新建",
			tip: "保存并新建",
		},
		tbitem5: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "删除并关闭",
			tip: "删除并关闭",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "新建",
			tip: "新建",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "拷贝",
			tip: "拷贝",
		},
	},
};
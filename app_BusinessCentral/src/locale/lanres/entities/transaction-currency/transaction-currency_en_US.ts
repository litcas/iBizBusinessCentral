
export default {
  fields: {
    updatedate: "更新时间",
    entityimageid: "实体图像 ID",
    transactioncurrencyid: "交易币种",
    versionnumber: "版本号",
    entityimage_url: "EntityImage_URL",
    exchangerate: "汇率",
    overriddencreatedon: "创建记录的时间",
    createdate: "建立时间",
    uniquedscid: "UniqueDscId",
    statecode: "状态",
    statuscode: "状态描述",
    updateman: "更新人",
    currencyprecision: "货币精度",
    currencyname: "货币名称",
    createman: "建立人",
    entityimage: "实体图像",
    isocurrencycode: "货币代码",
    entityimage_timestamp: "EntityImage_Timestamp",
    currencysymbol: "货币符号",
    importsequencenumber: "导入序列号",
  },
	views: {
		pickupgridview: {
			caption: "货币",
      		title: "货币选择表格视图",
		},
		editview: {
			caption: "货币",
      		title: "货币",
		},
		gridview: {
			caption: "货币",
      		title: "货币",
		},
		pickupview: {
			caption: "货币",
      		title: "货币数据选择视图",
		},
	},
	main_form: {
		details: {
			group1: "货币基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "交易币种", 
			srfmajortext: "货币名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			currencyname: "货币名称", 
			currencyprecision: "货币精度", 
			currencysymbol: "货币符号", 
			exchangerate: "汇率", 
			statecode: "状态", 
			statuscode: "状态描述", 
			isocurrencycode: "货币代码", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			transactioncurrencyid: "交易币种", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			currencyname: "货币名称",
			currencyprecision: "货币精度",
			currencysymbol: "货币符号",
			exchangerate: "汇率",
			isocurrencycode: "货币代码",
			statecode: "状态",
			statuscode: "状态描述",
			updatedate: "更新时间",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem6: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "Save",
			tip: "Save",
		},
		tbitem4: {
			caption: "Save And New",
			tip: "Save And New",
		},
		tbitem5: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "Remove And Close",
			tip: "Remove And Close Window",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "New",
			tip: "New",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "Copy",
			tip: "Copy {0}",
		},
	},
};
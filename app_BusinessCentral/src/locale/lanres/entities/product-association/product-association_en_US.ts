
export default {
  fields: {
    updatedate: "更新时间",
    quantity: "数量",
    updateman: "更新人",
    utcconversiontimezonecode: "UTC Conversion Time Zone Code",
    versionnumber: "Version Number",
    propertycustomizationstatus: "属性自定义",
    importsequencenumber: "Import Sequence Number",
    timezoneruleversionnumber: "Time Zone Rule Version Number",
    productisrequired: "必填",
    dmtimportstate: "仅供内部使用",
    statuscode: "状态描述",
    productassociationid: "产品关联 ID",
    exchangerate: "汇率",
    overriddencreatedon: "Record Created On",
    createdate: "建立时间",
    createman: "建立人",
    statecode: "状态",
    uomname: "计量单位",
    associatedproductname: "关联的产品",
    currencyname: "货币名称",
    productname: "产品名称",
    productid: "产品",
    uomid: "计价单位",
    transactioncurrencyid: "货币",
    associatedproduct: "关联的产品",
  },
	views: {
		gridview: {
			caption: "产品关联",
      		title: "产品关联表格视图",
		},
		editview: {
			caption: "产品关联",
      		title: "产品关联编辑视图",
		},
	},
	main_form: {
		details: {
			group1: "productassociation基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "产品关联 ID", 
			srfmajortext: "产品名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			productid: "产品", 
			associatedproductname: "关联的产品", 
			quantity: "数量", 
			uomname: "计量单位", 
			associatedproduct: "关联的产品", 
			productassociationid: "产品关联 ID", 
			uomid: "计价单位", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			associatedproductname: "关联的产品",
			quantity: "数量",
			uomname: "计量单位",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "New",
			tip: "New",
		},
		tbitem2: {
			caption: "-",
			tip: "",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
};
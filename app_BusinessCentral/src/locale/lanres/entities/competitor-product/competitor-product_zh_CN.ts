export default {
  fields: {
    relationshipsid: "关系标识",
    updateman: "更新人",
    createman: "建立人",
    relationshipsname: "关系名称",
    updatedate: "更新时间",
    relationshipstype: "关系类型",
    createdate: "建立时间",
    entityname: "竞争对手",
    productnumber: "产品ID",
    entity2name: "产品",
    entityid: "竞争对手",
    entity2id: "产品",
  },
	views: {
		compprogridview: {
			caption: "竞争对手产品",
      		title: "竞争对手产品信息",
		},
		edit_compproview: {
			caption: "竞争对手产品",
      		title: "竞争对手产品信息",
		},
	},
	edit_comppro_form: {
		details: {
			group1: "竞争对手产品基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "关系标识", 
			srfmajortext: "关系名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			entityid: "竞争对手", 
			entity2name: "产品", 
			productnumber: "产品ID", 
			entity2id: "产品", 
			relationshipsid: "关系标识", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			entityname: "竞争对手",
			entity2name: "产品",
			productnumber: "产品ID",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	compprogridviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "新建",
			tip: "新建",
		},
		tbitem2: {
			caption: "-",
			tip: "",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	edit_compproviewtoolbar_toolbar: {
		tbitem1: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
};
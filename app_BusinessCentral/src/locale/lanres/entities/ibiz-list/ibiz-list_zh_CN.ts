export default {
  fields: {
    query: "查询",
    updateman: "更新人",
    lockstatus: "已锁定",
    purpose: "用途",
    lastusedon: "上次使用时间",
    ownerid: "负责人",
    ownername: "负责人",
    listid: "列表",
    membertype: "目标对象",
    donotsendonoptout: "排除退出的成员",
    versionnumber: "Version Number",
    cost: "成本",
    statecode: "状态",
    exchangerate: "汇率",
    ownertype: "负责人类型",
    listname: "名称",
    statuscode: "状态描述",
    description: "说明",
    type: "类型",
    createdate: "建立时间",
    importsequencenumber: "Import Sequence Number",
    createman: "建立人",
    processid: "Process Id",
    cost_base: "成本 (Base)",
    membercount: "成员计数",
    ignoreinactivelistmembers: "忽略停用列表成员",
    stageid: "Stage Id",
    source: "来源",
    updatedate: "更新时间",
    utcconversiontimezonecode: "UTC Conversion Time Zone Code",
    createdfromcode: "成员类型",
    overriddencreatedon: "Record Created On",
    timezoneruleversionnumber: "Time Zone Rule Version Number",
    traversedpath: "Traversed Path",
    currencyname: "货币",
    transactioncurrencyid: "货币",
  },
	views: {
		info: {
			caption: "市场营销列表",
      		title: "市场营销列表",
		},
		statetabview: {
			caption: "市场营销列表",
      		title: "市场营销列表状态分页视图",
		},
		edit_datapanelview: {
			caption: "市场营销列表",
      		title: "头部信息编辑",
		},
		summary: {
			caption: "市场营销列表概览",
      		title: "市场营销列表概览",
		},
		editview: {
			caption: "市场营销列表",
      		title: "市场营销列表编辑视图",
		},
		info_abstract: {
			caption: "市场营销列表",
      		title: "市场营销列表编辑视图",
		},
		optionview: {
			caption: "摘要信息",
      		title: "市场营销列表选项操作视图",
		},
		quickcreate: {
			caption: "快速新建",
      		title: "快速新建",
		},
		stopgridview: {
			caption: "市场营销列表",
      		title: "市场营销列表信息",
		},
		gridview: {
			caption: "市场营销列表",
      		title: "市场营销列表信息",
		},
		effectivegridview: {
			caption: "市场营销列表",
      		title: "市场营销列表信息",
		},
	},
	abstractinfo_form: {
		details: {
			group1: "列表信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "列表", 
			srfmajortext: "名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			listname: "名称", 
			type: "类型", 
			purpose: "用途", 
			membertype: "目标对象", 
			source: "来源", 
			transactioncurrencyname: "货币", 
			cost: "成本", 
			ownername: "负责人", 
			description: "说明", 
			listid: "列表", 
		},
		uiactions: {
		},
	},
	abstractedit_form: {
		details: {
			group1: "市场营销列表基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "列表", 
			srfmajortext: "名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			listname: "名称", 
			type: "类型", 
			purpose: "用途", 
			membertype: "目标对象", 
			source: "来源", 
			transactioncurrencyname: "货币", 
			cost: "成本", 
			ownername: "负责人", 
			description: "说明", 
			transactioncurrencyid: "货币", 
			listid: "列表", 
		},
		uiactions: {
		},
	},
	edit_main_form: {
		details: {
			group1: "列表信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "列表", 
			srfmajortext: "名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			listname: "名称", 
			type: "类型", 
			purpose: "用途", 
			membertype: "目标对象", 
			source: "来源", 
			transactioncurrencyname: "货币", 
			cost: "成本", 
			ownername: "负责人", 
			description: "说明", 
			transactioncurrencyid: "货币", 
			listid: "列表", 
		},
		uiactions: {
		},
	},
	edit_datapanel_form: {
		details: {
			group1: "市场营销列表基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "列表", 
			srfmajortext: "名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			lockstatus: "已锁定", 
			lastusedon: "上次使用时间", 
			ownername: "负责人", 
			listid: "列表", 
		},
		uiactions: {
		},
	},
	datapanel_form: {
		details: {
			button1: "头信息编辑", 
			grouppanel1: "分组面板", 
			group1: "市场营销列表基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "列表", 
			srfmajortext: "名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			lockstatus: "已锁定", 
			lastusedon: "上次使用时间", 
			ownername: "负责人", 
			listid: "列表", 
		},
		uiactions: {
			ibizlist_openedit_datapanel: "头信息编辑",
		},
	},
	quickcreate_form: {
		details: {
			group1: "list基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "列表", 
			srfmajortext: "名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			listname: "名称", 
			membertype: "目标对象", 
			type: "类型", 
			purpose: "用途", 
			source: "来源", 
			transactioncurrencyname: "货币", 
			cost: "成本", 
			lastusedon: "上次使用时间", 
			lockstatus: "已锁定", 
			description: "说明", 
			ownerid: "负责人", 
			ownertype: "负责人类型", 
			ownername: "负责人", 
			transactioncurrencyid: "货币", 
			listid: "列表", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			listname: "名称",
			type: "类型",
			createdfromcode: "成员类型",
			lastusedon: "上次使用时间",
			purpose: "用途",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	editviewtoolbar_toolbar: {
		tbitem1: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	infotoolbar_toolbar: {
		tbitem1_openmaineditview: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem1_remove_sep: {
			caption: "",
			tip: "",
		},
		tbitem1_remove: {
			caption: "删除并关闭",
			tip: "删除并关闭",
		},
		seperator4: {
			caption: "",
			tip: "",
		},
		tbitem17_active: {
			caption: "激活",
			tip: "激活",
		},
		tbitem17_stop_sep: {
			caption: "",
			tip: "",
		},
		tbitem17_stop: {
			caption: "停用",
			tip: "停用",
		},
		tbitem2: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem1_quickcreate: {
			caption: "新建",
			tip: "新建",
		},
		tbitem2: {
			caption: "-",
			tip: "",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem6: {
			caption: "拷贝",
			tip: "拷贝",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	effectivegridviewtoolbar_toolbar: {
		tbitem1_quickcreate: {
			caption: "新建",
			tip: "新建",
		},
		tbitem2: {
			caption: "-",
			tip: "",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem6: {
			caption: "拷贝",
			tip: "拷贝",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem14_stop: {
			caption: "停用",
			tip: "停用",
		},
		tbitem15: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	stopgridviewtoolbar_toolbar: {
		tbitem1_quickcreate: {
			caption: "新建",
			tip: "新建",
		},
		tbitem2: {
			caption: "-",
			tip: "",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem6: {
			caption: "拷贝",
			tip: "拷贝",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem14_active: {
			caption: "激活",
			tip: "激活",
		},
		tbitem15: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
};
export default {
  fields: {
    overriddencreatedon: "创建记录的时间",
    updatedate: "更新时间",
    versionnumber: "版本号",
    description: "说明",
    importsequencenumber: "导入序列号",
    updateman: "更新人",
    createdate: "建立时间",
    parentsubjectname: "父主题的名称。",
    featuremask: "功能掩码",
    createman: "建立人",
    title: "标题",
    subjectid: "主题",
    parentsubject: "父主题",
  },
	views: {
		pickupgridview: {
			caption: "主题",
      		title: "主题选择表格视图",
		},
		pickupview: {
			caption: "主题",
      		title: "主题数据选择视图",
		},
	},
	main_grid: {
		columns: {
			title: "标题",
			updateman: "更新人",
			updatedate: "更新时间",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
};
export default {
  fields: {
    updatedate: "更新时间",
    updateman: "更新人",
    createdate: "建立时间",
    createman: "建立人",
    relationshipstype: "关系类型",
    relationshipsid: "关系标识",
    relationshipsname: "关系名称",
    entity2name: "名称",
    entityname: "活动名称",
    entity2id: "列表",
    entityid: "市场活动",
  },
	views: {
		editview: {
			caption: "市场活动-营销列表",
      		title: "市场活动-营销列表编辑视图",
		},
		gridview: {
			caption: "市场活动-营销列表",
      		title: "市场活动-营销列表表格视图",
		},
	},
	main_form: {
		details: {
			group1: "市场活动-营销列表基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "关系标识", 
			srfmajortext: "关系名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			relationshipsid: "关系标识", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			entity2name: "名称",
			relationshipstype: "关系类型",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "新建",
			tip: "新建",
		},
		tbitem2: {
			caption: "-",
			tip: "",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem12: {
			caption: "关闭",
			tip: "关闭",
		},
	},
};
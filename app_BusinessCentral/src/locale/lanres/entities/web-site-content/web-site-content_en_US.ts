
export default {
  fields: {
    content: "内容",
    websitecontentid: "内容标识",
    contentcode: "业务标识",
    createman: "建立人",
    memo: "备注",
    author: "作者",
    validflag: "是否启用",
    title: "标题",
    link: "内容链接",
    sn: "序号",
    updatedate: "更新时间",
    subtitle: "子标题",
    updateman: "更新人",
    websitecontentname: "内容名称",
    createdate: "建立时间",
    contenttype: "内容类型",
    websitechannelname: "频道",
    websitename: "站点",
    websiteid: "网站标识",
    websitechannelid: "频道标识",
  },
	views: {
		gridview: {
			caption: "站点内容",
      		title: "站点内容",
		},
		editview: {
			caption: "站点内容",
      		title: "站点内容",
		},
	},
	main_form: {
		details: {
			group1: "内容基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "内容标识", 
			srfmajortext: "内容名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			websitecontentid: "内容标识", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			websitename: "站点",
			websitechannelname: "频道",
			sn: "序号",
			contenttype: "内容类型",
			title: "标题",
			contentcode: "业务标识",
			author: "作者",
			validflag: "是否启用",
			memo: "备注",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem6: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "Save",
			tip: "Save",
		},
		tbitem4: {
			caption: "Save And New",
			tip: "Save And New",
		},
		tbitem5: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "Remove And Close",
			tip: "Remove And Close Window",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "New",
			tip: "New",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "Copy",
			tip: "Copy {0}",
		},
	},
};
export default {
  fields: {
    content: "内容",
    websitecontentid: "内容标识",
    contentcode: "业务标识",
    createman: "建立人",
    memo: "备注",
    author: "作者",
    validflag: "是否启用",
    title: "标题",
    link: "内容链接",
    sn: "序号",
    updatedate: "更新时间",
    subtitle: "子标题",
    updateman: "更新人",
    websitecontentname: "内容名称",
    createdate: "建立时间",
    contenttype: "内容类型",
    websitechannelname: "频道",
    websitename: "站点",
    websiteid: "网站标识",
    websitechannelid: "频道标识",
  },
	views: {
		gridview: {
			caption: "站点内容",
      		title: "站点内容",
		},
		editview: {
			caption: "站点内容",
      		title: "站点内容",
		},
	},
	main_form: {
		details: {
			group1: "内容基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "内容标识", 
			srfmajortext: "内容名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			websitecontentid: "内容标识", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			websitename: "站点",
			websitechannelname: "频道",
			sn: "序号",
			contenttype: "内容类型",
			title: "标题",
			contentcode: "业务标识",
			author: "作者",
			validflag: "是否启用",
			memo: "备注",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem6: {
			caption: "拷贝",
			tip: "拷贝",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "保存",
			tip: "保存",
		},
		tbitem4: {
			caption: "保存并新建",
			tip: "保存并新建",
		},
		tbitem5: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "删除并关闭",
			tip: "删除并关闭",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "新建",
			tip: "新建",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "拷贝",
			tip: "拷贝",
		},
	},
};
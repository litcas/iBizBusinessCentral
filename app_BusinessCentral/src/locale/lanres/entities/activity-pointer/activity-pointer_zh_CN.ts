export default {
  fields: {
    seriesid: "系列 ID",
    traversedpath: "遍历的路径",
    deliveryprioritycode: "传递优先级",
    ownername: "负责人",
    onholdtime: "暂候时间(分钟)",
    workflowcreated: "由工作流创建",
    senton: "发送日期",
    lastonholdtime: "上一暂候时间",
    actualdurationminutes: "实际持续时间",
    regardingobjectid: "关于",
    deliverylastattemptedon: "上次尝试传递的日期",
    mapiprivate: "隐藏",
    actualend: "实际结束时间",
    description: "说明",
    prioritycode: "优先级",
    billed: "已记帐",
    regularactivity: "是定期活动",
    utcconversiontimezonecode: "UTC 转换时区代码",
    createman: "建立人",
    scheduleddurationminutes: "计划持续时间",
    activityadditionalparams: "活动附加参数",
    activityid: "活动",
    regardingobjectname: "关于",
    updatedate: "更新时间",
    stageid: "流程阶段",
    leftvoicemail: "保留的语音邮件",
    scheduledend: "截止日期",
    exchangeitemid: "Exchange 项目 ID",
    exchangerate: "汇率",
    scheduledstart: "开始日期",
    instancetypecode: "定期实例类型",
    regardingobjecttypecode: "RegardingObjectTypeCode",
    sortdate: "排序日期",
    createdate: "建立时间",
    community: "社交渠道",
    ownerid: "负责人",
    processid: "流程",
    slaname: "SLAName",
    subject: "主题",
    versionnumber: "版本号",
    allparties: "所有活动方",
    updateman: "更新人",
    exchangeweblink: "Exchange 链接",
    statuscode: "状态描述",
    timezoneruleversionnumber: "时区规则版本号",
    actualstart: "实际开始时间",
    activitytypecode: "活动类型",
    regardingobjectidname: "关于",
    ownertype: "所有者类型",
    statecode: "活动状态",
    serviceid: "服务",
    transactioncurrencyid: "货币",
    slaid: "SLA",
  },
	views: {
		gridview: {
			caption: "活动信息",
      		title: "活动",
		},
		byparentkey: {
			caption: "活动",
      		title: "活动",
		},
		editview: {
			caption: "活动",
      		title: "活动编辑视图",
		},
		editview2: {
			caption: "活动",
      		title: "活动编辑视图",
		},
		redirectview: {
			caption: "活动",
      		title: "活动数据重定向视图",
		},
	},
	main_form: {
		details: {
			group1: "activitypointer基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "活动", 
			srfmajortext: "主题", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			subject: "主题", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			activityid: "活动", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			subject: "主题",
			regardingobjectidname: "关于",
			activitytypecode: "活动类型",
			statecode: "活动状态",
			prioritycode: "优先级",
			scheduledstart: "开始日期",
			scheduledend: "截止日期",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
			n_subject_like: "主题(%)", 
			n_activitytypecode_eq: "活动类型(等于(=))", 
			n_prioritycode_eq: "优先级(等于(=))", 
			n_statuscode_eq: "状态描述(等于(=))", 
			n_statecode_eq: "活动状态(等于(=))", 
		},
		uiactions: {
		},
	},
	byparentkeytoolbar_toolbar: {
		deuiaction6: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem26: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		seperator1: {
			caption: "",
			tip: "",
		},
		deuiaction1: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "保存",
			tip: "保存",
		},
		tbitem4: {
			caption: "保存并新建",
			tip: "保存并新建",
		},
		tbitem5: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "删除并关闭",
			tip: "删除并关闭",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "新建",
			tip: "新建",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "拷贝",
			tip: "拷贝",
		},
	},
	gridviewtoolbar_toolbar: {
		deuiaction6: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem26: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		seperator1: {
			caption: "",
			tip: "",
		},
		deuiaction1: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	editview2toolbar_toolbar: {
		tbitem3: {
			caption: "保存",
			tip: "保存",
		},
		tbitem4: {
			caption: "保存并新建",
			tip: "保存并新建",
		},
		tbitem5: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "删除并关闭",
			tip: "删除并关闭",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "新建",
			tip: "新建",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "拷贝",
			tip: "拷贝",
		},
	},
};
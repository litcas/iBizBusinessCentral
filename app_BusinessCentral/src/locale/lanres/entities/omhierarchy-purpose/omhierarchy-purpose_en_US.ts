
export default {
  fields: {
    updatedate: "更新时间",
    omhierarchypurposename: "组织层次机构目的名称",
    createman: "建立人",
    omhierarchypurposeid: "组织层次机构目的标识",
    createdate: "建立时间",
    updateman: "更新人",
  },
};

export default {
  fields: {
    updateman: "更新人",
    updatedate: "更新时间",
    createman: "建立人",
    organizationname: "组织名称",
    createdate: "建立时间",
    organizationid: "组织",
    organizationtype: "组织类型",
    shortname: "组织简称",
    orglevel: "组织层级",
    orgcode: "组织编码",
    showorder: "排序号",
  },
};
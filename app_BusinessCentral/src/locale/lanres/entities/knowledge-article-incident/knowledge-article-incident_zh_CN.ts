export default {
  fields: {
    ownername: "负责人",
    ownertype: "负责人类型",
    overriddencreatedon: "Record Created On",
    createman: "建立人",
    knowledgeusage: "知识使用情况",
    updateman: "更新人",
    timezoneruleversionnumber: "Time Zone Rule Version Number",
    exchangerate: "汇率",
    statecode: "状态",
    knowledgearticleincidentid: "KnowledgeArticle 事件",
    versionnumber: "Version Number",
    statuscode: "状态描述",
    utcconversiontimezonecode: "UTC Conversion Time Zone Code",
    ownerid: "负责人",
    updatedate: "更新时间",
    senttocustomer: "已发送给客户",
    createdate: "建立时间",
    importsequencenumber: "Import Sequence Number",
    knowledgearticlename: "知识文章",
    incidentname: "服务案例",
    incidentid: "事件",
    knowledgearticleid: "知识文章",
  },
	views: {
		gridview: {
			caption: "知识文章事件",
      		title: "知识文章事件表格视图",
		},
		editview: {
			caption: "知识文章事件",
      		title: "知识文章事件编辑视图",
		},
	},
	main_form: {
		details: {
			group1: "knowledgearticleincident基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "KnowledgeArticle 事件", 
			srfmajortext: "状态", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			knowledgearticleid: "知识文章", 
			incidentname: "服务案例", 
			incidentid: "事件", 
			knowledgearticleincidentid: "KnowledgeArticle 事件", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			knowledgearticlename: "知识文章",
			incidentname: "服务案例",
			statuscode: "状态描述",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "新建",
			tip: "新建",
		},
		tbitem2: {
			caption: "-",
			tip: "",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem1: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
};
import dictoption_en_US from '@locale/lanres/entities/dict-option/dict-option_en_US';
import uomschedule_en_US from '@locale/lanres/entities/uom-schedule/uom-schedule_en_US';
import wfremodel_en_US from '@locale/lanres/entities/wfremodel/wfremodel_en_US';
import productsalesliterature_en_US from '@locale/lanres/entities/product-sales-literature/product-sales-literature_en_US';
import listcontact_en_US from '@locale/lanres/entities/list-contact/list-contact_en_US';
import territory_en_US from '@locale/lanres/entities/territory/territory_en_US';
import quotedetail_en_US from '@locale/lanres/entities/quote-detail/quote-detail_en_US';
import sysapp_en_US from '@locale/lanres/entities/sys-app/sys-app_en_US';
import opportunity_en_US from '@locale/lanres/entities/opportunity/opportunity_en_US';
import entitlement_en_US from '@locale/lanres/entities/entitlement/entitlement_en_US';
import discounttype_en_US from '@locale/lanres/entities/discount-type/discount-type_en_US';
import salesliteratureitem_en_US from '@locale/lanres/entities/sales-literature-item/sales-literature-item_en_US';
import campaigncampaign_en_US from '@locale/lanres/entities/campaign-campaign/campaign-campaign_en_US';
import campaignlist_en_US from '@locale/lanres/entities/campaign-list/campaign-list_en_US';
import ibizlist_en_US from '@locale/lanres/entities/ibiz-list/ibiz-list_en_US';
import dictcatalog_en_US from '@locale/lanres/entities/dict-catalog/dict-catalog_en_US';
import salesorder_en_US from '@locale/lanres/entities/sales-order/sales-order_en_US';
import account_en_US from '@locale/lanres/entities/account/account_en_US';
import languagelocale_en_US from '@locale/lanres/entities/language-locale/language-locale_en_US';
import serviceappointment_en_US from '@locale/lanres/entities/service-appointment/service-appointment_en_US';
import omhierarchy_en_US from '@locale/lanres/entities/omhierarchy/omhierarchy_en_US';
import ibizservice_en_US from '@locale/lanres/entities/ibiz-service/ibiz-service_en_US';
import goal_en_US from '@locale/lanres/entities/goal/goal_en_US';
import opportunitycompetitor_en_US from '@locale/lanres/entities/opportunity-competitor/opportunity-competitor_en_US';
import ibzdepartment_en_US from '@locale/lanres/entities/ibzdepartment/ibzdepartment_en_US';
import quote_en_US from '@locale/lanres/entities/quote/quote_en_US';
import transactioncurrency_en_US from '@locale/lanres/entities/transaction-currency/transaction-currency_en_US';
import invoicedetail_en_US from '@locale/lanres/entities/invoice-detail/invoice-detail_en_US';
import employee_en_US from '@locale/lanres/entities/employee/employee_en_US';
import knowledgearticle_en_US from '@locale/lanres/entities/knowledge-article/knowledge-article_en_US';
import jobsregistry_en_US from '@locale/lanres/entities/jobs-registry/jobs-registry_en_US';
import wfgroup_en_US from '@locale/lanres/entities/wfgroup/wfgroup_en_US';
import wfuser_en_US from '@locale/lanres/entities/wfuser/wfuser_en_US';
import lead_en_US from '@locale/lanres/entities/lead/lead_en_US';
import competitor_en_US from '@locale/lanres/entities/competitor/competitor_en_US';
import campaign_en_US from '@locale/lanres/entities/campaign/campaign_en_US';
import phonecall_en_US from '@locale/lanres/entities/phone-call/phone-call_en_US';
import sysrolepermission_en_US from '@locale/lanres/entities/sys-role-permission/sys-role-permission_en_US';
import appointment_en_US from '@locale/lanres/entities/appointment/appointment_en_US';
import sysuser_en_US from '@locale/lanres/entities/sys-user/sys-user_en_US';
import invoice_en_US from '@locale/lanres/entities/invoice/invoice_en_US';
import jobslog_en_US from '@locale/lanres/entities/jobs-log/jobs-log_en_US';
import incidentcustomer_en_US from '@locale/lanres/entities/incident-customer/incident-customer_en_US';
import metric_en_US from '@locale/lanres/entities/metric/metric_en_US';
import leadcompetitor_en_US from '@locale/lanres/entities/lead-competitor/lead-competitor_en_US';
import syspermission_en_US from '@locale/lanres/entities/sys-permission/sys-permission_en_US';
import pricelevel_en_US from '@locale/lanres/entities/price-level/price-level_en_US';
import productpricelevel_en_US from '@locale/lanres/entities/product-price-level/product-price-level_en_US';
import connectionrole_en_US from '@locale/lanres/entities/connection-role/connection-role_en_US';
import wfprocessdefinition_en_US from '@locale/lanres/entities/wfprocess-definition/wfprocess-definition_en_US';
import competitorsalesliterature_en_US from '@locale/lanres/entities/competitor-sales-literature/competitor-sales-literature_en_US';
import legal_en_US from '@locale/lanres/entities/legal/legal_en_US';
import knowledgearticleincident_en_US from '@locale/lanres/entities/knowledge-article-incident/knowledge-article-incident_en_US';
import task_en_US from '@locale/lanres/entities/task/task_en_US';
import ibzemployee_en_US from '@locale/lanres/entities/ibzemployee/ibzemployee_en_US';
import salesorderdetail_en_US from '@locale/lanres/entities/sales-order-detail/sales-order-detail_en_US';
import sysauthlog_en_US from '@locale/lanres/entities/sys-auth-log/sys-auth-log_en_US';
import omhierarchypurposeref_en_US from '@locale/lanres/entities/omhierarchy-purpose-ref/omhierarchy-purpose-ref_en_US';
import sysuserrole_en_US from '@locale/lanres/entities/sys-user-role/sys-user-role_en_US';
import bulkoperation_en_US from '@locale/lanres/entities/bulk-operation/bulk-operation_en_US';
import jobsinfo_en_US from '@locale/lanres/entities/jobs-info/jobs-info_en_US';
import ibzorganization_en_US from '@locale/lanres/entities/ibzorganization/ibzorganization_en_US';
import omhierarchycat_en_US from '@locale/lanres/entities/omhierarchy-cat/omhierarchy-cat_en_US';
import productassociation_en_US from '@locale/lanres/entities/product-association/product-association_en_US';
import campaignactivity_en_US from '@locale/lanres/entities/campaign-activity/campaign-activity_en_US';
import activitypointer_en_US from '@locale/lanres/entities/activity-pointer/activity-pointer_en_US';
import campaignresponse_en_US from '@locale/lanres/entities/campaign-response/campaign-response_en_US';
import listaccount_en_US from '@locale/lanres/entities/list-account/list-account_en_US';
import sysrole_en_US from '@locale/lanres/entities/sys-role/sys-role_en_US';
import salesliterature_en_US from '@locale/lanres/entities/sales-literature/sales-literature_en_US';
import subject_en_US from '@locale/lanres/entities/subject/subject_en_US';
import operationunit_en_US from '@locale/lanres/entities/operation-unit/operation-unit_en_US';
import ibzdeptmember_en_US from '@locale/lanres/entities/ibzdept-member/ibzdept-member_en_US';
import letter_en_US from '@locale/lanres/entities/letter/letter_en_US';
import uom_en_US from '@locale/lanres/entities/uom/uom_en_US';
import product_en_US from '@locale/lanres/entities/product/product_en_US';
import multipickdata_en_US from '@locale/lanres/entities/multi-pick-data/multi-pick-data_en_US';
import wfmember_en_US from '@locale/lanres/entities/wfmember/wfmember_en_US';
import opportunityproduct_en_US from '@locale/lanres/entities/opportunity-product/opportunity-product_en_US';
import contact_en_US from '@locale/lanres/entities/contact/contact_en_US';
import omhierarchypurpose_en_US from '@locale/lanres/entities/omhierarchy-purpose/omhierarchy-purpose_en_US';
import incident_en_US from '@locale/lanres/entities/incident/incident_en_US';
import fax_en_US from '@locale/lanres/entities/fax/fax_en_US';
import competitorproduct_en_US from '@locale/lanres/entities/competitor-product/competitor-product_en_US';
import listlead_en_US from '@locale/lanres/entities/list-lead/list-lead_en_US';
import productsubstitute_en_US from '@locale/lanres/entities/product-substitute/product-substitute_en_US';
import websitecontent_en_US from '@locale/lanres/entities/web-site-content/web-site-content_en_US';
import organization_en_US from '@locale/lanres/entities/organization/organization_en_US';
import email_en_US from '@locale/lanres/entities/email/email_en_US';
import ibzpost_en_US from '@locale/lanres/entities/ibzpost/ibzpost_en_US';
import connection_en_US from '@locale/lanres/entities/connection/connection_en_US';
import components_en_US from '@locale/lanres/components/components_en_US';
import codelist_en_US from '@locale/lanres/codelist/codelist_en_US';
import userCustom_en_US from '@locale/lanres/userCustom/userCustom_en_US';

export default {
    app: {
        commonWords:{
            error: "Error",
            success: "Success",
            ok: "OK",
            cancel: "Cancel",
            save: "Save",
            codeNotExist: 'Code list does not exist',
            reqException: "Request exception",
            sysException: "System abnormality",
            warning: "Warning",
            wrong: "Error",
            rulesException: "Abnormal value check rule",
            saveSuccess: "Saved successfully",
            saveFailed: "Save failed",
            deleteSuccess: "Successfully deleted!",
            deleteError: "Failed to delete",
            delDataFail: "Failed to delete data",
            noData: "No data",
            startsuccess:"Start successful"
        },
        local:{
            new: "New",
            add: "Add",
        },
        gridpage: {
            choicecolumns: "Choice columns",
            refresh: "refresh",
            show: "Show",
            records: "records",
            totle: "totle",
            noData: "No data",
            valueVail: "Value cannot be empty",
            notConfig: {
                fetchAction: "The view table fetchaction parameter is not configured",
                removeAction: "The view table removeaction parameter is not configured",
                createAction: "The view table createaction parameter is not configured",
                updateAction: "The view table updateaction parameter is not configured",
                loaddraftAction: "The view table loadtrafaction parameter is not configured",
            },
            data: "Data",
            delDataFail: "Failed to delete data",
            delSuccess: "Delete successfully!",
            confirmDel: "Are you sure you want to delete",
            notRecoverable: "delete will not be recoverable?",
            notBatch: "Batch addition not implemented",
            grid: "Grid",
            exportFail: "Data export failed",
            sum: "Total",
            formitemFailed: "Form item update failed",
        },
        list: {
            notConfig: {
                fetchAction: "View list fetchAction parameter is not configured",
                removeAction: "View table removeAction parameter is not configured",
                createAction: "View list createAction parameter is not configured",
                updateAction: "View list updateAction parameter is not configured",
            },
            confirmDel: "Are you sure you want to delete",
            notRecoverable: "delete will not be recoverable?",
        },
        listExpBar: {
            title: "List navigation bar",
        },
        wfExpBar: {
            title: "Process navigation bar",
        },
        calendarExpBar:{
            title: "Calendar navigation bar",
        },
        treeExpBar: {
            title: "Tree view navigation bar",
        },
        portlet: {
            noExtensions: "No extensions",
        },
        tabpage: {
            sureclosetip: {
                title: "Close warning",
                content: "Form data Changed, are sure close?",
            },
            closeall: "Close all",
            closeother: "Close other",
        },
        fileUpload: {
            caption: "Upload",
        },
        searchButton: {
            search: "Search",
            reset: "Reset",
        },
        calendar:{
          today: "today",
          month: "month",
          week: "week",
          day: "day",
          list: "list",
          dateSelectModalTitle: "select the time you wanted",
          gotoDate: "goto",
          from: "From",
          to: "To",
        },
        // 非实体视图
        views: {
            setting: {
                caption: "设置",
                title: "设置",
            },
            central: {
                caption: "企业中心",
                title: "企业中心",
            },
        },
        utilview:{
            importview:"Import Data",
            warning:"warning",
            info:"Please configure the data import item"    
        },
        menus: {
            central: {
                user_menus: "用户菜单",
                top_menus: "顶部菜单",
                menuitem3: "系统设置",
                menuitem39: "区域",
                menuitem26: "货币",
                menuitem4: "链接角色",
                menuitem33: "计价单位",
                menuitem36: "计价单位组",
                menuitem37: "价目表",
                menuitem38: "折扣表",
                menuitem31: "目标度量",
                menuitem44: "字典管理",
                menuitem53: "组织管理",
                menuitem54: "单位管理",
                menuitem55: "部门管理",
                menuitem56: "人员管理",
                menuitem57: "岗位管理",
                menuitem17: "系统权限",
                menuitem40: "系统用户",
                menuitem41: " 用户角色",
                menuitem42: "认证日志",
                menuitem43: "接入应用",
                menuitem45: "任务管理",
                menuitem46: " 任务注册",
                menuitem47: " 任务管理",
                menuitem48: "任务日志",
                menuitem49: "流程管理",
                menuitem50: "流程定义",
                menuitem51: "流程角色",
                menuitem52: "发布新流程",
                menuitem34: "消息通知",
                menuitem35: "帮助",
                left_exp: "左侧菜单",
                menuitem18: "最近",
                menuitem19: "固定",
                menuitem24: "组织权限管理",
                menuitem60: "法人",
                menuitem58: "业务单位",
                menuitem59: "部门",
                menuitem29: "组织层次",
                menuitem61: "员工",
                bottom_exp: "底部内容",
                footer_left: "底部左侧",
                footer_center: "底部中间",
                footer_right: "底部右侧",
            },
        },
        formpage:{
            error: "Error",
            desc1: "Operation failed, failed to find current form item",
            desc2: "Can't continue",
            notconfig: {
                loadaction: "View form loadAction parameter is not configured",
                loaddraftaction: "View form loaddraftAction parameter is not configured",
                actionname: "View form actionName parameter is not configured",
                removeaction: "View form removeAction parameter is not configured",
            },
            saveerror: "Error saving data",
            savecontent: "The data is inconsistent. The background data may have been modified. Do you want to reload the data?",
            valuecheckex: "Value rule check exception",
            savesuccess: "Saved successfully!",
            deletesuccess: "Successfully deleted!",  
            workflow: {
                starterror: "Workflow started successfully",
                startsuccess: "Workflow failed to start",
                submiterror: "Workflow submission failed",
                submitsuccess: "Workflow submitted successfully",
            },
            updateerror: "Form item update failed",       
        },
        gridBar: {
            title: "Table navigation bar",
        },
        multiEditView: {
            notConfig: {
                fetchAction: "View multi-edit view panel fetchAction parameter is not configured",
                loaddraftAction: "View multi-edit view panel loaddraftAction parameter is not configured",
            },
        },
        dataViewExpBar: {
            title: "Card view navigation bar",
        },
        kanban: {
            notConfig: {
                fetchAction: "View list fetchAction parameter is not configured",
                removeAction: "View table removeAction parameter is not configured",
            },
            delete1: "Confirm to delete ",
            delete2: "the delete operation will be unrecoverable!",
        },
        dashBoard: {
            handleClick: {
                title: "Panel design",
            },
        },
        dataView: {
            sum: "total",
            data: "data",
        },
        chart: {
            undefined: "Undefined",
            quarter: "Quarter",   
            year: "Year",
        },
        searchForm: {
            notConfig: {
                loadAction: "View search form loadAction parameter is not configured",
                loaddraftAction: "View search form loaddraftAction parameter is not configured",
            },
            custom: "Store custom queries",
            title: "Name",
        },
        wizardPanel: {
            back: "Back",
            next: "Next",
            complete: "Complete",
            preactionmessage:"The calculation of the previous behavior is not configured"
        },
        viewLayoutPanel: {
            appLogoutView: {
                prompt1: "Dear customer, you have successfully exited the system, after",
                prompt2: "seconds, we will jump to the",
                logingPage: "login page",
            },
            appWfstepTraceView: {
                title: "Application process processing record view",
            },
            appWfstepDataView: {
                title: "Application process tracking view",
            },
            appLoginView: {
                username: "Username",
                password: "Password",
                login: "Login",
            },
        },
    },
    form: {
        group: {
            show_more: "Show More",
            hidden_more: "Hide More"
        }
    },
    entities: {
        dictoption: dictoption_en_US,
        uomschedule: uomschedule_en_US,
        wfremodel: wfremodel_en_US,
        productsalesliterature: productsalesliterature_en_US,
        listcontact: listcontact_en_US,
        territory: territory_en_US,
        quotedetail: quotedetail_en_US,
        sysapp: sysapp_en_US,
        opportunity: opportunity_en_US,
        entitlement: entitlement_en_US,
        discounttype: discounttype_en_US,
        salesliteratureitem: salesliteratureitem_en_US,
        campaigncampaign: campaigncampaign_en_US,
        campaignlist: campaignlist_en_US,
        ibizlist: ibizlist_en_US,
        dictcatalog: dictcatalog_en_US,
        salesorder: salesorder_en_US,
        account: account_en_US,
        languagelocale: languagelocale_en_US,
        serviceappointment: serviceappointment_en_US,
        omhierarchy: omhierarchy_en_US,
        ibizservice: ibizservice_en_US,
        goal: goal_en_US,
        opportunitycompetitor: opportunitycompetitor_en_US,
        ibzdepartment: ibzdepartment_en_US,
        quote: quote_en_US,
        transactioncurrency: transactioncurrency_en_US,
        invoicedetail: invoicedetail_en_US,
        employee: employee_en_US,
        knowledgearticle: knowledgearticle_en_US,
        jobsregistry: jobsregistry_en_US,
        wfgroup: wfgroup_en_US,
        wfuser: wfuser_en_US,
        lead: lead_en_US,
        competitor: competitor_en_US,
        campaign: campaign_en_US,
        phonecall: phonecall_en_US,
        sysrolepermission: sysrolepermission_en_US,
        appointment: appointment_en_US,
        sysuser: sysuser_en_US,
        invoice: invoice_en_US,
        jobslog: jobslog_en_US,
        incidentcustomer: incidentcustomer_en_US,
        metric: metric_en_US,
        leadcompetitor: leadcompetitor_en_US,
        syspermission: syspermission_en_US,
        pricelevel: pricelevel_en_US,
        productpricelevel: productpricelevel_en_US,
        connectionrole: connectionrole_en_US,
        wfprocessdefinition: wfprocessdefinition_en_US,
        competitorsalesliterature: competitorsalesliterature_en_US,
        legal: legal_en_US,
        knowledgearticleincident: knowledgearticleincident_en_US,
        task: task_en_US,
        ibzemployee: ibzemployee_en_US,
        salesorderdetail: salesorderdetail_en_US,
        sysauthlog: sysauthlog_en_US,
        omhierarchypurposeref: omhierarchypurposeref_en_US,
        sysuserrole: sysuserrole_en_US,
        bulkoperation: bulkoperation_en_US,
        jobsinfo: jobsinfo_en_US,
        ibzorganization: ibzorganization_en_US,
        omhierarchycat: omhierarchycat_en_US,
        productassociation: productassociation_en_US,
        campaignactivity: campaignactivity_en_US,
        activitypointer: activitypointer_en_US,
        campaignresponse: campaignresponse_en_US,
        listaccount: listaccount_en_US,
        sysrole: sysrole_en_US,
        salesliterature: salesliterature_en_US,
        subject: subject_en_US,
        operationunit: operationunit_en_US,
        ibzdeptmember: ibzdeptmember_en_US,
        letter: letter_en_US,
        uom: uom_en_US,
        product: product_en_US,
        multipickdata: multipickdata_en_US,
        wfmember: wfmember_en_US,
        opportunityproduct: opportunityproduct_en_US,
        contact: contact_en_US,
        omhierarchypurpose: omhierarchypurpose_en_US,
        incident: incident_en_US,
        fax: fax_en_US,
        competitorproduct: competitorproduct_en_US,
        listlead: listlead_en_US,
        productsubstitute: productsubstitute_en_US,
        websitecontent: websitecontent_en_US,
        organization: organization_en_US,
        email: email_en_US,
        ibzpost: ibzpost_en_US,
        connection: connection_en_US,
    },
    components: components_en_US,
    codelist: codelist_en_US,
    userCustom: userCustom_en_US,
};
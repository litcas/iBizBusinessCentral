import OMHierarchyPurposeAuthServiceBase from './omhierarchy-purpose-auth-service-base';


/**
 * 组织层次结构应用权限服务对象
 *
 * @export
 * @class OMHierarchyPurposeAuthService
 * @extends {OMHierarchyPurposeAuthServiceBase}
 */
export default class OMHierarchyPurposeAuthService extends OMHierarchyPurposeAuthServiceBase {

    /**
     * Creates an instance of  OMHierarchyPurposeAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  OMHierarchyPurposeAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
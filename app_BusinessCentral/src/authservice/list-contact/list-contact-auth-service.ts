import ListContactAuthServiceBase from './list-contact-auth-service-base';


/**
 * 营销列表-联系人权限服务对象
 *
 * @export
 * @class ListContactAuthService
 * @extends {ListContactAuthServiceBase}
 */
export default class ListContactAuthService extends ListContactAuthServiceBase {

    /**
     * Creates an instance of  ListContactAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  ListContactAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import OpportunityProductAuthServiceBase from './opportunity-product-auth-service-base';


/**
 * 商机产品权限服务对象
 *
 * @export
 * @class OpportunityProductAuthService
 * @extends {OpportunityProductAuthServiceBase}
 */
export default class OpportunityProductAuthService extends OpportunityProductAuthServiceBase {

    /**
     * Creates an instance of  OpportunityProductAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  OpportunityProductAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import TerritoryAuthServiceBase from './territory-auth-service-base';


/**
 * 区域权限服务对象
 *
 * @export
 * @class TerritoryAuthService
 * @extends {TerritoryAuthServiceBase}
 */
export default class TerritoryAuthService extends TerritoryAuthServiceBase {

    /**
     * Creates an instance of  TerritoryAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  TerritoryAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
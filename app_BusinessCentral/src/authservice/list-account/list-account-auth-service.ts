import ListAccountAuthServiceBase from './list-account-auth-service-base';


/**
 * 营销列表-账户权限服务对象
 *
 * @export
 * @class ListAccountAuthService
 * @extends {ListAccountAuthServiceBase}
 */
export default class ListAccountAuthService extends ListAccountAuthServiceBase {

    /**
     * Creates an instance of  ListAccountAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  ListAccountAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
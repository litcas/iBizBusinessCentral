import SubjectAuthServiceBase from './subject-auth-service-base';


/**
 * 主题权限服务对象
 *
 * @export
 * @class SubjectAuthService
 * @extends {SubjectAuthServiceBase}
 */
export default class SubjectAuthService extends SubjectAuthServiceBase {

    /**
     * Creates an instance of  SubjectAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  SubjectAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
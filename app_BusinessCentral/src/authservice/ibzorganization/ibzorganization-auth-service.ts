import IBZOrganizationAuthServiceBase from './ibzorganization-auth-service-base';


/**
 * 单位机构权限服务对象
 *
 * @export
 * @class IBZOrganizationAuthService
 * @extends {IBZOrganizationAuthServiceBase}
 */
export default class IBZOrganizationAuthService extends IBZOrganizationAuthServiceBase {

    /**
     * Creates an instance of  IBZOrganizationAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  IBZOrganizationAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
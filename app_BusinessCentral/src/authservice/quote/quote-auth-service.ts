import QuoteAuthServiceBase from './quote-auth-service-base';


/**
 * 报价单权限服务对象
 *
 * @export
 * @class QuoteAuthService
 * @extends {QuoteAuthServiceBase}
 */
export default class QuoteAuthService extends QuoteAuthServiceBase {

    /**
     * Creates an instance of  QuoteAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  QuoteAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
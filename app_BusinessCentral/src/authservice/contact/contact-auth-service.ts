import ContactAuthServiceBase from './contact-auth-service-base';


/**
 * 联系人权限服务对象
 *
 * @export
 * @class ContactAuthService
 * @extends {ContactAuthServiceBase}
 */
export default class ContactAuthService extends ContactAuthServiceBase {

    /**
     * Creates an instance of  ContactAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  ContactAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
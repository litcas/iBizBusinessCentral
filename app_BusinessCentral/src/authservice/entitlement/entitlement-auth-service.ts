import EntitlementAuthServiceBase from './entitlement-auth-service-base';


/**
 * 权利权限服务对象
 *
 * @export
 * @class EntitlementAuthService
 * @extends {EntitlementAuthServiceBase}
 */
export default class EntitlementAuthService extends EntitlementAuthServiceBase {

    /**
     * Creates an instance of  EntitlementAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EntitlementAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import CampaignListAuthServiceBase from './campaign-list-auth-service-base';


/**
 * 市场活动-营销列表权限服务对象
 *
 * @export
 * @class CampaignListAuthService
 * @extends {CampaignListAuthServiceBase}
 */
export default class CampaignListAuthService extends CampaignListAuthServiceBase {

    /**
     * Creates an instance of  CampaignListAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  CampaignListAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import CampaignAuthServiceBase from './campaign-auth-service-base';


/**
 * 市场活动权限服务对象
 *
 * @export
 * @class CampaignAuthService
 * @extends {CampaignAuthServiceBase}
 */
export default class CampaignAuthService extends CampaignAuthServiceBase {

    /**
     * Creates an instance of  CampaignAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  CampaignAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import ListLeadAuthServiceBase from './list-lead-auth-service-base';


/**
 * 营销列表-潜在客户权限服务对象
 *
 * @export
 * @class ListLeadAuthService
 * @extends {ListLeadAuthServiceBase}
 */
export default class ListLeadAuthService extends ListLeadAuthServiceBase {

    /**
     * Creates an instance of  ListLeadAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  ListLeadAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
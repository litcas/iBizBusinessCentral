import CampaignResponseAuthServiceBase from './campaign-response-auth-service-base';


/**
 * 市场活动响应权限服务对象
 *
 * @export
 * @class CampaignResponseAuthService
 * @extends {CampaignResponseAuthServiceBase}
 */
export default class CampaignResponseAuthService extends CampaignResponseAuthServiceBase {

    /**
     * Creates an instance of  CampaignResponseAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  CampaignResponseAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
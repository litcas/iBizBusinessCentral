import IncidentCustomerAuthServiceBase from './incident-customer-auth-service-base';


/**
 * 案例客户权限服务对象
 *
 * @export
 * @class IncidentCustomerAuthService
 * @extends {IncidentCustomerAuthServiceBase}
 */
export default class IncidentCustomerAuthService extends IncidentCustomerAuthServiceBase {

    /**
     * Creates an instance of  IncidentCustomerAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  IncidentCustomerAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
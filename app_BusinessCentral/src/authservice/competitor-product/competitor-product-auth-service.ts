import CompetitorProductAuthServiceBase from './competitor-product-auth-service-base';


/**
 * 竞争对手产品权限服务对象
 *
 * @export
 * @class CompetitorProductAuthService
 * @extends {CompetitorProductAuthServiceBase}
 */
export default class CompetitorProductAuthService extends CompetitorProductAuthServiceBase {

    /**
     * Creates an instance of  CompetitorProductAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  CompetitorProductAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
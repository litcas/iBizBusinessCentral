import SalesLiteratureAuthServiceBase from './sales-literature-auth-service-base';


/**
 * 销售宣传资料权限服务对象
 *
 * @export
 * @class SalesLiteratureAuthService
 * @extends {SalesLiteratureAuthServiceBase}
 */
export default class SalesLiteratureAuthService extends SalesLiteratureAuthServiceBase {

    /**
     * Creates an instance of  SalesLiteratureAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  SalesLiteratureAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
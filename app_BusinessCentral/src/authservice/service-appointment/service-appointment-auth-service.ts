import ServiceAppointmentAuthServiceBase from './service-appointment-auth-service-base';


/**
 * 服务活动权限服务对象
 *
 * @export
 * @class ServiceAppointmentAuthService
 * @extends {ServiceAppointmentAuthServiceBase}
 */
export default class ServiceAppointmentAuthService extends ServiceAppointmentAuthServiceBase {

    /**
     * Creates an instance of  ServiceAppointmentAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  ServiceAppointmentAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
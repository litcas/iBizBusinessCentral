import UomAuthServiceBase from './uom-auth-service-base';


/**
 * 计价单位权限服务对象
 *
 * @export
 * @class UomAuthService
 * @extends {UomAuthServiceBase}
 */
export default class UomAuthService extends UomAuthServiceBase {

    /**
     * Creates an instance of  UomAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  UomAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
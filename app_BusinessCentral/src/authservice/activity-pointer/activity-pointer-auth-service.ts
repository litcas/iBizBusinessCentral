import ActivityPointerAuthServiceBase from './activity-pointer-auth-service-base';


/**
 * 活动权限服务对象
 *
 * @export
 * @class ActivityPointerAuthService
 * @extends {ActivityPointerAuthServiceBase}
 */
export default class ActivityPointerAuthService extends ActivityPointerAuthServiceBase {

    /**
     * Creates an instance of  ActivityPointerAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  ActivityPointerAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import InvoiceDetailAuthServiceBase from './invoice-detail-auth-service-base';


/**
 * 发票产品权限服务对象
 *
 * @export
 * @class InvoiceDetailAuthService
 * @extends {InvoiceDetailAuthServiceBase}
 */
export default class InvoiceDetailAuthService extends InvoiceDetailAuthServiceBase {

    /**
     * Creates an instance of  InvoiceDetailAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  InvoiceDetailAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
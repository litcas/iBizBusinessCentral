import CampaignActivityAuthServiceBase from './campaign-activity-auth-service-base';


/**
 * 市场活动项目权限服务对象
 *
 * @export
 * @class CampaignActivityAuthService
 * @extends {CampaignActivityAuthServiceBase}
 */
export default class CampaignActivityAuthService extends CampaignActivityAuthServiceBase {

    /**
     * Creates an instance of  CampaignActivityAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  CampaignActivityAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
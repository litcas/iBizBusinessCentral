/**
 * 实体权限服务注册中心
 *
 * @export
 * @class AuthServiceRegister
 */
export class AuthServiceRegister {

    /**
     * 所有实体权限服务Map
     *
     * @protected
     * @type {*}
     * @memberof AuthServiceRegister
     */
    protected allAuthService: Map<string, () => Promise<any>> = new Map();

    /**
     * 已加载实体权限服务Map缓存
     *
     * @protected
     * @type {Map<string, any>}
     * @memberof AuthServiceRegister
     */
    protected serviceCache: Map<string, any> = new Map();

    /**
     * Creates an instance of AuthServiceRegister.
     * @memberof AuthServiceRegister
     */
    constructor() {
        this.init();
    }

    /**
     * 初始化
     *
     * @protected
     * @memberof AuthServiceRegister
     */
    protected init(): void {
                this.allAuthService.set('dictoption', () => import('@/authservice/dict-option/dict-option-auth-service'));
        this.allAuthService.set('uomschedule', () => import('@/authservice/uom-schedule/uom-schedule-auth-service'));
        this.allAuthService.set('wfremodel', () => import('@/authservice/wfremodel/wfremodel-auth-service'));
        this.allAuthService.set('productsalesliterature', () => import('@/authservice/product-sales-literature/product-sales-literature-auth-service'));
        this.allAuthService.set('listcontact', () => import('@/authservice/list-contact/list-contact-auth-service'));
        this.allAuthService.set('territory', () => import('@/authservice/territory/territory-auth-service'));
        this.allAuthService.set('quotedetail', () => import('@/authservice/quote-detail/quote-detail-auth-service'));
        this.allAuthService.set('sysapp', () => import('@/authservice/sys-app/sys-app-auth-service'));
        this.allAuthService.set('opportunity', () => import('@/authservice/opportunity/opportunity-auth-service'));
        this.allAuthService.set('entitlement', () => import('@/authservice/entitlement/entitlement-auth-service'));
        this.allAuthService.set('discounttype', () => import('@/authservice/discount-type/discount-type-auth-service'));
        this.allAuthService.set('salesliteratureitem', () => import('@/authservice/sales-literature-item/sales-literature-item-auth-service'));
        this.allAuthService.set('campaigncampaign', () => import('@/authservice/campaign-campaign/campaign-campaign-auth-service'));
        this.allAuthService.set('campaignlist', () => import('@/authservice/campaign-list/campaign-list-auth-service'));
        this.allAuthService.set('ibizlist', () => import('@/authservice/ibiz-list/ibiz-list-auth-service'));
        this.allAuthService.set('dictcatalog', () => import('@/authservice/dict-catalog/dict-catalog-auth-service'));
        this.allAuthService.set('salesorder', () => import('@/authservice/sales-order/sales-order-auth-service'));
        this.allAuthService.set('account', () => import('@/authservice/account/account-auth-service'));
        this.allAuthService.set('languagelocale', () => import('@/authservice/language-locale/language-locale-auth-service'));
        this.allAuthService.set('serviceappointment', () => import('@/authservice/service-appointment/service-appointment-auth-service'));
        this.allAuthService.set('omhierarchy', () => import('@/authservice/omhierarchy/omhierarchy-auth-service'));
        this.allAuthService.set('ibizservice', () => import('@/authservice/ibiz-service/ibiz-service-auth-service'));
        this.allAuthService.set('goal', () => import('@/authservice/goal/goal-auth-service'));
        this.allAuthService.set('opportunitycompetitor', () => import('@/authservice/opportunity-competitor/opportunity-competitor-auth-service'));
        this.allAuthService.set('ibzdepartment', () => import('@/authservice/ibzdepartment/ibzdepartment-auth-service'));
        this.allAuthService.set('quote', () => import('@/authservice/quote/quote-auth-service'));
        this.allAuthService.set('transactioncurrency', () => import('@/authservice/transaction-currency/transaction-currency-auth-service'));
        this.allAuthService.set('invoicedetail', () => import('@/authservice/invoice-detail/invoice-detail-auth-service'));
        this.allAuthService.set('employee', () => import('@/authservice/employee/employee-auth-service'));
        this.allAuthService.set('knowledgearticle', () => import('@/authservice/knowledge-article/knowledge-article-auth-service'));
        this.allAuthService.set('jobsregistry', () => import('@/authservice/jobs-registry/jobs-registry-auth-service'));
        this.allAuthService.set('wfgroup', () => import('@/authservice/wfgroup/wfgroup-auth-service'));
        this.allAuthService.set('wfuser', () => import('@/authservice/wfuser/wfuser-auth-service'));
        this.allAuthService.set('lead', () => import('@/authservice/lead/lead-auth-service'));
        this.allAuthService.set('competitor', () => import('@/authservice/competitor/competitor-auth-service'));
        this.allAuthService.set('campaign', () => import('@/authservice/campaign/campaign-auth-service'));
        this.allAuthService.set('phonecall', () => import('@/authservice/phone-call/phone-call-auth-service'));
        this.allAuthService.set('sysrolepermission', () => import('@/authservice/sys-role-permission/sys-role-permission-auth-service'));
        this.allAuthService.set('appointment', () => import('@/authservice/appointment/appointment-auth-service'));
        this.allAuthService.set('sysuser', () => import('@/authservice/sys-user/sys-user-auth-service'));
        this.allAuthService.set('invoice', () => import('@/authservice/invoice/invoice-auth-service'));
        this.allAuthService.set('jobslog', () => import('@/authservice/jobs-log/jobs-log-auth-service'));
        this.allAuthService.set('incidentcustomer', () => import('@/authservice/incident-customer/incident-customer-auth-service'));
        this.allAuthService.set('metric', () => import('@/authservice/metric/metric-auth-service'));
        this.allAuthService.set('leadcompetitor', () => import('@/authservice/lead-competitor/lead-competitor-auth-service'));
        this.allAuthService.set('syspermission', () => import('@/authservice/sys-permission/sys-permission-auth-service'));
        this.allAuthService.set('pricelevel', () => import('@/authservice/price-level/price-level-auth-service'));
        this.allAuthService.set('productpricelevel', () => import('@/authservice/product-price-level/product-price-level-auth-service'));
        this.allAuthService.set('connectionrole', () => import('@/authservice/connection-role/connection-role-auth-service'));
        this.allAuthService.set('wfprocessdefinition', () => import('@/authservice/wfprocess-definition/wfprocess-definition-auth-service'));
        this.allAuthService.set('competitorsalesliterature', () => import('@/authservice/competitor-sales-literature/competitor-sales-literature-auth-service'));
        this.allAuthService.set('legal', () => import('@/authservice/legal/legal-auth-service'));
        this.allAuthService.set('knowledgearticleincident', () => import('@/authservice/knowledge-article-incident/knowledge-article-incident-auth-service'));
        this.allAuthService.set('task', () => import('@/authservice/task/task-auth-service'));
        this.allAuthService.set('ibzemployee', () => import('@/authservice/ibzemployee/ibzemployee-auth-service'));
        this.allAuthService.set('salesorderdetail', () => import('@/authservice/sales-order-detail/sales-order-detail-auth-service'));
        this.allAuthService.set('sysauthlog', () => import('@/authservice/sys-auth-log/sys-auth-log-auth-service'));
        this.allAuthService.set('omhierarchypurposeref', () => import('@/authservice/omhierarchy-purpose-ref/omhierarchy-purpose-ref-auth-service'));
        this.allAuthService.set('sysuserrole', () => import('@/authservice/sys-user-role/sys-user-role-auth-service'));
        this.allAuthService.set('bulkoperation', () => import('@/authservice/bulk-operation/bulk-operation-auth-service'));
        this.allAuthService.set('jobsinfo', () => import('@/authservice/jobs-info/jobs-info-auth-service'));
        this.allAuthService.set('ibzorganization', () => import('@/authservice/ibzorganization/ibzorganization-auth-service'));
        this.allAuthService.set('omhierarchycat', () => import('@/authservice/omhierarchy-cat/omhierarchy-cat-auth-service'));
        this.allAuthService.set('productassociation', () => import('@/authservice/product-association/product-association-auth-service'));
        this.allAuthService.set('campaignactivity', () => import('@/authservice/campaign-activity/campaign-activity-auth-service'));
        this.allAuthService.set('activitypointer', () => import('@/authservice/activity-pointer/activity-pointer-auth-service'));
        this.allAuthService.set('campaignresponse', () => import('@/authservice/campaign-response/campaign-response-auth-service'));
        this.allAuthService.set('listaccount', () => import('@/authservice/list-account/list-account-auth-service'));
        this.allAuthService.set('sysrole', () => import('@/authservice/sys-role/sys-role-auth-service'));
        this.allAuthService.set('salesliterature', () => import('@/authservice/sales-literature/sales-literature-auth-service'));
        this.allAuthService.set('subject', () => import('@/authservice/subject/subject-auth-service'));
        this.allAuthService.set('operationunit', () => import('@/authservice/operation-unit/operation-unit-auth-service'));
        this.allAuthService.set('ibzdeptmember', () => import('@/authservice/ibzdept-member/ibzdept-member-auth-service'));
        this.allAuthService.set('letter', () => import('@/authservice/letter/letter-auth-service'));
        this.allAuthService.set('uom', () => import('@/authservice/uom/uom-auth-service'));
        this.allAuthService.set('product', () => import('@/authservice/product/product-auth-service'));
        this.allAuthService.set('multipickdata', () => import('@/authservice/multi-pick-data/multi-pick-data-auth-service'));
        this.allAuthService.set('wfmember', () => import('@/authservice/wfmember/wfmember-auth-service'));
        this.allAuthService.set('opportunityproduct', () => import('@/authservice/opportunity-product/opportunity-product-auth-service'));
        this.allAuthService.set('contact', () => import('@/authservice/contact/contact-auth-service'));
        this.allAuthService.set('omhierarchypurpose', () => import('@/authservice/omhierarchy-purpose/omhierarchy-purpose-auth-service'));
        this.allAuthService.set('incident', () => import('@/authservice/incident/incident-auth-service'));
        this.allAuthService.set('fax', () => import('@/authservice/fax/fax-auth-service'));
        this.allAuthService.set('competitorproduct', () => import('@/authservice/competitor-product/competitor-product-auth-service'));
        this.allAuthService.set('listlead', () => import('@/authservice/list-lead/list-lead-auth-service'));
        this.allAuthService.set('productsubstitute', () => import('@/authservice/product-substitute/product-substitute-auth-service'));
        this.allAuthService.set('websitecontent', () => import('@/authservice/web-site-content/web-site-content-auth-service'));
        this.allAuthService.set('organization', () => import('@/authservice/organization/organization-auth-service'));
        this.allAuthService.set('email', () => import('@/authservice/email/email-auth-service'));
        this.allAuthService.set('ibzpost', () => import('@/authservice/ibzpost/ibzpost-auth-service'));
        this.allAuthService.set('connection', () => import('@/authservice/connection/connection-auth-service'));
    }

    /**
     * 加载实体权限服务
     *
     * @protected
     * @param {string} serviceName
     * @returns {Promise<any>}
     * @memberof AuthServiceRegister
     */
    protected async loadService(serviceName: string): Promise<any> {
        const service = this.allAuthService.get(serviceName);
        if (service) {
            return service();
        }
    }

    /**
     * 获取应用实体权限服务
     *
     * @param {string} name
     * @returns {Promise<any>}
     * @memberof AuthServiceRegister
     */
    public async getService(name: string): Promise<any> {
        if (this.serviceCache.has(name)) {
            return this.serviceCache.get(name);
        }
        const authService: any = await this.loadService(name);
        if (authService && authService.default) {
            const instance: any = new authService.default();
            this.serviceCache.set(name, instance);
            return instance;
        }
    }

}
export const authServiceRegister: AuthServiceRegister = new AuthServiceRegister();
import AppointmentAuthServiceBase from './appointment-auth-service-base';


/**
 * 约会权限服务对象
 *
 * @export
 * @class AppointmentAuthService
 * @extends {AppointmentAuthServiceBase}
 */
export default class AppointmentAuthService extends AppointmentAuthServiceBase {

    /**
     * Creates an instance of  AppointmentAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  AppointmentAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
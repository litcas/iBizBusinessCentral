import ConnectionAuthServiceBase from './connection-auth-service-base';


/**
 * 连接权限服务对象
 *
 * @export
 * @class ConnectionAuthService
 * @extends {ConnectionAuthServiceBase}
 */
export default class ConnectionAuthService extends ConnectionAuthServiceBase {

    /**
     * Creates an instance of  ConnectionAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  ConnectionAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
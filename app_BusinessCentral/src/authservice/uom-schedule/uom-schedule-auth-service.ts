import UomScheduleAuthServiceBase from './uom-schedule-auth-service-base';


/**
 * 计价单位组权限服务对象
 *
 * @export
 * @class UomScheduleAuthService
 * @extends {UomScheduleAuthServiceBase}
 */
export default class UomScheduleAuthService extends UomScheduleAuthServiceBase {

    /**
     * Creates an instance of  UomScheduleAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  UomScheduleAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
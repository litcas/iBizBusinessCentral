import LeadAuthServiceBase from './lead-auth-service-base';


/**
 * 潜在顾客权限服务对象
 *
 * @export
 * @class LeadAuthService
 * @extends {LeadAuthServiceBase}
 */
export default class LeadAuthService extends LeadAuthServiceBase {

    /**
     * Creates an instance of  LeadAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  LeadAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import LetterAuthServiceBase from './letter-auth-service-base';


/**
 * 信件权限服务对象
 *
 * @export
 * @class LetterAuthService
 * @extends {LetterAuthServiceBase}
 */
export default class LetterAuthService extends LetterAuthServiceBase {

    /**
     * Creates an instance of  LetterAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  LetterAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
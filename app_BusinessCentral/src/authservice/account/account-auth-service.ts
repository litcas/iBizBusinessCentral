import AccountAuthServiceBase from './account-auth-service-base';


/**
 * 客户权限服务对象
 *
 * @export
 * @class AccountAuthService
 * @extends {AccountAuthServiceBase}
 */
export default class AccountAuthService extends AccountAuthServiceBase {

    /**
     * Creates an instance of  AccountAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  AccountAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
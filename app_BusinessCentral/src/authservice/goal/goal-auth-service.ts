import GoalAuthServiceBase from './goal-auth-service-base';


/**
 * 目标权限服务对象
 *
 * @export
 * @class GoalAuthService
 * @extends {GoalAuthServiceBase}
 */
export default class GoalAuthService extends GoalAuthServiceBase {

    /**
     * Creates an instance of  GoalAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  GoalAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
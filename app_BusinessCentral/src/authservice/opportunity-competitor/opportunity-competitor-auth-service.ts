import OpportunityCompetitorAuthServiceBase from './opportunity-competitor-auth-service-base';


/**
 * 商机对手权限服务对象
 *
 * @export
 * @class OpportunityCompetitorAuthService
 * @extends {OpportunityCompetitorAuthServiceBase}
 */
export default class OpportunityCompetitorAuthService extends OpportunityCompetitorAuthServiceBase {

    /**
     * Creates an instance of  OpportunityCompetitorAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  OpportunityCompetitorAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
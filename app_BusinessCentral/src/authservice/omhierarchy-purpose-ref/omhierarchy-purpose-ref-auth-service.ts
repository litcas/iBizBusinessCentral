import OMHierarchyPurposeRefAuthServiceBase from './omhierarchy-purpose-ref-auth-service-base';


/**
 * 组织层次结构分配权限服务对象
 *
 * @export
 * @class OMHierarchyPurposeRefAuthService
 * @extends {OMHierarchyPurposeRefAuthServiceBase}
 */
export default class OMHierarchyPurposeRefAuthService extends OMHierarchyPurposeRefAuthServiceBase {

    /**
     * Creates an instance of  OMHierarchyPurposeRefAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  OMHierarchyPurposeRefAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import InvoiceAuthServiceBase from './invoice-auth-service-base';


/**
 * 发票权限服务对象
 *
 * @export
 * @class InvoiceAuthService
 * @extends {InvoiceAuthServiceBase}
 */
export default class InvoiceAuthService extends InvoiceAuthServiceBase {

    /**
     * Creates an instance of  InvoiceAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  InvoiceAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
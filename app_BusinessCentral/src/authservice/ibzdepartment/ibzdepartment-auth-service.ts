import IBZDepartmentAuthServiceBase from './ibzdepartment-auth-service-base';


/**
 * 部门权限服务对象
 *
 * @export
 * @class IBZDepartmentAuthService
 * @extends {IBZDepartmentAuthServiceBase}
 */
export default class IBZDepartmentAuthService extends IBZDepartmentAuthServiceBase {

    /**
     * Creates an instance of  IBZDepartmentAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  IBZDepartmentAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
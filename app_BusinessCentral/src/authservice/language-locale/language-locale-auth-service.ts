import LanguageLocaleAuthServiceBase from './language-locale-auth-service-base';


/**
 * 语言权限服务对象
 *
 * @export
 * @class LanguageLocaleAuthService
 * @extends {LanguageLocaleAuthServiceBase}
 */
export default class LanguageLocaleAuthService extends LanguageLocaleAuthServiceBase {

    /**
     * Creates an instance of  LanguageLocaleAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  LanguageLocaleAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import MultiPickDataAuthServiceBase from './multi-pick-data-auth-service-base';


/**
 * 多类选择实体权限服务对象
 *
 * @export
 * @class MultiPickDataAuthService
 * @extends {MultiPickDataAuthServiceBase}
 */
export default class MultiPickDataAuthService extends MultiPickDataAuthServiceBase {

    /**
     * Creates an instance of  MultiPickDataAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  MultiPickDataAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
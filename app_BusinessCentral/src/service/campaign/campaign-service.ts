import { Http,Util } from '@/utils';
import CampaignServiceBase from './campaign-service-base';


/**
 * 市场活动服务对象
 *
 * @export
 * @class CampaignService
 * @extends {CampaignServiceBase}
 */
export default class CampaignService extends CampaignServiceBase {

    /**
     * Creates an instance of  CampaignService.
     * 
     * @param {*} [opts={}]
     * @memberof  CampaignService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import { Http,Util } from '@/utils';
import SalesOrderDetailServiceBase from './sales-order-detail-service-base';


/**
 * 订单产品服务对象
 *
 * @export
 * @class SalesOrderDetailService
 * @extends {SalesOrderDetailServiceBase}
 */
export default class SalesOrderDetailService extends SalesOrderDetailServiceBase {

    /**
     * Creates an instance of  SalesOrderDetailService.
     * 
     * @param {*} [opts={}]
     * @memberof  SalesOrderDetailService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
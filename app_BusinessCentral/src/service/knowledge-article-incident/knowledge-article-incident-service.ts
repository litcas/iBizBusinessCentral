import { Http,Util } from '@/utils';
import KnowledgeArticleIncidentServiceBase from './knowledge-article-incident-service-base';


/**
 * 知识文章事件服务对象
 *
 * @export
 * @class KnowledgeArticleIncidentService
 * @extends {KnowledgeArticleIncidentServiceBase}
 */
export default class KnowledgeArticleIncidentService extends KnowledgeArticleIncidentServiceBase {

    /**
     * Creates an instance of  KnowledgeArticleIncidentService.
     * 
     * @param {*} [opts={}]
     * @memberof  KnowledgeArticleIncidentService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import { Http,Util } from '@/utils';
import EntityService from '../entity-service';
import CancelLogic from '@/service/invoice/cancel-logic';
import PaidLogic from '@/service/invoice/paid-logic';



/**
 * 发票服务对象基类
 *
 * @export
 * @class InvoiceServiceBase
 * @extends {EntityServie}
 */
export default class InvoiceServiceBase extends EntityService {

    /**
     * Creates an instance of  InvoiceServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  InvoiceServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof InvoiceServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='invoice';
        this.APPDEKEY = 'invoiceid';
        this.APPDENAME = 'invoices';
        this.APPDETEXT = 'invoicename';
        this.APPNAME = 'businesscentral';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof InvoiceServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder && context.invoice){
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/select`,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder && context.invoice){
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/select`,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder && context.invoice){
            let res:any = Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/select`,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder && context.invoice){
            let res:any = Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/select`,isloading);
            
            return res;
        }
        if(context.quote && context.salesorder && context.invoice){
            let res:any = Http.getInstance().get(`/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/select`,isloading);
            
            return res;
        }
        if(context.salesorder && context.invoice){
            let res:any = Http.getInstance().get(`/salesorders/${context.salesorder}/invoices/${context.invoice}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/invoices/${context.invoice}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof InvoiceServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_invoicedetails',JSON.stringify(res.data.invoicedetails?res.data.invoicedetails:[]));
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_invoicedetails',JSON.stringify(res.data.invoicedetails?res.data.invoicedetails:[]));
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_invoicedetails',JSON.stringify(res.data.invoicedetails?res.data.invoicedetails:[]));
            
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_invoicedetails',JSON.stringify(res.data.invoicedetails?res.data.invoicedetails:[]));
            
            return res;
        }
        if(context.quote && context.salesorder && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/quotes/${context.quote}/salesorders/${context.salesorder}/invoices`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_invoicedetails',JSON.stringify(res.data.invoicedetails?res.data.invoicedetails:[]));
            
            return res;
        }
        if(context.salesorder && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/salesorders/${context.salesorder}/invoices`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_invoicedetails',JSON.stringify(res.data.invoicedetails?res.data.invoicedetails:[]));
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/invoices`,data,isloading);
        this.tempStorage.setItem(tempContext.srfsessionkey+'_invoicedetails',JSON.stringify(res.data.invoicedetails?res.data.invoicedetails:[]));
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof InvoiceServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder && context.invoice){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}`,data,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder && context.invoice){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}`,data,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder && context.invoice){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}`,data,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder && context.invoice){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}`,data,isloading);
            
            return res;
        }
        if(context.quote && context.salesorder && context.invoice){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}`,data,isloading);
            
            return res;
        }
        if(context.salesorder && context.invoice){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/salesorders/${context.salesorder}/invoices/${context.invoice}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/invoices/${context.invoice}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof InvoiceServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder && context.invoice){
            let res:any = Http.getInstance().delete(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}`,isloading);
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder && context.invoice){
            let res:any = Http.getInstance().delete(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}`,isloading);
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder && context.invoice){
            let res:any = Http.getInstance().delete(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}`,isloading);
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder && context.invoice){
            let res:any = Http.getInstance().delete(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}`,isloading);
            return res;
        }
        if(context.quote && context.salesorder && context.invoice){
            let res:any = Http.getInstance().delete(`/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}`,isloading);
            return res;
        }
        if(context.salesorder && context.invoice){
            let res:any = Http.getInstance().delete(`/salesorders/${context.salesorder}/invoices/${context.invoice}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/invoices/${context.invoice}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof InvoiceServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder && context.invoice){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}`,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder && context.invoice){
            let res:any = await Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}`,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder && context.invoice){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}`,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder && context.invoice){
            let res:any = await Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}`,isloading);
            
            return res;
        }
        if(context.quote && context.salesorder && context.invoice){
            let res:any = await Http.getInstance().get(`/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}`,isloading);
            
            return res;
        }
        if(context.salesorder && context.invoice){
            let res:any = await Http.getInstance().get(`/salesorders/${context.salesorder}/invoices/${context.invoice}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/invoices/${context.invoice}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof InvoiceServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder && true){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/getdraft`,isloading);
            res.data.invoice = data.invoice;
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder && true){
            let res:any = await Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/getdraft`,isloading);
            res.data.invoice = data.invoice;
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder && true){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/getdraft`,isloading);
            res.data.invoice = data.invoice;
            
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder && true){
            let res:any = await Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/getdraft`,isloading);
            res.data.invoice = data.invoice;
            
            return res;
        }
        if(context.quote && context.salesorder && true){
            let res:any = await Http.getInstance().get(`/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/getdraft`,isloading);
            res.data.invoice = data.invoice;
            
            return res;
        }
        if(context.salesorder && true){
            let res:any = await Http.getInstance().get(`/salesorders/${context.salesorder}/invoices/getdraft`,isloading);
            res.data.invoice = data.invoice;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/invoices/getdraft`,isloading);
        res.data.invoice = data.invoice;
        
        return res;
    }

    /**
     * Cancel接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof InvoiceServiceBase
     */
    public async Cancel(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let appLogic:CancelLogic = new CancelLogic({context:JSON.parse(JSON.stringify(context)),data:JSON.parse(JSON.stringify(data))});
        const res = await appLogic.onExecute(context,data,isloading?true:false);
        return {status:200,data:res};
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof InvoiceServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder && context.invoice){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder && context.invoice){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder && context.invoice){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder && context.invoice){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.quote && context.salesorder && context.invoice){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.salesorder && context.invoice){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/salesorders/${context.salesorder}/invoices/${context.invoice}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/invoices/${context.invoice}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Finish接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof InvoiceServiceBase
     */
    public async Finish(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder && context.invoice){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/finish`,data,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder && context.invoice){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/finish`,data,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder && context.invoice){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/finish`,data,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder && context.invoice){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/finish`,data,isloading);
            
            return res;
        }
        if(context.quote && context.salesorder && context.invoice){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/finish`,data,isloading);
            
            return res;
        }
        if(context.salesorder && context.invoice){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/salesorders/${context.salesorder}/invoices/${context.invoice}/finish`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/invoices/${context.invoice}/finish`,data,isloading);
            return res;
    }

    /**
     * Paid接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof InvoiceServiceBase
     */
    public async Paid(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let appLogic:PaidLogic = new PaidLogic({context:JSON.parse(JSON.stringify(context)),data:JSON.parse(JSON.stringify(data))});
        const res = await appLogic.onExecute(context,data,isloading?true:false);
        return {status:200,data:res};
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof InvoiceServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder && context.invoice){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/save`,data,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder && context.invoice){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/save`,data,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder && context.invoice){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/save`,data,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder && context.invoice){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/save`,data,isloading);
            
            return res;
        }
        if(context.quote && context.salesorder && context.invoice){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/save`,data,isloading);
            
            return res;
        }
        if(context.salesorder && context.invoice){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/salesorders/${context.salesorder}/invoices/${context.invoice}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/invoices/${context.invoice}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchByParentKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof InvoiceServiceBase
     */
    public async FetchByParentKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/fetchbyparentkey`,tempData,isloading);
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/fetchbyparentkey`,tempData,isloading);
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/fetchbyparentkey`,tempData,isloading);
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/fetchbyparentkey`,tempData,isloading);
            return res;
        }
        if(context.quote && context.salesorder && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/fetchbyparentkey`,tempData,isloading);
            return res;
        }
        if(context.salesorder && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/salesorders/${context.salesorder}/invoices/fetchbyparentkey`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/invoices/fetchbyparentkey`,tempData,isloading);
        return res;
    }

    /**
     * FetchCancel接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof InvoiceServiceBase
     */
    public async FetchCancel(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/fetchcancel`,tempData,isloading);
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/fetchcancel`,tempData,isloading);
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/fetchcancel`,tempData,isloading);
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/fetchcancel`,tempData,isloading);
            return res;
        }
        if(context.quote && context.salesorder && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/fetchcancel`,tempData,isloading);
            return res;
        }
        if(context.salesorder && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/salesorders/${context.salesorder}/invoices/fetchcancel`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/invoices/fetchcancel`,tempData,isloading);
        return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof InvoiceServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.quote && context.salesorder && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.salesorder && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/salesorders/${context.salesorder}/invoices/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/invoices/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * FetchPaid接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof InvoiceServiceBase
     */
    public async FetchPaid(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/fetchpaid`,tempData,isloading);
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/fetchpaid`,tempData,isloading);
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/fetchpaid`,tempData,isloading);
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/fetchpaid`,tempData,isloading);
            return res;
        }
        if(context.quote && context.salesorder && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/fetchpaid`,tempData,isloading);
            return res;
        }
        if(context.salesorder && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/salesorders/${context.salesorder}/invoices/fetchpaid`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/invoices/fetchpaid`,tempData,isloading);
        return res;
    }
}
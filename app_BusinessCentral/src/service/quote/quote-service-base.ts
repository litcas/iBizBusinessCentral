import { Http,Util } from '@/utils';
import EntityService from '../entity-service';
import ActiveLogic from '@/service/quote/active-logic';
import ClosedLogic from '@/service/quote/closed-logic';
import WinLogic from '@/service/quote/win-logic';



/**
 * 报价单服务对象基类
 *
 * @export
 * @class QuoteServiceBase
 * @extends {EntityServie}
 */
export default class QuoteServiceBase extends EntityService {

    /**
     * Creates an instance of  QuoteServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  QuoteServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof QuoteServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='quote';
        this.APPDEKEY = 'quoteid';
        this.APPDENAME = 'quotes';
        this.APPDETEXT = 'quotename';
        this.APPNAME = 'businesscentral';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof QuoteServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote){
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/select`,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote){
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/select`,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote){
            let res:any = Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/select`,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote){
            let res:any = Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/${context.quote}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/quotes/${context.quote}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof QuoteServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_quotedetails',JSON.stringify(res.data.quotedetails?res.data.quotedetails:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_salesorders',JSON.stringify(res.data.salesorders?res.data.salesorders:[]));
            
            return res;
        }
        if(context.contact && context.opportunity && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_quotedetails',JSON.stringify(res.data.quotedetails?res.data.quotedetails:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_salesorders',JSON.stringify(res.data.salesorders?res.data.salesorders:[]));
            
            return res;
        }
        if(context.account && context.opportunity && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_quotedetails',JSON.stringify(res.data.quotedetails?res.data.quotedetails:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_salesorders',JSON.stringify(res.data.salesorders?res.data.salesorders:[]));
            
            return res;
        }
        if(context.opportunity && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/opportunities/${context.opportunity}/quotes`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_quotedetails',JSON.stringify(res.data.quotedetails?res.data.quotedetails:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_salesorders',JSON.stringify(res.data.salesorders?res.data.salesorders:[]));
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/quotes`,data,isloading);
        this.tempStorage.setItem(tempContext.srfsessionkey+'_quotedetails',JSON.stringify(res.data.quotedetails?res.data.quotedetails:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_salesorders',JSON.stringify(res.data.salesorders?res.data.salesorders:[]));
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof QuoteServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}`,data,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}`,data,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}`,data,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/opportunities/${context.opportunity}/quotes/${context.quote}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/quotes/${context.quote}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof QuoteServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote){
            let res:any = Http.getInstance().delete(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}`,isloading);
            return res;
        }
        if(context.contact && context.opportunity && context.quote){
            let res:any = Http.getInstance().delete(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}`,isloading);
            return res;
        }
        if(context.account && context.opportunity && context.quote){
            let res:any = Http.getInstance().delete(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}`,isloading);
            return res;
        }
        if(context.opportunity && context.quote){
            let res:any = Http.getInstance().delete(`/opportunities/${context.opportunity}/quotes/${context.quote}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/quotes/${context.quote}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof QuoteServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}`,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote){
            let res:any = await Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}`,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}`,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote){
            let res:any = await Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/${context.quote}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/quotes/${context.quote}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof QuoteServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && true){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/getdraft`,isloading);
            res.data.quote = data.quote;
            
            return res;
        }
        if(context.contact && context.opportunity && true){
            let res:any = await Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/getdraft`,isloading);
            res.data.quote = data.quote;
            
            return res;
        }
        if(context.account && context.opportunity && true){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/getdraft`,isloading);
            res.data.quote = data.quote;
            
            return res;
        }
        if(context.opportunity && true){
            let res:any = await Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/getdraft`,isloading);
            res.data.quote = data.quote;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/quotes/getdraft`,isloading);
        res.data.quote = data.quote;
        
        return res;
    }

    /**
     * Active接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof QuoteServiceBase
     */
    public async Active(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let appLogic:ActiveLogic = new ActiveLogic({context:JSON.parse(JSON.stringify(context)),data:JSON.parse(JSON.stringify(data))});
        const res = await appLogic.onExecute(context,data,isloading?true:false);
        return {status:200,data:res};
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof QuoteServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/opportunities/${context.opportunity}/quotes/${context.quote}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/quotes/${context.quote}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Close接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof QuoteServiceBase
     */
    public async Close(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let appLogic:ClosedLogic = new ClosedLogic({context:JSON.parse(JSON.stringify(context)),data:JSON.parse(JSON.stringify(data))});
        const res = await appLogic.onExecute(context,data,isloading?true:false);
        return {status:200,data:res};
    }

    /**
     * GenSalesOrder接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof QuoteServiceBase
     */
    public async GenSalesOrder(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/gensalesorder`,data,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/gensalesorder`,data,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/gensalesorder`,data,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/opportunities/${context.opportunity}/quotes/${context.quote}/gensalesorder`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/quotes/${context.quote}/gensalesorder`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof QuoteServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/save`,data,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/save`,data,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/save`,data,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/opportunities/${context.opportunity}/quotes/${context.quote}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/quotes/${context.quote}/save`,data,isloading);
            
            return res;
    }

    /**
     * Win接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof QuoteServiceBase
     */
    public async Win(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let appLogic:WinLogic = new WinLogic({context:JSON.parse(JSON.stringify(context)),data:JSON.parse(JSON.stringify(data))});
        const res = await appLogic.onExecute(context,data,isloading?true:false);
        return {status:200,data:res};
    }

    /**
     * FetchByParentKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof QuoteServiceBase
     */
    public async FetchByParentKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/fetchbyparentkey`,tempData,isloading);
            return res;
        }
        if(context.contact && context.opportunity && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/fetchbyparentkey`,tempData,isloading);
            return res;
        }
        if(context.account && context.opportunity && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/fetchbyparentkey`,tempData,isloading);
            return res;
        }
        if(context.opportunity && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/fetchbyparentkey`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/quotes/fetchbyparentkey`,tempData,isloading);
        return res;
    }

    /**
     * FetchClosed接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof QuoteServiceBase
     */
    public async FetchClosed(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/fetchclosed`,tempData,isloading);
            return res;
        }
        if(context.contact && context.opportunity && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/fetchclosed`,tempData,isloading);
            return res;
        }
        if(context.account && context.opportunity && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/fetchclosed`,tempData,isloading);
            return res;
        }
        if(context.opportunity && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/fetchclosed`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/quotes/fetchclosed`,tempData,isloading);
        return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof QuoteServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.contact && context.opportunity && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.account && context.opportunity && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.opportunity && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/quotes/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * FetchDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof QuoteServiceBase
     */
    public async FetchDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/fetchdraft`,tempData,isloading);
            return res;
        }
        if(context.contact && context.opportunity && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/fetchdraft`,tempData,isloading);
            return res;
        }
        if(context.account && context.opportunity && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/fetchdraft`,tempData,isloading);
            return res;
        }
        if(context.opportunity && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/fetchdraft`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/quotes/fetchdraft`,tempData,isloading);
        return res;
    }

    /**
     * FetchEffective接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof QuoteServiceBase
     */
    public async FetchEffective(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/fetcheffective`,tempData,isloading);
            return res;
        }
        if(context.contact && context.opportunity && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/fetcheffective`,tempData,isloading);
            return res;
        }
        if(context.account && context.opportunity && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/fetcheffective`,tempData,isloading);
            return res;
        }
        if(context.opportunity && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/fetcheffective`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/quotes/fetcheffective`,tempData,isloading);
        return res;
    }

    /**
     * FetchWin接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof QuoteServiceBase
     */
    public async FetchWin(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/fetchwin`,tempData,isloading);
            return res;
        }
        if(context.contact && context.opportunity && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/fetchwin`,tempData,isloading);
            return res;
        }
        if(context.account && context.opportunity && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/fetchwin`,tempData,isloading);
            return res;
        }
        if(context.opportunity && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/fetchwin`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/quotes/fetchwin`,tempData,isloading);
        return res;
    }
}
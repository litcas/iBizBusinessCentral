import { Http,Util } from '@/utils';
import WinLogicBase from './win-logic-base';

/**
 * 赢单
 *
 * @export
 * @class WinLogic
 */
export default class WinLogic extends WinLogicBase{

    /**
     * Creates an instance of  WinLogic
     * 
     * @param {*} [opts={}]
     * @memberof  WinLogic
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
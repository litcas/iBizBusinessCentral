import { Http,Util } from '@/utils';
import ClosedLogicBase from './closed-logic-base';

/**
 * 结束报价单
 *
 * @export
 * @class ClosedLogic
 */
export default class ClosedLogic extends ClosedLogicBase{

    /**
     * Creates an instance of  ClosedLogic
     * 
     * @param {*} [opts={}]
     * @memberof  ClosedLogic
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
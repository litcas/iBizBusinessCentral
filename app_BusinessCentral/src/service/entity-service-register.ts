/**
 * 实体数据服务注册中心
 *
 * @export
 * @class EntityServiceRegister
 */
export class EntityServiceRegister {

    /**
     * 所有实体数据服务Map
     *
     * @protected
     * @type {*}
     * @memberof EntityServiceRegister
     */
    protected allEntityService: Map<string, () => Promise<any>> = new Map();

    /**
     * 已加载实体数据服务Map缓存
     *
     * @protected
     * @type {Map<string, any>}
     * @memberof EntityServiceRegister
     */
    protected serviceCache: Map<string, any> = new Map();

    /**
     * Creates an instance of EntityServiceRegister.
     * @memberof EntityServiceRegister
     */
    constructor() {
        this.init();
    }

    /**
     * 初始化
     *
     * @protected
     * @memberof EntityServiceRegister
     */
    protected init(): void {
                this.allEntityService.set('dictoption', () => import('@/service/dict-option/dict-option-service'));
        this.allEntityService.set('uomschedule', () => import('@/service/uom-schedule/uom-schedule-service'));
        this.allEntityService.set('wfremodel', () => import('@/service/wfremodel/wfremodel-service'));
        this.allEntityService.set('productsalesliterature', () => import('@/service/product-sales-literature/product-sales-literature-service'));
        this.allEntityService.set('listcontact', () => import('@/service/list-contact/list-contact-service'));
        this.allEntityService.set('territory', () => import('@/service/territory/territory-service'));
        this.allEntityService.set('quotedetail', () => import('@/service/quote-detail/quote-detail-service'));
        this.allEntityService.set('sysapp', () => import('@/service/sys-app/sys-app-service'));
        this.allEntityService.set('opportunity', () => import('@/service/opportunity/opportunity-service'));
        this.allEntityService.set('entitlement', () => import('@/service/entitlement/entitlement-service'));
        this.allEntityService.set('discounttype', () => import('@/service/discount-type/discount-type-service'));
        this.allEntityService.set('salesliteratureitem', () => import('@/service/sales-literature-item/sales-literature-item-service'));
        this.allEntityService.set('campaigncampaign', () => import('@/service/campaign-campaign/campaign-campaign-service'));
        this.allEntityService.set('campaignlist', () => import('@/service/campaign-list/campaign-list-service'));
        this.allEntityService.set('ibizlist', () => import('@/service/ibiz-list/ibiz-list-service'));
        this.allEntityService.set('dictcatalog', () => import('@/service/dict-catalog/dict-catalog-service'));
        this.allEntityService.set('salesorder', () => import('@/service/sales-order/sales-order-service'));
        this.allEntityService.set('account', () => import('@/service/account/account-service'));
        this.allEntityService.set('languagelocale', () => import('@/service/language-locale/language-locale-service'));
        this.allEntityService.set('serviceappointment', () => import('@/service/service-appointment/service-appointment-service'));
        this.allEntityService.set('omhierarchy', () => import('@/service/omhierarchy/omhierarchy-service'));
        this.allEntityService.set('ibizservice', () => import('@/service/ibiz-service/ibiz-service-service'));
        this.allEntityService.set('goal', () => import('@/service/goal/goal-service'));
        this.allEntityService.set('opportunitycompetitor', () => import('@/service/opportunity-competitor/opportunity-competitor-service'));
        this.allEntityService.set('ibzdepartment', () => import('@/service/ibzdepartment/ibzdepartment-service'));
        this.allEntityService.set('quote', () => import('@/service/quote/quote-service'));
        this.allEntityService.set('transactioncurrency', () => import('@/service/transaction-currency/transaction-currency-service'));
        this.allEntityService.set('invoicedetail', () => import('@/service/invoice-detail/invoice-detail-service'));
        this.allEntityService.set('employee', () => import('@/service/employee/employee-service'));
        this.allEntityService.set('knowledgearticle', () => import('@/service/knowledge-article/knowledge-article-service'));
        this.allEntityService.set('jobsregistry', () => import('@/service/jobs-registry/jobs-registry-service'));
        this.allEntityService.set('wfgroup', () => import('@/service/wfgroup/wfgroup-service'));
        this.allEntityService.set('wfuser', () => import('@/service/wfuser/wfuser-service'));
        this.allEntityService.set('lead', () => import('@/service/lead/lead-service'));
        this.allEntityService.set('competitor', () => import('@/service/competitor/competitor-service'));
        this.allEntityService.set('campaign', () => import('@/service/campaign/campaign-service'));
        this.allEntityService.set('phonecall', () => import('@/service/phone-call/phone-call-service'));
        this.allEntityService.set('sysrolepermission', () => import('@/service/sys-role-permission/sys-role-permission-service'));
        this.allEntityService.set('appointment', () => import('@/service/appointment/appointment-service'));
        this.allEntityService.set('sysuser', () => import('@/service/sys-user/sys-user-service'));
        this.allEntityService.set('invoice', () => import('@/service/invoice/invoice-service'));
        this.allEntityService.set('jobslog', () => import('@/service/jobs-log/jobs-log-service'));
        this.allEntityService.set('incidentcustomer', () => import('@/service/incident-customer/incident-customer-service'));
        this.allEntityService.set('metric', () => import('@/service/metric/metric-service'));
        this.allEntityService.set('leadcompetitor', () => import('@/service/lead-competitor/lead-competitor-service'));
        this.allEntityService.set('syspermission', () => import('@/service/sys-permission/sys-permission-service'));
        this.allEntityService.set('pricelevel', () => import('@/service/price-level/price-level-service'));
        this.allEntityService.set('productpricelevel', () => import('@/service/product-price-level/product-price-level-service'));
        this.allEntityService.set('connectionrole', () => import('@/service/connection-role/connection-role-service'));
        this.allEntityService.set('wfprocessdefinition', () => import('@/service/wfprocess-definition/wfprocess-definition-service'));
        this.allEntityService.set('competitorsalesliterature', () => import('@/service/competitor-sales-literature/competitor-sales-literature-service'));
        this.allEntityService.set('legal', () => import('@/service/legal/legal-service'));
        this.allEntityService.set('knowledgearticleincident', () => import('@/service/knowledge-article-incident/knowledge-article-incident-service'));
        this.allEntityService.set('task', () => import('@/service/task/task-service'));
        this.allEntityService.set('ibzemployee', () => import('@/service/ibzemployee/ibzemployee-service'));
        this.allEntityService.set('salesorderdetail', () => import('@/service/sales-order-detail/sales-order-detail-service'));
        this.allEntityService.set('sysauthlog', () => import('@/service/sys-auth-log/sys-auth-log-service'));
        this.allEntityService.set('omhierarchypurposeref', () => import('@/service/omhierarchy-purpose-ref/omhierarchy-purpose-ref-service'));
        this.allEntityService.set('sysuserrole', () => import('@/service/sys-user-role/sys-user-role-service'));
        this.allEntityService.set('bulkoperation', () => import('@/service/bulk-operation/bulk-operation-service'));
        this.allEntityService.set('jobsinfo', () => import('@/service/jobs-info/jobs-info-service'));
        this.allEntityService.set('ibzorganization', () => import('@/service/ibzorganization/ibzorganization-service'));
        this.allEntityService.set('omhierarchycat', () => import('@/service/omhierarchy-cat/omhierarchy-cat-service'));
        this.allEntityService.set('productassociation', () => import('@/service/product-association/product-association-service'));
        this.allEntityService.set('campaignactivity', () => import('@/service/campaign-activity/campaign-activity-service'));
        this.allEntityService.set('activitypointer', () => import('@/service/activity-pointer/activity-pointer-service'));
        this.allEntityService.set('campaignresponse', () => import('@/service/campaign-response/campaign-response-service'));
        this.allEntityService.set('listaccount', () => import('@/service/list-account/list-account-service'));
        this.allEntityService.set('sysrole', () => import('@/service/sys-role/sys-role-service'));
        this.allEntityService.set('salesliterature', () => import('@/service/sales-literature/sales-literature-service'));
        this.allEntityService.set('subject', () => import('@/service/subject/subject-service'));
        this.allEntityService.set('operationunit', () => import('@/service/operation-unit/operation-unit-service'));
        this.allEntityService.set('ibzdeptmember', () => import('@/service/ibzdept-member/ibzdept-member-service'));
        this.allEntityService.set('letter', () => import('@/service/letter/letter-service'));
        this.allEntityService.set('uom', () => import('@/service/uom/uom-service'));
        this.allEntityService.set('product', () => import('@/service/product/product-service'));
        this.allEntityService.set('multipickdata', () => import('@/service/multi-pick-data/multi-pick-data-service'));
        this.allEntityService.set('wfmember', () => import('@/service/wfmember/wfmember-service'));
        this.allEntityService.set('opportunityproduct', () => import('@/service/opportunity-product/opportunity-product-service'));
        this.allEntityService.set('contact', () => import('@/service/contact/contact-service'));
        this.allEntityService.set('omhierarchypurpose', () => import('@/service/omhierarchy-purpose/omhierarchy-purpose-service'));
        this.allEntityService.set('incident', () => import('@/service/incident/incident-service'));
        this.allEntityService.set('fax', () => import('@/service/fax/fax-service'));
        this.allEntityService.set('competitorproduct', () => import('@/service/competitor-product/competitor-product-service'));
        this.allEntityService.set('listlead', () => import('@/service/list-lead/list-lead-service'));
        this.allEntityService.set('productsubstitute', () => import('@/service/product-substitute/product-substitute-service'));
        this.allEntityService.set('websitecontent', () => import('@/service/web-site-content/web-site-content-service'));
        this.allEntityService.set('organization', () => import('@/service/organization/organization-service'));
        this.allEntityService.set('email', () => import('@/service/email/email-service'));
        this.allEntityService.set('ibzpost', () => import('@/service/ibzpost/ibzpost-service'));
        this.allEntityService.set('connection', () => import('@/service/connection/connection-service'));
    }

    /**
     * 加载实体数据服务
     *
     * @protected
     * @param {string} serviceName
     * @returns {Promise<any>}
     * @memberof EntityServiceRegister
     */
    protected async loadService(serviceName: string): Promise<any> {
        const service = this.allEntityService.get(serviceName);
        if (service) {
            return service();
        }
    }

    /**
     * 获取应用实体数据服务
     *
     * @param {string} name
     * @returns {Promise<any>}
     * @memberof EntityServiceRegister
     */
    public async getService(name: string): Promise<any> {
        if (this.serviceCache.has(name)) {
            return this.serviceCache.get(name);
        }
        const entityService: any = await this.loadService(name);
        if (entityService && entityService.default) {
            const instance: any = new entityService.default();
            this.serviceCache.set(name, instance);
            return instance;
        }
    }

}
export const entityServiceRegister: EntityServiceRegister = new EntityServiceRegister();
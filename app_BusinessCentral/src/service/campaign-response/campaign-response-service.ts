import { Http,Util } from '@/utils';
import CampaignResponseServiceBase from './campaign-response-service-base';


/**
 * 市场活动响应服务对象
 *
 * @export
 * @class CampaignResponseService
 * @extends {CampaignResponseServiceBase}
 */
export default class CampaignResponseService extends CampaignResponseServiceBase {

    /**
     * Creates an instance of  CampaignResponseService.
     * 
     * @param {*} [opts={}]
     * @memberof  CampaignResponseService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
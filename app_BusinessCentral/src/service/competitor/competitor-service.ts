import { Http,Util } from '@/utils';
import CompetitorServiceBase from './competitor-service-base';


/**
 * 竞争对手服务对象
 *
 * @export
 * @class CompetitorService
 * @extends {CompetitorServiceBase}
 */
export default class CompetitorService extends CompetitorServiceBase {

    /**
     * Creates an instance of  CompetitorService.
     * 
     * @param {*} [opts={}]
     * @memberof  CompetitorService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import { Http,Util } from '@/utils';
import ListLeadServiceBase from './list-lead-service-base';


/**
 * 营销列表-潜在客户服务对象
 *
 * @export
 * @class ListLeadService
 * @extends {ListLeadServiceBase}
 */
export default class ListLeadService extends ListLeadServiceBase {

    /**
     * Creates an instance of  ListLeadService.
     * 
     * @param {*} [opts={}]
     * @memberof  ListLeadService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
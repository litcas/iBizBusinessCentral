import { Http,Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 营销列表-潜在客户服务对象基类
 *
 * @export
 * @class ListLeadServiceBase
 * @extends {EntityServie}
 */
export default class ListLeadServiceBase extends EntityService {

    /**
     * Creates an instance of  ListLeadServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  ListLeadServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof ListLeadServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='listlead';
        this.APPDEKEY = 'relationshipsid';
        this.APPDENAME = 'listleads';
        this.APPDETEXT = 'relationshipsname';
        this.APPNAME = 'businesscentral';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ListLeadServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.lead && context.listlead){
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/leads/${context.lead}/listleads/${context.listlead}/select`,isloading);
            
            return res;
        }
        if(context.contact && context.lead && context.listlead){
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/leads/${context.lead}/listleads/${context.listlead}/select`,isloading);
            
            return res;
        }
        if(context.campaign && context.lead && context.listlead){
            let res:any = Http.getInstance().get(`/campaigns/${context.campaign}/leads/${context.lead}/listleads/${context.listlead}/select`,isloading);
            
            return res;
        }
        if(context.account && context.lead && context.listlead){
            let res:any = Http.getInstance().get(`/accounts/${context.account}/leads/${context.lead}/listleads/${context.listlead}/select`,isloading);
            
            return res;
        }
        if(context.ibizlist && context.listlead){
            let res:any = Http.getInstance().get(`/ibizlists/${context.ibizlist}/listleads/${context.listlead}/select`,isloading);
            
            return res;
        }
        if(context.lead && context.listlead){
            let res:any = Http.getInstance().get(`/leads/${context.lead}/listleads/${context.listlead}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/listleads/${context.listlead}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ListLeadServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.lead && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/leads/${context.lead}/listleads`,data,isloading);
            
            return res;
        }
        if(context.contact && context.lead && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/leads/${context.lead}/listleads`,data,isloading);
            
            return res;
        }
        if(context.campaign && context.lead && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/campaigns/${context.campaign}/leads/${context.lead}/listleads`,data,isloading);
            
            return res;
        }
        if(context.account && context.lead && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/leads/${context.lead}/listleads`,data,isloading);
            
            return res;
        }
        if(context.ibizlist && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/ibizlists/${context.ibizlist}/listleads`,data,isloading);
            
            return res;
        }
        if(context.lead && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/leads/${context.lead}/listleads`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/listleads`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ListLeadServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.lead && context.listlead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/accounts/${context.account}/contacts/${context.contact}/leads/${context.lead}/listleads/${context.listlead}`,data,isloading);
            
            return res;
        }
        if(context.contact && context.lead && context.listlead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/contacts/${context.contact}/leads/${context.lead}/listleads/${context.listlead}`,data,isloading);
            
            return res;
        }
        if(context.campaign && context.lead && context.listlead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/campaigns/${context.campaign}/leads/${context.lead}/listleads/${context.listlead}`,data,isloading);
            
            return res;
        }
        if(context.account && context.lead && context.listlead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/accounts/${context.account}/leads/${context.lead}/listleads/${context.listlead}`,data,isloading);
            
            return res;
        }
        if(context.ibizlist && context.listlead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/ibizlists/${context.ibizlist}/listleads/${context.listlead}`,data,isloading);
            
            return res;
        }
        if(context.lead && context.listlead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/leads/${context.lead}/listleads/${context.listlead}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/listleads/${context.listlead}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ListLeadServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.lead && context.listlead){
            let res:any = Http.getInstance().delete(`/accounts/${context.account}/contacts/${context.contact}/leads/${context.lead}/listleads/${context.listlead}`,isloading);
            return res;
        }
        if(context.contact && context.lead && context.listlead){
            let res:any = Http.getInstance().delete(`/contacts/${context.contact}/leads/${context.lead}/listleads/${context.listlead}`,isloading);
            return res;
        }
        if(context.campaign && context.lead && context.listlead){
            let res:any = Http.getInstance().delete(`/campaigns/${context.campaign}/leads/${context.lead}/listleads/${context.listlead}`,isloading);
            return res;
        }
        if(context.account && context.lead && context.listlead){
            let res:any = Http.getInstance().delete(`/accounts/${context.account}/leads/${context.lead}/listleads/${context.listlead}`,isloading);
            return res;
        }
        if(context.ibizlist && context.listlead){
            let res:any = Http.getInstance().delete(`/ibizlists/${context.ibizlist}/listleads/${context.listlead}`,isloading);
            return res;
        }
        if(context.lead && context.listlead){
            let res:any = Http.getInstance().delete(`/leads/${context.lead}/listleads/${context.listlead}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/listleads/${context.listlead}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ListLeadServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.lead && context.listlead){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/leads/${context.lead}/listleads/${context.listlead}`,isloading);
            
            return res;
        }
        if(context.contact && context.lead && context.listlead){
            let res:any = await Http.getInstance().get(`/contacts/${context.contact}/leads/${context.lead}/listleads/${context.listlead}`,isloading);
            
            return res;
        }
        if(context.campaign && context.lead && context.listlead){
            let res:any = await Http.getInstance().get(`/campaigns/${context.campaign}/leads/${context.lead}/listleads/${context.listlead}`,isloading);
            
            return res;
        }
        if(context.account && context.lead && context.listlead){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/leads/${context.lead}/listleads/${context.listlead}`,isloading);
            
            return res;
        }
        if(context.ibizlist && context.listlead){
            let res:any = await Http.getInstance().get(`/ibizlists/${context.ibizlist}/listleads/${context.listlead}`,isloading);
            
            return res;
        }
        if(context.lead && context.listlead){
            let res:any = await Http.getInstance().get(`/leads/${context.lead}/listleads/${context.listlead}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/listleads/${context.listlead}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ListLeadServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.lead && true){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/leads/${context.lead}/listleads/getdraft`,isloading);
            res.data.listlead = data.listlead;
            
            return res;
        }
        if(context.contact && context.lead && true){
            let res:any = await Http.getInstance().get(`/contacts/${context.contact}/leads/${context.lead}/listleads/getdraft`,isloading);
            res.data.listlead = data.listlead;
            
            return res;
        }
        if(context.campaign && context.lead && true){
            let res:any = await Http.getInstance().get(`/campaigns/${context.campaign}/leads/${context.lead}/listleads/getdraft`,isloading);
            res.data.listlead = data.listlead;
            
            return res;
        }
        if(context.account && context.lead && true){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/leads/${context.lead}/listleads/getdraft`,isloading);
            res.data.listlead = data.listlead;
            
            return res;
        }
        if(context.ibizlist && true){
            let res:any = await Http.getInstance().get(`/ibizlists/${context.ibizlist}/listleads/getdraft`,isloading);
            res.data.listlead = data.listlead;
            
            return res;
        }
        if(context.lead && true){
            let res:any = await Http.getInstance().get(`/leads/${context.lead}/listleads/getdraft`,isloading);
            res.data.listlead = data.listlead;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/listleads/getdraft`,isloading);
        res.data.listlead = data.listlead;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ListLeadServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.lead && context.listlead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/leads/${context.lead}/listleads/${context.listlead}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.contact && context.lead && context.listlead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/leads/${context.lead}/listleads/${context.listlead}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.campaign && context.lead && context.listlead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/campaigns/${context.campaign}/leads/${context.lead}/listleads/${context.listlead}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.account && context.lead && context.listlead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/leads/${context.lead}/listleads/${context.listlead}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.ibizlist && context.listlead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/ibizlists/${context.ibizlist}/listleads/${context.listlead}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.lead && context.listlead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/leads/${context.lead}/listleads/${context.listlead}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/listleads/${context.listlead}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ListLeadServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.lead && context.listlead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/leads/${context.lead}/listleads/${context.listlead}/save`,data,isloading);
            
            return res;
        }
        if(context.contact && context.lead && context.listlead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/leads/${context.lead}/listleads/${context.listlead}/save`,data,isloading);
            
            return res;
        }
        if(context.campaign && context.lead && context.listlead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/campaigns/${context.campaign}/leads/${context.lead}/listleads/${context.listlead}/save`,data,isloading);
            
            return res;
        }
        if(context.account && context.lead && context.listlead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/leads/${context.lead}/listleads/${context.listlead}/save`,data,isloading);
            
            return res;
        }
        if(context.ibizlist && context.listlead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/ibizlists/${context.ibizlist}/listleads/${context.listlead}/save`,data,isloading);
            
            return res;
        }
        if(context.lead && context.listlead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/leads/${context.lead}/listleads/${context.listlead}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/listleads/${context.listlead}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ListLeadServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.lead && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/leads/${context.lead}/listleads/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.contact && context.lead && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/leads/${context.lead}/listleads/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.campaign && context.lead && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/campaigns/${context.campaign}/leads/${context.lead}/listleads/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.account && context.lead && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/leads/${context.lead}/listleads/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.ibizlist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/ibizlists/${context.ibizlist}/listleads/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.lead && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/leads/${context.lead}/listleads/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/listleads/fetchdefault`,tempData,isloading);
        return res;
    }
}
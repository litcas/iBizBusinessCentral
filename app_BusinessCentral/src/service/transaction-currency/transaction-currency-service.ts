import { Http,Util } from '@/utils';
import TransactionCurrencyServiceBase from './transaction-currency-service-base';


/**
 * 货币服务对象
 *
 * @export
 * @class TransactionCurrencyService
 * @extends {TransactionCurrencyServiceBase}
 */
export default class TransactionCurrencyService extends TransactionCurrencyServiceBase {

    /**
     * Creates an instance of  TransactionCurrencyService.
     * 
     * @param {*} [opts={}]
     * @memberof  TransactionCurrencyService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
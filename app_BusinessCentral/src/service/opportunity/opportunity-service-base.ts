import { Http,Util } from '@/utils';
import EntityService from '../entity-service';
import ActiveLogic from '@/service/opportunity/active-logic';
import LostLogic from '@/service/opportunity/lost-logic';
import WinLogic from '@/service/opportunity/win-logic';



/**
 * 商机服务对象基类
 *
 * @export
 * @class OpportunityServiceBase
 * @extends {EntityServie}
 */
export default class OpportunityServiceBase extends EntityService {

    /**
     * Creates an instance of  OpportunityServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  OpportunityServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof OpportunityServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='opportunity';
        this.APPDEKEY = 'opportunityid';
        this.APPDENAME = 'opportunities';
        this.APPDETEXT = 'opportunityname';
        this.APPNAME = 'businesscentral';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OpportunityServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity){
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/select`,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity){
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/select`,isloading);
            
            return res;
        }
        if(context.account && context.opportunity){
            let res:any = Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/opportunities/${context.opportunity}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OpportunityServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/opportunities`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_opportunitycompetitors',JSON.stringify(res.data.opportunitycompetitors?res.data.opportunitycompetitors:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_opportunityproducts',JSON.stringify(res.data.opportunityproducts?res.data.opportunityproducts:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_quotes',JSON.stringify(res.data.quotes?res.data.quotes:[]));
            
            return res;
        }
        if(context.contact && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/opportunities`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_opportunitycompetitors',JSON.stringify(res.data.opportunitycompetitors?res.data.opportunitycompetitors:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_opportunityproducts',JSON.stringify(res.data.opportunityproducts?res.data.opportunityproducts:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_quotes',JSON.stringify(res.data.quotes?res.data.quotes:[]));
            
            return res;
        }
        if(context.account && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/opportunities`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_opportunitycompetitors',JSON.stringify(res.data.opportunitycompetitors?res.data.opportunitycompetitors:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_opportunityproducts',JSON.stringify(res.data.opportunityproducts?res.data.opportunityproducts:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_quotes',JSON.stringify(res.data.quotes?res.data.quotes:[]));
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/opportunities`,data,isloading);
        this.tempStorage.setItem(tempContext.srfsessionkey+'_opportunitycompetitors',JSON.stringify(res.data.opportunitycompetitors?res.data.opportunitycompetitors:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_opportunityproducts',JSON.stringify(res.data.opportunityproducts?res.data.opportunityproducts:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_quotes',JSON.stringify(res.data.quotes?res.data.quotes:[]));
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OpportunityServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}`,data,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/contacts/${context.contact}/opportunities/${context.opportunity}`,data,isloading);
            
            return res;
        }
        if(context.account && context.opportunity){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/accounts/${context.account}/opportunities/${context.opportunity}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/opportunities/${context.opportunity}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OpportunityServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity){
            let res:any = Http.getInstance().delete(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}`,isloading);
            return res;
        }
        if(context.contact && context.opportunity){
            let res:any = Http.getInstance().delete(`/contacts/${context.contact}/opportunities/${context.opportunity}`,isloading);
            return res;
        }
        if(context.account && context.opportunity){
            let res:any = Http.getInstance().delete(`/accounts/${context.account}/opportunities/${context.opportunity}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/opportunities/${context.opportunity}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OpportunityServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}`,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity){
            let res:any = await Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}`,isloading);
            
            return res;
        }
        if(context.account && context.opportunity){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/opportunities/${context.opportunity}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OpportunityServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && true){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/getdraft`,isloading);
            res.data.opportunity = data.opportunity;
            
            return res;
        }
        if(context.contact && true){
            let res:any = await Http.getInstance().get(`/contacts/${context.contact}/opportunities/getdraft`,isloading);
            res.data.opportunity = data.opportunity;
            
            return res;
        }
        if(context.account && true){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/opportunities/getdraft`,isloading);
            res.data.opportunity = data.opportunity;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/opportunities/getdraft`,isloading);
        res.data.opportunity = data.opportunity;
        
        return res;
    }

    /**
     * Active接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OpportunityServiceBase
     */
    public async Active(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let appLogic:ActiveLogic = new ActiveLogic({context:JSON.parse(JSON.stringify(context)),data:JSON.parse(JSON.stringify(data))});
        const res = await appLogic.onExecute(context,data,isloading?true:false);
        return {status:200,data:res};
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OpportunityServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/opportunities/${context.opportunity}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.account && context.opportunity){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/opportunities/${context.opportunity}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/opportunities/${context.opportunity}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Lose接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OpportunityServiceBase
     */
    public async Lose(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let appLogic:LostLogic = new LostLogic({context:JSON.parse(JSON.stringify(context)),data:JSON.parse(JSON.stringify(data))});
        const res = await appLogic.onExecute(context,data,isloading?true:false);
        return {status:200,data:res};
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OpportunityServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/save`,data,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/opportunities/${context.opportunity}/save`,data,isloading);
            
            return res;
        }
        if(context.account && context.opportunity){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/opportunities/${context.opportunity}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/opportunities/${context.opportunity}/save`,data,isloading);
            
            return res;
    }

    /**
     * Win接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OpportunityServiceBase
     */
    public async Win(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let appLogic:WinLogic = new WinLogic({context:JSON.parse(JSON.stringify(context)),data:JSON.parse(JSON.stringify(data))});
        const res = await appLogic.onExecute(context,data,isloading?true:false);
        return {status:200,data:res};
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OpportunityServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.contact && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/opportunities/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.account && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/opportunities/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/opportunities/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * FetchLost接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OpportunityServiceBase
     */
    public async FetchLost(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/fetchlost`,tempData,isloading);
            return res;
        }
        if(context.contact && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/opportunities/fetchlost`,tempData,isloading);
            return res;
        }
        if(context.account && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/opportunities/fetchlost`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/opportunities/fetchlost`,tempData,isloading);
        return res;
    }

    /**
     * FetchTop5接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OpportunityServiceBase
     */
    public async FetchTop5(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/fetchtop5`,tempData,isloading);
            return res;
        }
        if(context.contact && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/opportunities/fetchtop5`,tempData,isloading);
            return res;
        }
        if(context.account && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/opportunities/fetchtop5`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/opportunities/fetchtop5`,tempData,isloading);
        return res;
    }

    /**
     * FetchWin接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OpportunityServiceBase
     */
    public async FetchWin(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/fetchwin`,tempData,isloading);
            return res;
        }
        if(context.contact && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/opportunities/fetchwin`,tempData,isloading);
            return res;
        }
        if(context.account && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/opportunities/fetchwin`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/opportunities/fetchwin`,tempData,isloading);
        return res;
    }
}
import { Http,Util } from '@/utils';
import OpportunityServiceBase from './opportunity-service-base';


/**
 * 商机服务对象
 *
 * @export
 * @class OpportunityService
 * @extends {OpportunityServiceBase}
 */
export default class OpportunityService extends OpportunityServiceBase {

    /**
     * Creates an instance of  OpportunityService.
     * 
     * @param {*} [opts={}]
     * @memberof  OpportunityService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
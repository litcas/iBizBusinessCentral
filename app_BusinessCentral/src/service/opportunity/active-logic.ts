import { Http,Util } from '@/utils';
import ActiveLogicBase from './active-logic-base';

/**
 * 重新开启商机
 *
 * @export
 * @class ActiveLogic
 */
export default class ActiveLogic extends ActiveLogicBase{

    /**
     * Creates an instance of  ActiveLogic
     * 
     * @param {*} [opts={}]
     * @memberof  ActiveLogic
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
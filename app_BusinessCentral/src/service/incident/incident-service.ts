import { Http,Util } from '@/utils';
import IncidentServiceBase from './incident-service-base';


/**
 * 案例服务对象
 *
 * @export
 * @class IncidentService
 * @extends {IncidentServiceBase}
 */
export default class IncidentService extends IncidentServiceBase {

    /**
     * Creates an instance of  IncidentService.
     * 
     * @param {*} [opts={}]
     * @memberof  IncidentService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
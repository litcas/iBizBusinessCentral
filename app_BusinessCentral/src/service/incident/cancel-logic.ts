import { Http,Util } from '@/utils';
import CancelLogicBase from './cancel-logic-base';

/**
 * 取消案例
 *
 * @export
 * @class CancelLogic
 */
export default class CancelLogic extends CancelLogicBase{

    /**
     * Creates an instance of  CancelLogic
     * 
     * @param {*} [opts={}]
     * @memberof  CancelLogic
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
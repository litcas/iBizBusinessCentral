import { Http,Util } from '@/utils';
import ResolveLogicBase from './resolve-logic-base';

/**
 * 解决案例
 *
 * @export
 * @class ResolveLogic
 */
export default class ResolveLogic extends ResolveLogicBase{

    /**
     * Creates an instance of  ResolveLogic
     * 
     * @param {*} [opts={}]
     * @memberof  ResolveLogic
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
import { Http,Util } from '@/utils';
import MultiPickDataServiceBase from './multi-pick-data-service-base';


/**
 * 多类选择实体服务对象
 *
 * @export
 * @class MultiPickDataService
 * @extends {MultiPickDataServiceBase}
 */
export default class MultiPickDataService extends MultiPickDataServiceBase {

    /**
     * Creates an instance of  MultiPickDataService.
     * 
     * @param {*} [opts={}]
     * @memberof  MultiPickDataService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
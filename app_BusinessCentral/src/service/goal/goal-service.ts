import { Http,Util } from '@/utils';
import GoalServiceBase from './goal-service-base';


/**
 * 目标服务对象
 *
 * @export
 * @class GoalService
 * @extends {GoalServiceBase}
 */
export default class GoalService extends GoalServiceBase {

    /**
     * Creates an instance of  GoalService.
     * 
     * @param {*} [opts={}]
     * @memberof  GoalService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import { Http,Util } from '@/utils';
import KnowledgeArticleServiceBase from './knowledge-article-service-base';


/**
 * 知识文章服务对象
 *
 * @export
 * @class KnowledgeArticleService
 * @extends {KnowledgeArticleServiceBase}
 */
export default class KnowledgeArticleService extends KnowledgeArticleServiceBase {

    /**
     * Creates an instance of  KnowledgeArticleService.
     * 
     * @param {*} [opts={}]
     * @memberof  KnowledgeArticleService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
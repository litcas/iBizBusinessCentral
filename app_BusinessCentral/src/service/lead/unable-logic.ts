import { Http,Util } from '@/utils';
import UnableLogicBase from './unable-logic-base';

/**
 * 无法联系
 *
 * @export
 * @class UnableLogic
 */
export default class UnableLogic extends UnableLogicBase{

    /**
     * Creates an instance of  UnableLogic
     * 
     * @param {*} [opts={}]
     * @memberof  UnableLogic
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
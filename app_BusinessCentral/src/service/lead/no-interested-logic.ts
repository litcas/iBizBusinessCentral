import { Http,Util } from '@/utils';
import NoInterestedLogicBase from './no-interested-logic-base';

/**
 * 不再感兴趣
 *
 * @export
 * @class NoInterestedLogic
 */
export default class NoInterestedLogic extends NoInterestedLogicBase{

    /**
     * Creates an instance of  NoInterestedLogic
     * 
     * @param {*} [opts={}]
     * @memberof  NoInterestedLogic
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
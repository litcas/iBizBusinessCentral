import { Http,Util } from '@/utils';
import LostOrderLogicBase from './lost-order-logic-base';

/**
 * 丢单
 *
 * @export
 * @class LostOrderLogic
 */
export default class LostOrderLogic extends LostOrderLogicBase{

    /**
     * Creates an instance of  LostOrderLogic
     * 
     * @param {*} [opts={}]
     * @memberof  LostOrderLogic
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
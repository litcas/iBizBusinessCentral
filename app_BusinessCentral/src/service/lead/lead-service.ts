import { Http,Util } from '@/utils';
import LeadServiceBase from './lead-service-base';


/**
 * 潜在顾客服务对象
 *
 * @export
 * @class LeadService
 * @extends {LeadServiceBase}
 */
export default class LeadService extends LeadServiceBase {

    /**
     * Creates an instance of  LeadService.
     * 
     * @param {*} [opts={}]
     * @memberof  LeadService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
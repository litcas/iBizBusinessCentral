import { Http,Util } from '@/utils';
import EmployeeServiceBase from './employee-service-base';


/**
 * 员工服务对象
 *
 * @export
 * @class EmployeeService
 * @extends {EmployeeServiceBase}
 */
export default class EmployeeService extends EmployeeServiceBase {

    /**
     * Creates an instance of  EmployeeService.
     * 
     * @param {*} [opts={}]
     * @memberof  EmployeeService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import { Http,Util } from '@/utils';
import CompetitorProductServiceBase from './competitor-product-service-base';


/**
 * 竞争对手产品服务对象
 *
 * @export
 * @class CompetitorProductService
 * @extends {CompetitorProductServiceBase}
 */
export default class CompetitorProductService extends CompetitorProductServiceBase {

    /**
     * Creates an instance of  CompetitorProductService.
     * 
     * @param {*} [opts={}]
     * @memberof  CompetitorProductService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
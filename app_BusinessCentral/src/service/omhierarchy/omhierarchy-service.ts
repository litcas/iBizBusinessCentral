import { Http,Util } from '@/utils';
import OMHierarchyServiceBase from './omhierarchy-service-base';


/**
 * 组织层次结构服务对象
 *
 * @export
 * @class OMHierarchyService
 * @extends {OMHierarchyServiceBase}
 */
export default class OMHierarchyService extends OMHierarchyServiceBase {

    /**
     * Creates an instance of  OMHierarchyService.
     * 
     * @param {*} [opts={}]
     * @memberof  OMHierarchyService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import { Http,Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 组织层次结构服务对象基类
 *
 * @export
 * @class OMHierarchyServiceBase
 * @extends {EntityServie}
 */
export default class OMHierarchyServiceBase extends EntityService {

    /**
     * Creates an instance of  OMHierarchyServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  OMHierarchyServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof OMHierarchyServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='omhierarchy';
        this.APPDEKEY = 'omhierarchyid';
        this.APPDENAME = 'omhierarchies';
        this.APPDETEXT = 'omhierarchyname';
        this.APPNAME = 'businesscentral';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OMHierarchyServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.operationunit && context.omhierarchy){
            let res:any = Http.getInstance().get(`/operationunits/${context.operationunit}/omhierarchies/${context.omhierarchy}/select`,isloading);
            
            return res;
        }
        if(context.omhierarchycat && context.omhierarchy){
            let res:any = Http.getInstance().get(`/omhierarchycats/${context.omhierarchycat}/omhierarchies/${context.omhierarchy}/select`,isloading);
            
            return res;
        }
        if(context.legal && context.omhierarchy){
            let res:any = Http.getInstance().get(`/legals/${context.legal}/omhierarchies/${context.omhierarchy}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/omhierarchies/${context.omhierarchy}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OMHierarchyServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.operationunit && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/operationunits/${context.operationunit}/omhierarchies`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_omhierarchies',JSON.stringify(res.data.omhierarchies?res.data.omhierarchies:[]));
            
            return res;
        }
        if(context.omhierarchycat && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/omhierarchycats/${context.omhierarchycat}/omhierarchies`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_omhierarchies',JSON.stringify(res.data.omhierarchies?res.data.omhierarchies:[]));
            
            return res;
        }
        if(context.legal && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/legals/${context.legal}/omhierarchies`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_omhierarchies',JSON.stringify(res.data.omhierarchies?res.data.omhierarchies:[]));
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/omhierarchies`,data,isloading);
        this.tempStorage.setItem(tempContext.srfsessionkey+'_omhierarchies',JSON.stringify(res.data.omhierarchies?res.data.omhierarchies:[]));
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OMHierarchyServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.operationunit && context.omhierarchy){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/operationunits/${context.operationunit}/omhierarchies/${context.omhierarchy}`,data,isloading);
            
            return res;
        }
        if(context.omhierarchycat && context.omhierarchy){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/omhierarchycats/${context.omhierarchycat}/omhierarchies/${context.omhierarchy}`,data,isloading);
            
            return res;
        }
        if(context.legal && context.omhierarchy){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/legals/${context.legal}/omhierarchies/${context.omhierarchy}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/omhierarchies/${context.omhierarchy}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OMHierarchyServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.operationunit && context.omhierarchy){
            let res:any = Http.getInstance().delete(`/operationunits/${context.operationunit}/omhierarchies/${context.omhierarchy}`,isloading);
            return res;
        }
        if(context.omhierarchycat && context.omhierarchy){
            let res:any = Http.getInstance().delete(`/omhierarchycats/${context.omhierarchycat}/omhierarchies/${context.omhierarchy}`,isloading);
            return res;
        }
        if(context.legal && context.omhierarchy){
            let res:any = Http.getInstance().delete(`/legals/${context.legal}/omhierarchies/${context.omhierarchy}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/omhierarchies/${context.omhierarchy}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OMHierarchyServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.operationunit && context.omhierarchy){
            let res:any = await Http.getInstance().get(`/operationunits/${context.operationunit}/omhierarchies/${context.omhierarchy}`,isloading);
            
            return res;
        }
        if(context.omhierarchycat && context.omhierarchy){
            let res:any = await Http.getInstance().get(`/omhierarchycats/${context.omhierarchycat}/omhierarchies/${context.omhierarchy}`,isloading);
            
            return res;
        }
        if(context.legal && context.omhierarchy){
            let res:any = await Http.getInstance().get(`/legals/${context.legal}/omhierarchies/${context.omhierarchy}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/omhierarchies/${context.omhierarchy}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OMHierarchyServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.operationunit && true){
            let res:any = await Http.getInstance().get(`/operationunits/${context.operationunit}/omhierarchies/getdraft`,isloading);
            res.data.omhierarchy = data.omhierarchy;
            
            return res;
        }
        if(context.omhierarchycat && true){
            let res:any = await Http.getInstance().get(`/omhierarchycats/${context.omhierarchycat}/omhierarchies/getdraft`,isloading);
            res.data.omhierarchy = data.omhierarchy;
            
            return res;
        }
        if(context.legal && true){
            let res:any = await Http.getInstance().get(`/legals/${context.legal}/omhierarchies/getdraft`,isloading);
            res.data.omhierarchy = data.omhierarchy;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/omhierarchies/getdraft`,isloading);
        res.data.omhierarchy = data.omhierarchy;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OMHierarchyServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.operationunit && context.omhierarchy){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/operationunits/${context.operationunit}/omhierarchies/${context.omhierarchy}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.omhierarchycat && context.omhierarchy){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/omhierarchycats/${context.omhierarchycat}/omhierarchies/${context.omhierarchy}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.legal && context.omhierarchy){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/legals/${context.legal}/omhierarchies/${context.omhierarchy}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/omhierarchies/${context.omhierarchy}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OMHierarchyServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.operationunit && context.omhierarchy){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/operationunits/${context.operationunit}/omhierarchies/${context.omhierarchy}/save`,data,isloading);
            
            return res;
        }
        if(context.omhierarchycat && context.omhierarchy){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/omhierarchycats/${context.omhierarchycat}/omhierarchies/${context.omhierarchy}/save`,data,isloading);
            
            return res;
        }
        if(context.legal && context.omhierarchy){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/legals/${context.legal}/omhierarchies/${context.omhierarchy}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/omhierarchies/${context.omhierarchy}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OMHierarchyServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.operationunit && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/operationunits/${context.operationunit}/omhierarchies/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.omhierarchycat && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/omhierarchycats/${context.omhierarchycat}/omhierarchies/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.legal && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/legals/${context.legal}/omhierarchies/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/omhierarchies/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * FetchRootOrg接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OMHierarchyServiceBase
     */
    public async FetchRootOrg(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.operationunit && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/operationunits/${context.operationunit}/omhierarchies/fetchrootorg`,tempData,isloading);
            return res;
        }
        if(context.omhierarchycat && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/omhierarchycats/${context.omhierarchycat}/omhierarchies/fetchrootorg`,tempData,isloading);
            return res;
        }
        if(context.legal && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/legals/${context.legal}/omhierarchies/fetchrootorg`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/omhierarchies/fetchrootorg`,tempData,isloading);
        return res;
    }
}
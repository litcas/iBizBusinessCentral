import { Http,Util } from '@/utils';
import ContactServiceBase from './contact-service-base';


/**
 * 联系人服务对象
 *
 * @export
 * @class ContactService
 * @extends {ContactServiceBase}
 */
export default class ContactService extends ContactServiceBase {

    /**
     * Creates an instance of  ContactService.
     * 
     * @param {*} [opts={}]
     * @memberof  ContactService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
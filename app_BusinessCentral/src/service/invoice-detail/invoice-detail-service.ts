import { Http,Util } from '@/utils';
import InvoiceDetailServiceBase from './invoice-detail-service-base';


/**
 * 发票产品服务对象
 *
 * @export
 * @class InvoiceDetailService
 * @extends {InvoiceDetailServiceBase}
 */
export default class InvoiceDetailService extends InvoiceDetailServiceBase {

    /**
     * Creates an instance of  InvoiceDetailService.
     * 
     * @param {*} [opts={}]
     * @memberof  InvoiceDetailService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
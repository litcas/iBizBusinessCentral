import { Http,Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 营销列表-账户服务对象基类
 *
 * @export
 * @class ListAccountServiceBase
 * @extends {EntityServie}
 */
export default class ListAccountServiceBase extends EntityService {

    /**
     * Creates an instance of  ListAccountServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  ListAccountServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof ListAccountServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='listaccount';
        this.APPDEKEY = 'relationshipsid';
        this.APPDENAME = 'listaccounts';
        this.APPDETEXT = 'relationshipsname';
        this.APPNAME = 'businesscentral';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ListAccountServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.ibizlist && context.listaccount){
            let res:any = Http.getInstance().get(`/ibizlists/${context.ibizlist}/listaccounts/${context.listaccount}/select`,isloading);
            
            return res;
        }
        if(context.account && context.listaccount){
            let res:any = Http.getInstance().get(`/accounts/${context.account}/listaccounts/${context.listaccount}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/listaccounts/${context.listaccount}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ListAccountServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.ibizlist && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/ibizlists/${context.ibizlist}/listaccounts`,data,isloading);
            
            return res;
        }
        if(context.account && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/listaccounts`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/listaccounts`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ListAccountServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.ibizlist && context.listaccount){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/ibizlists/${context.ibizlist}/listaccounts/${context.listaccount}`,data,isloading);
            
            return res;
        }
        if(context.account && context.listaccount){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/accounts/${context.account}/listaccounts/${context.listaccount}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/listaccounts/${context.listaccount}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ListAccountServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.ibizlist && context.listaccount){
            let res:any = Http.getInstance().delete(`/ibizlists/${context.ibizlist}/listaccounts/${context.listaccount}`,isloading);
            return res;
        }
        if(context.account && context.listaccount){
            let res:any = Http.getInstance().delete(`/accounts/${context.account}/listaccounts/${context.listaccount}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/listaccounts/${context.listaccount}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ListAccountServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.ibizlist && context.listaccount){
            let res:any = await Http.getInstance().get(`/ibizlists/${context.ibizlist}/listaccounts/${context.listaccount}`,isloading);
            
            return res;
        }
        if(context.account && context.listaccount){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/listaccounts/${context.listaccount}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/listaccounts/${context.listaccount}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ListAccountServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.ibizlist && true){
            let res:any = await Http.getInstance().get(`/ibizlists/${context.ibizlist}/listaccounts/getdraft`,isloading);
            res.data.listaccount = data.listaccount;
            
            return res;
        }
        if(context.account && true){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/listaccounts/getdraft`,isloading);
            res.data.listaccount = data.listaccount;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/listaccounts/getdraft`,isloading);
        res.data.listaccount = data.listaccount;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ListAccountServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.ibizlist && context.listaccount){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/ibizlists/${context.ibizlist}/listaccounts/${context.listaccount}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.account && context.listaccount){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/listaccounts/${context.listaccount}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/listaccounts/${context.listaccount}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ListAccountServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.ibizlist && context.listaccount){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/ibizlists/${context.ibizlist}/listaccounts/${context.listaccount}/save`,data,isloading);
            
            return res;
        }
        if(context.account && context.listaccount){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/listaccounts/${context.listaccount}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/listaccounts/${context.listaccount}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ListAccountServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.ibizlist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/ibizlists/${context.ibizlist}/listaccounts/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.account && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/listaccounts/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/listaccounts/fetchdefault`,tempData,isloading);
        return res;
    }
}
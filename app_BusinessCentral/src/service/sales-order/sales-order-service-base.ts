import { Http,Util } from '@/utils';
import EntityService from '../entity-service';
import CancelLogic from '@/service/sales-order/cancel-logic';
import FinishLogic from '@/service/sales-order/finish-logic';



/**
 * 订单服务对象基类
 *
 * @export
 * @class SalesOrderServiceBase
 * @extends {EntityServie}
 */
export default class SalesOrderServiceBase extends EntityService {

    /**
     * Creates an instance of  SalesOrderServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  SalesOrderServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof SalesOrderServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='salesorder';
        this.APPDEKEY = 'salesorderid';
        this.APPDENAME = 'salesorders';
        this.APPDETEXT = 'salesordername';
        this.APPNAME = 'businesscentral';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesOrderServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder){
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/select`,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder){
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/select`,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder){
            let res:any = Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/select`,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder){
            let res:any = Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/select`,isloading);
            
            return res;
        }
        if(context.quote && context.salesorder){
            let res:any = Http.getInstance().get(`/quotes/${context.quote}/salesorders/${context.salesorder}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/salesorders/${context.salesorder}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesOrderServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_invoices',JSON.stringify(res.data.invoices?res.data.invoices:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_salesorderdetails',JSON.stringify(res.data.salesorderdetails?res.data.salesorderdetails:[]));
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_invoices',JSON.stringify(res.data.invoices?res.data.invoices:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_salesorderdetails',JSON.stringify(res.data.salesorderdetails?res.data.salesorderdetails:[]));
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_invoices',JSON.stringify(res.data.invoices?res.data.invoices:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_salesorderdetails',JSON.stringify(res.data.salesorderdetails?res.data.salesorderdetails:[]));
            
            return res;
        }
        if(context.opportunity && context.quote && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_invoices',JSON.stringify(res.data.invoices?res.data.invoices:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_salesorderdetails',JSON.stringify(res.data.salesorderdetails?res.data.salesorderdetails:[]));
            
            return res;
        }
        if(context.quote && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/quotes/${context.quote}/salesorders`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_invoices',JSON.stringify(res.data.invoices?res.data.invoices:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_salesorderdetails',JSON.stringify(res.data.salesorderdetails?res.data.salesorderdetails:[]));
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/salesorders`,data,isloading);
        this.tempStorage.setItem(tempContext.srfsessionkey+'_invoices',JSON.stringify(res.data.invoices?res.data.invoices:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_salesorderdetails',JSON.stringify(res.data.salesorderdetails?res.data.salesorderdetails:[]));
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesOrderServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}`,data,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}`,data,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}`,data,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}`,data,isloading);
            
            return res;
        }
        if(context.quote && context.salesorder){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/quotes/${context.quote}/salesorders/${context.salesorder}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/salesorders/${context.salesorder}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesOrderServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder){
            let res:any = Http.getInstance().delete(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}`,isloading);
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder){
            let res:any = Http.getInstance().delete(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}`,isloading);
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder){
            let res:any = Http.getInstance().delete(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}`,isloading);
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder){
            let res:any = Http.getInstance().delete(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}`,isloading);
            return res;
        }
        if(context.quote && context.salesorder){
            let res:any = Http.getInstance().delete(`/quotes/${context.quote}/salesorders/${context.salesorder}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/salesorders/${context.salesorder}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesOrderServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}`,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder){
            let res:any = await Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}`,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}`,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder){
            let res:any = await Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}`,isloading);
            
            return res;
        }
        if(context.quote && context.salesorder){
            let res:any = await Http.getInstance().get(`/quotes/${context.quote}/salesorders/${context.salesorder}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/salesorders/${context.salesorder}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesOrderServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && true){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/getdraft`,isloading);
            res.data.salesorder = data.salesorder;
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && true){
            let res:any = await Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/getdraft`,isloading);
            res.data.salesorder = data.salesorder;
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && true){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/getdraft`,isloading);
            res.data.salesorder = data.salesorder;
            
            return res;
        }
        if(context.opportunity && context.quote && true){
            let res:any = await Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/getdraft`,isloading);
            res.data.salesorder = data.salesorder;
            
            return res;
        }
        if(context.quote && true){
            let res:any = await Http.getInstance().get(`/quotes/${context.quote}/salesorders/getdraft`,isloading);
            res.data.salesorder = data.salesorder;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/salesorders/getdraft`,isloading);
        res.data.salesorder = data.salesorder;
        
        return res;
    }

    /**
     * Cancel接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesOrderServiceBase
     */
    public async Cancel(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let appLogic:CancelLogic = new CancelLogic({context:JSON.parse(JSON.stringify(context)),data:JSON.parse(JSON.stringify(data))});
        const res = await appLogic.onExecute(context,data,isloading?true:false);
        return {status:200,data:res};
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesOrderServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.quote && context.salesorder){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/quotes/${context.quote}/salesorders/${context.salesorder}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/salesorders/${context.salesorder}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Finish接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesOrderServiceBase
     */
    public async Finish(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let appLogic:FinishLogic = new FinishLogic({context:JSON.parse(JSON.stringify(context)),data:JSON.parse(JSON.stringify(data))});
        const res = await appLogic.onExecute(context,data,isloading?true:false);
        return {status:200,data:res};
    }

    /**
     * GenInvoice接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesOrderServiceBase
     */
    public async GenInvoice(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/geninvoice`,data,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/geninvoice`,data,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/geninvoice`,data,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/geninvoice`,data,isloading);
            
            return res;
        }
        if(context.quote && context.salesorder){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/quotes/${context.quote}/salesorders/${context.salesorder}/geninvoice`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/salesorders/${context.salesorder}/geninvoice`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesOrderServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/save`,data,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/save`,data,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/save`,data,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/save`,data,isloading);
            
            return res;
        }
        if(context.quote && context.salesorder){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/quotes/${context.quote}/salesorders/${context.salesorder}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/salesorders/${context.salesorder}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchByParentKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesOrderServiceBase
     */
    public async FetchByParentKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/fetchbyparentkey`,tempData,isloading);
            return res;
        }
        if(context.contact && context.opportunity && context.quote && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/fetchbyparentkey`,tempData,isloading);
            return res;
        }
        if(context.account && context.opportunity && context.quote && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/fetchbyparentkey`,tempData,isloading);
            return res;
        }
        if(context.opportunity && context.quote && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/fetchbyparentkey`,tempData,isloading);
            return res;
        }
        if(context.quote && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/quotes/${context.quote}/salesorders/fetchbyparentkey`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/salesorders/fetchbyparentkey`,tempData,isloading);
        return res;
    }

    /**
     * FetchCancel接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesOrderServiceBase
     */
    public async FetchCancel(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/fetchcancel`,tempData,isloading);
            return res;
        }
        if(context.contact && context.opportunity && context.quote && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/fetchcancel`,tempData,isloading);
            return res;
        }
        if(context.account && context.opportunity && context.quote && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/fetchcancel`,tempData,isloading);
            return res;
        }
        if(context.opportunity && context.quote && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/fetchcancel`,tempData,isloading);
            return res;
        }
        if(context.quote && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/quotes/${context.quote}/salesorders/fetchcancel`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/salesorders/fetchcancel`,tempData,isloading);
        return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesOrderServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.contact && context.opportunity && context.quote && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.account && context.opportunity && context.quote && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.opportunity && context.quote && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.quote && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/quotes/${context.quote}/salesorders/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/salesorders/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * FetchFinish接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesOrderServiceBase
     */
    public async FetchFinish(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/fetchfinish`,tempData,isloading);
            return res;
        }
        if(context.contact && context.opportunity && context.quote && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/fetchfinish`,tempData,isloading);
            return res;
        }
        if(context.account && context.opportunity && context.quote && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/fetchfinish`,tempData,isloading);
            return res;
        }
        if(context.opportunity && context.quote && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/fetchfinish`,tempData,isloading);
            return res;
        }
        if(context.quote && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/quotes/${context.quote}/salesorders/fetchfinish`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/salesorders/fetchfinish`,tempData,isloading);
        return res;
    }

    /**
     * FetchInvoiced接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesOrderServiceBase
     */
    public async FetchInvoiced(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/fetchinvoiced`,tempData,isloading);
            return res;
        }
        if(context.contact && context.opportunity && context.quote && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/fetchinvoiced`,tempData,isloading);
            return res;
        }
        if(context.account && context.opportunity && context.quote && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/fetchinvoiced`,tempData,isloading);
            return res;
        }
        if(context.opportunity && context.quote && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/fetchinvoiced`,tempData,isloading);
            return res;
        }
        if(context.quote && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/quotes/${context.quote}/salesorders/fetchinvoiced`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/salesorders/fetchinvoiced`,tempData,isloading);
        return res;
    }
}
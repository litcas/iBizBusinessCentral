import { Http,Util } from '@/utils';
import SalesLiteratureServiceBase from './sales-literature-service-base';


/**
 * 销售宣传资料服务对象
 *
 * @export
 * @class SalesLiteratureService
 * @extends {SalesLiteratureServiceBase}
 */
export default class SalesLiteratureService extends SalesLiteratureServiceBase {

    /**
     * Creates an instance of  SalesLiteratureService.
     * 
     * @param {*} [opts={}]
     * @memberof  SalesLiteratureService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import { Http,Util } from '@/utils';
import ConnectionServiceBase from './connection-service-base';


/**
 * 连接服务对象
 *
 * @export
 * @class ConnectionService
 * @extends {ConnectionServiceBase}
 */
export default class ConnectionService extends ConnectionServiceBase {

    /**
     * Creates an instance of  ConnectionService.
     * 
     * @param {*} [opts={}]
     * @memberof  ConnectionService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
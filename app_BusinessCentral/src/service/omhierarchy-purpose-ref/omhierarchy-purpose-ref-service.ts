import { Http,Util } from '@/utils';
import OMHierarchyPurposeRefServiceBase from './omhierarchy-purpose-ref-service-base';


/**
 * 组织层次结构分配服务对象
 *
 * @export
 * @class OMHierarchyPurposeRefService
 * @extends {OMHierarchyPurposeRefServiceBase}
 */
export default class OMHierarchyPurposeRefService extends OMHierarchyPurposeRefServiceBase {

    /**
     * Creates an instance of  OMHierarchyPurposeRefService.
     * 
     * @param {*} [opts={}]
     * @memberof  OMHierarchyPurposeRefService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
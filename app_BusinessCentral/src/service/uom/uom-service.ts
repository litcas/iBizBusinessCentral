import { Http,Util } from '@/utils';
import UomServiceBase from './uom-service-base';


/**
 * 计价单位服务对象
 *
 * @export
 * @class UomService
 * @extends {UomServiceBase}
 */
export default class UomService extends UomServiceBase {

    /**
     * Creates an instance of  UomService.
     * 
     * @param {*} [opts={}]
     * @memberof  UomService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
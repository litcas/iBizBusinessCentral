import { Http,Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 报价单产品服务对象基类
 *
 * @export
 * @class QuoteDetailServiceBase
 * @extends {EntityServie}
 */
export default class QuoteDetailServiceBase extends EntityService {

    /**
     * Creates an instance of  QuoteDetailServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  QuoteDetailServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof QuoteDetailServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='quotedetail';
        this.APPDEKEY = 'quotedetailid';
        this.APPDENAME = 'quotedetails';
        this.APPDETEXT = 'quotedetailname';
        this.APPNAME = 'businesscentral';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof QuoteDetailServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.quotedetail){
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails/${context.quotedetail}/select`,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.quotedetail){
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails/${context.quotedetail}/select`,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.quotedetail){
            let res:any = Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails/${context.quotedetail}/select`,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote && context.quotedetail){
            let res:any = Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails/${context.quotedetail}/select`,isloading);
            
            return res;
        }
        if(context.quote && context.quotedetail){
            let res:any = Http.getInstance().get(`/quotes/${context.quote}/quotedetails/${context.quotedetail}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/quotedetails/${context.quotedetail}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof QuoteDetailServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails`,data,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails`,data,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails`,data,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails`,data,isloading);
            
            return res;
        }
        if(context.quote && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/quotes/${context.quote}/quotedetails`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/quotedetails`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof QuoteDetailServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.quotedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails/${context.quotedetail}`,data,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.quotedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails/${context.quotedetail}`,data,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.quotedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails/${context.quotedetail}`,data,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote && context.quotedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails/${context.quotedetail}`,data,isloading);
            
            return res;
        }
        if(context.quote && context.quotedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/quotes/${context.quote}/quotedetails/${context.quotedetail}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/quotedetails/${context.quotedetail}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof QuoteDetailServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.quotedetail){
            let res:any = Http.getInstance().delete(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails/${context.quotedetail}`,isloading);
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.quotedetail){
            let res:any = Http.getInstance().delete(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails/${context.quotedetail}`,isloading);
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.quotedetail){
            let res:any = Http.getInstance().delete(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails/${context.quotedetail}`,isloading);
            return res;
        }
        if(context.opportunity && context.quote && context.quotedetail){
            let res:any = Http.getInstance().delete(`/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails/${context.quotedetail}`,isloading);
            return res;
        }
        if(context.quote && context.quotedetail){
            let res:any = Http.getInstance().delete(`/quotes/${context.quote}/quotedetails/${context.quotedetail}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/quotedetails/${context.quotedetail}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof QuoteDetailServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.quotedetail){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails/${context.quotedetail}`,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.quotedetail){
            let res:any = await Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails/${context.quotedetail}`,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.quotedetail){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails/${context.quotedetail}`,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote && context.quotedetail){
            let res:any = await Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails/${context.quotedetail}`,isloading);
            
            return res;
        }
        if(context.quote && context.quotedetail){
            let res:any = await Http.getInstance().get(`/quotes/${context.quote}/quotedetails/${context.quotedetail}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/quotedetails/${context.quotedetail}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof QuoteDetailServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && true){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails/getdraft`,isloading);
            res.data.quotedetail = data.quotedetail;
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && true){
            let res:any = await Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails/getdraft`,isloading);
            res.data.quotedetail = data.quotedetail;
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && true){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails/getdraft`,isloading);
            res.data.quotedetail = data.quotedetail;
            
            return res;
        }
        if(context.opportunity && context.quote && true){
            let res:any = await Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails/getdraft`,isloading);
            res.data.quotedetail = data.quotedetail;
            
            return res;
        }
        if(context.quote && true){
            let res:any = await Http.getInstance().get(`/quotes/${context.quote}/quotedetails/getdraft`,isloading);
            res.data.quotedetail = data.quotedetail;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/quotedetails/getdraft`,isloading);
        res.data.quotedetail = data.quotedetail;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof QuoteDetailServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.quotedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails/${context.quotedetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.quotedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails/${context.quotedetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.quotedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails/${context.quotedetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote && context.quotedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails/${context.quotedetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.quote && context.quotedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/quotes/${context.quote}/quotedetails/${context.quotedetail}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/quotedetails/${context.quotedetail}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof QuoteDetailServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.quotedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails/${context.quotedetail}/save`,data,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.quotedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails/${context.quotedetail}/save`,data,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.quotedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails/${context.quotedetail}/save`,data,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote && context.quotedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails/${context.quotedetail}/save`,data,isloading);
            
            return res;
        }
        if(context.quote && context.quotedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/quotes/${context.quote}/quotedetails/${context.quotedetail}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/quotedetails/${context.quotedetail}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof QuoteDetailServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.contact && context.opportunity && context.quote && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.account && context.opportunity && context.quote && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.opportunity && context.quote && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/${context.quote}/quotedetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.quote && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/quotes/${context.quote}/quotedetails/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/quotedetails/fetchdefault`,tempData,isloading);
        return res;
    }
}
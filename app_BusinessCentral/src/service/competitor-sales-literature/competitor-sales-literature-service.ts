import { Http,Util } from '@/utils';
import CompetitorSalesLiteratureServiceBase from './competitor-sales-literature-service-base';


/**
 * 竞争对手宣传资料服务对象
 *
 * @export
 * @class CompetitorSalesLiteratureService
 * @extends {CompetitorSalesLiteratureServiceBase}
 */
export default class CompetitorSalesLiteratureService extends CompetitorSalesLiteratureServiceBase {

    /**
     * Creates an instance of  CompetitorSalesLiteratureService.
     * 
     * @param {*} [opts={}]
     * @memberof  CompetitorSalesLiteratureService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
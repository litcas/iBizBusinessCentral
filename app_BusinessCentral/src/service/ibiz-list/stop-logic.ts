import { Http,Util } from '@/utils';
import StopLogicBase from './stop-logic-base';

/**
 * 停用
 *
 * @export
 * @class StopLogic
 */
export default class StopLogic extends StopLogicBase{

    /**
     * Creates an instance of  StopLogic
     * 
     * @param {*} [opts={}]
     * @memberof  StopLogic
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
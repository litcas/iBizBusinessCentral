import { Http,Util } from '@/utils';
import IBizListServiceBase from './ibiz-list-service-base';


/**
 * 市场营销列表服务对象
 *
 * @export
 * @class IBizListService
 * @extends {IBizListServiceBase}
 */
export default class IBizListService extends IBizListServiceBase {

    /**
     * Creates an instance of  IBizListService.
     * 
     * @param {*} [opts={}]
     * @memberof  IBizListService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import { Http,Util } from '@/utils';
import AppointmentServiceBase from './appointment-service-base';


/**
 * 约会服务对象
 *
 * @export
 * @class AppointmentService
 * @extends {AppointmentServiceBase}
 */
export default class AppointmentService extends AppointmentServiceBase {

    /**
     * Creates an instance of  AppointmentService.
     * 
     * @param {*} [opts={}]
     * @memberof  AppointmentService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
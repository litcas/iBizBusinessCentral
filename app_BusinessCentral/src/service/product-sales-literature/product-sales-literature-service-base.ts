import { Http,Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 产品宣传资料服务对象基类
 *
 * @export
 * @class ProductSalesLiteratureServiceBase
 * @extends {EntityServie}
 */
export default class ProductSalesLiteratureServiceBase extends EntityService {

    /**
     * Creates an instance of  ProductSalesLiteratureServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  ProductSalesLiteratureServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof ProductSalesLiteratureServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='productsalesliterature';
        this.APPDEKEY = 'relationshipsid';
        this.APPDENAME = 'productsalesliteratures';
        this.APPDETEXT = 'relationshipsname';
        this.APPNAME = 'businesscentral';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ProductSalesLiteratureServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.salesliterature && context.productsalesliterature){
            let res:any = Http.getInstance().get(`/salesliteratures/${context.salesliterature}/productsalesliteratures/${context.productsalesliterature}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/productsalesliteratures/${context.productsalesliterature}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ProductSalesLiteratureServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.salesliterature && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/salesliteratures/${context.salesliterature}/productsalesliteratures`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/productsalesliteratures`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ProductSalesLiteratureServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.salesliterature && context.productsalesliterature){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/salesliteratures/${context.salesliterature}/productsalesliteratures/${context.productsalesliterature}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/productsalesliteratures/${context.productsalesliterature}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ProductSalesLiteratureServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.salesliterature && context.productsalesliterature){
            let res:any = Http.getInstance().delete(`/salesliteratures/${context.salesliterature}/productsalesliteratures/${context.productsalesliterature}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/productsalesliteratures/${context.productsalesliterature}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ProductSalesLiteratureServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.salesliterature && context.productsalesliterature){
            let res:any = await Http.getInstance().get(`/salesliteratures/${context.salesliterature}/productsalesliteratures/${context.productsalesliterature}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/productsalesliteratures/${context.productsalesliterature}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ProductSalesLiteratureServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.salesliterature && true){
            let res:any = await Http.getInstance().get(`/salesliteratures/${context.salesliterature}/productsalesliteratures/getdraft`,isloading);
            res.data.productsalesliterature = data.productsalesliterature;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/productsalesliteratures/getdraft`,isloading);
        res.data.productsalesliterature = data.productsalesliterature;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ProductSalesLiteratureServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.salesliterature && context.productsalesliterature){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/salesliteratures/${context.salesliterature}/productsalesliteratures/${context.productsalesliterature}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/productsalesliteratures/${context.productsalesliterature}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ProductSalesLiteratureServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.salesliterature && context.productsalesliterature){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/salesliteratures/${context.salesliterature}/productsalesliteratures/${context.productsalesliterature}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/productsalesliteratures/${context.productsalesliterature}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ProductSalesLiteratureServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.salesliterature && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/salesliteratures/${context.salesliterature}/productsalesliteratures/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/productsalesliteratures/fetchdefault`,tempData,isloading);
        return res;
    }
}
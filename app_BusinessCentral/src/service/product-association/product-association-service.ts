import { Http,Util } from '@/utils';
import ProductAssociationServiceBase from './product-association-service-base';


/**
 * 产品关联服务对象
 *
 * @export
 * @class ProductAssociationService
 * @extends {ProductAssociationServiceBase}
 */
export default class ProductAssociationService extends ProductAssociationServiceBase {

    /**
     * Creates an instance of  ProductAssociationService.
     * 
     * @param {*} [opts={}]
     * @memberof  ProductAssociationService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
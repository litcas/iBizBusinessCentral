import { Http,Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 市场活动-市场活动服务对象基类
 *
 * @export
 * @class CampaignCampaignServiceBase
 * @extends {EntityServie}
 */
export default class CampaignCampaignServiceBase extends EntityService {

    /**
     * Creates an instance of  CampaignCampaignServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  CampaignCampaignServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof CampaignCampaignServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='campaigncampaign';
        this.APPDEKEY = 'relationshipsid';
        this.APPDENAME = 'campaigncampaigns';
        this.APPDETEXT = 'relationshipsname';
        this.APPNAME = 'businesscentral';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CampaignCampaignServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.campaign && context.campaigncampaign){
            let res:any = Http.getInstance().get(`/campaigns/${context.campaign}/campaigncampaigns/${context.campaigncampaign}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/campaigncampaigns/${context.campaigncampaign}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CampaignCampaignServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.campaign && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/campaigns/${context.campaign}/campaigncampaigns`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/campaigncampaigns`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CampaignCampaignServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.campaign && context.campaigncampaign){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/campaigns/${context.campaign}/campaigncampaigns/${context.campaigncampaign}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/campaigncampaigns/${context.campaigncampaign}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CampaignCampaignServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.campaign && context.campaigncampaign){
            let res:any = Http.getInstance().delete(`/campaigns/${context.campaign}/campaigncampaigns/${context.campaigncampaign}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/campaigncampaigns/${context.campaigncampaign}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CampaignCampaignServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.campaign && context.campaigncampaign){
            let res:any = await Http.getInstance().get(`/campaigns/${context.campaign}/campaigncampaigns/${context.campaigncampaign}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/campaigncampaigns/${context.campaigncampaign}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CampaignCampaignServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.campaign && true){
            let res:any = await Http.getInstance().get(`/campaigns/${context.campaign}/campaigncampaigns/getdraft`,isloading);
            res.data.campaigncampaign = data.campaigncampaign;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/campaigncampaigns/getdraft`,isloading);
        res.data.campaigncampaign = data.campaigncampaign;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CampaignCampaignServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.campaign && context.campaigncampaign){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/campaigns/${context.campaign}/campaigncampaigns/${context.campaigncampaign}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/campaigncampaigns/${context.campaigncampaign}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CampaignCampaignServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.campaign && context.campaigncampaign){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/campaigns/${context.campaign}/campaigncampaigns/${context.campaigncampaign}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/campaigncampaigns/${context.campaigncampaign}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CampaignCampaignServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.campaign && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/campaigns/${context.campaign}/campaigncampaigns/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/campaigncampaigns/fetchdefault`,tempData,isloading);
        return res;
    }
}
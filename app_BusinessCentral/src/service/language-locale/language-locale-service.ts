import { Http,Util } from '@/utils';
import LanguageLocaleServiceBase from './language-locale-service-base';


/**
 * 语言服务对象
 *
 * @export
 * @class LanguageLocaleService
 * @extends {LanguageLocaleServiceBase}
 */
export default class LanguageLocaleService extends LanguageLocaleServiceBase {

    /**
     * Creates an instance of  LanguageLocaleService.
     * 
     * @param {*} [opts={}]
     * @memberof  LanguageLocaleService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
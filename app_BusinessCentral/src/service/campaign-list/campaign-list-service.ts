import { Http,Util } from '@/utils';
import CampaignListServiceBase from './campaign-list-service-base';


/**
 * 市场活动-营销列表服务对象
 *
 * @export
 * @class CampaignListService
 * @extends {CampaignListServiceBase}
 */
export default class CampaignListService extends CampaignListServiceBase {

    /**
     * Creates an instance of  CampaignListService.
     * 
     * @param {*} [opts={}]
     * @memberof  CampaignListService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
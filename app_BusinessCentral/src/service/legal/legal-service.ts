import { Http,Util } from '@/utils';
import LegalServiceBase from './legal-service-base';


/**
 * 法人服务对象
 *
 * @export
 * @class LegalService
 * @extends {LegalServiceBase}
 */
export default class LegalService extends LegalServiceBase {

    /**
     * Creates an instance of  LegalService.
     * 
     * @param {*} [opts={}]
     * @memberof  LegalService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
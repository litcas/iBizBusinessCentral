import { Http,Util } from '@/utils';
import CampaignActivityServiceBase from './campaign-activity-service-base';


/**
 * 市场活动项目服务对象
 *
 * @export
 * @class CampaignActivityService
 * @extends {CampaignActivityServiceBase}
 */
export default class CampaignActivityService extends CampaignActivityServiceBase {

    /**
     * Creates an instance of  CampaignActivityService.
     * 
     * @param {*} [opts={}]
     * @memberof  CampaignActivityService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
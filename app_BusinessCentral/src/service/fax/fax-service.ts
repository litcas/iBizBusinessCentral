import { Http,Util } from '@/utils';
import FaxServiceBase from './fax-service-base';


/**
 * 传真服务对象
 *
 * @export
 * @class FaxService
 * @extends {FaxServiceBase}
 */
export default class FaxService extends FaxServiceBase {

    /**
     * Creates an instance of  FaxService.
     * 
     * @param {*} [opts={}]
     * @memberof  FaxService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
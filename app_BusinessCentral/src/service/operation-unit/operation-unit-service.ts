import { Http,Util } from '@/utils';
import OperationUnitServiceBase from './operation-unit-service-base';


/**
 * 运营单位服务对象
 *
 * @export
 * @class OperationUnitService
 * @extends {OperationUnitServiceBase}
 */
export default class OperationUnitService extends OperationUnitServiceBase {

    /**
     * Creates an instance of  OperationUnitService.
     * 
     * @param {*} [opts={}]
     * @memberof  OperationUnitService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
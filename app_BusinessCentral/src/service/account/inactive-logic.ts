import { Http,Util } from '@/utils';
import InactiveLogicBase from './inactive-logic-base';

/**
 * 停用
 *
 * @export
 * @class InactiveLogic
 */
export default class InactiveLogic extends InactiveLogicBase{

    /**
     * Creates an instance of  InactiveLogic
     * 
     * @param {*} [opts={}]
     * @memberof  InactiveLogic
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
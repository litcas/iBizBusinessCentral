import { Http,Util } from '@/utils';
import AccountServiceBase from './account-service-base';


/**
 * 客户服务对象
 *
 * @export
 * @class AccountService
 * @extends {AccountServiceBase}
 */
export default class AccountService extends AccountServiceBase {

    /**
     * Creates an instance of  AccountService.
     * 
     * @param {*} [opts={}]
     * @memberof  AccountService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import { Http,Util } from '@/utils';
import FillContactLogicBase from './fill-contact-logic-base';

/**
 * FillContact
 *
 * @export
 * @class FillContactLogic
 */
export default class FillContactLogic extends FillContactLogicBase{

    /**
     * Creates an instance of  FillContactLogic
     * 
     * @param {*} [opts={}]
     * @memberof  FillContactLogic
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
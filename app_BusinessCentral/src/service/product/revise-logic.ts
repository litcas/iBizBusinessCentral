import { Http,Util } from '@/utils';
import ReviseLogicBase from './revise-logic-base';

/**
 * 修订
 *
 * @export
 * @class ReviseLogic
 */
export default class ReviseLogic extends ReviseLogicBase{

    /**
     * Creates an instance of  ReviseLogic
     * 
     * @param {*} [opts={}]
     * @memberof  ReviseLogic
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
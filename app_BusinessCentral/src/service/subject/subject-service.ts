import { Http,Util } from '@/utils';
import SubjectServiceBase from './subject-service-base';


/**
 * 主题服务对象
 *
 * @export
 * @class SubjectService
 * @extends {SubjectServiceBase}
 */
export default class SubjectService extends SubjectServiceBase {

    /**
     * Creates an instance of  SubjectService.
     * 
     * @param {*} [opts={}]
     * @memberof  SubjectService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
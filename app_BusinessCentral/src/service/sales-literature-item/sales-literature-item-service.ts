import { Http,Util } from '@/utils';
import SalesLiteratureItemServiceBase from './sales-literature-item-service-base';


/**
 * 销售附件服务对象
 *
 * @export
 * @class SalesLiteratureItemService
 * @extends {SalesLiteratureItemServiceBase}
 */
export default class SalesLiteratureItemService extends SalesLiteratureItemServiceBase {

    /**
     * Creates an instance of  SalesLiteratureItemService.
     * 
     * @param {*} [opts={}]
     * @memberof  SalesLiteratureItemService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import { Http,Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 销售附件服务对象基类
 *
 * @export
 * @class SalesLiteratureItemServiceBase
 * @extends {EntityServie}
 */
export default class SalesLiteratureItemServiceBase extends EntityService {

    /**
     * Creates an instance of  SalesLiteratureItemServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  SalesLiteratureItemServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof SalesLiteratureItemServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='salesliteratureitem';
        this.APPDEKEY = 'salesliteratureitemid';
        this.APPDENAME = 'salesliteratureitems';
        this.APPDETEXT = 'title';
        this.APPNAME = 'businesscentral';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesLiteratureItemServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.salesliterature && context.salesliteratureitem){
            let res:any = Http.getInstance().get(`/salesliteratures/${context.salesliterature}/salesliteratureitems/${context.salesliteratureitem}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/salesliteratureitems/${context.salesliteratureitem}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesLiteratureItemServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.salesliterature && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/salesliteratures/${context.salesliterature}/salesliteratureitems`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/salesliteratureitems`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesLiteratureItemServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.salesliterature && context.salesliteratureitem){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/salesliteratures/${context.salesliterature}/salesliteratureitems/${context.salesliteratureitem}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/salesliteratureitems/${context.salesliteratureitem}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesLiteratureItemServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.salesliterature && context.salesliteratureitem){
            let res:any = Http.getInstance().delete(`/salesliteratures/${context.salesliterature}/salesliteratureitems/${context.salesliteratureitem}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/salesliteratureitems/${context.salesliteratureitem}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesLiteratureItemServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.salesliterature && context.salesliteratureitem){
            let res:any = await Http.getInstance().get(`/salesliteratures/${context.salesliterature}/salesliteratureitems/${context.salesliteratureitem}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/salesliteratureitems/${context.salesliteratureitem}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesLiteratureItemServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.salesliterature && true){
            let res:any = await Http.getInstance().get(`/salesliteratures/${context.salesliterature}/salesliteratureitems/getdraft`,isloading);
            res.data.salesliteratureitem = data.salesliteratureitem;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/salesliteratureitems/getdraft`,isloading);
        res.data.salesliteratureitem = data.salesliteratureitem;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesLiteratureItemServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.salesliterature && context.salesliteratureitem){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/salesliteratures/${context.salesliterature}/salesliteratureitems/${context.salesliteratureitem}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/salesliteratureitems/${context.salesliteratureitem}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesLiteratureItemServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.salesliterature && context.salesliteratureitem){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/salesliteratures/${context.salesliterature}/salesliteratureitems/${context.salesliteratureitem}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/salesliteratureitems/${context.salesliteratureitem}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesLiteratureItemServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.salesliterature && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/salesliteratures/${context.salesliterature}/salesliteratureitems/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/salesliteratureitems/fetchdefault`,tempData,isloading);
        return res;
    }
}
import { Http,Util } from '@/utils';
import OpportunityCompetitorServiceBase from './opportunity-competitor-service-base';


/**
 * 商机对手服务对象
 *
 * @export
 * @class OpportunityCompetitorService
 * @extends {OpportunityCompetitorServiceBase}
 */
export default class OpportunityCompetitorService extends OpportunityCompetitorServiceBase {

    /**
     * Creates an instance of  OpportunityCompetitorService.
     * 
     * @param {*} [opts={}]
     * @memberof  OpportunityCompetitorService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
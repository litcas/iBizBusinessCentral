import { Http,Util } from '@/utils';
import ActivityPointerServiceBase from './activity-pointer-service-base';


/**
 * 活动服务对象
 *
 * @export
 * @class ActivityPointerService
 * @extends {ActivityPointerServiceBase}
 */
export default class ActivityPointerService extends ActivityPointerServiceBase {

    /**
     * Creates an instance of  ActivityPointerService.
     * 
     * @param {*} [opts={}]
     * @memberof  ActivityPointerService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
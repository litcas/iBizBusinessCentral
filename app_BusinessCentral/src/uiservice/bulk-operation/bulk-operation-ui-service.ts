import BulkOperationUIServiceBase from './bulk-operation-ui-service-base';

/**
 * 快速市场活动UI服务对象
 *
 * @export
 * @class BulkOperationUIService
 */
export default class BulkOperationUIService extends BulkOperationUIServiceBase {

    /**
     * Creates an instance of  BulkOperationUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  BulkOperationUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
import OpportunityCompetitorUIServiceBase from './opportunity-competitor-ui-service-base';

/**
 * 商机对手UI服务对象
 *
 * @export
 * @class OpportunityCompetitorUIService
 */
export default class OpportunityCompetitorUIService extends OpportunityCompetitorUIServiceBase {

    /**
     * Creates an instance of  OpportunityCompetitorUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  OpportunityCompetitorUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
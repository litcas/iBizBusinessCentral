import SubjectUIServiceBase from './subject-ui-service-base';

/**
 * 主题UI服务对象
 *
 * @export
 * @class SubjectUIService
 */
export default class SubjectUIService extends SubjectUIServiceBase {

    /**
     * Creates an instance of  SubjectUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  SubjectUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
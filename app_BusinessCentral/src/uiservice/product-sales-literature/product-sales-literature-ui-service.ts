import ProductSalesLiteratureUIServiceBase from './product-sales-literature-ui-service-base';

/**
 * 产品宣传资料UI服务对象
 *
 * @export
 * @class ProductSalesLiteratureUIService
 */
export default class ProductSalesLiteratureUIService extends ProductSalesLiteratureUIServiceBase {

    /**
     * Creates an instance of  ProductSalesLiteratureUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  ProductSalesLiteratureUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
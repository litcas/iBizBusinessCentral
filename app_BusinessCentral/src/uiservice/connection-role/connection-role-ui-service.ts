import ConnectionRoleUIServiceBase from './connection-role-ui-service-base';

/**
 * 连接角色UI服务对象
 *
 * @export
 * @class ConnectionRoleUIService
 */
export default class ConnectionRoleUIService extends ConnectionRoleUIServiceBase {

    /**
     * Creates an instance of  ConnectionRoleUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  ConnectionRoleUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
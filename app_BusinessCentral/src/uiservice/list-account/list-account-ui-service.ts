import ListAccountUIServiceBase from './list-account-ui-service-base';

/**
 * 营销列表-账户UI服务对象
 *
 * @export
 * @class ListAccountUIService
 */
export default class ListAccountUIService extends ListAccountUIServiceBase {

    /**
     * Creates an instance of  ListAccountUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  ListAccountUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
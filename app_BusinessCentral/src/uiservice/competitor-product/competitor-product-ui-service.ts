import CompetitorProductUIServiceBase from './competitor-product-ui-service-base';

/**
 * 竞争对手产品UI服务对象
 *
 * @export
 * @class CompetitorProductUIService
 */
export default class CompetitorProductUIService extends CompetitorProductUIServiceBase {

    /**
     * Creates an instance of  CompetitorProductUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  CompetitorProductUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
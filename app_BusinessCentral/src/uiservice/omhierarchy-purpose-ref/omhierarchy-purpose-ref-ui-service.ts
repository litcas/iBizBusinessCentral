import OMHierarchyPurposeRefUIServiceBase from './omhierarchy-purpose-ref-ui-service-base';

/**
 * 组织层次结构分配UI服务对象
 *
 * @export
 * @class OMHierarchyPurposeRefUIService
 */
export default class OMHierarchyPurposeRefUIService extends OMHierarchyPurposeRefUIServiceBase {

    /**
     * Creates an instance of  OMHierarchyPurposeRefUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  OMHierarchyPurposeRefUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
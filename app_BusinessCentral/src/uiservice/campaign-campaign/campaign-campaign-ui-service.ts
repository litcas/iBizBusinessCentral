import CampaignCampaignUIServiceBase from './campaign-campaign-ui-service-base';

/**
 * 市场活动-市场活动UI服务对象
 *
 * @export
 * @class CampaignCampaignUIService
 */
export default class CampaignCampaignUIService extends CampaignCampaignUIServiceBase {

    /**
     * Creates an instance of  CampaignCampaignUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  CampaignCampaignUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
import LeadUIServiceBase from './lead-ui-service-base';

/**
 * 潜在顾客UI服务对象
 *
 * @export
 * @class LeadUIService
 */
export default class LeadUIService extends LeadUIServiceBase {

    /**
     * Creates an instance of  LeadUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  LeadUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
import LanguageLocaleUIServiceBase from './language-locale-ui-service-base';

/**
 * 语言UI服务对象
 *
 * @export
 * @class LanguageLocaleUIService
 */
export default class LanguageLocaleUIService extends LanguageLocaleUIServiceBase {

    /**
     * Creates an instance of  LanguageLocaleUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  LanguageLocaleUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
import IBizListUIServiceBase from './ibiz-list-ui-service-base';

/**
 * 市场营销列表UI服务对象
 *
 * @export
 * @class IBizListUIService
 */
export default class IBizListUIService extends IBizListUIServiceBase {

    /**
     * Creates an instance of  IBizListUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  IBizListUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
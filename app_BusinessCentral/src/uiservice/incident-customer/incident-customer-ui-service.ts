import IncidentCustomerUIServiceBase from './incident-customer-ui-service-base';

/**
 * 案例客户UI服务对象
 *
 * @export
 * @class IncidentCustomerUIService
 */
export default class IncidentCustomerUIService extends IncidentCustomerUIServiceBase {

    /**
     * Creates an instance of  IncidentCustomerUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  IncidentCustomerUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
import LeadCompetitorUIServiceBase from './lead-competitor-ui-service-base';

/**
 * 潜在客户对手UI服务对象
 *
 * @export
 * @class LeadCompetitorUIService
 */
export default class LeadCompetitorUIService extends LeadCompetitorUIServiceBase {

    /**
     * Creates an instance of  LeadCompetitorUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  LeadCompetitorUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
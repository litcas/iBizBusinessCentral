import ProductSubstituteUIServiceBase from './product-substitute-ui-service-base';

/**
 * 产品替换UI服务对象
 *
 * @export
 * @class ProductSubstituteUIService
 */
export default class ProductSubstituteUIService extends ProductSubstituteUIServiceBase {

    /**
     * Creates an instance of  ProductSubstituteUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  ProductSubstituteUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
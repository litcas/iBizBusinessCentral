import ListContactUIServiceBase from './list-contact-ui-service-base';

/**
 * 营销列表-联系人UI服务对象
 *
 * @export
 * @class ListContactUIService
 */
export default class ListContactUIService extends ListContactUIServiceBase {

    /**
     * Creates an instance of  ListContactUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  ListContactUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
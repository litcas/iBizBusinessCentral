import OpportunityProductUIServiceBase from './opportunity-product-ui-service-base';

/**
 * 商机产品UI服务对象
 *
 * @export
 * @class OpportunityProductUIService
 */
export default class OpportunityProductUIService extends OpportunityProductUIServiceBase {

    /**
     * Creates an instance of  OpportunityProductUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  OpportunityProductUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
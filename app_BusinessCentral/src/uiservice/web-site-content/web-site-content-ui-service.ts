import WebSiteContentUIServiceBase from './web-site-content-ui-service-base';

/**
 * 站点内容UI服务对象
 *
 * @export
 * @class WebSiteContentUIService
 */
export default class WebSiteContentUIService extends WebSiteContentUIServiceBase {

    /**
     * Creates an instance of  WebSiteContentUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  WebSiteContentUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
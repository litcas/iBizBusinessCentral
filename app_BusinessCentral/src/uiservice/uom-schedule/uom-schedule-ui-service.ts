import UomScheduleUIServiceBase from './uom-schedule-ui-service-base';

/**
 * 计价单位组UI服务对象
 *
 * @export
 * @class UomScheduleUIService
 */
export default class UomScheduleUIService extends UomScheduleUIServiceBase {

    /**
     * Creates an instance of  UomScheduleUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  UomScheduleUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
import SalesOrderDetailUIServiceBase from './sales-order-detail-ui-service-base';

/**
 * 订单产品UI服务对象
 *
 * @export
 * @class SalesOrderDetailUIService
 */
export default class SalesOrderDetailUIService extends SalesOrderDetailUIServiceBase {

    /**
     * Creates an instance of  SalesOrderDetailUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  SalesOrderDetailUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
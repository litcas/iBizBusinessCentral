import CampaignResponseUIServiceBase from './campaign-response-ui-service-base';

/**
 * 市场活动响应UI服务对象
 *
 * @export
 * @class CampaignResponseUIService
 */
export default class CampaignResponseUIService extends CampaignResponseUIServiceBase {

    /**
     * Creates an instance of  CampaignResponseUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  CampaignResponseUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
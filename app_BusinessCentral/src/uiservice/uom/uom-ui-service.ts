import UomUIServiceBase from './uom-ui-service-base';

/**
 * 计价单位UI服务对象
 *
 * @export
 * @class UomUIService
 */
export default class UomUIService extends UomUIServiceBase {

    /**
     * Creates an instance of  UomUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  UomUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
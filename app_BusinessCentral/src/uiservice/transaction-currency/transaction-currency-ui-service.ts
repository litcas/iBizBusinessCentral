import TransactionCurrencyUIServiceBase from './transaction-currency-ui-service-base';

/**
 * 货币UI服务对象
 *
 * @export
 * @class TransactionCurrencyUIService
 */
export default class TransactionCurrencyUIService extends TransactionCurrencyUIServiceBase {

    /**
     * Creates an instance of  TransactionCurrencyUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  TransactionCurrencyUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
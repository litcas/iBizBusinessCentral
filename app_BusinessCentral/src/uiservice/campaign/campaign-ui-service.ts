import CampaignUIServiceBase from './campaign-ui-service-base';

/**
 * 市场活动UI服务对象
 *
 * @export
 * @class CampaignUIService
 */
export default class CampaignUIService extends CampaignUIServiceBase {

    /**
     * Creates an instance of  CampaignUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  CampaignUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
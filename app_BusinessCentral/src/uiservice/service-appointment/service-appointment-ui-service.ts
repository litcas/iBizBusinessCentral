import ServiceAppointmentUIServiceBase from './service-appointment-ui-service-base';

/**
 * 服务活动UI服务对象
 *
 * @export
 * @class ServiceAppointmentUIService
 */
export default class ServiceAppointmentUIService extends ServiceAppointmentUIServiceBase {

    /**
     * Creates an instance of  ServiceAppointmentUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  ServiceAppointmentUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
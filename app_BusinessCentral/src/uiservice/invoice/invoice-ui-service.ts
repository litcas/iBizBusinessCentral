import InvoiceUIServiceBase from './invoice-ui-service-base';

/**
 * 发票UI服务对象
 *
 * @export
 * @class InvoiceUIService
 */
export default class InvoiceUIService extends InvoiceUIServiceBase {

    /**
     * Creates an instance of  InvoiceUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  InvoiceUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
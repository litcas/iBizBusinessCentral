import IBZOrganizationUIServiceBase from './ibzorganization-ui-service-base';

/**
 * 单位机构UI服务对象
 *
 * @export
 * @class IBZOrganizationUIService
 */
export default class IBZOrganizationUIService extends IBZOrganizationUIServiceBase {

    /**
     * Creates an instance of  IBZOrganizationUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  IBZOrganizationUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
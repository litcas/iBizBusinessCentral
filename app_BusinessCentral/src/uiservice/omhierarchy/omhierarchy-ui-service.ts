import OMHierarchyUIServiceBase from './omhierarchy-ui-service-base';

/**
 * 组织层次结构UI服务对象
 *
 * @export
 * @class OMHierarchyUIService
 */
export default class OMHierarchyUIService extends OMHierarchyUIServiceBase {

    /**
     * Creates an instance of  OMHierarchyUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  OMHierarchyUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
import SalesLiteratureUIServiceBase from './sales-literature-ui-service-base';

/**
 * 销售宣传资料UI服务对象
 *
 * @export
 * @class SalesLiteratureUIService
 */
export default class SalesLiteratureUIService extends SalesLiteratureUIServiceBase {

    /**
     * Creates an instance of  SalesLiteratureUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  SalesLiteratureUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
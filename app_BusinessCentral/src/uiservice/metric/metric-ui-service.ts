import MetricUIServiceBase from './metric-ui-service-base';

/**
 * 目标度量UI服务对象
 *
 * @export
 * @class MetricUIService
 */
export default class MetricUIService extends MetricUIServiceBase {

    /**
     * Creates an instance of  MetricUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  MetricUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
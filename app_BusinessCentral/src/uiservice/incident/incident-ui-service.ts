import IncidentUIServiceBase from './incident-ui-service-base';

/**
 * 案例UI服务对象
 *
 * @export
 * @class IncidentUIService
 */
export default class IncidentUIService extends IncidentUIServiceBase {

    /**
     * Creates an instance of  IncidentUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  IncidentUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
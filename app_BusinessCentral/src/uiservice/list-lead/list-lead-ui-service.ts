import ListLeadUIServiceBase from './list-lead-ui-service-base';

/**
 * 营销列表-潜在客户UI服务对象
 *
 * @export
 * @class ListLeadUIService
 */
export default class ListLeadUIService extends ListLeadUIServiceBase {

    /**
     * Creates an instance of  ListLeadUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  ListLeadUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
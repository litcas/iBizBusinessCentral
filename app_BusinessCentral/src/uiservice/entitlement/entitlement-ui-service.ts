import EntitlementUIServiceBase from './entitlement-ui-service-base';

/**
 * 权利UI服务对象
 *
 * @export
 * @class EntitlementUIService
 */
export default class EntitlementUIService extends EntitlementUIServiceBase {

    /**
     * Creates an instance of  EntitlementUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EntitlementUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
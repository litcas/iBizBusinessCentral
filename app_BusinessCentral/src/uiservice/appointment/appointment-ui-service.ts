import AppointmentUIServiceBase from './appointment-ui-service-base';

/**
 * 约会UI服务对象
 *
 * @export
 * @class AppointmentUIService
 */
export default class AppointmentUIService extends AppointmentUIServiceBase {

    /**
     * Creates an instance of  AppointmentUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  AppointmentUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
import LetterUIServiceBase from './letter-ui-service-base';

/**
 * 信件UI服务对象
 *
 * @export
 * @class LetterUIService
 */
export default class LetterUIService extends LetterUIServiceBase {

    /**
     * Creates an instance of  LetterUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  LetterUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
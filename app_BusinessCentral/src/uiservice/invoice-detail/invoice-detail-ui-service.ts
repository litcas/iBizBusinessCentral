import InvoiceDetailUIServiceBase from './invoice-detail-ui-service-base';

/**
 * 发票产品UI服务对象
 *
 * @export
 * @class InvoiceDetailUIService
 */
export default class InvoiceDetailUIService extends InvoiceDetailUIServiceBase {

    /**
     * Creates an instance of  InvoiceDetailUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  InvoiceDetailUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
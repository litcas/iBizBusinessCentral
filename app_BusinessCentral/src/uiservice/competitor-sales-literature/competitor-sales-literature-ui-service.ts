import CompetitorSalesLiteratureUIServiceBase from './competitor-sales-literature-ui-service-base';

/**
 * 竞争对手宣传资料UI服务对象
 *
 * @export
 * @class CompetitorSalesLiteratureUIService
 */
export default class CompetitorSalesLiteratureUIService extends CompetitorSalesLiteratureUIServiceBase {

    /**
     * Creates an instance of  CompetitorSalesLiteratureUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  CompetitorSalesLiteratureUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
import CompetitorUIServiceBase from './competitor-ui-service-base';

/**
 * 竞争对手UI服务对象
 *
 * @export
 * @class CompetitorUIService
 */
export default class CompetitorUIService extends CompetitorUIServiceBase {

    /**
     * Creates an instance of  CompetitorUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  CompetitorUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
/**
 * 约会
 *
 * @export
 * @interface Appointment
 */
export interface Appointment {

    /**
     * 导入序列号
     *
     * @returns {*}
     * @memberof Appointment
     */
    importsequencenumber?: any;

    /**
     * 预订
     *
     * @returns {*}
     * @memberof Appointment
     */
    subscriptionid?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof Appointment
     */
    updatedate?: any;

    /**
     * 附件计数
     *
     * @returns {*}
     * @memberof Appointment
     */
    attachmentcount?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof Appointment
     */
    createdate?: any;

    /**
     * 最初开始日期
     *
     * @returns {*}
     * @memberof Appointment
     */
    originalstartdate?: any;

    /**
     * SLAName
     *
     * @returns {*}
     * @memberof Appointment
     */
    slaname?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof Appointment
     */
    createman?: any;

    /**
     * 约会
     *
     * @returns {*}
     * @memberof Appointment
     */
    activityid?: any;

    /**
     * 实际开始时间
     *
     * @returns {*}
     * @memberof Appointment
     */
    actualstart?: any;

    /**
     * 关于
     *
     * @returns {*}
     * @memberof Appointment
     */
    regardingobjectid?: any;

    /**
     * 实际结束时间
     *
     * @returns {*}
     * @memberof Appointment
     */
    actualend?: any;

    /**
     * 子类别
     *
     * @returns {*}
     * @memberof Appointment
     */
    subcategory?: any;

    /**
     * 全天事件
     *
     * @returns {*}
     * @memberof Appointment
     */
    alldayevent?: any;

    /**
     * RegardingObjectTypeCode
     *
     * @returns {*}
     * @memberof Appointment
     */
    regardingobjecttypecode?: any;

    /**
     * 约会结束时间
     *
     * @returns {*}
     * @memberof Appointment
     */
    scheduledend?: any;

    /**
     * Outlook 约会负责人
     *
     * @returns {*}
     * @memberof Appointment
     */
    outlookownerapptid?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof Appointment
     */
    description?: any;

    /**
     * 是定期活动
     *
     * @returns {*}
     * @memberof Appointment
     */
    regularactivity?: any;

    /**
     * IsUnsafe
     *
     * @returns {*}
     * @memberof Appointment
     */
    unsafe?: any;

    /**
     * 系列 ID
     *
     * @returns {*}
     * @memberof Appointment
     */
    seriesid?: any;

    /**
     * Outlook 约会
     *
     * @returns {*}
     * @memberof Appointment
     */
    globalobjectid?: any;

    /**
     * 隐藏
     *
     * @returns {*}
     * @memberof Appointment
     */
    mapiprivate?: any;

    /**
     * 安全说明
     *
     * @returns {*}
     * @memberof Appointment
     */
    safedescription?: any;

    /**
     * 主题
     *
     * @returns {*}
     * @memberof Appointment
     */
    subject?: any;

    /**
     * 由工作流创建
     *
     * @returns {*}
     * @memberof Appointment
     */
    workflowcreated?: any;

    /**
     * 关于
     *
     * @returns {*}
     * @memberof Appointment
     */
    regardingobjectname?: any;

    /**
     * 类别
     *
     * @returns {*}
     * @memberof Appointment
     */
    category?: any;

    /**
     * UTC 转换时区代码
     *
     * @returns {*}
     * @memberof Appointment
     */
    utcconversiontimezonecode?: any;

    /**
     * 已修改的字段掩码
     *
     * @returns {*}
     * @memberof Appointment
     */
    modifiedfieldsmask?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof Appointment
     */
    statecode?: any;

    /**
     * 必须出席的人员
     *
     * @returns {*}
     * @memberof Appointment
     */
    requiredattendees?: any;

    /**
     * 排序日期
     *
     * @returns {*}
     * @memberof Appointment
     */
    sortdate?: any;

    /**
     * 版本号
     *
     * @returns {*}
     * @memberof Appointment
     */
    versionnumber?: any;

    /**
     * 实际持续时间
     *
     * @returns {*}
     * @memberof Appointment
     */
    actualdurationminutes?: any;

    /**
     * 上一暂候时间
     *
     * @returns {*}
     * @memberof Appointment
     */
    lastonholdtime?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof Appointment
     */
    updateman?: any;

    /**
     * 流程阶段
     *
     * @returns {*}
     * @memberof Appointment
     */
    stageid?: any;

    /**
     * 遍历的路径
     *
     * @returns {*}
     * @memberof Appointment
     */
    traversedpath?: any;

    /**
     * 优先级
     *
     * @returns {*}
     * @memberof Appointment
     */
    prioritycode?: any;

    /**
     * AttachmentErrors
     *
     * @returns {*}
     * @memberof Appointment
     */
    attachmenterrors?: any;

    /**
     * 状态描述
     *
     * @returns {*}
     * @memberof Appointment
     */
    statuscode?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Appointment
     */
    ownerid?: any;

    /**
     * 暂候时间(分钟)
     *
     * @returns {*}
     * @memberof Appointment
     */
    onholdtime?: any;

    /**
     * 组织者
     *
     * @returns {*}
     * @memberof Appointment
     */
    organizer?: any;

    /**
     * 时区规则版本号
     *
     * @returns {*}
     * @memberof Appointment
     */
    timezoneruleversionnumber?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Appointment
     */
    ownername?: any;

    /**
     * 已记帐
     *
     * @returns {*}
     * @memberof Appointment
     */
    billed?: any;

    /**
     * 活动类型
     *
     * @returns {*}
     * @memberof Appointment
     */
    activitytypecode?: any;

    /**
     * 创建记录的时间
     *
     * @returns {*}
     * @memberof Appointment
     */
    overriddencreatedon?: any;

    /**
     * 开始时间
     *
     * @returns {*}
     * @memberof Appointment
     */
    scheduledstart?: any;

    /**
     * 负责人类型
     *
     * @returns {*}
     * @memberof Appointment
     */
    ownertype?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof Appointment
     */
    exchangerate?: any;

    /**
     * 赴约者(备选对象)
     *
     * @returns {*}
     * @memberof Appointment
     */
    optionalattendees?: any;

    /**
     * 附加参数
     *
     * @returns {*}
     * @memberof Appointment
     */
    activityadditionalparams?: any;

    /**
     * IsDraft
     *
     * @returns {*}
     * @memberof Appointment
     */
    draft?: any;

    /**
     * 约会类型
     *
     * @returns {*}
     * @memberof Appointment
     */
    instancetypecode?: any;

    /**
     * 地点
     *
     * @returns {*}
     * @memberof Appointment
     */
    location?: any;

    /**
     * 持续时间
     *
     * @returns {*}
     * @memberof Appointment
     */
    scheduleddurationminutes?: any;

    /**
     * 流程
     *
     * @returns {*}
     * @memberof Appointment
     */
    processid?: any;

    /**
     * SLA
     *
     * @returns {*}
     * @memberof Appointment
     */
    slaid?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof Appointment
     */
    transactioncurrencyid?: any;

    /**
     * 服务
     *
     * @returns {*}
     * @memberof Appointment
     */
    serviceid?: any;
}
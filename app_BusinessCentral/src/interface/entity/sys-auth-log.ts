/**
 * 认证日志
 *
 * @export
 * @interface SysAuthLog
 */
export interface SysAuthLog {

    /**
     * 标识
     *
     * @returns {*}
     * @memberof SysAuthLog
     */
    logid?: any;

    /**
     * 用户全局名
     *
     * @returns {*}
     * @memberof SysAuthLog
     */
    username?: any;

    /**
     * 用户名称
     *
     * @returns {*}
     * @memberof SysAuthLog
     */
    personname?: any;

    /**
     * 域
     *
     * @returns {*}
     * @memberof SysAuthLog
     */
    domain?: any;

    /**
     * 认证时间
     *
     * @returns {*}
     * @memberof SysAuthLog
     */
    authtime?: any;

    /**
     * IP地址
     *
     * @returns {*}
     * @memberof SysAuthLog
     */
    ipaddr?: any;

    /**
     * MAC地址
     *
     * @returns {*}
     * @memberof SysAuthLog
     */
    macaddr?: any;

    /**
     * 客户端
     *
     * @returns {*}
     * @memberof SysAuthLog
     */
    useragent?: any;

    /**
     * 认证结果
     *
     * @returns {*}
     * @memberof SysAuthLog
     */
    authcode?: any;
}
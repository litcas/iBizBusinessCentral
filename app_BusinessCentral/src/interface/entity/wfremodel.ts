/**
 * 流程模型
 *
 * @export
 * @interface WFREModel
 */
export interface WFREModel {

    /**
     * ID
     *
     * @returns {*}
     * @memberof WFREModel
     */
    id?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof WFREModel
     */
    name?: any;

    /**
     * BPMN
     *
     * @returns {*}
     * @memberof WFREModel
     */
    bpmnfile?: any;
}
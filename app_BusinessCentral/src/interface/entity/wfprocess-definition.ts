/**
 * 流程定义
 *
 * @export
 * @interface WFProcessDefinition
 */
export interface WFProcessDefinition {

    /**
     * DefinitionKey
     *
     * @returns {*}
     * @memberof WFProcessDefinition
     */
    definitionkey?: any;

    /**
     * 流程定义名称
     *
     * @returns {*}
     * @memberof WFProcessDefinition
     */
    definitionname?: any;

    /**
     * 模型版本
     *
     * @returns {*}
     * @memberof WFProcessDefinition
     */
    modelversion?: any;

    /**
     * 模型是否启用
     *
     * @returns {*}
     * @memberof WFProcessDefinition
     */
    modelenable?: any;

    /**
     * 系统标识
     *
     * @returns {*}
     * @memberof WFProcessDefinition
     */
    pssystemid?: any;

    /**
     * 校验
     *
     * @returns {*}
     * @memberof WFProcessDefinition
     */
    md5check?: any;

    /**
     * BPMN
     *
     * @returns {*}
     * @memberof WFProcessDefinition
     */
    bpmnfile?: any;

    /**
     * DeployKey
     *
     * @returns {*}
     * @memberof WFProcessDefinition
     */
    deploykey?: any;
}
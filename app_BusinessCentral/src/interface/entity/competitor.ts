/**
 * 竞争对手
 *
 * @export
 * @interface Competitor
 */
export interface Competitor {

    /**
     * 实体图像
     *
     * @returns {*}
     * @memberof Competitor
     */
    entityimage?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof Competitor
     */
    createdate?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof Competitor
     */
    updatedate?: any;

    /**
     * 地址 2: 送货方式
     *
     * @returns {*}
     * @memberof Competitor
     */
    address2_shippingmethodcode?: any;

    /**
     * 地址 2: 国家/地区
     *
     * @returns {*}
     * @memberof Competitor
     */
    address2_country?: any;

    /**
     * 地址 2: 电话 2
     *
     * @returns {*}
     * @memberof Competitor
     */
    address2_telephone2?: any;

    /**
     * 获胜率
     *
     * @returns {*}
     * @memberof Competitor
     */
    winpercentage?: any;

    /**
     * 地址 1: 送货方式
     *
     * @returns {*}
     * @memberof Competitor
     */
    address1_shippingmethodcode?: any;

    /**
     * 引用信息的 URL
     *
     * @returns {*}
     * @memberof Competitor
     */
    referenceinfourl?: any;

    /**
     * 地址 2: 纬度
     *
     * @returns {*}
     * @memberof Competitor
     */
    address2_latitude?: any;

    /**
     * Version Number
     *
     * @returns {*}
     * @memberof Competitor
     */
    versionnumber?: any;

    /**
     * 地址 2: 县
     *
     * @returns {*}
     * @memberof Competitor
     */
    address2_county?: any;

    /**
     * Import Sequence Number
     *
     * @returns {*}
     * @memberof Competitor
     */
    importsequencenumber?: any;

    /**
     * 地址 2: 电话 3
     *
     * @returns {*}
     * @memberof Competitor
     */
    address2_telephone3?: any;

    /**
     * Traversed Path
     *
     * @returns {*}
     * @memberof Competitor
     */
    traversedpath?: any;

    /**
     * 市/县
     *
     * @returns {*}
     * @memberof Competitor
     */
    address1_city?: any;

    /**
     * 报告收入 (Base)
     *
     * @returns {*}
     * @memberof Competitor
     */
    reportedrevenue_base?: any;

    /**
     * 竞争对手
     *
     * @returns {*}
     * @memberof Competitor
     */
    competitorid?: any;

    /**
     * 地址 1: 传真
     *
     * @returns {*}
     * @memberof Competitor
     */
    address1_fax?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof Competitor
     */
    updateman?: any;

    /**
     * 概述
     *
     * @returns {*}
     * @memberof Competitor
     */
    overview?: any;

    /**
     * 地址 2: 省/市/自治区
     *
     * @returns {*}
     * @memberof Competitor
     */
    address2_stateorprovince?: any;

    /**
     * EntityImageId
     *
     * @returns {*}
     * @memberof Competitor
     */
    entityimageid?: any;

    /**
     * Time Zone Rule Version Number
     *
     * @returns {*}
     * @memberof Competitor
     */
    timezoneruleversionnumber?: any;

    /**
     * 地址 2: UPS 区域
     *
     * @returns {*}
     * @memberof Competitor
     */
    address2_upszone?: any;

    /**
     * 街道 3
     *
     * @returns {*}
     * @memberof Competitor
     */
    address1_line3?: any;

    /**
     * 证券交易所
     *
     * @returns {*}
     * @memberof Competitor
     */
    stockexchange?: any;

    /**
     * Stage Id
     *
     * @returns {*}
     * @memberof Competitor
     */
    stageid?: any;

    /**
     * 报告年度
     *
     * @returns {*}
     * @memberof Competitor
     */
    reportingyear?: any;

    /**
     * 报告季度
     *
     * @returns {*}
     * @memberof Competitor
     */
    reportingquarter?: any;

    /**
     * 省/直辖市/自治区
     *
     * @returns {*}
     * @memberof Competitor
     */
    address1_stateorprovince?: any;

    /**
     * 地址 1: UTC 时差
     *
     * @returns {*}
     * @memberof Competitor
     */
    address1_utcoffset?: any;

    /**
     * 地址 1: 纬度
     *
     * @returns {*}
     * @memberof Competitor
     */
    address1_latitude?: any;

    /**
     * 地址 1
     *
     * @returns {*}
     * @memberof Competitor
     */
    address1_composite?: any;

    /**
     * 地址 1: 县
     *
     * @returns {*}
     * @memberof Competitor
     */
    address1_county?: any;

    /**
     * 地址 1: 名称
     *
     * @returns {*}
     * @memberof Competitor
     */
    address1_name?: any;

    /**
     * 邮政编码
     *
     * @returns {*}
     * @memberof Competitor
     */
    address1_postalcode?: any;

    /**
     * 商机
     *
     * @returns {*}
     * @memberof Competitor
     */
    opportunities?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof Competitor
     */
    name?: any;

    /**
     * 地址 1: ID
     *
     * @returns {*}
     * @memberof Competitor
     */
    address1_addressid?: any;

    /**
     * 地址 2: 街道 3
     *
     * @returns {*}
     * @memberof Competitor
     */
    address2_line3?: any;

    /**
     * 地址 2: 传真
     *
     * @returns {*}
     * @memberof Competitor
     */
    address2_fax?: any;

    /**
     * Process Id
     *
     * @returns {*}
     * @memberof Competitor
     */
    processid?: any;

    /**
     * 地址 2: 邮政信箱
     *
     * @returns {*}
     * @memberof Competitor
     */
    address2_postofficebox?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof Competitor
     */
    createman?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof Competitor
     */
    exchangerate?: any;

    /**
     * Record Created On
     *
     * @returns {*}
     * @memberof Competitor
     */
    overriddencreatedon?: any;

    /**
     * 地址 1: 电话 2
     *
     * @returns {*}
     * @memberof Competitor
     */
    address1_telephone2?: any;

    /**
     * 优势
     *
     * @returns {*}
     * @memberof Competitor
     */
    strengths?: any;

    /**
     * 地址 2: 地址类型
     *
     * @returns {*}
     * @memberof Competitor
     */
    address2_addresstypecode?: any;

    /**
     * 地址 1: 邮政信箱
     *
     * @returns {*}
     * @memberof Competitor
     */
    address1_postofficebox?: any;

    /**
     * 劣势
     *
     * @returns {*}
     * @memberof Competitor
     */
    weaknesses?: any;

    /**
     * 街道 2
     *
     * @returns {*}
     * @memberof Competitor
     */
    address1_line2?: any;

    /**
     * 地址 2: 经度
     *
     * @returns {*}
     * @memberof Competitor
     */
    address2_longitude?: any;

    /**
     * 威胁
     *
     * @returns {*}
     * @memberof Competitor
     */
    threats?: any;

    /**
     * 地址 2
     *
     * @returns {*}
     * @memberof Competitor
     */
    address2_composite?: any;

    /**
     * EntityImage_Timestamp
     *
     * @returns {*}
     * @memberof Competitor
     */
    entityimage_timestamp?: any;

    /**
     * 地址 2: UTC 时差
     *
     * @returns {*}
     * @memberof Competitor
     */
    address2_utcoffset?: any;

    /**
     * 国家/地区
     *
     * @returns {*}
     * @memberof Competitor
     */
    address1_country?: any;

    /**
     * UTC Conversion Time Zone Code
     *
     * @returns {*}
     * @memberof Competitor
     */
    utcconversiontimezonecode?: any;

    /**
     * 报告收入
     *
     * @returns {*}
     * @memberof Competitor
     */
    reportedrevenue?: any;

    /**
     * 地址 1: 经度
     *
     * @returns {*}
     * @memberof Competitor
     */
    address1_longitude?: any;

    /**
     * 股票代号
     *
     * @returns {*}
     * @memberof Competitor
     */
    tickersymbol?: any;

    /**
     * 地址 2: 电话 1
     *
     * @returns {*}
     * @memberof Competitor
     */
    address2_telephone1?: any;

    /**
     * 主要产品
     *
     * @returns {*}
     * @memberof Competitor
     */
    keyproduct?: any;

    /**
     * EntityImage_URL
     *
     * @returns {*}
     * @memberof Competitor
     */
    entityimage_url?: any;

    /**
     * 地址 2: 市/县
     *
     * @returns {*}
     * @memberof Competitor
     */
    address2_city?: any;

    /**
     * 网站
     *
     * @returns {*}
     * @memberof Competitor
     */
    websiteurl?: any;

    /**
     * 地址 2: ID
     *
     * @returns {*}
     * @memberof Competitor
     */
    address2_addressid?: any;

    /**
     * 地址 1: 电话 1
     *
     * @returns {*}
     * @memberof Competitor
     */
    address1_telephone1?: any;

    /**
     * 地址 2: 邮政编码
     *
     * @returns {*}
     * @memberof Competitor
     */
    address2_postalcode?: any;

    /**
     * 街道 1
     *
     * @returns {*}
     * @memberof Competitor
     */
    address1_line1?: any;

    /**
     * 地址 1: 电话 3
     *
     * @returns {*}
     * @memberof Competitor
     */
    address1_telephone3?: any;

    /**
     * 地址 2: 街道 1
     *
     * @returns {*}
     * @memberof Competitor
     */
    address2_line1?: any;

    /**
     * 地址 1: 地址类型
     *
     * @returns {*}
     * @memberof Competitor
     */
    address1_addresstypecode?: any;

    /**
     * 地址 2: 名称
     *
     * @returns {*}
     * @memberof Competitor
     */
    address2_name?: any;

    /**
     * 竞争对手名称
     *
     * @returns {*}
     * @memberof Competitor
     */
    competitorname?: any;

    /**
     * 地址 1: UPS 区域
     *
     * @returns {*}
     * @memberof Competitor
     */
    address1_upszone?: any;

    /**
     * 地址 2: 街道 2
     *
     * @returns {*}
     * @memberof Competitor
     */
    address2_line2?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof Competitor
     */
    currencyname?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof Competitor
     */
    transactioncurrencyid?: any;
}
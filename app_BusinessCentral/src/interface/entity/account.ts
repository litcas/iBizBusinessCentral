/**
 * 客户
 *
 * @export
 * @interface Account
 */
export interface Account {

    /**
     * 地址 1: 主要联系人姓名
     *
     * @returns {*}
     * @memberof Account
     */
    address1_primarycontactname?: any;

    /**
     * 开启的收入 (Last Updated On)
     *
     * @returns {*}
     * @memberof Account
     */
    openrevenue_date?: any;

    /**
     * 地址 2: UTC 时差
     *
     * @returns {*}
     * @memberof Account
     */
    address2_utcoffset?: any;

    /**
     * 传真
     *
     * @returns {*}
     * @memberof Account
     */
    fax?: any;

    /**
     * 地址 1
     *
     * @returns {*}
     * @memberof Account
     */
    address1_composite?: any;

    /**
     * 默认图像
     *
     * @returns {*}
     * @memberof Account
     */
    entityimage?: any;

    /**
     * 电子邮件
     *
     * @returns {*}
     * @memberof Account
     */
    emailaddress1?: any;

    /**
     * 创建记录的时间
     *
     * @returns {*}
     * @memberof Account
     */
    overriddencreatedon?: any;

    /**
     * 地址 1: 省/市/自治区
     *
     * @returns {*}
     * @memberof Account
     */
    address1_stateorprovince?: any;

    /**
     * 开启的收入
     *
     * @returns {*}
     * @memberof Account
     */
    openrevenue?: any;

    /**
     * 地址 1: UPS 区域
     *
     * @returns {*}
     * @memberof Account
     */
    address1_upszone?: any;

    /**
     * 跟踪电子邮件活动
     *
     * @returns {*}
     * @memberof Account
     */
    followemail?: any;

    /**
     * 仅限市场营销
     *
     * @returns {*}
     * @memberof Account
     */
    marketingonly?: any;

    /**
     * 员工人数
     *
     * @returns {*}
     * @memberof Account
     */
    numberofemployees?: any;

    /**
     * 主要电话
     *
     * @returns {*}
     * @memberof Account
     */
    telephone1?: any;

    /**
     * 时效 60
     *
     * @returns {*}
     * @memberof Account
     */
    aging60?: any;

    /**
     * 已发行股票
     *
     * @returns {*}
     * @memberof Account
     */
    sharesoutstanding?: any;

    /**
     * 地址 2: 货运条款
     *
     * @returns {*}
     * @memberof Account
     */
    address2_freighttermscode?: any;

    /**
     * 实体图片时间戳
     *
     * @returns {*}
     * @memberof Account
     */
    entityimage_timestamp?: any;

    /**
     * 年收入(基础货币)
     *
     * @returns {*}
     * @memberof Account
     */
    revenue_base?: any;

    /**
     * 客户等级
     *
     * @returns {*}
     * @memberof Account
     */
    accountratingcode?: any;

    /**
     * 不允许使用批量电子邮件
     *
     * @returns {*}
     * @memberof Account
     */
    donotbulkemail?: any;

    /**
     * 时效 30
     *
     * @returns {*}
     * @memberof Account
     */
    aging30?: any;

    /**
     * 不允许使用批量邮件
     *
     * @returns {*}
     * @memberof Account
     */
    donotbulkpostalmail?: any;

    /**
     * 信用额度(基础货币)
     *
     * @returns {*}
     * @memberof Account
     */
    creditlimit_base?: any;

    /**
     * 时区规则版本号
     *
     * @returns {*}
     * @memberof Account
     */
    timezoneruleversionnumber?: any;

    /**
     * 关系类型
     *
     * @returns {*}
     * @memberof Account
     */
    customertypecode?: any;

    /**
     * 地址 2: 经度
     *
     * @returns {*}
     * @memberof Account
     */
    address2_longitude?: any;

    /**
     * 客户名称
     *
     * @returns {*}
     * @memberof Account
     */
    accountname?: any;

    /**
     * 参与工作流
     *
     * @returns {*}
     * @memberof Account
     */
    participatesinworkflow?: any;

    /**
     * 首选联系方式
     *
     * @returns {*}
     * @memberof Account
     */
    preferredcontactmethodcode?: any;

    /**
     * 首选日
     *
     * @returns {*}
     * @memberof Account
     */
    preferredappointmentdaycode?: any;

    /**
     * 市值
     *
     * @returns {*}
     * @memberof Account
     */
    marketcap?: any;

    /**
     * 地址 2: 电话 1
     *
     * @returns {*}
     * @memberof Account
     */
    address2_telephone1?: any;

    /**
     * 商业类型
     *
     * @returns {*}
     * @memberof Account
     */
    businesstypecode?: any;

    /**
     * 实体图像 ID
     *
     * @returns {*}
     * @memberof Account
     */
    entityimageid?: any;

    /**
     * 地址 2: 市/县
     *
     * @returns {*}
     * @memberof Account
     */
    address2_city?: any;

    /**
     * 所有权
     *
     * @returns {*}
     * @memberof Account
     */
    ownershipcode?: any;

    /**
     * UTC 转换时区代码
     *
     * @returns {*}
     * @memberof Account
     */
    utcconversiontimezonecode?: any;

    /**
     * 客户编号
     *
     * @returns {*}
     * @memberof Account
     */
    accountnumber?: any;

    /**
     * 地址 1: 电话 2
     *
     * @returns {*}
     * @memberof Account
     */
    address1_telephone2?: any;

    /**
     * 地址 1: 街道 1
     *
     * @returns {*}
     * @memberof Account
     */
    address1_line1?: any;

    /**
     * 不允许电话联络
     *
     * @returns {*}
     * @memberof Account
     */
    donotphone?: any;

    /**
     * 地址 1: 电话 3
     *
     * @returns {*}
     * @memberof Account
     */
    address1_telephone3?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof Account
     */
    exchangerate?: any;

    /**
     * 地址 2: 街道 2
     *
     * @returns {*}
     * @memberof Account
     */
    address2_line2?: any;

    /**
     * 地址 2: 电话 3
     *
     * @returns {*}
     * @memberof Account
     */
    address2_telephone3?: any;

    /**
     * 联络电话
     *
     * @returns {*}
     * @memberof Account
     */
    address1_telephone1?: any;

    /**
     * 地址 1: 传真
     *
     * @returns {*}
     * @memberof Account
     */
    address1_fax?: any;

    /**
     * 地址 1: 货运条款
     *
     * @returns {*}
     * @memberof Account
     */
    address1_freighttermscode?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof Account
     */
    description?: any;

    /**
     * 合并
     *
     * @returns {*}
     * @memberof Account
     */
    merged?: any;

    /**
     * 地址 1: 送货方式
     *
     * @returns {*}
     * @memberof Account
     */
    address1_shippingmethodcode?: any;

    /**
     * 地址 1: 名称
     *
     * @returns {*}
     * @memberof Account
     */
    address1_name?: any;

    /**
     * 时效 30 (基础货币)
     *
     * @returns {*}
     * @memberof Account
     */
    aging30_base?: any;

    /**
     * 地址 2: 电话 2
     *
     * @returns {*}
     * @memberof Account
     */
    address2_telephone2?: any;

    /**
     * 相关客户数量
     *
     * @returns {*}
     * @memberof Account
     */
    childaccountcount?: any;

    /**
     * 客户
     *
     * @returns {*}
     * @memberof Account
     */
    accountid?: any;

    /**
     * 主要 Twitter ID
     *
     * @returns {*}
     * @memberof Account
     */
    primarytwitterid?: any;

    /**
     * 股票代号
     *
     * @returns {*}
     * @memberof Account
     */
    tickersymbol?: any;

    /**
     * 证券交易所
     *
     * @returns {*}
     * @memberof Account
     */
    stockexchange?: any;

    /**
     * 地址 2: 省/市/自治区
     *
     * @returns {*}
     * @memberof Account
     */
    address2_stateorprovince?: any;

    /**
     * FTP 站点
     *
     * @returns {*}
     * @memberof Account
     */
    ftpsiteurl?: any;

    /**
     * 状态描述
     *
     * @returns {*}
     * @memberof Account
     */
    statuscode?: any;

    /**
     * 网站
     *
     * @returns {*}
     * @memberof Account
     */
    websiteurl?: any;

    /**
     * 不允许使用邮件
     *
     * @returns {*}
     * @memberof Account
     */
    donotpostalmail?: any;

    /**
     * 地址 1: 市/县
     *
     * @returns {*}
     * @memberof Account
     */
    address1_city?: any;

    /**
     * 地址 2: 传真
     *
     * @returns {*}
     * @memberof Account
     */
    address2_fax?: any;

    /**
     * 时效 90 (基础货币)
     *
     * @returns {*}
     * @memberof Account
     */
    aging90_base?: any;

    /**
     * 开启的交易 (Last Updated On)
     *
     * @returns {*}
     * @memberof Account
     */
    opendeals_date?: any;

    /**
     * 遍历的路径
     *
     * @returns {*}
     * @memberof Account
     */
    traversedpath?: any;

    /**
     * 送货方式
     *
     * @returns {*}
     * @memberof Account
     */
    shippingmethodcode?: any;

    /**
     * 地址 1: 街道 2
     *
     * @returns {*}
     * @memberof Account
     */
    address1_line2?: any;

    /**
     * 开启的收入 (Base)
     *
     * @returns {*}
     * @memberof Account
     */
    openrevenue_base?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof Account
     */
    updatedate?: any;

    /**
     * 地址 2: 街道 3
     *
     * @returns {*}
     * @memberof Account
     */
    address2_line3?: any;

    /**
     * 地址 1: 邮政编码
     *
     * @returns {*}
     * @memberof Account
     */
    address1_postalcode?: any;

    /**
     * 地址 2: 纬度
     *
     * @returns {*}
     * @memberof Account
     */
    address2_latitude?: any;

    /**
     * 是否私有
     *
     * @returns {*}
     * @memberof Account
     */
    ibizprivate?: any;

    /**
     * 区域代码
     *
     * @returns {*}
     * @memberof Account
     */
    territorycode?: any;

    /**
     * TeamsFollowed
     *
     * @returns {*}
     * @memberof Account
     */
    teamsfollowed?: any;

    /**
     * 地址 1: 纬度
     *
     * @returns {*}
     * @memberof Account
     */
    address1_latitude?: any;

    /**
     * 电话 3
     *
     * @returns {*}
     * @memberof Account
     */
    telephone3?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Account
     */
    ownerid?: any;

    /**
     * 地址 2: 国家/地区
     *
     * @returns {*}
     * @memberof Account
     */
    address2_country?: any;

    /**
     * 主客户
     *
     * @returns {*}
     * @memberof Account
     */
    masteraccountname?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Account
     */
    ownername?: any;

    /**
     * 行业
     *
     * @returns {*}
     * @memberof Account
     */
    industrycode?: any;

    /**
     * 客户
     *
     * @returns {*}
     * @memberof Account
     */
    name?: any;

    /**
     * 上一暂候时间
     *
     * @returns {*}
     * @memberof Account
     */
    lastonholdtime?: any;

    /**
     * 地址 2: ID
     *
     * @returns {*}
     * @memberof Account
     */
    address2_addressid?: any;

    /**
     * 暂候时间(分钟)
     *
     * @returns {*}
     * @memberof Account
     */
    onholdtime?: any;

    /**
     * 分类
     *
     * @returns {*}
     * @memberof Account
     */
    accountclassificationcode?: any;

    /**
     * 地址 1: 国家/地区
     *
     * @returns {*}
     * @memberof Account
     */
    address1_country?: any;

    /**
     * 地址 1: 地址类型
     *
     * @returns {*}
     * @memberof Account
     */
    address1_addresstypecode?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof Account
     */
    statecode?: any;

    /**
     * 地址 2: 地址类型
     *
     * @returns {*}
     * @memberof Account
     */
    address2_addresstypecode?: any;

    /**
     * 类别
     *
     * @returns {*}
     * @memberof Account
     */
    accountcategorycode?: any;

    /**
     * 电子邮件地址 2
     *
     * @returns {*}
     * @memberof Account
     */
    emailaddress2?: any;

    /**
     * 地址 2: 送货方式
     *
     * @returns {*}
     * @memberof Account
     */
    address2_shippingmethodcode?: any;

    /**
     * 导入序列号
     *
     * @returns {*}
     * @memberof Account
     */
    importsequencenumber?: any;

    /**
     * 主要 Satori ID
     *
     * @returns {*}
     * @memberof Account
     */
    primarysatoriid?: any;

    /**
     * 客户规模
     *
     * @returns {*}
     * @memberof Account
     */
    customersizecode?: any;

    /**
     * 开启的交易
     *
     * @returns {*}
     * @memberof Account
     */
    opendeals?: any;

    /**
     * 发送市场营销资料
     *
     * @returns {*}
     * @memberof Account
     */
    donotsendmm?: any;

    /**
     * 流程
     *
     * @returns {*}
     * @memberof Account
     */
    processid?: any;

    /**
     * 付款方式
     *
     * @returns {*}
     * @memberof Account
     */
    paymenttermscode?: any;

    /**
     * 行业编码
     *
     * @returns {*}
     * @memberof Account
     */
    sic?: any;

    /**
     * 地址 2: 主要联系人姓名
     *
     * @returns {*}
     * @memberof Account
     */
    address2_primarycontactname?: any;

    /**
     * 地址 1: UTC 时差
     *
     * @returns {*}
     * @memberof Account
     */
    address1_utcoffset?: any;

    /**
     * 上次参与市场活动的日期
     *
     * @returns {*}
     * @memberof Account
     */
    lastusedincampaign?: any;

    /**
     * 时效 60 (基础货币)
     *
     * @returns {*}
     * @memberof Account
     */
    aging60_base?: any;

    /**
     * 不允许使用传真
     *
     * @returns {*}
     * @memberof Account
     */
    donotfax?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof Account
     */
    updateman?: any;

    /**
     * 开启的收入 (State)
     *
     * @returns {*}
     * @memberof Account
     */
    openrevenue_state?: any;

    /**
     * 开启的交易 (State)
     *
     * @returns {*}
     * @memberof Account
     */
    opendeals_state?: any;

    /**
     * 版本号
     *
     * @returns {*}
     * @memberof Account
     */
    versionnumber?: any;

    /**
     * 地址 1: 经度
     *
     * @returns {*}
     * @memberof Account
     */
    address1_longitude?: any;

    /**
     * 时效 90
     *
     * @returns {*}
     * @memberof Account
     */
    aging90?: any;

    /**
     * 实体图片路径
     *
     * @returns {*}
     * @memberof Account
     */
    entityimage_url?: any;

    /**
     * 信用额度
     *
     * @returns {*}
     * @memberof Account
     */
    creditlimit?: any;

    /**
     * 首选用户
     *
     * @returns {*}
     * @memberof Account
     */
    preferredsystemuserid?: any;

    /**
     * 年收入
     *
     * @returns {*}
     * @memberof Account
     */
    revenue?: any;

    /**
     * 地址 1: 县
     *
     * @returns {*}
     * @memberof Account
     */
    address1_county?: any;

    /**
     * 地址 1: 街道 3
     *
     * @returns {*}
     * @memberof Account
     */
    address1_line3?: any;

    /**
     * 首选用户
     *
     * @returns {*}
     * @memberof Account
     */
    preferredsystemusername?: any;

    /**
     * 流程阶段
     *
     * @returns {*}
     * @memberof Account
     */
    stageid?: any;

    /**
     * 不允许使用电子邮件
     *
     * @returns {*}
     * @memberof Account
     */
    donotemail?: any;

    /**
     * 地址 2: 邮政编码
     *
     * @returns {*}
     * @memberof Account
     */
    address2_postalcode?: any;

    /**
     * 首选时间
     *
     * @returns {*}
     * @memberof Account
     */
    preferredappointmenttimecode?: any;

    /**
     * 地址 2: UPS 区域
     *
     * @returns {*}
     * @memberof Account
     */
    address2_upszone?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof Account
     */
    createdate?: any;

    /**
     * 地址 2: 县
     *
     * @returns {*}
     * @memberof Account
     */
    address2_county?: any;

    /**
     * 地址 2
     *
     * @returns {*}
     * @memberof Account
     */
    address2_composite?: any;

    /**
     * 地址 2: 名称
     *
     * @returns {*}
     * @memberof Account
     */
    address2_name?: any;

    /**
     * 地址 2: 街道 1
     *
     * @returns {*}
     * @memberof Account
     */
    address2_line1?: any;

    /**
     * 市值(基础货币)
     *
     * @returns {*}
     * @memberof Account
     */
    marketcap_base?: any;

    /**
     * 电子邮件地址 3
     *
     * @returns {*}
     * @memberof Account
     */
    emailaddress3?: any;

    /**
     * 所有者类型
     *
     * @returns {*}
     * @memberof Account
     */
    ownertype?: any;

    /**
     * 其他电话
     *
     * @returns {*}
     * @memberof Account
     */
    telephone2?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof Account
     */
    createman?: any;

    /**
     * 地址 1: 邮政信箱
     *
     * @returns {*}
     * @memberof Account
     */
    address1_postofficebox?: any;

    /**
     * 信用冻结
     *
     * @returns {*}
     * @memberof Account
     */
    creditonhold?: any;

    /**
     * 地址 1: ID
     *
     * @returns {*}
     * @memberof Account
     */
    address1_addressid?: any;

    /**
     * 地址 2: 邮政信箱
     *
     * @returns {*}
     * @memberof Account
     */
    address2_postofficebox?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof Account
     */
    currencyname?: any;

    /**
     * 价目表
     *
     * @returns {*}
     * @memberof Account
     */
    defaultpricelevelname?: any;

    /**
     * SLAName
     *
     * @returns {*}
     * @memberof Account
     */
    slaname?: any;

    /**
     * 首选设施/设备
     *
     * @returns {*}
     * @memberof Account
     */
    preferredequipmentname?: any;

    /**
     * 首选服务
     *
     * @returns {*}
     * @memberof Account
     */
    preferredservicename?: any;

    /**
     * 区域
     *
     * @returns {*}
     * @memberof Account
     */
    territoryname?: any;

    /**
     * 主要联系人
     *
     * @returns {*}
     * @memberof Account
     */
    primarycontactname?: any;

    /**
     * 上级客户
     *
     * @returns {*}
     * @memberof Account
     */
    parentaccountname?: any;

    /**
     * 原始潜在顾客
     *
     * @returns {*}
     * @memberof Account
     */
    originatingleadname?: any;

    /**
     * 原始潜在顾客
     *
     * @returns {*}
     * @memberof Account
     */
    originatingleadid?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof Account
     */
    transactioncurrencyid?: any;

    /**
     * 价目表
     *
     * @returns {*}
     * @memberof Account
     */
    defaultpricelevelid?: any;

    /**
     * 上级单位
     *
     * @returns {*}
     * @memberof Account
     */
    parentaccountid?: any;

    /**
     * 首选设施/设备
     *
     * @returns {*}
     * @memberof Account
     */
    preferredequipmentid?: any;

    /**
     * 区域
     *
     * @returns {*}
     * @memberof Account
     */
    territoryid?: any;

    /**
     * 主要联系人
     *
     * @returns {*}
     * @memberof Account
     */
    primarycontactid?: any;

    /**
     * SLA
     *
     * @returns {*}
     * @memberof Account
     */
    slaid?: any;

    /**
     * 首选服务
     *
     * @returns {*}
     * @memberof Account
     */
    preferredserviceid?: any;
}
/**
 * 联系人
 *
 * @export
 * @interface Contact
 */
export interface Contact {

    /**
     * 地址 1: 货运条款
     *
     * @returns {*}
     * @memberof Contact
     */
    address1_freighttermscode?: any;

    /**
     * 地址 3: 市/县
     *
     * @returns {*}
     * @memberof Contact
     */
    address3_city?: any;

    /**
     * 部门
     *
     * @returns {*}
     * @memberof Contact
     */
    department?: any;

    /**
     * 地址 1: 省/市/自治区
     *
     * @returns {*}
     * @memberof Contact
     */
    address1_stateorprovince?: any;

    /**
     * 时效 90 (基础货币)
     *
     * @returns {*}
     * @memberof Contact
     */
    aging90_base?: any;

    /**
     * 不允许使用批量邮件
     *
     * @returns {*}
     * @memberof Contact
     */
    donotbulkpostalmail?: any;

    /**
     * 经理
     *
     * @returns {*}
     * @memberof Contact
     */
    managername?: any;

    /**
     * 不允许使用邮件
     *
     * @returns {*}
     * @memberof Contact
     */
    donotpostalmail?: any;

    /**
     * 配偶/伴侣姓名
     *
     * @returns {*}
     * @memberof Contact
     */
    spousesname?: any;

    /**
     * 婚姻状况
     *
     * @returns {*}
     * @memberof Contact
     */
    familystatuscode?: any;

    /**
     * 地址 3
     *
     * @returns {*}
     * @memberof Contact
     */
    address3_composite?: any;

    /**
     * 地址 3: 送货方式
     *
     * @returns {*}
     * @memberof Contact
     */
    address3_shippingmethodcode?: any;

    /**
     * 姓
     *
     * @returns {*}
     * @memberof Contact
     */
    lastname?: any;

    /**
     * 上一暂候时间
     *
     * @returns {*}
     * @memberof Contact
     */
    lastonholdtime?: any;

    /**
     * 教育
     *
     * @returns {*}
     * @memberof Contact
     */
    educationcode?: any;

    /**
     * 地址 2: 街道 1
     *
     * @returns {*}
     * @memberof Contact
     */
    address2_line1?: any;

    /**
     * 生日
     *
     * @returns {*}
     * @memberof Contact
     */
    birthdate?: any;

    /**
     * 有子女
     *
     * @returns {*}
     * @memberof Contact
     */
    haschildrencode?: any;

    /**
     * 公司电话
     *
     * @returns {*}
     * @memberof Contact
     */
    company?: any;

    /**
     * 地址 2: 传真
     *
     * @returns {*}
     * @memberof Contact
     */
    address2_fax?: any;

    /**
     * 助理电话
     *
     * @returns {*}
     * @memberof Contact
     */
    assistantphone?: any;

    /**
     * 回拨号码
     *
     * @returns {*}
     * @memberof Contact
     */
    callback?: any;

    /**
     * 付款方式
     *
     * @returns {*}
     * @memberof Contact
     */
    paymenttermscode?: any;

    /**
     * 地址 2: 电话 1
     *
     * @returns {*}
     * @memberof Contact
     */
    address2_telephone1?: any;

    /**
     * 仅限市场营销
     *
     * @returns {*}
     * @memberof Contact
     */
    marketingonly?: any;

    /**
     * 地址 2: UTC 时差
     *
     * @returns {*}
     * @memberof Contact
     */
    address2_utcoffset?: any;

    /**
     * 地址 2: 送货方式
     *
     * @returns {*}
     * @memberof Contact
     */
    address2_shippingmethodcode?: any;

    /**
     * 遍历的路径
     *
     * @returns {*}
     * @memberof Contact
     */
    traversedpath?: any;

    /**
     * 员工
     *
     * @returns {*}
     * @memberof Contact
     */
    employeeid?: any;

    /**
     * 信用额度(基础货币)
     *
     * @returns {*}
     * @memberof Contact
     */
    creditlimit_base?: any;

    /**
     * 跟踪电子邮件活动
     *
     * @returns {*}
     * @memberof Contact
     */
    followemail?: any;

    /**
     * 地址 3: 邮政编码
     *
     * @returns {*}
     * @memberof Contact
     */
    address3_postalcode?: any;

    /**
     * 合并
     *
     * @returns {*}
     * @memberof Contact
     */
    merged?: any;

    /**
     * 职务
     *
     * @returns {*}
     * @memberof Contact
     */
    jobtitle?: any;

    /**
     * 地址 1: 电话
     *
     * @returns {*}
     * @memberof Contact
     */
    address1_telephone1?: any;

    /**
     * 客户规模
     *
     * @returns {*}
     * @memberof Contact
     */
    customersizecode?: any;

    /**
     * 地址 3: 地址类型
     *
     * @returns {*}
     * @memberof Contact
     */
    address3_addresstypecode?: any;

    /**
     * 寻呼机
     *
     * @returns {*}
     * @memberof Contact
     */
    pager?: any;

    /**
     * 助理
     *
     * @returns {*}
     * @memberof Contact
     */
    assistantname?: any;

    /**
     * 地址 1
     *
     * @returns {*}
     * @memberof Contact
     */
    address1_composite?: any;

    /**
     * 地址 1: 街道 1
     *
     * @returns {*}
     * @memberof Contact
     */
    address1_line1?: any;

    /**
     * 地址 1: 电话 3
     *
     * @returns {*}
     * @memberof Contact
     */
    address1_telephone3?: any;

    /**
     * 住宅电话
     *
     * @returns {*}
     * @memberof Contact
     */
    telephone2?: any;

    /**
     * 地址 2: ID
     *
     * @returns {*}
     * @memberof Contact
     */
    address2_addressid?: any;

    /**
     * 潜在顾客来源
     *
     * @returns {*}
     * @memberof Contact
     */
    leadsourcecode?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof Contact
     */
    statecode?: any;

    /**
     * 地址 2: 货运条款
     *
     * @returns {*}
     * @memberof Contact
     */
    address2_freighttermscode?: any;

    /**
     * 电子邮件
     *
     * @returns {*}
     * @memberof Contact
     */
    emailaddress1?: any;

    /**
     * EntityImage_Timestamp
     *
     * @returns {*}
     * @memberof Contact
     */
    entityimage_timestamp?: any;

    /**
     * 地址 3: 街道 1
     *
     * @returns {*}
     * @memberof Contact
     */
    address3_line1?: any;

    /**
     * 称呼
     *
     * @returns {*}
     * @memberof Contact
     */
    salutation?: any;

    /**
     * 地址 1: 街道 3
     *
     * @returns {*}
     * @memberof Contact
     */
    address1_line3?: any;

    /**
     * 地址 3: 主要联系人姓名
     *
     * @returns {*}
     * @memberof Contact
     */
    address3_primarycontactname?: any;

    /**
     * 是否私有
     *
     * @returns {*}
     * @memberof Contact
     */
    ibizprivate?: any;

    /**
     * 不允许使用传真
     *
     * @returns {*}
     * @memberof Contact
     */
    donotfax?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof Contact
     */
    createdate?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof Contact
     */
    updatedate?: any;

    /**
     * 地址 3: 省/直辖市/自治区
     *
     * @returns {*}
     * @memberof Contact
     */
    address3_stateorprovince?: any;

    /**
     * 地址 3: 街道 3
     *
     * @returns {*}
     * @memberof Contact
     */
    address3_line3?: any;

    /**
     * 信用额度
     *
     * @returns {*}
     * @memberof Contact
     */
    creditlimit?: any;

    /**
     * 时区规则版本号
     *
     * @returns {*}
     * @memberof Contact
     */
    timezoneruleversionnumber?: any;

    /**
     * 公司名称
     *
     * @returns {*}
     * @memberof Contact
     */
    parentcustomerid?: any;

    /**
     * 子女的姓名
     *
     * @returns {*}
     * @memberof Contact
     */
    childrensnames?: any;

    /**
     * 地址 1: 地址类型
     *
     * @returns {*}
     * @memberof Contact
     */
    address1_addresstypecode?: any;

    /**
     * 角色
     *
     * @returns {*}
     * @memberof Contact
     */
    accountrolecode?: any;

    /**
     * 不允许电话联络
     *
     * @returns {*}
     * @memberof Contact
     */
    donotphone?: any;

    /**
     * 经理电话
     *
     * @returns {*}
     * @memberof Contact
     */
    managerphone?: any;

    /**
     * 信用冻结
     *
     * @returns {*}
     * @memberof Contact
     */
    creditonhold?: any;

    /**
     * 客户
     *
     * @returns {*}
     * @memberof Contact
     */
    accountname?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof Contact
     */
    updateman?: any;

    /**
     * 地址 2: 邮政编码
     *
     * @returns {*}
     * @memberof Contact
     */
    address2_postalcode?: any;

    /**
     * 地址 1: 街道 2
     *
     * @returns {*}
     * @memberof Contact
     */
    address1_line2?: any;

    /**
     * 上级客户类型
     *
     * @returns {*}
     * @memberof Contact
     */
    parentcustomertype?: any;

    /**
     * 昵称
     *
     * @returns {*}
     * @memberof Contact
     */
    nickname?: any;

    /**
     * 版本号
     *
     * @returns {*}
     * @memberof Contact
     */
    versionnumber?: any;

    /**
     * 实体图像
     *
     * @returns {*}
     * @memberof Contact
     */
    entityimage?: any;

    /**
     * 送货方式
     *
     * @returns {*}
     * @memberof Contact
     */
    shippingmethodcode?: any;

    /**
     * 关系类型
     *
     * @returns {*}
     * @memberof Contact
     */
    customertypecode?: any;

    /**
     * 地址 2: 县
     *
     * @returns {*}
     * @memberof Contact
     */
    address2_county?: any;

    /**
     * 时效 90
     *
     * @returns {*}
     * @memberof Contact
     */
    aging90?: any;

    /**
     * 地址 2: 省/市/自治区
     *
     * @returns {*}
     * @memberof Contact
     */
    address2_stateorprovince?: any;

    /**
     * 地址 3: UTC 时差
     *
     * @returns {*}
     * @memberof Contact
     */
    address3_utcoffset?: any;

    /**
     * 全名
     *
     * @returns {*}
     * @memberof Contact
     */
    fullname?: any;

    /**
     * 参与工作流
     *
     * @returns {*}
     * @memberof Contact
     */
    participatesinworkflow?: any;

    /**
     * 网站
     *
     * @returns {*}
     * @memberof Contact
     */
    websiteurl?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof Contact
     */
    description?: any;

    /**
     * 地址 3: 电话 1
     *
     * @returns {*}
     * @memberof Contact
     */
    address3_telephone1?: any;

    /**
     * 地址 2: UPS 区域
     *
     * @returns {*}
     * @memberof Contact
     */
    address2_upszone?: any;

    /**
     * 地址 3: 县
     *
     * @returns {*}
     * @memberof Contact
     */
    address3_county?: any;

    /**
     * EntityImage_URL
     *
     * @returns {*}
     * @memberof Contact
     */
    entityimage_url?: any;

    /**
     * 地址 3: UPS 区域
     *
     * @returns {*}
     * @memberof Contact
     */
    address3_upszone?: any;

    /**
     * 地址 1: 名称
     *
     * @returns {*}
     * @memberof Contact
     */
    address1_name?: any;

    /**
     * 自动创建
     *
     * @returns {*}
     * @memberof Contact
     */
    autocreate?: any;

    /**
     * Back Office 客户
     *
     * @returns {*}
     * @memberof Contact
     */
    backofficecustomer?: any;

    /**
     * 地址 2: 经度
     *
     * @returns {*}
     * @memberof Contact
     */
    address2_longitude?: any;

    /**
     * 负责人类型
     *
     * @returns {*}
     * @memberof Contact
     */
    ownertype?: any;

    /**
     * 地址 1: 传真
     *
     * @returns {*}
     * @memberof Contact
     */
    address1_fax?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof Contact
     */
    exchangerate?: any;

    /**
     * 地址 1: 市/县
     *
     * @returns {*}
     * @memberof Contact
     */
    address1_city?: any;

    /**
     * 实体图像 ID
     *
     * @returns {*}
     * @memberof Contact
     */
    entityimageid?: any;

    /**
     * 地址 1: 电话 2
     *
     * @returns {*}
     * @memberof Contact
     */
    address1_telephone2?: any;

    /**
     * 地址 2
     *
     * @returns {*}
     * @memberof Contact
     */
    address2_composite?: any;

    /**
     * 导入序列号
     *
     * @returns {*}
     * @memberof Contact
     */
    importsequencenumber?: any;

    /**
     * 性别
     *
     * @returns {*}
     * @memberof Contact
     */
    gendercode?: any;

    /**
     * 年收入
     *
     * @returns {*}
     * @memberof Contact
     */
    annualincome?: any;

    /**
     * 预订
     *
     * @returns {*}
     * @memberof Contact
     */
    subscriptionid?: any;

    /**
     * 区域
     *
     * @returns {*}
     * @memberof Contact
     */
    territorycode?: any;

    /**
     * 创建记录的时间
     *
     * @returns {*}
     * @memberof Contact
     */
    overriddencreatedon?: any;

    /**
     * 地址 3: 国家/地区
     *
     * @returns {*}
     * @memberof Contact
     */
    address3_country?: any;

    /**
     * 不允许使用批量电子邮件
     *
     * @returns {*}
     * @memberof Contact
     */
    donotbulkemail?: any;

    /**
     * 地址 3: 电话 2
     *
     * @returns {*}
     * @memberof Contact
     */
    address3_telephone2?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Contact
     */
    ownerid?: any;

    /**
     * 外部用户标识符
     *
     * @returns {*}
     * @memberof Contact
     */
    externaluseridentifier?: any;

    /**
     * TeamsFollowed
     *
     * @returns {*}
     * @memberof Contact
     */
    teamsfollowed?: any;

    /**
     * 不允许使用电子邮件
     *
     * @returns {*}
     * @memberof Contact
     */
    donotemail?: any;

    /**
     * 纪念日
     *
     * @returns {*}
     * @memberof Contact
     */
    anniversary?: any;

    /**
     * 首选日
     *
     * @returns {*}
     * @memberof Contact
     */
    preferredappointmentdaycode?: any;

    /**
     * 中间名
     *
     * @returns {*}
     * @memberof Contact
     */
    middlename?: any;

    /**
     * 电子邮件地址 3
     *
     * @returns {*}
     * @memberof Contact
     */
    emailaddress3?: any;

    /**
     * 地址 2: 电话 2
     *
     * @returns {*}
     * @memberof Contact
     */
    address2_telephone2?: any;

    /**
     * 传真
     *
     * @returns {*}
     * @memberof Contact
     */
    fax?: any;

    /**
     * 移动电话
     *
     * @returns {*}
     * @memberof Contact
     */
    mobilephone?: any;

    /**
     * 住宅电话 2
     *
     * @returns {*}
     * @memberof Contact
     */
    home2?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof Contact
     */
    createman?: any;

    /**
     * 暂候时间(分钟)
     *
     * @returns {*}
     * @memberof Contact
     */
    onholdtime?: any;

    /**
     * 首选时间
     *
     * @returns {*}
     * @memberof Contact
     */
    preferredappointmenttimecode?: any;

    /**
     * 状态描述
     *
     * @returns {*}
     * @memberof Contact
     */
    statuscode?: any;

    /**
     * 身份证
     *
     * @returns {*}
     * @memberof Contact
     */
    governmentid?: any;

    /**
     * 地址 3: 电话 3
     *
     * @returns {*}
     * @memberof Contact
     */
    address3_telephone3?: any;

    /**
     * 商务电话 2
     *
     * @returns {*}
     * @memberof Contact
     */
    business2?: any;

    /**
     * 首选用户
     *
     * @returns {*}
     * @memberof Contact
     */
    preferredsystemuserid?: any;

    /**
     * 地址 1: UPS 区域
     *
     * @returns {*}
     * @memberof Contact
     */
    address1_upszone?: any;

    /**
     * 时效 60
     *
     * @returns {*}
     * @memberof Contact
     */
    aging60?: any;

    /**
     * 地址 3: 邮政信箱
     *
     * @returns {*}
     * @memberof Contact
     */
    address3_postofficebox?: any;

    /**
     * 父联系人
     *
     * @returns {*}
     * @memberof Contact
     */
    parentcontactname?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Contact
     */
    ownername?: any;

    /**
     * 地址 2: 市/县
     *
     * @returns {*}
     * @memberof Contact
     */
    address2_city?: any;

    /**
     * 流程
     *
     * @returns {*}
     * @memberof Contact
     */
    processid?: any;

    /**
     * 年收入(基础货币)
     *
     * @returns {*}
     * @memberof Contact
     */
    annualincome_base?: any;

    /**
     * 地址 3: ID
     *
     * @returns {*}
     * @memberof Contact
     */
    address3_addressid?: any;

    /**
     * 联系人
     *
     * @returns {*}
     * @memberof Contact
     */
    contactid?: any;

    /**
     * 时效 60 (基础货币)
     *
     * @returns {*}
     * @memberof Contact
     */
    aging60_base?: any;

    /**
     * 地址 3: 纬度
     *
     * @returns {*}
     * @memberof Contact
     */
    address3_latitude?: any;

    /**
     * 电话 3
     *
     * @returns {*}
     * @memberof Contact
     */
    telephone3?: any;

    /**
     * 地址 1: 主要联系人姓名
     *
     * @returns {*}
     * @memberof Contact
     */
    address1_primarycontactname?: any;

    /**
     * 地址 3: 传真
     *
     * @returns {*}
     * @memberof Contact
     */
    address3_fax?: any;

    /**
     * 首选联系方式
     *
     * @returns {*}
     * @memberof Contact
     */
    preferredcontactmethodcode?: any;

    /**
     * 地址 1: UTC 时差
     *
     * @returns {*}
     * @memberof Contact
     */
    address1_utcoffset?: any;

    /**
     * 发送市场营销资料
     *
     * @returns {*}
     * @memberof Contact
     */
    donotsendmm?: any;

    /**
     * 地址 2: 电话 3
     *
     * @returns {*}
     * @memberof Contact
     */
    address2_telephone3?: any;

    /**
     * 地址 2: 国家/地区
     *
     * @returns {*}
     * @memberof Contact
     */
    address2_country?: any;

    /**
     * 时效 30
     *
     * @returns {*}
     * @memberof Contact
     */
    aging30?: any;

    /**
     * 地址 2: 邮政信箱
     *
     * @returns {*}
     * @memberof Contact
     */
    address2_postofficebox?: any;

    /**
     * PreferredSystemUserName
     *
     * @returns {*}
     * @memberof Contact
     */
    preferredsystemusername?: any;

    /**
     * 商务电话
     *
     * @returns {*}
     * @memberof Contact
     */
    telephone1?: any;

    /**
     * 地址 3: 经度
     *
     * @returns {*}
     * @memberof Contact
     */
    address3_longitude?: any;

    /**
     * 父客户
     *
     * @returns {*}
     * @memberof Contact
     */
    parentcustomername?: any;

    /**
     * 上次参与市场活动的日期
     *
     * @returns {*}
     * @memberof Contact
     */
    lastusedincampaign?: any;

    /**
     * FTP 站点
     *
     * @returns {*}
     * @memberof Contact
     */
    ftpsiteurl?: any;

    /**
     * 时效 30 (基础货币)
     *
     * @returns {*}
     * @memberof Contact
     */
    aging30_base?: any;

    /**
     * 地址 2: 名称
     *
     * @returns {*}
     * @memberof Contact
     */
    address2_name?: any;

    /**
     * 后缀
     *
     * @returns {*}
     * @memberof Contact
     */
    suffix?: any;

    /**
     * 地址 1: 县
     *
     * @returns {*}
     * @memberof Contact
     */
    address1_county?: any;

    /**
     * 地址 2: 地址类型
     *
     * @returns {*}
     * @memberof Contact
     */
    address2_addresstypecode?: any;

    /**
     * 地址 1: 经度
     *
     * @returns {*}
     * @memberof Contact
     */
    address1_longitude?: any;

    /**
     * 地址 3: 街道 2
     *
     * @returns {*}
     * @memberof Contact
     */
    address3_line2?: any;

    /**
     * 地址 1: ID
     *
     * @returns {*}
     * @memberof Contact
     */
    address1_addressid?: any;

    /**
     * 地址 2: 街道 3
     *
     * @returns {*}
     * @memberof Contact
     */
    address2_line3?: any;

    /**
     * 地址 2: 纬度
     *
     * @returns {*}
     * @memberof Contact
     */
    address2_latitude?: any;

    /**
     * 地址 2: 街道 2
     *
     * @returns {*}
     * @memberof Contact
     */
    address2_line2?: any;

    /**
     * 地址 1: 送货方式
     *
     * @returns {*}
     * @memberof Contact
     */
    address1_shippingmethodcode?: any;

    /**
     * 地址 3: 货运条款
     *
     * @returns {*}
     * @memberof Contact
     */
    address3_freighttermscode?: any;

    /**
     * 主联系人
     *
     * @returns {*}
     * @memberof Contact
     */
    mastercontactname?: any;

    /**
     * 地址 1: 邮政信箱
     *
     * @returns {*}
     * @memberof Contact
     */
    address1_postofficebox?: any;

    /**
     * UTC 转换时区代码
     *
     * @returns {*}
     * @memberof Contact
     */
    utcconversiontimezonecode?: any;

    /**
     * 地址 3: 名称
     *
     * @returns {*}
     * @memberof Contact
     */
    address3_name?: any;

    /**
     * 地址 1: 纬度
     *
     * @returns {*}
     * @memberof Contact
     */
    address1_latitude?: any;

    /**
     * 子女数
     *
     * @returns {*}
     * @memberof Contact
     */
    numberofchildren?: any;

    /**
     * 地址 2: 主要联系人姓名
     *
     * @returns {*}
     * @memberof Contact
     */
    address2_primarycontactname?: any;

    /**
     * 地址 1: 邮政编码
     *
     * @returns {*}
     * @memberof Contact
     */
    address1_postalcode?: any;

    /**
     * 流程阶段
     *
     * @returns {*}
     * @memberof Contact
     */
    stageid?: any;

    /**
     * 地址 1: 国家/地区
     *
     * @returns {*}
     * @memberof Contact
     */
    address1_country?: any;

    /**
     * 电子邮件地址 2
     *
     * @returns {*}
     * @memberof Contact
     */
    emailaddress2?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof Contact
     */
    currencyname?: any;

    /**
     * 首选服务
     *
     * @returns {*}
     * @memberof Contact
     */
    preferredservicename?: any;

    /**
     * SLAName
     *
     * @returns {*}
     * @memberof Contact
     */
    slaname?: any;

    /**
     * 原始潜在顾客
     *
     * @returns {*}
     * @memberof Contact
     */
    originatingleadname?: any;

    /**
     * 首选设施/设备
     *
     * @returns {*}
     * @memberof Contact
     */
    preferredequipmentname?: any;

    /**
     * 客户
     *
     * @returns {*}
     * @memberof Contact
     */
    customername?: any;

    /**
     * 价目表
     *
     * @returns {*}
     * @memberof Contact
     */
    defaultpricelevelname?: any;

    /**
     * 客户
     *
     * @returns {*}
     * @memberof Contact
     */
    customerid?: any;

    /**
     * 价目表
     *
     * @returns {*}
     * @memberof Contact
     */
    defaultpricelevelid?: any;

    /**
     * 首选设施/设备
     *
     * @returns {*}
     * @memberof Contact
     */
    preferredequipmentid?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof Contact
     */
    transactioncurrencyid?: any;

    /**
     * SLA
     *
     * @returns {*}
     * @memberof Contact
     */
    slaid?: any;

    /**
     * 原始潜在顾客
     *
     * @returns {*}
     * @memberof Contact
     */
    originatingleadid?: any;

    /**
     * 首选服务
     *
     * @returns {*}
     * @memberof Contact
     */
    preferredserviceid?: any;
}
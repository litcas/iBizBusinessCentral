/**
 * 单位机构
 *
 * @export
 * @interface IBZOrganization
 */
export interface IBZOrganization {

    /**
     * 单位标识
     *
     * @returns {*}
     * @memberof IBZOrganization
     */
    orgid?: any;

    /**
     * 单位代码
     *
     * @returns {*}
     * @memberof IBZOrganization
     */
    orgcode?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof IBZOrganization
     */
    orgname?: any;

    /**
     * 上级单位
     *
     * @returns {*}
     * @memberof IBZOrganization
     */
    parentorgid?: any;

    /**
     * 单位简称
     *
     * @returns {*}
     * @memberof IBZOrganization
     */
    shortname?: any;

    /**
     * 单位级别
     *
     * @returns {*}
     * @memberof IBZOrganization
     */
    orglevel?: any;

    /**
     * 排序
     *
     * @returns {*}
     * @memberof IBZOrganization
     */
    showorder?: any;

    /**
     * 上级单位
     *
     * @returns {*}
     * @memberof IBZOrganization
     */
    parentorgname?: any;

    /**
     * 区属
     *
     * @returns {*}
     * @memberof IBZOrganization
     */
    domains?: any;

    /**
     * 逻辑有效
     *
     * @returns {*}
     * @memberof IBZOrganization
     */
    enable?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof IBZOrganization
     */
    createdate?: any;

    /**
     * 最后修改时间
     *
     * @returns {*}
     * @memberof IBZOrganization
     */
    updatedate?: any;
}
/**
 * 运营单位
 *
 * @export
 * @interface OperationUnit
 */
export interface OperationUnit {

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof OperationUnit
     */
    updatedate?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof OperationUnit
     */
    createman?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof OperationUnit
     */
    updateman?: any;

    /**
     * 运营单位标识
     *
     * @returns {*}
     * @memberof OperationUnit
     */
    operationunitid?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof OperationUnit
     */
    createdate?: any;

    /**
     * 运营单位名称
     *
     * @returns {*}
     * @memberof OperationUnit
     */
    operationunitname?: any;

    /**
     * 组织类型
     *
     * @returns {*}
     * @memberof OperationUnit
     */
    organizationtype?: any;

    /**
     * 排序号
     *
     * @returns {*}
     * @memberof OperationUnit
     */
    showorder?: any;

    /**
     * 组织层级
     *
     * @returns {*}
     * @memberof OperationUnit
     */
    orglevel?: any;

    /**
     * 组织简称
     *
     * @returns {*}
     * @memberof OperationUnit
     */
    shortname?: any;

    /**
     * 组织编码
     *
     * @returns {*}
     * @memberof OperationUnit
     */
    orgcode?: any;

    /**
     * 运营单位类型
     *
     * @returns {*}
     * @memberof OperationUnit
     */
    operationunittype?: any;
}
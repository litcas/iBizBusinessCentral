/**
 * 潜在客户对手
 *
 * @export
 * @interface LeadCompetitor
 */
export interface LeadCompetitor {

    /**
     * 关系标识
     *
     * @returns {*}
     * @memberof LeadCompetitor
     */
    relationshipsid?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof LeadCompetitor
     */
    createdate?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof LeadCompetitor
     */
    createman?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof LeadCompetitor
     */
    updatedate?: any;

    /**
     * 关系类型
     *
     * @returns {*}
     * @memberof LeadCompetitor
     */
    relationshipstype?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof LeadCompetitor
     */
    updateman?: any;

    /**
     * 关系名称
     *
     * @returns {*}
     * @memberof LeadCompetitor
     */
    relationshipsname?: any;

    /**
     * 潜在客户
     *
     * @returns {*}
     * @memberof LeadCompetitor
     */
    entityname?: any;

    /**
     * 对手
     *
     * @returns {*}
     * @memberof LeadCompetitor
     */
    entity2name?: any;

    /**
     * 潜在顾客
     *
     * @returns {*}
     * @memberof LeadCompetitor
     */
    entityid?: any;

    /**
     * 竞争对手
     *
     * @returns {*}
     * @memberof LeadCompetitor
     */
    entity2id?: any;
}
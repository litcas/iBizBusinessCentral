/**
 * 市场活动
 *
 * @export
 * @interface Campaign
 */
export interface Campaign {

    /**
     * 预期响应百分比
     *
     * @returns {*}
     * @memberof Campaign
     */
    expectedresponse?: any;

    /**
     * 活动类型
     *
     * @returns {*}
     * @memberof Campaign
     */
    typecode?: any;

    /**
     * 预算分配
     *
     * @returns {*}
     * @memberof Campaign
     */
    budgetedcost?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof Campaign
     */
    createdate?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Campaign
     */
    ownername?: any;

    /**
     * 市场活动总费用
     *
     * @returns {*}
     * @memberof Campaign
     */
    totalactualcost?: any;

    /**
     * UTC Conversion Time Zone Code
     *
     * @returns {*}
     * @memberof Campaign
     */
    utcconversiontimezonecode?: any;

    /**
     * Import Sequence Number
     *
     * @returns {*}
     * @memberof Campaign
     */
    importsequencenumber?: any;

    /**
     * 其他费用
     *
     * @returns {*}
     * @memberof Campaign
     */
    othercost?: any;

    /**
     * Time Zone Rule Version Number
     *
     * @returns {*}
     * @memberof Campaign
     */
    timezoneruleversionnumber?: any;

    /**
     * 活动名称
     *
     * @returns {*}
     * @memberof Campaign
     */
    campaignname?: any;

    /**
     * EntityImage_URL
     *
     * @returns {*}
     * @memberof Campaign
     */
    entityimage_url?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof Campaign
     */
    statecode?: any;

    /**
     * 模板
     *
     * @returns {*}
     * @memberof Campaign
     */
    template?: any;

    /**
     * 负责人类型
     *
     * @returns {*}
     * @memberof Campaign
     */
    ownertype?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof Campaign
     */
    exchangerate?: any;

    /**
     * 市场活动
     *
     * @returns {*}
     * @memberof Campaign
     */
    campaignid?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof Campaign
     */
    createman?: any;

    /**
     * 活动内容
     *
     * @returns {*}
     * @memberof Campaign
     */
    objective?: any;

    /**
     * 预算分配 (Base)
     *
     * @returns {*}
     * @memberof Campaign
     */
    budgetedcost_base?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof Campaign
     */
    updateman?: any;

    /**
     * 估计收入 (Base)
     *
     * @returns {*}
     * @memberof Campaign
     */
    expectedrevenue_base?: any;

    /**
     * EntityImage_Timestamp
     *
     * @returns {*}
     * @memberof Campaign
     */
    entityimage_timestamp?: any;

    /**
     * Traversed Path
     *
     * @returns {*}
     * @memberof Campaign
     */
    traversedpath?: any;

    /**
     * 其他费用 (Base)
     *
     * @returns {*}
     * @memberof Campaign
     */
    othercost_base?: any;

    /**
     * 拟定结束日期
     *
     * @returns {*}
     * @memberof Campaign
     */
    proposedend?: any;

    /**
     * Version Number
     *
     * @returns {*}
     * @memberof Campaign
     */
    versionnumber?: any;

    /**
     * 实体图像
     *
     * @returns {*}
     * @memberof Campaign
     */
    entityimage?: any;

    /**
     * 活动代码
     *
     * @returns {*}
     * @memberof Campaign
     */
    codename?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof Campaign
     */
    description?: any;

    /**
     * 实际结束日期
     *
     * @returns {*}
     * @memberof Campaign
     */
    actualend?: any;

    /**
     * 估计收入
     *
     * @returns {*}
     * @memberof Campaign
     */
    expectedrevenue?: any;

    /**
     * PriceListName
     *
     * @returns {*}
     * @memberof Campaign
     */
    pricelistname?: any;

    /**
     * EntityImageId
     *
     * @returns {*}
     * @memberof Campaign
     */
    entityimageid?: any;

    /**
     * Process Id
     *
     * @returns {*}
     * @memberof Campaign
     */
    processid?: any;

    /**
     * 实际开始日期
     *
     * @returns {*}
     * @memberof Campaign
     */
    actualstart?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof Campaign
     */
    updatedate?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Campaign
     */
    ownerid?: any;

    /**
     * 消息
     *
     * @returns {*}
     * @memberof Campaign
     */
    message?: any;

    /**
     * Stage Id
     *
     * @returns {*}
     * @memberof Campaign
     */
    stageid?: any;

    /**
     * 状态描述
     *
     * @returns {*}
     * @memberof Campaign
     */
    statuscode?: any;

    /**
     * Record Created On
     *
     * @returns {*}
     * @memberof Campaign
     */
    overriddencreatedon?: any;

    /**
     * 拟定日期
     *
     * @returns {*}
     * @memberof Campaign
     */
    proposedstart?: any;

    /**
     * 市场活动总费用 (Base)
     *
     * @returns {*}
     * @memberof Campaign
     */
    totalactualcost_base?: any;

    /**
     * Email Address
     *
     * @returns {*}
     * @memberof Campaign
     */
    emailaddress?: any;

    /**
     * 促销代码
     *
     * @returns {*}
     * @memberof Campaign
     */
    promotioncodename?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof Campaign
     */
    currencyname?: any;

    /**
     * 价目表
     *
     * @returns {*}
     * @memberof Campaign
     */
    pricelistid?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof Campaign
     */
    transactioncurrencyid?: any;
}
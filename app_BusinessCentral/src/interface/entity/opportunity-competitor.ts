/**
 * 商机对手
 *
 * @export
 * @interface OpportunityCompetitor
 */
export interface OpportunityCompetitor {

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof OpportunityCompetitor
     */
    updatedate?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof OpportunityCompetitor
     */
    createman?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof OpportunityCompetitor
     */
    createdate?: any;

    /**
     * 关系标识
     *
     * @returns {*}
     * @memberof OpportunityCompetitor
     */
    relationshipsid?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof OpportunityCompetitor
     */
    updateman?: any;

    /**
     * 关系类型
     *
     * @returns {*}
     * @memberof OpportunityCompetitor
     */
    relationshipstype?: any;

    /**
     * 关系名称
     *
     * @returns {*}
     * @memberof OpportunityCompetitor
     */
    relationshipsname?: any;

    /**
     * 潜在顾客
     *
     * @returns {*}
     * @memberof OpportunityCompetitor
     */
    originatingleadid?: any;

    /**
     * 商机
     *
     * @returns {*}
     * @memberof OpportunityCompetitor
     */
    entityname?: any;

    /**
     * 预计收入
     *
     * @returns {*}
     * @memberof OpportunityCompetitor
     */
    estimatedvalue?: any;

    /**
     * 潜在顾客
     *
     * @returns {*}
     * @memberof OpportunityCompetitor
     */
    originatingleadname?: any;

    /**
     * 预计结束日期
     *
     * @returns {*}
     * @memberof OpportunityCompetitor
     */
    estimatedclosedate?: any;

    /**
     * 实际收入
     *
     * @returns {*}
     * @memberof OpportunityCompetitor
     */
    actualvalue?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof OpportunityCompetitor
     */
    statecode?: any;

    /**
     * 竞争对手
     *
     * @returns {*}
     * @memberof OpportunityCompetitor
     */
    entity2name?: any;

    /**
     * 竞争对手
     *
     * @returns {*}
     * @memberof OpportunityCompetitor
     */
    entity2id?: any;

    /**
     * 商机
     *
     * @returns {*}
     * @memberof OpportunityCompetitor
     */
    entityid?: any;
}
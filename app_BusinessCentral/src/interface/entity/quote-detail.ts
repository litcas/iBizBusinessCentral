/**
 * 报价单产品
 *
 * @export
 * @interface QuoteDetail
 */
export interface QuoteDetail {

    /**
     * 金额 (Base)
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    baseamount_base?: any;

    /**
     * 负责人类型
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    ownertype?: any;

    /**
     * 送货地的传真号码
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    shipto_fax?: any;

    /**
     * 送至国家/地区
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    shipto_country?: any;

    /**
     * 送货地的名称
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    shipto_name?: any;

    /**
     * 送至街道 1
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    shipto_line1?: any;

    /**
     * 送货地址
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    willcall?: any;

    /**
     * 应收净额 (Base)
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    extendedamount_base?: any;

    /**
     * 明细项目编号
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    lineitemnumber?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    description?: any;

    /**
     * 送货地的联系人姓名
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    shipto_contactname?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    updatedate?: any;

    /**
     * 报价单产品
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    quotedetailid?: any;

    /**
     * Import Sequence Number
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    importsequencenumber?: any;

    /**
     * 送货地的电话号码
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    shipto_telephone?: any;

    /**
     * 零售折扣 (Base)
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    manualdiscountamount_base?: any;

    /**
     * 批发折扣 (Base)
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    volumediscountamount_base?: any;

    /**
     * 零售折扣
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    manualdiscountamount?: any;

    /**
     * 货运条款
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    shipto_freighttermscode?: any;

    /**
     * 送至市/县
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    shipto_city?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    createdate?: any;

    /**
     * 定价错误
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    pricingerrorcode?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    exchangerate?: any;

    /**
     * 送至省/市/自治区
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    shipto_stateorprovince?: any;

    /**
     * 送至街道 2
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    shipto_line2?: any;

    /**
     * Record Created On
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    overriddencreatedon?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    createman?: any;

    /**
     * 数量
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    quantity?: any;

    /**
     * 报价单状态
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    quotestatecode?: any;

    /**
     * 送至街道 3
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    shipto_line3?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    ownerid?: any;

    /**
     * 单价 (Base)
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    priceperunit_base?: any;

    /**
     * 销售员
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    salesrepname?: any;

    /**
     * 目录外产品
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    productdescription?: any;

    /**
     * 单价
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    priceperunit?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    updateman?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    ownername?: any;

    /**
     * 税 (Base)
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    tax_base?: any;

    /**
     * 自定义的价格
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    priceoverridden?: any;

    /**
     * UTC Conversion Time Zone Code
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    utcconversiontimezonecode?: any;

    /**
     * 父捆绑销售
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    parentbundleid?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    quotedetailname?: any;

    /**
     * 选择产品
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    productoverridden?: any;

    /**
     * Time Zone Rule Version Number
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    timezoneruleversionnumber?: any;

    /**
     * 送货地的邮政编码
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    shipto_postalcode?: any;

    /**
     * 送货地址 ID
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    shipto_addressid?: any;

    /**
     * 属性配置
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    propertyconfigurationstatus?: any;

    /**
     * 税
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    tax?: any;

    /**
     * 要求交付日期
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    requestdeliveryby?: any;

    /**
     * 产品类型
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    producttypecode?: any;

    /**
     * 金额
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    baseamount?: any;

    /**
     * 应收净额
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    extendedamount?: any;

    /**
     * 批发折扣
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    volumediscountamount?: any;

    /**
     * SkipPriceCalculation
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    skippricecalculation?: any;

    /**
     * 捆绑销售项关联
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    productassociationid?: any;

    /**
     * Version Number
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    versionnumber?: any;

    /**
     * 销售员
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    salesrepid?: any;

    /**
     * 序号
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    sequencenumber?: any;

    /**
     * 产品名称
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    productname?: any;

    /**
     * Parent bundle product
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    parentbundleidref?: any;

    /**
     * 报价单
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    quoteid?: any;

    /**
     * 现有产品
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    productid?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    transactioncurrencyid?: any;

    /**
     * 计价单位
     *
     * @returns {*}
     * @memberof QuoteDetail
     */
    uomid?: any;
}
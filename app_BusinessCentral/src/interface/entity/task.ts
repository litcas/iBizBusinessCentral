/**
 * 任务
 *
 * @export
 * @interface Task
 */
export interface Task {

    /**
     * 由工作流创建
     *
     * @returns {*}
     * @memberof Task
     */
    workflowcreated?: any;

    /**
     * 关于
     *
     * @returns {*}
     * @memberof Task
     */
    regardingobjectname?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof Task
     */
    createdate?: any;

    /**
     * 实际结束时间
     *
     * @returns {*}
     * @memberof Task
     */
    actualend?: any;

    /**
     * 活动状态
     *
     * @returns {*}
     * @memberof Task
     */
    statecode?: any;

    /**
     * 优先级
     *
     * @returns {*}
     * @memberof Task
     */
    prioritycode?: any;

    /**
     * 附加参数
     *
     * @returns {*}
     * @memberof Task
     */
    activityadditionalparams?: any;

    /**
     * 预订
     *
     * @returns {*}
     * @memberof Task
     */
    subscriptionid?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof Task
     */
    updateman?: any;

    /**
     * 计划持续时间
     *
     * @returns {*}
     * @memberof Task
     */
    scheduleddurationminutes?: any;

    /**
     * 排序日期
     *
     * @returns {*}
     * @memberof Task
     */
    sortdate?: any;

    /**
     * 活动类型
     *
     * @returns {*}
     * @memberof Task
     */
    activitytypecode?: any;

    /**
     * RegardingObjectTypeCode
     *
     * @returns {*}
     * @memberof Task
     */
    regardingobjecttypecode?: any;

    /**
     * 上一暂候时间
     *
     * @returns {*}
     * @memberof Task
     */
    lastonholdtime?: any;

    /**
     * 开始日期
     *
     * @returns {*}
     * @memberof Task
     */
    scheduledstart?: any;

    /**
     * 版本号
     *
     * @returns {*}
     * @memberof Task
     */
    versionnumber?: any;

    /**
     * 创建记录的时间
     *
     * @returns {*}
     * @memberof Task
     */
    overriddencreatedon?: any;

    /**
     * 暂候时间(分钟)
     *
     * @returns {*}
     * @memberof Task
     */
    onholdtime?: any;

    /**
     * 任务​​
     *
     * @returns {*}
     * @memberof Task
     */
    activityid?: any;

    /**
     * 主题
     *
     * @returns {*}
     * @memberof Task
     */
    subject?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof Task
     */
    updatedate?: any;

    /**
     * 遍历的路径
     *
     * @returns {*}
     * @memberof Task
     */
    traversedpath?: any;

    /**
     * 是定期活动
     *
     * @returns {*}
     * @memberof Task
     */
    regularactivity?: any;

    /**
     * UTC 转换时区代码
     *
     * @returns {*}
     * @memberof Task
     */
    utcconversiontimezonecode?: any;

    /**
     * 已分派任务的唯一 ID
     *
     * @returns {*}
     * @memberof Task
     */
    crmtaskassigneduniqueid?: any;

    /**
     * SLAName
     *
     * @returns {*}
     * @memberof Task
     */
    slaname?: any;

    /**
     * 实际开始时间
     *
     * @returns {*}
     * @memberof Task
     */
    actualstart?: any;

    /**
     * 子类别
     *
     * @returns {*}
     * @memberof Task
     */
    subcategory?: any;

    /**
     * 状态描述
     *
     * @returns {*}
     * @memberof Task
     */
    statuscode?: any;

    /**
     * 完成百分比
     *
     * @returns {*}
     * @memberof Task
     */
    percentcomplete?: any;

    /**
     * 关于
     *
     * @returns {*}
     * @memberof Task
     */
    regardingobjectid?: any;

    /**
     * 负责人类型
     *
     * @returns {*}
     * @memberof Task
     */
    ownertype?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof Task
     */
    description?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof Task
     */
    createman?: any;

    /**
     * 已记帐
     *
     * @returns {*}
     * @memberof Task
     */
    billed?: any;

    /**
     * 时区规则版本号
     *
     * @returns {*}
     * @memberof Task
     */
    timezoneruleversionnumber?: any;

    /**
     * 截止日期
     *
     * @returns {*}
     * @memberof Task
     */
    scheduledend?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Task
     */
    ownername?: any;

    /**
     * 流程
     *
     * @returns {*}
     * @memberof Task
     */
    processid?: any;

    /**
     * 流程阶段
     *
     * @returns {*}
     * @memberof Task
     */
    stageid?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Task
     */
    ownerid?: any;

    /**
     * 持续时间
     *
     * @returns {*}
     * @memberof Task
     */
    actualdurationminutes?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof Task
     */
    exchangerate?: any;

    /**
     * 类别
     *
     * @returns {*}
     * @memberof Task
     */
    category?: any;

    /**
     * 导入序列号
     *
     * @returns {*}
     * @memberof Task
     */
    importsequencenumber?: any;

    /**
     * SLA
     *
     * @returns {*}
     * @memberof Task
     */
    slaid?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof Task
     */
    transactioncurrencyid?: any;

    /**
     * 服务
     *
     * @returns {*}
     * @memberof Task
     */
    serviceid?: any;
}
/**
 * 电子邮件
 *
 * @export
 * @interface Email
 */
export interface Email {

    /**
     * MIME 类型
     *
     * @returns {*}
     * @memberof Email
     */
    mimetype?: any;

    /**
     * 收件人
     *
     * @returns {*}
     * @memberof Email
     */
    to?: any;

    /**
     * 上次打开时间
     *
     * @returns {*}
     * @memberof Email
     */
    lastopenedtime?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof Email
     */
    createman?: any;

    /**
     * 截止日期
     *
     * @returns {*}
     * @memberof Email
     */
    scheduledend?: any;

    /**
     * 关于
     *
     * @returns {*}
     * @memberof Email
     */
    regardingobjectid?: any;

    /**
     * 打开计数
     *
     * @returns {*}
     * @memberof Email
     */
    opencount?: any;

    /**
     * 负责人类型
     *
     * @returns {*}
     * @memberof Email
     */
    ownertype?: any;

    /**
     * 电子邮件发件人帐户名称
     *
     * @returns {*}
     * @memberof Email
     */
    sendersaccountname?: any;

    /**
     * 电子邮件提醒类型
     *
     * @returns {*}
     * @memberof Email
     */
    emailremindertype?: any;

    /**
     * 相关性方法
     *
     * @returns {*}
     * @memberof Email
     */
    correlationmethod?: any;

    /**
     * 消息 ID 重复检测
     *
     * @returns {*}
     * @memberof Email
     */
    messageiddupcheck?: any;

    /**
     * 发送日期
     *
     * @returns {*}
     * @memberof Email
     */
    senton?: any;

    /**
     * 链接单击计数
     *
     * @returns {*}
     * @memberof Email
     */
    linksclickedcount?: any;

    /**
     * 压缩
     *
     * @returns {*}
     * @memberof Email
     */
    compressed?: any;

    /**
     * 活动状态
     *
     * @returns {*}
     * @memberof Email
     */
    statecode?: any;

    /**
     * 答复消息
     *
     * @returns {*}
     * @memberof Email
     */
    inreplyto?: any;

    /**
     * 电子邮件提醒状态
     *
     * @returns {*}
     * @memberof Email
     */
    emailreminderstatus?: any;

    /**
     * 安全说明
     *
     * @returns {*}
     * @memberof Email
     */
    safedescription?: any;

    /**
     * 遍历的路径
     *
     * @returns {*}
     * @memberof Email
     */
    traversedpath?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof Email
     */
    description?: any;

    /**
     * 提醒操作卡 ID。
     *
     * @returns {*}
     * @memberof Email
     */
    reminderactioncardid?: any;

    /**
     * 版本号
     *
     * @returns {*}
     * @memberof Email
     */
    versionnumber?: any;

    /**
     * 暂候时间(分钟)
     *
     * @returns {*}
     * @memberof Email
     */
    onholdtime?: any;

    /**
     * 子类别
     *
     * @returns {*}
     * @memberof Email
     */
    subcategory?: any;

    /**
     * 后续活动
     *
     * @returns {*}
     * @memberof Email
     */
    emailfollowed?: any;

    /**
     * 跟踪
     *
     * @returns {*}
     * @memberof Email
     */
    followemailuserpreference?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Email
     */
    ownername?: any;

    /**
     * 电子邮件
     *
     * @returns {*}
     * @memberof Email
     */
    activityid?: any;

    /**
     * 电子邮件跟踪 ID
     *
     * @returns {*}
     * @memberof Email
     */
    emailtrackingid?: any;

    /**
     * 关于
     *
     * @returns {*}
     * @memberof Email
     */
    regardingobjectname?: any;

    /**
     * 开始日期
     *
     * @returns {*}
     * @memberof Email
     */
    scheduledstart?: any;

    /**
     * 电子邮件提醒文本
     *
     * @returns {*}
     * @memberof Email
     */
    emailremindertext?: any;

    /**
     * 跟踪令牌
     *
     * @returns {*}
     * @memberof Email
     */
    trackingtoken?: any;

    /**
     * 活动类型
     *
     * @returns {*}
     * @memberof Email
     */
    activitytypecode?: any;

    /**
     * 尝试传送的次数
     *
     * @returns {*}
     * @memberof Email
     */
    deliveryattempts?: any;

    /**
     * 发件人帐户类型
     *
     * @returns {*}
     * @memberof Email
     */
    sendersaccountobjecttypecode?: any;

    /**
     * 需要“送达”回执
     *
     * @returns {*}
     * @memberof Email
     */
    deliveryreceiptrequested?: any;

    /**
     * 电子邮件提醒到期时间
     *
     * @returns {*}
     * @memberof Email
     */
    emailreminderexpirytime?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof Email
     */
    exchangerate?: any;

    /**
     * 创建记录的时间
     *
     * @returns {*}
     * @memberof Email
     */
    overriddencreatedon?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Email
     */
    ownerid?: any;

    /**
     * 已记帐
     *
     * @returns {*}
     * @memberof Email
     */
    billed?: any;

    /**
     * 类别
     *
     * @returns {*}
     * @memberof Email
     */
    category?: any;

    /**
     * 对话索引(哈希)
     *
     * @returns {*}
     * @memberof Email
     */
    baseconversationindexhash?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof Email
     */
    createdate?: any;

    /**
     * 流程
     *
     * @returns {*}
     * @memberof Email
     */
    processid?: any;

    /**
     * 对话索引
     *
     * @returns {*}
     * @memberof Email
     */
    conversationindex?: any;

    /**
     * 流程阶段
     *
     * @returns {*}
     * @memberof Email
     */
    stageid?: any;

    /**
     * 延迟电子邮件处理，直至
     *
     * @returns {*}
     * @memberof Email
     */
    postponeemailprocessinguntil?: any;

    /**
     * 方向
     *
     * @returns {*}
     * @memberof Email
     */
    directioncode?: any;

    /**
     * 通知
     *
     * @returns {*}
     * @memberof Email
     */
    notifications?: any;

    /**
     * 电子邮件发件人类型
     *
     * @returns {*}
     * @memberof Email
     */
    emailsenderobjecttypecode?: any;

    /**
     * 排序日期
     *
     * @returns {*}
     * @memberof Email
     */
    sortdate?: any;

    /**
     * 消息 ID
     *
     * @returns {*}
     * @memberof Email
     */
    messageid?: any;

    /**
     * 附加参数
     *
     * @returns {*}
     * @memberof Email
     */
    activityadditionalparams?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof Email
     */
    updatedate?: any;

    /**
     * 需要“已读”回执
     *
     * @returns {*}
     * @memberof Email
     */
    readreceiptrequested?: any;

    /**
     * 由工作流创建
     *
     * @returns {*}
     * @memberof Email
     */
    workflowcreated?: any;

    /**
     * IsUnsafe
     *
     * @returns {*}
     * @memberof Email
     */
    unsafe?: any;

    /**
     * RegardingObjectTypeCode
     *
     * @returns {*}
     * @memberof Email
     */
    regardingobjecttypecode?: any;

    /**
     * 优先级
     *
     * @returns {*}
     * @memberof Email
     */
    prioritycode?: any;

    /**
     * 稍后发送
     *
     * @returns {*}
     * @memberof Email
     */
    delayedemailsendtime?: any;

    /**
     * 发件人
     *
     * @returns {*}
     * @memberof Email
     */
    from?: any;

    /**
     * 电子邮件发件人名称
     *
     * @returns {*}
     * @memberof Email
     */
    emailsendername?: any;

    /**
     * 密件抄送
     *
     * @returns {*}
     * @memberof Email
     */
    bcc?: any;

    /**
     * 实际开始时间
     *
     * @returns {*}
     * @memberof Email
     */
    actualstart?: any;

    /**
     * 实际结束时间
     *
     * @returns {*}
     * @memberof Email
     */
    actualend?: any;

    /**
     * SLAName
     *
     * @returns {*}
     * @memberof Email
     */
    slaname?: any;

    /**
     * 对话跟踪 ID
     *
     * @returns {*}
     * @memberof Email
     */
    conversationtrackingid?: any;

    /**
     * 发件人
     *
     * @returns {*}
     * @memberof Email
     */
    sender?: any;

    /**
     * 上一暂候时间
     *
     * @returns {*}
     * @memberof Email
     */
    lastonholdtime?: any;

    /**
     * 提交者
     *
     * @returns {*}
     * @memberof Email
     */
    submittedby?: any;

    /**
     * 至收件人
     *
     * @returns {*}
     * @memberof Email
     */
    torecipients?: any;

    /**
     * 传递优先级
     *
     * @returns {*}
     * @memberof Email
     */
    deliveryprioritycode?: any;

    /**
     * 附件计数
     *
     * @returns {*}
     * @memberof Email
     */
    attachmentcount?: any;

    /**
     * 附件打开计数
     *
     * @returns {*}
     * @memberof Email
     */
    attachmentopencount?: any;

    /**
     * 计划持续时间
     *
     * @returns {*}
     * @memberof Email
     */
    scheduleddurationminutes?: any;

    /**
     * 导入序列号
     *
     * @returns {*}
     * @memberof Email
     */
    importsequencenumber?: any;

    /**
     * 时区规则版本号
     *
     * @returns {*}
     * @memberof Email
     */
    timezoneruleversionnumber?: any;

    /**
     * 回复计数
     *
     * @returns {*}
     * @memberof Email
     */
    replycount?: any;

    /**
     * 是定期活动
     *
     * @returns {*}
     * @memberof Email
     */
    regularactivity?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof Email
     */
    updateman?: any;

    /**
     * 抄送
     *
     * @returns {*}
     * @memberof Email
     */
    cc?: any;

    /**
     * 状态描述
     *
     * @returns {*}
     * @memberof Email
     */
    statuscode?: any;

    /**
     * 主题
     *
     * @returns {*}
     * @memberof Email
     */
    subject?: any;

    /**
     * UTC 转换时区代码
     *
     * @returns {*}
     * @memberof Email
     */
    utcconversiontimezonecode?: any;

    /**
     * 已设置提醒
     *
     * @returns {*}
     * @memberof Email
     */
    emailreminderset?: any;

    /**
     * 持续时间
     *
     * @returns {*}
     * @memberof Email
     */
    actualdurationminutes?: any;

    /**
     * 父活动 ID
     *
     * @returns {*}
     * @memberof Email
     */
    parentactivityid?: any;

    /**
     * 服务
     *
     * @returns {*}
     * @memberof Email
     */
    serviceid?: any;

    /**
     * 使用的模板的 ID。
     *
     * @returns {*}
     * @memberof Email
     */
    templateid?: any;

    /**
     * SLA
     *
     * @returns {*}
     * @memberof Email
     */
    slaid?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof Email
     */
    transactioncurrencyid?: any;
}
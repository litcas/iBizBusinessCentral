/**
 * 知识文章事件
 *
 * @export
 * @interface KnowledgeArticleIncident
 */
export interface KnowledgeArticleIncident {

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof KnowledgeArticleIncident
     */
    ownername?: any;

    /**
     * 负责人类型
     *
     * @returns {*}
     * @memberof KnowledgeArticleIncident
     */
    ownertype?: any;

    /**
     * Record Created On
     *
     * @returns {*}
     * @memberof KnowledgeArticleIncident
     */
    overriddencreatedon?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof KnowledgeArticleIncident
     */
    createman?: any;

    /**
     * 知识使用情况
     *
     * @returns {*}
     * @memberof KnowledgeArticleIncident
     */
    knowledgeusage?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof KnowledgeArticleIncident
     */
    updateman?: any;

    /**
     * Time Zone Rule Version Number
     *
     * @returns {*}
     * @memberof KnowledgeArticleIncident
     */
    timezoneruleversionnumber?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof KnowledgeArticleIncident
     */
    exchangerate?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof KnowledgeArticleIncident
     */
    statecode?: any;

    /**
     * KnowledgeArticle 事件
     *
     * @returns {*}
     * @memberof KnowledgeArticleIncident
     */
    knowledgearticleincidentid?: any;

    /**
     * Version Number
     *
     * @returns {*}
     * @memberof KnowledgeArticleIncident
     */
    versionnumber?: any;

    /**
     * 状态描述
     *
     * @returns {*}
     * @memberof KnowledgeArticleIncident
     */
    statuscode?: any;

    /**
     * UTC Conversion Time Zone Code
     *
     * @returns {*}
     * @memberof KnowledgeArticleIncident
     */
    utcconversiontimezonecode?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof KnowledgeArticleIncident
     */
    ownerid?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof KnowledgeArticleIncident
     */
    updatedate?: any;

    /**
     * 已发送给客户
     *
     * @returns {*}
     * @memberof KnowledgeArticleIncident
     */
    senttocustomer?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof KnowledgeArticleIncident
     */
    createdate?: any;

    /**
     * Import Sequence Number
     *
     * @returns {*}
     * @memberof KnowledgeArticleIncident
     */
    importsequencenumber?: any;

    /**
     * 知识文章
     *
     * @returns {*}
     * @memberof KnowledgeArticleIncident
     */
    knowledgearticlename?: any;

    /**
     * 服务案例
     *
     * @returns {*}
     * @memberof KnowledgeArticleIncident
     */
    incidentname?: any;

    /**
     * 事件
     *
     * @returns {*}
     * @memberof KnowledgeArticleIncident
     */
    incidentid?: any;

    /**
     * 知识文章
     *
     * @returns {*}
     * @memberof KnowledgeArticleIncident
     */
    knowledgearticleid?: any;
}
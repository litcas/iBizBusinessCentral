/**
 * 订单产品
 *
 * @export
 * @interface SalesOrderDetail
 */
export interface SalesOrderDetail {

    /**
     * 送货地的名称
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    shipto_name?: any;

    /**
     * 税
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    tax?: any;

    /**
     * 送至省/市/自治区
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    shipto_stateorprovince?: any;

    /**
     * 送货地址 ID
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    shipto_addressid?: any;

    /**
     * 产品类型
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    producttypecode?: any;

    /**
     * 目录外产品
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    productdescription?: any;

    /**
     * 批发折扣
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    volumediscountamount?: any;

    /**
     * 订单产品
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    salesorderdetailid?: any;

    /**
     * 金额 (Base)
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    baseamount_base?: any;

    /**
     * 应收净额 (Base)
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    extendedamount_base?: any;

    /**
     * 送货地的电话号码
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    shipto_telephone?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    updateman?: any;

    /**
     * 税 (Base)
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    tax_base?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    description?: any;

    /**
     * SkipPriceCalculation
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    skippricecalculation?: any;

    /**
     * 延期交货数量
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    quantitybackordered?: any;

    /**
     * Version Number
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    versionnumber?: any;

    /**
     * 订单状态
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    salesorderstatecode?: any;

    /**
     * 数量
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    quantity?: any;

    /**
     * 选择产品
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    productoverridden?: any;

    /**
     * 送至国家/地区
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    shipto_country?: any;

    /**
     * 零售折扣
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    manualdiscountamount?: any;

    /**
     * 销售员
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    salesrepid?: any;

    /**
     * 父捆绑销售
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    parentbundleid?: any;

    /**
     * 要求交付日期
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    requestdeliveryby?: any;

    /**
     * 属性配置
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    propertyconfigurationstatus?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    createman?: any;

    /**
     * Import Sequence Number
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    importsequencenumber?: any;

    /**
     * 已复制
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    copied?: any;

    /**
     * 送至街道 3
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    shipto_line3?: any;

    /**
     * Time Zone Rule Version Number
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    timezoneruleversionnumber?: any;

    /**
     * 送货地的联系人姓名
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    shipto_contactname?: any;

    /**
     * UTC Conversion Time Zone Code
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    utcconversiontimezonecode?: any;

    /**
     * 金额
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    baseamount?: any;

    /**
     * 送至市/县
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    shipto_city?: any;

    /**
     * 货运条款
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    shipto_freighttermscode?: any;

    /**
     * 已取消数量
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    quantitycancelled?: any;

    /**
     * 定价错误
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    pricingerrorcode?: any;

    /**
     * 订单价格已锁定
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    salesorderispricelocked?: any;

    /**
     * 批发折扣 (Base)
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    volumediscountamount_base?: any;

    /**
     * 送货地址
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    willcall?: any;

    /**
     * 已发货数量
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    quantityshipped?: any;

    /**
     * 负责人类型
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    ownertype?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    updatedate?: any;

    /**
     * 应收净额
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    extendedamount?: any;

    /**
     * Record Created On
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    overriddencreatedon?: any;

    /**
     * 送货地的传真号码
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    shipto_fax?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    exchangerate?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    salesorderdetailname?: any;

    /**
     * 零售折扣 (Base)
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    manualdiscountamount_base?: any;

    /**
     * 送货地的邮政编码
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    shipto_postalcode?: any;

    /**
     * 定价
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    priceoverridden?: any;

    /**
     * 销售员
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    salesrepname?: any;

    /**
     * 序号
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    sequencenumber?: any;

    /**
     * 单价 (Base)
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    priceperunit_base?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    ownername?: any;

    /**
     * 单价
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    priceperunit?: any;

    /**
     * 明细项目编号
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    lineitemnumber?: any;

    /**
     * 捆绑销售项关联
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    productassociationid?: any;

    /**
     * 送至街道 1
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    shipto_line1?: any;

    /**
     * 送至街道 2
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    shipto_line2?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    ownerid?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    createdate?: any;

    /**
     * 产品名称
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    productname?: any;

    /**
     * 计价单位
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    uomname?: any;

    /**
     * 货币名称
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    currencyname?: any;

    /**
     * Quote Product Id
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    quotedetailid?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    transactioncurrencyid?: any;

    /**
     * 现有产品
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    productid?: any;

    /**
     * 订单
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    salesorderid?: any;

    /**
     * Parent bundle product
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    parentbundleidref?: any;

    /**
     * 计价单位
     *
     * @returns {*}
     * @memberof SalesOrderDetail
     */
    uomid?: any;
}
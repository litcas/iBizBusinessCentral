/**
 * 案例
 *
 * @export
 * @interface Incident
 */
export interface Incident {

    /**
     * 上一暂候时间
     *
     * @returns {*}
     * @memberof Incident
     */
    lastonholdtime?: any;

    /**
     * Version Number
     *
     * @returns {*}
     * @memberof Incident
     */
    versionnumber?: any;

    /**
     * 第一个响应 SLA 状态
     *
     * @returns {*}
     * @memberof Incident
     */
    firstresponseslastatus?: any;

    /**
     * 案例阶段
     *
     * @returns {*}
     * @memberof Incident
     */
    incidentstagecode?: any;

    /**
     * 影响分数
     *
     * @returns {*}
     * @memberof Incident
     */
    influencescore?: any;

    /**
     * 社交个人资料
     *
     * @returns {*}
     * @memberof Incident
     */
    socialprofileid?: any;

    /**
     * 案例号
     *
     * @returns {*}
     * @memberof Incident
     */
    ticketnumber?: any;

    /**
     * 仅供内部使用
     *
     * @returns {*}
     * @memberof Incident
     */
    merged?: any;

    /**
     * 序列号
     *
     * @returns {*}
     * @memberof Incident
     */
    productserialnumber?: any;

    /**
     * 客户
     *
     * @returns {*}
     * @memberof Incident
     */
    accountname?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof Incident
     */
    description?: any;

    /**
     * 已升级
     *
     * @returns {*}
     * @memberof Incident
     */
    escalated?: any;

    /**
     * Stage Id
     *
     * @returns {*}
     * @memberof Incident
     */
    stageid?: any;

    /**
     * 案例类型
     *
     * @returns {*}
     * @memberof Incident
     */
    casetypecode?: any;

    /**
     * Email Address
     *
     * @returns {*}
     * @memberof Incident
     */
    emailaddress?: any;

    /**
     * 检查电子邮件
     *
     * @returns {*}
     * @memberof Incident
     */
    checkemail?: any;

    /**
     * 严重性
     *
     * @returns {*}
     * @memberof Incident
     */
    severitycode?: any;

    /**
     * 呈报日期
     *
     * @returns {*}
     * @memberof Incident
     */
    escalatedon?: any;

    /**
     * UTC Conversion Time Zone Code
     *
     * @returns {*}
     * @memberof Incident
     */
    utcconversiontimezonecode?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Incident
     */
    ownerid?: any;

    /**
     * 记帐服务计价单位
     *
     * @returns {*}
     * @memberof Incident
     */
    billedserviceunits?: any;

    /**
     * 优先级
     *
     * @returns {*}
     * @memberof Incident
     */
    prioritycode?: any;

    /**
     * 实体图像
     *
     * @returns {*}
     * @memberof Incident
     */
    entityimage?: any;

    /**
     * 暂候时间(分钟)
     *
     * @returns {*}
     * @memberof Incident
     */
    onholdtime?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof Incident
     */
    createman?: any;

    /**
     * 知识库文章
     *
     * @returns {*}
     * @memberof Incident
     */
    kbarticleid?: any;

    /**
     * 服务级别
     *
     * @returns {*}
     * @memberof Incident
     */
    contractservicelevelcode?: any;

    /**
     * 负责人类型
     *
     * @returns {*}
     * @memberof Incident
     */
    ownertype?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Incident
     */
    ownername?: any;

    /**
     * EntityImage_URL
     *
     * @returns {*}
     * @memberof Incident
     */
    entityimage_url?: any;

    /**
     * 已联系客户
     *
     * @returns {*}
     * @memberof Incident
     */
    customercontacted?: any;

    /**
     * Process Id
     *
     * @returns {*}
     * @memberof Incident
     */
    processid?: any;

    /**
     * Time Zone Rule Version Number
     *
     * @returns {*}
     * @memberof Incident
     */
    timezoneruleversionnumber?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof Incident
     */
    createdate?: any;

    /**
     * 减少
     *
     * @returns {*}
     * @memberof Incident
     */
    decrementing?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof Incident
     */
    updateman?: any;

    /**
     * 状态描述
     *
     * @returns {*}
     * @memberof Incident
     */
    statuscode?: any;

    /**
     * 解决方
     *
     * @returns {*}
     * @memberof Incident
     */
    resolveby?: any;

    /**
     * 第一个响应者
     *
     * @returns {*}
     * @memberof Incident
     */
    responseby?: any;

    /**
     * EntityImageId
     *
     * @returns {*}
     * @memberof Incident
     */
    entityimageid?: any;

    /**
     * 已创建跟进任务
     *
     * @returns {*}
     * @memberof Incident
     */
    followuptaskcreated?: any;

    /**
     * 第一个响应已发送
     *
     * @returns {*}
     * @memberof Incident
     */
    firstresponsesent?: any;

    /**
     * 跟进工作截止日期
     *
     * @returns {*}
     * @memberof Incident
     */
    followupby?: any;

    /**
     * 案例
     *
     * @returns {*}
     * @memberof Incident
     */
    incidentid?: any;

    /**
     * 服务阶段
     *
     * @returns {*}
     * @memberof Incident
     */
    servicestage?: any;

    /**
     * Traversed Path
     *
     * @returns {*}
     * @memberof Incident
     */
    traversedpath?: any;

    /**
     * 子案例
     *
     * @returns {*}
     * @memberof Incident
     */
    numberofchildincidents?: any;

    /**
     * 已阻止的配置文件
     *
     * @returns {*}
     * @memberof Incident
     */
    blockedprofile?: any;

    /**
     * Record Created On
     *
     * @returns {*}
     * @memberof Incident
     */
    overriddencreatedon?: any;

    /**
     * 实际服务计价单位
     *
     * @returns {*}
     * @memberof Incident
     */
    actualserviceunits?: any;

    /**
     * 接收为
     *
     * @returns {*}
     * @memberof Incident
     */
    messagetypecode?: any;

    /**
     * 联系人
     *
     * @returns {*}
     * @memberof Incident
     */
    contactname?: any;

    /**
     * 传递案例
     *
     * @returns {*}
     * @memberof Incident
     */
    routecase?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof Incident
     */
    statecode?: any;

    /**
     * Import Sequence Number
     *
     * @returns {*}
     * @memberof Incident
     */
    importsequencenumber?: any;

    /**
     * 解决方 SLA 状态
     *
     * @returns {*}
     * @memberof Incident
     */
    resolvebyslastatus?: any;

    /**
     * 起源
     *
     * @returns {*}
     * @memberof Incident
     */
    caseorigincode?: any;

    /**
     * 活动完成
     *
     * @returns {*}
     * @memberof Incident
     */
    activitiescomplete?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof Incident
     */
    updatedate?: any;

    /**
     * 减少权利条件
     *
     * @returns {*}
     * @memberof Incident
     */
    decremententitlementterm?: any;

    /**
     * 满意度
     *
     * @returns {*}
     * @memberof Incident
     */
    customersatisfactioncode?: any;

    /**
     * 情绪值
     *
     * @returns {*}
     * @memberof Incident
     */
    sentimentvalue?: any;

    /**
     * EntityImage_Timestamp
     *
     * @returns {*}
     * @memberof Incident
     */
    entityimage_timestamp?: any;

    /**
     * 案例标题
     *
     * @returns {*}
     * @memberof Incident
     */
    title?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof Incident
     */
    exchangerate?: any;

    /**
     * 现有服务案例
     *
     * @returns {*}
     * @memberof Incident
     */
    existingcasename?: any;

    /**
     * 主题
     *
     * @returns {*}
     * @memberof Incident
     */
    subjectname?: any;

    /**
     * 合同子项
     *
     * @returns {*}
     * @memberof Incident
     */
    contractdetailname?: any;

    /**
     * 上级案例
     *
     * @returns {*}
     * @memberof Incident
     */
    parentcasename?: any;

    /**
     * 权利
     *
     * @returns {*}
     * @memberof Incident
     */
    entitlementname?: any;

    /**
     * 主案例
     *
     * @returns {*}
     * @memberof Incident
     */
    mastername?: any;

    /**
     * 合同
     *
     * @returns {*}
     * @memberof Incident
     */
    contractname?: any;

    /**
     * 客户
     *
     * @returns {*}
     * @memberof Incident
     */
    customername?: any;

    /**
     * 客户类型
     *
     * @returns {*}
     * @memberof Incident
     */
    customertype?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof Incident
     */
    currencyname?: any;

    /**
     * 产品
     *
     * @returns {*}
     * @memberof Incident
     */
    productname?: any;

    /**
     * SLA
     *
     * @returns {*}
     * @memberof Incident
     */
    slaname?: any;

    /**
     * 第一个响应者（按KPI）
     *
     * @returns {*}
     * @memberof Incident
     */
    firstresponsebykpiname?: any;

    /**
     * 解决方KPI
     *
     * @returns {*}
     * @memberof Incident
     */
    resolvebykpiname?: any;

    /**
     * 联系人
     *
     * @returns {*}
     * @memberof Incident
     */
    primarycontactname?: any;

    /**
     * 责任联系人
     *
     * @returns {*}
     * @memberof Incident
     */
    responsiblecontactname?: any;

    /**
     * 权利
     *
     * @returns {*}
     * @memberof Incident
     */
    entitlementid?: any;

    /**
     * 客户
     *
     * @returns {*}
     * @memberof Incident
     */
    customerid?: any;

    /**
     * 第一个响应者(按 KPI)
     *
     * @returns {*}
     * @memberof Incident
     */
    firstresponsebykpiid?: any;

    /**
     * SLA
     *
     * @returns {*}
     * @memberof Incident
     */
    slaid?: any;

    /**
     * 现有服务案例
     *
     * @returns {*}
     * @memberof Incident
     */
    existingcase?: any;

    /**
     * 产品
     *
     * @returns {*}
     * @memberof Incident
     */
    productid?: any;

    /**
     * 合同
     *
     * @returns {*}
     * @memberof Incident
     */
    contractid?: any;

    /**
     * 责任联系人
     *
     * @returns {*}
     * @memberof Incident
     */
    responsiblecontactid?: any;

    /**
     * 解决方 KPI
     *
     * @returns {*}
     * @memberof Incident
     */
    resolvebykpiid?: any;

    /**
     * 合同子项
     *
     * @returns {*}
     * @memberof Incident
     */
    contractdetailid?: any;

    /**
     * 主题
     *
     * @returns {*}
     * @memberof Incident
     */
    subjectid?: any;

    /**
     * 主案例
     *
     * @returns {*}
     * @memberof Incident
     */
    masterid?: any;

    /**
     * 上级案例
     *
     * @returns {*}
     * @memberof Incident
     */
    parentcaseid?: any;

    /**
     * 联系人
     *
     * @returns {*}
     * @memberof Incident
     */
    primarycontactid?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof Incident
     */
    transactioncurrencyid?: any;
}
/**
 * 语言
 *
 * @export
 * @interface LanguageLocale
 */
export interface LanguageLocale {

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof LanguageLocale
     */
    createdate?: any;

    /**
     * LanguageLocaleId
     *
     * @returns {*}
     * @memberof LanguageLocale
     */
    languagelocaleid?: any;

    /**
     * 语言环境名称
     *
     * @returns {*}
     * @memberof LanguageLocale
     */
    languagelocalename?: any;

    /**
     * 区域设置 ID
     *
     * @returns {*}
     * @memberof LanguageLocale
     */
    localeid?: any;

    /**
     * 语言
     *
     * @returns {*}
     * @memberof LanguageLocale
     */
    language?: any;

    /**
     * VersionNumber
     *
     * @returns {*}
     * @memberof LanguageLocale
     */
    versionnumber?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof LanguageLocale
     */
    updateman?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof LanguageLocale
     */
    updatedate?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof LanguageLocale
     */
    createman?: any;

    /**
     * 代码
     *
     * @returns {*}
     * @memberof LanguageLocale
     */
    code?: any;

    /**
     * 语言状态代码
     *
     * @returns {*}
     * @memberof LanguageLocale
     */
    statuscode?: any;

    /**
     * 状态代码
     *
     * @returns {*}
     * @memberof LanguageLocale
     */
    statecode?: any;

    /**
     * 地区
     *
     * @returns {*}
     * @memberof LanguageLocale
     */
    region?: any;
}
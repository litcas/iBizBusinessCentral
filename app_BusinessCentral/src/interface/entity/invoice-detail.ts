/**
 * 发票产品
 *
 * @export
 * @interface InvoiceDetail
 */
export interface InvoiceDetail {

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    updatedate?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    invoicedetailname?: any;

    /**
     * 延期交货数量
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    quantitybackordered?: any;

    /**
     * 产品类型
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    producttypecode?: any;

    /**
     * 发票状态
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    invoicestatecode?: any;

    /**
     * Time Zone Rule Version Number
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    timezoneruleversionnumber?: any;

    /**
     * 送至省/市/自治区
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    shipto_stateorprovince?: any;

    /**
     * 金额 (Base)
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    baseamount_base?: any;

    /**
     * 送货地址
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    willcall?: any;

    /**
     * Record Created On
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    overriddencreatedon?: any;

    /**
     * 父捆绑销售
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    parentbundleid?: any;

    /**
     * Import Sequence Number
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    importsequencenumber?: any;

    /**
     * 属性配置
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    propertyconfigurationstatus?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    updateman?: any;

    /**
     * 单价 (Base)
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    priceperunit_base?: any;

    /**
     * 送至国家/地区
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    shipto_country?: any;

    /**
     * 已复制
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    copied?: any;

    /**
     * 负责人类型
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    ownertype?: any;

    /**
     * 明细项目编号
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    lineitemnumber?: any;

    /**
     * 单价
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    priceperunit?: any;

    /**
     * 销售员
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    salesrepname?: any;

    /**
     * 批发折扣 (Base)
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    volumediscountamount_base?: any;

    /**
     * 送货地的传真号码
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    shipto_fax?: any;

    /**
     * 送至街道 1
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    shipto_line1?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    description?: any;

    /**
     * 货运条款
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    shipto_freighttermscode?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    ownerid?: any;

    /**
     * 税 (Base)
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    tax_base?: any;

    /**
     * 送货地的电话号码
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    shipto_telephone?: any;

    /**
     * 定价
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    priceoverridden?: any;

    /**
     * 交付时间
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    actualdeliveryon?: any;

    /**
     * 送货地的名称
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    shipto_name?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    createman?: any;

    /**
     * 金额
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    baseamount?: any;

    /**
     * 运货跟踪号码
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    shippingtrackingnumber?: any;

    /**
     * 发票产品
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    invoicedetailid?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    ownername?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    exchangerate?: any;

    /**
     * 送至街道 3
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    shipto_line3?: any;

    /**
     * 捆绑销售项关联
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    productassociationid?: any;

    /**
     * 送货地的邮政编码
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    shipto_postalcode?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    createdate?: any;

    /**
     * 选择产品
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    productoverridden?: any;

    /**
     * 销售员
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    salesrepid?: any;

    /**
     * UTC Conversion Time Zone Code
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    utcconversiontimezonecode?: any;

    /**
     * 应收净额
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    extendedamount?: any;

    /**
     * 目录外产品
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    productdescription?: any;

    /**
     * 送至街道 2
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    shipto_line2?: any;

    /**
     * 零售折扣
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    manualdiscountamount?: any;

    /**
     * 序号
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    sequencenumber?: any;

    /**
     * 已发货数量
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    quantityshipped?: any;

    /**
     * 已取消数量
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    quantitycancelled?: any;

    /**
     * 定价错误
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    pricingerrorcode?: any;

    /**
     * Version Number
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    versionnumber?: any;

    /**
     * SkipPriceCalculation
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    skippricecalculation?: any;

    /**
     * 数量
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    quantity?: any;

    /**
     * 税
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    tax?: any;

    /**
     * 应收净额 (Base)
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    extendedamount_base?: any;

    /**
     * 批发折扣
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    volumediscountamount?: any;

    /**
     * 送至市/县
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    shipto_city?: any;

    /**
     * 发票价格已锁定
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    invoiceispricelocked?: any;

    /**
     * 产品名称
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    productname?: any;

    /**
     * 计价单位
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    uomname?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    transactioncurrencyid?: any;

    /**
     * Order Product Id
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    salesorderdetailid?: any;

    /**
     * Parent bundle product
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    parentbundleidref?: any;

    /**
     * 现有产品
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    productid?: any;

    /**
     * 发票编码
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    invoiceid?: any;

    /**
     * 计价单位
     *
     * @returns {*}
     * @memberof InvoiceDetail
     */
    uomid?: any;
}
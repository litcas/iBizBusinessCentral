/**
 * 电话联络
 *
 * @export
 * @interface PhoneCall
 */
export interface PhoneCall {

    /**
     * UTC 转换时区代码
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    utcconversiontimezonecode?: any;

    /**
     * 遍历的路径
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    traversedpath?: any;

    /**
     * 关于
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    regardingobjectid?: any;

    /**
     * 导入序列号
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    importsequencenumber?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    updateman?: any;

    /**
     * 创建记录的时间
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    overriddencreatedon?: any;

    /**
     * 上一暂候时间
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    lastonholdtime?: any;

    /**
     * 呼叫方
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    from?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    ownername?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    createdate?: any;

    /**
     * 电话号码
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    phonenumber?: any;

    /**
     * 负责人类型
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    ownertype?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    description?: any;

    /**
     * 主题
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    subject?: any;

    /**
     * 关于
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    regardingobjectname?: any;

    /**
     * 排序日期
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    sortdate?: any;

    /**
     * 实际结束时间
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    actualend?: any;

    /**
     * 状态描述
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    statuscode?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    createman?: any;

    /**
     * 方向
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    directioncode?: any;

    /**
     * 实际开始时间
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    actualstart?: any;

    /**
     * 活动状态
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    statecode?: any;

    /**
     * 暂候时间(分钟)
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    onholdtime?: any;

    /**
     * 保留的语音邮件
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    leftvoicemail?: any;

    /**
     * 持续时间
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    actualdurationminutes?: any;

    /**
     * 版本号
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    versionnumber?: any;

    /**
     * 流程
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    processid?: any;

    /**
     * 子类别
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    subcategory?: any;

    /**
     * RegardingObjectTypeCode
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    regardingobjecttypecode?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    updatedate?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    ownerid?: any;

    /**
     * 流程阶段
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    stageid?: any;

    /**
     * SLAName
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    slaname?: any;

    /**
     * 类别
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    category?: any;

    /**
     * 预订
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    subscriptionid?: any;

    /**
     * 截止日期
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    scheduledend?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    exchangerate?: any;

    /**
     * 活动类型
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    activitytypecode?: any;

    /**
     * 开始日期
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    scheduledstart?: any;

    /**
     * 时区规则版本号
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    timezoneruleversionnumber?: any;

    /**
     * 由工作流创建
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    workflowcreated?: any;

    /**
     * 是定期活动
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    regularactivity?: any;

    /**
     * 被呼叫方
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    to?: any;

    /**
     * 优先级
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    prioritycode?: any;

    /**
     * 电话联络
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    activityid?: any;

    /**
     * 附加参数
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    activityadditionalparams?: any;

    /**
     * 计划持续时间
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    scheduleddurationminutes?: any;

    /**
     * 已记帐
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    billed?: any;

    /**
     * SLA
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    slaid?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    transactioncurrencyid?: any;

    /**
     * 服务
     *
     * @returns {*}
     * @memberof PhoneCall
     */
    serviceid?: any;
}
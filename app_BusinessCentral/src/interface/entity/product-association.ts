/**
 * 产品关联
 *
 * @export
 * @interface ProductAssociation
 */
export interface ProductAssociation {

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof ProductAssociation
     */
    updatedate?: any;

    /**
     * 数量
     *
     * @returns {*}
     * @memberof ProductAssociation
     */
    quantity?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof ProductAssociation
     */
    updateman?: any;

    /**
     * UTC Conversion Time Zone Code
     *
     * @returns {*}
     * @memberof ProductAssociation
     */
    utcconversiontimezonecode?: any;

    /**
     * Version Number
     *
     * @returns {*}
     * @memberof ProductAssociation
     */
    versionnumber?: any;

    /**
     * 属性自定义
     *
     * @returns {*}
     * @memberof ProductAssociation
     */
    propertycustomizationstatus?: any;

    /**
     * Import Sequence Number
     *
     * @returns {*}
     * @memberof ProductAssociation
     */
    importsequencenumber?: any;

    /**
     * Time Zone Rule Version Number
     *
     * @returns {*}
     * @memberof ProductAssociation
     */
    timezoneruleversionnumber?: any;

    /**
     * 必填
     *
     * @returns {*}
     * @memberof ProductAssociation
     */
    productisrequired?: any;

    /**
     * 仅供内部使用
     *
     * @returns {*}
     * @memberof ProductAssociation
     */
    dmtimportstate?: any;

    /**
     * 状态描述
     *
     * @returns {*}
     * @memberof ProductAssociation
     */
    statuscode?: any;

    /**
     * 产品关联 ID
     *
     * @returns {*}
     * @memberof ProductAssociation
     */
    productassociationid?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof ProductAssociation
     */
    exchangerate?: any;

    /**
     * Record Created On
     *
     * @returns {*}
     * @memberof ProductAssociation
     */
    overriddencreatedon?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof ProductAssociation
     */
    createdate?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof ProductAssociation
     */
    createman?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof ProductAssociation
     */
    statecode?: any;

    /**
     * 计量单位
     *
     * @returns {*}
     * @memberof ProductAssociation
     */
    uomname?: any;

    /**
     * 关联的产品
     *
     * @returns {*}
     * @memberof ProductAssociation
     */
    associatedproductname?: any;

    /**
     * 货币名称
     *
     * @returns {*}
     * @memberof ProductAssociation
     */
    currencyname?: any;

    /**
     * 产品名称
     *
     * @returns {*}
     * @memberof ProductAssociation
     */
    productname?: any;

    /**
     * 产品
     *
     * @returns {*}
     * @memberof ProductAssociation
     */
    productid?: any;

    /**
     * 计价单位
     *
     * @returns {*}
     * @memberof ProductAssociation
     */
    uomid?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof ProductAssociation
     */
    transactioncurrencyid?: any;

    /**
     * 关联的产品
     *
     * @returns {*}
     * @memberof ProductAssociation
     */
    associatedproduct?: any;
}
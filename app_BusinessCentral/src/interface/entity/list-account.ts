/**
 * 营销列表-账户
 *
 * @export
 * @interface ListAccount
 */
export interface ListAccount {

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof ListAccount
     */
    createdate?: any;

    /**
     * 关系名称
     *
     * @returns {*}
     * @memberof ListAccount
     */
    relationshipsname?: any;

    /**
     * 关系类型
     *
     * @returns {*}
     * @memberof ListAccount
     */
    relationshipstype?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof ListAccount
     */
    createman?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof ListAccount
     */
    updatedate?: any;

    /**
     * 关系标识
     *
     * @returns {*}
     * @memberof ListAccount
     */
    relationshipsid?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof ListAccount
     */
    updateman?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof ListAccount
     */
    statecode?: any;

    /**
     * 主要联系人
     *
     * @returns {*}
     * @memberof ListAccount
     */
    primarycontactid?: any;

    /**
     * 主要电话
     *
     * @returns {*}
     * @memberof ListAccount
     */
    telephone1?: any;

    /**
     * 主要联系人
     *
     * @returns {*}
     * @memberof ListAccount
     */
    primarycontactname?: any;

    /**
     * 营销列表
     *
     * @returns {*}
     * @memberof ListAccount
     */
    entityname?: any;

    /**
     * 选择客户：
     *
     * @returns {*}
     * @memberof ListAccount
     */
    entity2name?: any;

    /**
     * 列表
     *
     * @returns {*}
     * @memberof ListAccount
     */
    entityid?: any;

    /**
     * 客户
     *
     * @returns {*}
     * @memberof ListAccount
     */
    entity2id?: any;
}
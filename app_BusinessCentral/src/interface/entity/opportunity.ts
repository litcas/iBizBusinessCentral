/**
 * 商机
 *
 * @export
 * @interface Opportunity
 */
export interface Opportunity {

    /**
     * 决策者?
     *
     * @returns {*}
     * @memberof Opportunity
     */
    decisionmaker?: any;

    /**
     * 明细金额总计
     *
     * @returns {*}
     * @memberof Opportunity
     */
    totallineitemamount?: any;

    /**
     * Process Id
     *
     * @returns {*}
     * @memberof Opportunity
     */
    processid?: any;

    /**
     * 商机折扣(%)
     *
     * @returns {*}
     * @memberof Opportunity
     */
    discountpercentage?: any;

    /**
     * 负责人类型
     *
     * @returns {*}
     * @memberof Opportunity
     */
    ownertype?: any;

    /**
     * 定价错误
     *
     * @returns {*}
     * @memberof Opportunity
     */
    pricingerrorcode?: any;

    /**
     * 总金额
     *
     * @returns {*}
     * @memberof Opportunity
     */
    totalamount?: any;

    /**
     * 总税款 (Base)
     *
     * @returns {*}
     * @memberof Opportunity
     */
    totaltax_base?: any;

    /**
     * 总税款
     *
     * @returns {*}
     * @memberof Opportunity
     */
    totaltax?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof Opportunity
     */
    updateman?: any;

    /**
     * 预计收入 (Base)
     *
     * @returns {*}
     * @memberof Opportunity
     */
    estimatedvalue_base?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Opportunity
     */
    ownername?: any;

    /**
     * 提供最终建议
     *
     * @returns {*}
     * @memberof Opportunity
     */
    presentfinalproposal?: any;

    /**
     * 制定建议
     *
     * @returns {*}
     * @memberof Opportunity
     */
    developproposal?: any;

    /**
     * 等级
     *
     * @returns {*}
     * @memberof Opportunity
     */
    opportunityratingcode?: any;

    /**
     * 总金额 (Base)
     *
     * @returns {*}
     * @memberof Opportunity
     */
    totalamount_base?: any;

    /**
     * 折后金额总计
     *
     * @returns {*}
     * @memberof Opportunity
     */
    totalamountlessfreight?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof Opportunity
     */
    description?: any;

    /**
     * 识别竞争对手
     *
     * @returns {*}
     * @memberof Opportunity
     */
    identifycompetitors?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof Opportunity
     */
    updatedate?: any;

    /**
     * 发送致谢信
     *
     * @returns {*}
     * @memberof Opportunity
     */
    sendthankyounote?: any;

    /**
     * 折扣金额总和 (Base)
     *
     * @returns {*}
     * @memberof Opportunity
     */
    totaldiscountamount_base?: any;

    /**
     * 明细项目折扣金额总和
     *
     * @returns {*}
     * @memberof Opportunity
     */
    totallineitemdiscountamount?: any;

    /**
     * 采购程序
     *
     * @returns {*}
     * @memberof Opportunity
     */
    purchaseprocess?: any;

    /**
     * Traversed Path
     *
     * @returns {*}
     * @memberof Opportunity
     */
    traversedpath?: any;

    /**
     * 已收集建议反馈
     *
     * @returns {*}
     * @memberof Opportunity
     */
    captureproposalfeedback?: any;

    /**
     * 最终决策日期
     *
     * @returns {*}
     * @memberof Opportunity
     */
    finaldecisiondate?: any;

    /**
     * 步骤
     *
     * @returns {*}
     * @memberof Opportunity
     */
    stepid?: any;

    /**
     * 反馈已解决
     *
     * @returns {*}
     * @memberof Opportunity
     */
    resolvefeedback?: any;

    /**
     * 预算金额 (Base)
     *
     * @returns {*}
     * @memberof Opportunity
     */
    budgetamount_base?: any;

    /**
     * 决定进行/不进行
     *
     * @returns {*}
     * @memberof Opportunity
     */
    pursuitdecision?: any;

    /**
     * 收入
     *
     * @returns {*}
     * @memberof Opportunity
     */
    revenuesystemcalculated?: any;

    /**
     * 安排建议会议
     *
     * @returns {*}
     * @memberof Opportunity
     */
    scheduleproposalmeeting?: any;

    /**
     * 潜在客户类型
     *
     * @returns {*}
     * @memberof Opportunity
     */
    customertype?: any;

    /**
     * 可能性
     *
     * @returns {*}
     * @memberof Opportunity
     */
    closeprobability?: any;

    /**
     * UTC Conversion Time Zone Code
     *
     * @returns {*}
     * @memberof Opportunity
     */
    utcconversiontimezonecode?: any;

    /**
     * 已提供建议
     *
     * @returns {*}
     * @memberof Opportunity
     */
    presentproposal?: any;

    /**
     * 已安排跟进(潜在客户)
     *
     * @returns {*}
     * @memberof Opportunity
     */
    schedulefollowup_prospect?: any;

    /**
     * 状态描述
     *
     * @returns {*}
     * @memberof Opportunity
     */
    statuscode?: any;

    /**
     * 实际截止日期
     *
     * @returns {*}
     * @memberof Opportunity
     */
    actualclosedate?: any;

    /**
     * 确认有兴趣
     *
     * @returns {*}
     * @memberof Opportunity
     */
    confirminterest?: any;

    /**
     * 潜在客户
     *
     * @returns {*}
     * @memberof Opportunity
     */
    customerid?: any;

    /**
     * 销售阶段
     *
     * @returns {*}
     * @memberof Opportunity
     */
    salesstage?: any;

    /**
     * 商机
     *
     * @returns {*}
     * @memberof Opportunity
     */
    opportunityid?: any;

    /**
     * 暂候时间(分钟)
     *
     * @returns {*}
     * @memberof Opportunity
     */
    onholdtime?: any;

    /**
     * 资格注释
     *
     * @returns {*}
     * @memberof Opportunity
     */
    qualificationcomments?: any;

    /**
     * 已安排跟进(授予资格)
     *
     * @returns {*}
     * @memberof Opportunity
     */
    schedulefollowup_qualify?: any;

    /**
     * 参与工作流
     *
     * @returns {*}
     * @memberof Opportunity
     */
    participatesinworkflow?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof Opportunity
     */
    exchangerate?: any;

    /**
     * 准备阶段
     *
     * @returns {*}
     * @memberof Opportunity
     */
    stepname?: any;

    /**
     * 客户需求
     *
     * @returns {*}
     * @memberof Opportunity
     */
    customerneed?: any;

    /**
     * Import Sequence Number
     *
     * @returns {*}
     * @memberof Opportunity
     */
    importsequencenumber?: any;

    /**
     * 完成内部审核
     *
     * @returns {*}
     * @memberof Opportunity
     */
    completeinternalreview?: any;

    /**
     * 评估相符程度
     *
     * @returns {*}
     * @memberof Opportunity
     */
    evaluatefit?: any;

    /**
     * 处理代码
     *
     * @returns {*}
     * @memberof Opportunity
     */
    salesstagecode?: any;

    /**
     * 识别销售团队
     *
     * @returns {*}
     * @memberof Opportunity
     */
    identifypursuitteam?: any;

    /**
     * 初始通信
     *
     * @returns {*}
     * @memberof Opportunity
     */
    initialcommunication?: any;

    /**
     * Version Number
     *
     * @returns {*}
     * @memberof Opportunity
     */
    versionnumber?: any;

    /**
     * 运费金额 (Base)
     *
     * @returns {*}
     * @memberof Opportunity
     */
    freightamount_base?: any;

    /**
     * 客户
     *
     * @returns {*}
     * @memberof Opportunity
     */
    accountname?: any;

    /**
     * TeamsFollowed
     *
     * @returns {*}
     * @memberof Opportunity
     */
    teamsfollowed?: any;

    /**
     * 已拟定解决方案
     *
     * @returns {*}
     * @memberof Opportunity
     */
    proposedsolution?: any;

    /**
     * 归档任务报告
     *
     * @returns {*}
     * @memberof Opportunity
     */
    filedebrief?: any;

    /**
     * 预算金额
     *
     * @returns {*}
     * @memberof Opportunity
     */
    budgetamount?: any;

    /**
     * 预算
     *
     * @returns {*}
     * @memberof Opportunity
     */
    budgetstatus?: any;

    /**
     * 商机折扣额 (Base)
     *
     * @returns {*}
     * @memberof Opportunity
     */
    discountamount_base?: any;

    /**
     * 最终建议已就绪
     *
     * @returns {*}
     * @memberof Opportunity
     */
    completefinalproposal?: any;

    /**
     * 商机折扣额
     *
     * @returns {*}
     * @memberof Opportunity
     */
    discountamount?: any;

    /**
     * 需求
     *
     * @returns {*}
     * @memberof Opportunity
     */
    need?: any;

    /**
     * Stage Id
     *
     * @returns {*}
     * @memberof Opportunity
     */
    stageid?: any;

    /**
     * 运费金额
     *
     * @returns {*}
     * @memberof Opportunity
     */
    freightamount?: any;

    /**
     * 实际收入
     *
     * @returns {*}
     * @memberof Opportunity
     */
    actualvalue?: any;

    /**
     * 是否私有
     *
     * @returns {*}
     * @memberof Opportunity
     */
    ibizprivate?: any;

    /**
     * Time Zone Rule Version Number
     *
     * @returns {*}
     * @memberof Opportunity
     */
    timezoneruleversionnumber?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof Opportunity
     */
    createdate?: any;

    /**
     * 优先级
     *
     * @returns {*}
     * @memberof Opportunity
     */
    prioritycode?: any;

    /**
     * 明细金额总计 (Base)
     *
     * @returns {*}
     * @memberof Opportunity
     */
    totallineitemamount_base?: any;

    /**
     * 客户
     *
     * @returns {*}
     * @memberof Opportunity
     */
    customername?: any;

    /**
     * 预计结束日期
     *
     * @returns {*}
     * @memberof Opportunity
     */
    estimatedclosedate?: any;

    /**
     * 当前状况
     *
     * @returns {*}
     * @memberof Opportunity
     */
    currentsituation?: any;

    /**
     * 实际收入 (Base)
     *
     * @returns {*}
     * @memberof Opportunity
     */
    actualvalue_base?: any;

    /**
     * 客户难点
     *
     * @returns {*}
     * @memberof Opportunity
     */
    customerpainpoints?: any;

    /**
     * 折扣金额总和
     *
     * @returns {*}
     * @memberof Opportunity
     */
    totaldiscountamount?: any;

    /**
     * 商机名称
     *
     * @returns {*}
     * @memberof Opportunity
     */
    opportunityname?: any;

    /**
     * 识别客户联系人
     *
     * @returns {*}
     * @memberof Opportunity
     */
    identifycustomercontacts?: any;

    /**
     * 日程表
     *
     * @returns {*}
     * @memberof Opportunity
     */
    timeline?: any;

    /**
     * 联系人
     *
     * @returns {*}
     * @memberof Opportunity
     */
    contactname?: any;

    /**
     * 上一暂候时间
     *
     * @returns {*}
     * @memberof Opportunity
     */
    lastonholdtime?: any;

    /**
     * 折后金额总计 (Base)
     *
     * @returns {*}
     * @memberof Opportunity
     */
    totalamountlessfreight_base?: any;

    /**
     * 预计收入
     *
     * @returns {*}
     * @memberof Opportunity
     */
    estimatedvalue?: any;

    /**
     * Email Address
     *
     * @returns {*}
     * @memberof Opportunity
     */
    emailaddress?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof Opportunity
     */
    statecode?: any;

    /**
     * 购买时间范围
     *
     * @returns {*}
     * @memberof Opportunity
     */
    purchasetimeframe?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof Opportunity
     */
    createman?: any;

    /**
     * 报价单注释
     *
     * @returns {*}
     * @memberof Opportunity
     */
    quotecomments?: any;

    /**
     * Record Created On
     *
     * @returns {*}
     * @memberof Opportunity
     */
    overriddencreatedon?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Opportunity
     */
    ownerid?: any;

    /**
     * 帐户
     *
     * @returns {*}
     * @memberof Opportunity
     */
    parentaccountname?: any;

    /**
     * 原始潜在顾客
     *
     * @returns {*}
     * @memberof Opportunity
     */
    originatingleadname?: any;

    /**
     * 联系人
     *
     * @returns {*}
     * @memberof Opportunity
     */
    parentcontactname?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof Opportunity
     */
    currencyname?: any;

    /**
     * 价目表
     *
     * @returns {*}
     * @memberof Opportunity
     */
    pricelevelname?: any;

    /**
     * SLA
     *
     * @returns {*}
     * @memberof Opportunity
     */
    slaname?: any;

    /**
     * 源市场活动
     *
     * @returns {*}
     * @memberof Opportunity
     */
    campaignname?: any;

    /**
     * 联系人
     *
     * @returns {*}
     * @memberof Opportunity
     */
    parentcontactid?: any;

    /**
     * 源市场活动
     *
     * @returns {*}
     * @memberof Opportunity
     */
    campaignid?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof Opportunity
     */
    transactioncurrencyid?: any;

    /**
     * 帐户
     *
     * @returns {*}
     * @memberof Opportunity
     */
    parentaccountid?: any;

    /**
     * 价目表
     *
     * @returns {*}
     * @memberof Opportunity
     */
    pricelevelid?: any;

    /**
     * 原始潜在顾客
     *
     * @returns {*}
     * @memberof Opportunity
     */
    originatingleadid?: any;

    /**
     * SLA
     *
     * @returns {*}
     * @memberof Opportunity
     */
    slaid?: any;
}
/**
 * 部门成员
 *
 * @export
 * @interface IBZDeptMember
 */
export interface IBZDeptMember {

    /**
     * 标识
     *
     * @returns {*}
     * @memberof IBZDeptMember
     */
    memberid?: any;

    /**
     * 部门标识
     *
     * @returns {*}
     * @memberof IBZDeptMember
     */
    deptid?: any;

    /**
     * 部门名称
     *
     * @returns {*}
     * @memberof IBZDeptMember
     */
    deptname?: any;

    /**
     * 用户标识
     *
     * @returns {*}
     * @memberof IBZDeptMember
     */
    userid?: any;

    /**
     * 成员
     *
     * @returns {*}
     * @memberof IBZDeptMember
     */
    personname?: any;

    /**
     * 岗位标识
     *
     * @returns {*}
     * @memberof IBZDeptMember
     */
    postid?: any;

    /**
     * 岗位名称
     *
     * @returns {*}
     * @memberof IBZDeptMember
     */
    postname?: any;

    /**
     * 业务条线
     *
     * @returns {*}
     * @memberof IBZDeptMember
     */
    bcode?: any;

    /**
     * 区属
     *
     * @returns {*}
     * @memberof IBZDeptMember
     */
    domains?: any;
}
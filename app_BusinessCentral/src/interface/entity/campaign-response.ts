/**
 * 市场活动响应
 *
 * @export
 * @interface CampaignResponse
 */
export interface CampaignResponse {

    /**
     * RegardingObjectTypeCode
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    regardingobjecttypecode?: any;

    /**
     * 已记帐
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    billed?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    updatedate?: any;

    /**
     * 渠道
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    channeltypecode?: any;

    /**
     * 上一暂候时间
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    lastonholdtime?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    description?: any;

    /**
     * Import Sequence Number
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    importsequencenumber?: any;

    /**
     * Optional Attendees
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    optionalattendees?: any;

    /**
     * Customers
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    customers?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    createdate?: any;

    /**
     * 活动
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    originatingactivityname?: any;

    /**
     * Organizer
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    organizer?: any;

    /**
     * 优先级
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    prioritycode?: any;

    /**
     * 状态描述
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    statuscode?: any;

    /**
     * 流程
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    processid?: any;

    /**
     * 实际持续时间
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    actualdurationminutes?: any;

    /**
     * 时区规则版本号
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    timezoneruleversionnumber?: any;

    /**
     * Required Attendees
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    requiredattendees?: any;

    /**
     * 关于
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    regardingobjectid?: any;

    /**
     * 电话
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    telephone?: any;

    /**
     * 负责人类型
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    ownertype?: any;

    /**
     * 计划开始时间
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    scheduledstart?: any;

    /**
     * 响应代码
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    responsecode?: any;

    /**
     * 实际开始时间
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    actualstart?: any;

    /**
     * Resources
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    resources?: any;

    /**
     * 服务
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    serviceid?: any;

    /**
     * 定期实例类型
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    instancetypecode?: any;

    /**
     * 主题
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    subject?: any;

    /**
     * Exchange WebLink
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    exchangeweblink?: any;

    /**
     * 由工作流创建
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    workflowcreated?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    createman?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    ownername?: any;

    /**
     * 名
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    firstname?: any;

    /**
     * 来自
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    from?: any;

    /**
     * 版本号
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    versionnumber?: any;

    /**
     * 活动类型
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    activitytypecode?: any;

    /**
     * 促销代码
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    promotioncodename?: any;

    /**
     * CC
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    cc?: any;

    /**
     * 电子邮件
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    emailaddress?: any;

    /**
     * 上次尝试传递的日期
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    deliverylastattemptedon?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    ownerid?: any;

    /**
     * 类别
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    category?: any;

    /**
     * 暂候时间(分钟)
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    onholdtime?: any;

    /**
     * 接收时间
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    receivedon?: any;

    /**
     * OriginatingActivityTypeCode
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    originatingactivitytypecode?: any;

    /**
     * 外包供应商
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    partner?: any;

    /**
     * UTC 转换时区代码
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    utcconversiontimezonecode?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    updateman?: any;

    /**
     * 流程阶段
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    stageid?: any;

    /**
     * 子类别
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    subcategory?: any;

    /**
     * 实际结束时间
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    actualend?: any;

    /**
     * 传真
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    fax?: any;

    /**
     * 隐藏
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    mapiprivate?: any;

    /**
     * 原始活动
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    originatingactivityid?: any;

    /**
     * 是定期活动
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    regularactivity?: any;

    /**
     * BCC
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    bcc?: any;

    /**
     * Outsource Vendors
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    partners?: any;

    /**
     * 发送日期
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    senton?: any;

    /**
     * 客户
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    customer?: any;

    /**
     * 关于
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    regardingobjectname?: any;

    /**
     * Record Created On
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    overriddencreatedon?: any;

    /**
     * Exchange 项目 ID
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    exchangeitemid?: any;

    /**
     * SLAName
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    slaname?: any;

    /**
     * 系列 ID
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    seriesid?: any;

    /**
     * 社交渠道
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    community?: any;

    /**
     * 遍历的路径
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    traversedpath?: any;

    /**
     * 传递优先级
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    deliveryprioritycode?: any;

    /**
     * 公司名称
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    companyname?: any;

    /**
     * 排序日期
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    sortdate?: any;

    /**
     * 附加参数
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    activityadditionalparams?: any;

    /**
     * 市场活动响应
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    activityid?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    statecode?: any;

    /**
     * 计划持续时间
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    scheduleddurationminutes?: any;

    /**
     * 姓
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    lastname?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    exchangerate?: any;

    /**
     * To
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    to?: any;

    /**
     * 保留的语音邮件
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    leftvoicemail?: any;

    /**
     * 结束时间
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    scheduledend?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    transactioncurrencyid?: any;

    /**
     * SLA
     *
     * @returns {*}
     * @memberof CampaignResponse
     */
    slaid?: any;
}
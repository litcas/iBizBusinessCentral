/**
 * 销售宣传资料
 *
 * @export
 * @interface SalesLiterature
 */
export interface SalesLiterature {

    /**
     * Import Sequence Number
     *
     * @returns {*}
     * @memberof SalesLiterature
     */
    importsequencenumber?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof SalesLiterature
     */
    createdate?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof SalesLiterature
     */
    createman?: any;

    /**
     * Time Zone Rule Version Number
     *
     * @returns {*}
     * @memberof SalesLiterature
     */
    timezoneruleversionnumber?: any;

    /**
     * 向客户公开
     *
     * @returns {*}
     * @memberof SalesLiterature
     */
    customerviewable?: any;

    /**
     * 到期日期
     *
     * @returns {*}
     * @memberof SalesLiterature
     */
    expirationdate?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof SalesLiterature
     */
    exchangerate?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof SalesLiterature
     */
    updatedate?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof SalesLiterature
     */
    description?: any;

    /**
     * 销售资料名称
     *
     * @returns {*}
     * @memberof SalesLiterature
     */
    salesliteraturename?: any;

    /**
     * Process Id
     *
     * @returns {*}
     * @memberof SalesLiterature
     */
    processid?: any;

    /**
     * Stage Id
     *
     * @returns {*}
     * @memberof SalesLiterature
     */
    stageid?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof SalesLiterature
     */
    updateman?: any;

    /**
     * 实体图像
     *
     * @returns {*}
     * @memberof SalesLiterature
     */
    entityimage?: any;

    /**
     * EntityImage_URL
     *
     * @returns {*}
     * @memberof SalesLiterature
     */
    entityimage_url?: any;

    /**
     * 有附件
     *
     * @returns {*}
     * @memberof SalesLiterature
     */
    hasattachments?: any;

    /**
     * Record Created On
     *
     * @returns {*}
     * @memberof SalesLiterature
     */
    overriddencreatedon?: any;

    /**
     * EntityImage_Timestamp
     *
     * @returns {*}
     * @memberof SalesLiterature
     */
    entityimage_timestamp?: any;

    /**
     * 责任人
     *
     * @returns {*}
     * @memberof SalesLiterature
     */
    employeecontactid?: any;

    /**
     * 关键字
     *
     * @returns {*}
     * @memberof SalesLiterature
     */
    keywords?: any;

    /**
     * Version Number
     *
     * @returns {*}
     * @memberof SalesLiterature
     */
    versionnumber?: any;

    /**
     * UTC Conversion Time Zone Code
     *
     * @returns {*}
     * @memberof SalesLiterature
     */
    utcconversiontimezonecode?: any;

    /**
     * 类型
     *
     * @returns {*}
     * @memberof SalesLiterature
     */
    literaturetypecode?: any;

    /**
     * 销售宣传资料
     *
     * @returns {*}
     * @memberof SalesLiterature
     */
    salesliteratureid?: any;

    /**
     * EntityImageId
     *
     * @returns {*}
     * @memberof SalesLiterature
     */
    entityimageid?: any;

    /**
     * 联系人
     *
     * @returns {*}
     * @memberof SalesLiterature
     */
    employeecontactname?: any;

    /**
     * Traversed Path
     *
     * @returns {*}
     * @memberof SalesLiterature
     */
    traversedpath?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof SalesLiterature
     */
    currencyname?: any;

    /**
     * 主题
     *
     * @returns {*}
     * @memberof SalesLiterature
     */
    subjectname?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof SalesLiterature
     */
    transactioncurrencyid?: any;

    /**
     * 主题
     *
     * @returns {*}
     * @memberof SalesLiterature
     */
    subjectid?: any;
}
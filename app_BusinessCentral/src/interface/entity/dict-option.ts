/**
 * 字典项
 *
 * @export
 * @interface DictOption
 */
export interface DictOption {

    /**
     * 标识
     *
     * @returns {*}
     * @memberof DictOption
     */
    value_key?: any;

    /**
     * 目录代码
     *
     * @returns {*}
     * @memberof DictOption
     */
    catalog_id?: any;

    /**
     * 目录
     *
     * @returns {*}
     * @memberof DictOption
     */
    catalog_name?: any;

    /**
     * 代码值
     *
     * @returns {*}
     * @memberof DictOption
     */
    value?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof DictOption
     */
    label?: any;

    /**
     * 父代码值
     *
     * @returns {*}
     * @memberof DictOption
     */
    parent?: any;

    /**
     * 过滤项
     *
     * @returns {*}
     * @memberof DictOption
     */
    filter?: any;

    /**
     * 栏目样式
     *
     * @returns {*}
     * @memberof DictOption
     */
    cls?: any;

    /**
     * 图标
     *
     * @returns {*}
     * @memberof DictOption
     */
    icon_class?: any;

    /**
     * 是否禁用
     *
     * @returns {*}
     * @memberof DictOption
     */
    disabled?: any;

    /**
     * 过期/失效
     *
     * @returns {*}
     * @memberof DictOption
     */
    expired?: any;

    /**
     * 排序
     *
     * @returns {*}
     * @memberof DictOption
     */
    showorder?: any;

    /**
     * 扩展
     *
     * @returns {*}
     * @memberof DictOption
     */
    extension?: any;

    /**
     * 最后修改时间
     *
     * @returns {*}
     * @memberof DictOption
     */
    updatedate?: any;
}
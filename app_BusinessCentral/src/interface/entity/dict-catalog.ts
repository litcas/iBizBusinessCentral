/**
 * 字典
 *
 * @export
 * @interface DictCatalog
 */
export interface DictCatalog {

    /**
     * 标识
     *
     * @returns {*}
     * @memberof DictCatalog
     */
    id?: any;

    /**
     * 代码
     *
     * @returns {*}
     * @memberof DictCatalog
     */
    code?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof DictCatalog
     */
    name?: any;

    /**
     * 分组
     *
     * @returns {*}
     * @memberof DictCatalog
     */
    group?: any;

    /**
     * 备注
     *
     * @returns {*}
     * @memberof DictCatalog
     */
    memo?: any;

    /**
     * 是否有效
     *
     * @returns {*}
     * @memberof DictCatalog
     */
    enable?: any;

    /**
     * 最后修改时间
     *
     * @returns {*}
     * @memberof DictCatalog
     */
    updatedate?: any;
}
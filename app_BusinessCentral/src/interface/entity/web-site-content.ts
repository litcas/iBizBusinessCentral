/**
 * 站点内容
 *
 * @export
 * @interface WebSiteContent
 */
export interface WebSiteContent {

    /**
     * 内容
     *
     * @returns {*}
     * @memberof WebSiteContent
     */
    content?: any;

    /**
     * 内容标识
     *
     * @returns {*}
     * @memberof WebSiteContent
     */
    websitecontentid?: any;

    /**
     * 业务标识
     *
     * @returns {*}
     * @memberof WebSiteContent
     */
    contentcode?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof WebSiteContent
     */
    createman?: any;

    /**
     * 备注
     *
     * @returns {*}
     * @memberof WebSiteContent
     */
    memo?: any;

    /**
     * 作者
     *
     * @returns {*}
     * @memberof WebSiteContent
     */
    author?: any;

    /**
     * 是否启用
     *
     * @returns {*}
     * @memberof WebSiteContent
     */
    validflag?: any;

    /**
     * 标题
     *
     * @returns {*}
     * @memberof WebSiteContent
     */
    title?: any;

    /**
     * 内容链接
     *
     * @returns {*}
     * @memberof WebSiteContent
     */
    link?: any;

    /**
     * 序号
     *
     * @returns {*}
     * @memberof WebSiteContent
     */
    sn?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof WebSiteContent
     */
    updatedate?: any;

    /**
     * 子标题
     *
     * @returns {*}
     * @memberof WebSiteContent
     */
    subtitle?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof WebSiteContent
     */
    updateman?: any;

    /**
     * 内容名称
     *
     * @returns {*}
     * @memberof WebSiteContent
     */
    websitecontentname?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof WebSiteContent
     */
    createdate?: any;

    /**
     * 内容类型
     *
     * @returns {*}
     * @memberof WebSiteContent
     */
    contenttype?: any;

    /**
     * 频道
     *
     * @returns {*}
     * @memberof WebSiteContent
     */
    websitechannelname?: any;

    /**
     * 站点
     *
     * @returns {*}
     * @memberof WebSiteContent
     */
    websitename?: any;

    /**
     * 网站标识
     *
     * @returns {*}
     * @memberof WebSiteContent
     */
    websiteid?: any;

    /**
     * 频道标识
     *
     * @returns {*}
     * @memberof WebSiteContent
     */
    websitechannelid?: any;
}
/**
 * 订单
 *
 * @export
 * @interface SalesOrder
 */
export interface SalesOrder {

    /**
     * 实体图像
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    entityimage?: any;

    /**
     * 定价错误
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    pricingerrorcode?: any;

    /**
     * 送至国家/地区
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    shipto_country?: any;

    /**
     * 送货地的传真号码
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    shipto_fax?: any;

    /**
     * 明细金额总计 (Base)
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    totallineitemamount_base?: any;

    /**
     * 帐单寄往国家/地区
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    billto_country?: any;

    /**
     * 折后金额总计
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    totalamountlessfreight?: any;

    /**
     * 送货地址
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    willcall?: any;

    /**
     * 客户
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    accountname?: any;

    /**
     * 订单折扣金额 (Base)
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    discountamount_base?: any;

    /**
     * 完成日期
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    datefulfilled?: any;

    /**
     * 客户类型
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    customertype?: any;

    /**
     * 帐单寄往街道 3
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    billto_line3?: any;

    /**
     * 帐单寄往街道 1
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    billto_line1?: any;

    /**
     * UTC Conversion Time Zone Code
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    utcconversiontimezonecode?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    ownerid?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    exchangerate?: any;

    /**
     * EntityImageId
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    entityimageid?: any;

    /**
     * 送至街道 3
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    shipto_line3?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    createman?: any;

    /**
     * 送货地的电话号码
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    shipto_telephone?: any;

    /**
     * Stage Id
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    stageid?: any;

    /**
     * 客户
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    customername?: any;

    /**
     * Traversed Path
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    traversedpath?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    updateman?: any;

    /**
     * 订单编码
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    ordernumber?: any;

    /**
     * Time Zone Rule Version Number
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    timezoneruleversionnumber?: any;

    /**
     * 折扣金额总和 (Base)
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    totaldiscountamount_base?: any;

    /**
     * 送货地的联系人姓名
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    shipto_contactname?: any;

    /**
     * 要求交付日期
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    requestdeliveryby?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    ownername?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    statecode?: any;

    /**
     * 明细项目折扣金额总和
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    totallineitemdiscountamount?: any;

    /**
     * 货运条款
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    shipto_freighttermscode?: any;

    /**
     * 总税款
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    totaltax?: any;

    /**
     * 送至街道 1
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    shipto_line1?: any;

    /**
     * 订单折扣(%)
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    discountpercentage?: any;

    /**
     * 优先级
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    prioritycode?: any;

    /**
     * 帐单寄往地址
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    billto_composite?: any;

    /**
     * 状态描述
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    statuscode?: any;

    /**
     * 送货地址 ID
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    shipto_addressid?: any;

    /**
     * 付款条件
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    paymenttermscode?: any;

    /**
     * 帐单寄往地邮政编码
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    billto_postalcode?: any;

    /**
     * 帐单邮寄地址 ID
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    billto_addressid?: any;

    /**
     * 帐单寄往地的电话号码
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    billto_telephone?: any;

    /**
     * 送货地址
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    shipto_composite?: any;

    /**
     * 已提交状态
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    submitstatus?: any;

    /**
     * 已提交状态说明
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    submitstatusdescription?: any;

    /**
     * Email Address
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    emailaddress?: any;

    /**
     * 帐单寄往市/县
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    billto_city?: any;

    /**
     * 运费金额 (Base)
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    freightamount_base?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    createdate?: any;

    /**
     * 订单
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    salesorderid?: any;

    /**
     * 联系人
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    contactname?: any;

    /**
     * 运费金额
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    freightamount?: any;

    /**
     * 送至省/市/自治区
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    shipto_stateorprovince?: any;

    /**
     * 订单折扣金额
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    discountamount?: any;

    /**
     * 总金额
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    totalamount?: any;

    /**
     * 折扣金额总和
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    totaldiscountamount?: any;

    /**
     * 总金额 (Base)
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    totalamount_base?: any;

    /**
     * Import Sequence Number
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    importsequencenumber?: any;

    /**
     * 折后金额总计 (Base)
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    totalamountlessfreight_base?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    updatedate?: any;

    /**
     * Version Number
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    versionnumber?: any;

    /**
     * 送至市/县
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    shipto_city?: any;

    /**
     * 帐单寄往街道 2
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    billto_line2?: any;

    /**
     * 送至街道 2
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    shipto_line2?: any;

    /**
     * Process Id
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    processid?: any;

    /**
     * 帐单寄往地的名称
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    billto_name?: any;

    /**
     * 明细金额总计
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    totallineitemamount?: any;

    /**
     * 上一暂候时间
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    lastonholdtime?: any;

    /**
     * 最近提交给 Back Office
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    lastbackofficesubmit?: any;

    /**
     * 提交日期
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    submitdate?: any;

    /**
     * 帐单寄往地的传真号码
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    billto_fax?: any;

    /**
     * 送货地的邮政编码
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    shipto_postalcode?: any;

    /**
     * EntityImage_URL
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    entityimage_url?: any;

    /**
     * Record Created On
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    overriddencreatedon?: any;

    /**
     * 客户
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    customerid?: any;

    /**
     * 帐单寄往地联系人姓名
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    billto_contactname?: any;

    /**
     * 暂候时间(分钟)
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    onholdtime?: any;

    /**
     * 总税款 (Base)
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    totaltax_base?: any;

    /**
     * 负责人类型
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    ownertype?: any;

    /**
     * EntityImage_Timestamp
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    entityimage_timestamp?: any;

    /**
     * 货运条款
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    freighttermscode?: any;

    /**
     * 帐单寄往省/市/自治区
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    billto_stateorprovince?: any;

    /**
     * 送货地的名称
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    shipto_name?: any;

    /**
     * 销售订单名称
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    salesordername?: any;

    /**
     * 送货方式
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    shippingmethodcode?: any;

    /**
     * 已锁定的价格
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    pricelocked?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    description?: any;

    /**
     * 源市场活动
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    campaignname?: any;

    /**
     * 商机
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    opportunityname?: any;

    /**
     * SLA
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    slaname?: any;

    /**
     * 报价单
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    quotename?: any;

    /**
     * 价目表
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    pricelevelname?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    currencyname?: any;

    /**
     * 报价单
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    quoteid?: any;

    /**
     * SLA
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    slaid?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    transactioncurrencyid?: any;

    /**
     * 商机
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    opportunityid?: any;

    /**
     * 价目表
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    pricelevelid?: any;

    /**
     * 源市场活动
     *
     * @returns {*}
     * @memberof SalesOrder
     */
    campaignid?: any;
}
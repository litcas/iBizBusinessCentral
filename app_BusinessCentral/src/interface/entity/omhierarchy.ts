/**
 * 组织层次结构
 *
 * @export
 * @interface OMHierarchy
 */
export interface OMHierarchy {

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof OMHierarchy
     */
    createdate?: any;

    /**
     * 组织层次结构标识
     *
     * @returns {*}
     * @memberof OMHierarchy
     */
    omhierarchyid?: any;

    /**
     * 组织层次结构名称
     *
     * @returns {*}
     * @memberof OMHierarchy
     */
    omhierarchyname?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof OMHierarchy
     */
    updatedate?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof OMHierarchy
     */
    updateman?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof OMHierarchy
     */
    createman?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof OMHierarchy
     */
    organizationid?: any;

    /**
     * 父组织层次结构标识
     *
     * @returns {*}
     * @memberof OMHierarchy
     */
    pomhierarchyid?: any;

    /**
     * 结构层次类别标识
     *
     * @returns {*}
     * @memberof OMHierarchy
     */
    omhierarchycatid?: any;

    /**
     * 是否有效
     *
     * @returns {*}
     * @memberof OMHierarchy
     */
    valid?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof OMHierarchy
     */
    displayname?: any;

    /**
     * 序号
     *
     * @returns {*}
     * @memberof OMHierarchy
     */
    sn?: any;

    /**
     * 代码
     *
     * @returns {*}
     * @memberof OMHierarchy
     */
    codevalue?: any;

    /**
     * 排序号
     *
     * @returns {*}
     * @memberof OMHierarchy
     */
    showorder?: any;

    /**
     * 组织简称
     *
     * @returns {*}
     * @memberof OMHierarchy
     */
    shortname?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof OMHierarchy
     */
    organizationname?: any;

    /**
     * 层次类别
     *
     * @returns {*}
     * @memberof OMHierarchy
     */
    omhierarchycatname?: any;

    /**
     * 上级组织
     *
     * @returns {*}
     * @memberof OMHierarchy
     */
    pomhierarchyname?: any;
}
/**
 * 营销列表-潜在客户
 *
 * @export
 * @interface ListLead
 */
export interface ListLead {

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof ListLead
     */
    updateman?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof ListLead
     */
    createdate?: any;

    /**
     * 关系类型
     *
     * @returns {*}
     * @memberof ListLead
     */
    relationshipstype?: any;

    /**
     * 关系标识
     *
     * @returns {*}
     * @memberof ListLead
     */
    relationshipsid?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof ListLead
     */
    createman?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof ListLead
     */
    updatedate?: any;

    /**
     * 关系名称
     *
     * @returns {*}
     * @memberof ListLead
     */
    relationshipsname?: any;

    /**
     * 营销列表
     *
     * @returns {*}
     * @memberof ListLead
     */
    entityname?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof ListLead
     */
    ownerid?: any;

    /**
     * 潜在顾客
     *
     * @returns {*}
     * @memberof ListLead
     */
    entity2name?: any;

    /**
     * 主题
     *
     * @returns {*}
     * @memberof ListLead
     */
    subject?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof ListLead
     */
    ownername?: any;

    /**
     * 状态描述
     *
     * @returns {*}
     * @memberof ListLead
     */
    statuscode?: any;

    /**
     * 列表
     *
     * @returns {*}
     * @memberof ListLead
     */
    entityid?: any;

    /**
     * 潜在顾客
     *
     * @returns {*}
     * @memberof ListLead
     */
    entity2id?: any;
}
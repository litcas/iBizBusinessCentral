/**
 * 结构层次类别
 *
 * @export
 * @interface OMHierarchyCat
 */
export interface OMHierarchyCat {

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof OMHierarchyCat
     */
    updatedate?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof OMHierarchyCat
     */
    updateman?: any;

    /**
     *  结构层次类别名称
     *
     * @returns {*}
     * @memberof OMHierarchyCat
     */
    omhierarchycatname?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof OMHierarchyCat
     */
    createman?: any;

    /**
     *  结构层次类别标识
     *
     * @returns {*}
     * @memberof OMHierarchyCat
     */
    omhierarchycatid?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof OMHierarchyCat
     */
    createdate?: any;

    /**
     * 备注
     *
     * @returns {*}
     * @memberof OMHierarchyCat
     */
    memo?: any;
}
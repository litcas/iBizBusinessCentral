/**
 * 快速市场活动
 *
 * @export
 * @interface BulkOperation
 */
export interface BulkOperation {

    /**
     * 参数
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    parameters?: any;

    /**
     * 关于
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    regardingobjectname?: any;

    /**
     * Customers
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    customers?: any;

    /**
     * 遍历的路径
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    traversedpath?: any;

    /**
     * 开始日期
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    scheduledstart?: any;

    /**
     * 成员总数
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    targetmemberscount?: any;

    /**
     * BCC
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    bcc?: any;

    /**
     * 实际持续时间
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    actualdurationminutes?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    exchangerate?: any;

    /**
     * 批量操作号
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    bulkoperationnumber?: any;

    /**
     * 截止日期
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    scheduledend?: any;

    /**
     * 暂候时间(分钟)
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    onholdtime?: any;

    /**
     * 保留的语音邮件
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    leftvoicemail?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    ownername?: any;

    /**
     * 时区规则版本号
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    timezoneruleversionnumber?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    description?: any;

    /**
     * Outsource Vendors
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    partners?: any;

    /**
     * 工作流信息
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    workflowinfo?: any;

    /**
     * 服务
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    serviceid?: any;

    /**
     * 社交渠道
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    community?: any;

    /**
     * 由工作流创建
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    workflowcreated?: any;

    /**
     * 负责人类型
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    ownertype?: any;

    /**
     * 活动类型
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    createdrecordtypecode?: any;

    /**
     * 错误号
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    errornumber?: any;

    /**
     * Record Created On
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    overriddencreatedon?: any;

    /**
     * 导入文件名称
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    regardingobjectid?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    statecode?: any;

    /**
     * 上次尝试传递的日期
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    deliverylastattemptedon?: any;

    /**
     * 传递优先级
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    deliveryprioritycode?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    createman?: any;

    /**
     * Organizer
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    organizer?: any;

    /**
     * 流程阶段
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    stageid?: any;

    /**
     * 成员类型
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    targetedrecordtypecode?: any;

    /**
     * 定期实例类型
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    instancetypecode?: any;

    /**
     * To
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    to?: any;

    /**
     * 计划持续时间
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    scheduleddurationminutes?: any;

    /**
     * Exchange 项目 ID
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    exchangeitemid?: any;

    /**
     * 活动
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    activityid?: any;

    /**
     * From
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    from?: any;

    /**
     * 隐藏
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    mapiprivate?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    ownerid?: any;

    /**
     * 状态描述
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    statuscode?: any;

    /**
     * 操作
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    operationtypecode?: any;

    /**
     * 活动附加参数
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    activityadditionalparams?: any;

    /**
     * UTC 转换时区代码
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    utcconversiontimezonecode?: any;

    /**
     * Import Sequence Number
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    importsequencenumber?: any;

    /**
     * 系列 ID
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    seriesid?: any;

    /**
     * 是定期活动
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    regularactivity?: any;

    /**
     * 实际结束时间
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    actualend?: any;

    /**
     * 流程
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    processid?: any;

    /**
     * Required Attendees
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    requiredattendees?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    updateman?: any;

    /**
     * 上一暂候时间
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    lastonholdtime?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    updatedate?: any;

    /**
     * Exchange WebLink
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    exchangeweblink?: any;

    /**
     * 发送日期
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    senton?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    createdate?: any;

    /**
     * 失败数
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    failurecount?: any;

    /**
     * SLAName
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    slaname?: any;

    /**
     * 活动类型
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    activitytypecode?: any;

    /**
     * 成功数
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    successcount?: any;

    /**
     * CC
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    cc?: any;

    /**
     * Optional Attendees
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    optionalattendees?: any;

    /**
     * 实际开始时间
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    actualstart?: any;

    /**
     * 排序日期
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    sortdate?: any;

    /**
     * RegardingObjectTypeCode
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    regardingobjecttypecode?: any;

    /**
     * 已记帐
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    billed?: any;

    /**
     * 优先级
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    prioritycode?: any;

    /**
     * 版本号
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    versionnumber?: any;

    /**
     * 主题
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    subject?: any;

    /**
     * Resources
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    resources?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    transactioncurrencyid?: any;

    /**
     * SLA
     *
     * @returns {*}
     * @memberof BulkOperation
     */
    slaid?: any;
}
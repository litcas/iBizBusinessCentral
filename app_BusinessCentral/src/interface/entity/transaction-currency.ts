/**
 * 货币
 *
 * @export
 * @interface TransactionCurrency
 */
export interface TransactionCurrency {

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof TransactionCurrency
     */
    updatedate?: any;

    /**
     * 实体图像 ID
     *
     * @returns {*}
     * @memberof TransactionCurrency
     */
    entityimageid?: any;

    /**
     * 交易币种
     *
     * @returns {*}
     * @memberof TransactionCurrency
     */
    transactioncurrencyid?: any;

    /**
     * 版本号
     *
     * @returns {*}
     * @memberof TransactionCurrency
     */
    versionnumber?: any;

    /**
     * EntityImage_URL
     *
     * @returns {*}
     * @memberof TransactionCurrency
     */
    entityimage_url?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof TransactionCurrency
     */
    exchangerate?: any;

    /**
     * 创建记录的时间
     *
     * @returns {*}
     * @memberof TransactionCurrency
     */
    overriddencreatedon?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof TransactionCurrency
     */
    createdate?: any;

    /**
     * UniqueDscId
     *
     * @returns {*}
     * @memberof TransactionCurrency
     */
    uniquedscid?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof TransactionCurrency
     */
    statecode?: any;

    /**
     * 状态描述
     *
     * @returns {*}
     * @memberof TransactionCurrency
     */
    statuscode?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof TransactionCurrency
     */
    updateman?: any;

    /**
     * 货币精度
     *
     * @returns {*}
     * @memberof TransactionCurrency
     */
    currencyprecision?: any;

    /**
     * 货币名称
     *
     * @returns {*}
     * @memberof TransactionCurrency
     */
    currencyname?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof TransactionCurrency
     */
    createman?: any;

    /**
     * 实体图像
     *
     * @returns {*}
     * @memberof TransactionCurrency
     */
    entityimage?: any;

    /**
     * 货币代码
     *
     * @returns {*}
     * @memberof TransactionCurrency
     */
    isocurrencycode?: any;

    /**
     * EntityImage_Timestamp
     *
     * @returns {*}
     * @memberof TransactionCurrency
     */
    entityimage_timestamp?: any;

    /**
     * 货币符号
     *
     * @returns {*}
     * @memberof TransactionCurrency
     */
    currencysymbol?: any;

    /**
     * 导入序列号
     *
     * @returns {*}
     * @memberof TransactionCurrency
     */
    importsequencenumber?: any;
}
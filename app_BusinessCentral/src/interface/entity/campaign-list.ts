/**
 * 市场活动-营销列表
 *
 * @export
 * @interface CampaignList
 */
export interface CampaignList {

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof CampaignList
     */
    updatedate?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof CampaignList
     */
    updateman?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof CampaignList
     */
    createdate?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof CampaignList
     */
    createman?: any;

    /**
     * 关系类型
     *
     * @returns {*}
     * @memberof CampaignList
     */
    relationshipstype?: any;

    /**
     * 关系标识
     *
     * @returns {*}
     * @memberof CampaignList
     */
    relationshipsid?: any;

    /**
     * 关系名称
     *
     * @returns {*}
     * @memberof CampaignList
     */
    relationshipsname?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof CampaignList
     */
    entity2name?: any;

    /**
     * 活动名称
     *
     * @returns {*}
     * @memberof CampaignList
     */
    entityname?: any;

    /**
     * 列表
     *
     * @returns {*}
     * @memberof CampaignList
     */
    entity2id?: any;

    /**
     * 市场活动
     *
     * @returns {*}
     * @memberof CampaignList
     */
    entityid?: any;
}
/**
 * 市场活动项目
 *
 * @export
 * @interface CampaignActivity
 */
export interface CampaignActivity {

    /**
     * 状态描述
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    statuscode?: any;

    /**
     * 是定期活动
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    regularactivity?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    createdate?: any;

    /**
     * Record Created On
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    overriddencreatedon?: any;

    /**
     * 排序日期
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    sortdate?: any;

    /**
     * 遍历的路径
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    traversedpath?: any;

    /**
     * 活动类型
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    activitytypecode?: any;

    /**
     * Optional Attendees
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    optionalattendees?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    ownerid?: any;

    /**
     * 计划结束时间
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    scheduledend?: any;

    /**
     * Customers
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    customers?: any;

    /**
     * Organizer
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    organizer?: any;

    /**
     * 预算分配
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    budgetedcost?: any;

    /**
     * 类别
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    category?: any;

    /**
     * 渠道
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    channeltypecode?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    ownername?: any;

    /**
     * 预算分配 (Base)
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    budgetedcost_base?: any;

    /**
     * BCC
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    bcc?: any;

    /**
     * 计划持续时间
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    scheduleddurationminutes?: any;

    /**
     * 子类别
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    subcategory?: any;

    /**
     * CC
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    cc?: any;

    /**
     * 优先级
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    prioritycode?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    updateman?: any;

    /**
     * Outsource Vendors
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    partners?: any;

    /**
     * 类型
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    typecode?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    statecode?: any;

    /**
     * 实际开始时间
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    actualstart?: any;

    /**
     * SLAName
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    slaname?: any;

    /**
     * 版本号
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    versionnumber?: any;

    /**
     * 排除退出的成员
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    donotsendonoptout?: any;

    /**
     * Exchange 项目 ID
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    exchangeitemid?: any;

    /**
     * Exchange WebLink
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    exchangeweblink?: any;

    /**
     * 社交渠道
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    community?: any;

    /**
     * 主题
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    subject?: any;

    /**
     * 实际成本 (Base)
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    actualcost_base?: any;

    /**
     * To
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    to?: any;

    /**
     * 定期实例类型
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    instancetypecode?: any;

    /**
     * 上一暂候时间
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    lastonholdtime?: any;

    /**
     * 时区规则版本号
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    timezoneruleversionnumber?: any;

    /**
     * 流程阶段
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    stageid?: any;

    /**
     * 暂候时间(分钟)
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    onholdtime?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    description?: any;

    /**
     * 隐藏
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    mapiprivate?: any;

    /**
     * 活动附加参数
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    activityadditionalparams?: any;

    /**
     * 传递优先级
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    deliveryprioritycode?: any;

    /**
     * Required Attendees
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    requiredattendees?: any;

    /**
     * 服务
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    serviceid?: any;

    /**
     * 关于
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    regardingobjectname?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    exchangerate?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    updatedate?: any;

    /**
     * Import Sequence Number
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    importsequencenumber?: any;

    /**
     * 计划开始时间
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    scheduledstart?: any;

    /**
     * 实际持续时间
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    actualdurationminutes?: any;

    /**
     * 发送日期
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    senton?: any;

    /**
     * 市场活动项目
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    activityid?: any;

    /**
     * 天数
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    excludeifcontactedinxdays?: any;

    /**
     * 保留的语音邮件
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    leftvoicemail?: any;

    /**
     * 由工作流创建
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    workflowcreated?: any;

    /**
     * RegardingObjectTypeCode
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    regardingobjecttypecode?: any;

    /**
     * UTC 转换时区代码
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    utcconversiontimezonecode?: any;

    /**
     * 关于
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    regardingobjectid?: any;

    /**
     * 忽略停用市场营销列表成员
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    ignoreinactivelistmembers?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    createman?: any;

    /**
     * 来自
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    from?: any;

    /**
     * 已记帐
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    billed?: any;

    /**
     * 实际成本
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    actualcost?: any;

    /**
     * 负责人类型
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    ownertype?: any;

    /**
     * 上次尝试传递的日期
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    deliverylastattemptedon?: any;

    /**
     * 实际结束时间
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    actualend?: any;

    /**
     * 系列 ID
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    seriesid?: any;

    /**
     * 流程
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    processid?: any;

    /**
     * Resources
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    resources?: any;

    /**
     * SLA
     *
     * @returns {*}
     * @memberof CampaignActivity
     */
    slaid?: any;
}
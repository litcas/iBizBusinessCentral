/**
 * 主题
 *
 * @export
 * @interface Subject
 */
export interface Subject {

    /**
     * 创建记录的时间
     *
     * @returns {*}
     * @memberof Subject
     */
    overriddencreatedon?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof Subject
     */
    updatedate?: any;

    /**
     * 版本号
     *
     * @returns {*}
     * @memberof Subject
     */
    versionnumber?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof Subject
     */
    description?: any;

    /**
     * 导入序列号
     *
     * @returns {*}
     * @memberof Subject
     */
    importsequencenumber?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof Subject
     */
    updateman?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof Subject
     */
    createdate?: any;

    /**
     * 父主题的名称。
     *
     * @returns {*}
     * @memberof Subject
     */
    parentsubjectname?: any;

    /**
     * 功能掩码
     *
     * @returns {*}
     * @memberof Subject
     */
    featuremask?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof Subject
     */
    createman?: any;

    /**
     * 标题
     *
     * @returns {*}
     * @memberof Subject
     */
    title?: any;

    /**
     * 主题
     *
     * @returns {*}
     * @memberof Subject
     */
    subjectid?: any;

    /**
     * 父主题
     *
     * @returns {*}
     * @memberof Subject
     */
    parentsubject?: any;
}
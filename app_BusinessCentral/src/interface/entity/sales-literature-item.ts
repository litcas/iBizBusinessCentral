/**
 * 销售附件
 *
 * @export
 * @interface SalesLiteratureItem
 */
export interface SalesLiteratureItem {

    /**
     * 作者姓名
     *
     * @returns {*}
     * @memberof SalesLiteratureItem
     */
    authorname?: any;

    /**
     * Time Zone Rule Version Number
     *
     * @returns {*}
     * @memberof SalesLiteratureItem
     */
    timezoneruleversionnumber?: any;

    /**
     * 标题
     *
     * @returns {*}
     * @memberof SalesLiteratureItem
     */
    title?: any;

    /**
     * 文件大小(字节)
     *
     * @returns {*}
     * @memberof SalesLiteratureItem
     */
    filesize?: any;

    /**
     * Mode
     *
     * @returns {*}
     * @memberof SalesLiteratureItem
     */
    mode?: any;

    /**
     * 文件类型
     *
     * @returns {*}
     * @memberof SalesLiteratureItem
     */
    filetypecode?: any;

    /**
     * Import Sequence Number
     *
     * @returns {*}
     * @memberof SalesLiteratureItem
     */
    importsequencenumber?: any;

    /**
     * 销售宣传资料项
     *
     * @returns {*}
     * @memberof SalesLiteratureItem
     */
    salesliteratureitemid?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof SalesLiteratureItem
     */
    updatedate?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof SalesLiteratureItem
     */
    updateman?: any;

    /**
     * MIME 类型
     *
     * @returns {*}
     * @memberof SalesLiteratureItem
     */
    mimetype?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof SalesLiteratureItem
     */
    createman?: any;

    /**
     * 关键字
     *
     * @returns {*}
     * @memberof SalesLiteratureItem
     */
    keywords?: any;

    /**
     * FileType
     *
     * @returns {*}
     * @memberof SalesLiteratureItem
     */
    filetype?: any;

    /**
     * 附加文档 URL
     *
     * @returns {*}
     * @memberof SalesLiteratureItem
     */
    attacheddocumenturl?: any;

    /**
     * 显示销售宣传资料文档附件的编码内容。
     *
     * @returns {*}
     * @memberof SalesLiteratureItem
     */
    documentbody?: any;

    /**
     * Version Number
     *
     * @returns {*}
     * @memberof SalesLiteratureItem
     */
    versionnumber?: any;

    /**
     * 是否抽象
     *
     * @returns {*}
     * @memberof SalesLiteratureItem
     */
    ibizabstract?: any;

    /**
     * Record Created On
     *
     * @returns {*}
     * @memberof SalesLiteratureItem
     */
    overriddencreatedon?: any;

    /**
     * 向客户公开
     *
     * @returns {*}
     * @memberof SalesLiteratureItem
     */
    customerviewable?: any;

    /**
     * 文件名
     *
     * @returns {*}
     * @memberof SalesLiteratureItem
     */
    filename?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof SalesLiteratureItem
     */
    organizationid?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof SalesLiteratureItem
     */
    createdate?: any;

    /**
     * UTC Conversion Time Zone Code
     *
     * @returns {*}
     * @memberof SalesLiteratureItem
     */
    utcconversiontimezonecode?: any;

    /**
     * 销售宣传资料
     *
     * @returns {*}
     * @memberof SalesLiteratureItem
     */
    salesliteratureid?: any;
}
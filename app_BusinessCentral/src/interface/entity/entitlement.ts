/**
 * 权利
 *
 * @export
 * @interface Entitlement
 */
export interface Entitlement {

    /**
     * 开始日期
     *
     * @returns {*}
     * @memberof Entitlement
     */
    startdate?: any;

    /**
     * Process Id
     *
     * @returns {*}
     * @memberof Entitlement
     */
    processid?: any;

    /**
     * Traversed Path
     *
     * @returns {*}
     * @memberof Entitlement
     */
    traversedpath?: any;

    /**
     * 缩短剩余期限
     *
     * @returns {*}
     * @memberof Entitlement
     */
    decreaseremainingon?: any;

    /**
     * entitytype
     *
     * @returns {*}
     * @memberof Entitlement
     */
    entitytype?: any;

    /**
     * 联系人
     *
     * @returns {*}
     * @memberof Entitlement
     */
    contactname?: any;

    /**
     * Import Sequence Number
     *
     * @returns {*}
     * @memberof Entitlement
     */
    importsequencenumber?: any;

    /**
     * 结束日期
     *
     * @returns {*}
     * @memberof Entitlement
     */
    enddate?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof Entitlement
     */
    statecode?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof Entitlement
     */
    createman?: any;

    /**
     * 客户
     *
     * @returns {*}
     * @memberof Entitlement
     */
    customername?: any;

    /**
     * KB 访问级别
     *
     * @returns {*}
     * @memberof Entitlement
     */
    kbaccesslevel?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof Entitlement
     */
    description?: any;

    /**
     * Email Address
     *
     * @returns {*}
     * @memberof Entitlement
     */
    emailaddress?: any;

    /**
     * Time Zone Rule Version Number
     *
     * @returns {*}
     * @memberof Entitlement
     */
    timezoneruleversionnumber?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof Entitlement
     */
    exchangerate?: any;

    /**
     * 客户类型
     *
     * @returns {*}
     * @memberof Entitlement
     */
    customertype?: any;

    /**
     * Version Number
     *
     * @returns {*}
     * @memberof Entitlement
     */
    versionnumber?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Entitlement
     */
    ownerid?: any;

    /**
     * UTC Conversion Time Zone Code
     *
     * @returns {*}
     * @memberof Entitlement
     */
    utcconversiontimezonecode?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof Entitlement
     */
    createdate?: any;

    /**
     * 分配类型
     *
     * @returns {*}
     * @memberof Entitlement
     */
    allocationtypecode?: any;

    /**
     * 状态代码
     *
     * @returns {*}
     * @memberof Entitlement
     */
    statuscode?: any;

    /**
     * 总期数
     *
     * @returns {*}
     * @memberof Entitlement
     */
    totalterms?: any;

    /**
     * 客户
     *
     * @returns {*}
     * @memberof Entitlement
     */
    customerid?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Entitlement
     */
    ownername?: any;

    /**
     * 根据权利期限进行限制
     *
     * @returns {*}
     * @memberof Entitlement
     */
    restrictcasecreation?: any;

    /**
     * 权利
     *
     * @returns {*}
     * @memberof Entitlement
     */
    entitlementid?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof Entitlement
     */
    updateman?: any;

    /**
     * 剩余期数
     *
     * @returns {*}
     * @memberof Entitlement
     */
    remainingterms?: any;

    /**
     * 客户
     *
     * @returns {*}
     * @memberof Entitlement
     */
    accountname?: any;

    /**
     * Record Created On
     *
     * @returns {*}
     * @memberof Entitlement
     */
    overriddencreatedon?: any;

    /**
     * 负责人类型
     *
     * @returns {*}
     * @memberof Entitlement
     */
    ownertype?: any;

    /**
     * Stage Id
     *
     * @returns {*}
     * @memberof Entitlement
     */
    stageid?: any;

    /**
     * 授权名称
     *
     * @returns {*}
     * @memberof Entitlement
     */
    entitlementname?: any;

    /**
     * 是否默认
     *
     * @returns {*}
     * @memberof Entitlement
     */
    ibizdefault?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof Entitlement
     */
    updatedate?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof Entitlement
     */
    transactioncurrencyid?: any;

    /**
     * SLA
     *
     * @returns {*}
     * @memberof Entitlement
     */
    slaid?: any;

    /**
     * 权利模板
     *
     * @returns {*}
     * @memberof Entitlement
     */
    entitlementtemplateid?: any;
}
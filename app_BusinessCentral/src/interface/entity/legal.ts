/**
 * 法人
 *
 * @export
 * @interface Legal
 */
export interface Legal {

    /**
     * 法人名称
     *
     * @returns {*}
     * @memberof Legal
     */
    legalname?: any;

    /**
     * 组织类型
     *
     * @returns {*}
     * @memberof Legal
     */
    organizationtype?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof Legal
     */
    createman?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof Legal
     */
    updateman?: any;

    /**
     * 法人标识
     *
     * @returns {*}
     * @memberof Legal
     */
    legalid?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof Legal
     */
    createdate?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof Legal
     */
    updatedate?: any;

    /**
     * 组织编码
     *
     * @returns {*}
     * @memberof Legal
     */
    orgcode?: any;

    /**
     * 组织层级
     *
     * @returns {*}
     * @memberof Legal
     */
    orglevel?: any;

    /**
     * 排序号
     *
     * @returns {*}
     * @memberof Legal
     */
    showorder?: any;

    /**
     * 组织简称
     *
     * @returns {*}
     * @memberof Legal
     */
    shortname?: any;
}
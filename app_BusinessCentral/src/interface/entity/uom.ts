/**
 * 计价单位
 *
 * @export
 * @interface Uom
 */
export interface Uom {

    /**
     * Record Created On
     *
     * @returns {*}
     * @memberof Uom
     */
    overriddencreatedon?: any;

    /**
     * BaseUoMName
     *
     * @returns {*}
     * @memberof Uom
     */
    baseuomname?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof Uom
     */
    updateman?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof Uom
     */
    createman?: any;

    /**
     * Version Number
     *
     * @returns {*}
     * @memberof Uom
     */
    versionnumber?: any;

    /**
     * 数量
     *
     * @returns {*}
     * @memberof Uom
     */
    quantity?: any;

    /**
     * 计价单位
     *
     * @returns {*}
     * @memberof Uom
     */
    uomid?: any;

    /**
     * 为计划基础单位
     *
     * @returns {*}
     * @memberof Uom
     */
    schedulebaseuom?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof Uom
     */
    organizationid?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof Uom
     */
    createdate?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof Uom
     */
    updatedate?: any;

    /**
     * 计量单位名称
     *
     * @returns {*}
     * @memberof Uom
     */
    uomname?: any;

    /**
     * UTC Conversion Time Zone Code
     *
     * @returns {*}
     * @memberof Uom
     */
    utcconversiontimezonecode?: any;

    /**
     * Time Zone Rule Version Number
     *
     * @returns {*}
     * @memberof Uom
     */
    timezoneruleversionnumber?: any;

    /**
     * Import Sequence Number
     *
     * @returns {*}
     * @memberof Uom
     */
    importsequencenumber?: any;

    /**
     * 单位计划
     *
     * @returns {*}
     * @memberof Uom
     */
    uomscheduleid?: any;

    /**
     * 基础单位
     *
     * @returns {*}
     * @memberof Uom
     */
    baseuom?: any;
}
/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof MainModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'opportunityid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'opportunityname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'opportunityname',
        prop: 'opportunityname',
        dataType: 'TEXT',
      },
      {
        name: 'parentcontactname',
        prop: 'parentcontactname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'parentaccountname',
        prop: 'parentaccountname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'purchasetimeframe',
        prop: 'purchasetimeframe',
        dataType: 'SSCODELIST',
      },
      {
        name: 'transactioncurrencyname',
        prop: 'currencyname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'budgetamount',
        prop: 'budgetamount',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'purchaseprocess',
        prop: 'purchaseprocess',
        dataType: 'SSCODELIST',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'currentsituation',
        prop: 'currentsituation',
        dataType: 'TEXT',
      },
      {
        name: 'customerneed',
        prop: 'customerneed',
        dataType: 'TEXT',
      },
      {
        name: 'proposedsolution',
        prop: 'proposedsolution',
        dataType: 'TEXT',
      },
      {
        name: 'pricelevelname',
        prop: 'pricelevelname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'parentcontactid',
        prop: 'parentcontactid',
        dataType: 'PICKUP',
      },
      {
        name: 'parentaccountid',
        prop: 'parentaccountid',
        dataType: 'PICKUP',
      },
      {
        name: 'transactioncurrencyid',
        prop: 'transactioncurrencyid',
        dataType: 'PICKUP',
      },
      {
        name: 'opportunityid',
        prop: 'opportunityid',
        dataType: 'GUID',
      },
      {
        name: 'pricelevelid',
        prop: 'pricelevelid',
        dataType: 'PICKUP',
      },
      {
        name: 'opportunity',
        prop: 'opportunityid',
        dataType: 'FONTKEY',
      },
    ]
  }

}
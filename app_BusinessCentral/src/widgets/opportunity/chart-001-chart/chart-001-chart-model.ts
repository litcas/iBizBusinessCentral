/**
 * CHART_001 部件模型
 *
 * @export
 * @class CHART_001Model
 */
export default class CHART_001Model {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof CHART_001Db_sysportlet1_chartMode
	 */
	public getDataItems(): any[] {
		return [
			{
			name:'size',
			prop:'size'
			},
			{
			name:'query',
			prop:'query'
			},
			{
			name:'page',
			prop:'page'
			},
			{
			name:'sort',
			prop:'sort'
			}
		]
	}

}
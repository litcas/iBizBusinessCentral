/**
 * Infotabviewpanel3 部件模型
 *
 * @export
 * @class Infotabviewpanel3Model
 */
export default class Infotabviewpanel3Model {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof Infotabviewpanel3Model
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'decisionmaker',
      },
      {
        name: 'totallineitemamount',
      },
      {
        name: 'processid',
      },
      {
        name: 'discountpercentage',
      },
      {
        name: 'ownertype',
      },
      {
        name: 'pricingerrorcode',
      },
      {
        name: 'totalamount',
      },
      {
        name: 'totaltax_base',
      },
      {
        name: 'totaltax',
      },
      {
        name: 'updateman',
      },
      {
        name: 'estimatedvalue_base',
      },
      {
        name: 'ownername',
      },
      {
        name: 'presentfinalproposal',
      },
      {
        name: 'developproposal',
      },
      {
        name: 'opportunityratingcode',
      },
      {
        name: 'totalamount_base',
      },
      {
        name: 'totalamountlessfreight',
      },
      {
        name: 'description',
      },
      {
        name: 'identifycompetitors',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'sendthankyounote',
      },
      {
        name: 'totaldiscountamount_base',
      },
      {
        name: 'totallineitemdiscountamount',
      },
      {
        name: 'purchaseprocess',
      },
      {
        name: 'traversedpath',
      },
      {
        name: 'captureproposalfeedback',
      },
      {
        name: 'finaldecisiondate',
      },
      {
        name: 'stepid',
      },
      {
        name: 'resolvefeedback',
      },
      {
        name: 'budgetamount_base',
      },
      {
        name: 'pursuitdecision',
      },
      {
        name: 'revenuesystemcalculated',
      },
      {
        name: 'scheduleproposalmeeting',
      },
      {
        name: 'customertype',
      },
      {
        name: 'closeprobability',
      },
      {
        name: 'utcconversiontimezonecode',
      },
      {
        name: 'presentproposal',
      },
      {
        name: 'schedulefollowup_prospect',
      },
      {
        name: 'statuscode',
      },
      {
        name: 'actualclosedate',
      },
      {
        name: 'confirminterest',
      },
      {
        name: 'customerid',
      },
      {
        name: 'salesstage',
      },
      {
        name: 'opportunity',
        prop: 'opportunityid',
      },
      {
        name: 'onholdtime',
      },
      {
        name: 'qualificationcomments',
      },
      {
        name: 'schedulefollowup_qualify',
      },
      {
        name: 'participatesinworkflow',
      },
      {
        name: 'exchangerate',
      },
      {
        name: 'stepname',
      },
      {
        name: 'customerneed',
      },
      {
        name: 'importsequencenumber',
      },
      {
        name: 'completeinternalreview',
      },
      {
        name: 'evaluatefit',
      },
      {
        name: 'salesstagecode',
      },
      {
        name: 'identifypursuitteam',
      },
      {
        name: 'initialcommunication',
      },
      {
        name: 'versionnumber',
      },
      {
        name: 'freightamount_base',
      },
      {
        name: 'accountname',
      },
      {
        name: 'teamsfollowed',
      },
      {
        name: 'proposedsolution',
      },
      {
        name: 'filedebrief',
      },
      {
        name: 'budgetamount',
      },
      {
        name: 'budgetstatus',
      },
      {
        name: 'discountamount_base',
      },
      {
        name: 'completefinalproposal',
      },
      {
        name: 'discountamount',
      },
      {
        name: 'need',
      },
      {
        name: 'stageid',
      },
      {
        name: 'freightamount',
      },
      {
        name: 'actualvalue',
      },
      {
        name: 'ibizprivate',
      },
      {
        name: 'timezoneruleversionnumber',
      },
      {
        name: 'createdate',
      },
      {
        name: 'prioritycode',
      },
      {
        name: 'totallineitemamount_base',
      },
      {
        name: 'customername',
      },
      {
        name: 'estimatedclosedate',
      },
      {
        name: 'currentsituation',
      },
      {
        name: 'actualvalue_base',
      },
      {
        name: 'customerpainpoints',
      },
      {
        name: 'totaldiscountamount',
      },
      {
        name: 'opportunityname',
      },
      {
        name: 'identifycustomercontacts',
      },
      {
        name: 'timeline',
      },
      {
        name: 'contactname',
      },
      {
        name: 'lastonholdtime',
      },
      {
        name: 'totalamountlessfreight_base',
      },
      {
        name: 'estimatedvalue',
      },
      {
        name: 'emailaddress',
      },
      {
        name: 'statecode',
      },
      {
        name: 'purchasetimeframe',
      },
      {
        name: 'createman',
      },
      {
        name: 'quotecomments',
      },
      {
        name: 'overriddencreatedon',
      },
      {
        name: 'ownerid',
      },
      {
        name: 'parentaccountname',
      },
      {
        name: 'originatingleadname',
      },
      {
        name: 'parentcontactname',
      },
      {
        name: 'currencyname',
      },
      {
        name: 'pricelevelname',
      },
      {
        name: 'slaname',
      },
      {
        name: 'campaignname',
      },
      {
        name: 'parentcontactid',
      },
      {
        name: 'campaignid',
      },
      {
        name: 'transactioncurrencyid',
      },
      {
        name: 'parentaccountid',
      },
      {
        name: 'pricelevelid',
      },
      {
        name: 'originatingleadid',
      },
      {
        name: 'slaid',
      },
    ]
  }


}
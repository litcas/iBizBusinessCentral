import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, MainControlBase } from '@/studio-core';
import OpportunityService from '@/service/opportunity/opportunity-service';
import PART_CHT_001Service from './part-cht-001-portlet-service';
import OpportunityUIService from '@/uiservice/opportunity/opportunity-ui-service';
import { Environment } from '@/environments/environment';


/**
 * db_sysportlet1部件基类
 *
 * @export
 * @class MainControlBase
 * @extends {PART_CHT_001PortletBase}
 */
export class PART_CHT_001PortletBase extends MainControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof PART_CHT_001PortletBase
     */
    protected controlType: string = 'PORTLET';

    /**
     * 建构部件服务对象
     *
     * @type {PART_CHT_001Service}
     * @memberof PART_CHT_001PortletBase
     */
    public service: PART_CHT_001Service = new PART_CHT_001Service({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {OpportunityService}
     * @memberof PART_CHT_001PortletBase
     */
    public appEntityService: OpportunityService = new OpportunityService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof PART_CHT_001PortletBase
     */
    protected appDeName: string = 'opportunity';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof PART_CHT_001PortletBase
     */
    protected appDeLogicName: string = '商机';

    /**
     * 界面UI服务对象
     *
     * @type {OpportunityUIService}
     * @memberof PART_CHT_001Base
     */  
    public appUIService:OpportunityUIService = new OpportunityUIService(this.$store);

    /**
     * 长度
     *
     * @type {number}
     * @memberof PART_CHT_001
     */
    @Prop() public height?: number;

    /**
     * 宽度
     *
     * @type {number}
     * @memberof PART_CHT_001
     */
    @Prop() public width?: number;



    /**
     * 是否自适应大小
     *
     * @returns {boolean}
     * @memberof PART_CHT_001Base
     */
    @Prop({default: false})public isAdaptiveSize!: boolean;

    /**
     * 获取多项数据
     *
     * @returns {any[]}
     * @memberof PART_CHT_001Base
     */
    public getDatas(): any[] {
        return [];
    }

    /**
     * 获取单项树
     *
     * @returns {*}
     * @memberof PART_CHT_001Base
     */
    public getData(): any {
        return {};
    }

    /**
     * 获取高度
     *
     * @returns {any[]}
     * @memberof PART_CHT_001Base
     */
    get getHeight(): any{
        if(!this.$util.isEmpty(this.height) && !this.$util.isNumberNaN(this.height)){
            if(this.height == 0){
                return 'auto';
            } else {
                return this.height+'px';
            }
        } else {
            return '300px';
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof PART_CHT_001Base
     */
    public created() {
        this.afterCreated();
    }

    /**
     * 执行created后的逻辑
     *
     *  @memberof PART_CHT_001Base
     */    
    public afterCreated(){
        if (this.viewState) {
            this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }) => {
                if (!Object.is(tag, this.name)) {
                    return;
                }
                const refs: any = this.$refs;
                Object.keys(refs).forEach((_name: string) => {
                    this.viewState.next({ tag: _name, action: action, data: data });
                });
            });
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof PART_CHT_001Base
     */
    public destroyed() {
        this.afterDestroy();
    }

    /**
     * 执行destroyed后的逻辑
     *
     * @memberof PART_CHT_001Base
     */
    public afterDestroy() {
        if (this.viewStateEvent) {
            this.viewStateEvent.unsubscribe();
        }
    }


}

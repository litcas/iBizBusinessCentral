/**
 * AbstractInfo 部件模型
 *
 * @export
 * @class AbstractInfoModel
 */
export default class AbstractInfoModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof AbstractInfoModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'opportunityid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'opportunityname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'opportunityname',
        prop: 'opportunityname',
        dataType: 'TEXT',
      },
      {
        name: 'parentcontactname',
        prop: 'parentcontactname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'parentaccountname',
        prop: 'parentaccountname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'purchasetimeframe',
        prop: 'purchasetimeframe',
        dataType: 'SSCODELIST',
      },
      {
        name: 'transactioncurrencyname',
        prop: 'currencyname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'budgetamount',
        prop: 'budgetamount',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'purchaseprocess',
        prop: 'purchaseprocess',
        dataType: 'SSCODELIST',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'pricelevelname',
        prop: 'pricelevelname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'currentsituation',
        prop: 'currentsituation',
        dataType: 'TEXT',
      },
      {
        name: 'customerneed',
        prop: 'customerneed',
        dataType: 'TEXT',
      },
      {
        name: 'proposedsolution',
        prop: 'proposedsolution',
        dataType: 'TEXT',
      },
      {
        name: 'parentcontactid',
        prop: 'parentcontactid',
        dataType: 'PICKUP',
      },
      {
        name: 'parentaccountid',
        prop: 'parentaccountid',
        dataType: 'PICKUP',
      },
      {
        name: 'opportunityid',
        prop: 'opportunityid',
        dataType: 'GUID',
      },
      {
        name: 'opportunity',
        prop: 'opportunityid',
        dataType: 'FONTKEY',
      },
    ]
  }

}
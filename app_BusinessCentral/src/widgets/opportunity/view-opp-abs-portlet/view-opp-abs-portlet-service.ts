import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * View_OppAbs 部件服务对象
 *
 * @export
 * @class View_OppAbsService
 */
export default class View_OppAbsService extends ControlService {
}

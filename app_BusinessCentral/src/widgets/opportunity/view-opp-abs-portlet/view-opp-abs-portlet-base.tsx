import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, MainControlBase } from '@/studio-core';
import OpportunityService from '@/service/opportunity/opportunity-service';
import View_OppAbsService from './view-opp-abs-portlet-service';
import OpportunityUIService from '@/uiservice/opportunity/opportunity-ui-service';
import { Environment } from '@/environments/environment';


/**
 * dashboard_sysportlet1部件基类
 *
 * @export
 * @class MainControlBase
 * @extends {View_OppAbsPortletBase}
 */
export class View_OppAbsPortletBase extends MainControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof View_OppAbsPortletBase
     */
    protected controlType: string = 'PORTLET';

    /**
     * 建构部件服务对象
     *
     * @type {View_OppAbsService}
     * @memberof View_OppAbsPortletBase
     */
    public service: View_OppAbsService = new View_OppAbsService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {OpportunityService}
     * @memberof View_OppAbsPortletBase
     */
    public appEntityService: OpportunityService = new OpportunityService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof View_OppAbsPortletBase
     */
    protected appDeName: string = 'opportunity';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof View_OppAbsPortletBase
     */
    protected appDeLogicName: string = '商机';

    /**
     * 界面UI服务对象
     *
     * @type {OpportunityUIService}
     * @memberof View_OppAbsBase
     */  
    public appUIService:OpportunityUIService = new OpportunityUIService(this.$store);

    /**
     * 长度
     *
     * @type {number}
     * @memberof View_OppAbs
     */
    @Prop() public height?: number;

    /**
     * 宽度
     *
     * @type {number}
     * @memberof View_OppAbs
     */
    @Prop() public width?: number;



    /**
     * 是否自适应大小
     *
     * @returns {boolean}
     * @memberof View_OppAbsBase
     */
    @Prop({default: false})public isAdaptiveSize!: boolean;

    /**
     * 获取多项数据
     *
     * @returns {any[]}
     * @memberof View_OppAbsBase
     */
    public getDatas(): any[] {
        return [];
    }

    /**
     * 获取单项树
     *
     * @returns {*}
     * @memberof View_OppAbsBase
     */
    public getData(): any {
        return {};
    }

    /**
     * 获取高度
     *
     * @returns {any[]}
     * @memberof View_OppAbsBase
     */
    get getHeight(): any{
        if(!this.$util.isEmpty(this.height) && !this.$util.isNumberNaN(this.height)){
            if(this.height == 0){
                return 'auto';
            } else {
                return this.height+'px';
            }
        } else {
            return 'auto';
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof View_OppAbsBase
     */
    public created() {
        this.afterCreated();
    }

    /**
     * 执行created后的逻辑
     *
     *  @memberof View_OppAbsBase
     */    
    public afterCreated(){
        if (this.viewState) {
            this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }) => {
                if (!Object.is(tag, this.name)) {
                    return;
                }
                const refs: any = this.$refs;
                Object.keys(refs).forEach((_name: string) => {
                    this.viewState.next({ tag: _name, action: action, data: data });
                });
            });
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof View_OppAbsBase
     */
    public destroyed() {
        this.afterDestroy();
    }

    /**
     * 执行destroyed后的逻辑
     *
     * @memberof View_OppAbsBase
     */
    public afterDestroy() {
        if (this.viewStateEvent) {
            this.viewStateEvent.unsubscribe();
        }
    }


}

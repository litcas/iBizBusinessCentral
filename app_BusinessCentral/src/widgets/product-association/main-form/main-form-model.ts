/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof MainModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'productassociationid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'productname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'productid',
        prop: 'productid',
        dataType: 'PICKUP',
      },
      {
        name: 'associatedproductname',
        prop: 'associatedproductname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'quantity',
        prop: 'quantity',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'uomname',
        prop: 'uomname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'associatedproduct',
        prop: 'associatedproduct',
        dataType: 'PICKUP',
      },
      {
        name: 'productassociationid',
        prop: 'productassociationid',
        dataType: 'GUID',
      },
      {
        name: 'uomid',
        prop: 'uomid',
        dataType: 'PICKUP',
      },
      {
        name: 'productassociation',
        prop: 'productassociationid',
        dataType: 'FONTKEY',
      },
    ]
  }

}
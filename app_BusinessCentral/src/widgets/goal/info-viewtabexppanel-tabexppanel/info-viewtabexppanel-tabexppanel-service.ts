import { Http,Util,Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import GoalService from '@/service/goal/goal-service';
import InfoViewtabexppanelModel from './info-viewtabexppanel-tabexppanel-model';


/**
 * InfoViewtabexppanel 部件服务对象
 *
 * @export
 * @class InfoViewtabexppanelService
 */
export default class InfoViewtabexppanelService extends ControlService {

    /**
     * 目标服务对象
     *
     * @type {GoalService}
     * @memberof InfoViewtabexppanelService
     */
    public appEntityService: GoalService = new GoalService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof InfoViewtabexppanelService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of InfoViewtabexppanelService.
     * 
     * @param {*} [opts={}]
     * @memberof InfoViewtabexppanelService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new InfoViewtabexppanelModel();
    }

    
}
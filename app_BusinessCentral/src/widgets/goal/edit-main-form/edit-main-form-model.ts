/**
 * Edit_Main 部件模型
 *
 * @export
 * @class Edit_MainModel
 */
export default class Edit_MainModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Edit_MainModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'goalid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'title',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'title',
        prop: 'title',
        dataType: 'TEXT',
      },
      {
        name: 'parentgoalname',
        prop: 'parentgoalname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'metricname',
        prop: 'metricname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'goalownerid',
        prop: 'goalownerid',
        dataType: 'TEXT',
      },
      {
        name: 'ownerid',
        prop: 'ownerid',
        dataType: 'TEXT',
      },
      {
        name: 'fiscalperiodgoal',
        prop: 'fiscalperiodgoal',
        dataType: 'YESNO',
      },
      {
        name: 'fiscalyear',
        prop: 'fiscalyear',
        dataType: 'SSCODELIST',
      },
      {
        name: 'fiscalperiod',
        prop: 'fiscalperiod',
        dataType: 'SSCODELIST',
      },
      {
        name: 'goalstartdate',
        prop: 'goalstartdate',
        dataType: 'DATETIME',
      },
      {
        name: 'goalenddate',
        prop: 'goalenddate',
        dataType: 'DATETIME',
      },
      {
        name: 'targetmoney',
        prop: 'targetmoney',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'actualmoney',
        prop: 'actualmoney',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'percentage',
        prop: 'percentage',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'inprogressmoney',
        prop: 'inprogressmoney',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'parentgoalid',
        prop: 'parentgoalid',
        dataType: 'PICKUP',
      },
      {
        name: 'goalid',
        prop: 'goalid',
        dataType: 'GUID',
      },
      {
        name: 'metricid',
        prop: 'metricid',
        dataType: 'PICKUP',
      },
      {
        name: 'goal',
        prop: 'goalid',
        dataType: 'FONTKEY',
      },
    ]
  }

}
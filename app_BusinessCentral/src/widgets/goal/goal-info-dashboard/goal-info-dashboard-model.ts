/**
 * GoalInfo 部件模型
 *
 * @export
 * @class GoalInfoModel
 */
export default class GoalInfoModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof GoalInfoModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'inprogressdecimal',
      },
      {
        name: 'goal',
        prop: 'goalid',
      },
      {
        name: 'ownertype',
      },
      {
        name: 'stretchtargetmoney',
      },
      {
        name: 'actualdecimal',
      },
      {
        name: 'computedtargetasoftodayinteger',
      },
      {
        name: 'timezoneruleversionnumber',
      },
      {
        name: 'exchangerate',
      },
      {
        name: 'entityimageid',
      },
      {
        name: 'targetinteger',
      },
      {
        name: 'customrollupfieldmoney_base',
      },
      {
        name: 'entityimage_url',
      },
      {
        name: 'actualmoney_base',
      },
      {
        name: 'utcconversiontimezonecode',
      },
      {
        name: 'inprogressinteger',
      },
      {
        name: 'statecode',
      },
      {
        name: 'targetmoney_base',
      },
      {
        name: 'inprogressstring',
      },
      {
        name: 'entityimage_timestamp',
      },
      {
        name: 'goalstartdate',
      },
      {
        name: 'percentage',
      },
      {
        name: 'ownerid',
      },
      {
        name: 'targetdecimal',
      },
      {
        name: 'amount',
      },
      {
        name: 'statuscode',
      },
      {
        name: 'amountdatatype',
      },
      {
        name: 'customrollupfieldinteger',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'goalownername',
      },
      {
        name: 'inprogressmoney',
      },
      {
        name: 'fiscalyear',
      },
      {
        name: 'fiscalperiod',
      },
      {
        name: 'title',
      },
      {
        name: 'stretchtargetdecimal',
      },
      {
        name: 'customrollupfielddecimal',
      },
      {
        name: 'consideronlygoalownersrecords',
      },
      {
        name: 'overriddencreatedon',
      },
      {
        name: 'actualmoney',
      },
      {
        name: 'ownername',
      },
      {
        name: 'rolluponlyfromchildgoals',
      },
      {
        name: 'goalenddate',
      },
      {
        name: 'createdate',
      },
      {
        name: 'customrollupfieldmoney',
      },
      {
        name: 'rolluperrorcode',
      },
      {
        name: 'versionnumber',
      },
      {
        name: 'stretchtargetmoney_base',
      },
      {
        name: 'treeid',
      },
      {
        name: 'goalownertype',
      },
      {
        name: 'entityimage',
      },
      {
        name: 'stretchtargetstring',
      },
      {
        name: 'stretchtargetinteger',
      },
      {
        name: 'fiscalperiodgoal',
      },
      {
        name: 'inprogressmoney_base',
      },
      {
        name: 'overridden',
      },
      {
        name: 'actualstring',
      },
      {
        name: 'actualinteger',
      },
      {
        name: 'importsequencenumber',
      },
      {
        name: 'goalownerid',
      },
      {
        name: 'computedtargetasoftodaydecimal',
      },
      {
        name: 'createman',
      },
      {
        name: 'targetstring',
      },
      {
        name: 'customrollupfieldstring',
      },
      {
        name: 'targetmoney',
      },
      {
        name: 'lastrolledupdate',
      },
      {
        name: 'updateman',
      },
      {
        name: 'override',
      },
      {
        name: 'computedtargetasoftodaymoney',
      },
      {
        name: 'metricname',
      },
      {
        name: 'parentgoalname',
      },
      {
        name: 'parentgoalid',
      },
      {
        name: 'rollupqueryinprogressmoneyid',
      },
      {
        name: 'rollupquerycustommoneyid',
      },
      {
        name: 'rollupqueryinprogressintegerid',
      },
      {
        name: 'rollupquerycustomdecimalid',
      },
      {
        name: 'rollupqueryactualmoneyid',
      },
      {
        name: 'rollupquerycustomintegerid',
      },
      {
        name: 'goalwitherrorid',
      },
      {
        name: 'metricid',
      },
      {
        name: 'rollupqueryinprogressdecimalid',
      },
      {
        name: 'rollupqueryactualintegerid',
      },
      {
        name: 'rollupqueryactualdecimalid',
      },
    ]
  }


}
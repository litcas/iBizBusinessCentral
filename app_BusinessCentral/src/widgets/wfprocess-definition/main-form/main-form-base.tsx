import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, EditFormControlBase } from '@/studio-core';
import WFProcessDefinitionService from '@/service/wfprocess-definition/wfprocess-definition-service';
import MainService from './main-form-service';
import WFProcessDefinitionUIService from '@/uiservice/wfprocess-definition/wfprocess-definition-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {MainEditFormBase}
 */
export class MainEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof MainEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {MainService}
     * @memberof MainEditFormBase
     */
    public service: MainService = new MainService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {WFProcessDefinitionService}
     * @memberof MainEditFormBase
     */
    public appEntityService: WFProcessDefinitionService = new WFProcessDefinitionService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MainEditFormBase
     */
    protected appDeName: string = 'wfprocessdefinition';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof MainEditFormBase
     */
    protected appDeLogicName: string = '流程定义';

    /**
     * 界面UI服务对象
     *
     * @type {WFProcessDefinitionUIService}
     * @memberof MainBase
     */  
    public appUIService:WFProcessDefinitionUIService = new WFProcessDefinitionUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof MainEditFormBase
     */
    public data: any = {
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        definitionkey: null,
        deploykey: null,
        definitionname: null,
        pssystemid: null,
        modelversion: null,
        modelenable: null,
        bpmnfile: null,
        md5check: null,
        wfprocessdefinition:null,
    };

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof MainEditFormBase
     */
    public rules: any = {
        deploykey: [
            { required: true, type: 'string', message: 'DefinitionKey 值不能为空', trigger: 'change' },
            { required: true, type: 'string', message: 'DefinitionKey 值不能为空', trigger: 'blur' },
        ],
        definitionname: [
            { required: true, type: 'string', message: '流程定义名称 值不能为空', trigger: 'change' },
            { required: true, type: 'string', message: '流程定义名称 值不能为空', trigger: 'blur' },
        ],
        bpmnfile: [
            { required: true, type: 'string', message: 'BPMN 值不能为空', trigger: 'change' },
            { required: true, type: 'string', message: 'BPMN 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof MainBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof MainEditFormBase
     */
    public detailsModel: any = {
        group1: new FormGroupPanelModel({ caption: '模型基本信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.wfprocessdefinition.main_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: 'DefinitionKey', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfmajortext: new FormItemModel({ caption: '流程定义名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        definitionkey: new FormItemModel({ caption: 'DefinitionKey', detailType: 'FORMITEM', name: 'definitionkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        deploykey: new FormItemModel({ caption: 'DefinitionKey', detailType: 'FORMITEM', name: 'deploykey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 1 }),

        definitionname: new FormItemModel({ caption: '流程定义名称', detailType: 'FORMITEM', name: 'definitionname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        pssystemid: new FormItemModel({ caption: '系统标识', detailType: 'FORMITEM', name: 'pssystemid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        modelversion: new FormItemModel({ caption: '模型版本', detailType: 'FORMITEM', name: 'modelversion', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        modelenable: new FormItemModel({ caption: '模型是否启用', detailType: 'FORMITEM', name: 'modelenable', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        bpmnfile: new FormItemModel({ caption: 'BPMN', detailType: 'FORMITEM', name: 'bpmnfile', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        md5check: new FormItemModel({ caption: '校验', detailType: 'FORMITEM', name: 'md5check', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

    };
}
import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, SearchFormControlBase } from '@/studio-core';
import WFProcessDefinitionService from '@/service/wfprocess-definition/wfprocess-definition-service';
import DefaultService from './default-searchform-service';
import WFProcessDefinitionUIService from '@/uiservice/wfprocess-definition/wfprocess-definition-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';


/**
 * searchform部件基类
 *
 * @export
 * @class SearchFormControlBase
 * @extends {DefaultSearchFormBase}
 */
export class DefaultSearchFormBase extends SearchFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected controlType: string = 'SEARCHFORM';

    /**
     * 建构部件服务对象
     *
     * @type {DefaultService}
     * @memberof DefaultSearchFormBase
     */
    public service: DefaultService = new DefaultService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {WFProcessDefinitionService}
     * @memberof DefaultSearchFormBase
     */
    public appEntityService: WFProcessDefinitionService = new WFProcessDefinitionService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeName: string = 'wfprocessdefinition';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeLogicName: string = '流程定义';

    /**
     * 界面UI服务对象
     *
     * @type {WFProcessDefinitionUIService}
     * @memberof DefaultBase
     */  
    public appUIService:WFProcessDefinitionUIService = new WFProcessDefinitionUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public data: any = {
        n_deploykey_like: null,
        n_definitionname_like: null,
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public detailsModel: any = {
        formpage1: new FormPageModel({ caption: '常规条件', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this })
, 
        n_deploykey_like: new FormItemModel({ caption: 'DefinitionKey', detailType: 'FORMITEM', name: 'n_deploykey_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_definitionname_like: new FormItemModel({ caption: '流程定义名称', detailType: 'FORMITEM', name: 'n_definitionname_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
    };
}
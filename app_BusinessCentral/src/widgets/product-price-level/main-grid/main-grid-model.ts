/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'pricingmethodcode',
          prop: 'pricingmethodcode',
          dataType: 'SSCODELIST',
        },
        {
          name: 'pricelevelname',
          prop: 'pricelevelname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'uomscheduleid',
          prop: 'uomscheduleid',
          dataType: 'PICKUP',
        },
        {
          name: 'transactioncurrencyid',
          prop: 'transactioncurrencyid',
          dataType: 'PICKUP',
        },
        {
          name: 'uomname',
          prop: 'uomname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'srfmajortext',
          prop: 'productname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'srfkey',
          prop: 'productpricelevelid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'productpricelevelid',
          dataType: 'GUID',
        },
        {
          name: 'amount',
          prop: 'amount',
          dataType: 'BIGDECIMAL',
        },
        {
          name: 'productid',
          prop: 'productid',
          dataType: 'PICKUP',
        },
        {
          name: 'uomid',
          prop: 'uomid',
          dataType: 'PICKUP',
        },
        {
          name: 'discounttypeid',
          prop: 'discounttypeid',
          dataType: 'PICKUP',
        },
        {
          name: 'pricelevelid',
          prop: 'pricelevelid',
          dataType: 'PICKUP',
        },
        {
          name: 'productpricelevel',
          prop: 'productpricelevelid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}
import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, EditFormControlBase } from '@/studio-core';
import ProductPriceLevelService from '@/service/product-price-level/product-price-level-service';
import ProPrilvService from './pro-prilv-form-service';
import ProductPriceLevelUIService from '@/uiservice/product-price-level/product-price-level-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {ProPrilvEditFormBase}
 */
export class ProPrilvEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof ProPrilvEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {ProPrilvService}
     * @memberof ProPrilvEditFormBase
     */
    public service: ProPrilvService = new ProPrilvService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {ProductPriceLevelService}
     * @memberof ProPrilvEditFormBase
     */
    public appEntityService: ProductPriceLevelService = new ProductPriceLevelService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof ProPrilvEditFormBase
     */
    protected appDeName: string = 'productpricelevel';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof ProPrilvEditFormBase
     */
    protected appDeLogicName: string = '价目表项';

    /**
     * 界面UI服务对象
     *
     * @type {ProductPriceLevelUIService}
     * @memberof ProPrilvBase
     */  
    public appUIService:ProductPriceLevelUIService = new ProductPriceLevelUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof ProPrilvEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        productname: null,
        pricelevelname: null,
        uomname: null,
        currencyname: null,
        discounttypename: null,
        quantitysellingcode: null,
        pricingmethodcode: null,
        amount: null,
        productid: null,
        transactioncurrencyid: null,
        uomid: null,
        discounttypeid: null,
        pricelevelid: null,
        productpricelevelid: null,
        productpricelevel:null,
    };

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof ProPrilvEditFormBase
     */
    public rules: any = {
        quantitysellingcode: [
            { required: true, type: 'string', message: '销售数量控制 值不能为空', trigger: 'change' },
            { required: true, type: 'string', message: '销售数量控制 值不能为空', trigger: 'blur' },
        ],
        pricingmethodcode: [
            { required: true, type: 'string', message: '定价方式 值不能为空', trigger: 'change' },
            { required: true, type: 'string', message: '定价方式 值不能为空', trigger: 'blur' },
        ],
        amount: [
            { required: true, type: 'number', message: '金额 值不能为空', trigger: 'change' },
            { required: true, type: 'number', message: '金额 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof ProPrilvBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof ProPrilvEditFormBase
     */
    public detailsModel: any = {
        group1: new FormGroupPanelModel({ caption: '价目表项基本信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.productpricelevel.proprilv_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: '产品价目表', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfmajortext: new FormItemModel({ caption: '产品名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        productname: new FormItemModel({ caption: '产品名称', detailType: 'FORMITEM', name: 'productname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        pricelevelname: new FormItemModel({ caption: '价目表', detailType: 'FORMITEM', name: 'pricelevelname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        uomname: new FormItemModel({ caption: '计量单位', detailType: 'FORMITEM', name: 'uomname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        currencyname: new FormItemModel({ caption: '货币', detailType: 'FORMITEM', name: 'currencyname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        discounttypename: new FormItemModel({ caption: '折扣表', detailType: 'FORMITEM', name: 'discounttypename', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        quantitysellingcode: new FormItemModel({ caption: '销售数量控制', detailType: 'FORMITEM', name: 'quantitysellingcode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        pricingmethodcode: new FormItemModel({ caption: '定价方式', detailType: 'FORMITEM', name: 'pricingmethodcode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        amount: new FormItemModel({ caption: '金额', detailType: 'FORMITEM', name: 'amount', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        productid: new FormItemModel({ caption: '产品', detailType: 'FORMITEM', name: 'productid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        transactioncurrencyid: new FormItemModel({ caption: '货币', detailType: 'FORMITEM', name: 'transactioncurrencyid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        uomid: new FormItemModel({ caption: '计价单位', detailType: 'FORMITEM', name: 'uomid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        discounttypeid: new FormItemModel({ caption: '折扣表', detailType: 'FORMITEM', name: 'discounttypeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        pricelevelid: new FormItemModel({ caption: '价目表', detailType: 'FORMITEM', name: 'pricelevelid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        productpricelevelid: new FormItemModel({ caption: '产品价目表', detailType: 'FORMITEM', name: 'productpricelevelid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

    };
}
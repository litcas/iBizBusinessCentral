/**
 * ProPrilv 部件模型
 *
 * @export
 * @class ProPrilvModel
 */
export default class ProPrilvModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof ProPrilvModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'productpricelevelid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'productname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'productname',
        prop: 'productname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'pricelevelname',
        prop: 'pricelevelname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'uomname',
        prop: 'uomname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'currencyname',
        prop: 'currencyname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'discounttypename',
        prop: 'discounttypename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'quantitysellingcode',
        prop: 'quantitysellingcode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'pricingmethodcode',
        prop: 'pricingmethodcode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'amount',
        prop: 'amount',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'productid',
        prop: 'productid',
        dataType: 'PICKUP',
      },
      {
        name: 'transactioncurrencyid',
        prop: 'transactioncurrencyid',
        dataType: 'PICKUP',
      },
      {
        name: 'uomid',
        prop: 'uomid',
        dataType: 'PICKUP',
      },
      {
        name: 'discounttypeid',
        prop: 'discounttypeid',
        dataType: 'PICKUP',
      },
      {
        name: 'pricelevelid',
        prop: 'pricelevelid',
        dataType: 'PICKUP',
      },
      {
        name: 'productpricelevelid',
        prop: 'productpricelevelid',
        dataType: 'GUID',
      },
      {
        name: 'productpricelevel',
        prop: 'productpricelevelid',
        dataType: 'FONTKEY',
      },
    ]
  }

}
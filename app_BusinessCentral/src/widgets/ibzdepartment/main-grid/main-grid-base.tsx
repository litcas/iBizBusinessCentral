import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, GridControlBase } from '@/studio-core';
import IBZDepartmentService from '@/service/ibzdepartment/ibzdepartment-service';
import MainService from './main-grid-service';
import IBZDepartmentUIService from '@/uiservice/ibzdepartment/ibzdepartment-ui-service';
import { FormItemModel } from '@/model/form-detail';


/**
 * grid部件基类
 *
 * @export
 * @class GridControlBase
 * @extends {MainGridBase}
 */
export class MainGridBase extends GridControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected controlType: string = 'GRID';

    /**
     * 建构部件服务对象
     *
     * @type {MainService}
     * @memberof MainGridBase
     */
    public service: MainService = new MainService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {IBZDepartmentService}
     * @memberof MainGridBase
     */
    public appEntityService: IBZDepartmentService = new IBZDepartmentService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeName: string = 'ibzdepartment';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeLogicName: string = '部门';

    /**
     * 界面UI服务对象
     *
     * @type {IBZDepartmentUIService}
     * @memberof MainBase
     */  
    public appUIService:IBZDepartmentUIService = new IBZDepartmentUIService(this.$store);

    /**
     * 界面行为模型
     *
     * @type {*}
     * @memberof MainBase
     */  
    public ActionModel: any = {
    };

    /**
     * 本地缓存标识
     *
     * @protected
     * @type {string}
     * @memberof MainBase
     */
    protected localStorageTag: string = 'ibzdept_main_grid';

    /**
     * 分页条数
     *
     * @type {number}
     * @memberof MainGridBase
     */
    public limit: number = 10;

    /**
     * 所有列成员
     *
     * @type {any[]}
     * @memberof MainGridBase
     */
    public allColumns: any[] = [
        {
            name: 'deptcode',
            label: '部门代码',
            langtag: 'entities.ibzdepartment.main_grid.columns.deptcode',
            show: true,
            unit: 'px',
            isEnableRowEdit: false,
        },
        {
            name: 'deptname',
            label: '部门名称',
            langtag: 'entities.ibzdepartment.main_grid.columns.deptname',
            show: true,
            unit: 'px',
            isEnableRowEdit: false,
        },
        {
            name: 'orgname',
            label: '单位',
            langtag: 'entities.ibzdepartment.main_grid.columns.orgname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'pdeptname',
            label: '上级部门',
            langtag: 'entities.ibzdepartment.main_grid.columns.pdeptname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'deptlevel',
            label: '部门级别',
            langtag: 'entities.ibzdepartment.main_grid.columns.deptlevel',
            show: true,
            unit: 'px',
            isEnableRowEdit: false,
        },
        {
            name: 'shortname',
            label: '部门简称',
            langtag: 'entities.ibzdepartment.main_grid.columns.shortname',
            show: true,
            unit: 'px',
            isEnableRowEdit: false,
        },
        {
            name: 'bcode',
            label: '业务编码',
            langtag: 'entities.ibzdepartment.main_grid.columns.bcode',
            show: true,
            unit: 'px',
            isEnableRowEdit: false,
        },
        {
            name: 'leadername',
            label: '分管领导',
            langtag: 'entities.ibzdepartment.main_grid.columns.leadername',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'showorder',
            label: '排序',
            langtag: 'entities.ibzdepartment.main_grid.columns.showorder',
            show: true,
            unit: 'px',
            isEnableRowEdit: false,
        },
        {
            name: 'createdate',
            label: '创建时间',
            langtag: 'entities.ibzdepartment.main_grid.columns.createdate',
            show: true,
            unit: 'px',
            isEnableRowEdit: false,
        },
        {
            name: 'updatedate',
            label: '最后修改时间',
            langtag: 'entities.ibzdepartment.main_grid.columns.updatedate',
            show: true,
            unit: 'px',
            isEnableRowEdit: false,
        },
        {
            name: 'orgid',
            label: '单位',
            langtag: 'entities.ibzdepartment.main_grid.columns.orgid',
            show: false,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'pdeptid',
            label: '上级部门',
            langtag: 'entities.ibzdepartment.main_grid.columns.pdeptid',
            show: false,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'leaderid',
            label: '分管领导标识',
            langtag: 'entities.ibzdepartment.main_grid.columns.leaderid',
            show: false,
            unit: 'PX',
            isEnableRowEdit: false,
        },
    ]

    /**
     * 获取表格行模型
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public getGridRowModel(){
        return {
          srfkey: new FormItemModel(),
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public rules: any = {
        srfkey: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '部门标识 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '部门标识 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 获取对应列class
     *
     * @type {*}
     * @memberof MainBase
     */
    public hasRowEdit: any = {
        'deptcode':false,
        'deptname':false,
        'orgname':false,
        'pdeptname':false,
        'deptlevel':false,
        'shortname':false,
        'bcode':false,
        'leadername':false,
        'showorder':false,
        'createdate':false,
        'updatedate':false,
        'orgid':false,
        'pdeptid':false,
        'leaderid':false,
    };

    /**
     * 获取对应列class
     *
     * @param {*} $args row 行数据，column 列数据，rowIndex 行索引，列索引
     * @returns {void}
     * @memberof MainBase
     */
    public getCellClassName(args: {row: any, column: any, rowIndex: number, columnIndex: number}): any {
        return ( this.hasRowEdit[args.column.property] && this.actualIsOpenEdit ) ? "edit-cell" : "info-cell";
    }


    /**
     * 导出数据格式化
     *
     * @param {*} filterVal
     * @param {*} jsonData
     * @param {any[]} [codelistColumns=[]]
     * @returns {Promise<any>}
     * @memberof MainGridBase
     */
    public async formatExcelData(filterVal: any, jsonData: any, codelistColumns?: any[]): Promise<any> {
        return super.formatExcelData(filterVal, jsonData, [
        ]);
    }

}
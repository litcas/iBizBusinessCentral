import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * Head_Summary 部件服务对象
 *
 * @export
 * @class Head_SummaryService
 */
export default class Head_SummaryService extends ControlService {
}
/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'template',
          prop: 'template',
          dataType: 'YESNO',
        },
        {
          name: 'campaignname',
          prop: 'campaignname',
          dataType: 'TEXT',
        },
        {
          name: 'typecode',
          prop: 'typecode',
          dataType: 'SSCODELIST',
        },
        {
          name: 'createdate',
          prop: 'createdate',
          dataType: 'DATETIME',
        },
        {
          name: 'budgetedcost',
          prop: 'budgetedcost',
          dataType: 'BIGDECIMAL',
        },
        {
          name: 'transactioncurrencyid',
          prop: 'transactioncurrencyid',
          dataType: 'PICKUP',
        },
        {
          name: 'proposedstart',
          prop: 'proposedstart',
          dataType: 'DATETIME',
        },
        {
          name: 'currencyname',
          prop: 'currencyname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'srfmajortext',
          prop: 'campaignname',
          dataType: 'TEXT',
        },
        {
          name: 'srfkey',
          prop: 'campaignid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'campaignid',
          dataType: 'GUID',
        },
        {
          name: 'codename',
          prop: 'codename',
          dataType: 'TEXT',
        },
        {
          name: 'pricelistid',
          prop: 'pricelistid',
          dataType: 'PICKUP',
        },
        {
          name: 'statuscode',
          prop: 'statuscode',
          dataType: 'NSCODELIST',
        },
        {
          name: 'campaign',
          prop: 'campaignid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}
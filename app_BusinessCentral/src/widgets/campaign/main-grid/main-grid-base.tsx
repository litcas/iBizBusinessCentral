import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, GridControlBase } from '@/studio-core';
import CampaignService from '@/service/campaign/campaign-service';
import MainService from './main-grid-service';
import CampaignUIService from '@/uiservice/campaign/campaign-ui-service';
import { FormItemModel } from '@/model/form-detail';


/**
 * grid部件基类
 *
 * @export
 * @class GridControlBase
 * @extends {MainGridBase}
 */
export class MainGridBase extends GridControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected controlType: string = 'GRID';

    /**
     * 建构部件服务对象
     *
     * @type {MainService}
     * @memberof MainGridBase
     */
    public service: MainService = new MainService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {CampaignService}
     * @memberof MainGridBase
     */
    public appEntityService: CampaignService = new CampaignService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeName: string = 'campaign';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeLogicName: string = '市场活动';

    /**
     * 界面UI服务对象
     *
     * @type {CampaignUIService}
     * @memberof MainBase
     */  
    public appUIService:CampaignUIService = new CampaignUIService(this.$store);

    /**
     * 界面行为模型
     *
     * @type {*}
     * @memberof MainBase
     */  
    public ActionModel: any = {
    };

    /**
     * 本地缓存标识
     *
     * @protected
     * @type {string}
     * @memberof MainBase
     */
    protected localStorageTag: string = 'campaign_main_grid';

    /**
     * 所有列成员
     *
     * @type {any[]}
     * @memberof MainGridBase
     */
    public allColumns: any[] = [
        {
            name: 'campaignname',
            label: '活动名称',
            langtag: 'entities.campaign.main_grid.columns.campaignname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'codename',
            label: '活动代码',
            langtag: 'entities.campaign.main_grid.columns.codename',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'typecode',
            label: '活动类型',
            langtag: 'entities.campaign.main_grid.columns.typecode',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'transactioncurrencyname',
            label: '货币',
            langtag: 'entities.campaign.main_grid.columns.transactioncurrencyname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'template',
            label: '模板',
            langtag: 'entities.campaign.main_grid.columns.template',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'statuscode',
            label: '状态描述',
            langtag: 'entities.campaign.main_grid.columns.statuscode',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'proposedstart',
            label: '拟定日期',
            langtag: 'entities.campaign.main_grid.columns.proposedstart',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'budgetedcost',
            label: '预算分配',
            langtag: 'entities.campaign.main_grid.columns.budgetedcost',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'createdate',
            label: '建立时间',
            langtag: 'entities.campaign.main_grid.columns.createdate',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
    ]

    /**
     * 获取表格行模型
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public getGridRowModel(){
        return {
          srfkey: new FormItemModel(),
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public rules: any = {
        srfkey: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '市场活动 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '市场活动 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 获取对应列class
     *
     * @type {*}
     * @memberof MainBase
     */
    public hasRowEdit: any = {
        'campaignname':false,
        'codename':false,
        'typecode':false,
        'transactioncurrencyname':false,
        'template':false,
        'statuscode':false,
        'proposedstart':false,
        'budgetedcost':false,
        'createdate':false,
    };

    /**
     * 获取对应列class
     *
     * @param {*} $args row 行数据，column 列数据，rowIndex 行索引，列索引
     * @returns {void}
     * @memberof MainBase
     */
    public getCellClassName(args: {row: any, column: any, rowIndex: number, columnIndex: number}): any {
        return ( this.hasRowEdit[args.column.property] && this.actualIsOpenEdit ) ? "edit-cell" : "info-cell";
    }


    /**
     * 导出数据格式化
     *
     * @param {*} filterVal
     * @param {*} jsonData
     * @param {any[]} [codelistColumns=[]]
     * @returns {Promise<any>}
     * @memberof MainGridBase
     */
    public async formatExcelData(filterVal: any, jsonData: any, codelistColumns?: any[]): Promise<any> {
        return super.formatExcelData(filterVal, jsonData, [
            {
                name: 'typecode',
                srfkey: 'Campaign__TypeCode',
                codelistType : 'STATIC',
                renderMode: 'other',
                textSeparator: '、',
                valueSeparator: ',',
            },
            {
                name: 'template',
                srfkey: 'YesNo',
                codelistType : 'STATIC',
                renderMode: 'other',
                textSeparator: '、',
                valueSeparator: ',',
            },
            {
                name: 'statuscode',
                srfkey: 'Campaign__StatusCode',
                codelistType : 'STATIC',
                renderMode: 'other',
                textSeparator: '、',
                valueSeparator: ',',
            },
        ]);
    }

}
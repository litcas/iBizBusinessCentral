/**
 * Edit_Main 部件模型
 *
 * @export
 * @class Edit_MainModel
 */
export default class Edit_MainModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Edit_MainModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'campaignid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'campaignname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'campaignname',
        prop: 'campaignname',
        dataType: 'TEXT',
      },
      {
        name: 'codename',
        prop: 'codename',
        dataType: 'TEXT',
      },
      {
        name: 'transactioncurrencyname',
        prop: 'currencyname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'typecode',
        prop: 'typecode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'expectedresponse',
        prop: 'expectedresponse',
        dataType: 'INT',
      },
      {
        name: 'objective',
        prop: 'objective',
        dataType: 'TEXT',
      },
      {
        name: 'proposedstart',
        prop: 'proposedstart',
        dataType: 'DATETIME',
      },
      {
        name: 'proposedend',
        prop: 'proposedend',
        dataType: 'DATETIME',
      },
      {
        name: 'actualstart',
        prop: 'actualstart',
        dataType: 'DATETIME',
      },
      {
        name: 'actualend',
        prop: 'actualend',
        dataType: 'DATETIME',
      },
      {
        name: 'budgetedcost',
        prop: 'budgetedcost',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'othercost',
        prop: 'othercost',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'totalactualcost',
        prop: 'totalactualcost',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'ownername',
        prop: 'ownername',
        dataType: 'TEXT',
      },
      {
        name: 'transactioncurrencyid',
        prop: 'transactioncurrencyid',
        dataType: 'PICKUP',
      },
      {
        name: 'campaignid',
        prop: 'campaignid',
        dataType: 'GUID',
      },
      {
        name: 'campaign',
        prop: 'campaignid',
        dataType: 'FONTKEY',
      },
    ]
  }

}
import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, EditFormControlBase } from '@/studio-core';
import SalesOrderDetailService from '@/service/sales-order-detail/sales-order-detail-service';
import SOProductEditService from './soproduct-edit-form-service';
import SalesOrderDetailUIService from '@/uiservice/sales-order-detail/sales-order-detail-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {SOProductEditEditFormBase}
 */
export class SOProductEditEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof SOProductEditEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {SOProductEditService}
     * @memberof SOProductEditEditFormBase
     */
    public service: SOProductEditService = new SOProductEditService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {SalesOrderDetailService}
     * @memberof SOProductEditEditFormBase
     */
    public appEntityService: SalesOrderDetailService = new SalesOrderDetailService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof SOProductEditEditFormBase
     */
    protected appDeName: string = 'salesorderdetail';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof SOProductEditEditFormBase
     */
    protected appDeLogicName: string = '订单产品';

    /**
     * 界面UI服务对象
     *
     * @type {SalesOrderDetailUIService}
     * @memberof SOProductEditBase
     */  
    public appUIService:SalesOrderDetailUIService = new SalesOrderDetailUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof SOProductEditEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        productname: null,
        priceperunit: null,
        quantity: null,
        uomname: null,
        manualdiscountamount: null,
        tax: null,
        productid: null,
        uomid: null,
        salesorderdetailid: null,
        salesorderdetail:null,
    };

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof SOProductEditEditFormBase
     */
    public rules: any = {
        quantity: [
            { required: true, type: 'number', message: '数量 值不能为空', trigger: 'change' },
            { required: true, type: 'number', message: '数量 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof SOProductEditBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof SOProductEditEditFormBase
     */
    public detailsModel: any = {
        group1: new FormGroupPanelModel({ caption: '订单产品基本信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.salesorderdetail.soproductedit_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: '订单产品', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfmajortext: new FormItemModel({ caption: '名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        productname: new FormItemModel({ caption: '产品名称', detailType: 'FORMITEM', name: 'productname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        priceperunit: new FormItemModel({ caption: '单价', detailType: 'FORMITEM', name: 'priceperunit', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        quantity: new FormItemModel({ caption: '数量', detailType: 'FORMITEM', name: 'quantity', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        uomname: new FormItemModel({ caption: '计价单位', detailType: 'FORMITEM', name: 'uomname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        manualdiscountamount: new FormItemModel({ caption: '零售折扣', detailType: 'FORMITEM', name: 'manualdiscountamount', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        tax: new FormItemModel({ caption: '税', detailType: 'FORMITEM', name: 'tax', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        productid: new FormItemModel({ caption: '现有产品', detailType: 'FORMITEM', name: 'productid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        uomid: new FormItemModel({ caption: '计价单位', detailType: 'FORMITEM', name: 'uomid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        salesorderdetailid: new FormItemModel({ caption: '订单产品', detailType: 'FORMITEM', name: 'salesorderdetailid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

    };
}
/**
 * SOProduct 部件模型
 *
 * @export
 * @class SOProductModel
 */
export default class SOProductModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof SOProductGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof SOProductGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'extendedamount',
          prop: 'extendedamount',
          dataType: 'BIGDECIMAL',
        },
        {
          name: 'parentbundleidref',
          prop: 'parentbundleidref',
          dataType: 'PICKUP',
        },
        {
          name: 'transactioncurrencyid',
          prop: 'transactioncurrencyid',
          dataType: 'PICKUP',
        },
        {
          name: 'manualdiscountamount',
          prop: 'manualdiscountamount',
          dataType: 'BIGDECIMAL',
        },
        {
          name: 'srfmajortext',
          prop: 'salesorderdetailname',
          dataType: 'TEXT',
        },
        {
          name: 'srfkey',
          prop: 'salesorderdetailid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'salesorderdetailid',
          dataType: 'GUID',
        },
        {
          name: 'productid',
          prop: 'productid',
          dataType: 'PICKUP',
        },
        {
          name: 'priceperunit',
          prop: 'priceperunit',
          dataType: 'BIGDECIMAL',
        },
        {
          name: 'quotedetailid',
          prop: 'quotedetailid',
          dataType: 'PICKUP',
        },
        {
          name: 'quantity',
          prop: 'quantity',
          dataType: 'BIGDECIMAL',
        },
        {
          name: 'salesorderid',
          prop: 'salesorderid',
          dataType: 'PICKUP',
        },
        {
          name: 'uomid',
          prop: 'uomid',
          dataType: 'PICKUP',
        },
        {
          name: 'productname',
          prop: 'productname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'salesorderdetail',
          prop: 'salesorderdetailid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}
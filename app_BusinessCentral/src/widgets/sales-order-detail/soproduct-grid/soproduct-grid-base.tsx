import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, GridControlBase } from '@/studio-core';
import SalesOrderDetailService from '@/service/sales-order-detail/sales-order-detail-service';
import SOProductService from './soproduct-grid-service';
import SalesOrderDetailUIService from '@/uiservice/sales-order-detail/sales-order-detail-ui-service';
import { FormItemModel } from '@/model/form-detail';


/**
 * grid部件基类
 *
 * @export
 * @class GridControlBase
 * @extends {SOProductGridBase}
 */
export class SOProductGridBase extends GridControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof SOProductGridBase
     */
    protected controlType: string = 'GRID';

    /**
     * 建构部件服务对象
     *
     * @type {SOProductService}
     * @memberof SOProductGridBase
     */
    public service: SOProductService = new SOProductService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {SalesOrderDetailService}
     * @memberof SOProductGridBase
     */
    public appEntityService: SalesOrderDetailService = new SalesOrderDetailService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof SOProductGridBase
     */
    protected appDeName: string = 'salesorderdetail';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof SOProductGridBase
     */
    protected appDeLogicName: string = '订单产品';

    /**
     * 界面UI服务对象
     *
     * @type {SalesOrderDetailUIService}
     * @memberof SOProductBase
     */  
    public appUIService:SalesOrderDetailUIService = new SalesOrderDetailUIService(this.$store);

    /**
     * 界面行为模型
     *
     * @type {*}
     * @memberof SOProductBase
     */  
    public ActionModel: any = {
    };

    /**
     * 本地缓存标识
     *
     * @protected
     * @type {string}
     * @memberof SOProductBase
     */
    protected localStorageTag: string = 'salesorderdetail_soproduct_grid';

    /**
     * 所有列成员
     *
     * @type {any[]}
     * @memberof SOProductGridBase
     */
    public allColumns: any[] = [
        {
            name: 'productname',
            label: '产品名称',
            langtag: 'entities.salesorderdetail.soproduct_grid.columns.productname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'uomid',
            label: '计价单位',
            langtag: 'entities.salesorderdetail.soproduct_grid.columns.uomid',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'priceperunit',
            label: '单价',
            langtag: 'entities.salesorderdetail.soproduct_grid.columns.priceperunit',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'quantity',
            label: '数量',
            langtag: 'entities.salesorderdetail.soproduct_grid.columns.quantity',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'manualdiscountamount',
            label: '零售折扣',
            langtag: 'entities.salesorderdetail.soproduct_grid.columns.manualdiscountamount',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'extendedamount',
            label: '应收净额',
            langtag: 'entities.salesorderdetail.soproduct_grid.columns.extendedamount',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
    ]

    /**
     * 获取表格行模型
     *
     * @type {*}
     * @memberof SOProductGridBase
     */
    public getGridRowModel(){
        return {
          srfkey: new FormItemModel(),
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof SOProductGridBase
     */
    public rules: any = {
        srfkey: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '订单产品 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '订单产品 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 获取对应列class
     *
     * @type {*}
     * @memberof SOProductBase
     */
    public hasRowEdit: any = {
        'productname':false,
        'uomid':false,
        'priceperunit':false,
        'quantity':false,
        'manualdiscountamount':false,
        'extendedamount':false,
    };

    /**
     * 获取对应列class
     *
     * @param {*} $args row 行数据，column 列数据，rowIndex 行索引，列索引
     * @returns {void}
     * @memberof SOProductBase
     */
    public getCellClassName(args: {row: any, column: any, rowIndex: number, columnIndex: number}): any {
        return ( this.hasRowEdit[args.column.property] && this.actualIsOpenEdit ) ? "edit-cell" : "info-cell";
    }


    /**
     * 导出数据格式化
     *
     * @param {*} filterVal
     * @param {*} jsonData
     * @param {any[]} [codelistColumns=[]]
     * @returns {Promise<any>}
     * @memberof SOProductGridBase
     */
    public async formatExcelData(filterVal: any, jsonData: any, codelistColumns?: any[]): Promise<any> {
        return super.formatExcelData(filterVal, jsonData, [
        ]);
    }

}
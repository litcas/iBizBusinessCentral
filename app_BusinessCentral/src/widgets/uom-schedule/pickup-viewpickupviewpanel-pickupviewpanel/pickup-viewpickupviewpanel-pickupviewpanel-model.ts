/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'importsequencenumber',
      },
      {
        name: 'timezoneruleversionnumber',
      },
      {
        name: 'createman',
      },
      {
        name: 'description',
      },
      {
        name: 'baseuomname',
      },
      {
        name: 'updateman',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'versionnumber',
      },
      {
        name: 'createdate',
      },
      {
        name: 'utcconversiontimezonecode',
      },
      {
        name: 'statecode',
      },
      {
        name: 'statuscode',
      },
      {
        name: 'overriddencreatedon',
      },
      {
        name: 'uomschedulename',
      },
      {
        name: 'uomschedule',
        prop: 'uomscheduleid',
      },
    ]
  }


}
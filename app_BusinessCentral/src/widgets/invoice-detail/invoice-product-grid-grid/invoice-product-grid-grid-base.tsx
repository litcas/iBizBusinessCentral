import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, GridControlBase } from '@/studio-core';
import InvoiceDetailService from '@/service/invoice-detail/invoice-detail-service';
import InvoiceProductGridService from './invoice-product-grid-grid-service';
import InvoiceDetailUIService from '@/uiservice/invoice-detail/invoice-detail-ui-service';
import { FormItemModel } from '@/model/form-detail';


/**
 * grid部件基类
 *
 * @export
 * @class GridControlBase
 * @extends {InvoiceProductGridGridBase}
 */
export class InvoiceProductGridGridBase extends GridControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof InvoiceProductGridGridBase
     */
    protected controlType: string = 'GRID';

    /**
     * 建构部件服务对象
     *
     * @type {InvoiceProductGridService}
     * @memberof InvoiceProductGridGridBase
     */
    public service: InvoiceProductGridService = new InvoiceProductGridService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {InvoiceDetailService}
     * @memberof InvoiceProductGridGridBase
     */
    public appEntityService: InvoiceDetailService = new InvoiceDetailService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof InvoiceProductGridGridBase
     */
    protected appDeName: string = 'invoicedetail';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof InvoiceProductGridGridBase
     */
    protected appDeLogicName: string = '发票产品';

    /**
     * 界面UI服务对象
     *
     * @type {InvoiceDetailUIService}
     * @memberof InvoiceProductGridBase
     */  
    public appUIService:InvoiceDetailUIService = new InvoiceDetailUIService(this.$store);

    /**
     * 界面行为模型
     *
     * @type {*}
     * @memberof InvoiceProductGridBase
     */  
    public ActionModel: any = {
    };

    /**
     * 本地缓存标识
     *
     * @protected
     * @type {string}
     * @memberof InvoiceProductGridBase
     */
    protected localStorageTag: string = 'invoicedetail_invoiceproductgrid_grid';

    /**
     * 所有列成员
     *
     * @type {any[]}
     * @memberof InvoiceProductGridGridBase
     */
    public allColumns: any[] = [
        {
            name: 'productname',
            label: '产品名称',
            langtag: 'entities.invoicedetail.invoiceproductgrid_grid.columns.productname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'priceperunit',
            label: '单价',
            langtag: 'entities.invoicedetail.invoiceproductgrid_grid.columns.priceperunit',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'quantity',
            label: '数量',
            langtag: 'entities.invoicedetail.invoiceproductgrid_grid.columns.quantity',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'manualdiscountamount',
            label: '零售折扣',
            langtag: 'entities.invoicedetail.invoiceproductgrid_grid.columns.manualdiscountamount',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'extendedamount',
            label: '应收净额',
            langtag: 'entities.invoicedetail.invoiceproductgrid_grid.columns.extendedamount',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
    ]

    /**
     * 获取表格行模型
     *
     * @type {*}
     * @memberof InvoiceProductGridGridBase
     */
    public getGridRowModel(){
        return {
          srfkey: new FormItemModel(),
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof InvoiceProductGridGridBase
     */
    public rules: any = {
        srfkey: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '发票产品 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '发票产品 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 获取对应列class
     *
     * @type {*}
     * @memberof InvoiceProductGridBase
     */
    public hasRowEdit: any = {
        'productname':false,
        'priceperunit':false,
        'quantity':false,
        'manualdiscountamount':false,
        'extendedamount':false,
    };

    /**
     * 获取对应列class
     *
     * @param {*} $args row 行数据，column 列数据，rowIndex 行索引，列索引
     * @returns {void}
     * @memberof InvoiceProductGridBase
     */
    public getCellClassName(args: {row: any, column: any, rowIndex: number, columnIndex: number}): any {
        return ( this.hasRowEdit[args.column.property] && this.actualIsOpenEdit ) ? "edit-cell" : "info-cell";
    }


    /**
     * 导出数据格式化
     *
     * @param {*} filterVal
     * @param {*} jsonData
     * @param {any[]} [codelistColumns=[]]
     * @returns {Promise<any>}
     * @memberof InvoiceProductGridGridBase
     */
    public async formatExcelData(filterVal: any, jsonData: any, codelistColumns?: any[]): Promise<any> {
        return super.formatExcelData(filterVal, jsonData, [
        ]);
    }

}
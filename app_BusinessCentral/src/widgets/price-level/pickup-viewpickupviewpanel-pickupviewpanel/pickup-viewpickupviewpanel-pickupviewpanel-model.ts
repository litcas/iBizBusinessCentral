/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'createman',
      },
      {
        name: 'updateman',
      },
      {
        name: 'description',
      },
      {
        name: 'statecode',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'paymentmethodcode',
      },
      {
        name: 'pricelevel',
        prop: 'pricelevelid',
      },
      {
        name: 'shippingmethodcode',
      },
      {
        name: 'overriddencreatedon',
      },
      {
        name: 'utcconversiontimezonecode',
      },
      {
        name: 'versionnumber',
      },
      {
        name: 'enddate',
      },
      {
        name: 'exchangerate',
      },
      {
        name: 'begindate',
      },
      {
        name: 'importsequencenumber',
      },
      {
        name: 'pricelevelname',
      },
      {
        name: 'statuscode',
      },
      {
        name: 'freighttermscode',
      },
      {
        name: 'createdate',
      },
      {
        name: 'timezoneruleversionnumber',
      },
      {
        name: 'transactioncurrencyid',
      },
    ]
  }


}
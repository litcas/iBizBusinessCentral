import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, GridControlBase } from '@/studio-core';
import PriceLevelService from '@/service/price-level/price-level-service';
import MainService from './main-grid-service';
import PriceLevelUIService from '@/uiservice/price-level/price-level-ui-service';
import { FormItemModel } from '@/model/form-detail';


/**
 * grid部件基类
 *
 * @export
 * @class GridControlBase
 * @extends {MainGridBase}
 */
export class MainGridBase extends GridControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected controlType: string = 'GRID';

    /**
     * 建构部件服务对象
     *
     * @type {MainService}
     * @memberof MainGridBase
     */
    public service: MainService = new MainService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {PriceLevelService}
     * @memberof MainGridBase
     */
    public appEntityService: PriceLevelService = new PriceLevelService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeName: string = 'pricelevel';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeLogicName: string = '价目表';

    /**
     * 界面UI服务对象
     *
     * @type {PriceLevelUIService}
     * @memberof MainBase
     */  
    public appUIService:PriceLevelUIService = new PriceLevelUIService(this.$store);

    /**
     * 界面行为模型
     *
     * @type {*}
     * @memberof MainBase
     */  
    public ActionModel: any = {
    };

    /**
     * 本地缓存标识
     *
     * @protected
     * @type {string}
     * @memberof MainBase
     */
    protected localStorageTag: string = 'pricelevel_main_grid';

    /**
     * 所有列成员
     *
     * @type {any[]}
     * @memberof MainGridBase
     */
    public allColumns: any[] = [
        {
            name: 'pricelevelname',
            label: '价目表名称',
            langtag: 'entities.pricelevel.main_grid.columns.pricelevelname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'begindate',
            label: '开始日期',
            langtag: 'entities.pricelevel.main_grid.columns.begindate',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'enddate',
            label: '结束日期',
            langtag: 'entities.pricelevel.main_grid.columns.enddate',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'paymentmethodcode',
            label: '付款方式',
            langtag: 'entities.pricelevel.main_grid.columns.paymentmethodcode',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'description',
            label: '说明',
            langtag: 'entities.pricelevel.main_grid.columns.description',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
    ]

    /**
     * 获取表格行模型
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public getGridRowModel(){
        return {
          srfkey: new FormItemModel(),
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public rules: any = {
        srfkey: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '价目表 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '价目表 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 获取对应列class
     *
     * @type {*}
     * @memberof MainBase
     */
    public hasRowEdit: any = {
        'pricelevelname':false,
        'begindate':false,
        'enddate':false,
        'paymentmethodcode':false,
        'description':false,
    };

    /**
     * 获取对应列class
     *
     * @param {*} $args row 行数据，column 列数据，rowIndex 行索引，列索引
     * @returns {void}
     * @memberof MainBase
     */
    public getCellClassName(args: {row: any, column: any, rowIndex: number, columnIndex: number}): any {
        return ( this.hasRowEdit[args.column.property] && this.actualIsOpenEdit ) ? "edit-cell" : "info-cell";
    }


    /**
     * 导出数据格式化
     *
     * @param {*} filterVal
     * @param {*} jsonData
     * @param {any[]} [codelistColumns=[]]
     * @returns {Promise<any>}
     * @memberof MainGridBase
     */
    public async formatExcelData(filterVal: any, jsonData: any, codelistColumns?: any[]): Promise<any> {
        return super.formatExcelData(filterVal, jsonData, [
            {
                name: 'paymentmethodcode',
                srfkey: 'Pricelevel__PaymentMethodCode',
                codelistType : 'STATIC',
                renderMode: 'other',
                textSeparator: '、',
                valueSeparator: ',',
            },
        ]);
    }

}
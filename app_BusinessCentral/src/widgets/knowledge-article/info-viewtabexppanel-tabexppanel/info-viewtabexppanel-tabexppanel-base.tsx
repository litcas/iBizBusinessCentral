import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, TabExpPanelControlBase } from '@/studio-core';
import KnowledgeArticleService from '@/service/knowledge-article/knowledge-article-service';
import InfoViewtabexppanelService from './info-viewtabexppanel-tabexppanel-service';
import KnowledgeArticleUIService from '@/uiservice/knowledge-article/knowledge-article-ui-service';


/**
 * tabexppanel部件基类
 *
 * @export
 * @class TabExpPanelControlBase
 * @extends {InfoViewtabexppanelTabexppanelBase}
 */
export class InfoViewtabexppanelTabexppanelBase extends TabExpPanelControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof InfoViewtabexppanelTabexppanelBase
     */
    protected controlType: string = 'TABEXPPANEL';

    /**
     * 建构部件服务对象
     *
     * @type {InfoViewtabexppanelService}
     * @memberof InfoViewtabexppanelTabexppanelBase
     */
    public service: InfoViewtabexppanelService = new InfoViewtabexppanelService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {KnowledgeArticleService}
     * @memberof InfoViewtabexppanelTabexppanelBase
     */
    public appEntityService: KnowledgeArticleService = new KnowledgeArticleService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof InfoViewtabexppanelTabexppanelBase
     */
    protected appDeName: string = 'knowledgearticle';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof InfoViewtabexppanelTabexppanelBase
     */
    protected appDeLogicName: string = '知识文章';

    /**
     * 界面UI服务对象
     *
     * @type {KnowledgeArticleUIService}
     * @memberof InfoViewtabexppanelBase
     */  
    public appUIService:KnowledgeArticleUIService = new KnowledgeArticleUIService(this.$store);
    /**
     * 是否初始化
     *
     * @protected
     * @returns {any}
     * @memberof InfoViewtabexppanel
     */
    protected isInit: any = {
        tabviewpanel:  true ,
        tabviewpanel2:  false ,
    }

    /**
     * 被激活的分页面板
     *
     * @protected
     * @type {string}
     * @memberof InfoViewtabexppanel
     */
    protected activatedTabViewPanel: string = 'tabviewpanel';

    /**
     * 组件创建完毕
     *
     * @protected
     * @memberof InfoViewtabexppanel
     */
    protected ctrlCreated(): void {
        //设置分页导航srfparentdename和srfparentkey
        if (this.context.knowledgearticle) {
            Object.assign(this.context, { srfparentdename: 'KnowledgeArticle', srfparentkey: this.context.knowledgearticle });
        }
        super.ctrlCreated();
    }
}
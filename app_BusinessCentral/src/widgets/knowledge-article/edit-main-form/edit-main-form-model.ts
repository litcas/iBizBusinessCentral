/**
 * Edit_Main 部件模型
 *
 * @export
 * @class Edit_MainModel
 */
export default class Edit_MainModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Edit_MainModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'knowledgearticleid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'title',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'title',
        prop: 'title',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'keywords',
        prop: 'keywords',
        dataType: 'LONGTEXT',
      },
      {
        name: 'internal',
        prop: 'internal',
        dataType: 'YESNO',
      },
      {
        name: 'statuscode',
        prop: 'statuscode',
        dataType: 'NSCODELIST',
      },
      {
        name: 'ownerid',
        prop: 'ownerid',
        dataType: 'TEXT',
      },
      {
        name: 'primaryauthorid',
        prop: 'primaryauthorid',
        dataType: 'TEXT',
      },
      {
        name: 'subjectname',
        prop: 'subjectname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'publishon',
        prop: 'publishon',
        dataType: 'DATETIME',
      },
      {
        name: 'expirationdate',
        prop: 'expirationdate',
        dataType: 'DATETIME',
      },
      {
        name: 'content',
        prop: 'content',
        dataType: 'LONGTEXT',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'knowledgearticleid',
        prop: 'knowledgearticleid',
        dataType: 'GUID',
      },
      {
        name: 'subjectid',
        prop: 'subjectid',
        dataType: 'PICKUP',
      },
      {
        name: 'knowledgearticle',
        prop: 'knowledgearticleid',
        dataType: 'FONTKEY',
      },
    ]
  }

}
import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * InfoViewtabviewpanel 部件服务对象
 *
 * @export
 * @class InfoViewtabviewpanelService
 */
export default class InfoViewtabviewpanelService extends ControlService {
}
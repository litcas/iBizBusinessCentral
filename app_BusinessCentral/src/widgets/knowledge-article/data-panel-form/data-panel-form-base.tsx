import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, EditFormControlBase } from '@/studio-core';
import KnowledgeArticleService from '@/service/knowledge-article/knowledge-article-service';
import DataPanelService from './data-panel-form-service';
import KnowledgeArticleUIService from '@/uiservice/knowledge-article/knowledge-article-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * datapanel部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {DataPanelEditFormBase}
 */
export class DataPanelEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof DataPanelEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {DataPanelService}
     * @memberof DataPanelEditFormBase
     */
    public service: DataPanelService = new DataPanelService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {KnowledgeArticleService}
     * @memberof DataPanelEditFormBase
     */
    public appEntityService: KnowledgeArticleService = new KnowledgeArticleService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof DataPanelEditFormBase
     */
    protected appDeName: string = 'knowledgearticle';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof DataPanelEditFormBase
     */
    protected appDeLogicName: string = '知识文章';

    /**
     * 界面UI服务对象
     *
     * @type {KnowledgeArticleUIService}
     * @memberof DataPanelBase
     */  
    public appUIService:KnowledgeArticleUIService = new KnowledgeArticleUIService(this.$store);

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public datapanel_button1_click(params: any = {}, tag?: any, $event?: any) {
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this;
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:KnowledgeArticleUIService  = new KnowledgeArticleUIService();
        curUIService.KnowledgeArticle_OpenEdit_DataPanelView(datas,contextJO, paramJO,  $event, xData,this,"KnowledgeArticle");
    }

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof DataPanelEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        languagelocalename: null,
        statecode: null,
        knowledgearticleid: null,
        knowledgearticle:null,
    };

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof DataPanelEditFormBase
     */
    public rules: any = {
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof DataPanelBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof DataPanelEditFormBase
     */
    public detailsModel: any = {
        button1: new FormButtonModel({ caption: '头信息编辑', detailType: 'BUTTON', name: 'button1', visible: true, isShowCaption: false, form: this, showMoreMode: 0,disabled: false, uiaction: { type: 'DEUIACTION', 
 tag: 'OpenEdit_DataPanelView',actiontarget: 'SINGLEKEY',noprivdisplaymode:2,visabled: true,disabled: false} }),

        grouppanel1: new FormGroupPanelModel({ caption: '分组面板', detailType: 'GROUPPANEL', name: 'grouppanel1', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.knowledgearticle.datapanel_form', extractMode: 'ITEM', details: [] } }),

        group1: new FormGroupPanelModel({ caption: '知识文章基本信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.knowledgearticle.datapanel_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: '知识文章', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfmajortext: new FormItemModel({ caption: '标题', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        languagelocalename: new FormItemModel({ caption: '语言', detailType: 'FORMITEM', name: 'languagelocalename', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        statecode: new FormItemModel({ caption: '状态', detailType: 'FORMITEM', name: 'statecode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        knowledgearticleid: new FormItemModel({ caption: '知识文章', detailType: 'FORMITEM', name: 'knowledgearticleid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

    };

	/**
	 * 表单 头信息编辑 事件
	 *
	 * @memberof @memberof DataPanelEditFormBase
	 */
    public button1_click($event: any): void {
        this.datapanel_button1_click(null, null, $event);

    }
}
/**
 * Infotabviewpanel 部件模型
 *
 * @export
 * @class InfotabviewpanelModel
 */
export default class InfotabviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof InfotabviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'billto_country',
      },
      {
        name: 'onholdtime',
      },
      {
        name: 'willcall',
      },
      {
        name: 'shipto_line1',
      },
      {
        name: 'freighttermscode',
      },
      {
        name: 'statecode',
      },
      {
        name: 'effectivefrom',
      },
      {
        name: 'totalamountlessfreight_base',
      },
      {
        name: 'quotename',
      },
      {
        name: 'totaldiscountamount_base',
      },
      {
        name: 'shipto_postalcode',
      },
      {
        name: 'totalamount',
      },
      {
        name: 'billto_addressid',
      },
      {
        name: 'updateman',
      },
      {
        name: 'createman',
      },
      {
        name: 'totalamount_base',
      },
      {
        name: 'shipto_line2',
      },
      {
        name: 'totallineitemdiscountamount',
      },
      {
        name: 'timezoneruleversionnumber',
      },
      {
        name: 'totallineitemamount_base',
      },
      {
        name: 'shipto_contactname',
      },
      {
        name: 'overriddencreatedon',
      },
      {
        name: 'shipto_addressid',
      },
      {
        name: 'shipto_name',
      },
      {
        name: 'statuscode',
      },
      {
        name: 'quotenumber',
      },
      {
        name: 'billto_telephone',
      },
      {
        name: 'closedon',
      },
      {
        name: 'freightamount_base',
      },
      {
        name: 'shipto_line3',
      },
      {
        name: 'shippingmethodcode',
      },
      {
        name: 'customerid',
      },
      {
        name: 'effectiveto',
      },
      {
        name: 'shipto_freighttermscode',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'totallineitemamount',
      },
      {
        name: 'shipto_fax',
      },
      {
        name: 'ownerid',
      },
      {
        name: 'accountname',
      },
      {
        name: 'emailaddress',
      },
      {
        name: 'importsequencenumber',
      },
      {
        name: 'utcconversiontimezonecode',
      },
      {
        name: 'customername',
      },
      {
        name: 'pricingerrorcode',
      },
      {
        name: 'uniquedscid',
      },
      {
        name: 'billto_line3',
      },
      {
        name: 'description',
      },
      {
        name: 'billto_composite',
      },
      {
        name: 'requestdeliveryby',
      },
      {
        name: 'totaltax',
      },
      {
        name: 'quote',
        prop: 'quoteid',
      },
      {
        name: 'processid',
      },
      {
        name: 'stageid',
      },
      {
        name: 'createdate',
      },
      {
        name: 'contactname',
      },
      {
        name: 'exchangerate',
      },
      {
        name: 'versionnumber',
      },
      {
        name: 'shipto_country',
      },
      {
        name: 'expireson',
      },
      {
        name: 'ownertype',
      },
      {
        name: 'billto_postalcode',
      },
      {
        name: 'billto_line2',
      },
      {
        name: 'billto_name',
      },
      {
        name: 'billto_line1',
      },
      {
        name: 'revisionnumber',
      },
      {
        name: 'billto_contactname',
      },
      {
        name: 'shipto_stateorprovince',
      },
      {
        name: 'billto_city',
      },
      {
        name: 'lastonholdtime',
      },
      {
        name: 'discountpercentage',
      },
      {
        name: 'shipto_city',
      },
      {
        name: 'customertype',
      },
      {
        name: 'discountamount',
      },
      {
        name: 'shipto_composite',
      },
      {
        name: 'shipto_telephone',
      },
      {
        name: 'billto_stateorprovince',
      },
      {
        name: 'paymenttermscode',
      },
      {
        name: 'ownername',
      },
      {
        name: 'totalamountlessfreight',
      },
      {
        name: 'traversedpath',
      },
      {
        name: 'billto_fax',
      },
      {
        name: 'totaldiscountamount',
      },
      {
        name: 'totaltax_base',
      },
      {
        name: 'discountamount_base',
      },
      {
        name: 'freightamount',
      },
      {
        name: 'currencyname',
      },
      {
        name: 'pricelevelname',
      },
      {
        name: 'opportunityname',
      },
      {
        name: 'slaname',
      },
      {
        name: 'campaignname',
      },
      {
        name: 'pricelevelid',
      },
      {
        name: 'slaid',
      },
      {
        name: 'transactioncurrencyid',
      },
      {
        name: 'opportunityid',
      },
      {
        name: 'campaignid',
      },
    ]
  }


}
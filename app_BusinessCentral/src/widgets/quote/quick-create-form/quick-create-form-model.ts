/**
 * QuickCreate 部件模型
 *
 * @export
 * @class QuickCreateModel
 */
export default class QuickCreateModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof QuickCreateModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'quoteid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'quotename',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'quotenumber',
        prop: 'quotenumber',
        dataType: 'TEXT',
      },
      {
        name: 'quotename',
        prop: 'quotename',
        dataType: 'TEXT',
      },
      {
        name: 'transactioncurrencyname',
        prop: 'currencyname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'opportunityname',
        prop: 'opportunityname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'customerid',
        prop: 'customerid',
        dataType: 'TEXT',
      },
      {
        name: 'pricelevelname',
        prop: 'pricelevelname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'transactioncurrencyid',
        prop: 'transactioncurrencyid',
        dataType: 'PICKUP',
      },
      {
        name: 'opportunityid',
        prop: 'opportunityid',
        dataType: 'PICKUP',
      },
      {
        name: 'pricelevelid',
        prop: 'pricelevelid',
        dataType: 'PICKUP',
      },
      {
        name: 'quoteid',
        prop: 'quoteid',
        dataType: 'GUID',
      },
      {
        name: 'quote',
        prop: 'quoteid',
        dataType: 'FONTKEY',
      },
    ]
  }

}
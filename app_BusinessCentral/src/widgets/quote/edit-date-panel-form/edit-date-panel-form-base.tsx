import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, EditFormControlBase } from '@/studio-core';
import QuoteService from '@/service/quote/quote-service';
import Edit_DatePanelService from './edit-date-panel-form-service';
import QuoteUIService from '@/uiservice/quote/quote-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {Edit_DatePanelEditFormBase}
 */
export class Edit_DatePanelEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof Edit_DatePanelEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {Edit_DatePanelService}
     * @memberof Edit_DatePanelEditFormBase
     */
    public service: Edit_DatePanelService = new Edit_DatePanelService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {QuoteService}
     * @memberof Edit_DatePanelEditFormBase
     */
    public appEntityService: QuoteService = new QuoteService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Edit_DatePanelEditFormBase
     */
    protected appDeName: string = 'quote';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof Edit_DatePanelEditFormBase
     */
    protected appDeLogicName: string = '报价单';

    /**
     * 界面UI服务对象
     *
     * @type {QuoteUIService}
     * @memberof Edit_DatePanelBase
     */  
    public appUIService:QuoteUIService = new QuoteUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof Edit_DatePanelEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        totalamount: null,
        effectivefrom: null,
        effectiveto: null,
        ownername: null,
        quoteid: null,
        quote:null,
    };

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Edit_DatePanelEditFormBase
     */
    public rules: any = {
        ownername: [
            { required: true, type: 'string', message: '负责人 值不能为空', trigger: 'change' },
            { required: true, type: 'string', message: '负责人 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Edit_DatePanelBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof Edit_DatePanelEditFormBase
     */
    public detailsModel: any = {
        group1: new FormGroupPanelModel({ caption: '报价单基本信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.quote.edit_datepanel_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: '报价单', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfmajortext: new FormItemModel({ caption: '报价名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        totalamount: new FormItemModel({ caption: '总金额', detailType: 'FORMITEM', name: 'totalamount', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        effectivefrom: new FormItemModel({ caption: '开始生效日期', detailType: 'FORMITEM', name: 'effectivefrom', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        effectiveto: new FormItemModel({ caption: '有效截止时间', detailType: 'FORMITEM', name: 'effectiveto', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        ownername: new FormItemModel({ caption: '负责人', detailType: 'FORMITEM', name: 'ownername', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        quoteid: new FormItemModel({ caption: '报价单', detailType: 'FORMITEM', name: 'quoteid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

    };
}
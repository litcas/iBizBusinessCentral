/**
 * DataPanel 部件模型
 *
 * @export
 * @class DataPanelModel
 */
export default class DataPanelModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof DataPanelModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'quoteid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'quotename',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'totalamount',
        prop: 'totalamount',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'effectivefrom',
        prop: 'effectivefrom',
        dataType: 'DATETIME',
      },
      {
        name: 'effectiveto',
        prop: 'effectiveto',
        dataType: 'DATETIME',
      },
      {
        name: 'ownername',
        prop: 'ownername',
        dataType: 'TEXT',
      },
      {
        name: 'quoteid',
        prop: 'quoteid',
        dataType: 'GUID',
      },
      {
        name: 'quote',
        prop: 'quoteid',
        dataType: 'FONTKEY',
      },
    ]
  }

}
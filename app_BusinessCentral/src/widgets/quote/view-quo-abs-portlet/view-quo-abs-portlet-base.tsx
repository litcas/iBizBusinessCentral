import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, MainControlBase } from '@/studio-core';
import QuoteService from '@/service/quote/quote-service';
import View_QuoAbsService from './view-quo-abs-portlet-service';
import QuoteUIService from '@/uiservice/quote/quote-ui-service';
import { Environment } from '@/environments/environment';


/**
 * dashboard_sysportlet1部件基类
 *
 * @export
 * @class MainControlBase
 * @extends {View_QuoAbsPortletBase}
 */
export class View_QuoAbsPortletBase extends MainControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof View_QuoAbsPortletBase
     */
    protected controlType: string = 'PORTLET';

    /**
     * 建构部件服务对象
     *
     * @type {View_QuoAbsService}
     * @memberof View_QuoAbsPortletBase
     */
    public service: View_QuoAbsService = new View_QuoAbsService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {QuoteService}
     * @memberof View_QuoAbsPortletBase
     */
    public appEntityService: QuoteService = new QuoteService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof View_QuoAbsPortletBase
     */
    protected appDeName: string = 'quote';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof View_QuoAbsPortletBase
     */
    protected appDeLogicName: string = '报价单';

    /**
     * 界面UI服务对象
     *
     * @type {QuoteUIService}
     * @memberof View_QuoAbsBase
     */  
    public appUIService:QuoteUIService = new QuoteUIService(this.$store);

    /**
     * 长度
     *
     * @type {number}
     * @memberof View_QuoAbs
     */
    @Prop() public height?: number;

    /**
     * 宽度
     *
     * @type {number}
     * @memberof View_QuoAbs
     */
    @Prop() public width?: number;



    /**
     * 是否自适应大小
     *
     * @returns {boolean}
     * @memberof View_QuoAbsBase
     */
    @Prop({default: false})public isAdaptiveSize!: boolean;

    /**
     * 获取多项数据
     *
     * @returns {any[]}
     * @memberof View_QuoAbsBase
     */
    public getDatas(): any[] {
        return [];
    }

    /**
     * 获取单项树
     *
     * @returns {*}
     * @memberof View_QuoAbsBase
     */
    public getData(): any {
        return {};
    }

    /**
     * 获取高度
     *
     * @returns {any[]}
     * @memberof View_QuoAbsBase
     */
    get getHeight(): any{
        if(!this.$util.isEmpty(this.height) && !this.$util.isNumberNaN(this.height)){
            if(this.height == 0){
                return 'auto';
            } else {
                return this.height+'px';
            }
        } else {
            return 'auto';
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof View_QuoAbsBase
     */
    public created() {
        this.afterCreated();
    }

    /**
     * 执行created后的逻辑
     *
     *  @memberof View_QuoAbsBase
     */    
    public afterCreated(){
        if (this.viewState) {
            this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }) => {
                if (!Object.is(tag, this.name)) {
                    return;
                }
                const refs: any = this.$refs;
                Object.keys(refs).forEach((_name: string) => {
                    this.viewState.next({ tag: _name, action: action, data: data });
                });
            });
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof View_QuoAbsBase
     */
    public destroyed() {
        this.afterDestroy();
    }

    /**
     * 执行destroyed后的逻辑
     *
     * @memberof View_QuoAbsBase
     */
    public afterDestroy() {
        if (this.viewStateEvent) {
            this.viewStateEvent.unsubscribe();
        }
    }


}

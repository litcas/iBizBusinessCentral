/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'statecode',
          prop: 'statecode',
          dataType: 'NSCODELIST',
        },
        {
          name: 'customerid',
          prop: 'customerid',
          dataType: 'TEXT',
        },
        {
          name: 'transactioncurrencyid',
          prop: 'transactioncurrencyid',
          dataType: 'PICKUP',
        },
        {
          name: 'quotename',
          prop: 'quotename',
          dataType: 'TEXT',
        },
        {
          name: 'campaignid',
          prop: 'campaignid',
          dataType: 'PICKUP',
        },
        {
          name: 'opportunityid',
          prop: 'opportunityid',
          dataType: 'PICKUP',
        },
        {
          name: 'slaid',
          prop: 'slaid',
          dataType: 'PICKUP',
        },
        {
          name: 'pricelevelid',
          prop: 'pricelevelid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'quotename',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'quoteid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'quoteid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'totalamount',
          prop: 'totalamount',
          dataType: 'BIGDECIMAL',
        },
        {
          name: 'quote',
          prop: 'quoteid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}
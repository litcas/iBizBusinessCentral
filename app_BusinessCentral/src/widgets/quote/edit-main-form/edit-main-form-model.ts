/**
 * Edit_Main 部件模型
 *
 * @export
 * @class Edit_MainModel
 */
export default class Edit_MainModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Edit_MainModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'quoteid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'quotename',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'quotenumber',
        prop: 'quotenumber',
        dataType: 'TEXT',
      },
      {
        name: 'quotename',
        prop: 'quotename',
        dataType: 'TEXT',
      },
      {
        name: 'transactioncurrencyname',
        prop: 'currencyname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'opportunityname',
        prop: 'opportunityname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'customerid',
        prop: 'customerid',
        dataType: 'TEXT',
      },
      {
        name: 'pricelevelname',
        prop: 'pricelevelname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'statecode',
        prop: 'statecode',
        dataType: 'NSCODELIST',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'paymenttermscode',
        prop: 'paymenttermscode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'freighttermscode',
        prop: 'freighttermscode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'billto_postalcode',
        prop: 'billto_postalcode',
        dataType: 'TEXT',
      },
      {
        name: 'formitem',
      },
      {
        name: 'billto_country',
        prop: 'billto_country',
        dataType: 'TEXT',
      },
      {
        name: 'billto_stateorprovince',
        prop: 'billto_stateorprovince',
        dataType: 'TEXT',
      },
      {
        name: 'billto_city',
        prop: 'billto_city',
        dataType: 'TEXT',
      },
      {
        name: 'billto_line1',
        prop: 'billto_line1',
        dataType: 'TEXT',
      },
      {
        name: 'shippingmethodcode',
        prop: 'shippingmethodcode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'shipto_postalcode',
        prop: 'shipto_postalcode',
        dataType: 'TEXT',
      },
      {
        name: 'willcall',
        prop: 'willcall',
        dataType: 'YESNO',
      },
      {
        name: 'transactioncurrencyid',
        prop: 'transactioncurrencyid',
        dataType: 'PICKUP',
      },
      {
        name: 'opportunityid',
        prop: 'opportunityid',
        dataType: 'PICKUP',
      },
      {
        name: 'pricelevelid',
        prop: 'pricelevelid',
        dataType: 'PICKUP',
      },
      {
        name: 'quoteid',
        prop: 'quoteid',
        dataType: 'GUID',
      },
      {
        name: 'quote',
        prop: 'quoteid',
        dataType: 'FONTKEY',
      },
    ]
  }

}
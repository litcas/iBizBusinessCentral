/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'extendedamount',
          prop: 'extendedamount',
          dataType: 'BIGDECIMAL',
        },
        {
          name: 'productid',
          prop: 'productid',
          dataType: 'PICKUP',
        },
        {
          name: 'parentbundleidref',
          prop: 'parentbundleidref',
          dataType: 'PICKUP',
        },
        {
          name: 'priceperunit',
          prop: 'priceperunit',
          dataType: 'BIGDECIMAL',
        },
        {
          name: 'transactioncurrencyid',
          prop: 'transactioncurrencyid',
          dataType: 'PICKUP',
        },
        {
          name: 'quantity',
          prop: 'quantity',
          dataType: 'BIGDECIMAL',
        },
        {
          name: 'uomid',
          prop: 'uomid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'quotedetailname',
          dataType: 'TEXT',
        },
        {
          name: 'quoteid',
          prop: 'quoteid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfdataaccaction',
          prop: 'quotedetailid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'quotedetailid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'productname',
          prop: 'productname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'quotedetail',
          prop: 'quotedetailid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}
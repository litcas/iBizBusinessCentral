/**
 * EF_MasterQuick 部件模型
 *
 * @export
 * @class EF_MasterQuickModel
 */
export default class EF_MasterQuickModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof EF_MasterQuickModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'employeeid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'employeename',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'employeecode',
        prop: 'employeecode',
        dataType: 'TEXT',
      },
      {
        name: 'employeename',
        prop: 'employeename',
        dataType: 'TEXT',
      },
      {
        name: 'certtype',
        prop: 'certtype',
        dataType: 'SSCODELIST',
      },
      {
        name: 'certnum',
        prop: 'certnum',
        dataType: 'TEXT',
      },
      {
        name: 'employeeid',
        prop: 'employeeid',
        dataType: 'GUID',
      },
      {
        name: 'employee',
        prop: 'employeeid',
        dataType: 'FONTKEY',
      },
    ]
  }

}
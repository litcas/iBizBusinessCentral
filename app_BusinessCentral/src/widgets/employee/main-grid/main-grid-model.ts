/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'organizationname',
          prop: 'organizationname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'organizationid',
          prop: 'organizationid',
          dataType: 'PICKUP',
        },
        {
          name: 'employeename',
          prop: 'employeename',
          dataType: 'TEXT',
        },
        {
          name: 'certtype',
          prop: 'certtype',
          dataType: 'SSCODELIST',
        },
        {
          name: 'certnum',
          prop: 'certnum',
          dataType: 'TEXT',
        },
        {
          name: 'employeecode',
          prop: 'employeecode',
          dataType: 'TEXT',
        },
        {
          name: 'srfmajortext',
          prop: 'employeename',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'employeeid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'employeeid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'employee',
          prop: 'employeeid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}
/**
 * AbstractEdit 部件模型
 *
 * @export
 * @class AbstractEditModel
 */
export default class AbstractEditModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof AbstractEditModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'listid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'listname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'listname',
        prop: 'listname',
        dataType: 'TEXT',
      },
      {
        name: 'type',
        prop: 'type',
        dataType: 'YESNO',
      },
      {
        name: 'purpose',
        prop: 'purpose',
        dataType: 'TEXT',
      },
      {
        name: 'membertype',
        prop: 'membertype',
        dataType: 'INT',
      },
      {
        name: 'source',
        prop: 'source',
        dataType: 'TEXT',
      },
      {
        name: 'transactioncurrencyname',
        prop: 'currencyname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'cost',
        prop: 'cost',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'ownername',
        prop: 'ownername',
        dataType: 'TEXT',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'transactioncurrencyid',
        prop: 'transactioncurrencyid',
        dataType: 'PICKUP',
      },
      {
        name: 'listid',
        prop: 'listid',
        dataType: 'GUID',
      },
      {
        name: 'ibizlist',
        prop: 'listid',
        dataType: 'FONTKEY',
      },
    ]
  }

}
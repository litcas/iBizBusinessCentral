import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, TabExpPanelControlBase } from '@/studio-core';
import IBizListService from '@/service/ibiz-list/ibiz-list-service';
import StateTabViewtabexppanelService from './state-tab-viewtabexppanel-tabexppanel-service';
import IBizListUIService from '@/uiservice/ibiz-list/ibiz-list-ui-service';


/**
 * tabexppanel部件基类
 *
 * @export
 * @class TabExpPanelControlBase
 * @extends {StateTabViewtabexppanelTabexppanelBase}
 */
export class StateTabViewtabexppanelTabexppanelBase extends TabExpPanelControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof StateTabViewtabexppanelTabexppanelBase
     */
    protected controlType: string = 'TABEXPPANEL';

    /**
     * 建构部件服务对象
     *
     * @type {StateTabViewtabexppanelService}
     * @memberof StateTabViewtabexppanelTabexppanelBase
     */
    public service: StateTabViewtabexppanelService = new StateTabViewtabexppanelService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {IBizListService}
     * @memberof StateTabViewtabexppanelTabexppanelBase
     */
    public appEntityService: IBizListService = new IBizListService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof StateTabViewtabexppanelTabexppanelBase
     */
    protected appDeName: string = 'ibizlist';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof StateTabViewtabexppanelTabexppanelBase
     */
    protected appDeLogicName: string = '市场营销列表';

    /**
     * 界面UI服务对象
     *
     * @type {IBizListUIService}
     * @memberof StateTabViewtabexppanelBase
     */  
    public appUIService:IBizListUIService = new IBizListUIService(this.$store);
    /**
     * 是否初始化
     *
     * @protected
     * @returns {any}
     * @memberof StateTabViewtabexppanel
     */
    protected isInit: any = {
        tabviewpanel:  true ,
        tabviewpanel2:  false ,
        tabviewpanel3:  false ,
    }

    /**
     * 被激活的分页面板
     *
     * @protected
     * @type {string}
     * @memberof StateTabViewtabexppanel
     */
    protected activatedTabViewPanel: string = 'tabviewpanel';

    /**
     * 组件创建完毕
     *
     * @protected
     * @memberof StateTabViewtabexppanel
     */
    protected ctrlCreated(): void {
        //设置分页导航srfparentdename和srfparentkey
        if (this.context.ibizlist) {
            Object.assign(this.context, { srfparentdename: 'IBizList', srfparentkey: this.context.ibizlist });
        }
        super.ctrlCreated();
    }
}
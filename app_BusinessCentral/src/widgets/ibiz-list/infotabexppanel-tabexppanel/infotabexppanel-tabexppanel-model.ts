/**
 * Infotabexppanel 部件模型
 *
 * @export
 * @class InfotabexppanelModel
 */
export default class InfotabexppanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof InfotabexppanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'query',
      },
      {
        name: 'updateman',
      },
      {
        name: 'lockstatus',
      },
      {
        name: 'purpose',
      },
      {
        name: 'lastusedon',
      },
      {
        name: 'ownerid',
      },
      {
        name: 'ownername',
      },
      {
        name: 'ibizlist',
        prop: 'listid',
      },
      {
        name: 'membertype',
      },
      {
        name: 'donotsendonoptout',
      },
      {
        name: 'versionnumber',
      },
      {
        name: 'cost',
      },
      {
        name: 'statecode',
      },
      {
        name: 'exchangerate',
      },
      {
        name: 'ownertype',
      },
      {
        name: 'listname',
      },
      {
        name: 'statuscode',
      },
      {
        name: 'description',
      },
      {
        name: 'type',
      },
      {
        name: 'createdate',
      },
      {
        name: 'importsequencenumber',
      },
      {
        name: 'createman',
      },
      {
        name: 'processid',
      },
      {
        name: 'cost_base',
      },
      {
        name: 'membercount',
      },
      {
        name: 'ignoreinactivelistmembers',
      },
      {
        name: 'stageid',
      },
      {
        name: 'source',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'utcconversiontimezonecode',
      },
      {
        name: 'createdfromcode',
      },
      {
        name: 'overriddencreatedon',
      },
      {
        name: 'timezoneruleversionnumber',
      },
      {
        name: 'traversedpath',
      },
      {
        name: 'currencyname',
      },
      {
        name: 'transactioncurrencyid',
      },
    ]
  }


}
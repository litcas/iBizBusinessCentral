/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'lastusedon',
          prop: 'lastusedon',
          dataType: 'DATETIME',
        },
        {
          name: 'transactioncurrencyid',
          prop: 'transactioncurrencyid',
          dataType: 'PICKUP',
        },
        {
          name: 'purpose',
          prop: 'purpose',
          dataType: 'TEXT',
        },
        {
          name: 'createdfromcode',
          prop: 'createdfromcode',
          dataType: 'SSCODELIST',
        },
        {
          name: 'type',
          prop: 'type',
          dataType: 'YESNO',
        },
        {
          name: 'srfmajortext',
          prop: 'listname',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'listid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'listid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'listname',
          prop: 'listname',
          dataType: 'TEXT',
        },
        {
          name: 'ibizlist',
          prop: 'listid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}
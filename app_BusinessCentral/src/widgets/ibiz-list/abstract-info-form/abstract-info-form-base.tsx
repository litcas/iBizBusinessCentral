import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, EditFormControlBase } from '@/studio-core';
import IBizListService from '@/service/ibiz-list/ibiz-list-service';
import AbstractInfoService from './abstract-info-form-service';
import IBizListUIService from '@/uiservice/ibiz-list/ibiz-list-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {AbstractInfoEditFormBase}
 */
export class AbstractInfoEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof AbstractInfoEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {AbstractInfoService}
     * @memberof AbstractInfoEditFormBase
     */
    public service: AbstractInfoService = new AbstractInfoService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {IBizListService}
     * @memberof AbstractInfoEditFormBase
     */
    public appEntityService: IBizListService = new IBizListService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof AbstractInfoEditFormBase
     */
    protected appDeName: string = 'ibizlist';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof AbstractInfoEditFormBase
     */
    protected appDeLogicName: string = '市场营销列表';

    /**
     * 界面UI服务对象
     *
     * @type {IBizListUIService}
     * @memberof AbstractInfoBase
     */  
    public appUIService:IBizListUIService = new IBizListUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof AbstractInfoEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        listname: null,
        type: null,
        purpose: null,
        membertype: null,
        source: null,
        transactioncurrencyname: null,
        cost: null,
        ownername: null,
        description: null,
        listid: null,
        ibizlist:null,
    };

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof AbstractInfoEditFormBase
     */
    public rules: any = {
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof AbstractInfoBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof AbstractInfoEditFormBase
     */
    public detailsModel: any = {
        group1: new FormGroupPanelModel({ caption: '列表信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.ibizlist.abstractinfo_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: '列表', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfmajortext: new FormItemModel({ caption: '名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        listname: new FormItemModel({ caption: '名称', detailType: 'FORMITEM', name: 'listname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        type: new FormItemModel({ caption: '类型', detailType: 'FORMITEM', name: 'type', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        purpose: new FormItemModel({ caption: '用途', detailType: 'FORMITEM', name: 'purpose', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        membertype: new FormItemModel({ caption: '目标对象', detailType: 'FORMITEM', name: 'membertype', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        source: new FormItemModel({ caption: '来源', detailType: 'FORMITEM', name: 'source', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        transactioncurrencyname: new FormItemModel({ caption: '货币', detailType: 'FORMITEM', name: 'transactioncurrencyname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        cost: new FormItemModel({ caption: '成本', detailType: 'FORMITEM', name: 'cost', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        ownername: new FormItemModel({ caption: '负责人', detailType: 'FORMITEM', name: 'ownername', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        description: new FormItemModel({ caption: '说明', detailType: 'FORMITEM', name: 'description', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        listid: new FormItemModel({ caption: '列表', detailType: 'FORMITEM', name: 'listid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

    };
}
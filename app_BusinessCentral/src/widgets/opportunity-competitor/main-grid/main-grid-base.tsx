import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, GridControlBase } from '@/studio-core';
import OpportunityCompetitorService from '@/service/opportunity-competitor/opportunity-competitor-service';
import MainService from './main-grid-service';
import OpportunityCompetitorUIService from '@/uiservice/opportunity-competitor/opportunity-competitor-ui-service';
import { FormItemModel } from '@/model/form-detail';


/**
 * grid部件基类
 *
 * @export
 * @class GridControlBase
 * @extends {MainGridBase}
 */
export class MainGridBase extends GridControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected controlType: string = 'GRID';

    /**
     * 建构部件服务对象
     *
     * @type {MainService}
     * @memberof MainGridBase
     */
    public service: MainService = new MainService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {OpportunityCompetitorService}
     * @memberof MainGridBase
     */
    public appEntityService: OpportunityCompetitorService = new OpportunityCompetitorService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeName: string = 'opportunitycompetitor';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeLogicName: string = '商机对手';

    /**
     * 界面UI服务对象
     *
     * @type {OpportunityCompetitorUIService}
     * @memberof MainBase
     */  
    public appUIService:OpportunityCompetitorUIService = new OpportunityCompetitorUIService(this.$store);

    /**
     * 界面行为模型
     *
     * @type {*}
     * @memberof MainBase
     */  
    public ActionModel: any = {
    };

    /**
     * 本地缓存标识
     *
     * @protected
     * @type {string}
     * @memberof MainBase
     */
    protected localStorageTag: string = 'opportunitycompetitor_main_grid';

    /**
     * 所有列成员
     *
     * @type {any[]}
     * @memberof MainGridBase
     */
    public allColumns: any[] = [
        {
            name: 'entityname',
            label: '商机',
            langtag: 'entities.opportunitycompetitor.main_grid.columns.entityname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'entity2name',
            label: '竞争对手',
            langtag: 'entities.opportunitycompetitor.main_grid.columns.entity2name',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'originatingleadname',
            label: '潜在顾客',
            langtag: 'entities.opportunitycompetitor.main_grid.columns.originatingleadname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'estimatedvalue',
            label: '预计收入',
            langtag: 'entities.opportunitycompetitor.main_grid.columns.estimatedvalue',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'estimatedclosedate',
            label: '预计结束日期',
            langtag: 'entities.opportunitycompetitor.main_grid.columns.estimatedclosedate',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'actualvalue',
            label: '实际收入',
            langtag: 'entities.opportunitycompetitor.main_grid.columns.actualvalue',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
    ]

    /**
     * 获取表格行模型
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public getGridRowModel(){
        return {
          srfkey: new FormItemModel(),
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public rules: any = {
        srfkey: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '关系标识 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '关系标识 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 获取对应列class
     *
     * @type {*}
     * @memberof MainBase
     */
    public hasRowEdit: any = {
        'entityname':false,
        'entity2name':false,
        'originatingleadname':false,
        'estimatedvalue':false,
        'estimatedclosedate':false,
        'actualvalue':false,
    };

    /**
     * 获取对应列class
     *
     * @param {*} $args row 行数据，column 列数据，rowIndex 行索引，列索引
     * @returns {void}
     * @memberof MainBase
     */
    public getCellClassName(args: {row: any, column: any, rowIndex: number, columnIndex: number}): any {
        return ( this.hasRowEdit[args.column.property] && this.actualIsOpenEdit ) ? "edit-cell" : "info-cell";
    }


    /**
     * 导出数据格式化
     *
     * @param {*} filterVal
     * @param {*} jsonData
     * @param {any[]} [codelistColumns=[]]
     * @returns {Promise<any>}
     * @memberof MainGridBase
     */
    public async formatExcelData(filterVal: any, jsonData: any, codelistColumns?: any[]): Promise<any> {
        return super.formatExcelData(filterVal, jsonData, [
        ]);
    }

}
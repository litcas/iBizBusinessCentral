import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * CenteralPortal_db 部件服务对象
 *
 * @export
 * @class CenteralPortal_dbService
 */
export default class CenteralPortal_dbService extends ControlService {
}
/**
 * CenteralPortal_db 部件模型
 *
 * @export
 * @class CenteralPortal_dbModel
 */
export default class CenteralPortal_dbModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof CenteralPortal_dbModel
    */
  public getDataItems(): any[] {
    return [
    ]
  }


}
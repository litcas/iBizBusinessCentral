/**
 * Setting 部件模型
 *
 * @export
 * @class SettingModel
 */
export default class SettingModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof SettingModel
    */
  public getDataItems(): any[] {
    return [
    ]
  }


}

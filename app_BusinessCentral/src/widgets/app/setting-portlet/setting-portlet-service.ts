import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * Setting 部件服务对象
 *
 * @export
 * @class SettingService
 */
export default class SettingService extends ControlService {
}

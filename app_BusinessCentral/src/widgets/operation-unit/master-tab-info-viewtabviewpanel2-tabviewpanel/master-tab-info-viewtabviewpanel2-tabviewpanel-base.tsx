import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, MainControlBase } from '@/studio-core';
import OperationUnitService from '@/service/operation-unit/operation-unit-service';
import MasterTabInfoViewtabviewpanel2Service from './master-tab-info-viewtabviewpanel2-tabviewpanel-service';
import OperationUnitUIService from '@/uiservice/operation-unit/operation-unit-ui-service';


/**
 * tabviewpanel2部件基类
 *
 * @export
 * @class MainControlBase
 * @extends {MasterTabInfoViewtabviewpanel2TabviewpanelBase}
 */
export class MasterTabInfoViewtabviewpanel2TabviewpanelBase extends MainControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof MasterTabInfoViewtabviewpanel2TabviewpanelBase
     */
    protected controlType: string = 'TABVIEWPANEL';

    /**
     * 建构部件服务对象
     *
     * @type {MasterTabInfoViewtabviewpanel2Service}
     * @memberof MasterTabInfoViewtabviewpanel2TabviewpanelBase
     */
    public service: MasterTabInfoViewtabviewpanel2Service = new MasterTabInfoViewtabviewpanel2Service({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {OperationUnitService}
     * @memberof MasterTabInfoViewtabviewpanel2TabviewpanelBase
     */
    public appEntityService: OperationUnitService = new OperationUnitService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MasterTabInfoViewtabviewpanel2TabviewpanelBase
     */
    protected appDeName: string = 'operationunit';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof MasterTabInfoViewtabviewpanel2TabviewpanelBase
     */
    protected appDeLogicName: string = '运营单位';

    /**
     * 界面UI服务对象
     *
     * @type {OperationUnitUIService}
     * @memberof MasterTabInfoViewtabviewpanel2Base
     */  
    public appUIService:OperationUnitUIService = new OperationUnitUIService(this.$store);

    /**
     * 导航模式下项是否激活
     *
     * @type {*}
     * @memberof MasterTabInfoViewtabviewpanel2
     */
    @Prop()
    public expActive!: any;

    /**
     * 获取多项数据
     *
     * @returns {any[]}
     * @memberof MasterTabInfoViewtabviewpanel2
     */
    public getDatas(): any[] {
        return [];
    }

    /**
     * 获取单项树
     *
     * @returns {*}
     * @memberof MasterTabInfoViewtabviewpanel2
     */
    public getData(): any {
        return null;
    }

    /**
     * 是否被激活
     *
     * @type {boolean}
     * @memberof MasterTabInfoViewtabviewpanel2
     */
    public isActivied: boolean = true;

    /**
     * 局部上下文
     *
     * @type {*}
     * @memberof MasterTabInfoViewtabviewpanel2
     */
    public localContext: any = null;

    /**
     * 局部视图参数
     *
     * @type {*}
     * @memberof MasterTabInfoViewtabviewpanel2
     */
    public localViewParam: any = null;

    /**
     * 传入上下文
     *
     * @type {string}
     * @memberof TabExpViewtabviewpanel
     */
    public viewdata: string = JSON.stringify(this.context);

    /**
     * 传入视图参数
     *
     * @type {string}
     * @memberof PickupViewpickupviewpanel
     */
    public viewparam: string = JSON.stringify(this.viewparams);

    /**
     * 视图面板过滤项
     *
     * @type {string}
     * @memberof MasterTabInfoViewtabviewpanel2
     */
    public navfilter: string = "";
             
    /**
     * vue 生命周期
     *
     * @returns
     * @memberof MasterTabInfoViewtabviewpanel2
     */
    public created() {
        this.afterCreated();
    }

    /**
     * 执行created后的逻辑
     *
     *  @memberof MasterTabInfoViewtabviewpanel2
     */    
    public afterCreated(){
        this.initNavParam();
        if (this.viewState) {
            this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }) => {
                if (!Object.is(tag, this.name)) {
                    return;
                }
                this.$forceUpdate();
                this.initNavParam();
            });
        }
    }

    /**
     * 初始化导航参数
     *
     * @memberof MasterTabInfoViewtabviewpanel2
     */
    public initNavParam(){
        if(!Object.is(this.navfilter,"")){
            Object.assign(this.viewparams,{[this.navfilter]:this.context['majorentity']})
        }
        if(this.localContext && Object.keys(this.localContext).length >0){
            let _context:any = this.$util.computedNavData({},this.context,this.viewparams,this.localContext);
            Object.assign(this.context,_context);
        }
        if(this.localViewParam && Object.keys(this.localViewParam).length >0){
            let _param:any = this.$util.computedNavData({},this.context,this.viewparams,this.localViewParam);
            Object.assign(this.viewparams,_param);
        }
        this.viewdata =JSON.stringify(this.context);
        this.viewparam = JSON.stringify(this.viewparams);
    }

    /**
     * 视图数据变化
     *
     * @memberof  MasterTabInfoViewtabviewpanel2
     */
    public viewDatasChange($event:any){
        this.$emit('viewpanelDatasChange',$event);
    }

    /**
     * vue 生命周期
     *
     * @memberof MasterTabInfoViewtabviewpanel2
     */
    public destroyed() {
        this.afterDestroy();
    }

    /**
     * 执行destroyed后的逻辑
     *
     * @memberof MasterTabInfoViewtabviewpanel2
     */
    public afterDestroy() {
        if (this.viewStateEvent) {
            this.viewStateEvent.unsubscribe();
        }
    }
}
/**
 * MasterTabInfoViewtabviewpanel 部件模型
 *
 * @export
 * @class MasterTabInfoViewtabviewpanelModel
 */
export default class MasterTabInfoViewtabviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof MasterTabInfoViewtabviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'updatedate',
      },
      {
        name: 'createman',
      },
      {
        name: 'updateman',
      },
      {
        name: 'operationunit',
        prop: 'operationunitid',
      },
      {
        name: 'createdate',
      },
      {
        name: 'operationunitname',
      },
      {
        name: 'organizationtype',
      },
      {
        name: 'showorder',
      },
      {
        name: 'orglevel',
      },
      {
        name: 'shortname',
      },
      {
        name: 'orgcode',
      },
      {
        name: 'operationunittype',
      },
    ]
  }


}
/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof MainModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'connectionid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'connectionname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'connectionname',
        prop: 'connectionname',
        dataType: 'TEXT',
      },
      {
        name: 'record1id',
        prop: 'record1id',
        dataType: 'TEXT',
      },
      {
        name: 'record1rolename',
        prop: 'record1rolename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'record2rolename',
        prop: 'record2rolename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'record2id',
        prop: 'record2id',
        dataType: 'TEXT',
      },
      {
        name: 'record2idobjecttypecode',
        prop: 'record2idobjecttypecode',
        dataType: 'TEXT',
      },
      {
        name: 'record2objecttypecode',
        prop: 'record2objecttypecode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'statecode',
        prop: 'statecode',
        dataType: 'NSCODELIST',
      },
      {
        name: 'statuscode',
        prop: 'statuscode',
        dataType: 'NSCODELIST',
      },
      {
        name: 'effectiveend',
        prop: 'effectiveend',
        dataType: 'DATETIME',
      },
      {
        name: 'effectivestart',
        prop: 'effectivestart',
        dataType: 'DATETIME',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'ownerid',
        prop: 'ownerid',
        dataType: 'TEXT',
      },
      {
        name: 'ownertype',
        prop: 'ownertype',
        dataType: 'TEXT',
      },
      {
        name: 'ownername',
        prop: 'ownername',
        dataType: 'TEXT',
      },
      {
        name: 'connectionid',
        prop: 'connectionid',
        dataType: 'GUID',
      },
      {
        name: 'record1roleid',
        prop: 'record1roleid',
        dataType: 'PICKUP',
      },
      {
        name: 'record2roleid',
        prop: 'record2roleid',
        dataType: 'PICKUP',
      },
      {
        name: 'connection',
        prop: 'connectionid',
        dataType: 'FONTKEY',
      },
    ]
  }

}
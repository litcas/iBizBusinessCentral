import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, SearchFormControlBase } from '@/studio-core';
import DictCatalogService from '@/service/dict-catalog/dict-catalog-service';
import DefaultService from './default-searchform-service';
import DictCatalogUIService from '@/uiservice/dict-catalog/dict-catalog-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';


/**
 * searchform部件基类
 *
 * @export
 * @class SearchFormControlBase
 * @extends {DefaultSearchFormBase}
 */
export class DefaultSearchFormBase extends SearchFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected controlType: string = 'SEARCHFORM';

    /**
     * 建构部件服务对象
     *
     * @type {DefaultService}
     * @memberof DefaultSearchFormBase
     */
    public service: DefaultService = new DefaultService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {DictCatalogService}
     * @memberof DefaultSearchFormBase
     */
    public appEntityService: DictCatalogService = new DictCatalogService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeName: string = 'dictcatalog';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeLogicName: string = '字典';

    /**
     * 界面UI服务对象
     *
     * @type {DictCatalogUIService}
     * @memberof DefaultBase
     */  
    public appUIService:DictCatalogUIService = new DictCatalogUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public data: any = {
        n_ccode_like: null,
        n_cname_like: null,
        n_cgroup_like: null,
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public detailsModel: any = {
        formpage1: new FormPageModel({ caption: '常规条件', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this })
, 
        n_ccode_like: new FormItemModel({ caption: '代码', detailType: 'FORMITEM', name: 'n_ccode_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_cname_like: new FormItemModel({ caption: '名称', detailType: 'FORMITEM', name: 'n_cname_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_cgroup_like: new FormItemModel({ caption: '分组', detailType: 'FORMITEM', name: 'n_cgroup_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
    };
}
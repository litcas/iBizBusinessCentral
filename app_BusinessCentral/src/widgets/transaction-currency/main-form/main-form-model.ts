/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof MainModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'transactioncurrencyid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'currencyname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'currencyname',
        prop: 'currencyname',
        dataType: 'TEXT',
      },
      {
        name: 'currencyprecision',
        prop: 'currencyprecision',
        dataType: 'INT',
      },
      {
        name: 'currencysymbol',
        prop: 'currencysymbol',
        dataType: 'TEXT',
      },
      {
        name: 'exchangerate',
        prop: 'exchangerate',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'statecode',
        prop: 'statecode',
        dataType: 'NSCODELIST',
      },
      {
        name: 'statuscode',
        prop: 'statuscode',
        dataType: 'NSCODELIST',
      },
      {
        name: 'isocurrencycode',
        prop: 'isocurrencycode',
        dataType: 'TEXT',
      },
      {
        name: 'createman',
        prop: 'createman',
        dataType: 'TEXT',
      },
      {
        name: 'createdate',
        prop: 'createdate',
        dataType: 'DATETIME',
      },
      {
        name: 'updateman',
        prop: 'updateman',
        dataType: 'TEXT',
      },
      {
        name: 'updatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'transactioncurrencyid',
        prop: 'transactioncurrencyid',
        dataType: 'GUID',
      },
      {
        name: 'transactioncurrency',
        prop: 'transactioncurrencyid',
        dataType: 'FONTKEY',
      },
    ]
  }

}
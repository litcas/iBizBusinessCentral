/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'updatedate',
      },
      {
        name: 'entityimageid',
      },
      {
        name: 'transactioncurrency',
        prop: 'transactioncurrencyid',
      },
      {
        name: 'versionnumber',
      },
      {
        name: 'entityimage_url',
      },
      {
        name: 'exchangerate',
      },
      {
        name: 'overriddencreatedon',
      },
      {
        name: 'createdate',
      },
      {
        name: 'uniquedscid',
      },
      {
        name: 'statecode',
      },
      {
        name: 'statuscode',
      },
      {
        name: 'updateman',
      },
      {
        name: 'currencyprecision',
      },
      {
        name: 'currencyname',
      },
      {
        name: 'createman',
      },
      {
        name: 'entityimage',
      },
      {
        name: 'isocurrencycode',
      },
      {
        name: 'entityimage_timestamp',
      },
      {
        name: 'currencysymbol',
      },
      {
        name: 'importsequencenumber',
      },
    ]
  }


}
import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, SearchFormControlBase } from '@/studio-core';
import SysUserService from '@/service/sys-user/sys-user-service';
import DefaultService from './default-searchform-service';
import SysUserUIService from '@/uiservice/sys-user/sys-user-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';


/**
 * searchform部件基类
 *
 * @export
 * @class SearchFormControlBase
 * @extends {DefaultSearchFormBase}
 */
export class DefaultSearchFormBase extends SearchFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected controlType: string = 'SEARCHFORM';

    /**
     * 建构部件服务对象
     *
     * @type {DefaultService}
     * @memberof DefaultSearchFormBase
     */
    public service: DefaultService = new DefaultService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {SysUserService}
     * @memberof DefaultSearchFormBase
     */
    public appEntityService: SysUserService = new SysUserService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeName: string = 'sysuser';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeLogicName: string = '系统用户';

    /**
     * 界面UI服务对象
     *
     * @type {SysUserUIService}
     * @memberof DefaultBase
     */  
    public appUIService:SysUserUIService = new SysUserUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public data: any = {
        n_loginname_like: null,
        n_personname_like: null,
        n_orgname_like: null,
        n_mdeptname_like: null,
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public detailsModel: any = {
        formpage1: new FormPageModel({ caption: '常规条件', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this })
, 
        n_loginname_like: new FormItemModel({ caption: '登录名(文本包含(%))', detailType: 'FORMITEM', name: 'n_loginname_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_personname_like: new FormItemModel({ caption: '用户姓名(文本包含(%))', detailType: 'FORMITEM', name: 'n_personname_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_orgname_like: new FormItemModel({ caption: '单位名称(文本包含(%))', detailType: 'FORMITEM', name: 'n_orgname_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_mdeptname_like: new FormItemModel({ caption: '主部门名称(文本包含(%))', detailType: 'FORMITEM', name: 'n_mdeptname_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
    };
}
/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'defaultuomscheduleid',
          prop: 'defaultuomscheduleid',
          dataType: 'PICKUP',
        },
        {
          name: 'transactioncurrencyid',
          prop: 'transactioncurrencyid',
          dataType: 'PICKUP',
        },
        {
          name: 'productnumber',
          prop: 'productnumber',
          dataType: 'TEXT',
        },
        {
          name: 'srfmajortext',
          prop: 'productname',
          dataType: 'TEXT',
        },
        {
          name: 'srfkey',
          prop: 'productid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'productid',
          dataType: 'GUID',
        },
        {
          name: 'validfromdate',
          prop: 'validfromdate',
          dataType: 'DATETIME',
        },
        {
          name: 'defaultuomid',
          prop: 'defaultuomid',
          dataType: 'PICKUP',
        },
        {
          name: 'parentproductid',
          prop: 'parentproductid',
          dataType: 'PICKUP',
        },
        {
          name: 'validtodate',
          prop: 'validtodate',
          dataType: 'DATETIME',
        },
        {
          name: 'pricelevelid',
          prop: 'pricelevelid',
          dataType: 'PICKUP',
        },
        {
          name: 'subjectid',
          prop: 'subjectid',
          dataType: 'PICKUP',
        },
        {
          name: 'productname',
          prop: 'productname',
          dataType: 'TEXT',
        },
        {
          name: 'product',
          prop: 'productid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}
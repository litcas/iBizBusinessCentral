/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'substitutedproductid',
          prop: 'substitutedproductid',
          dataType: 'PICKUP',
        },
        {
          name: 'productid',
          prop: 'productid',
          dataType: 'PICKUP',
        },
        {
          name: 'substitutedproductname',
          prop: 'substitutedproductname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'direction',
          prop: 'direction',
          dataType: 'SSCODELIST',
        },
        {
          name: 'transactioncurrencyid',
          prop: 'transactioncurrencyid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'productsubstitutename',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'productsubstituteid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'productsubstituteid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'salesrelationshiptype',
          prop: 'salesrelationshiptype',
          dataType: 'SSCODELIST',
        },
        {
          name: 'productsubstitute',
          prop: 'productsubstituteid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}
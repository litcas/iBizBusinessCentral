/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof MainModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'productsubstituteid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'productsubstitutename',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'productid',
        prop: 'productid',
        dataType: 'PICKUP',
      },
      {
        name: 'substitutedproductname',
        prop: 'substitutedproductname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'salesrelationshiptype',
        prop: 'salesrelationshiptype',
        dataType: 'SSCODELIST',
      },
      {
        name: 'direction',
        prop: 'direction',
        dataType: 'SSCODELIST',
      },
      {
        name: 'substitutedproductid',
        prop: 'substitutedproductid',
        dataType: 'PICKUP',
      },
      {
        name: 'productsubstituteid',
        prop: 'productsubstituteid',
        dataType: 'GUID',
      },
      {
        name: 'productsubstitute',
        prop: 'productsubstituteid',
        dataType: 'FONTKEY',
      },
    ]
  }

}
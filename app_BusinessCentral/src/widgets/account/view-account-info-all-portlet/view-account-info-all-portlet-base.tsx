import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, MainControlBase } from '@/studio-core';
import AccountService from '@/service/account/account-service';
import View_AccountInfoAllService from './view-account-info-all-portlet-service';
import AccountUIService from '@/uiservice/account/account-ui-service';
import { Environment } from '@/environments/environment';


/**
 * dashboard_sysportlet6部件基类
 *
 * @export
 * @class MainControlBase
 * @extends {View_AccountInfoAllPortletBase}
 */
export class View_AccountInfoAllPortletBase extends MainControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof View_AccountInfoAllPortletBase
     */
    protected controlType: string = 'PORTLET';

    /**
     * 建构部件服务对象
     *
     * @type {View_AccountInfoAllService}
     * @memberof View_AccountInfoAllPortletBase
     */
    public service: View_AccountInfoAllService = new View_AccountInfoAllService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {AccountService}
     * @memberof View_AccountInfoAllPortletBase
     */
    public appEntityService: AccountService = new AccountService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof View_AccountInfoAllPortletBase
     */
    protected appDeName: string = 'account';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof View_AccountInfoAllPortletBase
     */
    protected appDeLogicName: string = '客户';

    /**
     * 界面UI服务对象
     *
     * @type {AccountUIService}
     * @memberof View_AccountInfoAllBase
     */  
    public appUIService:AccountUIService = new AccountUIService(this.$store);

    /**
     * 长度
     *
     * @type {number}
     * @memberof View_AccountInfoAll
     */
    @Prop() public height?: number;

    /**
     * 宽度
     *
     * @type {number}
     * @memberof View_AccountInfoAll
     */
    @Prop() public width?: number;



    /**
     * 是否自适应大小
     *
     * @returns {boolean}
     * @memberof View_AccountInfoAllBase
     */
    @Prop({default: false})public isAdaptiveSize!: boolean;

    /**
     * 获取多项数据
     *
     * @returns {any[]}
     * @memberof View_AccountInfoAllBase
     */
    public getDatas(): any[] {
        return [];
    }

    /**
     * 获取单项树
     *
     * @returns {*}
     * @memberof View_AccountInfoAllBase
     */
    public getData(): any {
        return {};
    }

    /**
     * 获取高度
     *
     * @returns {any[]}
     * @memberof View_AccountInfoAllBase
     */
    get getHeight(): any{
        if(!this.$util.isEmpty(this.height) && !this.$util.isNumberNaN(this.height)){
            if(this.height == 0){
                return 'auto';
            } else {
                return this.height+'px';
            }
        } else {
            return 'auto';
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof View_AccountInfoAllBase
     */
    public created() {
        this.afterCreated();
    }

    /**
     * 执行created后的逻辑
     *
     *  @memberof View_AccountInfoAllBase
     */    
    public afterCreated(){
        if (this.viewState) {
            this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }) => {
                if (!Object.is(tag, this.name)) {
                    return;
                }
                const refs: any = this.$refs;
                Object.keys(refs).forEach((_name: string) => {
                    this.viewState.next({ tag: _name, action: action, data: data });
                });
            });
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof View_AccountInfoAllBase
     */
    public destroyed() {
        this.afterDestroy();
    }

    /**
     * 执行destroyed后的逻辑
     *
     * @memberof View_AccountInfoAllBase
     */
    public afterDestroy() {
        if (this.viewStateEvent) {
            this.viewStateEvent.unsubscribe();
        }
    }


}

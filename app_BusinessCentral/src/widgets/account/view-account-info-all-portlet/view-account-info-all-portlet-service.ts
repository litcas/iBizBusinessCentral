import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * View_AccountInfoAll 部件服务对象
 *
 * @export
 * @class View_AccountInfoAllService
 */
export default class View_AccountInfoAllService extends ControlService {
}

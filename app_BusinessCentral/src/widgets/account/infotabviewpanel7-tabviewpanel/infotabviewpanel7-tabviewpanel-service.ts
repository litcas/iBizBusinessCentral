import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * Infotabviewpanel7 部件服务对象
 *
 * @export
 * @class Infotabviewpanel7Service
 */
export default class Infotabviewpanel7Service extends ControlService {
}
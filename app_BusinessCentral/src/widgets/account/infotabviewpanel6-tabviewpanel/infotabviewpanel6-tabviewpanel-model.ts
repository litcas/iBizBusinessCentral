/**
 * Infotabviewpanel6 部件模型
 *
 * @export
 * @class Infotabviewpanel6Model
 */
export default class Infotabviewpanel6Model {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof Infotabviewpanel6Model
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'address1_primarycontactname',
      },
      {
        name: 'openrevenue_date',
      },
      {
        name: 'address2_utcoffset',
      },
      {
        name: 'fax',
      },
      {
        name: 'address1_composite',
      },
      {
        name: 'entityimage',
      },
      {
        name: 'emailaddress1',
      },
      {
        name: 'overriddencreatedon',
      },
      {
        name: 'address1_stateorprovince',
      },
      {
        name: 'openrevenue',
      },
      {
        name: 'address1_upszone',
      },
      {
        name: 'followemail',
      },
      {
        name: 'marketingonly',
      },
      {
        name: 'numberofemployees',
      },
      {
        name: 'telephone1',
      },
      {
        name: 'aging60',
      },
      {
        name: 'sharesoutstanding',
      },
      {
        name: 'address2_freighttermscode',
      },
      {
        name: 'entityimage_timestamp',
      },
      {
        name: 'revenue_base',
      },
      {
        name: 'accountratingcode',
      },
      {
        name: 'donotbulkemail',
      },
      {
        name: 'aging30',
      },
      {
        name: 'donotbulkpostalmail',
      },
      {
        name: 'creditlimit_base',
      },
      {
        name: 'timezoneruleversionnumber',
      },
      {
        name: 'customertypecode',
      },
      {
        name: 'address2_longitude',
      },
      {
        name: 'accountname',
      },
      {
        name: 'participatesinworkflow',
      },
      {
        name: 'preferredcontactmethodcode',
      },
      {
        name: 'preferredappointmentdaycode',
      },
      {
        name: 'marketcap',
      },
      {
        name: 'address2_telephone1',
      },
      {
        name: 'businesstypecode',
      },
      {
        name: 'entityimageid',
      },
      {
        name: 'address2_city',
      },
      {
        name: 'ownershipcode',
      },
      {
        name: 'utcconversiontimezonecode',
      },
      {
        name: 'accountnumber',
      },
      {
        name: 'address1_telephone2',
      },
      {
        name: 'address1_line1',
      },
      {
        name: 'donotphone',
      },
      {
        name: 'address1_telephone3',
      },
      {
        name: 'exchangerate',
      },
      {
        name: 'address2_line2',
      },
      {
        name: 'address2_telephone3',
      },
      {
        name: 'address1_telephone1',
      },
      {
        name: 'address1_fax',
      },
      {
        name: 'address1_freighttermscode',
      },
      {
        name: 'description',
      },
      {
        name: 'merged',
      },
      {
        name: 'address1_shippingmethodcode',
      },
      {
        name: 'address1_name',
      },
      {
        name: 'aging30_base',
      },
      {
        name: 'address2_telephone2',
      },
      {
        name: 'childaccountcount',
      },
      {
        name: 'account',
        prop: 'accountid',
      },
      {
        name: 'primarytwitterid',
      },
      {
        name: 'tickersymbol',
      },
      {
        name: 'stockexchange',
      },
      {
        name: 'address2_stateorprovince',
      },
      {
        name: 'ftpsiteurl',
      },
      {
        name: 'statuscode',
      },
      {
        name: 'websiteurl',
      },
      {
        name: 'donotpostalmail',
      },
      {
        name: 'address1_city',
      },
      {
        name: 'address2_fax',
      },
      {
        name: 'aging90_base',
      },
      {
        name: 'opendeals_date',
      },
      {
        name: 'traversedpath',
      },
      {
        name: 'shippingmethodcode',
      },
      {
        name: 'address1_line2',
      },
      {
        name: 'openrevenue_base',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'address2_line3',
      },
      {
        name: 'address1_postalcode',
      },
      {
        name: 'address2_latitude',
      },
      {
        name: 'ibizprivate',
      },
      {
        name: 'territorycode',
      },
      {
        name: 'teamsfollowed',
      },
      {
        name: 'address1_latitude',
      },
      {
        name: 'telephone3',
      },
      {
        name: 'ownerid',
      },
      {
        name: 'address2_country',
      },
      {
        name: 'masteraccountname',
      },
      {
        name: 'ownername',
      },
      {
        name: 'industrycode',
      },
      {
        name: 'name',
      },
      {
        name: 'lastonholdtime',
      },
      {
        name: 'address2_addressid',
      },
      {
        name: 'onholdtime',
      },
      {
        name: 'accountclassificationcode',
      },
      {
        name: 'address1_country',
      },
      {
        name: 'address1_addresstypecode',
      },
      {
        name: 'statecode',
      },
      {
        name: 'address2_addresstypecode',
      },
      {
        name: 'accountcategorycode',
      },
      {
        name: 'emailaddress2',
      },
      {
        name: 'address2_shippingmethodcode',
      },
      {
        name: 'importsequencenumber',
      },
      {
        name: 'primarysatoriid',
      },
      {
        name: 'customersizecode',
      },
      {
        name: 'opendeals',
      },
      {
        name: 'donotsendmm',
      },
      {
        name: 'processid',
      },
      {
        name: 'paymenttermscode',
      },
      {
        name: 'sic',
      },
      {
        name: 'address2_primarycontactname',
      },
      {
        name: 'address1_utcoffset',
      },
      {
        name: 'lastusedincampaign',
      },
      {
        name: 'aging60_base',
      },
      {
        name: 'donotfax',
      },
      {
        name: 'updateman',
      },
      {
        name: 'openrevenue_state',
      },
      {
        name: 'opendeals_state',
      },
      {
        name: 'versionnumber',
      },
      {
        name: 'address1_longitude',
      },
      {
        name: 'aging90',
      },
      {
        name: 'entityimage_url',
      },
      {
        name: 'creditlimit',
      },
      {
        name: 'preferredsystemuserid',
      },
      {
        name: 'revenue',
      },
      {
        name: 'address1_county',
      },
      {
        name: 'address1_line3',
      },
      {
        name: 'preferredsystemusername',
      },
      {
        name: 'stageid',
      },
      {
        name: 'donotemail',
      },
      {
        name: 'address2_postalcode',
      },
      {
        name: 'preferredappointmenttimecode',
      },
      {
        name: 'address2_upszone',
      },
      {
        name: 'createdate',
      },
      {
        name: 'address2_county',
      },
      {
        name: 'address2_composite',
      },
      {
        name: 'address2_name',
      },
      {
        name: 'address2_line1',
      },
      {
        name: 'marketcap_base',
      },
      {
        name: 'emailaddress3',
      },
      {
        name: 'ownertype',
      },
      {
        name: 'telephone2',
      },
      {
        name: 'createman',
      },
      {
        name: 'address1_postofficebox',
      },
      {
        name: 'creditonhold',
      },
      {
        name: 'address1_addressid',
      },
      {
        name: 'address2_postofficebox',
      },
      {
        name: 'currencyname',
      },
      {
        name: 'defaultpricelevelname',
      },
      {
        name: 'slaname',
      },
      {
        name: 'preferredequipmentname',
      },
      {
        name: 'preferredservicename',
      },
      {
        name: 'territoryname',
      },
      {
        name: 'primarycontactname',
      },
      {
        name: 'parentaccountname',
      },
      {
        name: 'originatingleadname',
      },
      {
        name: 'originatingleadid',
      },
      {
        name: 'transactioncurrencyid',
      },
      {
        name: 'defaultpricelevelid',
      },
      {
        name: 'parentaccountid',
      },
      {
        name: 'preferredequipmentid',
      },
      {
        name: 'territoryid',
      },
      {
        name: 'primarycontactid',
      },
      {
        name: 'slaid',
      },
      {
        name: 'preferredserviceid',
      },
    ]
  }


}
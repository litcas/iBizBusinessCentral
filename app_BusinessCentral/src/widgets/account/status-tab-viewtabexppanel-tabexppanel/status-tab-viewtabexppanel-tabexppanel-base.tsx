import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, TabExpPanelControlBase } from '@/studio-core';
import AccountService from '@/service/account/account-service';
import StatusTabViewtabexppanelService from './status-tab-viewtabexppanel-tabexppanel-service';
import AccountUIService from '@/uiservice/account/account-ui-service';


/**
 * tabexppanel部件基类
 *
 * @export
 * @class TabExpPanelControlBase
 * @extends {StatusTabViewtabexppanelTabexppanelBase}
 */
export class StatusTabViewtabexppanelTabexppanelBase extends TabExpPanelControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof StatusTabViewtabexppanelTabexppanelBase
     */
    protected controlType: string = 'TABEXPPANEL';

    /**
     * 建构部件服务对象
     *
     * @type {StatusTabViewtabexppanelService}
     * @memberof StatusTabViewtabexppanelTabexppanelBase
     */
    public service: StatusTabViewtabexppanelService = new StatusTabViewtabexppanelService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {AccountService}
     * @memberof StatusTabViewtabexppanelTabexppanelBase
     */
    public appEntityService: AccountService = new AccountService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof StatusTabViewtabexppanelTabexppanelBase
     */
    protected appDeName: string = 'account';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof StatusTabViewtabexppanelTabexppanelBase
     */
    protected appDeLogicName: string = '客户';

    /**
     * 界面UI服务对象
     *
     * @type {AccountUIService}
     * @memberof StatusTabViewtabexppanelBase
     */  
    public appUIService:AccountUIService = new AccountUIService(this.$store);
    /**
     * 是否初始化
     *
     * @protected
     * @returns {any}
     * @memberof StatusTabViewtabexppanel
     */
    protected isInit: any = {
        tabviewpanel:  true ,
        tabviewpanel2:  false ,
        tabviewpanel3:  false ,
    }

    /**
     * 被激活的分页面板
     *
     * @protected
     * @type {string}
     * @memberof StatusTabViewtabexppanel
     */
    protected activatedTabViewPanel: string = 'tabviewpanel';

    /**
     * 组件创建完毕
     *
     * @protected
     * @memberof StatusTabViewtabexppanel
     */
    protected ctrlCreated(): void {
        //设置分页导航srfparentdename和srfparentkey
        if (this.context.account) {
            Object.assign(this.context, { srfparentdename: 'Account', srfparentkey: this.context.account });
        }
        super.ctrlCreated();
    }
}
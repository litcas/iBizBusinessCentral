/**
 * Info_DataPanel 部件模型
 *
 * @export
 * @class Info_DataPanelModel
 */
export default class Info_DataPanelModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Info_DataPanelModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'accountid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'accountname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'accountid',
        prop: 'accountid',
        dataType: 'GUID',
      },
      {
        name: 'industrycode',
        prop: 'industrycode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'telephone1',
        prop: 'telephone1',
        dataType: 'TEXT',
      },
      {
        name: 'revenue',
        prop: 'revenue',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'numberofemployees',
        prop: 'numberofemployees',
        dataType: 'INT',
      },
      {
        name: 'statuscode',
        prop: 'statuscode',
        dataType: 'NSCODELIST',
      },
      {
        name: 'ownername',
        prop: 'ownername',
        dataType: 'TEXT',
      },
      {
        name: 'account',
        prop: 'accountid',
        dataType: 'FONTKEY',
      },
    ]
  }

}
/**
 * InnerPickip 部件模型
 *
 * @export
 * @class InnerPickipModel
 */
export default class InnerPickipModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof InnerPickipGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof InnerPickipGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'accountname',
          prop: 'accountname',
          dataType: 'TEXT',
        },
        {
          name: 'primarycontactid',
          prop: 'primarycontactid',
          dataType: 'PICKUP',
        },
        {
          name: 'transactioncurrencyid',
          prop: 'transactioncurrencyid',
          dataType: 'PICKUP',
        },
        {
          name: 'slaid',
          prop: 'slaid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'accountname',
          dataType: 'TEXT',
        },
        {
          name: 'srfkey',
          prop: 'accountid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'accountid',
          dataType: 'GUID',
        },
        {
          name: 'preferredequipmentid',
          prop: 'preferredequipmentid',
          dataType: 'PICKUP',
        },
        {
          name: 'territoryid',
          prop: 'territoryid',
          dataType: 'PICKUP',
        },
        {
          name: 'preferredserviceid',
          prop: 'preferredserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'defaultpricelevelid',
          prop: 'defaultpricelevelid',
          dataType: 'PICKUP',
        },
        {
          name: 'emailaddress1',
          prop: 'emailaddress1',
          dataType: 'TEXT',
        },
        {
          name: 'originatingleadid',
          prop: 'originatingleadid',
          dataType: 'PICKUP',
        },
        {
          name: 'parentaccountid',
          prop: 'parentaccountid',
          dataType: 'PICKUP',
        },
        {
          name: 'account',
          prop: 'accountid',
        },
        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}
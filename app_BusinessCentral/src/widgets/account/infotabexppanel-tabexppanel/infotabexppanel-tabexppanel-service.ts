import { Http,Util,Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import AccountService from '@/service/account/account-service';
import InfotabexppanelModel from './infotabexppanel-tabexppanel-model';


/**
 * Infotabexppanel 部件服务对象
 *
 * @export
 * @class InfotabexppanelService
 */
export default class InfotabexppanelService extends ControlService {

    /**
     * 客户服务对象
     *
     * @type {AccountService}
     * @memberof InfotabexppanelService
     */
    public appEntityService: AccountService = new AccountService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof InfotabexppanelService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of InfotabexppanelService.
     * 
     * @param {*} [opts={}]
     * @memberof InfotabexppanelService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new InfotabexppanelModel();
    }

    
}
import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * Summary 部件服务对象
 *
 * @export
 * @class SummaryService
 */
export default class SummaryService extends ControlService {
}
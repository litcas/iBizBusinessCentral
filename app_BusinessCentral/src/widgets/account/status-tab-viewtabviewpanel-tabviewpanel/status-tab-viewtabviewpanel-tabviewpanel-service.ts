import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * StatusTabViewtabviewpanel 部件服务对象
 *
 * @export
 * @class StatusTabViewtabviewpanelService
 */
export default class StatusTabViewtabviewpanelService extends ControlService {
}
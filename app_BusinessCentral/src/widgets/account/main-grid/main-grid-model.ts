/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'telephone1',
          prop: 'telephone1',
          dataType: 'TEXT',
        },
        {
          name: 'accountname',
          prop: 'accountname',
          dataType: 'TEXT',
        },
        {
          name: 'primarycontactid',
          prop: 'primarycontactid',
          dataType: 'PICKUP',
        },
        {
          name: 'transactioncurrencyid',
          prop: 'transactioncurrencyid',
          dataType: 'PICKUP',
        },
        {
          name: 'parentaccountname',
          prop: 'parentaccountname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'slaid',
          prop: 'slaid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'accountname',
          dataType: 'TEXT',
        },
        {
          name: 'srfkey',
          prop: 'accountid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'accountid',
          dataType: 'GUID',
        },
        {
          name: 'territoryid',
          prop: 'territoryid',
          dataType: 'PICKUP',
        },
        {
          name: 'preferredequipmentid',
          prop: 'preferredequipmentid',
          dataType: 'PICKUP',
        },
        {
          name: 'preferredserviceid',
          prop: 'preferredserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'defaultpricelevelid',
          prop: 'defaultpricelevelid',
          dataType: 'PICKUP',
        },
        {
          name: 'emailaddress1',
          prop: 'emailaddress1',
          dataType: 'TEXT',
        },
        {
          name: 'parentaccountid',
          prop: 'parentaccountid',
          dataType: 'PICKUP',
        },
        {
          name: 'originatingleadid',
          prop: 'originatingleadid',
          dataType: 'PICKUP',
        },
        {
          name: 'primarycontactname',
          prop: 'primarycontactname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'address1_city',
          prop: 'address1_city',
          dataType: 'TEXT',
        },
        {
          name: 'revenue',
          prop: 'revenue',
          dataType: 'BIGDECIMAL',
        },
        {
          name: 'account',
          prop: 'accountid',
        },
      {
        name: 'n_accountname_like',
        prop: 'n_accountname_like',
        dataType: 'TEXT',
      },
      {
        name: 'n_statecode_eq',
        prop: 'n_statecode_eq',
        dataType: 'NSCODELIST',
      },
      {
        name: 'n_accountcategorycode_eq',
        prop: 'n_accountcategorycode_eq',
        dataType: 'SSCODELIST',
      },
      {
        name: 'n_accountclassificationcode_eq',
        prop: 'n_accountclassificationcode_eq',
        dataType: 'SSCODELIST',
      },
      {
        name: 'n_accountratingcode_eq',
        prop: 'n_accountratingcode_eq',
        dataType: 'SSCODELIST',
      },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}
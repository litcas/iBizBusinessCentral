import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * InnerPickupViewpickupviewpanel 部件服务对象
 *
 * @export
 * @class InnerPickupViewpickupviewpanelService
 */
export default class InnerPickupViewpickupviewpanelService extends ControlService {
}
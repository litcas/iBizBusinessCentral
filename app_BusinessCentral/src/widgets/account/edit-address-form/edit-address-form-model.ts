/**
 * Edit_Address 部件模型
 *
 * @export
 * @class Edit_AddressModel
 */
export default class Edit_AddressModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Edit_AddressModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'accountid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'accountname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'address1_addressid',
        prop: 'address1_addressid',
        dataType: 'TEXT',
      },
      {
        name: 'address1_name',
        prop: 'address1_name',
        dataType: 'TEXT',
      },
      {
        name: 'address1_addresstypecode',
        prop: 'address1_addresstypecode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'address1_country',
        prop: 'address1_country',
        dataType: 'TEXT',
      },
      {
        name: 'address1_stateorprovince',
        prop: 'address1_stateorprovince',
        dataType: 'TEXT',
      },
      {
        name: 'address1_city',
        prop: 'address1_city',
        dataType: 'TEXT',
      },
      {
        name: 'address1_county',
        prop: 'address1_county',
        dataType: 'TEXT',
      },
      {
        name: 'accountid',
        prop: 'accountid',
        dataType: 'GUID',
      },
      {
        name: 'account',
        prop: 'accountid',
        dataType: 'FONTKEY',
      },
    ]
  }

}
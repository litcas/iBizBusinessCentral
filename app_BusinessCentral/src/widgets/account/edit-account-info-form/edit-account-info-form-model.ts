/**
 * Edit_AccountInfo 部件模型
 *
 * @export
 * @class Edit_AccountInfoModel
 */
export default class Edit_AccountInfoModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Edit_AccountInfoModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'accountid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'accountname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'accountname',
        prop: 'accountname',
        dataType: 'TEXT',
      },
      {
        name: 'telephone1',
        prop: 'telephone1',
        dataType: 'TEXT',
      },
      {
        name: 'fax',
        prop: 'fax',
        dataType: 'TEXT',
      },
      {
        name: 'websiteurl',
        prop: 'websiteurl',
        dataType: 'TEXT',
      },
      {
        name: 'parentaccountname',
        prop: 'parentaccountname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'tickersymbol',
        prop: 'tickersymbol',
        dataType: 'TEXT',
      },
      {
        name: 'customertypecode',
        prop: 'customertypecode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'defaultpricelevelname',
        prop: 'defaultpricelevelname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'defaultpricelevelid',
        prop: 'defaultpricelevelid',
        dataType: 'PICKUP',
      },
      {
        name: 'parentaccountid',
        prop: 'parentaccountid',
        dataType: 'PICKUP',
      },
      {
        name: 'accountid',
        prop: 'accountid',
        dataType: 'GUID',
      },
      {
        name: 'account',
        prop: 'accountid',
        dataType: 'FONTKEY',
      },
    ]
  }

}
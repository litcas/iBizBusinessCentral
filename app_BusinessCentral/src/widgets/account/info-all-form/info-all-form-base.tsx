import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, EditFormControlBase } from '@/studio-core';
import AccountService from '@/service/account/account-service';
import Info_AllService from './info-all-form-service';
import AccountUIService from '@/uiservice/account/account-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {Info_AllEditFormBase}
 */
export class Info_AllEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof Info_AllEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {Info_AllService}
     * @memberof Info_AllEditFormBase
     */
    public service: Info_AllService = new Info_AllService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {AccountService}
     * @memberof Info_AllEditFormBase
     */
    public appEntityService: AccountService = new AccountService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Info_AllEditFormBase
     */
    protected appDeName: string = 'account';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof Info_AllEditFormBase
     */
    protected appDeLogicName: string = '客户';

    /**
     * 界面UI服务对象
     *
     * @type {AccountUIService}
     * @memberof Info_AllBase
     */  
    public appUIService:AccountUIService = new AccountUIService(this.$store);

    /**
     * 关系界面数量
     *
     * @protected
     * @type {number}
     * @memberof Info_AllEditFormBase
     */
    protected drCount: number = 1;

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof Info_AllEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        accountname: null,
        customertypecode: null,
        telephone1: null,
        industrycode: null,
        sic: null,
        fax: null,
        websiteurl: null,
        parentaccountname: null,
        tickersymbol: null,
        ownershipcode: null,
        defaultpricelevelname: null,
        description: null,
        address1_addressid: null,
        address1_name: null,
        address1_addresstypecode: null,
        address1_country: null,
        address1_stateorprovince: null,
        address1_city: null,
        address1_county: null,
        address1_line1: null,
        address1_fax: null,
        address1_freighttermscode: null,
        address1_postalcode: null,
        primarycontactid: null,
        primarycontactname: null,
        preferredcontactmethodcode: null,
        donotemail: null,
        donotbulkemail: null,
        donotphone: null,
        donotfax: null,
        donotpostalmail: null,
        parentaccountid: null,
        accountid: null,
        account:null,
    };

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Info_AllEditFormBase
     */
    public rules: any = {
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Info_AllBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof Info_AllEditFormBase
     */
    public detailsModel: any = {
        group1: new FormGroupPanelModel({ caption: '账户信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: true, form: this, showMoreMode: 2, anchorPoints: ['telephone1', 'sic', 'fax', 'websiteurl', 'tickersymbol', 'ownershipcode', ], controlledItems: [
                    'sic',
                    'fax',
                    'websiteurl',
                    'parentaccountname',
                    'tickersymbol',
                    'ownershipcode',
                    'defaultpricelevelname',
        ], uiActionGroup: { caption: '', langbase: 'entities.account.info_all_form', extractMode: 'ITEM', details: [] } }),

        grouppanel1: new FormGroupPanelModel({ caption: '地址信息', detailType: 'GROUPPANEL', name: 'grouppanel1', visible: true, isShowCaption: true, form: this, showMoreMode: 2, anchorPoints: ['address1_country', ], controlledItems: [
                    'address1_addressid',
                    'address1_name',
                    'address1_addresstypecode',
                    'address1_county',
                    'address1_fax',
                    'address1_freighttermscode',
        ], uiActionGroup: { caption: '', langbase: 'entities.account.info_all_form', extractMode: 'ITEM', details: [] } }),

        druipart1: new FormDRUIPartModel({ caption: '', detailType: 'DRUIPART', name: 'druipart1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        grouppanel4: new FormGroupPanelModel({ caption: '联系人', detailType: 'GROUPPANEL', name: 'grouppanel4', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.account.info_all_form', extractMode: 'ITEM', details: [] } }),

        grouppanel2: new FormGroupPanelModel({ caption: '联系人首选项', detailType: 'GROUPPANEL', name: 'grouppanel2', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.account.info_all_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: '客户', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfmajortext: new FormItemModel({ caption: '客户名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        accountname: new FormItemModel({ caption: '客户名称', detailType: 'FORMITEM', name: 'accountname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        customertypecode: new FormItemModel({ caption: '关系类型', detailType: 'FORMITEM', name: 'customertypecode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        telephone1: new FormItemModel({ caption: '主要电话', detailType: 'FORMITEM', name: 'telephone1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        industrycode: new FormItemModel({ caption: '行业', detailType: 'FORMITEM', name: 'industrycode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        sic: new FormItemModel({ caption: '行业编码', detailType: 'FORMITEM', name: 'sic', visible: true, isShowCaption: true, form: this, showMoreMode: 1, disabled: false, enableCond: 3 }),

        fax: new FormItemModel({ caption: '传真', detailType: 'FORMITEM', name: 'fax', visible: true, isShowCaption: true, form: this, showMoreMode: 1, disabled: false, enableCond: 3 }),

        websiteurl: new FormItemModel({ caption: '网站', detailType: 'FORMITEM', name: 'websiteurl', visible: true, isShowCaption: true, form: this, showMoreMode: 1, disabled: false, enableCond: 3 }),

        parentaccountname: new FormItemModel({ caption: '上级客户', detailType: 'FORMITEM', name: 'parentaccountname', visible: true, isShowCaption: true, form: this, showMoreMode: 1, disabled: false, enableCond: 3 }),

        tickersymbol: new FormItemModel({ caption: '股票代号', detailType: 'FORMITEM', name: 'tickersymbol', visible: true, isShowCaption: true, form: this, showMoreMode: 1, disabled: false, enableCond: 3 }),

        ownershipcode: new FormItemModel({ caption: '所有权', detailType: 'FORMITEM', name: 'ownershipcode', visible: true, isShowCaption: true, form: this, showMoreMode: 1, disabled: false, enableCond: 3 }),

        defaultpricelevelname: new FormItemModel({ caption: '价目表', detailType: 'FORMITEM', name: 'defaultpricelevelname', visible: true, isShowCaption: true, form: this, showMoreMode: 1, disabled: false, enableCond: 3 }),

        description: new FormItemModel({ caption: '说明', detailType: 'FORMITEM', name: 'description', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        address1_addressid: new FormItemModel({ caption: '地址 1: ID', detailType: 'FORMITEM', name: 'address1_addressid', visible: true, isShowCaption: true, form: this, showMoreMode: 1, disabled: false, enableCond: 3 }),

        address1_name: new FormItemModel({ caption: '地址 1: 名称', detailType: 'FORMITEM', name: 'address1_name', visible: true, isShowCaption: true, form: this, showMoreMode: 1, disabled: false, enableCond: 3 }),

        address1_addresstypecode: new FormItemModel({ caption: '地址 1: 地址类型', detailType: 'FORMITEM', name: 'address1_addresstypecode', visible: true, isShowCaption: true, form: this, showMoreMode: 1, disabled: false, enableCond: 3 }),

        address1_country: new FormItemModel({ caption: '地址 1: 国家/地区', detailType: 'FORMITEM', name: 'address1_country', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        address1_stateorprovince: new FormItemModel({ caption: '地址 1: 省/市/自治区', detailType: 'FORMITEM', name: 'address1_stateorprovince', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        address1_city: new FormItemModel({ caption: '地址 1: 市/县', detailType: 'FORMITEM', name: 'address1_city', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        address1_county: new FormItemModel({ caption: '地址 1: 县', detailType: 'FORMITEM', name: 'address1_county', visible: true, isShowCaption: true, form: this, showMoreMode: 1, disabled: false, enableCond: 3 }),

        address1_line1: new FormItemModel({ caption: '地址 1: 街道 1', detailType: 'FORMITEM', name: 'address1_line1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        address1_fax: new FormItemModel({ caption: '地址 1: 传真', detailType: 'FORMITEM', name: 'address1_fax', visible: true, isShowCaption: true, form: this, showMoreMode: 1, disabled: false, enableCond: 3 }),

        address1_freighttermscode: new FormItemModel({ caption: '地址 1: 货运条款', detailType: 'FORMITEM', name: 'address1_freighttermscode', visible: true, isShowCaption: true, form: this, showMoreMode: 1, disabled: false, enableCond: 3 }),

        address1_postalcode: new FormItemModel({ caption: '地址 1: 邮政编码', detailType: 'FORMITEM', name: 'address1_postalcode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        primarycontactid: new FormItemModel({ caption: '主要联系人', detailType: 'FORMITEM', name: 'primarycontactid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        primarycontactname: new FormItemModel({ caption: '主要联系人', detailType: 'FORMITEM', name: 'primarycontactname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        preferredcontactmethodcode: new FormItemModel({ caption: '首选联系方式', detailType: 'FORMITEM', name: 'preferredcontactmethodcode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        donotemail: new FormItemModel({ caption: '不允许使用电子邮件', detailType: 'FORMITEM', name: 'donotemail', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        donotbulkemail: new FormItemModel({ caption: '不允许使用批量电子邮件', detailType: 'FORMITEM', name: 'donotbulkemail', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        donotphone: new FormItemModel({ caption: '不允许电话联络', detailType: 'FORMITEM', name: 'donotphone', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        donotfax: new FormItemModel({ caption: '不允许使用传真', detailType: 'FORMITEM', name: 'donotfax', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        donotpostalmail: new FormItemModel({ caption: '不允许使用邮件', detailType: 'FORMITEM', name: 'donotpostalmail', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        parentaccountid: new FormItemModel({ caption: '上级单位', detailType: 'FORMITEM', name: 'parentaccountid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        accountid: new FormItemModel({ caption: '客户', detailType: 'FORMITEM', name: 'accountid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

    };
}
import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, GridControlBase } from '@/studio-core';
import JobsLogService from '@/service/jobs-log/jobs-log-service';
import MainService from './main-grid-service';
import JobsLogUIService from '@/uiservice/jobs-log/jobs-log-ui-service';
import { FormItemModel } from '@/model/form-detail';


/**
 * grid部件基类
 *
 * @export
 * @class GridControlBase
 * @extends {MainGridBase}
 */
export class MainGridBase extends GridControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected controlType: string = 'GRID';

    /**
     * 建构部件服务对象
     *
     * @type {MainService}
     * @memberof MainGridBase
     */
    public service: MainService = new MainService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {JobsLogService}
     * @memberof MainGridBase
     */
    public appEntityService: JobsLogService = new JobsLogService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeName: string = 'jobslog';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeLogicName: string = '任务调度日志';

    /**
     * 界面UI服务对象
     *
     * @type {JobsLogUIService}
     * @memberof MainBase
     */  
    public appUIService:JobsLogUIService = new JobsLogUIService(this.$store);

    /**
     * 界面行为模型
     *
     * @type {*}
     * @memberof MainBase
     */  
    public ActionModel: any = {
    };

    /**
     * 本地缓存标识
     *
     * @protected
     * @type {string}
     * @memberof MainBase
     */
    protected localStorageTag: string = 'jobs_log_main_grid';

    /**
     * 所有列成员
     *
     * @type {any[]}
     * @memberof MainGridBase
     */
    public allColumns: any[] = [
        {
            name: 'id',
            label: '主键ID',
            langtag: 'entities.jobslog.main_grid.columns.id',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'job_id',
            label: '任务ID',
            langtag: 'entities.jobslog.main_grid.columns.job_id',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'handler',
            label: '执行器任务HANDLER',
            langtag: 'entities.jobslog.main_grid.columns.handler',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'address',
            label: '执行地址',
            langtag: 'entities.jobslog.main_grid.columns.address',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'trigger_code',
            label: '触发器调度返回码',
            langtag: 'entities.jobslog.main_grid.columns.trigger_code',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'trigger_type',
            label: '触发器调度类型',
            langtag: 'entities.jobslog.main_grid.columns.trigger_type',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'fail_retry_count',
            label: '失败重试次数',
            langtag: 'entities.jobslog.main_grid.columns.fail_retry_count',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'create_time',
            label: '创建时间',
            langtag: 'entities.jobslog.main_grid.columns.create_time',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
    ]

    /**
     * 获取表格行模型
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public getGridRowModel(){
        return {
          srfkey: new FormItemModel(),
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public rules: any = {
        srfkey: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '主键ID 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '主键ID 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 获取对应列class
     *
     * @type {*}
     * @memberof MainBase
     */
    public hasRowEdit: any = {
        'id':false,
        'job_id':false,
        'handler':false,
        'address':false,
        'trigger_code':false,
        'trigger_type':false,
        'fail_retry_count':false,
        'create_time':false,
    };

    /**
     * 获取对应列class
     *
     * @param {*} $args row 行数据，column 列数据，rowIndex 行索引，列索引
     * @returns {void}
     * @memberof MainBase
     */
    public getCellClassName(args: {row: any, column: any, rowIndex: number, columnIndex: number}): any {
        return ( this.hasRowEdit[args.column.property] && this.actualIsOpenEdit ) ? "edit-cell" : "info-cell";
    }


    /**
     * 导出数据格式化
     *
     * @param {*} filterVal
     * @param {*} jsonData
     * @param {any[]} [codelistColumns=[]]
     * @returns {Promise<any>}
     * @memberof MainGridBase
     */
    public async formatExcelData(filterVal: any, jsonData: any, codelistColumns?: any[]): Promise<any> {
        return super.formatExcelData(filterVal, jsonData, [
        ]);
    }

}
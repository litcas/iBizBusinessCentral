import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, MainControlBase } from '@/studio-core';
import ActivityPointerService from '@/service/activity-pointer/activity-pointer-service';
import List_ByParentKeyService from './list-by-parent-key-portlet-service';
import TaskUIService from '@/uiservice/task/task-ui-service';
import EmailUIService from '@/uiservice/email/email-ui-service';
import AppointmentUIService from '@/uiservice/appointment/appointment-ui-service';
import PhoneCallUIService from '@/uiservice/phone-call/phone-call-ui-service';
import ActivityPointerUIService from '@/uiservice/activity-pointer/activity-pointer-ui-service';
import { Environment } from '@/environments/environment';


/**
 * dashboard_sysportlet2部件基类
 *
 * @export
 * @class MainControlBase
 * @extends {List_ByParentKeyPortletBase}
 */
export class List_ByParentKeyPortletBase extends MainControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof List_ByParentKeyPortletBase
     */
    protected controlType: string = 'PORTLET';

    /**
     * 建构部件服务对象
     *
     * @type {List_ByParentKeyService}
     * @memberof List_ByParentKeyPortletBase
     */
    public service: List_ByParentKeyService = new List_ByParentKeyService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {ActivityPointerService}
     * @memberof List_ByParentKeyPortletBase
     */
    public appEntityService: ActivityPointerService = new ActivityPointerService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof List_ByParentKeyPortletBase
     */
    protected appDeName: string = 'activitypointer';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof List_ByParentKeyPortletBase
     */
    protected appDeLogicName: string = '活动';

    /**
     * 界面UI服务对象
     *
     * @type {ActivityPointerUIService}
     * @memberof List_ByParentKeyBase
     */  
    public appUIService:ActivityPointerUIService = new ActivityPointerUIService(this.$store);

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public dashboard_sysportlet2_u4817114_click(params: any = {}, tag?: any, $event?: any) {
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this;
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:TaskUIService  = new TaskUIService();
        curUIService.Task_Create(datas,contextJO, paramJO,  $event, xData,this,"ActivityPointer");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public dashboard_sysportlet2_u6bd9f1d_click(params: any = {}, tag?: any, $event?: any) {
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this;
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:EmailUIService  = new EmailUIService();
        curUIService.Email_Create(datas,contextJO, paramJO,  $event, xData,this,"ActivityPointer");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public dashboard_sysportlet2_u177452d_click(params: any = {}, tag?: any, $event?: any) {
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this;
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:AppointmentUIService  = new AppointmentUIService();
        curUIService.Appointment_Create(datas,contextJO, paramJO,  $event, xData,this,"ActivityPointer");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public dashboard_sysportlet2_u21a597a_click(params: any = {}, tag?: any, $event?: any) {
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this;
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:PhoneCallUIService  = new PhoneCallUIService();
        curUIService.PhoneCall_Create(datas,contextJO, paramJO,  $event, xData,this,"ActivityPointer");
    }

    /**
     * 长度
     *
     * @type {number}
     * @memberof List_ByParentKey
     */
    @Prop() public height?: number;

    /**
     * 宽度
     *
     * @type {number}
     * @memberof List_ByParentKey
     */
    @Prop() public width?: number;



    /**
     * 是否自适应大小
     *
     * @returns {boolean}
     * @memberof List_ByParentKeyBase
     */
    @Prop({default: false})public isAdaptiveSize!: boolean;

    /**
     * 获取多项数据
     *
     * @returns {any[]}
     * @memberof List_ByParentKeyBase
     */
    public getDatas(): any[] {
        return [];
    }

    /**
     * 获取单项树
     *
     * @returns {*}
     * @memberof List_ByParentKeyBase
     */
    public getData(): any {
        return {};
    }

    /**
     * 获取高度
     *
     * @returns {any[]}
     * @memberof List_ByParentKeyBase
     */
    get getHeight(): any{
        if(!this.$util.isEmpty(this.height) && !this.$util.isNumberNaN(this.height)){
            if(this.height == 0){
                return 'auto';
            } else {
                return this.height+'px';
            }
        } else {
            return 'auto';
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof List_ByParentKeyBase
     */
    public created() {
        this.afterCreated();
    }

    /**
     * 执行created后的逻辑
     *
     *  @memberof List_ByParentKeyBase
     */    
    public afterCreated(){
        if (this.viewState) {
            this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }) => {
                if (!Object.is(tag, this.name)) {
                    return;
                }
                const refs: any = this.$refs;
                Object.keys(refs).forEach((_name: string) => {
                    this.viewState.next({ tag: _name, action: action, data: data });
                });
            });
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof List_ByParentKeyBase
     */
    public destroyed() {
        this.afterDestroy();
    }

    /**
     * 执行destroyed后的逻辑
     *
     * @memberof List_ByParentKeyBase
     */
    public afterDestroy() {
        if (this.viewStateEvent) {
            this.viewStateEvent.unsubscribe();
        }
    }

    /**
     * 执行界面行为
     *
     * @memberof List_ByParentKeyBase
     */
    public uiAction(tag:string,event:any){
        if(Object.is(tag,'u4817114')){
            this.dashboard_sysportlet2_u4817114_click(null,tag,event);
        }
        if(Object.is(tag,'u6bd9f1d')){
            this.dashboard_sysportlet2_u6bd9f1d_click(null,tag,event);
        }
        if(Object.is(tag,'u177452d')){
            this.dashboard_sysportlet2_u177452d_click(null,tag,event);
        }
        if(Object.is(tag,'u21a597a')){
            this.dashboard_sysportlet2_u21a597a_click(null,tag,event);
        }
    }

}

/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'activitytypecode',
          prop: 'activitytypecode',
          dataType: 'SSCODELIST',
        },
        {
          name: 'statecode',
          prop: 'statecode',
          dataType: 'NSCODELIST',
        },
        {
          name: 'prioritycode',
          prop: 'prioritycode',
          dataType: 'SSCODELIST',
        },
        {
          name: 'scheduledstart',
          prop: 'scheduledstart',
          dataType: 'DATETIME',
        },
        {
          name: 'subject',
          prop: 'subject',
          dataType: 'TEXT',
        },
        {
          name: 'regardingobjectidname',
          prop: 'regardingobjectidname',
          dataType: 'TEXT',
        },
        {
          name: 'transactioncurrencyid',
          prop: 'transactioncurrencyid',
          dataType: 'PICKUP',
        },
        {
          name: 'slaid',
          prop: 'slaid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'subject',
          dataType: 'TEXT',
        },
        {
          name: 'srfkey',
          prop: 'activityid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'activityid',
          dataType: 'GUID',
        },
        {
          name: 'serviceid',
          prop: 'serviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'scheduledend',
          prop: 'scheduledend',
          dataType: 'DATETIME',
        },
        {
          name: 'srfdatatype',
          prop: 'activitytypecode',
          dataType: 'SSCODELIST',
        },
        {
          name: 'activitypointer',
          prop: 'activityid',
        },
      {
        name: 'n_subject_like',
        prop: 'n_subject_like',
        dataType: 'TEXT',
      },
      {
        name: 'n_activitytypecode_eq',
        prop: 'n_activitytypecode_eq',
        dataType: 'SSCODELIST',
      },
      {
        name: 'n_prioritycode_eq',
        prop: 'n_prioritycode_eq',
        dataType: 'SSCODELIST',
      },
      {
        name: 'n_statuscode_eq',
        prop: 'n_statuscode_eq',
        dataType: 'NSCODELIST',
      },
      {
        name: 'n_statecode_eq',
        prop: 'n_statecode_eq',
        dataType: 'NSCODELIST',
      },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}
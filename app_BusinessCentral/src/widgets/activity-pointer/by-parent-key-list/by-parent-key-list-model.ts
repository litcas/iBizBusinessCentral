/**
 * ByParentKey 部件模型
 *
 * @export
 * @class ByParentKeyModel
 */
export default class ByParentKeyModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof ByParentKeyDashboard_sysportlet2_listMode
	 */
	public getDataItems(): any[] {
		return [
			{
				name: 'subject',
			},
			{
				name: 'activityid',
			},
			{
				name: 'activitytypecode',
        codelist:{tag:'ActivityTypeCode',codelistType:'STATIC'},
			},
			{
				name: 'srfkey',
				prop: 'activityid',
				dataType: 'GUID',
			},
			{
				name: 'srfmajortext',
				prop: 'subject',
				dataType: 'TEXT',
			},
			{
				name: 'srfdatatype',
				prop: 'activitytypecode',
				dataType: 'SSCODELIST',
        codelist:{tag:'ActivityTypeCode',codelistType:'STATIC'},
			},
			{
				name: 'serviceid',
				prop: 'serviceid',
				dataType: 'PICKUP',
			},
			{
				name: 'transactioncurrencyid',
				prop: 'transactioncurrencyid',
				dataType: 'PICKUP',
			},
			{
				name: 'slaid',
				prop: 'slaid',
				dataType: 'PICKUP',
			},
			{
				name: 'activitypointer',
				prop: 'activityid',
				dataType: 'FONTKEY',
			},
      {
        name:'size',
        prop:'size'
      },
      {
        name:'query',
        prop:'query'
      },
      {
        name:'sort',
        prop:'sort'
      },
      {
        name:'page',
        prop:'page'
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
		]
	}

}
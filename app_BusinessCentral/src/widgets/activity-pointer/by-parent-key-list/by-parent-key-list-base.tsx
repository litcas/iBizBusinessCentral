import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, ListControlBase } from '@/studio-core';
import ActivityPointerService from '@/service/activity-pointer/activity-pointer-service';
import ByParentKeyService from './by-parent-key-list-service';
import ActivityPointerUIService from '@/uiservice/activity-pointer/activity-pointer-ui-service';


/**
 * dashboard_sysportlet2_list部件基类
 *
 * @export
 * @class ListControlBase
 * @extends {ByParentKeyListBase}
 */
export class ByParentKeyListBase extends ListControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof ByParentKeyListBase
     */
    protected controlType: string = 'LIST';

    /**
     * 建构部件服务对象
     *
     * @type {ByParentKeyService}
     * @memberof ByParentKeyListBase
     */
    public service: ByParentKeyService = new ByParentKeyService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {ActivityPointerService}
     * @memberof ByParentKeyListBase
     */
    public appEntityService: ActivityPointerService = new ActivityPointerService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof ByParentKeyListBase
     */
    protected appDeName: string = 'activitypointer';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof ByParentKeyListBase
     */
    protected appDeLogicName: string = '活动';

    /**
     * 界面UI服务对象
     *
     * @type {ActivityPointerUIService}
     * @memberof ByParentKeyBase
     */  
    public appUIService:ActivityPointerUIService = new ActivityPointerUIService(this.$store);

    /**
     * 分页条数
     *
     * @type {number}
     * @memberof ByParentKeyListBase
     */
    public limit: number = 1000;

    /**
     * 排序方向
     *
     * @type {string}
     * @memberof ByParentKeyListBase
     */
    public minorSortDir: string = '';
}
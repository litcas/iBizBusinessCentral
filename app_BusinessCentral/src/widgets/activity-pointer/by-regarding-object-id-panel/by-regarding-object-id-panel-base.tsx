import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, PanelControlBase } from '@/studio-core';
import ActivityPointerService from '@/service/activity-pointer/activity-pointer-service';
import ByRegardingObjectIdService from './by-regarding-object-id-panel-service';
import ActivityPointerUIService from '@/uiservice/activity-pointer/activity-pointer-ui-service';
import { FormItemModel } from '@/model/form-detail';
import ByRegardingObjectIdModel from './by-regarding-object-id-panel-model';
import CodeListService from "@service/app/codelist-service";


/**
 * dashboard_sysportlet2_list_itempanel部件基类
 *
 * @export
 * @class PanelControlBase
 * @extends {ByRegardingObjectIdPanelBase}
 */
export class ByRegardingObjectIdPanelBase extends PanelControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof ByRegardingObjectIdPanelBase
     */
    protected controlType: string = 'PANEL';

    /**
     * 建构部件服务对象
     *
     * @type {ByRegardingObjectIdService}
     * @memberof ByRegardingObjectIdPanelBase
     */
    public service: ByRegardingObjectIdService = new ByRegardingObjectIdService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {ActivityPointerService}
     * @memberof ByRegardingObjectIdPanelBase
     */
    public appEntityService: ActivityPointerService = new ActivityPointerService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof ByRegardingObjectIdPanelBase
     */
    protected appDeName: string = 'activitypointer';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof ByRegardingObjectIdPanelBase
     */
    protected appDeLogicName: string = '活动';

    /**
     * 界面UI服务对象
     *
     * @type {ActivityPointerUIService}
     * @memberof ByRegardingObjectIdBase
     */  
    public appUIService:ActivityPointerUIService = new ActivityPointerUIService(this.$store);

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof ByRegardingObjectId
     */
    public detailsModel: any = {
        activitytypecode: new FormItemModel({ visible: true, disabled: false, enableCond: 3 }), 
        subject: new FormItemModel({ visible: true, disabled: false, enableCond: 3 }), 
        container1: new FormItemModel({ visible: true, disabled: false, enableCond: 3 }), 
    };

    /**
     * 面板逻辑
     *
     * @public
     * @param {{ name: string, newVal: any, oldVal: any }} { name, newVal, oldVal }
     * @memberof ByRegardingObjectId
     */
    public panelLogic({ name, newVal, oldVal }: { name: string, newVal: any, oldVal: any }): void {
                



    }

    /**
     * 数据模型对象
     *
     * @type {ByRegardingObjectIdModel}
     * @memberof ByRegardingObjectId
     */
    public dataModel: ByRegardingObjectIdModel = new ByRegardingObjectIdModel();

    /**
     * 界面行为
     *
     * @param {*} row
     * @param {*} tag
     * @param {*} $event
     * @memberof ByRegardingObjectId
     */
    public async uiAction(row: any, tag: any, $event: any) {
        await this.computePanelData();
    }
}
/**
 * ByRegardingObjectId 部件模型
 *
 * @export
 * @class ByRegardingObjectIdModel
 */
export default class ByRegardingObjectIdModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof ByRegardingObjectIdModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'activitytypecode',
        prop: 'activitytypecode'
      },
      {
        name: 'subject',
        prop: 'subject'
      }
    ]
  }
}
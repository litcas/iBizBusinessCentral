import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, SearchFormControlBase } from '@/studio-core';
import ActivityPointerService from '@/service/activity-pointer/activity-pointer-service';
import DefaultService from './default-searchform-service';
import ActivityPointerUIService from '@/uiservice/activity-pointer/activity-pointer-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';


/**
 * searchform部件基类
 *
 * @export
 * @class SearchFormControlBase
 * @extends {DefaultSearchFormBase}
 */
export class DefaultSearchFormBase extends SearchFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected controlType: string = 'SEARCHFORM';

    /**
     * 建构部件服务对象
     *
     * @type {DefaultService}
     * @memberof DefaultSearchFormBase
     */
    public service: DefaultService = new DefaultService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {ActivityPointerService}
     * @memberof DefaultSearchFormBase
     */
    public appEntityService: ActivityPointerService = new ActivityPointerService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeName: string = 'activitypointer';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeLogicName: string = '活动';

    /**
     * 界面UI服务对象
     *
     * @type {ActivityPointerUIService}
     * @memberof DefaultBase
     */  
    public appUIService:ActivityPointerUIService = new ActivityPointerUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public data: any = {
        n_subject_like: null,
        n_activitytypecode_eq: null,
        n_prioritycode_eq: null,
        n_statuscode_eq: null,
        n_statecode_eq: null,
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public detailsModel: any = {
        formpage1: new FormPageModel({ caption: '常规条件', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this })
, 
        n_subject_like: new FormItemModel({ caption: '主题(%)', detailType: 'FORMITEM', name: 'n_subject_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_activitytypecode_eq: new FormItemModel({ caption: '活动类型(等于(=))', detailType: 'FORMITEM', name: 'n_activitytypecode_eq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_prioritycode_eq: new FormItemModel({ caption: '优先级(等于(=))', detailType: 'FORMITEM', name: 'n_prioritycode_eq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_statuscode_eq: new FormItemModel({ caption: '状态描述(等于(=))', detailType: 'FORMITEM', name: 'n_statuscode_eq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_statecode_eq: new FormItemModel({ caption: '活动状态(等于(=))', detailType: 'FORMITEM', name: 'n_statecode_eq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
    };
}
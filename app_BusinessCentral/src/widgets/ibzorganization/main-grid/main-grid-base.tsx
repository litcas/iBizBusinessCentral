import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, GridControlBase } from '@/studio-core';
import IBZOrganizationService from '@/service/ibzorganization/ibzorganization-service';
import MainService from './main-grid-service';
import IBZOrganizationUIService from '@/uiservice/ibzorganization/ibzorganization-ui-service';
import { FormItemModel } from '@/model/form-detail';


/**
 * grid部件基类
 *
 * @export
 * @class GridControlBase
 * @extends {MainGridBase}
 */
export class MainGridBase extends GridControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected controlType: string = 'GRID';

    /**
     * 建构部件服务对象
     *
     * @type {MainService}
     * @memberof MainGridBase
     */
    public service: MainService = new MainService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {IBZOrganizationService}
     * @memberof MainGridBase
     */
    public appEntityService: IBZOrganizationService = new IBZOrganizationService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeName: string = 'ibzorganization';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeLogicName: string = '单位机构';

    /**
     * 界面UI服务对象
     *
     * @type {IBZOrganizationUIService}
     * @memberof MainBase
     */  
    public appUIService:IBZOrganizationUIService = new IBZOrganizationUIService(this.$store);

    /**
     * 界面行为模型
     *
     * @type {*}
     * @memberof MainBase
     */  
    public ActionModel: any = {
    };

    /**
     * 本地缓存标识
     *
     * @protected
     * @type {string}
     * @memberof MainBase
     */
    protected localStorageTag: string = 'ibzorg_main_grid';

    /**
     * 分页条数
     *
     * @type {number}
     * @memberof MainGridBase
     */
    public limit: number = 10;

    /**
     * 所有列成员
     *
     * @type {any[]}
     * @memberof MainGridBase
     */
    public allColumns: any[] = [
        {
            name: 'orgid',
            label: '单位标识',
            langtag: 'entities.ibzorganization.main_grid.columns.orgid',
            show: false,
            unit: 'px',
            isEnableRowEdit: false,
        },
        {
            name: 'orgcode',
            label: '单位代码',
            langtag: 'entities.ibzorganization.main_grid.columns.orgcode',
            show: true,
            unit: 'px',
            isEnableRowEdit: false,
        },
        {
            name: 'orgname',
            label: '名称',
            langtag: 'entities.ibzorganization.main_grid.columns.orgname',
            show: true,
            unit: 'px',
            isEnableRowEdit: false,
        },
        {
            name: 'orglevel',
            label: '单位级别',
            langtag: 'entities.ibzorganization.main_grid.columns.orglevel',
            show: true,
            unit: 'px',
            isEnableRowEdit: false,
        },
        {
            name: 'shortname',
            label: '单位简称',
            langtag: 'entities.ibzorganization.main_grid.columns.shortname',
            show: true,
            unit: 'px',
            isEnableRowEdit: false,
        },
        {
            name: 'porgname',
            label: '上级单位',
            langtag: 'entities.ibzorganization.main_grid.columns.porgname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'porgid',
            label: '上级单位',
            langtag: 'entities.ibzorganization.main_grid.columns.porgid',
            show: false,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'showorder',
            label: '排序',
            langtag: 'entities.ibzorganization.main_grid.columns.showorder',
            show: true,
            unit: 'px',
            isEnableRowEdit: false,
        },
        {
            name: 'createdate',
            label: '创建时间',
            langtag: 'entities.ibzorganization.main_grid.columns.createdate',
            show: true,
            unit: 'px',
            isEnableRowEdit: false,
        },
        {
            name: 'updatedate',
            label: '最后修改时间',
            langtag: 'entities.ibzorganization.main_grid.columns.updatedate',
            show: true,
            unit: 'px',
            isEnableRowEdit: false,
        },
    ]

    /**
     * 获取表格行模型
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public getGridRowModel(){
        return {
          srfkey: new FormItemModel(),
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public rules: any = {
        srfkey: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '单位标识 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '单位标识 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 获取对应列class
     *
     * @type {*}
     * @memberof MainBase
     */
    public hasRowEdit: any = {
        'orgid':false,
        'orgcode':false,
        'orgname':false,
        'orglevel':false,
        'shortname':false,
        'porgname':false,
        'porgid':false,
        'showorder':false,
        'createdate':false,
        'updatedate':false,
    };

    /**
     * 获取对应列class
     *
     * @param {*} $args row 行数据，column 列数据，rowIndex 行索引，列索引
     * @returns {void}
     * @memberof MainBase
     */
    public getCellClassName(args: {row: any, column: any, rowIndex: number, columnIndex: number}): any {
        return ( this.hasRowEdit[args.column.property] && this.actualIsOpenEdit ) ? "edit-cell" : "info-cell";
    }


    /**
     * 导出数据格式化
     *
     * @param {*} filterVal
     * @param {*} jsonData
     * @param {any[]} [codelistColumns=[]]
     * @returns {Promise<any>}
     * @memberof MainGridBase
     */
    public async formatExcelData(filterVal: any, jsonData: any, codelistColumns?: any[]): Promise<any> {
        return super.formatExcelData(filterVal, jsonData, [
        ]);
    }

}
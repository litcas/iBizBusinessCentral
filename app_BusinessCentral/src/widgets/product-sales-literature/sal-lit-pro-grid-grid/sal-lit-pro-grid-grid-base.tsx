import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, GridControlBase } from '@/studio-core';
import ProductSalesLiteratureService from '@/service/product-sales-literature/product-sales-literature-service';
import SalLitProGridService from './sal-lit-pro-grid-grid-service';
import ProductSalesLiteratureUIService from '@/uiservice/product-sales-literature/product-sales-literature-ui-service';
import { FormItemModel } from '@/model/form-detail';


/**
 * grid部件基类
 *
 * @export
 * @class GridControlBase
 * @extends {SalLitProGridGridBase}
 */
export class SalLitProGridGridBase extends GridControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof SalLitProGridGridBase
     */
    protected controlType: string = 'GRID';

    /**
     * 建构部件服务对象
     *
     * @type {SalLitProGridService}
     * @memberof SalLitProGridGridBase
     */
    public service: SalLitProGridService = new SalLitProGridService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {ProductSalesLiteratureService}
     * @memberof SalLitProGridGridBase
     */
    public appEntityService: ProductSalesLiteratureService = new ProductSalesLiteratureService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof SalLitProGridGridBase
     */
    protected appDeName: string = 'productsalesliterature';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof SalLitProGridGridBase
     */
    protected appDeLogicName: string = '实体';

    /**
     * 界面UI服务对象
     *
     * @type {ProductSalesLiteratureUIService}
     * @memberof SalLitProGridBase
     */  
    public appUIService:ProductSalesLiteratureUIService = new ProductSalesLiteratureUIService(this.$store);

    /**
     * 界面行为模型
     *
     * @type {*}
     * @memberof SalLitProGridBase
     */  
    public ActionModel: any = {
    };

    /**
     * 本地缓存标识
     *
     * @protected
     * @type {string}
     * @memberof SalLitProGridBase
     */
    protected localStorageTag: string = 'productsalesliterature_sallitprogrid_grid';

    /**
     * 所有列成员
     *
     * @type {any[]}
     * @memberof SalLitProGridGridBase
     */
    public allColumns: any[] = [
        {
            name: 'entity2name',
            label: '销售宣传资料',
            langtag: 'entities.productsalesliterature.sallitprogrid_grid.columns.entity2name',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'entityname',
            label: '产品',
            langtag: 'entities.productsalesliterature.sallitprogrid_grid.columns.entityname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
    ]

    /**
     * 获取表格行模型
     *
     * @type {*}
     * @memberof SalLitProGridGridBase
     */
    public getGridRowModel(){
        return {
          srfkey: new FormItemModel(),
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof SalLitProGridGridBase
     */
    public rules: any = {
        srfkey: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '关系标识 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '关系标识 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 获取对应列class
     *
     * @type {*}
     * @memberof SalLitProGridBase
     */
    public hasRowEdit: any = {
        'entity2name':false,
        'entityname':false,
    };

    /**
     * 获取对应列class
     *
     * @param {*} $args row 行数据，column 列数据，rowIndex 行索引，列索引
     * @returns {void}
     * @memberof SalLitProGridBase
     */
    public getCellClassName(args: {row: any, column: any, rowIndex: number, columnIndex: number}): any {
        return ( this.hasRowEdit[args.column.property] && this.actualIsOpenEdit ) ? "edit-cell" : "info-cell";
    }


    /**
     * 导出数据格式化
     *
     * @param {*} filterVal
     * @param {*} jsonData
     * @param {any[]} [codelistColumns=[]]
     * @returns {Promise<any>}
     * @memberof SalLitProGridGridBase
     */
    public async formatExcelData(filterVal: any, jsonData: any, codelistColumns?: any[]): Promise<any> {
        return super.formatExcelData(filterVal, jsonData, [
        ]);
    }

}
import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, EditFormControlBase } from '@/studio-core';
import JobsInfoService from '@/service/jobs-info/jobs-info-service';
import MainService from './main-form-service';
import JobsInfoUIService from '@/uiservice/jobs-info/jobs-info-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {MainEditFormBase}
 */
export class MainEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof MainEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {MainService}
     * @memberof MainEditFormBase
     */
    public service: MainService = new MainService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {JobsInfoService}
     * @memberof MainEditFormBase
     */
    public appEntityService: JobsInfoService = new JobsInfoService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MainEditFormBase
     */
    protected appDeName: string = 'jobsinfo';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof MainEditFormBase
     */
    protected appDeLogicName: string = '任务信息';

    /**
     * 界面UI服务对象
     *
     * @type {JobsInfoUIService}
     * @memberof MainBase
     */  
    public appUIService:JobsInfoUIService = new JobsInfoUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof MainEditFormBase
     */
    public data: any = {
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        app: null,
        handler: null,
        cron: null,
        param: null,
        last_time: null,
        next_time: null,
        timeout: null,
        fail_retry_count: null,
        author: null,
        remark: null,
        status: null,
        tenant_id: null,
        create_time: null,
        update_time: null,
        id: null,
        jobsinfo:null,
    };

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof MainEditFormBase
     */
    public rules: any = {
        app: [
            { required: true, type: 'string', message: '服务名 值不能为空', trigger: 'change' },
            { required: true, type: 'string', message: '服务名 值不能为空', trigger: 'blur' },
        ],
        cron: [
            { required: true, type: 'string', message: '任务执行CRON 值不能为空', trigger: 'change' },
            { required: true, type: 'string', message: '任务执行CRON 值不能为空', trigger: 'blur' },
        ],
        last_time: [
            { required: true, type: 'number', message: '上次调度时间 值不能为空', trigger: 'change' },
            { required: true, type: 'number', message: '上次调度时间 值不能为空', trigger: 'blur' },
        ],
        next_time: [
            { required: true, type: 'number', message: '下次调度时间 值不能为空', trigger: 'change' },
            { required: true, type: 'number', message: '下次调度时间 值不能为空', trigger: 'blur' },
        ],
        timeout: [
            { required: true, type: 'number', message: '任务执行超时时间（秒） 值不能为空', trigger: 'change' },
            { required: true, type: 'number', message: '任务执行超时时间（秒） 值不能为空', trigger: 'blur' },
        ],
        fail_retry_count: [
            { required: true, type: 'number', message: '失败重试次数 值不能为空', trigger: 'change' },
            { required: true, type: 'number', message: '失败重试次数 值不能为空', trigger: 'blur' },
        ],
        status: [
            { required: true, type: 'number', message: '状态 值不能为空', trigger: 'change' },
            { required: true, type: 'number', message: '状态 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof MainBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof MainEditFormBase
     */
    public detailsModel: any = {
        group1: new FormGroupPanelModel({ caption: '任务信息基本信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.jobsinfo.main_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: '主键ID', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfmajortext: new FormItemModel({ caption: '执行器任务HANDLER', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        app: new FormItemModel({ caption: '服务名', detailType: 'FORMITEM', name: 'app', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        handler: new FormItemModel({ caption: '执行器任务HANDLER', detailType: 'FORMITEM', name: 'handler', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        cron: new FormItemModel({ caption: '任务执行CRON', detailType: 'FORMITEM', name: 'cron', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        param: new FormItemModel({ caption: '执行器任务参数', detailType: 'FORMITEM', name: 'param', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        last_time: new FormItemModel({ caption: '上次调度时间', detailType: 'FORMITEM', name: 'last_time', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        next_time: new FormItemModel({ caption: '下次调度时间', detailType: 'FORMITEM', name: 'next_time', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        timeout: new FormItemModel({ caption: '任务执行超时时间（秒）', detailType: 'FORMITEM', name: 'timeout', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        fail_retry_count: new FormItemModel({ caption: '失败重试次数', detailType: 'FORMITEM', name: 'fail_retry_count', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        author: new FormItemModel({ caption: '所有者', detailType: 'FORMITEM', name: 'author', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        remark: new FormItemModel({ caption: '备注', detailType: 'FORMITEM', name: 'remark', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        status: new FormItemModel({ caption: '状态', detailType: 'FORMITEM', name: 'status', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        tenant_id: new FormItemModel({ caption: '租户ID', detailType: 'FORMITEM', name: 'tenant_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        create_time: new FormItemModel({ caption: '创建时间', detailType: 'FORMITEM', name: 'create_time', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        update_time: new FormItemModel({ caption: '更新时间', detailType: 'FORMITEM', name: 'update_time', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        id: new FormItemModel({ caption: '主键ID', detailType: 'FORMITEM', name: 'id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

    };
}
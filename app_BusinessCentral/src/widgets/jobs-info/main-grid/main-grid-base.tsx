import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, GridControlBase } from '@/studio-core';
import JobsInfoService from '@/service/jobs-info/jobs-info-service';
import MainService from './main-grid-service';
import JobsInfoUIService from '@/uiservice/jobs-info/jobs-info-ui-service';
import { FormItemModel } from '@/model/form-detail';


/**
 * grid部件基类
 *
 * @export
 * @class GridControlBase
 * @extends {MainGridBase}
 */
export class MainGridBase extends GridControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected controlType: string = 'GRID';

    /**
     * 建构部件服务对象
     *
     * @type {MainService}
     * @memberof MainGridBase
     */
    public service: MainService = new MainService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {JobsInfoService}
     * @memberof MainGridBase
     */
    public appEntityService: JobsInfoService = new JobsInfoService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeName: string = 'jobsinfo';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeLogicName: string = '任务信息';

    /**
     * 界面UI服务对象
     *
     * @type {JobsInfoUIService}
     * @memberof MainBase
     */  
    public appUIService:JobsInfoUIService = new JobsInfoUIService(this.$store);

    /**
     * 界面行为模型
     *
     * @type {*}
     * @memberof MainBase
     */  
    public ActionModel: any = {
    };

    /**
     * 本地缓存标识
     *
     * @protected
     * @type {string}
     * @memberof MainBase
     */
    protected localStorageTag: string = 'jobs_info_main_grid';

    /**
     * 所有列成员
     *
     * @type {any[]}
     * @memberof MainGridBase
     */
    public allColumns: any[] = [
        {
            name: 'id',
            label: '主键ID',
            langtag: 'entities.jobsinfo.main_grid.columns.id',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'app',
            label: '服务名',
            langtag: 'entities.jobsinfo.main_grid.columns.app',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'handler',
            label: '执行器任务HANDLER',
            langtag: 'entities.jobsinfo.main_grid.columns.handler',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'cron',
            label: '任务执行CRON',
            langtag: 'entities.jobsinfo.main_grid.columns.cron',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'last_time',
            label: '上次调度时间',
            langtag: 'entities.jobsinfo.main_grid.columns.last_time',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'next_time',
            label: '下次调度时间',
            langtag: 'entities.jobsinfo.main_grid.columns.next_time',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'status',
            label: '状态',
            langtag: 'entities.jobsinfo.main_grid.columns.status',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'timeout',
            label: '任务执行超时时间（秒）',
            langtag: 'entities.jobsinfo.main_grid.columns.timeout',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'update_time',
            label: '更新时间',
            langtag: 'entities.jobsinfo.main_grid.columns.update_time',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
    ]

    /**
     * 获取表格行模型
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public getGridRowModel(){
        return {
          srfkey: new FormItemModel(),
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public rules: any = {
        srfkey: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '主键ID 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '主键ID 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 获取对应列class
     *
     * @type {*}
     * @memberof MainBase
     */
    public hasRowEdit: any = {
        'id':false,
        'app':false,
        'handler':false,
        'cron':false,
        'last_time':false,
        'next_time':false,
        'status':false,
        'timeout':false,
        'update_time':false,
    };

    /**
     * 获取对应列class
     *
     * @param {*} $args row 行数据，column 列数据，rowIndex 行索引，列索引
     * @returns {void}
     * @memberof MainBase
     */
    public getCellClassName(args: {row: any, column: any, rowIndex: number, columnIndex: number}): any {
        return ( this.hasRowEdit[args.column.property] && this.actualIsOpenEdit ) ? "edit-cell" : "info-cell";
    }


    /**
     * 导出数据格式化
     *
     * @param {*} filterVal
     * @param {*} jsonData
     * @param {any[]} [codelistColumns=[]]
     * @returns {Promise<any>}
     * @memberof MainGridBase
     */
    public async formatExcelData(filterVal: any, jsonData: any, codelistColumns?: any[]): Promise<any> {
        return super.formatExcelData(filterVal, jsonData, [
            {
                name: 'status',
                srfkey: 'CodeListJobStatus',
                codelistType : 'STATIC',
                renderMode: 'other',
                textSeparator: '、',
                valueSeparator: ',',
            },
        ]);
    }

}
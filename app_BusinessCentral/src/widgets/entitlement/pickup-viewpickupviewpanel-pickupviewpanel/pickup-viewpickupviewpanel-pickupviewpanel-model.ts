/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'startdate',
      },
      {
        name: 'processid',
      },
      {
        name: 'traversedpath',
      },
      {
        name: 'decreaseremainingon',
      },
      {
        name: 'entitytype',
      },
      {
        name: 'contactname',
      },
      {
        name: 'importsequencenumber',
      },
      {
        name: 'enddate',
      },
      {
        name: 'statecode',
      },
      {
        name: 'createman',
      },
      {
        name: 'customername',
      },
      {
        name: 'kbaccesslevel',
      },
      {
        name: 'description',
      },
      {
        name: 'emailaddress',
      },
      {
        name: 'timezoneruleversionnumber',
      },
      {
        name: 'exchangerate',
      },
      {
        name: 'customertype',
      },
      {
        name: 'versionnumber',
      },
      {
        name: 'ownerid',
      },
      {
        name: 'utcconversiontimezonecode',
      },
      {
        name: 'createdate',
      },
      {
        name: 'allocationtypecode',
      },
      {
        name: 'statuscode',
      },
      {
        name: 'totalterms',
      },
      {
        name: 'customerid',
      },
      {
        name: 'ownername',
      },
      {
        name: 'restrictcasecreation',
      },
      {
        name: 'entitlement',
        prop: 'entitlementid',
      },
      {
        name: 'updateman',
      },
      {
        name: 'remainingterms',
      },
      {
        name: 'accountname',
      },
      {
        name: 'overriddencreatedon',
      },
      {
        name: 'ownertype',
      },
      {
        name: 'stageid',
      },
      {
        name: 'entitlementname',
      },
      {
        name: 'ibizdefault',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'transactioncurrencyid',
      },
      {
        name: 'slaid',
      },
      {
        name: 'entitlementtemplateid',
      },
    ]
  }


}
/**
 * Edit_DatePanel 部件模型
 *
 * @export
 * @class Edit_DatePanelModel
 */
export default class Edit_DatePanelModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Edit_DatePanelModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'leadid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'fullname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'revenue',
        prop: 'revenue',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'numberofemployees',
        prop: 'numberofemployees',
        dataType: 'INT',
      },
      {
        name: 'statuscode',
        prop: 'statuscode',
        dataType: 'NSCODELIST',
      },
      {
        name: 'ownername',
        prop: 'ownername',
        dataType: 'TEXT',
      },
      {
        name: 'leadid',
        prop: 'leadid',
        dataType: 'GUID',
      },
      {
        name: 'lead',
        prop: 'leadid',
        dataType: 'FONTKEY',
      },
    ]
  }

}
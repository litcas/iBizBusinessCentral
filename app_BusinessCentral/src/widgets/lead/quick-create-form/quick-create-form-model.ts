/**
 * QuickCreate 部件模型
 *
 * @export
 * @class QuickCreateModel
 */
export default class QuickCreateModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof QuickCreateModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'leadid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'fullname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'subject',
        prop: 'subject',
        dataType: 'TEXT',
      },
      {
        name: 'budgetamount',
        prop: 'budgetamount',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'purchasetimeframe',
        prop: 'purchasetimeframe',
        dataType: 'SSCODELIST',
      },
      {
        name: 'leadsourcecode',
        prop: 'leadsourcecode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'fullname',
        prop: 'fullname',
        dataType: 'TEXT',
      },
      {
        name: 'companyname',
        prop: 'companyname',
        dataType: 'TEXT',
      },
      {
        name: 'jobtitle',
        prop: 'jobtitle',
        dataType: 'TEXT',
      },
      {
        name: 'mobilephone',
        prop: 'mobilephone',
        dataType: 'TEXT',
      },
      {
        name: 'emailaddress1',
        prop: 'emailaddress1',
        dataType: 'TEXT',
      },
      {
        name: 'leadid',
        prop: 'leadid',
        dataType: 'GUID',
      },
      {
        name: 'lead',
        prop: 'leadid',
        dataType: 'FONTKEY',
      },
    ]
  }

}
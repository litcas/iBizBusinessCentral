import { Http,Util,Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import LeadService from '@/service/lead/lead-service';
import StatusTabViewtabexppanelModel from './status-tab-viewtabexppanel-tabexppanel-model';


/**
 * StatusTabViewtabexppanel 部件服务对象
 *
 * @export
 * @class StatusTabViewtabexppanelService
 */
export default class StatusTabViewtabexppanelService extends ControlService {

    /**
     * 潜在顾客服务对象
     *
     * @type {LeadService}
     * @memberof StatusTabViewtabexppanelService
     */
    public appEntityService: LeadService = new LeadService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof StatusTabViewtabexppanelService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of StatusTabViewtabexppanelService.
     * 
     * @param {*} [opts={}]
     * @memberof StatusTabViewtabexppanelService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new StatusTabViewtabexppanelModel();
    }

    
}
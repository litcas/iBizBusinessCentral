/**
 * StatusTabViewtabexppanel 部件模型
 *
 * @export
 * @class StatusTabViewtabexppanelModel
 */
export default class StatusTabViewtabexppanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof StatusTabViewtabexppanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'address1_fax',
      },
      {
        name: 'address2_utcoffset',
      },
      {
        name: 'jobtitle',
      },
      {
        name: 'address2_country',
      },
      {
        name: 'budgetamount',
      },
      {
        name: 'address2_fax',
      },
      {
        name: 'onholdtime',
      },
      {
        name: 'lastname',
      },
      {
        name: 'address1_telephone2',
      },
      {
        name: 'address1_stateorprovince',
      },
      {
        name: 'estimatedvalue',
      },
      {
        name: 'lead',
        prop: 'leadid',
      },
      {
        name: 'address1_longitude',
      },
      {
        name: 'address1_line1',
      },
      {
        name: 'leadqualitycode',
      },
      {
        name: 'donotphone',
      },
      {
        name: 'exchangerate',
      },
      {
        name: 'entityimage_url',
      },
      {
        name: 'createman',
      },
      {
        name: 'address2_stateorprovince',
      },
      {
        name: 'description',
      },
      {
        name: 'numberofemployees',
      },
      {
        name: 'address1_city',
      },
      {
        name: 'entityimageid',
      },
      {
        name: 'address2_line3',
      },
      {
        name: 'statuscode',
      },
      {
        name: 'address2_line1',
      },
      {
        name: 'customerid',
      },
      {
        name: 'companyname',
      },
      {
        name: 'versionnumber',
      },
      {
        name: 'qualificationcomments',
      },
      {
        name: 'address2_name',
      },
      {
        name: 'emailaddress1',
      },
      {
        name: 'followemail',
      },
      {
        name: 'address1_country',
      },
      {
        name: 'websiteurl',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'address1_line3',
      },
      {
        name: 'address2_addressid',
      },
      {
        name: 'address1_composite',
      },
      {
        name: 'subject',
      },
      {
        name: 'address1_utcoffset',
      },
      {
        name: 'budgetamount_base',
      },
      {
        name: 'address1_addresstypecode',
      },
      {
        name: 'address2_telephone3',
      },
      {
        name: 'customername',
      },
      {
        name: 'masterleadname',
      },
      {
        name: 'evaluatefit',
      },
      {
        name: 'address1_latitude',
      },
      {
        name: 'address1_telephone3',
      },
      {
        name: 'fullname',
      },
      {
        name: 'estimatedamount_base',
      },
      {
        name: 'budgetstatus',
      },
      {
        name: 'industrycode',
      },
      {
        name: 'address1_line2',
      },
      {
        name: 'ownername',
      },
      {
        name: 'initialcommunication',
      },
      {
        name: 'address1_postofficebox',
      },
      {
        name: 'address1_telephone1',
      },
      {
        name: 'revenue',
      },
      {
        name: 'address2_county',
      },
      {
        name: 'stageid',
      },
      {
        name: 'address1_shippingmethodcode',
      },
      {
        name: 'estimatedamount',
      },
      {
        name: 'address1_county',
      },
      {
        name: 'utcconversiontimezonecode',
      },
      {
        name: 'lastonholdtime',
      },
      {
        name: 'createdate',
      },
      {
        name: 'teamsfollowed',
      },
      {
        name: 'salutation',
      },
      {
        name: 'address2_shippingmethodcode',
      },
      {
        name: 'address2_latitude',
      },
      {
        name: 'participatesinworkflow',
      },
      {
        name: 'address2_composite',
      },
      {
        name: 'salesstage',
      },
      {
        name: 'donotpostalmail',
      },
      {
        name: 'revenue_base',
      },
      {
        name: 'overriddencreatedon',
      },
      {
        name: 'schedulefollowup_prospect',
      },
      {
        name: 'address2_city',
      },
      {
        name: 'telephone1',
      },
      {
        name: 'ibizprivate',
      },
      {
        name: 'mobilephone',
      },
      {
        name: 'need',
      },
      {
        name: 'prioritycode',
      },
      {
        name: 'address1_addressid',
      },
      {
        name: 'salesstagecode',
      },
      {
        name: 'importsequencenumber',
      },
      {
        name: 'address2_telephone1',
      },
      {
        name: 'ownerid',
      },
      {
        name: 'middlename',
      },
      {
        name: 'telephone2',
      },
      {
        name: 'purchasetimeframe',
      },
      {
        name: 'leadsourcecode',
      },
      {
        name: 'entityimage_timestamp',
      },
      {
        name: 'preferredcontactmethodcode',
      },
      {
        name: 'entityimage',
      },
      {
        name: 'address2_telephone2',
      },
      {
        name: 'donotsendmm',
      },
      {
        name: 'purchaseprocess',
      },
      {
        name: 'donotbulkemail',
      },
      {
        name: 'sic',
      },
      {
        name: 'contactname',
      },
      {
        name: 'donotemail',
      },
      {
        name: 'address2_longitude',
      },
      {
        name: 'confirminterest',
      },
      {
        name: 'ownertype',
      },
      {
        name: 'address2_postofficebox',
      },
      {
        name: 'statecode',
      },
      {
        name: 'autocreate',
      },
      {
        name: 'address1_name',
      },
      {
        name: 'timezoneruleversionnumber',
      },
      {
        name: 'lastusedincampaign',
      },
      {
        name: 'estimatedclosedate',
      },
      {
        name: 'address2_line2',
      },
      {
        name: 'emailaddress3',
      },
      {
        name: 'pager',
      },
      {
        name: 'address2_upszone',
      },
      {
        name: 'traversedpath',
      },
      {
        name: 'fax',
      },
      {
        name: 'schedulefollowup_qualify',
      },
      {
        name: 'telephone3',
      },
      {
        name: 'processid',
      },
      {
        name: 'emailaddress2',
      },
      {
        name: 'updateman',
      },
      {
        name: 'decisionmaker',
      },
      {
        name: 'address1_postalcode',
      },
      {
        name: 'address2_postalcode',
      },
      {
        name: 'donotfax',
      },
      {
        name: 'customertype',
      },
      {
        name: 'firstname',
      },
      {
        name: 'merged',
      },
      {
        name: 'address2_addresstypecode',
      },
      {
        name: 'address1_upszone',
      },
      {
        name: 'slaname',
      },
      {
        name: 'relatedobjectname',
      },
      {
        name: 'currencyname',
      },
      {
        name: 'originatingcasename',
      },
      {
        name: 'campaignname',
      },
      {
        name: 'parentaccountname',
      },
      {
        name: 'parentcontactname',
      },
      {
        name: 'qualifyingopportunityname',
      },
      {
        name: 'qualifyingopportunityid',
      },
      {
        name: 'slaid',
      },
      {
        name: 'campaignid',
      },
      {
        name: 'relatedobjectid',
      },
      {
        name: 'transactioncurrencyid',
      },
      {
        name: 'parentaccountid',
      },
      {
        name: 'originatingcaseid',
      },
      {
        name: 'parentcontactid',
      },
    ]
  }


}
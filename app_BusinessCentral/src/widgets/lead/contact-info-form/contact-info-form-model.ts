/**
 * ContactInfo 部件模型
 *
 * @export
 * @class ContactInfoModel
 */
export default class ContactInfoModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof ContactInfoModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'leadid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'fullname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'subject',
        prop: 'subject',
        dataType: 'TEXT',
      },
      {
        name: 'fullname',
        prop: 'fullname',
        dataType: 'TEXT',
      },
      {
        name: 'jobtitle',
        prop: 'jobtitle',
        dataType: 'TEXT',
      },
      {
        name: 'telephone1',
        prop: 'telephone1',
        dataType: 'TEXT',
      },
      {
        name: 'mobilephone',
        prop: 'mobilephone',
        dataType: 'TEXT',
      },
      {
        name: 'emailaddress1',
        prop: 'emailaddress1',
        dataType: 'TEXT',
      },
      {
        name: 'companyname',
        prop: 'companyname',
        dataType: 'TEXT',
      },
      {
        name: 'websiteurl',
        prop: 'websiteurl',
        dataType: 'TEXT',
      },
      {
        name: 'address1_postalcode',
        prop: 'address1_postalcode',
        dataType: 'TEXT',
      },
      {
        name: 'address1_country',
        prop: 'address1_country',
        dataType: 'TEXT',
      },
      {
        name: 'address1_stateorprovince',
        prop: 'address1_stateorprovince',
        dataType: 'TEXT',
      },
      {
        name: 'address1_city',
        prop: 'address1_city',
        dataType: 'TEXT',
      },
      {
        name: 'address1_line1',
        prop: 'address1_line1',
        dataType: 'TEXT',
      },
      {
        name: 'industrycode',
        prop: 'industrycode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'revenue',
        prop: 'revenue',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'numberofemployees',
        prop: 'numberofemployees',
        dataType: 'INT',
      },
      {
        name: 'transactioncurrencyname',
        prop: 'currencyname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'campaignname',
        prop: 'campaignname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'donotsendmm',
        prop: 'donotsendmm',
        dataType: 'YESNO',
      },
      {
        name: 'lastusedincampaign',
        prop: 'lastusedincampaign',
        dataType: 'DATETIME',
      },
      {
        name: 'leadid',
        prop: 'leadid',
        dataType: 'GUID',
      },
      {
        name: 'lead',
        prop: 'leadid',
        dataType: 'FONTKEY',
      },
    ]
  }

}
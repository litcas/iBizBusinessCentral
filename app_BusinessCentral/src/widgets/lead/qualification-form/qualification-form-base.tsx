import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, EditFormControlBase } from '@/studio-core';
import LeadService from '@/service/lead/lead-service';
import QualificationService from './qualification-form-service';
import LeadUIService from '@/uiservice/lead/lead-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {QualificationEditFormBase}
 */
export class QualificationEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof QualificationEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {QualificationService}
     * @memberof QualificationEditFormBase
     */
    public service: QualificationService = new QualificationService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {LeadService}
     * @memberof QualificationEditFormBase
     */
    public appEntityService: LeadService = new LeadService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof QualificationEditFormBase
     */
    protected appDeName: string = 'lead';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof QualificationEditFormBase
     */
    protected appDeLogicName: string = '潜在顾客';

    /**
     * 界面UI服务对象
     *
     * @type {LeadUIService}
     * @memberof QualificationBase
     */  
    public appUIService:LeadUIService = new LeadUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof QualificationEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        parentcontactname: null,
        parentaccountname: null,
        purchasetimeframe: null,
        budgetamount: null,
        purchaseprocess: null,
        decisionmaker: null,
        description: null,
        fullname: null,
        companyname: null,
        parentcontactid: null,
        parentaccountid: null,
        leadid: null,
        lead:null,
    };

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof QualificationEditFormBase
     */
    public rules: any = {
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof QualificationBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof QualificationEditFormBase
     */
    public detailsModel: any = {
        group1: new FormGroupPanelModel({ caption: '潜在顾客基本信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.lead.qualification_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: '潜在顾客', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfmajortext: new FormItemModel({ caption: '姓名', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        parentcontactname: new FormItemModel({ caption: '识别联系人', detailType: 'FORMITEM', name: 'parentcontactname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        parentaccountname: new FormItemModel({ caption: '识别客户', detailType: 'FORMITEM', name: 'parentaccountname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        purchasetimeframe: new FormItemModel({ caption: '购买时间范围', detailType: 'FORMITEM', name: 'purchasetimeframe', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        budgetamount: new FormItemModel({ caption: '预算金额', detailType: 'FORMITEM', name: 'budgetamount', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        purchaseprocess: new FormItemModel({ caption: '采购程序', detailType: 'FORMITEM', name: 'purchaseprocess', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        decisionmaker: new FormItemModel({ caption: '决策者?', detailType: 'FORMITEM', name: 'decisionmaker', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        description: new FormItemModel({ caption: '说明', detailType: 'FORMITEM', name: 'description', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        fullname: new FormItemModel({ caption: '姓名', detailType: 'FORMITEM', name: 'fullname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        companyname: new FormItemModel({ caption: '公司名称', detailType: 'FORMITEM', name: 'companyname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        parentcontactid: new FormItemModel({ caption: '潜在顾客的上司', detailType: 'FORMITEM', name: 'parentcontactid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        parentaccountid: new FormItemModel({ caption: '潜在顾客的上级单位', detailType: 'FORMITEM', name: 'parentaccountid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        leadid: new FormItemModel({ caption: '潜在顾客', detailType: 'FORMITEM', name: 'leadid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

    };
}
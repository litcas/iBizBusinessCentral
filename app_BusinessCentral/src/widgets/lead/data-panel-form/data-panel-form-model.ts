/**
 * DataPanel 部件模型
 *
 * @export
 * @class DataPanelModel
 */
export default class DataPanelModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof DataPanelModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'leadid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'fullname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'revenue',
        prop: 'revenue',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'numberofemployees',
        prop: 'numberofemployees',
        dataType: 'INT',
      },
      {
        name: 'statuscode',
        prop: 'statuscode',
        dataType: 'NSCODELIST',
      },
      {
        name: 'parentaccountname',
        prop: 'parentaccountname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'parentcontactname',
        prop: 'parentcontactname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'ownername',
        prop: 'ownername',
        dataType: 'TEXT',
      },
      {
        name: 'parentcontactid',
        prop: 'parentcontactid',
        dataType: 'PICKUP',
      },
      {
        name: 'parentaccountid',
        prop: 'parentaccountid',
        dataType: 'PICKUP',
      },
      {
        name: 'leadid',
        prop: 'leadid',
        dataType: 'GUID',
      },
      {
        name: 'lead',
        prop: 'leadid',
        dataType: 'FONTKEY',
      },
    ]
  }

}
import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * MasterSummary 部件服务对象
 *
 * @export
 * @class MasterSummaryService
 */
export default class MasterSummaryService extends ControlService {
}

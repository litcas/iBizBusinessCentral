import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, MainControlBase } from '@/studio-core';
import LegalService from '@/service/legal/legal-service';
import MasterSummaryService from './master-summary-portlet-service';
import LegalUIService from '@/uiservice/legal/legal-ui-service';
import { Environment } from '@/environments/environment';


/**
 * dashboard_sysportlet1部件基类
 *
 * @export
 * @class MainControlBase
 * @extends {MasterSummaryPortletBase}
 */
export class MasterSummaryPortletBase extends MainControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof MasterSummaryPortletBase
     */
    protected controlType: string = 'PORTLET';

    /**
     * 建构部件服务对象
     *
     * @type {MasterSummaryService}
     * @memberof MasterSummaryPortletBase
     */
    public service: MasterSummaryService = new MasterSummaryService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {LegalService}
     * @memberof MasterSummaryPortletBase
     */
    public appEntityService: LegalService = new LegalService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MasterSummaryPortletBase
     */
    protected appDeName: string = 'legal';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof MasterSummaryPortletBase
     */
    protected appDeLogicName: string = '法人';

    /**
     * 界面UI服务对象
     *
     * @type {LegalUIService}
     * @memberof MasterSummaryBase
     */  
    public appUIService:LegalUIService = new LegalUIService(this.$store);

    /**
     * 长度
     *
     * @type {number}
     * @memberof MasterSummary
     */
    @Prop() public height?: number;

    /**
     * 宽度
     *
     * @type {number}
     * @memberof MasterSummary
     */
    @Prop() public width?: number;



    /**
     * 是否自适应大小
     *
     * @returns {boolean}
     * @memberof MasterSummaryBase
     */
    @Prop({default: false})public isAdaptiveSize!: boolean;

    /**
     * 获取多项数据
     *
     * @returns {any[]}
     * @memberof MasterSummaryBase
     */
    public getDatas(): any[] {
        return [];
    }

    /**
     * 获取单项树
     *
     * @returns {*}
     * @memberof MasterSummaryBase
     */
    public getData(): any {
        return {};
    }

    /**
     * 获取高度
     *
     * @returns {any[]}
     * @memberof MasterSummaryBase
     */
    get getHeight(): any{
        if(!this.$util.isEmpty(this.height) && !this.$util.isNumberNaN(this.height)){
            if(this.height == 0){
                return 'auto';
            } else {
                return this.height+'px';
            }
        } else {
            return 'auto';
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof MasterSummaryBase
     */
    public created() {
        this.afterCreated();
    }

    /**
     * 执行created后的逻辑
     *
     *  @memberof MasterSummaryBase
     */    
    public afterCreated(){
        if (this.viewState) {
            this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }) => {
                if (!Object.is(tag, this.name)) {
                    return;
                }
                const refs: any = this.$refs;
                Object.keys(refs).forEach((_name: string) => {
                    this.viewState.next({ tag: _name, action: action, data: data });
                });
            });
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof MasterSummaryBase
     */
    public destroyed() {
        this.afterDestroy();
    }

    /**
     * 执行destroyed后的逻辑
     *
     * @memberof MasterSummaryBase
     */
    public afterDestroy() {
        if (this.viewStateEvent) {
            this.viewStateEvent.unsubscribe();
        }
    }


}

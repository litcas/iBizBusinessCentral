import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * MasterTabInfoViewtabviewpanel2 部件服务对象
 *
 * @export
 * @class MasterTabInfoViewtabviewpanel2Service
 */
export default class MasterTabInfoViewtabviewpanel2Service extends ControlService {
}
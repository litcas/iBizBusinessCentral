/**
 * MasterTabInfoViewtabviewpanel2 部件模型
 *
 * @export
 * @class MasterTabInfoViewtabviewpanel2Model
 */
export default class MasterTabInfoViewtabviewpanel2Model {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof MasterTabInfoViewtabviewpanel2Model
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'legalname',
      },
      {
        name: 'organizationtype',
      },
      {
        name: 'createman',
      },
      {
        name: 'updateman',
      },
      {
        name: 'legal',
        prop: 'legalid',
      },
      {
        name: 'createdate',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'orgcode',
      },
      {
        name: 'orglevel',
      },
      {
        name: 'showorder',
      },
      {
        name: 'shortname',
      },
    ]
  }


}
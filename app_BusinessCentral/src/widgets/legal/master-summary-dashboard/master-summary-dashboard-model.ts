/**
 * MasterSummary 部件模型
 *
 * @export
 * @class MasterSummaryModel
 */
export default class MasterSummaryModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof MasterSummaryModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'legalname',
      },
      {
        name: 'organizationtype',
      },
      {
        name: 'createman',
      },
      {
        name: 'updateman',
      },
      {
        name: 'legal',
        prop: 'legalid',
      },
      {
        name: 'createdate',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'orgcode',
      },
      {
        name: 'orglevel',
      },
      {
        name: 'showorder',
      },
      {
        name: 'shortname',
      },
    ]
  }


}
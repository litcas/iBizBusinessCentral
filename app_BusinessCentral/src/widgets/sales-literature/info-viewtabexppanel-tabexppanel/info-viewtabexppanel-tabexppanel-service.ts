import { Http,Util,Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import SalesLiteratureService from '@/service/sales-literature/sales-literature-service';
import InfoViewtabexppanelModel from './info-viewtabexppanel-tabexppanel-model';


/**
 * InfoViewtabexppanel 部件服务对象
 *
 * @export
 * @class InfoViewtabexppanelService
 */
export default class InfoViewtabexppanelService extends ControlService {

    /**
     * 销售宣传资料服务对象
     *
     * @type {SalesLiteratureService}
     * @memberof InfoViewtabexppanelService
     */
    public appEntityService: SalesLiteratureService = new SalesLiteratureService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof InfoViewtabexppanelService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of InfoViewtabexppanelService.
     * 
     * @param {*} [opts={}]
     * @memberof InfoViewtabexppanelService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new InfoViewtabexppanelModel();
    }

    
}
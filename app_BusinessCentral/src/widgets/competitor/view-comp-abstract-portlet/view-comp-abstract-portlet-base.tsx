import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, MainControlBase } from '@/studio-core';
import CompetitorService from '@/service/competitor/competitor-service';
import View_CompAbstractService from './view-comp-abstract-portlet-service';
import CompetitorUIService from '@/uiservice/competitor/competitor-ui-service';
import { Environment } from '@/environments/environment';


/**
 * dashboard_sysportlet1部件基类
 *
 * @export
 * @class MainControlBase
 * @extends {View_CompAbstractPortletBase}
 */
export class View_CompAbstractPortletBase extends MainControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof View_CompAbstractPortletBase
     */
    protected controlType: string = 'PORTLET';

    /**
     * 建构部件服务对象
     *
     * @type {View_CompAbstractService}
     * @memberof View_CompAbstractPortletBase
     */
    public service: View_CompAbstractService = new View_CompAbstractService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {CompetitorService}
     * @memberof View_CompAbstractPortletBase
     */
    public appEntityService: CompetitorService = new CompetitorService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof View_CompAbstractPortletBase
     */
    protected appDeName: string = 'competitor';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof View_CompAbstractPortletBase
     */
    protected appDeLogicName: string = '竞争对手';

    /**
     * 界面UI服务对象
     *
     * @type {CompetitorUIService}
     * @memberof View_CompAbstractBase
     */  
    public appUIService:CompetitorUIService = new CompetitorUIService(this.$store);

    /**
     * 长度
     *
     * @type {number}
     * @memberof View_CompAbstract
     */
    @Prop() public height?: number;

    /**
     * 宽度
     *
     * @type {number}
     * @memberof View_CompAbstract
     */
    @Prop() public width?: number;



    /**
     * 是否自适应大小
     *
     * @returns {boolean}
     * @memberof View_CompAbstractBase
     */
    @Prop({default: false})public isAdaptiveSize!: boolean;

    /**
     * 获取多项数据
     *
     * @returns {any[]}
     * @memberof View_CompAbstractBase
     */
    public getDatas(): any[] {
        return [];
    }

    /**
     * 获取单项树
     *
     * @returns {*}
     * @memberof View_CompAbstractBase
     */
    public getData(): any {
        return {};
    }

    /**
     * 获取高度
     *
     * @returns {any[]}
     * @memberof View_CompAbstractBase
     */
    get getHeight(): any{
        if(!this.$util.isEmpty(this.height) && !this.$util.isNumberNaN(this.height)){
            if(this.height == 0){
                return 'auto';
            } else {
                return this.height+'px';
            }
        } else {
            return 'auto';
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof View_CompAbstractBase
     */
    public created() {
        this.afterCreated();
    }

    /**
     * 执行created后的逻辑
     *
     *  @memberof View_CompAbstractBase
     */    
    public afterCreated(){
        if (this.viewState) {
            this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }) => {
                if (!Object.is(tag, this.name)) {
                    return;
                }
                const refs: any = this.$refs;
                Object.keys(refs).forEach((_name: string) => {
                    this.viewState.next({ tag: _name, action: action, data: data });
                });
            });
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof View_CompAbstractBase
     */
    public destroyed() {
        this.afterDestroy();
    }

    /**
     * 执行destroyed后的逻辑
     *
     * @memberof View_CompAbstractBase
     */
    public afterDestroy() {
        if (this.viewStateEvent) {
            this.viewStateEvent.unsubscribe();
        }
    }


}

import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * View_CompAbstract 部件服务对象
 *
 * @export
 * @class View_CompAbstractService
 */
export default class View_CompAbstractService extends ControlService {
}

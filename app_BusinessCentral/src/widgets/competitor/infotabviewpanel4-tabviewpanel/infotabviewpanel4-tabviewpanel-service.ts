import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * Infotabviewpanel4 部件服务对象
 *
 * @export
 * @class Infotabviewpanel4Service
 */
export default class Infotabviewpanel4Service extends ControlService {
}
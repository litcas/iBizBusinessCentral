import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * IncidentInfo 部件服务对象
 *
 * @export
 * @class IncidentInfoService
 */
export default class IncidentInfoService extends ControlService {
}
import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, MainControlBase } from '@/studio-core';
import IncidentService from '@/service/incident/incident-service';
import View_IncidentInfoService from './view-incident-info-portlet-service';
import IncidentUIService from '@/uiservice/incident/incident-ui-service';
import { Environment } from '@/environments/environment';


/**
 * dashboard_sysportlet1部件基类
 *
 * @export
 * @class MainControlBase
 * @extends {View_IncidentInfoPortletBase}
 */
export class View_IncidentInfoPortletBase extends MainControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof View_IncidentInfoPortletBase
     */
    protected controlType: string = 'PORTLET';

    /**
     * 建构部件服务对象
     *
     * @type {View_IncidentInfoService}
     * @memberof View_IncidentInfoPortletBase
     */
    public service: View_IncidentInfoService = new View_IncidentInfoService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {IncidentService}
     * @memberof View_IncidentInfoPortletBase
     */
    public appEntityService: IncidentService = new IncidentService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof View_IncidentInfoPortletBase
     */
    protected appDeName: string = 'incident';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof View_IncidentInfoPortletBase
     */
    protected appDeLogicName: string = '案例';

    /**
     * 界面UI服务对象
     *
     * @type {IncidentUIService}
     * @memberof View_IncidentInfoBase
     */  
    public appUIService:IncidentUIService = new IncidentUIService(this.$store);

    /**
     * 长度
     *
     * @type {number}
     * @memberof View_IncidentInfo
     */
    @Prop() public height?: number;

    /**
     * 宽度
     *
     * @type {number}
     * @memberof View_IncidentInfo
     */
    @Prop() public width?: number;



    /**
     * 是否自适应大小
     *
     * @returns {boolean}
     * @memberof View_IncidentInfoBase
     */
    @Prop({default: false})public isAdaptiveSize!: boolean;

    /**
     * 获取多项数据
     *
     * @returns {any[]}
     * @memberof View_IncidentInfoBase
     */
    public getDatas(): any[] {
        return [];
    }

    /**
     * 获取单项树
     *
     * @returns {*}
     * @memberof View_IncidentInfoBase
     */
    public getData(): any {
        return {};
    }

    /**
     * 获取高度
     *
     * @returns {any[]}
     * @memberof View_IncidentInfoBase
     */
    get getHeight(): any{
        if(!this.$util.isEmpty(this.height) && !this.$util.isNumberNaN(this.height)){
            if(this.height == 0){
                return 'auto';
            } else {
                return this.height+'px';
            }
        } else {
            return 'auto';
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof View_IncidentInfoBase
     */
    public created() {
        this.afterCreated();
    }

    /**
     * 执行created后的逻辑
     *
     *  @memberof View_IncidentInfoBase
     */    
    public afterCreated(){
        if (this.viewState) {
            this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }) => {
                if (!Object.is(tag, this.name)) {
                    return;
                }
                const refs: any = this.$refs;
                Object.keys(refs).forEach((_name: string) => {
                    this.viewState.next({ tag: _name, action: action, data: data });
                });
            });
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof View_IncidentInfoBase
     */
    public destroyed() {
        this.afterDestroy();
    }

    /**
     * 执行destroyed后的逻辑
     *
     * @memberof View_IncidentInfoBase
     */
    public afterDestroy() {
        if (this.viewStateEvent) {
            this.viewStateEvent.unsubscribe();
        }
    }


}

import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * View_IncidentInfo 部件服务对象
 *
 * @export
 * @class View_IncidentInfoService
 */
export default class View_IncidentInfoService extends ControlService {
}

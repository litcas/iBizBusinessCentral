/**
 * Edit_Main 部件模型
 *
 * @export
 * @class Edit_MainModel
 */
export default class Edit_MainModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Edit_MainModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'incidentid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'title',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'title',
        prop: 'title',
        dataType: 'TEXT',
      },
      {
        name: 'subjectname',
        prop: 'subjectname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'caseorigincode',
        prop: 'caseorigincode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'productname',
        prop: 'productname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'entitlementname',
        prop: 'entitlementname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'primarycontactname',
        prop: 'primarycontactname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'casetypecode',
        prop: 'casetypecode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'parentcasename',
        prop: 'parentcasename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'escalated',
        prop: 'escalated',
        dataType: 'YESNO',
      },
      {
        name: 'followupby',
        prop: 'followupby',
        dataType: 'DATETIME',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'productid',
        prop: 'productid',
        dataType: 'PICKUP',
      },
      {
        name: 'incidentid',
        prop: 'incidentid',
        dataType: 'GUID',
      },
      {
        name: 'primarycontactid',
        prop: 'primarycontactid',
        dataType: 'PICKUP',
      },
      {
        name: 'parentcaseid',
        prop: 'parentcaseid',
        dataType: 'PICKUP',
      },
      {
        name: 'subjectid',
        prop: 'subjectid',
        dataType: 'PICKUP',
      },
      {
        name: 'entitlementid',
        prop: 'entitlementid',
        dataType: 'PICKUP',
      },
      {
        name: 'incident',
        prop: 'incidentid',
        dataType: 'FONTKEY',
      },
    ]
  }

}
import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, EditFormControlBase } from '@/studio-core';
import IncidentService from '@/service/incident/incident-service';
import ByParentKeyService from './by-parent-key-form-service';
import IncidentUIService from '@/uiservice/incident/incident-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {ByParentKeyEditFormBase}
 */
export class ByParentKeyEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof ByParentKeyEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {ByParentKeyService}
     * @memberof ByParentKeyEditFormBase
     */
    public service: ByParentKeyService = new ByParentKeyService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {IncidentService}
     * @memberof ByParentKeyEditFormBase
     */
    public appEntityService: IncidentService = new IncidentService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof ByParentKeyEditFormBase
     */
    protected appDeName: string = 'incident';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof ByParentKeyEditFormBase
     */
    protected appDeLogicName: string = '案例';

    /**
     * 界面UI服务对象
     *
     * @type {IncidentUIService}
     * @memberof ByParentKeyBase
     */  
    public appUIService:IncidentUIService = new IncidentUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof ByParentKeyEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        title: null,
        subjectname: null,
        caseorigincode: null,
        productname: null,
        entitlementname: null,
        productid: null,
        incidentid: null,
        subjectid: null,
        entitlementid: null,
        incident:null,
    };

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof ByParentKeyEditFormBase
     */
    public rules: any = {
        title: [
            { required: true, type: 'string', message: '案例标题 值不能为空', trigger: 'change' },
            { required: true, type: 'string', message: '案例标题 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof ByParentKeyBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof ByParentKeyEditFormBase
     */
    public detailsModel: any = {
        group1: new FormGroupPanelModel({ caption: '案例基本信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.incident.byparentkey_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: '案例', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfmajortext: new FormItemModel({ caption: '案例标题', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        title: new FormItemModel({ caption: '案例标题', detailType: 'FORMITEM', name: 'title', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        subjectname: new FormItemModel({ caption: '主题', detailType: 'FORMITEM', name: 'subjectname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        caseorigincode: new FormItemModel({ caption: '起源', detailType: 'FORMITEM', name: 'caseorigincode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        productname: new FormItemModel({ caption: '产品', detailType: 'FORMITEM', name: 'productname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        entitlementname: new FormItemModel({ caption: '权利', detailType: 'FORMITEM', name: 'entitlementname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        productid: new FormItemModel({ caption: '产品', detailType: 'FORMITEM', name: 'productid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        incidentid: new FormItemModel({ caption: '案例', detailType: 'FORMITEM', name: 'incidentid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        subjectid: new FormItemModel({ caption: '主题', detailType: 'FORMITEM', name: 'subjectid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        entitlementid: new FormItemModel({ caption: '权利', detailType: 'FORMITEM', name: 'entitlementid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

    };
}
/**
 * DatePanel 部件模型
 *
 * @export
 * @class DatePanelModel
 */
export default class DatePanelModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof DatePanelModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'incidentid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'title',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'prioritycode',
        prop: 'prioritycode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'escalatedon',
        prop: 'escalatedon',
        dataType: 'DATETIME',
      },
      {
        name: 'statecode',
        prop: 'statecode',
        dataType: 'NSCODELIST',
      },
      {
        name: 'ownername',
        prop: 'ownername',
        dataType: 'TEXT',
      },
      {
        name: 'incidentid',
        prop: 'incidentid',
        dataType: 'GUID',
      },
      {
        name: 'incident',
        prop: 'incidentid',
        dataType: 'FONTKEY',
      },
    ]
  }

}
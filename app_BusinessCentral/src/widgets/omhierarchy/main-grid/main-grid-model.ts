/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'pomhierarchyid',
          prop: 'pomhierarchyid',
          dataType: 'PICKUP',
        },
        {
          name: 'organizationname',
          prop: 'organizationname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'organizationid',
          prop: 'organizationid',
          dataType: 'PICKUP',
        },
        {
          name: 'omhierarchycatname',
          prop: 'omhierarchycatname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'pomhierarchyname',
          prop: 'pomhierarchyname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'shortname',
          prop: 'shortname',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'srfmajortext',
          prop: 'omhierarchyname',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'omhierarchyid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'omhierarchyid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'omhierarchycatid',
          prop: 'omhierarchycatid',
          dataType: 'PICKUP',
        },
        {
          name: 'omhierarchy',
          prop: 'omhierarchyid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}
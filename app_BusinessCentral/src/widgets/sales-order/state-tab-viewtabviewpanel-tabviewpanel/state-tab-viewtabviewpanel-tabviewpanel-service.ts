import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * StateTabViewtabviewpanel 部件服务对象
 *
 * @export
 * @class StateTabViewtabviewpanelService
 */
export default class StateTabViewtabviewpanelService extends ControlService {
}
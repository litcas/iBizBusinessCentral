import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * ConAbs 部件服务对象
 *
 * @export
 * @class ConAbsService
 */
export default class ConAbsService extends ControlService {
}
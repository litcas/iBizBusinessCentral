import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * Infotabviewpanel2 部件服务对象
 *
 * @export
 * @class Infotabviewpanel2Service
 */
export default class Infotabviewpanel2Service extends ControlService {
}
import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, EditFormControlBase } from '@/studio-core';
import ContactService from '@/service/contact/contact-service';
import MarketEditService from './market-edit-form-service';
import ContactUIService from '@/uiservice/contact/contact-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {MarketEditEditFormBase}
 */
export class MarketEditEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof MarketEditEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {MarketEditService}
     * @memberof MarketEditEditFormBase
     */
    public service: MarketEditService = new MarketEditService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {ContactService}
     * @memberof MarketEditEditFormBase
     */
    public appEntityService: ContactService = new ContactService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MarketEditEditFormBase
     */
    protected appDeName: string = 'contact';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof MarketEditEditFormBase
     */
    protected appDeLogicName: string = '联系人';

    /**
     * 界面UI服务对象
     *
     * @type {ContactUIService}
     * @memberof MarketEditBase
     */  
    public appUIService:ContactUIService = new ContactUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof MarketEditEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        originatingleadname: null,
        lastusedincampaign: null,
        donotsendmm: null,
        shippingmethodcode: null,
        originatingleadid: null,
        contactid: null,
        contact:null,
    };

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof MarketEditEditFormBase
     */
    public rules: any = {
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof MarketEditBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof MarketEditEditFormBase
     */
    public detailsModel: any = {
        group1: new FormGroupPanelModel({ caption: '联系人基本信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.contact.marketedit_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: '联系人', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfmajortext: new FormItemModel({ caption: '全名', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        originatingleadname: new FormItemModel({ caption: '原始潜在顾客', detailType: 'FORMITEM', name: 'originatingleadname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        lastusedincampaign: new FormItemModel({ caption: '上次参与市场活动的日期', detailType: 'FORMITEM', name: 'lastusedincampaign', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        donotsendmm: new FormItemModel({ caption: '发送市场营销资料', detailType: 'FORMITEM', name: 'donotsendmm', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        shippingmethodcode: new FormItemModel({ caption: '送货方式', detailType: 'FORMITEM', name: 'shippingmethodcode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        originatingleadid: new FormItemModel({ caption: '原始潜在顾客', detailType: 'FORMITEM', name: 'originatingleadid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        contactid: new FormItemModel({ caption: '联系人', detailType: 'FORMITEM', name: 'contactid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

    };
}
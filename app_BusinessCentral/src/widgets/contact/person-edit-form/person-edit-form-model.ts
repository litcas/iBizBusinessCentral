/**
 * PersonEdit 部件模型
 *
 * @export
 * @class PersonEditModel
 */
export default class PersonEditModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof PersonEditModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'contactid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'fullname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'gendercode',
        prop: 'gendercode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'familystatuscode',
        prop: 'familystatuscode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'spousesname',
        prop: 'spousesname',
        dataType: 'TEXT',
      },
      {
        name: 'birthdate',
        prop: 'birthdate',
        dataType: 'DATETIME',
      },
      {
        name: 'anniversary',
        prop: 'anniversary',
        dataType: 'DATETIME',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'contactid',
        prop: 'contactid',
        dataType: 'GUID',
      },
      {
        name: 'contact',
        prop: 'contactid',
        dataType: 'FONTKEY',
      },
    ]
  }

}
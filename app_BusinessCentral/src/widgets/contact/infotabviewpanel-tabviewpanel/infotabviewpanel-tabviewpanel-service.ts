import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * Infotabviewpanel 部件服务对象
 *
 * @export
 * @class InfotabviewpanelService
 */
export default class InfotabviewpanelService extends ControlService {
}
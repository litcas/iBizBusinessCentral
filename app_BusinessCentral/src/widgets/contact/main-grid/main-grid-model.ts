/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'telephone1',
          prop: 'telephone1',
          dataType: 'TEXT',
        },
        {
          name: 'parentcustomerid',
          prop: 'parentcustomerid',
          dataType: 'TEXT',
        },
        {
          name: 'transactioncurrencyid',
          prop: 'transactioncurrencyid',
          dataType: 'PICKUP',
        },
        {
          name: 'slaid',
          prop: 'slaid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'fullname',
          dataType: 'TEXT',
        },
        {
          name: 'srfkey',
          prop: 'contactid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'contactid',
          dataType: 'GUID',
        },
        {
          name: 'preferredequipmentid',
          prop: 'preferredequipmentid',
          dataType: 'PICKUP',
        },
        {
          name: 'preferredserviceid',
          prop: 'preferredserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'defaultpricelevelid',
          prop: 'defaultpricelevelid',
          dataType: 'PICKUP',
        },
        {
          name: 'customerid',
          prop: 'customerid',
          dataType: 'PICKUP',
        },
        {
          name: 'emailaddress1',
          prop: 'emailaddress1',
          dataType: 'TEXT',
        },
        {
          name: 'originatingleadid',
          prop: 'originatingleadid',
          dataType: 'PICKUP',
        },
        {
          name: 'fullname',
          prop: 'fullname',
          dataType: 'TEXT',
        },
        {
          name: 'contact',
          prop: 'contactid',
        },
      {
        name: 'n_fullname_like',
        prop: 'n_fullname_like',
        dataType: 'TEXT',
      },
      {
        name: 'n_statecode_eq',
        prop: 'n_statecode_eq',
        dataType: 'NSCODELIST',
      },
      {
        name: 'n_leadsourcecode_eq',
        prop: 'n_leadsourcecode_eq',
        dataType: 'SSCODELIST',
      },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}
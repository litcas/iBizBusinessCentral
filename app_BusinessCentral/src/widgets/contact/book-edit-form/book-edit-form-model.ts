/**
 * BookEdit 部件模型
 *
 * @export
 * @class BookEditModel
 */
export default class BookEditModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof BookEditModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'contactid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'fullname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'transactioncurrencyname',
        prop: 'currencyname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'creditlimit',
        prop: 'creditlimit',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'creditonhold',
        prop: 'creditonhold',
        dataType: 'YESNO',
      },
      {
        name: 'paymenttermscode',
        prop: 'paymenttermscode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'transactioncurrencyid',
        prop: 'transactioncurrencyid',
        dataType: 'PICKUP',
      },
      {
        name: 'contactid',
        prop: 'contactid',
        dataType: 'GUID',
      },
      {
        name: 'contact',
        prop: 'contactid',
        dataType: 'FONTKEY',
      },
    ]
  }

}
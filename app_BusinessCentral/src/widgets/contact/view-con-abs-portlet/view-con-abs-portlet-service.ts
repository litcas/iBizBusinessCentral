import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * View_ConAbs 部件服务对象
 *
 * @export
 * @class View_ConAbsService
 */
export default class View_ConAbsService extends ControlService {
}

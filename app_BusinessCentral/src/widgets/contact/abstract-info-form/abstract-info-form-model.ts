/**
 * AbstractInfo 部件模型
 *
 * @export
 * @class AbstractInfoModel
 */
export default class AbstractInfoModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof AbstractInfoModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'contactid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'fullname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'fullname',
        prop: 'fullname',
        dataType: 'TEXT',
      },
      {
        name: 'jobtitle',
        prop: 'jobtitle',
        dataType: 'TEXT',
      },
      {
        name: 'customername',
        prop: 'customername',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'emailaddress1',
        prop: 'emailaddress1',
        dataType: 'TEXT',
      },
      {
        name: 'telephone1',
        prop: 'telephone1',
        dataType: 'TEXT',
      },
      {
        name: 'mobilephone',
        prop: 'mobilephone',
        dataType: 'TEXT',
      },
      {
        name: 'fax',
        prop: 'fax',
        dataType: 'TEXT',
      },
      {
        name: 'address1_country',
        prop: 'address1_country',
        dataType: 'TEXT',
      },
      {
        name: 'address1_stateorprovince',
        prop: 'address1_stateorprovince',
        dataType: 'TEXT',
      },
      {
        name: 'address1_city',
        prop: 'address1_city',
        dataType: 'TEXT',
      },
      {
        name: 'address1_line1',
        prop: 'address1_line1',
        dataType: 'TEXT',
      },
      {
        name: 'address1_postalcode',
        prop: 'address1_postalcode',
        dataType: 'TEXT',
      },
      {
        name: 'gendercode',
        prop: 'gendercode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'familystatuscode',
        prop: 'familystatuscode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'spousesname',
        prop: 'spousesname',
        dataType: 'TEXT',
      },
      {
        name: 'birthdate',
        prop: 'birthdate',
        dataType: 'DATETIME',
      },
      {
        name: 'anniversary',
        prop: 'anniversary',
        dataType: 'DATETIME',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'originatingleadname',
        prop: 'originatingleadname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'lastusedincampaign',
        prop: 'lastusedincampaign',
        dataType: 'DATETIME',
      },
      {
        name: 'donotsendmm',
        prop: 'donotsendmm',
        dataType: 'YESNO',
      },
      {
        name: 'shippingmethodcode',
        prop: 'shippingmethodcode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'transactioncurrencyname',
        prop: 'currencyname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'creditlimit',
        prop: 'creditlimit',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'creditonhold',
        prop: 'creditonhold',
        dataType: 'YESNO',
      },
      {
        name: 'paymenttermscode',
        prop: 'paymenttermscode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'customerid',
        prop: 'customerid',
        dataType: 'PICKUP',
      },
      {
        name: 'originatingleadid',
        prop: 'originatingleadid',
        dataType: 'PICKUP',
      },
      {
        name: 'contactid',
        prop: 'contactid',
        dataType: 'GUID',
      },
      {
        name: 'contact',
        prop: 'contactid',
        dataType: 'FONTKEY',
      },
    ]
  }

}
import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, GridControlBase } from '@/studio-core';
import SysAuthLogService from '@/service/sys-auth-log/sys-auth-log-service';
import MainService from './main-grid-service';
import SysAuthLogUIService from '@/uiservice/sys-auth-log/sys-auth-log-ui-service';
import { FormItemModel } from '@/model/form-detail';


/**
 * grid部件基类
 *
 * @export
 * @class GridControlBase
 * @extends {MainGridBase}
 */
export class MainGridBase extends GridControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected controlType: string = 'GRID';

    /**
     * 建构部件服务对象
     *
     * @type {MainService}
     * @memberof MainGridBase
     */
    public service: MainService = new MainService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {SysAuthLogService}
     * @memberof MainGridBase
     */
    public appEntityService: SysAuthLogService = new SysAuthLogService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeName: string = 'sysauthlog';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeLogicName: string = '认证日志';

    /**
     * 界面UI服务对象
     *
     * @type {SysAuthLogUIService}
     * @memberof MainBase
     */  
    public appUIService:SysAuthLogUIService = new SysAuthLogUIService(this.$store);

    /**
     * 界面行为模型
     *
     * @type {*}
     * @memberof MainBase
     */  
    public ActionModel: any = {
    };

    /**
     * 本地缓存标识
     *
     * @protected
     * @type {string}
     * @memberof MainBase
     */
    protected localStorageTag: string = 'sys_authlog_main_grid';

    /**
     * 排序方向
     *
     * @type {string}
     * @memberof MainGridBase
     */
    public minorSortDir: string = 'DESC';

    /**
     * 排序字段
     *
     * @type {string}
     * @memberof MainGridBase
     */
    public minorSortPSDEF: string = 'authtime';

    /**
     * 所有列成员
     *
     * @type {any[]}
     * @memberof MainGridBase
     */
    public allColumns: any[] = [
        {
            name: 'username',
            label: '用户全局名',
            langtag: 'entities.sysauthlog.main_grid.columns.username',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'personname',
            label: '用户名称',
            langtag: 'entities.sysauthlog.main_grid.columns.personname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'authtime',
            label: '认证时间',
            langtag: 'entities.sysauthlog.main_grid.columns.authtime',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'authcode',
            label: '认证结果',
            langtag: 'entities.sysauthlog.main_grid.columns.authcode',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'ipaddr',
            label: 'IP地址',
            langtag: 'entities.sysauthlog.main_grid.columns.ipaddr',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'macaddr',
            label: 'MAC地址',
            langtag: 'entities.sysauthlog.main_grid.columns.macaddr',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'useragent',
            label: '客户端',
            langtag: 'entities.sysauthlog.main_grid.columns.useragent',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'domain',
            label: '域',
            langtag: 'entities.sysauthlog.main_grid.columns.domain',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
    ]

    /**
     * 获取表格行模型
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public getGridRowModel(){
        return {
          srfkey: new FormItemModel(),
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public rules: any = {
        srfkey: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '标识 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '标识 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 获取对应列class
     *
     * @type {*}
     * @memberof MainBase
     */
    public hasRowEdit: any = {
        'username':false,
        'personname':false,
        'authtime':false,
        'authcode':false,
        'ipaddr':false,
        'macaddr':false,
        'useragent':false,
        'domain':false,
    };

    /**
     * 获取对应列class
     *
     * @param {*} $args row 行数据，column 列数据，rowIndex 行索引，列索引
     * @returns {void}
     * @memberof MainBase
     */
    public getCellClassName(args: {row: any, column: any, rowIndex: number, columnIndex: number}): any {
        return ( this.hasRowEdit[args.column.property] && this.actualIsOpenEdit ) ? "edit-cell" : "info-cell";
    }


    /**
     * 导出数据格式化
     *
     * @param {*} filterVal
     * @param {*} jsonData
     * @param {any[]} [codelistColumns=[]]
     * @returns {Promise<any>}
     * @memberof MainGridBase
     */
    public async formatExcelData(filterVal: any, jsonData: any, codelistColumns?: any[]): Promise<any> {
        return super.formatExcelData(filterVal, jsonData, [
            {
                name: 'authcode',
                srfkey: 'CLAuthCode',
                codelistType : 'STATIC',
                renderMode: 'other',
                textSeparator: '、',
                valueSeparator: ',',
            },
        ]);
    }

}
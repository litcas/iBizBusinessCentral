import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, GridControlBase } from '@/studio-core';
import SysAppService from '@/service/sys-app/sys-app-service';
import MainService from './main-grid-service';
import SysAppUIService from '@/uiservice/sys-app/sys-app-ui-service';
import { FormItemModel } from '@/model/form-detail';


/**
 * grid部件基类
 *
 * @export
 * @class GridControlBase
 * @extends {MainGridBase}
 */
export class MainGridBase extends GridControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected controlType: string = 'GRID';

    /**
     * 建构部件服务对象
     *
     * @type {MainService}
     * @memberof MainGridBase
     */
    public service: MainService = new MainService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {SysAppService}
     * @memberof MainGridBase
     */
    public appEntityService: SysAppService = new SysAppService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeName: string = 'sysapp';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeLogicName: string = '应用';

    /**
     * 界面UI服务对象
     *
     * @type {SysAppUIService}
     * @memberof MainBase
     */  
    public appUIService:SysAppUIService = new SysAppUIService(this.$store);

    /**
     * 界面行为模型
     *
     * @type {*}
     * @memberof MainBase
     */  
    public ActionModel: any = {
    };

    /**
     * 本地缓存标识
     *
     * @protected
     * @type {string}
     * @memberof MainBase
     */
    protected localStorageTag: string = 'sys_app_main_grid';

    /**
     * 是否支持分页
     *
     * @type {boolean}
     * @memberof MainGridBase
     */
    public isEnablePagingBar: boolean = false;

    /**
     * 分页条数
     *
     * @type {number}
     * @memberof MainGridBase
     */
    public limit: number = 1000;

    /**
     * 所有列成员
     *
     * @type {any[]}
     * @memberof MainGridBase
     */
    public allColumns: any[] = [
        {
            name: 'pssystemid',
            label: '系统标识',
            langtag: 'entities.sysapp.main_grid.columns.pssystemid',
            show: true,
            unit: 'PX',
            isEnableRowEdit: true,
        },
        {
            name: 'appid',
            label: '应用标识',
            langtag: 'entities.sysapp.main_grid.columns.appid',
            show: true,
            unit: 'PX',
            isEnableRowEdit: true,
        },
        {
            name: 'appname',
            label: '应用名',
            langtag: 'entities.sysapp.main_grid.columns.appname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: true,
        },
        {
            name: 'appgroup',
            label: '分组',
            langtag: 'entities.sysapp.main_grid.columns.appgroup',
            show: true,
            unit: 'PX',
            isEnableRowEdit: true,
        },
        {
            name: 'apptype',
            label: '类型',
            langtag: 'entities.sysapp.main_grid.columns.apptype',
            show: true,
            unit: 'PX',
            isEnableRowEdit: true,
        },
        {
            name: 'fullname',
            label: '全称',
            langtag: 'entities.sysapp.main_grid.columns.fullname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: true,
        },
        {
            name: 'icon',
            label: '图标',
            langtag: 'entities.sysapp.main_grid.columns.icon',
            show: true,
            unit: 'PX',
            isEnableRowEdit: true,
        },
        {
            name: 'addr',
            label: '地址',
            langtag: 'entities.sysapp.main_grid.columns.addr',
            show: true,
            unit: 'PX',
            isEnableRowEdit: true,
        },
        {
            name: 'visabled',
            label: '可见',
            langtag: 'entities.sysapp.main_grid.columns.visabled',
            show: true,
            unit: 'PX',
            isEnableRowEdit: true,
        },
    ]

    /**
     * 获取表格行模型
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public getGridRowModel(){
        return {
          icon: new FormItemModel(),
          visabled: new FormItemModel(),
          appname: new FormItemModel(),
          appid: new FormItemModel(),
          pssystemid: new FormItemModel(),
          addr: new FormItemModel(),
          apptype: new FormItemModel(),
          srfkey: new FormItemModel(),
          fullname: new FormItemModel(),
          appgroup: new FormItemModel(),
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public rules: any = {
        icon: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '图标 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '图标 值不能为空', trigger: 'blur' },
        ],
        visabled: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '可见 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '可见 值不能为空', trigger: 'blur' },
        ],
        appname: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '应用名 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '应用名 值不能为空', trigger: 'blur' },
        ],
        appid: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '应用标识 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '应用标识 值不能为空', trigger: 'blur' },
        ],
        pssystemid: [
            { required: true, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '系统标识 值不能为空', trigger: 'change' },
            { required: true, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '系统标识 值不能为空', trigger: 'blur' },
        ],
        addr: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '地址 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '地址 值不能为空', trigger: 'blur' },
        ],
        apptype: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '类型 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '类型 值不能为空', trigger: 'blur' },
        ],
        srfkey: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '应用标识 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '应用标识 值不能为空', trigger: 'blur' },
        ],
        fullname: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '全称 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '全称 值不能为空', trigger: 'blur' },
        ],
        appgroup: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '分组 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '分组 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 获取对应列class
     *
     * @type {*}
     * @memberof MainBase
     */
    public hasRowEdit: any = {
        'pssystemid':true,
        'appid':true,
        'appname':true,
        'appgroup':true,
        'apptype':true,
        'fullname':true,
        'icon':true,
        'addr':true,
        'visabled':true,
    };

    /**
     * 获取对应列class
     *
     * @param {*} $args row 行数据，column 列数据，rowIndex 行索引，列索引
     * @returns {void}
     * @memberof MainBase
     */
    public getCellClassName(args: {row: any, column: any, rowIndex: number, columnIndex: number}): any {
        return ( this.hasRowEdit[args.column.property] && this.actualIsOpenEdit ) ? "edit-cell" : "info-cell";
    }


    /**
     * 导出数据格式化
     *
     * @param {*} filterVal
     * @param {*} jsonData
     * @param {any[]} [codelistColumns=[]]
     * @returns {Promise<any>}
     * @memberof MainGridBase
     */
    public async formatExcelData(filterVal: any, jsonData: any, codelistColumns?: any[]): Promise<any> {
        return super.formatExcelData(filterVal, jsonData, [
            {
                name: 'apptype',
                srfkey: 'AppType',
                codelistType : 'STATIC',
                renderMode: 'other',
                textSeparator: '、',
                valueSeparator: ',',
            },
            {
                name: 'visabled',
                srfkey: 'YesNo',
                codelistType : 'STATIC',
                renderMode: 'other',
                textSeparator: '、',
                valueSeparator: ',',
            },
        ]);
    }

}
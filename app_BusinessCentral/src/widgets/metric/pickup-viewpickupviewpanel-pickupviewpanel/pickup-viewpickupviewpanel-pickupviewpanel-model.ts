/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'createman',
      },
      {
        name: 'statuscode',
      },
      {
        name: 'amount',
      },
      {
        name: 'timezoneruleversionnumber',
      },
      {
        name: 'updateman',
      },
      {
        name: 'description',
      },
      {
        name: 'utcconversiontimezonecode',
      },
      {
        name: 'statecode',
      },
      {
        name: 'overriddencreatedon',
      },
      {
        name: 'metricname',
      },
      {
        name: 'importsequencenumber',
      },
      {
        name: 'metric',
        prop: 'metricid',
      },
      {
        name: 'amountdatatype',
      },
      {
        name: 'createdate',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'versionnumber',
      },
      {
        name: 'stretchtracked',
      },
    ]
  }


}
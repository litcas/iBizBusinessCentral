/**
 * StateTabViewtabviewpanel 部件模型
 *
 * @export
 * @class StateTabViewtabviewpanelModel
 */
export default class StateTabViewtabviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof StateTabViewtabviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'ownername',
      },
      {
        name: 'billto_line1',
      },
      {
        name: 'billto_telephone',
      },
      {
        name: 'lastonholdtime',
      },
      {
        name: 'billto_stateorprovince',
      },
      {
        name: 'duedate',
      },
      {
        name: 'pricingerrorcode',
      },
      {
        name: 'traversedpath',
      },
      {
        name: 'totaldiscountamount_base',
      },
      {
        name: 'totalamountlessfreight_base',
      },
      {
        name: 'lastbackofficesubmit',
      },
      {
        name: 'shipto_telephone',
      },
      {
        name: 'ownertype',
      },
      {
        name: 'billto_city',
      },
      {
        name: 'createman',
      },
      {
        name: 'shipto_composite',
      },
      {
        name: 'overriddencreatedon',
      },
      {
        name: 'billto_postalcode',
      },
      {
        name: 'freightamount_base',
      },
      {
        name: 'invoicename',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'shipto_city',
      },
      {
        name: 'totalamount',
      },
      {
        name: 'customerid',
      },
      {
        name: 'totalamount_base',
      },
      {
        name: 'billto_name',
      },
      {
        name: 'timezoneruleversionnumber',
      },
      {
        name: 'description',
      },
      {
        name: 'utcconversiontimezonecode',
      },
      {
        name: 'freightamount',
      },
      {
        name: 'prioritycode',
      },
      {
        name: 'discountamount_base',
      },
      {
        name: 'onholdtime',
      },
      {
        name: 'shipto_country',
      },
      {
        name: 'paymenttermscode',
      },
      {
        name: 'shipto_freighttermscode',
      },
      {
        name: 'updateman',
      },
      {
        name: 'shipto_line3',
      },
      {
        name: 'totaltax',
      },
      {
        name: 'billto_line2',
      },
      {
        name: 'createdate',
      },
      {
        name: 'exchangerate',
      },
      {
        name: 'shipto_line1',
      },
      {
        name: 'invoice',
        prop: 'invoiceid',
      },
      {
        name: 'customername',
      },
      {
        name: 'discountpercentage',
      },
      {
        name: 'totallineitemamount_base',
      },
      {
        name: 'stageid',
      },
      {
        name: 'entityimage_url',
      },
      {
        name: 'statecode',
      },
      {
        name: 'totalamountlessfreight',
      },
      {
        name: 'totaltax_base',
      },
      {
        name: 'shipto_line2',
      },
      {
        name: 'billto_country',
      },
      {
        name: 'entityimage',
      },
      {
        name: 'importsequencenumber',
      },
      {
        name: 'shipto_fax',
      },
      {
        name: 'ownerid',
      },
      {
        name: 'datedelivered',
      },
      {
        name: 'contactname',
      },
      {
        name: 'entityimage_timestamp',
      },
      {
        name: 'processid',
      },
      {
        name: 'totaldiscountamount',
      },
      {
        name: 'pricelocked',
      },
      {
        name: 'willcall',
      },
      {
        name: 'billto_line3',
      },
      {
        name: 'shipto_name',
      },
      {
        name: 'statuscode',
      },
      {
        name: 'totallineitemamount',
      },
      {
        name: 'discountamount',
      },
      {
        name: 'shipto_stateorprovince',
      },
      {
        name: 'entityimageid',
      },
      {
        name: 'versionnumber',
      },
      {
        name: 'shipto_postalcode',
      },
      {
        name: 'invoicenumber',
      },
      {
        name: 'billto_composite',
      },
      {
        name: 'emailaddress',
      },
      {
        name: 'shippingmethodcode',
      },
      {
        name: 'customertype',
      },
      {
        name: 'totallineitemdiscountamount',
      },
      {
        name: 'accountname',
      },
      {
        name: 'billto_fax',
      },
      {
        name: 'opportunityname',
      },
      {
        name: 'currencyname',
      },
      {
        name: 'pricelevelname',
      },
      {
        name: 'slaname',
      },
      {
        name: 'salesordername',
      },
      {
        name: 'transactioncurrencyid',
      },
      {
        name: 'pricelevelid',
      },
      {
        name: 'salesorderid',
      },
      {
        name: 'opportunityid',
      },
      {
        name: 'slaid',
      },
    ]
  }


}
/**
 * Info 部件模型
 *
 * @export
 * @class InfoModel
 */
export default class InfoModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof InfoModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'invoiceid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'invoicename',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'invoicenumber',
        prop: 'invoicenumber',
        dataType: 'TEXT',
      },
      {
        name: 'invoicename',
        prop: 'invoicename',
        dataType: 'TEXT',
      },
      {
        name: 'transactioncurrencyname',
        prop: 'currencyname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'pricelevelname',
        prop: 'pricelevelname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'datedelivered',
        prop: 'datedelivered',
        dataType: 'DATETIME',
      },
      {
        name: 'duedate',
        prop: 'duedate',
        dataType: 'DATETIME',
      },
      {
        name: 'shippingmethodcode',
        prop: 'shippingmethodcode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'paymenttermscode',
        prop: 'paymenttermscode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'totallineitemamount',
        prop: 'totallineitemamount',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'discountpercentage',
        prop: 'discountpercentage',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'discountamount',
        prop: 'discountamount',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'totalamountlessfreight',
        prop: 'totalamountlessfreight',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'freightamount',
        prop: 'freightamount',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'totalamount',
        prop: 'totalamount',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'opportunityname',
        prop: 'opportunityname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'salesordername',
        prop: 'salesordername',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'customerid',
        prop: 'customerid',
        dataType: 'TEXT',
      },
      {
        name: 'billto_country',
        prop: 'billto_country',
        dataType: 'TEXT',
      },
      {
        name: 'billto_stateorprovince',
        prop: 'billto_stateorprovince',
        dataType: 'TEXT',
      },
      {
        name: 'billto_city',
        prop: 'billto_city',
        dataType: 'TEXT',
      },
      {
        name: 'billto_line1',
        prop: 'billto_line1',
        dataType: 'TEXT',
      },
      {
        name: 'salesorderid',
        prop: 'salesorderid',
        dataType: 'PICKUP',
      },
      {
        name: 'opportunityid',
        prop: 'opportunityid',
        dataType: 'PICKUP',
      },
      {
        name: 'invoiceid',
        prop: 'invoiceid',
        dataType: 'GUID',
      },
      {
        name: 'invoice',
        prop: 'invoiceid',
        dataType: 'FONTKEY',
      },
    ]
  }

}
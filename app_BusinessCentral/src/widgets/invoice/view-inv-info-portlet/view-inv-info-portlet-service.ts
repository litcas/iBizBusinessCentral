import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * View_InvInfo 部件服务对象
 *
 * @export
 * @class View_InvInfoService
 */
export default class View_InvInfoService extends ControlService {
}

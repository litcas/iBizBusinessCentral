import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * PART_V_001 部件服务对象
 *
 * @export
 * @class PART_V_001Service
 */
export default class PART_V_001Service extends ControlService {
}

import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * BOARD_001 部件服务对象
 *
 * @export
 * @class BOARD_001Service
 */
export default class BOARD_001Service extends ControlService {
}
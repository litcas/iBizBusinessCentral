/**
 * EF_001 部件模型
 *
 * @export
 * @class EF_001Model
 */
export default class EF_001Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof EF_001Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'omhierarchycatid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'omhierarchycatname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'omhierarchycatname',
        prop: 'omhierarchycatname',
        dataType: 'TEXT',
      },
      {
        name: 'memo',
        prop: 'memo',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'omhierarchycatid',
        prop: 'omhierarchycatid',
        dataType: 'GUID',
      },
      {
        name: 'omhierarchycat',
        prop: 'omhierarchycatid',
        dataType: 'FONTKEY',
      },
    ]
  }

}
/**
 * QuickCreate 部件模型
 *
 * @export
 * @class QuickCreateModel
 */
export default class QuickCreateModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof QuickCreateModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'activityid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'subject',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'subject',
        prop: 'subject',
        dataType: 'TEXT',
      },
      {
        name: 'category',
        prop: 'category',
        dataType: 'TEXT',
      },
      {
        name: 'channeltypecode',
        prop: 'channeltypecode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'scheduledstart',
        prop: 'scheduledstart',
        dataType: 'DATETIME',
      },
      {
        name: 'scheduledend',
        prop: 'scheduledend',
        dataType: 'DATETIME',
      },
      {
        name: 'actualstart',
        prop: 'actualstart',
        dataType: 'DATETIME',
      },
      {
        name: 'actualend',
        prop: 'actualend',
        dataType: 'DATETIME',
      },
      {
        name: 'budgetedcost',
        prop: 'budgetedcost',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'actualcost',
        prop: 'actualcost',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'regardingobjectid',
        prop: 'regardingobjectid',
        dataType: 'TEXT',
      },
      {
        name: 'activityid',
        prop: 'activityid',
        dataType: 'GUID',
      },
      {
        name: 'campaignactivity',
        prop: 'activityid',
        dataType: 'FONTKEY',
      },
    ]
  }

}
/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'businesstypecode',
          prop: 'businesstypecode',
          dataType: 'SSCODELIST',
        },
        {
          name: 'customertype',
          prop: 'customertype',
          dataType: 'SSCODELIST',
        },
        {
          name: 'srfmajortext',
          prop: 'customername',
          dataType: 'TEXT',
        },
        {
          name: 'srfdatatype',
          prop: 'customertype',
          dataType: 'SSCODELIST',
        },
        {
          name: 'srfdataaccaction',
          prop: 'customerid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'customerid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'customername',
          prop: 'customername',
          dataType: 'TEXT',
        },
        {
          name: 'incidentcustomer',
          prop: 'customerid',
        },
      {
        name: 'n_customertype_eq',
        prop: 'n_customertype_eq',
        dataType: 'SSCODELIST',
      },
      {
        name: 'n_customername_like',
        prop: 'n_customername_like',
        dataType: 'TEXT',
      },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}
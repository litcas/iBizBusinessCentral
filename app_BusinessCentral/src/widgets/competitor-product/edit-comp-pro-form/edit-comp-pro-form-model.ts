/**
 * Edit_CompPro 部件模型
 *
 * @export
 * @class Edit_CompProModel
 */
export default class Edit_CompProModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Edit_CompProModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'relationshipsid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'relationshipsname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'entityid',
        prop: 'entityid',
        dataType: 'PICKUP',
      },
      {
        name: 'entity2name',
        prop: 'entity2name',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'productnumber',
        prop: 'productnumber',
        dataType: 'PICKUPDATA',
      },
      {
        name: 'entity2id',
        prop: 'entity2id',
        dataType: 'PICKUP',
      },
      {
        name: 'relationshipsid',
        prop: 'relationshipsid',
        dataType: 'GUID',
      },
      {
        name: 'competitorproduct',
        prop: 'relationshipsid',
        dataType: 'FONTKEY',
      },
    ]
  }

}
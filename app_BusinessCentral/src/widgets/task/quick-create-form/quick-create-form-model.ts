/**
 * QuickCreate 部件模型
 *
 * @export
 * @class QuickCreateModel
 */
export default class QuickCreateModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof QuickCreateModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'activityid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'subject',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'ownerid',
        prop: 'ownerid',
        dataType: 'TEXT',
      },
      {
        name: 'ownertype',
        prop: 'ownertype',
        dataType: 'TEXT',
      },
      {
        name: 'ownername',
        prop: 'ownername',
        dataType: 'TEXT',
      },
      {
        name: 'subject',
        prop: 'subject',
        dataType: 'TEXT',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'scheduledend',
        prop: 'scheduledend',
        dataType: 'DATETIME',
      },
      {
        name: 'actualdurationminutes',
        prop: 'actualdurationminutes',
        dataType: 'INT',
      },
      {
        name: 'prioritycode',
        prop: 'prioritycode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'regardingobjectid',
        prop: 'regardingobjectid',
        dataType: 'TEXT',
      },
      {
        name: 'regardingobjectname',
        prop: 'regardingobjectname',
        dataType: 'TEXT',
      },
      {
        name: 'regardingobjecttypecode',
        prop: 'regardingobjecttypecode',
        dataType: 'TEXT',
      },
      {
        name: 'activityid',
        prop: 'activityid',
        dataType: 'GUID',
      },
      {
        name: 'task',
        prop: 'activityid',
        dataType: 'FONTKEY',
      },
    ]
  }

}
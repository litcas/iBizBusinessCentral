package cn.ibizlab.businesscentral.core.service.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.service.domain.KnowledgeArticleIncident;
import cn.ibizlab.businesscentral.core.service.filter.KnowledgeArticleIncidentSearchContext;
import cn.ibizlab.businesscentral.core.service.service.IKnowledgeArticleIncidentService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.service.mapper.KnowledgeArticleIncidentMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[知识文章事件] 服务对象接口实现
 */
@Slf4j
@Service("KnowledgeArticleIncidentServiceImpl")
public class KnowledgeArticleIncidentServiceImpl extends ServiceImpl<KnowledgeArticleIncidentMapper, KnowledgeArticleIncident> implements IKnowledgeArticleIncidentService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IIncidentService incidentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IKnowledgeArticleService knowledgearticleService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(KnowledgeArticleIncident et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getKnowledgearticleincidentid()),et);
        return true;
    }

    @Override
    public void createBatch(List<KnowledgeArticleIncident> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(KnowledgeArticleIncident et) {
        fillParentData(et);
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("knowledgearticleincidentid",et.getKnowledgearticleincidentid())))
            return false;
        CachedBeanCopier.copy(get(et.getKnowledgearticleincidentid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<KnowledgeArticleIncident> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public KnowledgeArticleIncident get(String key) {
        KnowledgeArticleIncident et = getById(key);
        if(et==null){
            et=new KnowledgeArticleIncident();
            et.setKnowledgearticleincidentid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public KnowledgeArticleIncident getDraft(KnowledgeArticleIncident et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(KnowledgeArticleIncident et) {
        return (!ObjectUtils.isEmpty(et.getKnowledgearticleincidentid()))&&(!Objects.isNull(this.getById(et.getKnowledgearticleincidentid())));
    }
    @Override
    @Transactional
    public boolean save(KnowledgeArticleIncident et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(KnowledgeArticleIncident et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<KnowledgeArticleIncident> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<KnowledgeArticleIncident> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<KnowledgeArticleIncident> selectByIncidentid(String incidentid) {
        return baseMapper.selectByIncidentid(incidentid);
    }

    @Override
    public void removeByIncidentid(String incidentid) {
        this.remove(new QueryWrapper<KnowledgeArticleIncident>().eq("incidentid",incidentid));
    }

	@Override
    public List<KnowledgeArticleIncident> selectByKnowledgearticleid(String knowledgearticleid) {
        return baseMapper.selectByKnowledgearticleid(knowledgearticleid);
    }

    @Override
    public void removeByKnowledgearticleid(String knowledgearticleid) {
        this.remove(new QueryWrapper<KnowledgeArticleIncident>().eq("knowledgearticleid",knowledgearticleid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<KnowledgeArticleIncident> searchDefault(KnowledgeArticleIncidentSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<KnowledgeArticleIncident> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<KnowledgeArticleIncident>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(KnowledgeArticleIncident et){
        //实体关系[DER1N_KNOWLEDGEARTICLEINCIDENT__INCIDENT__INCIDENTID]
        if(!ObjectUtils.isEmpty(et.getIncidentid())){
            cn.ibizlab.businesscentral.core.service.domain.Incident incident=et.getIncident();
            if(ObjectUtils.isEmpty(incident)){
                cn.ibizlab.businesscentral.core.service.domain.Incident majorEntity=incidentService.get(et.getIncidentid());
                et.setIncident(majorEntity);
                incident=majorEntity;
            }
            et.setIncidentname(incident.getTitle());
        }
        //实体关系[DER1N_KNOWLEDGEARTICLEINCIDENT__KNOWLEDGEARTICLE__KNOWLEDGEARTICLEID]
        if(!ObjectUtils.isEmpty(et.getKnowledgearticleid())){
            cn.ibizlab.businesscentral.core.base.domain.KnowledgeArticle knowledgearticle=et.getKnowledgearticle();
            if(ObjectUtils.isEmpty(knowledgearticle)){
                cn.ibizlab.businesscentral.core.base.domain.KnowledgeArticle majorEntity=knowledgearticleService.get(et.getKnowledgearticleid());
                et.setKnowledgearticle(majorEntity);
                knowledgearticle=majorEntity;
            }
            et.setKnowledgearticlename(knowledgearticle.getTitle());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<KnowledgeArticleIncident> getKnowledgearticleincidentByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<KnowledgeArticleIncident> getKnowledgearticleincidentByEntities(List<KnowledgeArticleIncident> entities) {
        List ids =new ArrayList();
        for(KnowledgeArticleIncident entity : entities){
            Serializable id=entity.getKnowledgearticleincidentid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




package cn.ibizlab.businesscentral.core.sales.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.sales.domain.SalesOrder;
import cn.ibizlab.businesscentral.core.sales.filter.SalesOrderSearchContext;
import cn.ibizlab.businesscentral.core.sales.service.ISalesOrderService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.sales.mapper.SalesOrderMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[订单] 服务对象接口实现
 */
@Slf4j
@Service("SalesOrderServiceImpl")
public class SalesOrderServiceImpl extends ServiceImpl<SalesOrderMapper, SalesOrder> implements ISalesOrderService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.finance.service.IInvoiceService invoiceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IOrderCloseService ordercloseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.ISalesOrderDetailService salesorderdetailService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.marketing.service.ICampaignService campaignService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IOpportunityService opportunityService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.product.service.IPriceLevelService pricelevelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IQuoteService quoteService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ISlaService slaService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.logic.ISalesOrderCancelLogic cancelLogic;

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.logic.ISalesOrderFinishLogic finishLogic;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(SalesOrder et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getSalesorderid()),et);
        return true;
    }

    @Override
    public void createBatch(List<SalesOrder> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(SalesOrder et) {
        fillParentData(et);
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("salesorderid",et.getSalesorderid())))
            return false;
        CachedBeanCopier.copy(get(et.getSalesorderid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<SalesOrder> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public SalesOrder get(String key) {
        SalesOrder et = getById(key);
        if(et==null){
            et=new SalesOrder();
            et.setSalesorderid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public SalesOrder getDraft(SalesOrder et) {
        fillParentData(et);
        return et;
    }

    @Override
    @Transactional
    public SalesOrder cancel(SalesOrder et) {
        cancelLogic.execute(et);
         return et ;
    }

    @Override
    public boolean checkKey(SalesOrder et) {
        return (!ObjectUtils.isEmpty(et.getSalesorderid()))&&(!Objects.isNull(this.getById(et.getSalesorderid())));
    }
    @Override
    @Transactional
    public SalesOrder finish(SalesOrder et) {
        finishLogic.execute(et);
         return et ;
    }

    @Override
    @Transactional
    public SalesOrder genInvoice(SalesOrder et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public boolean save(SalesOrder et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(SalesOrder et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<SalesOrder> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<SalesOrder> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<SalesOrder> selectByCampaignid(String campaignid) {
        return baseMapper.selectByCampaignid(campaignid);
    }

    @Override
    public void removeByCampaignid(String campaignid) {
        this.remove(new QueryWrapper<SalesOrder>().eq("campaignid",campaignid));
    }

	@Override
    public List<SalesOrder> selectByOpportunityid(String opportunityid) {
        return baseMapper.selectByOpportunityid(opportunityid);
    }

    @Override
    public void removeByOpportunityid(String opportunityid) {
        this.remove(new QueryWrapper<SalesOrder>().eq("opportunityid",opportunityid));
    }

	@Override
    public List<SalesOrder> selectByPricelevelid(String pricelevelid) {
        return baseMapper.selectByPricelevelid(pricelevelid);
    }

    @Override
    public void removeByPricelevelid(String pricelevelid) {
        this.remove(new QueryWrapper<SalesOrder>().eq("pricelevelid",pricelevelid));
    }

	@Override
    public List<SalesOrder> selectByQuoteid(String quoteid) {
        return baseMapper.selectByQuoteid(quoteid);
    }

    @Override
    public void removeByQuoteid(String quoteid) {
        this.remove(new QueryWrapper<SalesOrder>().eq("quoteid",quoteid));
    }

	@Override
    public List<SalesOrder> selectBySlaid(String slaid) {
        return baseMapper.selectBySlaid(slaid);
    }

    @Override
    public void removeBySlaid(String slaid) {
        this.remove(new QueryWrapper<SalesOrder>().eq("slaid",slaid));
    }

	@Override
    public List<SalesOrder> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<SalesOrder>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 ByParentKey
     */
    @Override
    public Page<SalesOrder> searchByParentKey(SalesOrderSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<SalesOrder> pages=baseMapper.searchByParentKey(context.getPages(),context,context.getSelectCond());
        return new PageImpl<SalesOrder>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 已取消
     */
    @Override
    public Page<SalesOrder> searchCancel(SalesOrderSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<SalesOrder> pages=baseMapper.searchCancel(context.getPages(),context,context.getSelectCond());
        return new PageImpl<SalesOrder>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<SalesOrder> searchDefault(SalesOrderSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<SalesOrder> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<SalesOrder>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 已完成
     */
    @Override
    public Page<SalesOrder> searchFinish(SalesOrderSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<SalesOrder> pages=baseMapper.searchFinish(context.getPages(),context,context.getSelectCond());
        return new PageImpl<SalesOrder>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 已开发票
     */
    @Override
    public Page<SalesOrder> searchInvoiced(SalesOrderSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<SalesOrder> pages=baseMapper.searchInvoiced(context.getPages(),context,context.getSelectCond());
        return new PageImpl<SalesOrder>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(SalesOrder et){
        //实体关系[DER1N_SALESORDER__CAMPAIGN__CAMPAIGNID]
        if(!ObjectUtils.isEmpty(et.getCampaignid())){
            cn.ibizlab.businesscentral.core.marketing.domain.Campaign campaign=et.getCampaign();
            if(ObjectUtils.isEmpty(campaign)){
                cn.ibizlab.businesscentral.core.marketing.domain.Campaign majorEntity=campaignService.get(et.getCampaignid());
                et.setCampaign(majorEntity);
                campaign=majorEntity;
            }
            et.setCampaignname(campaign.getCampaignname());
        }
        //实体关系[DER1N_SALESORDER__OPPORTUNITY__OPPORTUNITYID]
        if(!ObjectUtils.isEmpty(et.getOpportunityid())){
            cn.ibizlab.businesscentral.core.sales.domain.Opportunity opportunity=et.getOpportunity();
            if(ObjectUtils.isEmpty(opportunity)){
                cn.ibizlab.businesscentral.core.sales.domain.Opportunity majorEntity=opportunityService.get(et.getOpportunityid());
                et.setOpportunity(majorEntity);
                opportunity=majorEntity;
            }
            et.setOpportunityname(opportunity.getOpportunityname());
        }
        //实体关系[DER1N_SALESORDER__PRICELEVEL__PRICELEVELID]
        if(!ObjectUtils.isEmpty(et.getPricelevelid())){
            cn.ibizlab.businesscentral.core.product.domain.PriceLevel pricelevel=et.getPricelevel();
            if(ObjectUtils.isEmpty(pricelevel)){
                cn.ibizlab.businesscentral.core.product.domain.PriceLevel majorEntity=pricelevelService.get(et.getPricelevelid());
                et.setPricelevel(majorEntity);
                pricelevel=majorEntity;
            }
            et.setPricelevelname(pricelevel.getPricelevelname());
        }
        //实体关系[DER1N_SALESORDER__QUOTE__QUOTEID]
        if(!ObjectUtils.isEmpty(et.getQuoteid())){
            cn.ibizlab.businesscentral.core.sales.domain.Quote quote=et.getQuote();
            if(ObjectUtils.isEmpty(quote)){
                cn.ibizlab.businesscentral.core.sales.domain.Quote majorEntity=quoteService.get(et.getQuoteid());
                et.setQuote(majorEntity);
                quote=majorEntity;
            }
            et.setQuotename(quote.getQuotename());
        }
        //实体关系[DER1N_SALESORDER__SLA__SLAID]
        if(!ObjectUtils.isEmpty(et.getSlaid())){
            cn.ibizlab.businesscentral.core.base.domain.Sla sla=et.getSla();
            if(ObjectUtils.isEmpty(sla)){
                cn.ibizlab.businesscentral.core.base.domain.Sla majorEntity=slaService.get(et.getSlaid());
                et.setSla(majorEntity);
                sla=majorEntity;
            }
            et.setSlaname(sla.getSlaname());
        }
        //实体关系[DER1N_SALESORDER__TRANSACTIONCURRENCY__TRANSACTIONCURRENCYID]
        if(!ObjectUtils.isEmpty(et.getTransactioncurrencyid())){
            cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency=et.getTransactioncurrency();
            if(ObjectUtils.isEmpty(transactioncurrency)){
                cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency majorEntity=transactioncurrencyService.get(et.getTransactioncurrencyid());
                et.setTransactioncurrency(majorEntity);
                transactioncurrency=majorEntity;
            }
            et.setCurrencyname(transactioncurrency.getCurrencyname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<SalesOrder> getSalesorderByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<SalesOrder> getSalesorderByEntities(List<SalesOrder> entities) {
        List ids =new ArrayList();
        for(SalesOrder entity : entities){
            Serializable id=entity.getSalesorderid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




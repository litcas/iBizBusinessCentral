package cn.ibizlab.businesscentral.core.product.service.logic.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;

import cn.ibizlab.businesscentral.core.product.service.logic.IProductReviseLogic;
import cn.ibizlab.businesscentral.core.product.domain.Product;

/**
 * 关系型数据实体[Revise] 对象
 */
@Slf4j
@Service
public class ProductReviseLogicImpl implements IProductReviseLogic{

    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private cn.ibizlab.businesscentral.core.product.service.IProductService productservice;

    public cn.ibizlab.businesscentral.core.product.service.IProductService getProductService() {
        return this.productservice;
    }


    @Autowired
    private cn.ibizlab.businesscentral.core.product.service.IProductService iBzSysDefaultService;

    public cn.ibizlab.businesscentral.core.product.service.IProductService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    public void execute(Product et){

          KieSession kieSession = null;
        try{
           kieSession=kieContainer.newKieSession();
           kieSession.insert(et); 
           kieSession.setGlobal("productrevisedefault",et);
           kieSession.setGlobal("productservice",productservice);
           kieSession.setGlobal("iBzSysProductDefaultService",iBzSysDefaultService);
           kieSession.setGlobal("curuser", cn.ibizlab.businesscentral.util.security.AuthenticationUser.getAuthenticationUser());
           kieSession.startProcess("cn.ibizlab.businesscentral.core.product.service.logic.productrevise");

        }catch(Exception e){
            throw new RuntimeException("执行[修订]处理逻辑发生异常"+e);
        }finally {
            if(kieSession!=null)
            kieSession.destroy();
        }
    }

}

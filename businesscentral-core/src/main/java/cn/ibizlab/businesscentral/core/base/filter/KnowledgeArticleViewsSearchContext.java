package cn.ibizlab.businesscentral.core.base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.base.domain.KnowledgeArticleViews;
/**
 * 关系型数据实体[KnowledgeArticleViews] 查询条件对象
 */
@Slf4j
@Data
public class KnowledgeArticleViewsSearchContext extends QueryWrapperContext<KnowledgeArticleViews> {

	private Integer n_statuscode_eq;//[状态描述]
	public void setN_statuscode_eq(Integer n_statuscode_eq) {
        this.n_statuscode_eq = n_statuscode_eq;
        if(!ObjectUtils.isEmpty(this.n_statuscode_eq)){
            this.getSearchCond().eq("statuscode", n_statuscode_eq);
        }
    }
	private String n_location_eq;//[位置]
	public void setN_location_eq(String n_location_eq) {
        this.n_location_eq = n_location_eq;
        if(!ObjectUtils.isEmpty(this.n_location_eq)){
            this.getSearchCond().eq("location", n_location_eq);
        }
    }
	private Integer n_statecode_eq;//[状态]
	public void setN_statecode_eq(Integer n_statecode_eq) {
        this.n_statecode_eq = n_statecode_eq;
        if(!ObjectUtils.isEmpty(this.n_statecode_eq)){
            this.getSearchCond().eq("statecode", n_statecode_eq);
        }
    }
	private String n_knowledgearticleid_eq;//[知识文章]
	public void setN_knowledgearticleid_eq(String n_knowledgearticleid_eq) {
        this.n_knowledgearticleid_eq = n_knowledgearticleid_eq;
        if(!ObjectUtils.isEmpty(this.n_knowledgearticleid_eq)){
            this.getSearchCond().eq("knowledgearticleid", n_knowledgearticleid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("statecode", query)   
            );
		 }
	}
}




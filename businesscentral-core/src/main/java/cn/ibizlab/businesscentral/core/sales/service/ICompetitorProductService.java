package cn.ibizlab.businesscentral.core.sales.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.sales.domain.CompetitorProduct;
import cn.ibizlab.businesscentral.core.sales.filter.CompetitorProductSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[CompetitorProduct] 服务对象接口
 */
public interface ICompetitorProductService extends IService<CompetitorProduct>{

    boolean create(CompetitorProduct et) ;
    void createBatch(List<CompetitorProduct> list) ;
    boolean update(CompetitorProduct et) ;
    void updateBatch(List<CompetitorProduct> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    CompetitorProduct get(String key) ;
    CompetitorProduct getDraft(CompetitorProduct et) ;
    boolean checkKey(CompetitorProduct et) ;
    boolean save(CompetitorProduct et) ;
    void saveBatch(List<CompetitorProduct> list) ;
    Page<CompetitorProduct> searchDefault(CompetitorProductSearchContext context) ;
    List<CompetitorProduct> selectByEntityid(String competitorid) ;
    void removeByEntityid(String competitorid) ;
    List<CompetitorProduct> selectByEntity2id(String productid) ;
    void removeByEntity2id(String productid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<CompetitorProduct> getCompetitorproductByIds(List<String> ids) ;
    List<CompetitorProduct> getCompetitorproductByEntities(List<CompetitorProduct> entities) ;
}



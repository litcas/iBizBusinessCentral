package cn.ibizlab.businesscentral.core.service.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.service.domain.Contract;
import cn.ibizlab.businesscentral.core.service.filter.ContractSearchContext;
import cn.ibizlab.businesscentral.core.service.service.IContractService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.service.mapper.ContractMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[合同] 服务对象接口实现
 */
@Slf4j
@Service("ContractServiceImpl")
public class ContractServiceImpl extends ServiceImpl<ContractMapper, Contract> implements IContractService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IContractDetailService contractdetailService;

    protected cn.ibizlab.businesscentral.core.service.service.IContractService contractService = this;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IIncidentService incidentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IContractTemplateService contracttemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ICustomerAddressService customeraddressService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(Contract et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getContractid()),et);
        return true;
    }

    @Override
    public void createBatch(List<Contract> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Contract et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("contractid",et.getContractid())))
            return false;
        CachedBeanCopier.copy(get(et.getContractid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<Contract> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Contract get(String key) {
        Contract et = getById(key);
        if(et==null){
            et=new Contract();
            et.setContractid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Contract getDraft(Contract et) {
        return et;
    }

    @Override
    public boolean checkKey(Contract et) {
        return (!ObjectUtils.isEmpty(et.getContractid()))&&(!Objects.isNull(this.getById(et.getContractid())));
    }
    @Override
    @Transactional
    public boolean save(Contract et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Contract et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<Contract> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<Contract> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Contract> selectByContracttemplateid(String contracttemplateid) {
        return baseMapper.selectByContracttemplateid(contracttemplateid);
    }

    @Override
    public void removeByContracttemplateid(String contracttemplateid) {
        this.remove(new QueryWrapper<Contract>().eq("contracttemplateid",contracttemplateid));
    }

	@Override
    public List<Contract> selectByOriginatingcontract(String contractid) {
        return baseMapper.selectByOriginatingcontract(contractid);
    }

    @Override
    public void removeByOriginatingcontract(String contractid) {
        this.remove(new QueryWrapper<Contract>().eq("originatingcontract",contractid));
    }

	@Override
    public List<Contract> selectByBilltoaddress(String customeraddressid) {
        return baseMapper.selectByBilltoaddress(customeraddressid);
    }

    @Override
    public void removeByBilltoaddress(String customeraddressid) {
        this.remove(new QueryWrapper<Contract>().eq("billtoaddress",customeraddressid));
    }

	@Override
    public List<Contract> selectByServiceaddress(String customeraddressid) {
        return baseMapper.selectByServiceaddress(customeraddressid);
    }

    @Override
    public void removeByServiceaddress(String customeraddressid) {
        this.remove(new QueryWrapper<Contract>().eq("serviceaddress",customeraddressid));
    }

	@Override
    public List<Contract> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<Contract>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<Contract> searchDefault(ContractSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Contract> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Contract>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Contract> getContractByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Contract> getContractByEntities(List<Contract> entities) {
        List ids =new ArrayList();
        for(Contract entity : entities){
            Serializable id=entity.getContractid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




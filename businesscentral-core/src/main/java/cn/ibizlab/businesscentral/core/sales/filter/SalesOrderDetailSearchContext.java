package cn.ibizlab.businesscentral.core.sales.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.sales.domain.SalesOrderDetail;
/**
 * 关系型数据实体[SalesOrderDetail] 查询条件对象
 */
@Slf4j
@Data
public class SalesOrderDetailSearchContext extends QueryWrapperContext<SalesOrderDetail> {

	private String n_producttypecode_eq;//[产品类型]
	public void setN_producttypecode_eq(String n_producttypecode_eq) {
        this.n_producttypecode_eq = n_producttypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_producttypecode_eq)){
            this.getSearchCond().eq("producttypecode", n_producttypecode_eq);
        }
    }
	private String n_skippricecalculation_eq;//[SkipPriceCalculation]
	public void setN_skippricecalculation_eq(String n_skippricecalculation_eq) {
        this.n_skippricecalculation_eq = n_skippricecalculation_eq;
        if(!ObjectUtils.isEmpty(this.n_skippricecalculation_eq)){
            this.getSearchCond().eq("skippricecalculation", n_skippricecalculation_eq);
        }
    }
	private String n_salesorderstatecode_eq;//[订单状态]
	public void setN_salesorderstatecode_eq(String n_salesorderstatecode_eq) {
        this.n_salesorderstatecode_eq = n_salesorderstatecode_eq;
        if(!ObjectUtils.isEmpty(this.n_salesorderstatecode_eq)){
            this.getSearchCond().eq("salesorderstatecode", n_salesorderstatecode_eq);
        }
    }
	private String n_propertyconfigurationstatus_eq;//[属性配置]
	public void setN_propertyconfigurationstatus_eq(String n_propertyconfigurationstatus_eq) {
        this.n_propertyconfigurationstatus_eq = n_propertyconfigurationstatus_eq;
        if(!ObjectUtils.isEmpty(this.n_propertyconfigurationstatus_eq)){
            this.getSearchCond().eq("propertyconfigurationstatus", n_propertyconfigurationstatus_eq);
        }
    }
	private String n_shipto_freighttermscode_eq;//[货运条款]
	public void setN_shipto_freighttermscode_eq(String n_shipto_freighttermscode_eq) {
        this.n_shipto_freighttermscode_eq = n_shipto_freighttermscode_eq;
        if(!ObjectUtils.isEmpty(this.n_shipto_freighttermscode_eq)){
            this.getSearchCond().eq("shipto_freighttermscode", n_shipto_freighttermscode_eq);
        }
    }
	private String n_pricingerrorcode_eq;//[定价错误]
	public void setN_pricingerrorcode_eq(String n_pricingerrorcode_eq) {
        this.n_pricingerrorcode_eq = n_pricingerrorcode_eq;
        if(!ObjectUtils.isEmpty(this.n_pricingerrorcode_eq)){
            this.getSearchCond().eq("pricingerrorcode", n_pricingerrorcode_eq);
        }
    }
	private String n_salesorderdetailname_like;//[名称]
	public void setN_salesorderdetailname_like(String n_salesorderdetailname_like) {
        this.n_salesorderdetailname_like = n_salesorderdetailname_like;
        if(!ObjectUtils.isEmpty(this.n_salesorderdetailname_like)){
            this.getSearchCond().like("salesorderdetailname", n_salesorderdetailname_like);
        }
    }
	private String n_productname_eq;//[产品名称]
	public void setN_productname_eq(String n_productname_eq) {
        this.n_productname_eq = n_productname_eq;
        if(!ObjectUtils.isEmpty(this.n_productname_eq)){
            this.getSearchCond().eq("productname", n_productname_eq);
        }
    }
	private String n_productname_like;//[产品名称]
	public void setN_productname_like(String n_productname_like) {
        this.n_productname_like = n_productname_like;
        if(!ObjectUtils.isEmpty(this.n_productname_like)){
            this.getSearchCond().like("productname", n_productname_like);
        }
    }
	private String n_uomname_eq;//[计价单位]
	public void setN_uomname_eq(String n_uomname_eq) {
        this.n_uomname_eq = n_uomname_eq;
        if(!ObjectUtils.isEmpty(this.n_uomname_eq)){
            this.getSearchCond().eq("uomname", n_uomname_eq);
        }
    }
	private String n_uomname_like;//[计价单位]
	public void setN_uomname_like(String n_uomname_like) {
        this.n_uomname_like = n_uomname_like;
        if(!ObjectUtils.isEmpty(this.n_uomname_like)){
            this.getSearchCond().like("uomname", n_uomname_like);
        }
    }
	private String n_currencyname_eq;//[货币名称]
	public void setN_currencyname_eq(String n_currencyname_eq) {
        this.n_currencyname_eq = n_currencyname_eq;
        if(!ObjectUtils.isEmpty(this.n_currencyname_eq)){
            this.getSearchCond().eq("currencyname", n_currencyname_eq);
        }
    }
	private String n_currencyname_like;//[货币名称]
	public void setN_currencyname_like(String n_currencyname_like) {
        this.n_currencyname_like = n_currencyname_like;
        if(!ObjectUtils.isEmpty(this.n_currencyname_like)){
            this.getSearchCond().like("currencyname", n_currencyname_like);
        }
    }
	private String n_quotedetailid_eq;//[Quote Product Id]
	public void setN_quotedetailid_eq(String n_quotedetailid_eq) {
        this.n_quotedetailid_eq = n_quotedetailid_eq;
        if(!ObjectUtils.isEmpty(this.n_quotedetailid_eq)){
            this.getSearchCond().eq("quotedetailid", n_quotedetailid_eq);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }
	private String n_productid_eq;//[现有产品]
	public void setN_productid_eq(String n_productid_eq) {
        this.n_productid_eq = n_productid_eq;
        if(!ObjectUtils.isEmpty(this.n_productid_eq)){
            this.getSearchCond().eq("productid", n_productid_eq);
        }
    }
	private String n_salesorderid_eq;//[订单]
	public void setN_salesorderid_eq(String n_salesorderid_eq) {
        this.n_salesorderid_eq = n_salesorderid_eq;
        if(!ObjectUtils.isEmpty(this.n_salesorderid_eq)){
            this.getSearchCond().eq("salesorderid", n_salesorderid_eq);
        }
    }
	private String n_parentbundleidref_eq;//[Parent bundle product]
	public void setN_parentbundleidref_eq(String n_parentbundleidref_eq) {
        this.n_parentbundleidref_eq = n_parentbundleidref_eq;
        if(!ObjectUtils.isEmpty(this.n_parentbundleidref_eq)){
            this.getSearchCond().eq("parentbundleidref", n_parentbundleidref_eq);
        }
    }
	private String n_uomid_eq;//[计价单位]
	public void setN_uomid_eq(String n_uomid_eq) {
        this.n_uomid_eq = n_uomid_eq;
        if(!ObjectUtils.isEmpty(this.n_uomid_eq)){
            this.getSearchCond().eq("uomid", n_uomid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("salesorderdetailname", query)   
            );
		 }
	}
}




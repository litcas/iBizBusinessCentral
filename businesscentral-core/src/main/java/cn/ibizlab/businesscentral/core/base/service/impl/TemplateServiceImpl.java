package cn.ibizlab.businesscentral.core.base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.base.domain.Template;
import cn.ibizlab.businesscentral.core.base.filter.TemplateSearchContext;
import cn.ibizlab.businesscentral.core.base.service.ITemplateService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.base.mapper.TemplateMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[电子邮件模板] 服务对象接口实现
 */
@Slf4j
@Service("TemplateServiceImpl")
public class TemplateServiceImpl extends ServiceImpl<TemplateMapper, Template> implements ITemplateService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IEmailService emailService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(Template et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getTemplateid()),et);
        return true;
    }

    @Override
    public void createBatch(List<Template> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Template et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("templateid",et.getTemplateid())))
            return false;
        CachedBeanCopier.copy(get(et.getTemplateid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<Template> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Template get(String key) {
        Template et = getById(key);
        if(et==null){
            et=new Template();
            et.setTemplateid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Template getDraft(Template et) {
        return et;
    }

    @Override
    public boolean checkKey(Template et) {
        return (!ObjectUtils.isEmpty(et.getTemplateid()))&&(!Objects.isNull(this.getById(et.getTemplateid())));
    }
    @Override
    @Transactional
    public boolean save(Template et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Template et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<Template> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<Template> list) {
        saveOrUpdateBatch(list,batchSize);
    }



    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<Template> searchDefault(TemplateSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Template> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Template>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Template> getTemplateByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Template> getTemplateByEntities(List<Template> entities) {
        List ids =new ArrayList();
        for(Template entity : entities){
            Serializable id=entity.getTemplateid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




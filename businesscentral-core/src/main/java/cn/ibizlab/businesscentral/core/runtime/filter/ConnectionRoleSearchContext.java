package cn.ibizlab.businesscentral.core.runtime.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.runtime.domain.ConnectionRole;
/**
 * 关系型数据实体[ConnectionRole] 查询条件对象
 */
@Slf4j
@Data
public class ConnectionRoleSearchContext extends QueryWrapperContext<ConnectionRole> {

	private String n_componentstate_eq;//[组件状态]
	public void setN_componentstate_eq(String n_componentstate_eq) {
        this.n_componentstate_eq = n_componentstate_eq;
        if(!ObjectUtils.isEmpty(this.n_componentstate_eq)){
            this.getSearchCond().eq("componentstate", n_componentstate_eq);
        }
    }
	private String n_category_eq;//[连接角色类别]
	public void setN_category_eq(String n_category_eq) {
        this.n_category_eq = n_category_eq;
        if(!ObjectUtils.isEmpty(this.n_category_eq)){
            this.getSearchCond().eq("category", n_category_eq);
        }
    }
	private String n_connectionrolename_like;//[关联角色名称]
	public void setN_connectionrolename_like(String n_connectionrolename_like) {
        this.n_connectionrolename_like = n_connectionrolename_like;
        if(!ObjectUtils.isEmpty(this.n_connectionrolename_like)){
            this.getSearchCond().like("connectionrolename", n_connectionrolename_like);
        }
    }
	private Integer n_statuscode_eq;//[状态描述]
	public void setN_statuscode_eq(Integer n_statuscode_eq) {
        this.n_statuscode_eq = n_statuscode_eq;
        if(!ObjectUtils.isEmpty(this.n_statuscode_eq)){
            this.getSearchCond().eq("statuscode", n_statuscode_eq);
        }
    }
	private Integer n_statecode_eq;//[状态]
	public void setN_statecode_eq(Integer n_statecode_eq) {
        this.n_statecode_eq = n_statecode_eq;
        if(!ObjectUtils.isEmpty(this.n_statecode_eq)){
            this.getSearchCond().eq("statecode", n_statecode_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("connectionrolename", query)   
            );
		 }
	}
}




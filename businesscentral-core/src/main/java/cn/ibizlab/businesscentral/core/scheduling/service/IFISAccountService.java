package cn.ibizlab.businesscentral.core.scheduling.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.scheduling.domain.FISAccount;
import cn.ibizlab.businesscentral.core.scheduling.filter.FISAccountSearchContext;


/**
 * 实体[FISAccount] 服务对象接口
 */
public interface IFISAccountService{

    boolean create(FISAccount et) ;
    void createBatch(List<FISAccount> list) ;
    boolean update(FISAccount et) ;
    void updateBatch(List<FISAccount> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    FISAccount get(String key) ;
    FISAccount getDraft(FISAccount et) ;
    boolean checkKey(FISAccount et) ;
    boolean save(FISAccount et) ;
    void saveBatch(List<FISAccount> list) ;
    Page<FISAccount> searchDefault(FISAccountSearchContext context) ;

}




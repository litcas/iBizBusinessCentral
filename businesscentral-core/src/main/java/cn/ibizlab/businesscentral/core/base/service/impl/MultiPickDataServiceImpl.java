package cn.ibizlab.businesscentral.core.base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.base.domain.MultiPickData;
import cn.ibizlab.businesscentral.core.base.filter.MultiPickDataSearchContext;
import cn.ibizlab.businesscentral.core.base.service.IMultiPickDataService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.base.mapper.MultiPickDataMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[多类选择实体] 服务对象接口实现
 */
@Slf4j
@Service("MultiPickDataServiceImpl")
public class MultiPickDataServiceImpl extends ServiceImpl<MultiPickDataMapper, MultiPickData> implements IMultiPickDataService {


    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(MultiPickData et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getPickdataid()),et);
        return true;
    }

    @Override
    public void createBatch(List<MultiPickData> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(MultiPickData et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("pickdataid",et.getPickdataid())))
            return false;
        CachedBeanCopier.copy(get(et.getPickdataid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<MultiPickData> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public MultiPickData get(String key) {
        MultiPickData et = getById(key);
        if(et==null){
            et=new MultiPickData();
            et.setPickdataid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public MultiPickData getDraft(MultiPickData et) {
        return et;
    }

    @Override
    public boolean checkKey(MultiPickData et) {
        return (!ObjectUtils.isEmpty(et.getPickdataid()))&&(!Objects.isNull(this.getById(et.getPickdataid())));
    }
    @Override
    @Transactional
    public boolean save(MultiPickData et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(MultiPickData et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<MultiPickData> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<MultiPickData> list) {
        saveOrUpdateBatch(list,batchSize);
    }



    /**
     * 查询集合 客户、联系人
     */
    @Override
    public Page<MultiPickData> searchAC(MultiPickDataSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<MultiPickData> pages=baseMapper.searchAC(context.getPages(),context,context.getSelectCond());
        return new PageImpl<MultiPickData>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 客户、联系人、潜在客户
     */
    @Override
    public Page<MultiPickData> searchACL(MultiPickDataSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<MultiPickData> pages=baseMapper.searchACL(context.getPages(),context,context.getSelectCond());
        return new PageImpl<MultiPickData>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<MultiPickData> searchDefault(MultiPickDataSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<MultiPickData> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<MultiPickData>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }


}




package cn.ibizlab.businesscentral.core.marketing.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.marketing.domain.CampaignList;
import cn.ibizlab.businesscentral.core.marketing.filter.CampaignListSearchContext;
import cn.ibizlab.businesscentral.core.marketing.service.ICampaignListService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.marketing.mapper.CampaignListMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[市场活动-营销列表] 服务对象接口实现
 */
@Slf4j
@Service("CampaignListServiceImpl")
public class CampaignListServiceImpl extends ServiceImpl<CampaignListMapper, CampaignList> implements ICampaignListService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.marketing.service.ICampaignService campaignService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.marketing.service.IIBizListService ibizlistService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(CampaignList et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getRelationshipsid()),et);
        return true;
    }

    @Override
    public void createBatch(List<CampaignList> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(CampaignList et) {
        fillParentData(et);
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("relationshipsid",et.getRelationshipsid())))
            return false;
        CachedBeanCopier.copy(get(et.getRelationshipsid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<CampaignList> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public CampaignList get(String key) {
        CampaignList et = getById(key);
        if(et==null){
            et=new CampaignList();
            et.setRelationshipsid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public CampaignList getDraft(CampaignList et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(CampaignList et) {
        return (!ObjectUtils.isEmpty(et.getRelationshipsid()))&&(!Objects.isNull(this.getById(et.getRelationshipsid())));
    }
    @Override
    @Transactional
    public boolean save(CampaignList et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(CampaignList et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<CampaignList> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<CampaignList> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<CampaignList> selectByEntityid(String campaignid) {
        return baseMapper.selectByEntityid(campaignid);
    }

    @Override
    public void removeByEntityid(String campaignid) {
        this.remove(new QueryWrapper<CampaignList>().eq("entityid",campaignid));
    }

	@Override
    public List<CampaignList> selectByEntity2id(String listid) {
        return baseMapper.selectByEntity2id(listid);
    }

    @Override
    public void removeByEntity2id(String listid) {
        this.remove(new QueryWrapper<CampaignList>().eq("entity2id",listid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<CampaignList> searchDefault(CampaignListSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<CampaignList> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<CampaignList>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(CampaignList et){
        //实体关系[DER1N_CAMPAIGNLIST_CAMPAIGN_CAMPAIGNID]
        if(!ObjectUtils.isEmpty(et.getEntityid())){
            cn.ibizlab.businesscentral.core.marketing.domain.Campaign campaign=et.getCampaign();
            if(ObjectUtils.isEmpty(campaign)){
                cn.ibizlab.businesscentral.core.marketing.domain.Campaign majorEntity=campaignService.get(et.getEntityid());
                et.setCampaign(majorEntity);
                campaign=majorEntity;
            }
            et.setEntityname(campaign.getCampaignname());
        }
        //实体关系[DER1N_CAMPAIGNLIST_LIST_LISTID]
        if(!ObjectUtils.isEmpty(et.getEntity2id())){
            cn.ibizlab.businesscentral.core.marketing.domain.IBizList list=et.getList();
            if(ObjectUtils.isEmpty(list)){
                cn.ibizlab.businesscentral.core.marketing.domain.IBizList majorEntity=ibizlistService.get(et.getEntity2id());
                et.setList(majorEntity);
                list=majorEntity;
            }
            et.setEntity2name(list.getListname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<CampaignList> getCampaignlistByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<CampaignList> getCampaignlistByEntities(List<CampaignList> entities) {
        List ids =new ArrayList();
        for(CampaignList entity : entities){
            Serializable id=entity.getRelationshipsid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




package cn.ibizlab.businesscentral.core.marketing.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.marketing.domain.CampaignCampaign;
import cn.ibizlab.businesscentral.core.marketing.filter.CampaignCampaignSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface CampaignCampaignMapper extends BaseMapper<CampaignCampaign>{

    Page<CampaignCampaign> searchDefault(IPage page, @Param("srf") CampaignCampaignSearchContext context, @Param("ew") Wrapper<CampaignCampaign> wrapper) ;
    @Override
    CampaignCampaign selectById(Serializable id);
    @Override
    int insert(CampaignCampaign entity);
    @Override
    int updateById(@Param(Constants.ENTITY) CampaignCampaign entity);
    @Override
    int update(@Param(Constants.ENTITY) CampaignCampaign entity, @Param("ew") Wrapper<CampaignCampaign> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<CampaignCampaign> selectByEntity2id(@Param("campaignid") Serializable campaignid) ;

    List<CampaignCampaign> selectByEntityid(@Param("campaignid") Serializable campaignid) ;

}

package cn.ibizlab.businesscentral.core.marketing.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.marketing.domain.ListContact;
import cn.ibizlab.businesscentral.core.marketing.filter.ListContactSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[ListContact] 服务对象接口
 */
public interface IListContactService extends IService<ListContact>{

    boolean create(ListContact et) ;
    void createBatch(List<ListContact> list) ;
    boolean update(ListContact et) ;
    void updateBatch(List<ListContact> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    ListContact get(String key) ;
    ListContact getDraft(ListContact et) ;
    boolean checkKey(ListContact et) ;
    boolean save(ListContact et) ;
    void saveBatch(List<ListContact> list) ;
    Page<ListContact> searchDefault(ListContactSearchContext context) ;
    List<ListContact> selectByEntity2id(String contactid) ;
    void removeByEntity2id(String contactid) ;
    List<ListContact> selectByEntityid(String listid) ;
    void removeByEntityid(String listid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<ListContact> getListcontactByIds(List<String> ids) ;
    List<ListContact> getListcontactByEntities(List<ListContact> entities) ;
}



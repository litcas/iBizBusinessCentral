package cn.ibizlab.businesscentral.core.sales.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.sales.domain.SalesOrder;
/**
 * 关系型数据实体[SalesOrder] 查询条件对象
 */
@Slf4j
@Data
public class SalesOrderSearchContext extends QueryWrapperContext<SalesOrder> {

	private String n_pricingerrorcode_eq;//[定价错误]
	public void setN_pricingerrorcode_eq(String n_pricingerrorcode_eq) {
        this.n_pricingerrorcode_eq = n_pricingerrorcode_eq;
        if(!ObjectUtils.isEmpty(this.n_pricingerrorcode_eq)){
            this.getSearchCond().eq("pricingerrorcode", n_pricingerrorcode_eq);
        }
    }
	private Integer n_statecode_eq;//[状态]
	public void setN_statecode_eq(Integer n_statecode_eq) {
        this.n_statecode_eq = n_statecode_eq;
        if(!ObjectUtils.isEmpty(this.n_statecode_eq)){
            this.getSearchCond().eq("statecode", n_statecode_eq);
        }
    }
	private String n_shipto_freighttermscode_eq;//[货运条款]
	public void setN_shipto_freighttermscode_eq(String n_shipto_freighttermscode_eq) {
        this.n_shipto_freighttermscode_eq = n_shipto_freighttermscode_eq;
        if(!ObjectUtils.isEmpty(this.n_shipto_freighttermscode_eq)){
            this.getSearchCond().eq("shipto_freighttermscode", n_shipto_freighttermscode_eq);
        }
    }
	private String n_prioritycode_eq;//[优先级]
	public void setN_prioritycode_eq(String n_prioritycode_eq) {
        this.n_prioritycode_eq = n_prioritycode_eq;
        if(!ObjectUtils.isEmpty(this.n_prioritycode_eq)){
            this.getSearchCond().eq("prioritycode", n_prioritycode_eq);
        }
    }
	private Integer n_statuscode_eq;//[状态描述]
	public void setN_statuscode_eq(Integer n_statuscode_eq) {
        this.n_statuscode_eq = n_statuscode_eq;
        if(!ObjectUtils.isEmpty(this.n_statuscode_eq)){
            this.getSearchCond().eq("statuscode", n_statuscode_eq);
        }
    }
	private String n_paymenttermscode_eq;//[付款条件]
	public void setN_paymenttermscode_eq(String n_paymenttermscode_eq) {
        this.n_paymenttermscode_eq = n_paymenttermscode_eq;
        if(!ObjectUtils.isEmpty(this.n_paymenttermscode_eq)){
            this.getSearchCond().eq("paymenttermscode", n_paymenttermscode_eq);
        }
    }
	private String n_freighttermscode_eq;//[货运条款]
	public void setN_freighttermscode_eq(String n_freighttermscode_eq) {
        this.n_freighttermscode_eq = n_freighttermscode_eq;
        if(!ObjectUtils.isEmpty(this.n_freighttermscode_eq)){
            this.getSearchCond().eq("freighttermscode", n_freighttermscode_eq);
        }
    }
	private String n_salesordername_like;//[销售订单名称]
	public void setN_salesordername_like(String n_salesordername_like) {
        this.n_salesordername_like = n_salesordername_like;
        if(!ObjectUtils.isEmpty(this.n_salesordername_like)){
            this.getSearchCond().like("salesordername", n_salesordername_like);
        }
    }
	private String n_shippingmethodcode_eq;//[送货方式]
	public void setN_shippingmethodcode_eq(String n_shippingmethodcode_eq) {
        this.n_shippingmethodcode_eq = n_shippingmethodcode_eq;
        if(!ObjectUtils.isEmpty(this.n_shippingmethodcode_eq)){
            this.getSearchCond().eq("shippingmethodcode", n_shippingmethodcode_eq);
        }
    }
	private String n_campaignname_eq;//[源市场活动]
	public void setN_campaignname_eq(String n_campaignname_eq) {
        this.n_campaignname_eq = n_campaignname_eq;
        if(!ObjectUtils.isEmpty(this.n_campaignname_eq)){
            this.getSearchCond().eq("campaignname", n_campaignname_eq);
        }
    }
	private String n_campaignname_like;//[源市场活动]
	public void setN_campaignname_like(String n_campaignname_like) {
        this.n_campaignname_like = n_campaignname_like;
        if(!ObjectUtils.isEmpty(this.n_campaignname_like)){
            this.getSearchCond().like("campaignname", n_campaignname_like);
        }
    }
	private String n_opportunityname_eq;//[商机]
	public void setN_opportunityname_eq(String n_opportunityname_eq) {
        this.n_opportunityname_eq = n_opportunityname_eq;
        if(!ObjectUtils.isEmpty(this.n_opportunityname_eq)){
            this.getSearchCond().eq("opportunityname", n_opportunityname_eq);
        }
    }
	private String n_opportunityname_like;//[商机]
	public void setN_opportunityname_like(String n_opportunityname_like) {
        this.n_opportunityname_like = n_opportunityname_like;
        if(!ObjectUtils.isEmpty(this.n_opportunityname_like)){
            this.getSearchCond().like("opportunityname", n_opportunityname_like);
        }
    }
	private String n_quotename_eq;//[报价单]
	public void setN_quotename_eq(String n_quotename_eq) {
        this.n_quotename_eq = n_quotename_eq;
        if(!ObjectUtils.isEmpty(this.n_quotename_eq)){
            this.getSearchCond().eq("quotename", n_quotename_eq);
        }
    }
	private String n_quotename_like;//[报价单]
	public void setN_quotename_like(String n_quotename_like) {
        this.n_quotename_like = n_quotename_like;
        if(!ObjectUtils.isEmpty(this.n_quotename_like)){
            this.getSearchCond().like("quotename", n_quotename_like);
        }
    }
	private String n_pricelevelname_eq;//[价目表]
	public void setN_pricelevelname_eq(String n_pricelevelname_eq) {
        this.n_pricelevelname_eq = n_pricelevelname_eq;
        if(!ObjectUtils.isEmpty(this.n_pricelevelname_eq)){
            this.getSearchCond().eq("pricelevelname", n_pricelevelname_eq);
        }
    }
	private String n_pricelevelname_like;//[价目表]
	public void setN_pricelevelname_like(String n_pricelevelname_like) {
        this.n_pricelevelname_like = n_pricelevelname_like;
        if(!ObjectUtils.isEmpty(this.n_pricelevelname_like)){
            this.getSearchCond().like("pricelevelname", n_pricelevelname_like);
        }
    }
	private String n_currencyname_eq;//[货币]
	public void setN_currencyname_eq(String n_currencyname_eq) {
        this.n_currencyname_eq = n_currencyname_eq;
        if(!ObjectUtils.isEmpty(this.n_currencyname_eq)){
            this.getSearchCond().eq("currencyname", n_currencyname_eq);
        }
    }
	private String n_currencyname_like;//[货币]
	public void setN_currencyname_like(String n_currencyname_like) {
        this.n_currencyname_like = n_currencyname_like;
        if(!ObjectUtils.isEmpty(this.n_currencyname_like)){
            this.getSearchCond().like("currencyname", n_currencyname_like);
        }
    }
	private String n_quoteid_eq;//[报价单]
	public void setN_quoteid_eq(String n_quoteid_eq) {
        this.n_quoteid_eq = n_quoteid_eq;
        if(!ObjectUtils.isEmpty(this.n_quoteid_eq)){
            this.getSearchCond().eq("quoteid", n_quoteid_eq);
        }
    }
	private String n_slaid_eq;//[SLA]
	public void setN_slaid_eq(String n_slaid_eq) {
        this.n_slaid_eq = n_slaid_eq;
        if(!ObjectUtils.isEmpty(this.n_slaid_eq)){
            this.getSearchCond().eq("slaid", n_slaid_eq);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }
	private String n_opportunityid_eq;//[商机]
	public void setN_opportunityid_eq(String n_opportunityid_eq) {
        this.n_opportunityid_eq = n_opportunityid_eq;
        if(!ObjectUtils.isEmpty(this.n_opportunityid_eq)){
            this.getSearchCond().eq("opportunityid", n_opportunityid_eq);
        }
    }
	private String n_pricelevelid_eq;//[价目表]
	public void setN_pricelevelid_eq(String n_pricelevelid_eq) {
        this.n_pricelevelid_eq = n_pricelevelid_eq;
        if(!ObjectUtils.isEmpty(this.n_pricelevelid_eq)){
            this.getSearchCond().eq("pricelevelid", n_pricelevelid_eq);
        }
    }
	private String n_campaignid_eq;//[源市场活动]
	public void setN_campaignid_eq(String n_campaignid_eq) {
        this.n_campaignid_eq = n_campaignid_eq;
        if(!ObjectUtils.isEmpty(this.n_campaignid_eq)){
            this.getSearchCond().eq("campaignid", n_campaignid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("salesordername", query)   
            );
		 }
	}
}




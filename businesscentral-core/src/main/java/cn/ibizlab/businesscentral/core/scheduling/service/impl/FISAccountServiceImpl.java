package cn.ibizlab.businesscentral.core.scheduling.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.scheduling.domain.FISAccount;
import cn.ibizlab.businesscentral.core.scheduling.filter.FISAccountSearchContext;
import cn.ibizlab.businesscentral.core.scheduling.service.IFISAccountService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import cn.ibizlab.businesscentral.core.scheduling.client.FISAccountFeignClient;

/**
 * 实体[FIS账户] 服务对象接口实现
 */
@Slf4j
@Service
public class FISAccountServiceImpl implements IFISAccountService {

    @Autowired
    FISAccountFeignClient fISAccountFeignClient;


    @Override
    public boolean create(FISAccount et) {
        FISAccount rt = fISAccountFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<FISAccount> list){
        fISAccountFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(FISAccount et) {
        FISAccount rt = fISAccountFeignClient.update(et.getAccountid(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<FISAccount> list){
        fISAccountFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(String accountid) {
        boolean result=fISAccountFeignClient.remove(accountid) ;
        return result;
    }

    public void removeBatch(Collection<String> idList){
        fISAccountFeignClient.removeBatch(idList);
    }

    @Override
    public FISAccount get(String accountid) {
		FISAccount et=fISAccountFeignClient.get(accountid);
        if(et==null){
            et=new FISAccount();
            et.setAccountid(accountid);
        }
        else{
        }
        return  et;
    }

    @Override
    public FISAccount getDraft(FISAccount et) {
        et=fISAccountFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean checkKey(FISAccount et) {
        return fISAccountFeignClient.checkKey(et);
    }
    @Override
    @Transactional
    public boolean save(FISAccount et) {
        if(et.getAccountid()==null) et.setAccountid((String)et.getDefaultKey(true));
        if(!fISAccountFeignClient.save(et))
            return false;
        return true;
    }

    @Override
    public void saveBatch(List<FISAccount> list) {
        fISAccountFeignClient.saveBatch(list) ;
    }





    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<FISAccount> searchDefault(FISAccountSearchContext context) {
        Page<FISAccount> fISAccounts=fISAccountFeignClient.searchDefault(context);
        return fISAccounts;
    }



}




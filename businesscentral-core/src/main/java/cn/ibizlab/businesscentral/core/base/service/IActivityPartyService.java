package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.ActivityParty;
import cn.ibizlab.businesscentral.core.base.filter.ActivityPartySearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[ActivityParty] 服务对象接口
 */
public interface IActivityPartyService extends IService<ActivityParty>{

    boolean create(ActivityParty et) ;
    void createBatch(List<ActivityParty> list) ;
    boolean update(ActivityParty et) ;
    void updateBatch(List<ActivityParty> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    ActivityParty get(String key) ;
    ActivityParty getDraft(ActivityParty et) ;
    boolean checkKey(ActivityParty et) ;
    boolean save(ActivityParty et) ;
    void saveBatch(List<ActivityParty> list) ;
    Page<ActivityParty> searchDefault(ActivityPartySearchContext context) ;
    List<ActivityParty> selectByActivityid(String activityid) ;
    void removeByActivityid(String activityid) ;
    List<ActivityParty> selectByResourcespecid(String resourcespecid) ;
    void removeByResourcespecid(String resourcespecid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<ActivityParty> getActivitypartyByIds(List<String> ids) ;
    List<ActivityParty> getActivitypartyByEntities(List<ActivityParty> entities) ;
}



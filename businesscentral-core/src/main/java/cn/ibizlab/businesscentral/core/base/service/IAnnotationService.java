package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.Annotation;
import cn.ibizlab.businesscentral.core.base.filter.AnnotationSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Annotation] 服务对象接口
 */
public interface IAnnotationService extends IService<Annotation>{

    boolean create(Annotation et) ;
    void createBatch(List<Annotation> list) ;
    boolean update(Annotation et) ;
    void updateBatch(List<Annotation> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Annotation get(String key) ;
    Annotation getDraft(Annotation et) ;
    boolean checkKey(Annotation et) ;
    boolean save(Annotation et) ;
    void saveBatch(List<Annotation> list) ;
    Page<Annotation> searchDefault(AnnotationSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Annotation> getAnnotationByIds(List<String> ids) ;
    List<Annotation> getAnnotationByEntities(List<Annotation> entities) ;
}



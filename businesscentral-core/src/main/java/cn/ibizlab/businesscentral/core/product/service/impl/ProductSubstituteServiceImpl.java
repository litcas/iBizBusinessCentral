package cn.ibizlab.businesscentral.core.product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.product.domain.ProductSubstitute;
import cn.ibizlab.businesscentral.core.product.filter.ProductSubstituteSearchContext;
import cn.ibizlab.businesscentral.core.product.service.IProductSubstituteService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.product.mapper.ProductSubstituteMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[产品替换] 服务对象接口实现
 */
@Slf4j
@Service("ProductSubstituteServiceImpl")
public class ProductSubstituteServiceImpl extends ServiceImpl<ProductSubstituteMapper, ProductSubstitute> implements IProductSubstituteService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.product.service.IProductService productService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(ProductSubstitute et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getProductsubstituteid()),et);
        return true;
    }

    @Override
    public void createBatch(List<ProductSubstitute> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(ProductSubstitute et) {
        fillParentData(et);
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("productsubstituteid",et.getProductsubstituteid())))
            return false;
        CachedBeanCopier.copy(get(et.getProductsubstituteid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<ProductSubstitute> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public ProductSubstitute get(String key) {
        ProductSubstitute et = getById(key);
        if(et==null){
            et=new ProductSubstitute();
            et.setProductsubstituteid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public ProductSubstitute getDraft(ProductSubstitute et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(ProductSubstitute et) {
        return (!ObjectUtils.isEmpty(et.getProductsubstituteid()))&&(!Objects.isNull(this.getById(et.getProductsubstituteid())));
    }
    @Override
    @Transactional
    public boolean save(ProductSubstitute et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(ProductSubstitute et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<ProductSubstitute> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<ProductSubstitute> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<ProductSubstitute> selectByProductid(String productid) {
        return baseMapper.selectByProductid(productid);
    }

    @Override
    public void removeByProductid(String productid) {
        this.remove(new QueryWrapper<ProductSubstitute>().eq("productid",productid));
    }

	@Override
    public List<ProductSubstitute> selectBySubstitutedproductid(String productid) {
        return baseMapper.selectBySubstitutedproductid(productid);
    }

    @Override
    public void removeBySubstitutedproductid(String productid) {
        this.remove(new QueryWrapper<ProductSubstitute>().eq("substitutedproductid",productid));
    }

	@Override
    public List<ProductSubstitute> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<ProductSubstitute>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<ProductSubstitute> searchDefault(ProductSubstituteSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<ProductSubstitute> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<ProductSubstitute>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(ProductSubstitute et){
        //实体关系[DER1N_PRODUCTSUBSTITUTE__PRODUCT__PRODUCTID]
        if(!ObjectUtils.isEmpty(et.getProductid())){
            cn.ibizlab.businesscentral.core.product.domain.Product product=et.getProduct();
            if(ObjectUtils.isEmpty(product)){
                cn.ibizlab.businesscentral.core.product.domain.Product majorEntity=productService.get(et.getProductid());
                et.setProduct(majorEntity);
                product=majorEntity;
            }
            et.setProductname(product.getProductname());
        }
        //实体关系[DER1N_PRODUCTSUBSTITUTE__PRODUCT__SUBSTITUTEDPRODUCTID]
        if(!ObjectUtils.isEmpty(et.getSubstitutedproductid())){
            cn.ibizlab.businesscentral.core.product.domain.Product substitutedproduct=et.getSubstitutedproduct();
            if(ObjectUtils.isEmpty(substitutedproduct)){
                cn.ibizlab.businesscentral.core.product.domain.Product majorEntity=productService.get(et.getSubstitutedproductid());
                et.setSubstitutedproduct(majorEntity);
                substitutedproduct=majorEntity;
            }
            et.setSubstitutedproductname(substitutedproduct.getProductname());
        }
        //实体关系[DER1N_PRODUCTSUBSTITUTE__TRANSACTIONCURRENCY__TRANSACTIONCURRENCYID]
        if(!ObjectUtils.isEmpty(et.getTransactioncurrencyid())){
            cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency=et.getTransactioncurrency();
            if(ObjectUtils.isEmpty(transactioncurrency)){
                cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency majorEntity=transactioncurrencyService.get(et.getTransactioncurrencyid());
                et.setTransactioncurrency(majorEntity);
                transactioncurrency=majorEntity;
            }
            et.setCurrencyname(transactioncurrency.getCurrencyname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<ProductSubstitute> getProductsubstituteByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<ProductSubstitute> getProductsubstituteByEntities(List<ProductSubstitute> entities) {
        List ids =new ArrayList();
        for(ProductSubstitute entity : entities){
            Serializable id=entity.getProductsubstituteid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




package cn.ibizlab.businesscentral.core.base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.base.domain.SlaKpiInstance;
/**
 * 关系型数据实体[SlaKpiInstance] 查询条件对象
 */
@Slf4j
@Data
public class SlaKpiInstanceSearchContext extends QueryWrapperContext<SlaKpiInstance> {

	private String n_status_eq;//[状态]
	public void setN_status_eq(String n_status_eq) {
        this.n_status_eq = n_status_eq;
        if(!ObjectUtils.isEmpty(this.n_status_eq)){
            this.getSearchCond().eq("status", n_status_eq);
        }
    }
	private String n_slakpiinstancename_like;//[服务协议KPI实例名称]
	public void setN_slakpiinstancename_like(String n_slakpiinstancename_like) {
        this.n_slakpiinstancename_like = n_slakpiinstancename_like;
        if(!ObjectUtils.isEmpty(this.n_slakpiinstancename_like)){
            this.getSearchCond().like("slakpiinstancename", n_slakpiinstancename_like);
        }
    }
	private String n_warningtimereached_eq;//[已达到警告时间]
	public void setN_warningtimereached_eq(String n_warningtimereached_eq) {
        this.n_warningtimereached_eq = n_warningtimereached_eq;
        if(!ObjectUtils.isEmpty(this.n_warningtimereached_eq)){
            this.getSearchCond().eq("warningtimereached", n_warningtimereached_eq);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }
	private String n_owningbusinessunit_eq;//[负责的业务部门]
	public void setN_owningbusinessunit_eq(String n_owningbusinessunit_eq) {
        this.n_owningbusinessunit_eq = n_owningbusinessunit_eq;
        if(!ObjectUtils.isEmpty(this.n_owningbusinessunit_eq)){
            this.getSearchCond().eq("owningbusinessunit", n_owningbusinessunit_eq);
        }
    }
	private String n_regarding_eq;//[关于]
	public void setN_regarding_eq(String n_regarding_eq) {
        this.n_regarding_eq = n_regarding_eq;
        if(!ObjectUtils.isEmpty(this.n_regarding_eq)){
            this.getSearchCond().eq("regarding", n_regarding_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("slakpiinstancename", query)   
            );
		 }
	}
}




package cn.ibizlab.businesscentral.core.runtime.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.runtime.domain.QueueItem;
import cn.ibizlab.businesscentral.core.runtime.filter.QueueItemSearchContext;
import cn.ibizlab.businesscentral.core.runtime.service.IQueueItemService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.runtime.mapper.QueueItemMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[队列项] 服务对象接口实现
 */
@Slf4j
@Service("QueueItemServiceImpl")
public class QueueItemServiceImpl extends ServiceImpl<QueueItemMapper, QueueItem> implements IQueueItemService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.runtime.service.IQueueService queueService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(QueueItem et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getQueueitemid()),et);
        return true;
    }

    @Override
    public void createBatch(List<QueueItem> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(QueueItem et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("queueitemid",et.getQueueitemid())))
            return false;
        CachedBeanCopier.copy(get(et.getQueueitemid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<QueueItem> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public QueueItem get(String key) {
        QueueItem et = getById(key);
        if(et==null){
            et=new QueueItem();
            et.setQueueitemid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public QueueItem getDraft(QueueItem et) {
        return et;
    }

    @Override
    public boolean checkKey(QueueItem et) {
        return (!ObjectUtils.isEmpty(et.getQueueitemid()))&&(!Objects.isNull(this.getById(et.getQueueitemid())));
    }
    @Override
    @Transactional
    public boolean save(QueueItem et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(QueueItem et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<QueueItem> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<QueueItem> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<QueueItem> selectByQueueid(String queueid) {
        return baseMapper.selectByQueueid(queueid);
    }

    @Override
    public void removeByQueueid(String queueid) {
        this.remove(new QueryWrapper<QueueItem>().eq("queueid",queueid));
    }

	@Override
    public List<QueueItem> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<QueueItem>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<QueueItem> searchDefault(QueueItemSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<QueueItem> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<QueueItem>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<QueueItem> getQueueitemByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<QueueItem> getQueueitemByEntities(List<QueueItem> entities) {
        List ids =new ArrayList();
        for(QueueItem entity : entities){
            Serializable id=entity.getQueueitemid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




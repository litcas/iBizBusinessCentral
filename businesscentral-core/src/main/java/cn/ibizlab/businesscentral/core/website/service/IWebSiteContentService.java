package cn.ibizlab.businesscentral.core.website.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.website.domain.WebSiteContent;
import cn.ibizlab.businesscentral.core.website.filter.WebSiteContentSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[WebSiteContent] 服务对象接口
 */
public interface IWebSiteContentService extends IService<WebSiteContent>{

    boolean create(WebSiteContent et) ;
    void createBatch(List<WebSiteContent> list) ;
    boolean update(WebSiteContent et) ;
    void updateBatch(List<WebSiteContent> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    WebSiteContent get(String key) ;
    WebSiteContent getDraft(WebSiteContent et) ;
    boolean checkKey(WebSiteContent et) ;
    boolean save(WebSiteContent et) ;
    void saveBatch(List<WebSiteContent> list) ;
    Page<WebSiteContent> searchDefault(WebSiteContentSearchContext context) ;
    List<WebSiteContent> selectByWebsitechannelid(String websitechannelid) ;
    void removeByWebsitechannelid(String websitechannelid) ;
    List<WebSiteContent> selectByWebsiteid(String websiteid) ;
    void removeByWebsiteid(String websiteid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<WebSiteContent> getWebsitecontentByIds(List<String> ids) ;
    List<WebSiteContent> getWebsitecontentByEntities(List<WebSiteContent> entities) ;
}



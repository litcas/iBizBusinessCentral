package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.KnowledgeBaseRecord;
import cn.ibizlab.businesscentral.core.base.filter.KnowledgeBaseRecordSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[KnowledgeBaseRecord] 服务对象接口
 */
public interface IKnowledgeBaseRecordService extends IService<KnowledgeBaseRecord>{

    boolean create(KnowledgeBaseRecord et) ;
    void createBatch(List<KnowledgeBaseRecord> list) ;
    boolean update(KnowledgeBaseRecord et) ;
    void updateBatch(List<KnowledgeBaseRecord> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    KnowledgeBaseRecord get(String key) ;
    KnowledgeBaseRecord getDraft(KnowledgeBaseRecord et) ;
    boolean checkKey(KnowledgeBaseRecord et) ;
    boolean save(KnowledgeBaseRecord et) ;
    void saveBatch(List<KnowledgeBaseRecord> list) ;
    Page<KnowledgeBaseRecord> searchDefault(KnowledgeBaseRecordSearchContext context) ;
    List<KnowledgeBaseRecord> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<KnowledgeBaseRecord> getKnowledgebaserecordByIds(List<String> ids) ;
    List<KnowledgeBaseRecord> getKnowledgebaserecordByEntities(List<KnowledgeBaseRecord> entities) ;
}



package cn.ibizlab.businesscentral.core.extensions.service;

import cn.ibizlab.businesscentral.core.sales.service.impl.QuoteServiceImpl;
import lombok.extern.slf4j.Slf4j;
import cn.ibizlab.businesscentral.core.sales.domain.Quote;
import org.springframework.stereotype.Service;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Primary;
import java.util.*;

/**
 * 实体[报价单] 自定义服务对象
 */
@Slf4j
@Primary
@Service("QuoteExService")
public class QuoteExService extends QuoteServiceImpl {

    @Override
    protected Class currentModelClass() {
        return com.baomidou.mybatisplus.core.toolkit.ReflectionKit.getSuperClassGenericType(this.getClass().getSuperclass(), 1);
    }

    /**
     * 自定义行为[GenSalesOrder]用户扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public Quote genSalesOrder(Quote et) {
        return super.genSalesOrder(et);
    }
}


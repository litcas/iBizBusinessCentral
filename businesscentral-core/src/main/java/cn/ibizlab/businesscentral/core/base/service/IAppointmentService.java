package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.Appointment;
import cn.ibizlab.businesscentral.core.base.filter.AppointmentSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Appointment] 服务对象接口
 */
public interface IAppointmentService extends IService<Appointment>{

    boolean create(Appointment et) ;
    void createBatch(List<Appointment> list) ;
    boolean update(Appointment et) ;
    void updateBatch(List<Appointment> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Appointment get(String key) ;
    Appointment getDraft(Appointment et) ;
    boolean checkKey(Appointment et) ;
    boolean save(Appointment et) ;
    void saveBatch(List<Appointment> list) ;
    Page<Appointment> searchDefault(AppointmentSearchContext context) ;
    List<Appointment> selectByServiceid(String serviceid) ;
    void removeByServiceid(String serviceid) ;
    List<Appointment> selectBySlaid(String slaid) ;
    void removeBySlaid(String slaid) ;
    List<Appointment> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Appointment> getAppointmentByIds(List<String> ids) ;
    List<Appointment> getAppointmentByEntities(List<Appointment> entities) ;
}



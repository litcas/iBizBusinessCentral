package cn.ibizlab.businesscentral.core.sales.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[商机]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "OPPORTUNITY",resultMap = "OpportunityResultMap")
public class Opportunity extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 决策者?
     */
    @DEField(defaultValue = "0")
    @TableField(value = "decisionmaker")
    @JSONField(name = "decisionmaker")
    @JsonProperty("decisionmaker")
    private Integer decisionmaker;
    /**
     * 明细金额总计
     */
    @TableField(value = "totallineitemamount")
    @JSONField(name = "totallineitemamount")
    @JsonProperty("totallineitemamount")
    private BigDecimal totallineitemamount;
    /**
     * Process Id
     */
    @TableField(value = "processid")
    @JSONField(name = "processid")
    @JsonProperty("processid")
    private String processid;
    /**
     * 商机折扣(%)
     */
    @TableField(value = "discountpercentage")
    @JSONField(name = "discountpercentage")
    @JsonProperty("discountpercentage")
    private BigDecimal discountpercentage;
    /**
     * 负责人类型
     */
    @TableField(value = "ownertype")
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;
    /**
     * 定价错误
     */
    @TableField(value = "pricingerrorcode")
    @JSONField(name = "pricingerrorcode")
    @JsonProperty("pricingerrorcode")
    private String pricingerrorcode;
    /**
     * 总金额
     */
    @TableField(value = "totalamount")
    @JSONField(name = "totalamount")
    @JsonProperty("totalamount")
    private BigDecimal totalamount;
    /**
     * 总税款 (Base)
     */
    @DEField(name = "totaltax_base")
    @TableField(value = "totaltax_base")
    @JSONField(name = "totaltax_base")
    @JsonProperty("totaltax_base")
    private BigDecimal totaltaxBase;
    /**
     * 总税款
     */
    @TableField(value = "totaltax")
    @JSONField(name = "totaltax")
    @JsonProperty("totaltax")
    private BigDecimal totaltax;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 预计收入 (Base)
     */
    @DEField(name = "estimatedvalue_base")
    @TableField(value = "estimatedvalue_base")
    @JSONField(name = "estimatedvalue_base")
    @JsonProperty("estimatedvalue_base")
    private BigDecimal estimatedvalueBase;
    /**
     * 负责人
     */
    @TableField(value = "ownername")
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;
    /**
     * 提供最终建议
     */
    @DEField(defaultValue = "0")
    @TableField(value = "presentfinalproposal")
    @JSONField(name = "presentfinalproposal")
    @JsonProperty("presentfinalproposal")
    private Integer presentfinalproposal;
    /**
     * 制定建议
     */
    @DEField(defaultValue = "0")
    @TableField(value = "developproposal")
    @JSONField(name = "developproposal")
    @JsonProperty("developproposal")
    private Integer developproposal;
    /**
     * 等级
     */
    @TableField(value = "opportunityratingcode")
    @JSONField(name = "opportunityratingcode")
    @JsonProperty("opportunityratingcode")
    private String opportunityratingcode;
    /**
     * 总金额 (Base)
     */
    @DEField(name = "totalamount_base")
    @TableField(value = "totalamount_base")
    @JSONField(name = "totalamount_base")
    @JsonProperty("totalamount_base")
    private BigDecimal totalamountBase;
    /**
     * 折后金额总计
     */
    @TableField(value = "totalamountlessfreight")
    @JSONField(name = "totalamountlessfreight")
    @JsonProperty("totalamountlessfreight")
    private BigDecimal totalamountlessfreight;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 识别竞争对手
     */
    @DEField(defaultValue = "0")
    @TableField(value = "identifycompetitors")
    @JSONField(name = "identifycompetitors")
    @JsonProperty("identifycompetitors")
    private Integer identifycompetitors;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 发送致谢信
     */
    @DEField(defaultValue = "0")
    @TableField(value = "sendthankyounote")
    @JSONField(name = "sendthankyounote")
    @JsonProperty("sendthankyounote")
    private Integer sendthankyounote;
    /**
     * 折扣金额总和 (Base)
     */
    @DEField(name = "totaldiscountamount_base")
    @TableField(value = "totaldiscountamount_base")
    @JSONField(name = "totaldiscountamount_base")
    @JsonProperty("totaldiscountamount_base")
    private BigDecimal totaldiscountamountBase;
    /**
     * 明细项目折扣金额总和
     */
    @TableField(value = "totallineitemdiscountamount")
    @JSONField(name = "totallineitemdiscountamount")
    @JsonProperty("totallineitemdiscountamount")
    private BigDecimal totallineitemdiscountamount;
    /**
     * 采购程序
     */
    @TableField(value = "purchaseprocess")
    @JSONField(name = "purchaseprocess")
    @JsonProperty("purchaseprocess")
    private String purchaseprocess;
    /**
     * Traversed Path
     */
    @TableField(value = "traversedpath")
    @JSONField(name = "traversedpath")
    @JsonProperty("traversedpath")
    private String traversedpath;
    /**
     * 已收集建议反馈
     */
    @DEField(defaultValue = "0")
    @TableField(value = "captureproposalfeedback")
    @JSONField(name = "captureproposalfeedback")
    @JsonProperty("captureproposalfeedback")
    private Integer captureproposalfeedback;
    /**
     * 最终决策日期
     */
    @TableField(value = "finaldecisiondate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "finaldecisiondate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("finaldecisiondate")
    private Timestamp finaldecisiondate;
    /**
     * 步骤
     */
    @TableField(value = "stepid")
    @JSONField(name = "stepid")
    @JsonProperty("stepid")
    private String stepid;
    /**
     * 反馈已解决
     */
    @DEField(defaultValue = "0")
    @TableField(value = "resolvefeedback")
    @JSONField(name = "resolvefeedback")
    @JsonProperty("resolvefeedback")
    private Integer resolvefeedback;
    /**
     * 预算金额 (Base)
     */
    @DEField(name = "budgetamount_base")
    @TableField(value = "budgetamount_base")
    @JSONField(name = "budgetamount_base")
    @JsonProperty("budgetamount_base")
    private BigDecimal budgetamountBase;
    /**
     * 决定进行/不进行
     */
    @DEField(defaultValue = "0")
    @TableField(value = "pursuitdecision")
    @JSONField(name = "pursuitdecision")
    @JsonProperty("pursuitdecision")
    private Integer pursuitdecision;
    /**
     * 收入
     */
    @DEField(defaultValue = "0")
    @TableField(value = "revenuesystemcalculated")
    @JSONField(name = "revenuesystemcalculated")
    @JsonProperty("revenuesystemcalculated")
    private Integer revenuesystemcalculated;
    /**
     * 安排建议会议
     */
    @TableField(value = "scheduleproposalmeeting")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "scheduleproposalmeeting" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("scheduleproposalmeeting")
    private Timestamp scheduleproposalmeeting;
    /**
     * 潜在客户类型
     */
    @TableField(value = "customertype")
    @JSONField(name = "customertype")
    @JsonProperty("customertype")
    private String customertype;
    /**
     * 可能性
     */
    @TableField(value = "closeprobability")
    @JSONField(name = "closeprobability")
    @JsonProperty("closeprobability")
    private Integer closeprobability;
    /**
     * UTC Conversion Time Zone Code
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * 已提供建议
     */
    @DEField(defaultValue = "0")
    @TableField(value = "presentproposal")
    @JSONField(name = "presentproposal")
    @JsonProperty("presentproposal")
    private Integer presentproposal;
    /**
     * 已安排跟进(潜在客户)
     */
    @DEField(name = "schedulefollowup_prospect")
    @TableField(value = "schedulefollowup_prospect")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "schedulefollowup_prospect" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("schedulefollowup_prospect")
    private Timestamp schedulefollowupProspect;
    /**
     * 状态描述
     */
    @DEField(defaultValue = "1")
    @TableField(value = "statuscode")
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;
    /**
     * 实际截止日期
     */
    @TableField(value = "actualclosedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "actualclosedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("actualclosedate")
    private Timestamp actualclosedate;
    /**
     * 确认有兴趣
     */
    @DEField(defaultValue = "0")
    @TableField(value = "confirminterest")
    @JSONField(name = "confirminterest")
    @JsonProperty("confirminterest")
    private Integer confirminterest;
    /**
     * 潜在客户
     */
    @TableField(value = "customerid")
    @JSONField(name = "customerid")
    @JsonProperty("customerid")
    private String customerid;
    /**
     * 销售阶段
     */
    @TableField(value = "salesstage")
    @JSONField(name = "salesstage")
    @JsonProperty("salesstage")
    private String salesstage;
    /**
     * 商机
     */
    @DEField(isKeyField=true)
    @TableId(value= "opportunityid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "opportunityid")
    @JsonProperty("opportunityid")
    private String opportunityid;
    /**
     * 暂候时间(分钟)
     */
    @TableField(value = "onholdtime")
    @JSONField(name = "onholdtime")
    @JsonProperty("onholdtime")
    private Integer onholdtime;
    /**
     * 资格注释
     */
    @TableField(value = "qualificationcomments")
    @JSONField(name = "qualificationcomments")
    @JsonProperty("qualificationcomments")
    private String qualificationcomments;
    /**
     * 已安排跟进(授予资格)
     */
    @DEField(name = "schedulefollowup_qualify")
    @TableField(value = "schedulefollowup_qualify")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "schedulefollowup_qualify" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("schedulefollowup_qualify")
    private Timestamp schedulefollowupQualify;
    /**
     * 参与工作流
     */
    @DEField(defaultValue = "0")
    @TableField(value = "participatesinworkflow")
    @JSONField(name = "participatesinworkflow")
    @JsonProperty("participatesinworkflow")
    private Integer participatesinworkflow;
    /**
     * 汇率
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * 准备阶段
     */
    @TableField(value = "stepname")
    @JSONField(name = "stepname")
    @JsonProperty("stepname")
    private String stepname;
    /**
     * 客户需求
     */
    @TableField(value = "customerneed")
    @JSONField(name = "customerneed")
    @JsonProperty("customerneed")
    private String customerneed;
    /**
     * Import Sequence Number
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * 完成内部审核
     */
    @DEField(defaultValue = "0")
    @TableField(value = "completeinternalreview")
    @JSONField(name = "completeinternalreview")
    @JsonProperty("completeinternalreview")
    private Integer completeinternalreview;
    /**
     * 评估相符程度
     */
    @DEField(defaultValue = "0")
    @TableField(value = "evaluatefit")
    @JSONField(name = "evaluatefit")
    @JsonProperty("evaluatefit")
    private Integer evaluatefit;
    /**
     * 处理代码
     */
    @TableField(value = "salesstagecode")
    @JSONField(name = "salesstagecode")
    @JsonProperty("salesstagecode")
    private String salesstagecode;
    /**
     * 识别销售团队
     */
    @DEField(defaultValue = "0")
    @TableField(value = "identifypursuitteam")
    @JSONField(name = "identifypursuitteam")
    @JsonProperty("identifypursuitteam")
    private Integer identifypursuitteam;
    /**
     * 初始通信
     */
    @TableField(value = "initialcommunication")
    @JSONField(name = "initialcommunication")
    @JsonProperty("initialcommunication")
    private String initialcommunication;
    /**
     * Version Number
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 运费金额 (Base)
     */
    @DEField(name = "freightamount_base")
    @TableField(value = "freightamount_base")
    @JSONField(name = "freightamount_base")
    @JsonProperty("freightamount_base")
    private BigDecimal freightamountBase;
    /**
     * 客户
     */
    @TableField(value = "accountname")
    @JSONField(name = "accountname")
    @JsonProperty("accountname")
    private String accountname;
    /**
     * TeamsFollowed
     */
    @TableField(value = "teamsfollowed")
    @JSONField(name = "teamsfollowed")
    @JsonProperty("teamsfollowed")
    private Integer teamsfollowed;
    /**
     * 已拟定解决方案
     */
    @TableField(value = "proposedsolution")
    @JSONField(name = "proposedsolution")
    @JsonProperty("proposedsolution")
    private String proposedsolution;
    /**
     * 归档任务报告
     */
    @DEField(defaultValue = "0")
    @TableField(value = "filedebrief")
    @JSONField(name = "filedebrief")
    @JsonProperty("filedebrief")
    private Integer filedebrief;
    /**
     * 预算金额
     */
    @TableField(value = "budgetamount")
    @JSONField(name = "budgetamount")
    @JsonProperty("budgetamount")
    private BigDecimal budgetamount;
    /**
     * 预算
     */
    @TableField(value = "budgetstatus")
    @JSONField(name = "budgetstatus")
    @JsonProperty("budgetstatus")
    private String budgetstatus;
    /**
     * 商机折扣额 (Base)
     */
    @DEField(name = "discountamount_base")
    @TableField(value = "discountamount_base")
    @JSONField(name = "discountamount_base")
    @JsonProperty("discountamount_base")
    private BigDecimal discountamountBase;
    /**
     * 最终建议已就绪
     */
    @DEField(defaultValue = "0")
    @TableField(value = "completefinalproposal")
    @JSONField(name = "completefinalproposal")
    @JsonProperty("completefinalproposal")
    private Integer completefinalproposal;
    /**
     * 商机折扣额
     */
    @TableField(value = "discountamount")
    @JSONField(name = "discountamount")
    @JsonProperty("discountamount")
    private BigDecimal discountamount;
    /**
     * 需求
     */
    @TableField(value = "need")
    @JSONField(name = "need")
    @JsonProperty("need")
    private String need;
    /**
     * Stage Id
     */
    @TableField(value = "stageid")
    @JSONField(name = "stageid")
    @JsonProperty("stageid")
    private String stageid;
    /**
     * 运费金额
     */
    @TableField(value = "freightamount")
    @JSONField(name = "freightamount")
    @JsonProperty("freightamount")
    private BigDecimal freightamount;
    /**
     * 实际收入
     */
    @TableField(value = "actualvalue")
    @JSONField(name = "actualvalue")
    @JsonProperty("actualvalue")
    private BigDecimal actualvalue;
    /**
     * 是否私有
     */
    @DEField(defaultValue = "0")
    @TableField(value = "private")
    @JSONField(name = "ibizprivate")
    @JsonProperty("ibizprivate")
    private Integer ibizprivate;
    /**
     * Time Zone Rule Version Number
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 优先级
     */
    @TableField(value = "prioritycode")
    @JSONField(name = "prioritycode")
    @JsonProperty("prioritycode")
    private String prioritycode;
    /**
     * 明细金额总计 (Base)
     */
    @DEField(name = "totallineitemamount_base")
    @TableField(value = "totallineitemamount_base")
    @JSONField(name = "totallineitemamount_base")
    @JsonProperty("totallineitemamount_base")
    private BigDecimal totallineitemamountBase;
    /**
     * 客户
     */
    @TableField(value = "customername")
    @JSONField(name = "customername")
    @JsonProperty("customername")
    private String customername;
    /**
     * 预计结束日期
     */
    @TableField(value = "estimatedclosedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "estimatedclosedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("estimatedclosedate")
    private Timestamp estimatedclosedate;
    /**
     * 当前状况
     */
    @TableField(value = "currentsituation")
    @JSONField(name = "currentsituation")
    @JsonProperty("currentsituation")
    private String currentsituation;
    /**
     * 实际收入 (Base)
     */
    @DEField(name = "actualvalue_base")
    @TableField(value = "actualvalue_base")
    @JSONField(name = "actualvalue_base")
    @JsonProperty("actualvalue_base")
    private BigDecimal actualvalueBase;
    /**
     * 客户难点
     */
    @TableField(value = "customerpainpoints")
    @JSONField(name = "customerpainpoints")
    @JsonProperty("customerpainpoints")
    private String customerpainpoints;
    /**
     * 折扣金额总和
     */
    @TableField(value = "totaldiscountamount")
    @JSONField(name = "totaldiscountamount")
    @JsonProperty("totaldiscountamount")
    private BigDecimal totaldiscountamount;
    /**
     * 商机名称
     */
    @TableField(value = "opportunityname")
    @JSONField(name = "opportunityname")
    @JsonProperty("opportunityname")
    private String opportunityname;
    /**
     * 识别客户联系人
     */
    @DEField(defaultValue = "0")
    @TableField(value = "identifycustomercontacts")
    @JSONField(name = "identifycustomercontacts")
    @JsonProperty("identifycustomercontacts")
    private Integer identifycustomercontacts;
    /**
     * 日程表
     */
    @TableField(value = "timeline")
    @JSONField(name = "timeline")
    @JsonProperty("timeline")
    private String timeline;
    /**
     * 联系人
     */
    @TableField(value = "contactname")
    @JSONField(name = "contactname")
    @JsonProperty("contactname")
    private String contactname;
    /**
     * 上一暂候时间
     */
    @TableField(value = "lastonholdtime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "lastonholdtime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("lastonholdtime")
    private Timestamp lastonholdtime;
    /**
     * 折后金额总计 (Base)
     */
    @DEField(name = "totalamountlessfreight_base")
    @TableField(value = "totalamountlessfreight_base")
    @JSONField(name = "totalamountlessfreight_base")
    @JsonProperty("totalamountlessfreight_base")
    private BigDecimal totalamountlessfreightBase;
    /**
     * 预计收入
     */
    @TableField(value = "estimatedvalue")
    @JSONField(name = "estimatedvalue")
    @JsonProperty("estimatedvalue")
    private BigDecimal estimatedvalue;
    /**
     * Email Address
     */
    @TableField(value = "emailaddress")
    @JSONField(name = "emailaddress")
    @JsonProperty("emailaddress")
    private String emailaddress;
    /**
     * 状态
     */
    @DEField(defaultValue = "0")
    @TableField(value = "statecode")
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;
    /**
     * 购买时间范围
     */
    @TableField(value = "purchasetimeframe")
    @JSONField(name = "purchasetimeframe")
    @JsonProperty("purchasetimeframe")
    private String purchasetimeframe;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 报价单注释
     */
    @TableField(value = "quotecomments")
    @JSONField(name = "quotecomments")
    @JsonProperty("quotecomments")
    private String quotecomments;
    /**
     * Record Created On
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 负责人
     */
    @TableField(value = "ownerid")
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;
    /**
     * 帐户
     */
    @TableField(value = "parentaccountname")
    @JSONField(name = "parentaccountname")
    @JsonProperty("parentaccountname")
    private String parentaccountname;
    /**
     * 原始潜在顾客
     */
    @TableField(value = "originatingleadname")
    @JSONField(name = "originatingleadname")
    @JsonProperty("originatingleadname")
    private String originatingleadname;
    /**
     * 联系人
     */
    @TableField(value = "parentcontactname")
    @JSONField(name = "parentcontactname")
    @JsonProperty("parentcontactname")
    private String parentcontactname;
    /**
     * 货币
     */
    @TableField(value = "currencyname")
    @JSONField(name = "currencyname")
    @JsonProperty("currencyname")
    private String currencyname;
    /**
     * 价目表
     */
    @TableField(value = "pricelevelname")
    @JSONField(name = "pricelevelname")
    @JsonProperty("pricelevelname")
    private String pricelevelname;
    /**
     * SLA
     */
    @TableField(value = "slaname")
    @JSONField(name = "slaname")
    @JsonProperty("slaname")
    private String slaname;
    /**
     * 源市场活动
     */
    @TableField(value = "campaignname")
    @JSONField(name = "campaignname")
    @JsonProperty("campaignname")
    private String campaignname;
    /**
     * 联系人
     */
    @TableField(value = "parentcontactid")
    @JSONField(name = "parentcontactid")
    @JsonProperty("parentcontactid")
    private String parentcontactid;
    /**
     * 源市场活动
     */
    @TableField(value = "campaignid")
    @JSONField(name = "campaignid")
    @JsonProperty("campaignid")
    private String campaignid;
    /**
     * 货币
     */
    @TableField(value = "transactioncurrencyid")
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;
    /**
     * 帐户
     */
    @TableField(value = "parentaccountid")
    @JSONField(name = "parentaccountid")
    @JsonProperty("parentaccountid")
    private String parentaccountid;
    /**
     * 价目表
     */
    @TableField(value = "pricelevelid")
    @JSONField(name = "pricelevelid")
    @JsonProperty("pricelevelid")
    private String pricelevelid;
    /**
     * 原始潜在顾客
     */
    @TableField(value = "originatingleadid")
    @JSONField(name = "originatingleadid")
    @JsonProperty("originatingleadid")
    private String originatingleadid;
    /**
     * SLA
     */
    @TableField(value = "slaid")
    @JSONField(name = "slaid")
    @JsonProperty("slaid")
    private String slaid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Account parentaccount;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.marketing.domain.Campaign campaign;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Contact parentcontact;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.sales.domain.Lead originatinglead;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.product.domain.PriceLevel pricelevel;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Sla sla;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency;



    /**
     * 设置 [决策者?]
     */
    public void setDecisionmaker(Integer decisionmaker){
        this.decisionmaker = decisionmaker ;
        this.modify("decisionmaker",decisionmaker);
    }

    /**
     * 设置 [明细金额总计]
     */
    public void setTotallineitemamount(BigDecimal totallineitemamount){
        this.totallineitemamount = totallineitemamount ;
        this.modify("totallineitemamount",totallineitemamount);
    }

    /**
     * 设置 [Process Id]
     */
    public void setProcessid(String processid){
        this.processid = processid ;
        this.modify("processid",processid);
    }

    /**
     * 设置 [商机折扣(%)]
     */
    public void setDiscountpercentage(BigDecimal discountpercentage){
        this.discountpercentage = discountpercentage ;
        this.modify("discountpercentage",discountpercentage);
    }

    /**
     * 设置 [负责人类型]
     */
    public void setOwnertype(String ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [定价错误]
     */
    public void setPricingerrorcode(String pricingerrorcode){
        this.pricingerrorcode = pricingerrorcode ;
        this.modify("pricingerrorcode",pricingerrorcode);
    }

    /**
     * 设置 [总金额]
     */
    public void setTotalamount(BigDecimal totalamount){
        this.totalamount = totalamount ;
        this.modify("totalamount",totalamount);
    }

    /**
     * 设置 [总税款 (Base)]
     */
    public void setTotaltaxBase(BigDecimal totaltaxBase){
        this.totaltaxBase = totaltaxBase ;
        this.modify("totaltax_base",totaltaxBase);
    }

    /**
     * 设置 [总税款]
     */
    public void setTotaltax(BigDecimal totaltax){
        this.totaltax = totaltax ;
        this.modify("totaltax",totaltax);
    }

    /**
     * 设置 [预计收入 (Base)]
     */
    public void setEstimatedvalueBase(BigDecimal estimatedvalueBase){
        this.estimatedvalueBase = estimatedvalueBase ;
        this.modify("estimatedvalue_base",estimatedvalueBase);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnername(String ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [提供最终建议]
     */
    public void setPresentfinalproposal(Integer presentfinalproposal){
        this.presentfinalproposal = presentfinalproposal ;
        this.modify("presentfinalproposal",presentfinalproposal);
    }

    /**
     * 设置 [制定建议]
     */
    public void setDevelopproposal(Integer developproposal){
        this.developproposal = developproposal ;
        this.modify("developproposal",developproposal);
    }

    /**
     * 设置 [等级]
     */
    public void setOpportunityratingcode(String opportunityratingcode){
        this.opportunityratingcode = opportunityratingcode ;
        this.modify("opportunityratingcode",opportunityratingcode);
    }

    /**
     * 设置 [总金额 (Base)]
     */
    public void setTotalamountBase(BigDecimal totalamountBase){
        this.totalamountBase = totalamountBase ;
        this.modify("totalamount_base",totalamountBase);
    }

    /**
     * 设置 [折后金额总计]
     */
    public void setTotalamountlessfreight(BigDecimal totalamountlessfreight){
        this.totalamountlessfreight = totalamountlessfreight ;
        this.modify("totalamountlessfreight",totalamountlessfreight);
    }

    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [识别竞争对手]
     */
    public void setIdentifycompetitors(Integer identifycompetitors){
        this.identifycompetitors = identifycompetitors ;
        this.modify("identifycompetitors",identifycompetitors);
    }

    /**
     * 设置 [发送致谢信]
     */
    public void setSendthankyounote(Integer sendthankyounote){
        this.sendthankyounote = sendthankyounote ;
        this.modify("sendthankyounote",sendthankyounote);
    }

    /**
     * 设置 [折扣金额总和 (Base)]
     */
    public void setTotaldiscountamountBase(BigDecimal totaldiscountamountBase){
        this.totaldiscountamountBase = totaldiscountamountBase ;
        this.modify("totaldiscountamount_base",totaldiscountamountBase);
    }

    /**
     * 设置 [明细项目折扣金额总和]
     */
    public void setTotallineitemdiscountamount(BigDecimal totallineitemdiscountamount){
        this.totallineitemdiscountamount = totallineitemdiscountamount ;
        this.modify("totallineitemdiscountamount",totallineitemdiscountamount);
    }

    /**
     * 设置 [采购程序]
     */
    public void setPurchaseprocess(String purchaseprocess){
        this.purchaseprocess = purchaseprocess ;
        this.modify("purchaseprocess",purchaseprocess);
    }

    /**
     * 设置 [Traversed Path]
     */
    public void setTraversedpath(String traversedpath){
        this.traversedpath = traversedpath ;
        this.modify("traversedpath",traversedpath);
    }

    /**
     * 设置 [已收集建议反馈]
     */
    public void setCaptureproposalfeedback(Integer captureproposalfeedback){
        this.captureproposalfeedback = captureproposalfeedback ;
        this.modify("captureproposalfeedback",captureproposalfeedback);
    }

    /**
     * 设置 [最终决策日期]
     */
    public void setFinaldecisiondate(Timestamp finaldecisiondate){
        this.finaldecisiondate = finaldecisiondate ;
        this.modify("finaldecisiondate",finaldecisiondate);
    }

    /**
     * 格式化日期 [最终决策日期]
     */
    public String formatFinaldecisiondate(){
        if (this.finaldecisiondate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(finaldecisiondate);
    }
    /**
     * 设置 [步骤]
     */
    public void setStepid(String stepid){
        this.stepid = stepid ;
        this.modify("stepid",stepid);
    }

    /**
     * 设置 [反馈已解决]
     */
    public void setResolvefeedback(Integer resolvefeedback){
        this.resolvefeedback = resolvefeedback ;
        this.modify("resolvefeedback",resolvefeedback);
    }

    /**
     * 设置 [预算金额 (Base)]
     */
    public void setBudgetamountBase(BigDecimal budgetamountBase){
        this.budgetamountBase = budgetamountBase ;
        this.modify("budgetamount_base",budgetamountBase);
    }

    /**
     * 设置 [决定进行/不进行]
     */
    public void setPursuitdecision(Integer pursuitdecision){
        this.pursuitdecision = pursuitdecision ;
        this.modify("pursuitdecision",pursuitdecision);
    }

    /**
     * 设置 [收入]
     */
    public void setRevenuesystemcalculated(Integer revenuesystemcalculated){
        this.revenuesystemcalculated = revenuesystemcalculated ;
        this.modify("revenuesystemcalculated",revenuesystemcalculated);
    }

    /**
     * 设置 [安排建议会议]
     */
    public void setScheduleproposalmeeting(Timestamp scheduleproposalmeeting){
        this.scheduleproposalmeeting = scheduleproposalmeeting ;
        this.modify("scheduleproposalmeeting",scheduleproposalmeeting);
    }

    /**
     * 格式化日期 [安排建议会议]
     */
    public String formatScheduleproposalmeeting(){
        if (this.scheduleproposalmeeting == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(scheduleproposalmeeting);
    }
    /**
     * 设置 [潜在客户类型]
     */
    public void setCustomertype(String customertype){
        this.customertype = customertype ;
        this.modify("customertype",customertype);
    }

    /**
     * 设置 [可能性]
     */
    public void setCloseprobability(Integer closeprobability){
        this.closeprobability = closeprobability ;
        this.modify("closeprobability",closeprobability);
    }

    /**
     * 设置 [UTC Conversion Time Zone Code]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [已提供建议]
     */
    public void setPresentproposal(Integer presentproposal){
        this.presentproposal = presentproposal ;
        this.modify("presentproposal",presentproposal);
    }

    /**
     * 设置 [已安排跟进(潜在客户)]
     */
    public void setSchedulefollowupProspect(Timestamp schedulefollowupProspect){
        this.schedulefollowupProspect = schedulefollowupProspect ;
        this.modify("schedulefollowup_prospect",schedulefollowupProspect);
    }

    /**
     * 格式化日期 [已安排跟进(潜在客户)]
     */
    public String formatSchedulefollowupProspect(){
        if (this.schedulefollowupProspect == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(schedulefollowupProspect);
    }
    /**
     * 设置 [状态描述]
     */
    public void setStatuscode(Integer statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [实际截止日期]
     */
    public void setActualclosedate(Timestamp actualclosedate){
        this.actualclosedate = actualclosedate ;
        this.modify("actualclosedate",actualclosedate);
    }

    /**
     * 格式化日期 [实际截止日期]
     */
    public String formatActualclosedate(){
        if (this.actualclosedate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(actualclosedate);
    }
    /**
     * 设置 [确认有兴趣]
     */
    public void setConfirminterest(Integer confirminterest){
        this.confirminterest = confirminterest ;
        this.modify("confirminterest",confirminterest);
    }

    /**
     * 设置 [潜在客户]
     */
    public void setCustomerid(String customerid){
        this.customerid = customerid ;
        this.modify("customerid",customerid);
    }

    /**
     * 设置 [销售阶段]
     */
    public void setSalesstage(String salesstage){
        this.salesstage = salesstage ;
        this.modify("salesstage",salesstage);
    }

    /**
     * 设置 [暂候时间(分钟)]
     */
    public void setOnholdtime(Integer onholdtime){
        this.onholdtime = onholdtime ;
        this.modify("onholdtime",onholdtime);
    }

    /**
     * 设置 [资格注释]
     */
    public void setQualificationcomments(String qualificationcomments){
        this.qualificationcomments = qualificationcomments ;
        this.modify("qualificationcomments",qualificationcomments);
    }

    /**
     * 设置 [已安排跟进(授予资格)]
     */
    public void setSchedulefollowupQualify(Timestamp schedulefollowupQualify){
        this.schedulefollowupQualify = schedulefollowupQualify ;
        this.modify("schedulefollowup_qualify",schedulefollowupQualify);
    }

    /**
     * 格式化日期 [已安排跟进(授予资格)]
     */
    public String formatSchedulefollowupQualify(){
        if (this.schedulefollowupQualify == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(schedulefollowupQualify);
    }
    /**
     * 设置 [参与工作流]
     */
    public void setParticipatesinworkflow(Integer participatesinworkflow){
        this.participatesinworkflow = participatesinworkflow ;
        this.modify("participatesinworkflow",participatesinworkflow);
    }

    /**
     * 设置 [汇率]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [准备阶段]
     */
    public void setStepname(String stepname){
        this.stepname = stepname ;
        this.modify("stepname",stepname);
    }

    /**
     * 设置 [客户需求]
     */
    public void setCustomerneed(String customerneed){
        this.customerneed = customerneed ;
        this.modify("customerneed",customerneed);
    }

    /**
     * 设置 [Import Sequence Number]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [完成内部审核]
     */
    public void setCompleteinternalreview(Integer completeinternalreview){
        this.completeinternalreview = completeinternalreview ;
        this.modify("completeinternalreview",completeinternalreview);
    }

    /**
     * 设置 [评估相符程度]
     */
    public void setEvaluatefit(Integer evaluatefit){
        this.evaluatefit = evaluatefit ;
        this.modify("evaluatefit",evaluatefit);
    }

    /**
     * 设置 [处理代码]
     */
    public void setSalesstagecode(String salesstagecode){
        this.salesstagecode = salesstagecode ;
        this.modify("salesstagecode",salesstagecode);
    }

    /**
     * 设置 [识别销售团队]
     */
    public void setIdentifypursuitteam(Integer identifypursuitteam){
        this.identifypursuitteam = identifypursuitteam ;
        this.modify("identifypursuitteam",identifypursuitteam);
    }

    /**
     * 设置 [初始通信]
     */
    public void setInitialcommunication(String initialcommunication){
        this.initialcommunication = initialcommunication ;
        this.modify("initialcommunication",initialcommunication);
    }

    /**
     * 设置 [Version Number]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [运费金额 (Base)]
     */
    public void setFreightamountBase(BigDecimal freightamountBase){
        this.freightamountBase = freightamountBase ;
        this.modify("freightamount_base",freightamountBase);
    }

    /**
     * 设置 [客户]
     */
    public void setAccountname(String accountname){
        this.accountname = accountname ;
        this.modify("accountname",accountname);
    }

    /**
     * 设置 [TeamsFollowed]
     */
    public void setTeamsfollowed(Integer teamsfollowed){
        this.teamsfollowed = teamsfollowed ;
        this.modify("teamsfollowed",teamsfollowed);
    }

    /**
     * 设置 [已拟定解决方案]
     */
    public void setProposedsolution(String proposedsolution){
        this.proposedsolution = proposedsolution ;
        this.modify("proposedsolution",proposedsolution);
    }

    /**
     * 设置 [归档任务报告]
     */
    public void setFiledebrief(Integer filedebrief){
        this.filedebrief = filedebrief ;
        this.modify("filedebrief",filedebrief);
    }

    /**
     * 设置 [预算金额]
     */
    public void setBudgetamount(BigDecimal budgetamount){
        this.budgetamount = budgetamount ;
        this.modify("budgetamount",budgetamount);
    }

    /**
     * 设置 [预算]
     */
    public void setBudgetstatus(String budgetstatus){
        this.budgetstatus = budgetstatus ;
        this.modify("budgetstatus",budgetstatus);
    }

    /**
     * 设置 [商机折扣额 (Base)]
     */
    public void setDiscountamountBase(BigDecimal discountamountBase){
        this.discountamountBase = discountamountBase ;
        this.modify("discountamount_base",discountamountBase);
    }

    /**
     * 设置 [最终建议已就绪]
     */
    public void setCompletefinalproposal(Integer completefinalproposal){
        this.completefinalproposal = completefinalproposal ;
        this.modify("completefinalproposal",completefinalproposal);
    }

    /**
     * 设置 [商机折扣额]
     */
    public void setDiscountamount(BigDecimal discountamount){
        this.discountamount = discountamount ;
        this.modify("discountamount",discountamount);
    }

    /**
     * 设置 [需求]
     */
    public void setNeed(String need){
        this.need = need ;
        this.modify("need",need);
    }

    /**
     * 设置 [Stage Id]
     */
    public void setStageid(String stageid){
        this.stageid = stageid ;
        this.modify("stageid",stageid);
    }

    /**
     * 设置 [运费金额]
     */
    public void setFreightamount(BigDecimal freightamount){
        this.freightamount = freightamount ;
        this.modify("freightamount",freightamount);
    }

    /**
     * 设置 [实际收入]
     */
    public void setActualvalue(BigDecimal actualvalue){
        this.actualvalue = actualvalue ;
        this.modify("actualvalue",actualvalue);
    }

    /**
     * 设置 [是否私有]
     */
    public void setIbizprivate(Integer ibizprivate){
        this.ibizprivate = ibizprivate ;
        this.modify("private",ibizprivate);
    }

    /**
     * 设置 [Time Zone Rule Version Number]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [优先级]
     */
    public void setPrioritycode(String prioritycode){
        this.prioritycode = prioritycode ;
        this.modify("prioritycode",prioritycode);
    }

    /**
     * 设置 [明细金额总计 (Base)]
     */
    public void setTotallineitemamountBase(BigDecimal totallineitemamountBase){
        this.totallineitemamountBase = totallineitemamountBase ;
        this.modify("totallineitemamount_base",totallineitemamountBase);
    }

    /**
     * 设置 [客户]
     */
    public void setCustomername(String customername){
        this.customername = customername ;
        this.modify("customername",customername);
    }

    /**
     * 设置 [预计结束日期]
     */
    public void setEstimatedclosedate(Timestamp estimatedclosedate){
        this.estimatedclosedate = estimatedclosedate ;
        this.modify("estimatedclosedate",estimatedclosedate);
    }

    /**
     * 格式化日期 [预计结束日期]
     */
    public String formatEstimatedclosedate(){
        if (this.estimatedclosedate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(estimatedclosedate);
    }
    /**
     * 设置 [当前状况]
     */
    public void setCurrentsituation(String currentsituation){
        this.currentsituation = currentsituation ;
        this.modify("currentsituation",currentsituation);
    }

    /**
     * 设置 [实际收入 (Base)]
     */
    public void setActualvalueBase(BigDecimal actualvalueBase){
        this.actualvalueBase = actualvalueBase ;
        this.modify("actualvalue_base",actualvalueBase);
    }

    /**
     * 设置 [客户难点]
     */
    public void setCustomerpainpoints(String customerpainpoints){
        this.customerpainpoints = customerpainpoints ;
        this.modify("customerpainpoints",customerpainpoints);
    }

    /**
     * 设置 [折扣金额总和]
     */
    public void setTotaldiscountamount(BigDecimal totaldiscountamount){
        this.totaldiscountamount = totaldiscountamount ;
        this.modify("totaldiscountamount",totaldiscountamount);
    }

    /**
     * 设置 [商机名称]
     */
    public void setOpportunityname(String opportunityname){
        this.opportunityname = opportunityname ;
        this.modify("opportunityname",opportunityname);
    }

    /**
     * 设置 [识别客户联系人]
     */
    public void setIdentifycustomercontacts(Integer identifycustomercontacts){
        this.identifycustomercontacts = identifycustomercontacts ;
        this.modify("identifycustomercontacts",identifycustomercontacts);
    }

    /**
     * 设置 [日程表]
     */
    public void setTimeline(String timeline){
        this.timeline = timeline ;
        this.modify("timeline",timeline);
    }

    /**
     * 设置 [联系人]
     */
    public void setContactname(String contactname){
        this.contactname = contactname ;
        this.modify("contactname",contactname);
    }

    /**
     * 设置 [上一暂候时间]
     */
    public void setLastonholdtime(Timestamp lastonholdtime){
        this.lastonholdtime = lastonholdtime ;
        this.modify("lastonholdtime",lastonholdtime);
    }

    /**
     * 格式化日期 [上一暂候时间]
     */
    public String formatLastonholdtime(){
        if (this.lastonholdtime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(lastonholdtime);
    }
    /**
     * 设置 [折后金额总计 (Base)]
     */
    public void setTotalamountlessfreightBase(BigDecimal totalamountlessfreightBase){
        this.totalamountlessfreightBase = totalamountlessfreightBase ;
        this.modify("totalamountlessfreight_base",totalamountlessfreightBase);
    }

    /**
     * 设置 [预计收入]
     */
    public void setEstimatedvalue(BigDecimal estimatedvalue){
        this.estimatedvalue = estimatedvalue ;
        this.modify("estimatedvalue",estimatedvalue);
    }

    /**
     * 设置 [Email Address]
     */
    public void setEmailaddress(String emailaddress){
        this.emailaddress = emailaddress ;
        this.modify("emailaddress",emailaddress);
    }

    /**
     * 设置 [状态]
     */
    public void setStatecode(Integer statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [购买时间范围]
     */
    public void setPurchasetimeframe(String purchasetimeframe){
        this.purchasetimeframe = purchasetimeframe ;
        this.modify("purchasetimeframe",purchasetimeframe);
    }

    /**
     * 设置 [报价单注释]
     */
    public void setQuotecomments(String quotecomments){
        this.quotecomments = quotecomments ;
        this.modify("quotecomments",quotecomments);
    }

    /**
     * 设置 [Record Created On]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [Record Created On]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [负责人]
     */
    public void setOwnerid(String ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [帐户]
     */
    public void setParentaccountname(String parentaccountname){
        this.parentaccountname = parentaccountname ;
        this.modify("parentaccountname",parentaccountname);
    }

    /**
     * 设置 [原始潜在顾客]
     */
    public void setOriginatingleadname(String originatingleadname){
        this.originatingleadname = originatingleadname ;
        this.modify("originatingleadname",originatingleadname);
    }

    /**
     * 设置 [联系人]
     */
    public void setParentcontactname(String parentcontactname){
        this.parentcontactname = parentcontactname ;
        this.modify("parentcontactname",parentcontactname);
    }

    /**
     * 设置 [货币]
     */
    public void setCurrencyname(String currencyname){
        this.currencyname = currencyname ;
        this.modify("currencyname",currencyname);
    }

    /**
     * 设置 [价目表]
     */
    public void setPricelevelname(String pricelevelname){
        this.pricelevelname = pricelevelname ;
        this.modify("pricelevelname",pricelevelname);
    }

    /**
     * 设置 [SLA]
     */
    public void setSlaname(String slaname){
        this.slaname = slaname ;
        this.modify("slaname",slaname);
    }

    /**
     * 设置 [源市场活动]
     */
    public void setCampaignname(String campaignname){
        this.campaignname = campaignname ;
        this.modify("campaignname",campaignname);
    }

    /**
     * 设置 [联系人]
     */
    public void setParentcontactid(String parentcontactid){
        this.parentcontactid = parentcontactid ;
        this.modify("parentcontactid",parentcontactid);
    }

    /**
     * 设置 [源市场活动]
     */
    public void setCampaignid(String campaignid){
        this.campaignid = campaignid ;
        this.modify("campaignid",campaignid);
    }

    /**
     * 设置 [货币]
     */
    public void setTransactioncurrencyid(String transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }

    /**
     * 设置 [帐户]
     */
    public void setParentaccountid(String parentaccountid){
        this.parentaccountid = parentaccountid ;
        this.modify("parentaccountid",parentaccountid);
    }

    /**
     * 设置 [价目表]
     */
    public void setPricelevelid(String pricelevelid){
        this.pricelevelid = pricelevelid ;
        this.modify("pricelevelid",pricelevelid);
    }

    /**
     * 设置 [原始潜在顾客]
     */
    public void setOriginatingleadid(String originatingleadid){
        this.originatingleadid = originatingleadid ;
        this.modify("originatingleadid",originatingleadid);
    }

    /**
     * 设置 [SLA]
     */
    public void setSlaid(String slaid){
        this.slaid = slaid ;
        this.modify("slaid",slaid);
    }


}



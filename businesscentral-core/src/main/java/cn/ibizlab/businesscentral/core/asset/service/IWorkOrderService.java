package cn.ibizlab.businesscentral.core.asset.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.asset.domain.WorkOrder;
import cn.ibizlab.businesscentral.core.asset.filter.WorkOrderSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[WorkOrder] 服务对象接口
 */
public interface IWorkOrderService extends IService<WorkOrder>{

    boolean create(WorkOrder et) ;
    void createBatch(List<WorkOrder> list) ;
    boolean update(WorkOrder et) ;
    void updateBatch(List<WorkOrder> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    WorkOrder get(String key) ;
    WorkOrder getDraft(WorkOrder et) ;
    boolean checkKey(WorkOrder et) ;
    boolean save(WorkOrder et) ;
    void saveBatch(List<WorkOrder> list) ;
    Page<WorkOrder> searchDefault(WorkOrderSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<WorkOrder> getWorkorderByIds(List<String> ids) ;
    List<WorkOrder> getWorkorderByEntities(List<WorkOrder> entities) ;
}



package cn.ibizlab.businesscentral.core.service.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[合同模板]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "CONTRACTTEMPLATE",resultMap = "ContractTemplateResultMap")
public class ContractTemplate extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 记帐频率
     */
    @TableField(value = "billingfrequencycode")
    @JSONField(name = "billingfrequencycode")
    @JsonProperty("billingfrequencycode")
    private String billingfrequencycode;
    /**
     * 是托管的
     */
    @DEField(defaultValue = "0")
    @TableField(value = "managed")
    @JSONField(name = "managed")
    @JsonProperty("managed")
    private Integer managed;
    /**
     * Version Number
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * Record Created On
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 组件状态
     */
    @TableField(value = "componentstate")
    @JSONField(name = "componentstate")
    @JsonProperty("componentstate")
    private String componentstate;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 可自定义
     */
    @TableField(value = "customizable")
    @JSONField(name = "customizable")
    @JsonProperty("customizable")
    private String customizable;
    /**
     * 引入的版本
     */
    @TableField(value = "introducedversion")
    @JSONField(name = "introducedversion")
    @JsonProperty("introducedversion")
    private String introducedversion;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 解决方案
     */
    @TableField(value = "supportingsolutionid")
    @JSONField(name = "supportingsolutionid")
    @JsonProperty("supportingsolutionid")
    private String supportingsolutionid;
    /**
     * 解决方案
     */
    @TableField(value = "solutionid")
    @JSONField(name = "solutionid")
    @JsonProperty("solutionid")
    private String solutionid;
    /**
     * Time Zone Rule Version Number
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * 合同模板
     */
    @DEField(isKeyField=true)
    @TableId(value= "contracttemplateid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "contracttemplateid")
    @JsonProperty("contracttemplateid")
    private String contracttemplateid;
    /**
     * 记录替代时间
     */
    @TableField(value = "overwritetime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overwritetime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overwritetime")
    private Timestamp overwritetime;
    /**
     * 合同服务级别
     */
    @TableField(value = "contractservicelevelcode")
    @JSONField(name = "contractservicelevelcode")
    @JsonProperty("contractservicelevelcode")
    private String contractservicelevelcode;
    /**
     * UTC Conversion Time Zone Code
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * 支持日历
     */
    @TableField(value = "effectivitycalendar")
    @JSONField(name = "effectivitycalendar")
    @JsonProperty("effectivitycalendar")
    private String effectivitycalendar;
    /**
     * 使用折扣百分比
     */
    @DEField(defaultValue = "0")
    @TableField(value = "usediscountaspercentage")
    @JSONField(name = "usediscountaspercentage")
    @JsonProperty("usediscountaspercentage")
    private Integer usediscountaspercentage;
    /**
     * Import Sequence Number
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * 合同模板名称
     */
    @TableField(value = "contracttemplatename")
    @JSONField(name = "contracttemplatename")
    @JsonProperty("contracttemplatename")
    private String contracttemplatename;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 缩写
     */
    @TableField(value = "abbreviation")
    @JSONField(name = "abbreviation")
    @JsonProperty("abbreviation")
    private String abbreviation;
    /**
     * 服务配额类型
     */
    @TableField(value = "allotmenttypecode")
    @JSONField(name = "allotmenttypecode")
    @JsonProperty("allotmenttypecode")
    private String allotmenttypecode;
    /**
     * 仅供内部使用。
     */
    @TableField(value = "contracttemplateidunique")
    @JSONField(name = "contracttemplateidunique")
    @JsonProperty("contracttemplateidunique")
    private String contracttemplateidunique;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;



    /**
     * 设置 [记帐频率]
     */
    public void setBillingfrequencycode(String billingfrequencycode){
        this.billingfrequencycode = billingfrequencycode ;
        this.modify("billingfrequencycode",billingfrequencycode);
    }

    /**
     * 设置 [是托管的]
     */
    public void setManaged(Integer managed){
        this.managed = managed ;
        this.modify("managed",managed);
    }

    /**
     * 设置 [Version Number]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [Record Created On]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [Record Created On]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [组件状态]
     */
    public void setComponentstate(String componentstate){
        this.componentstate = componentstate ;
        this.modify("componentstate",componentstate);
    }

    /**
     * 设置 [可自定义]
     */
    public void setCustomizable(String customizable){
        this.customizable = customizable ;
        this.modify("customizable",customizable);
    }

    /**
     * 设置 [引入的版本]
     */
    public void setIntroducedversion(String introducedversion){
        this.introducedversion = introducedversion ;
        this.modify("introducedversion",introducedversion);
    }

    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [解决方案]
     */
    public void setSupportingsolutionid(String supportingsolutionid){
        this.supportingsolutionid = supportingsolutionid ;
        this.modify("supportingsolutionid",supportingsolutionid);
    }

    /**
     * 设置 [解决方案]
     */
    public void setSolutionid(String solutionid){
        this.solutionid = solutionid ;
        this.modify("solutionid",solutionid);
    }

    /**
     * 设置 [Time Zone Rule Version Number]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [记录替代时间]
     */
    public void setOverwritetime(Timestamp overwritetime){
        this.overwritetime = overwritetime ;
        this.modify("overwritetime",overwritetime);
    }

    /**
     * 格式化日期 [记录替代时间]
     */
    public String formatOverwritetime(){
        if (this.overwritetime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overwritetime);
    }
    /**
     * 设置 [合同服务级别]
     */
    public void setContractservicelevelcode(String contractservicelevelcode){
        this.contractservicelevelcode = contractservicelevelcode ;
        this.modify("contractservicelevelcode",contractservicelevelcode);
    }

    /**
     * 设置 [UTC Conversion Time Zone Code]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [支持日历]
     */
    public void setEffectivitycalendar(String effectivitycalendar){
        this.effectivitycalendar = effectivitycalendar ;
        this.modify("effectivitycalendar",effectivitycalendar);
    }

    /**
     * 设置 [使用折扣百分比]
     */
    public void setUsediscountaspercentage(Integer usediscountaspercentage){
        this.usediscountaspercentage = usediscountaspercentage ;
        this.modify("usediscountaspercentage",usediscountaspercentage);
    }

    /**
     * 设置 [Import Sequence Number]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [合同模板名称]
     */
    public void setContracttemplatename(String contracttemplatename){
        this.contracttemplatename = contracttemplatename ;
        this.modify("contracttemplatename",contracttemplatename);
    }

    /**
     * 设置 [缩写]
     */
    public void setAbbreviation(String abbreviation){
        this.abbreviation = abbreviation ;
        this.modify("abbreviation",abbreviation);
    }

    /**
     * 设置 [服务配额类型]
     */
    public void setAllotmenttypecode(String allotmenttypecode){
        this.allotmenttypecode = allotmenttypecode ;
        this.modify("allotmenttypecode",allotmenttypecode);
    }

    /**
     * 设置 [仅供内部使用。]
     */
    public void setContracttemplateidunique(String contracttemplateidunique){
        this.contracttemplateidunique = contracttemplateidunique ;
        this.modify("contracttemplateidunique",contracttemplateidunique);
    }


}



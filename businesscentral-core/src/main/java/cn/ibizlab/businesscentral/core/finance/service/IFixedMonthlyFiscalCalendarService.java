package cn.ibizlab.businesscentral.core.finance.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.finance.domain.FixedMonthlyFiscalCalendar;
import cn.ibizlab.businesscentral.core.finance.filter.FixedMonthlyFiscalCalendarSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[FixedMonthlyFiscalCalendar] 服务对象接口
 */
public interface IFixedMonthlyFiscalCalendarService extends IService<FixedMonthlyFiscalCalendar>{

    boolean create(FixedMonthlyFiscalCalendar et) ;
    void createBatch(List<FixedMonthlyFiscalCalendar> list) ;
    boolean update(FixedMonthlyFiscalCalendar et) ;
    void updateBatch(List<FixedMonthlyFiscalCalendar> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    FixedMonthlyFiscalCalendar get(String key) ;
    FixedMonthlyFiscalCalendar getDraft(FixedMonthlyFiscalCalendar et) ;
    boolean checkKey(FixedMonthlyFiscalCalendar et) ;
    boolean save(FixedMonthlyFiscalCalendar et) ;
    void saveBatch(List<FixedMonthlyFiscalCalendar> list) ;
    Page<FixedMonthlyFiscalCalendar> searchDefault(FixedMonthlyFiscalCalendarSearchContext context) ;
    List<FixedMonthlyFiscalCalendar> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<FixedMonthlyFiscalCalendar> getFixedmonthlyfiscalcalendarByIds(List<String> ids) ;
    List<FixedMonthlyFiscalCalendar> getFixedmonthlyfiscalcalendarByEntities(List<FixedMonthlyFiscalCalendar> entities) ;
}



package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.Team;
import cn.ibizlab.businesscentral.core.base.filter.TeamSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Team] 服务对象接口
 */
public interface ITeamService extends IService<Team>{

    boolean create(Team et) ;
    void createBatch(List<Team> list) ;
    boolean update(Team et) ;
    void updateBatch(List<Team> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Team get(String key) ;
    Team getDraft(Team et) ;
    boolean checkKey(Team et) ;
    boolean save(Team et) ;
    void saveBatch(List<Team> list) ;
    Page<Team> searchDefault(TeamSearchContext context) ;
    List<Team> selectByBusinessunitid(String businessunitid) ;
    void removeByBusinessunitid(String businessunitid) ;
    List<Team> selectByQueueid(String queueid) ;
    void removeByQueueid(String queueid) ;
    List<Team> selectByTeamtemplateid(String teamtemplateid) ;
    void removeByTeamtemplateid(String teamtemplateid) ;
    List<Team> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Team> getTeamByIds(List<String> ids) ;
    List<Team> getTeamByEntities(List<Team> entities) ;
}



package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.TeamTemplate;
import cn.ibizlab.businesscentral.core.base.filter.TeamTemplateSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[TeamTemplate] 服务对象接口
 */
public interface ITeamTemplateService extends IService<TeamTemplate>{

    boolean create(TeamTemplate et) ;
    void createBatch(List<TeamTemplate> list) ;
    boolean update(TeamTemplate et) ;
    void updateBatch(List<TeamTemplate> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    TeamTemplate get(String key) ;
    TeamTemplate getDraft(TeamTemplate et) ;
    boolean checkKey(TeamTemplate et) ;
    boolean save(TeamTemplate et) ;
    void saveBatch(List<TeamTemplate> list) ;
    Page<TeamTemplate> searchDefault(TeamTemplateSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<TeamTemplate> getTeamtemplateByIds(List<String> ids) ;
    List<TeamTemplate> getTeamtemplateByEntities(List<TeamTemplate> entities) ;
}



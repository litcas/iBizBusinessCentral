package cn.ibizlab.businesscentral.core.scheduling.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.scheduling.domain.PSAccount;
import cn.ibizlab.businesscentral.core.scheduling.filter.PSAccountSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[PSAccount] 服务对象接口
 */
@Component
public class PSAccountFallback implements PSAccountFeignClient{

    public Page<PSAccount> select(){
            return null;
     }

    public PSAccount create(PSAccount psaccount){
            return null;
     }
    public Boolean createBatch(List<PSAccount> psaccounts){
            return false;
     }

    public PSAccount update(String accountid, PSAccount psaccount){
            return null;
     }
    public Boolean updateBatch(List<PSAccount> psaccounts){
            return false;
     }


    public Boolean remove(String accountid){
            return false;
     }
    public Boolean removeBatch(Collection<String> idList){
            return false;
     }

    public PSAccount get(String accountid){
            return null;
     }


    public PSAccount getDraft(){
            return null;
    }



    public Boolean checkKey(PSAccount psaccount){
            return false;
     }


    public Boolean save(PSAccount psaccount){
            return false;
     }
    public Boolean saveBatch(List<PSAccount> psaccounts){
            return false;
     }

    public Page<PSAccount> searchDefault(PSAccountSearchContext context){
            return null;
     }


}

package cn.ibizlab.businesscentral.core.sales.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.sales.domain.Opportunity;
import cn.ibizlab.businesscentral.core.sales.filter.OpportunitySearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Opportunity] 服务对象接口
 */
public interface IOpportunityService extends IService<Opportunity>{

    boolean create(Opportunity et) ;
    void createBatch(List<Opportunity> list) ;
    boolean update(Opportunity et) ;
    void updateBatch(List<Opportunity> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Opportunity get(String key) ;
    Opportunity getDraft(Opportunity et) ;
    Opportunity active(Opportunity et) ;
    boolean checkKey(Opportunity et) ;
    Opportunity lose(Opportunity et) ;
    boolean save(Opportunity et) ;
    void saveBatch(List<Opportunity> list) ;
    Opportunity win(Opportunity et) ;
    Page<Opportunity> searchDefault(OpportunitySearchContext context) ;
    Page<Opportunity> searchLost(OpportunitySearchContext context) ;
    Page<Opportunity> searchTop5(OpportunitySearchContext context) ;
    Page<Opportunity> searchWin(OpportunitySearchContext context) ;
    List<Opportunity> selectByParentaccountid(String accountid) ;
    void removeByParentaccountid(String accountid) ;
    List<Opportunity> selectByCampaignid(String campaignid) ;
    void removeByCampaignid(String campaignid) ;
    List<Opportunity> selectByParentcontactid(String contactid) ;
    void removeByParentcontactid(String contactid) ;
    List<Opportunity> selectByOriginatingleadid(String leadid) ;
    void removeByOriginatingleadid(String leadid) ;
    List<Opportunity> selectByPricelevelid(String pricelevelid) ;
    void removeByPricelevelid(String pricelevelid) ;
    List<Opportunity> selectBySlaid(String slaid) ;
    void removeBySlaid(String slaid) ;
    List<Opportunity> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Opportunity> getOpportunityByIds(List<String> ids) ;
    List<Opportunity> getOpportunityByEntities(List<Opportunity> entities) ;
}



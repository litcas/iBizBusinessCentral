package cn.ibizlab.businesscentral.core.runtime.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.runtime.domain.BulkDeleteOperation;
import cn.ibizlab.businesscentral.core.runtime.filter.BulkDeleteOperationSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[BulkDeleteOperation] 服务对象接口
 */
public interface IBulkDeleteOperationService extends IService<BulkDeleteOperation>{

    boolean create(BulkDeleteOperation et) ;
    void createBatch(List<BulkDeleteOperation> list) ;
    boolean update(BulkDeleteOperation et) ;
    void updateBatch(List<BulkDeleteOperation> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    BulkDeleteOperation get(String key) ;
    BulkDeleteOperation getDraft(BulkDeleteOperation et) ;
    boolean checkKey(BulkDeleteOperation et) ;
    boolean save(BulkDeleteOperation et) ;
    void saveBatch(List<BulkDeleteOperation> list) ;
    Page<BulkDeleteOperation> searchDefault(BulkDeleteOperationSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<BulkDeleteOperation> getBulkdeleteoperationByIds(List<String> ids) ;
    List<BulkDeleteOperation> getBulkdeleteoperationByEntities(List<BulkDeleteOperation> entities) ;
}



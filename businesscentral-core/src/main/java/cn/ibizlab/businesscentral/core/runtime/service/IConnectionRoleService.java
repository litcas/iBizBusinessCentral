package cn.ibizlab.businesscentral.core.runtime.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.runtime.domain.ConnectionRole;
import cn.ibizlab.businesscentral.core.runtime.filter.ConnectionRoleSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[ConnectionRole] 服务对象接口
 */
public interface IConnectionRoleService extends IService<ConnectionRole>{

    boolean create(ConnectionRole et) ;
    void createBatch(List<ConnectionRole> list) ;
    boolean update(ConnectionRole et) ;
    void updateBatch(List<ConnectionRole> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    ConnectionRole get(String key) ;
    ConnectionRole getDraft(ConnectionRole et) ;
    boolean checkKey(ConnectionRole et) ;
    boolean save(ConnectionRole et) ;
    void saveBatch(List<ConnectionRole> list) ;
    Page<ConnectionRole> searchDefault(ConnectionRoleSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<ConnectionRole> getConnectionroleByIds(List<String> ids) ;
    List<ConnectionRole> getConnectionroleByEntities(List<ConnectionRole> entities) ;
}



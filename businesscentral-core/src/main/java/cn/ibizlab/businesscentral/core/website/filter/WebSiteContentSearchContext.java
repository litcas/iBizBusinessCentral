package cn.ibizlab.businesscentral.core.website.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.website.domain.WebSiteContent;
/**
 * 关系型数据实体[WebSiteContent] 查询条件对象
 */
@Slf4j
@Data
public class WebSiteContentSearchContext extends QueryWrapperContext<WebSiteContent> {

	private String n_websitecontentname_like;//[内容名称]
	public void setN_websitecontentname_like(String n_websitecontentname_like) {
        this.n_websitecontentname_like = n_websitecontentname_like;
        if(!ObjectUtils.isEmpty(this.n_websitecontentname_like)){
            this.getSearchCond().like("websitecontentname", n_websitecontentname_like);
        }
    }
	private String n_contenttype_eq;//[内容类型]
	public void setN_contenttype_eq(String n_contenttype_eq) {
        this.n_contenttype_eq = n_contenttype_eq;
        if(!ObjectUtils.isEmpty(this.n_contenttype_eq)){
            this.getSearchCond().eq("contenttype", n_contenttype_eq);
        }
    }
	private String n_websitechannelname_eq;//[频道]
	public void setN_websitechannelname_eq(String n_websitechannelname_eq) {
        this.n_websitechannelname_eq = n_websitechannelname_eq;
        if(!ObjectUtils.isEmpty(this.n_websitechannelname_eq)){
            this.getSearchCond().eq("websitechannelname", n_websitechannelname_eq);
        }
    }
	private String n_websitechannelname_like;//[频道]
	public void setN_websitechannelname_like(String n_websitechannelname_like) {
        this.n_websitechannelname_like = n_websitechannelname_like;
        if(!ObjectUtils.isEmpty(this.n_websitechannelname_like)){
            this.getSearchCond().like("websitechannelname", n_websitechannelname_like);
        }
    }
	private String n_websitename_eq;//[站点]
	public void setN_websitename_eq(String n_websitename_eq) {
        this.n_websitename_eq = n_websitename_eq;
        if(!ObjectUtils.isEmpty(this.n_websitename_eq)){
            this.getSearchCond().eq("websitename", n_websitename_eq);
        }
    }
	private String n_websitename_like;//[站点]
	public void setN_websitename_like(String n_websitename_like) {
        this.n_websitename_like = n_websitename_like;
        if(!ObjectUtils.isEmpty(this.n_websitename_like)){
            this.getSearchCond().like("websitename", n_websitename_like);
        }
    }
	private String n_websiteid_eq;//[网站标识]
	public void setN_websiteid_eq(String n_websiteid_eq) {
        this.n_websiteid_eq = n_websiteid_eq;
        if(!ObjectUtils.isEmpty(this.n_websiteid_eq)){
            this.getSearchCond().eq("websiteid", n_websiteid_eq);
        }
    }
	private String n_websitechannelid_eq;//[频道标识]
	public void setN_websitechannelid_eq(String n_websitechannelid_eq) {
        this.n_websitechannelid_eq = n_websitechannelid_eq;
        if(!ObjectUtils.isEmpty(this.n_websitechannelid_eq)){
            this.getSearchCond().eq("websitechannelid", n_websitechannelid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("websitecontentname", query)   
            );
		 }
	}
}




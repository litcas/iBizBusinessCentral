package cn.ibizlab.businesscentral.core.runtime.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.runtime.domain.Connection;
import cn.ibizlab.businesscentral.core.runtime.filter.ConnectionSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Connection] 服务对象接口
 */
public interface IConnectionService extends IService<Connection>{

    boolean create(Connection et) ;
    void createBatch(List<Connection> list) ;
    boolean update(Connection et) ;
    void updateBatch(List<Connection> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Connection get(String key) ;
    Connection getDraft(Connection et) ;
    boolean checkKey(Connection et) ;
    boolean save(Connection et) ;
    void saveBatch(List<Connection> list) ;
    Page<Connection> searchByParentKey(ConnectionSearchContext context) ;
    Page<Connection> searchDefault(ConnectionSearchContext context) ;
    List<Connection> selectByRecord1roleid(String connectionroleid) ;
    void removeByRecord1roleid(String connectionroleid) ;
    List<Connection> selectByRecord2roleid(String connectionroleid) ;
    void removeByRecord2roleid(String connectionroleid) ;
    List<Connection> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Connection> getConnectionByIds(List<String> ids) ;
    List<Connection> getConnectionByEntities(List<Connection> entities) ;
}



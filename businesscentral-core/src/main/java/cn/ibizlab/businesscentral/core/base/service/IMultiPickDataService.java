package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.MultiPickData;
import cn.ibizlab.businesscentral.core.base.filter.MultiPickDataSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[MultiPickData] 服务对象接口
 */
public interface IMultiPickDataService extends IService<MultiPickData>{

    boolean create(MultiPickData et) ;
    void createBatch(List<MultiPickData> list) ;
    boolean update(MultiPickData et) ;
    void updateBatch(List<MultiPickData> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    MultiPickData get(String key) ;
    MultiPickData getDraft(MultiPickData et) ;
    boolean checkKey(MultiPickData et) ;
    boolean save(MultiPickData et) ;
    void saveBatch(List<MultiPickData> list) ;
    Page<MultiPickData> searchAC(MultiPickDataSearchContext context) ;
    Page<MultiPickData> searchACL(MultiPickDataSearchContext context) ;
    Page<MultiPickData> searchDefault(MultiPickDataSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}



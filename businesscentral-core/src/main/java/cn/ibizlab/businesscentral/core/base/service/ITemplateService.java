package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.Template;
import cn.ibizlab.businesscentral.core.base.filter.TemplateSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Template] 服务对象接口
 */
public interface ITemplateService extends IService<Template>{

    boolean create(Template et) ;
    void createBatch(List<Template> list) ;
    boolean update(Template et) ;
    void updateBatch(List<Template> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Template get(String key) ;
    Template getDraft(Template et) ;
    boolean checkKey(Template et) ;
    boolean save(Template et) ;
    void saveBatch(List<Template> list) ;
    Page<Template> searchDefault(TemplateSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Template> getTemplateByIds(List<String> ids) ;
    List<Template> getTemplateByEntities(List<Template> entities) ;
}



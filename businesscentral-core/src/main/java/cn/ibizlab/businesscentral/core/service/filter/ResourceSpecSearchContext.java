package cn.ibizlab.businesscentral.core.service.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.service.domain.ResourceSpec;
/**
 * 关系型数据实体[ResourceSpec] 查询条件对象
 */
@Slf4j
@Data
public class ResourceSpecSearchContext extends QueryWrapperContext<ResourceSpec> {

	private String n_resourcespecname_like;//[特殊资源名称]
	public void setN_resourcespecname_like(String n_resourcespecname_like) {
        this.n_resourcespecname_like = n_resourcespecname_like;
        if(!ObjectUtils.isEmpty(this.n_resourcespecname_like)){
            this.getSearchCond().like("resourcespecname", n_resourcespecname_like);
        }
    }
	private String n_businessunitid_eq;//[业务部门]
	public void setN_businessunitid_eq(String n_businessunitid_eq) {
        this.n_businessunitid_eq = n_businessunitid_eq;
        if(!ObjectUtils.isEmpty(this.n_businessunitid_eq)){
            this.getSearchCond().eq("businessunitid", n_businessunitid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("resourcespecname", query)   
            );
		 }
	}
}




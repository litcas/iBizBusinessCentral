package cn.ibizlab.businesscentral.core.base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.base.domain.Post;
/**
 * 关系型数据实体[Post] 查询条件对象
 */
@Slf4j
@Data
public class PostSearchContext extends QueryWrapperContext<Post> {

	private String n_type_eq;//[类型]
	public void setN_type_eq(String n_type_eq) {
        this.n_type_eq = n_type_eq;
        if(!ObjectUtils.isEmpty(this.n_type_eq)){
            this.getSearchCond().eq("type", n_type_eq);
        }
    }
	private String n_text_like;//[文本]
	public void setN_text_like(String n_text_like) {
        this.n_text_like = n_text_like;
        if(!ObjectUtils.isEmpty(this.n_text_like)){
            this.getSearchCond().like("text", n_text_like);
        }
    }
	private String n_source_eq;//[来源]
	public void setN_source_eq(String n_source_eq) {
        this.n_source_eq = n_source_eq;
        if(!ObjectUtils.isEmpty(this.n_source_eq)){
            this.getSearchCond().eq("source", n_source_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("text", query)   
            );
		 }
	}
}




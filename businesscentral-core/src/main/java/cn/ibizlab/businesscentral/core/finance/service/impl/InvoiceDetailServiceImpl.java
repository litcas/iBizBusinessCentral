package cn.ibizlab.businesscentral.core.finance.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.finance.domain.InvoiceDetail;
import cn.ibizlab.businesscentral.core.finance.filter.InvoiceDetailSearchContext;
import cn.ibizlab.businesscentral.core.finance.service.IInvoiceDetailService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.finance.mapper.InvoiceDetailMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[发票产品] 服务对象接口实现
 */
@Slf4j
@Service("InvoiceDetailServiceImpl")
public class InvoiceDetailServiceImpl extends ServiceImpl<InvoiceDetailMapper, InvoiceDetail> implements IInvoiceDetailService {


    protected cn.ibizlab.businesscentral.core.finance.service.IInvoiceDetailService invoicedetailService = this;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.finance.service.IInvoiceService invoiceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.product.service.IProductService productService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.ISalesOrderDetailService salesorderdetailService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IUomService uomService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(InvoiceDetail et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getInvoicedetailid()),et);
        return true;
    }

    @Override
    public void createBatch(List<InvoiceDetail> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(InvoiceDetail et) {
        fillParentData(et);
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("invoicedetailid",et.getInvoicedetailid())))
            return false;
        CachedBeanCopier.copy(get(et.getInvoicedetailid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<InvoiceDetail> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public InvoiceDetail get(String key) {
        InvoiceDetail et = getById(key);
        if(et==null){
            et=new InvoiceDetail();
            et.setInvoicedetailid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public InvoiceDetail getDraft(InvoiceDetail et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(InvoiceDetail et) {
        return (!ObjectUtils.isEmpty(et.getInvoicedetailid()))&&(!Objects.isNull(this.getById(et.getInvoicedetailid())));
    }
    @Override
    @Transactional
    public boolean save(InvoiceDetail et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(InvoiceDetail et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<InvoiceDetail> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<InvoiceDetail> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<InvoiceDetail> selectByParentbundleidref(String invoicedetailid) {
        return baseMapper.selectByParentbundleidref(invoicedetailid);
    }

    @Override
    public void removeByParentbundleidref(String invoicedetailid) {
        this.remove(new QueryWrapper<InvoiceDetail>().eq("parentbundleidref",invoicedetailid));
    }

	@Override
    public List<InvoiceDetail> selectByInvoiceid(String invoiceid) {
        return baseMapper.selectByInvoiceid(invoiceid);
    }

    @Override
    public void removeByInvoiceid(String invoiceid) {
        this.remove(new QueryWrapper<InvoiceDetail>().eq("invoiceid",invoiceid));
    }

	@Override
    public List<InvoiceDetail> selectByProductid(String productid) {
        return baseMapper.selectByProductid(productid);
    }

    @Override
    public void removeByProductid(String productid) {
        this.remove(new QueryWrapper<InvoiceDetail>().eq("productid",productid));
    }

	@Override
    public List<InvoiceDetail> selectBySalesorderdetailid(String salesorderdetailid) {
        return baseMapper.selectBySalesorderdetailid(salesorderdetailid);
    }

    @Override
    public void removeBySalesorderdetailid(String salesorderdetailid) {
        this.remove(new QueryWrapper<InvoiceDetail>().eq("salesorderdetailid",salesorderdetailid));
    }

	@Override
    public List<InvoiceDetail> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<InvoiceDetail>().eq("transactioncurrencyid",transactioncurrencyid));
    }

	@Override
    public List<InvoiceDetail> selectByUomid(String uomid) {
        return baseMapper.selectByUomid(uomid);
    }

    @Override
    public void removeByUomid(String uomid) {
        this.remove(new QueryWrapper<InvoiceDetail>().eq("uomid",uomid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<InvoiceDetail> searchDefault(InvoiceDetailSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<InvoiceDetail> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<InvoiceDetail>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(InvoiceDetail et){
        //实体关系[DER1N_INVOICEDETAIL__PRODUCT__PRODUCTID]
        if(!ObjectUtils.isEmpty(et.getProductid())){
            cn.ibizlab.businesscentral.core.product.domain.Product product=et.getProduct();
            if(ObjectUtils.isEmpty(product)){
                cn.ibizlab.businesscentral.core.product.domain.Product majorEntity=productService.get(et.getProductid());
                et.setProduct(majorEntity);
                product=majorEntity;
            }
            et.setProductname(product.getProductname());
        }
        //实体关系[DER1N_INVOICEDETAIL__UOM__UOMID]
        if(!ObjectUtils.isEmpty(et.getUomid())){
            cn.ibizlab.businesscentral.core.base.domain.Uom uom=et.getUom();
            if(ObjectUtils.isEmpty(uom)){
                cn.ibizlab.businesscentral.core.base.domain.Uom majorEntity=uomService.get(et.getUomid());
                et.setUom(majorEntity);
                uom=majorEntity;
            }
            et.setUomname(uom.getUomname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<InvoiceDetail> getInvoicedetailByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<InvoiceDetail> getInvoicedetailByEntities(List<InvoiceDetail> entities) {
        List ids =new ArrayList();
        for(InvoiceDetail entity : entities){
            Serializable id=entity.getInvoicedetailid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




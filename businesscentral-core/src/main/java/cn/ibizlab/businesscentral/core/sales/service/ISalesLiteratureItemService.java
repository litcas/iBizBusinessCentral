package cn.ibizlab.businesscentral.core.sales.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.sales.domain.SalesLiteratureItem;
import cn.ibizlab.businesscentral.core.sales.filter.SalesLiteratureItemSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[SalesLiteratureItem] 服务对象接口
 */
public interface ISalesLiteratureItemService extends IService<SalesLiteratureItem>{

    boolean create(SalesLiteratureItem et) ;
    void createBatch(List<SalesLiteratureItem> list) ;
    boolean update(SalesLiteratureItem et) ;
    void updateBatch(List<SalesLiteratureItem> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    SalesLiteratureItem get(String key) ;
    SalesLiteratureItem getDraft(SalesLiteratureItem et) ;
    boolean checkKey(SalesLiteratureItem et) ;
    boolean save(SalesLiteratureItem et) ;
    void saveBatch(List<SalesLiteratureItem> list) ;
    Page<SalesLiteratureItem> searchDefault(SalesLiteratureItemSearchContext context) ;
    List<SalesLiteratureItem> selectBySalesliteratureid(String salesliteratureid) ;
    void removeBySalesliteratureid(String salesliteratureid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<SalesLiteratureItem> getSalesliteratureitemByIds(List<String> ids) ;
    List<SalesLiteratureItem> getSalesliteratureitemByEntities(List<SalesLiteratureItem> entities) ;
}



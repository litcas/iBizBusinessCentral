package cn.ibizlab.businesscentral.core.marketing.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[市场活动响应]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "CAMPAIGNRESPONSE",resultMap = "CampaignResponseResultMap")
public class CampaignResponse extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * RegardingObjectTypeCode
     */
    @TableField(value = "regardingobjecttypecode")
    @JSONField(name = "regardingobjecttypecode")
    @JsonProperty("regardingobjecttypecode")
    private String regardingobjecttypecode;
    /**
     * 已记帐
     */
    @DEField(defaultValue = "0")
    @TableField(value = "billed")
    @JSONField(name = "billed")
    @JsonProperty("billed")
    private Integer billed;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 渠道
     */
    @TableField(value = "channeltypecode")
    @JSONField(name = "channeltypecode")
    @JsonProperty("channeltypecode")
    private String channeltypecode;
    /**
     * 上一暂候时间
     */
    @TableField(value = "lastonholdtime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "lastonholdtime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("lastonholdtime")
    private Timestamp lastonholdtime;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * Import Sequence Number
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * Optional Attendees
     */
    @TableField(value = "optionalattendees")
    @JSONField(name = "optionalattendees")
    @JsonProperty("optionalattendees")
    private String optionalattendees;
    /**
     * Customers
     */
    @TableField(value = "customers")
    @JSONField(name = "customers")
    @JsonProperty("customers")
    private String customers;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 活动
     */
    @TableField(value = "originatingactivityname")
    @JSONField(name = "originatingactivityname")
    @JsonProperty("originatingactivityname")
    private String originatingactivityname;
    /**
     * Organizer
     */
    @TableField(value = "organizer")
    @JSONField(name = "organizer")
    @JsonProperty("organizer")
    private String organizer;
    /**
     * 优先级
     */
    @TableField(value = "prioritycode")
    @JSONField(name = "prioritycode")
    @JsonProperty("prioritycode")
    private String prioritycode;
    /**
     * 状态描述
     */
    @TableField(value = "statuscode")
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;
    /**
     * 流程
     */
    @TableField(value = "processid")
    @JSONField(name = "processid")
    @JsonProperty("processid")
    private String processid;
    /**
     * 实际持续时间
     */
    @TableField(value = "actualdurationminutes")
    @JSONField(name = "actualdurationminutes")
    @JsonProperty("actualdurationminutes")
    private Integer actualdurationminutes;
    /**
     * 时区规则版本号
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * Required Attendees
     */
    @TableField(value = "requiredattendees")
    @JSONField(name = "requiredattendees")
    @JsonProperty("requiredattendees")
    private String requiredattendees;
    /**
     * 关于
     */
    @TableField(value = "regardingobjectid")
    @JSONField(name = "regardingobjectid")
    @JsonProperty("regardingobjectid")
    private String regardingobjectid;
    /**
     * 电话
     */
    @TableField(value = "telephone")
    @JSONField(name = "telephone")
    @JsonProperty("telephone")
    private String telephone;
    /**
     * 负责人类型
     */
    @TableField(value = "ownertype")
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;
    /**
     * 计划开始时间
     */
    @TableField(value = "scheduledstart")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "scheduledstart" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("scheduledstart")
    private Timestamp scheduledstart;
    /**
     * 响应代码
     */
    @TableField(value = "responsecode")
    @JSONField(name = "responsecode")
    @JsonProperty("responsecode")
    private String responsecode;
    /**
     * 实际开始时间
     */
    @TableField(value = "actualstart")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "actualstart" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("actualstart")
    private Timestamp actualstart;
    /**
     * Resources
     */
    @TableField(value = "resources")
    @JSONField(name = "resources")
    @JsonProperty("resources")
    private String resources;
    /**
     * 服务
     */
    @TableField(value = "serviceid")
    @JSONField(name = "serviceid")
    @JsonProperty("serviceid")
    private String serviceid;
    /**
     * 定期实例类型
     */
    @TableField(value = "instancetypecode")
    @JSONField(name = "instancetypecode")
    @JsonProperty("instancetypecode")
    private String instancetypecode;
    /**
     * 主题
     */
    @TableField(value = "subject")
    @JSONField(name = "subject")
    @JsonProperty("subject")
    private String subject;
    /**
     * Exchange WebLink
     */
    @TableField(value = "exchangeweblink")
    @JSONField(name = "exchangeweblink")
    @JsonProperty("exchangeweblink")
    private String exchangeweblink;
    /**
     * 由工作流创建
     */
    @DEField(defaultValue = "0")
    @TableField(value = "workflowcreated")
    @JSONField(name = "workflowcreated")
    @JsonProperty("workflowcreated")
    private Integer workflowcreated;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 负责人
     */
    @TableField(value = "ownername")
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;
    /**
     * 名
     */
    @TableField(value = "firstname")
    @JSONField(name = "firstname")
    @JsonProperty("firstname")
    private String firstname;
    /**
     * 来自
     */
    @TableField(value = "from")
    @JSONField(name = "from")
    @JsonProperty("from")
    private String from;
    /**
     * 版本号
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 活动类型
     */
    @TableField(value = "activitytypecode")
    @JSONField(name = "activitytypecode")
    @JsonProperty("activitytypecode")
    private String activitytypecode;
    /**
     * 促销代码
     */
    @TableField(value = "promotioncodename")
    @JSONField(name = "promotioncodename")
    @JsonProperty("promotioncodename")
    private String promotioncodename;
    /**
     * CC
     */
    @TableField(value = "cc")
    @JSONField(name = "cc")
    @JsonProperty("cc")
    private String cc;
    /**
     * 电子邮件
     */
    @TableField(value = "emailaddress")
    @JSONField(name = "emailaddress")
    @JsonProperty("emailaddress")
    private String emailaddress;
    /**
     * 上次尝试传递的日期
     */
    @TableField(value = "deliverylastattemptedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "deliverylastattemptedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("deliverylastattemptedon")
    private Timestamp deliverylastattemptedon;
    /**
     * 负责人
     */
    @TableField(value = "ownerid")
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;
    /**
     * 类别
     */
    @TableField(value = "category")
    @JSONField(name = "category")
    @JsonProperty("category")
    private String category;
    /**
     * 暂候时间(分钟)
     */
    @TableField(value = "onholdtime")
    @JSONField(name = "onholdtime")
    @JsonProperty("onholdtime")
    private Integer onholdtime;
    /**
     * 接收时间
     */
    @TableField(value = "receivedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "receivedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("receivedon")
    private Timestamp receivedon;
    /**
     * OriginatingActivityTypeCode
     */
    @TableField(value = "originatingactivitytypecode")
    @JSONField(name = "originatingactivitytypecode")
    @JsonProperty("originatingactivitytypecode")
    private String originatingactivitytypecode;
    /**
     * 外包供应商
     */
    @TableField(value = "partner")
    @JSONField(name = "partner")
    @JsonProperty("partner")
    private String partner;
    /**
     * UTC 转换时区代码
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 流程阶段
     */
    @TableField(value = "stageid")
    @JSONField(name = "stageid")
    @JsonProperty("stageid")
    private String stageid;
    /**
     * 子类别
     */
    @TableField(value = "subcategory")
    @JSONField(name = "subcategory")
    @JsonProperty("subcategory")
    private String subcategory;
    /**
     * 实际结束时间
     */
    @TableField(value = "actualend")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "actualend" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("actualend")
    private Timestamp actualend;
    /**
     * 传真
     */
    @TableField(value = "fax")
    @JSONField(name = "fax")
    @JsonProperty("fax")
    private String fax;
    /**
     * 隐藏
     */
    @DEField(defaultValue = "0")
    @TableField(value = "mapiprivate")
    @JSONField(name = "mapiprivate")
    @JsonProperty("mapiprivate")
    private Integer mapiprivate;
    /**
     * 原始活动
     */
    @TableField(value = "originatingactivityid")
    @JSONField(name = "originatingactivityid")
    @JsonProperty("originatingactivityid")
    private String originatingactivityid;
    /**
     * 是定期活动
     */
    @DEField(defaultValue = "0")
    @TableField(value = "regularactivity")
    @JSONField(name = "regularactivity")
    @JsonProperty("regularactivity")
    private Integer regularactivity;
    /**
     * BCC
     */
    @TableField(value = "bcc")
    @JSONField(name = "bcc")
    @JsonProperty("bcc")
    private String bcc;
    /**
     * Outsource Vendors
     */
    @TableField(value = "partners")
    @JSONField(name = "partners")
    @JsonProperty("partners")
    private String partners;
    /**
     * 发送日期
     */
    @TableField(value = "senton")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "senton" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("senton")
    private Timestamp senton;
    /**
     * 客户
     */
    @TableField(value = "customer")
    @JSONField(name = "customer")
    @JsonProperty("customer")
    private String customer;
    /**
     * 关于
     */
    @TableField(value = "regardingobjectname")
    @JSONField(name = "regardingobjectname")
    @JsonProperty("regardingobjectname")
    private String regardingobjectname;
    /**
     * Record Created On
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * Exchange 项目 ID
     */
    @TableField(value = "exchangeitemid")
    @JSONField(name = "exchangeitemid")
    @JsonProperty("exchangeitemid")
    private String exchangeitemid;
    /**
     * SLAName
     */
    @TableField(value = "slaname")
    @JSONField(name = "slaname")
    @JsonProperty("slaname")
    private String slaname;
    /**
     * 系列 ID
     */
    @TableField(value = "seriesid")
    @JSONField(name = "seriesid")
    @JsonProperty("seriesid")
    private String seriesid;
    /**
     * 社交渠道
     */
    @TableField(value = "community")
    @JSONField(name = "community")
    @JsonProperty("community")
    private String community;
    /**
     * 遍历的路径
     */
    @TableField(value = "traversedpath")
    @JSONField(name = "traversedpath")
    @JsonProperty("traversedpath")
    private String traversedpath;
    /**
     * 传递优先级
     */
    @TableField(value = "deliveryprioritycode")
    @JSONField(name = "deliveryprioritycode")
    @JsonProperty("deliveryprioritycode")
    private String deliveryprioritycode;
    /**
     * 公司名称
     */
    @TableField(value = "companyname")
    @JSONField(name = "companyname")
    @JsonProperty("companyname")
    private String companyname;
    /**
     * 排序日期
     */
    @TableField(value = "sortdate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "sortdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("sortdate")
    private Timestamp sortdate;
    /**
     * 附加参数
     */
    @TableField(value = "activityadditionalparams")
    @JSONField(name = "activityadditionalparams")
    @JsonProperty("activityadditionalparams")
    private String activityadditionalparams;
    /**
     * 市场活动响应
     */
    @DEField(isKeyField=true)
    @TableId(value= "activityid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "activityid")
    @JsonProperty("activityid")
    private String activityid;
    /**
     * 状态
     */
    @TableField(value = "statecode")
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;
    /**
     * 计划持续时间
     */
    @TableField(value = "scheduleddurationminutes")
    @JSONField(name = "scheduleddurationminutes")
    @JsonProperty("scheduleddurationminutes")
    private Integer scheduleddurationminutes;
    /**
     * 姓
     */
    @TableField(value = "lastname")
    @JSONField(name = "lastname")
    @JsonProperty("lastname")
    private String lastname;
    /**
     * 汇率
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * To
     */
    @TableField(value = "to")
    @JSONField(name = "to")
    @JsonProperty("to")
    private String to;
    /**
     * 保留的语音邮件
     */
    @DEField(defaultValue = "0")
    @TableField(value = "leftvoicemail")
    @JSONField(name = "leftvoicemail")
    @JsonProperty("leftvoicemail")
    private Integer leftvoicemail;
    /**
     * 结束时间
     */
    @TableField(value = "scheduledend")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "scheduledend" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("scheduledend")
    private Timestamp scheduledend;
    /**
     * 货币
     */
    @TableField(value = "transactioncurrencyid")
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;
    /**
     * SLA
     */
    @TableField(value = "slaid")
    @JSONField(name = "slaid")
    @JsonProperty("slaid")
    private String slaid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Sla sla;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency;



    /**
     * 设置 [RegardingObjectTypeCode]
     */
    public void setRegardingobjecttypecode(String regardingobjecttypecode){
        this.regardingobjecttypecode = regardingobjecttypecode ;
        this.modify("regardingobjecttypecode",regardingobjecttypecode);
    }

    /**
     * 设置 [已记帐]
     */
    public void setBilled(Integer billed){
        this.billed = billed ;
        this.modify("billed",billed);
    }

    /**
     * 设置 [渠道]
     */
    public void setChanneltypecode(String channeltypecode){
        this.channeltypecode = channeltypecode ;
        this.modify("channeltypecode",channeltypecode);
    }

    /**
     * 设置 [上一暂候时间]
     */
    public void setLastonholdtime(Timestamp lastonholdtime){
        this.lastonholdtime = lastonholdtime ;
        this.modify("lastonholdtime",lastonholdtime);
    }

    /**
     * 格式化日期 [上一暂候时间]
     */
    public String formatLastonholdtime(){
        if (this.lastonholdtime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(lastonholdtime);
    }
    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [Import Sequence Number]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [Optional Attendees]
     */
    public void setOptionalattendees(String optionalattendees){
        this.optionalattendees = optionalattendees ;
        this.modify("optionalattendees",optionalattendees);
    }

    /**
     * 设置 [Customers]
     */
    public void setCustomers(String customers){
        this.customers = customers ;
        this.modify("customers",customers);
    }

    /**
     * 设置 [活动]
     */
    public void setOriginatingactivityname(String originatingactivityname){
        this.originatingactivityname = originatingactivityname ;
        this.modify("originatingactivityname",originatingactivityname);
    }

    /**
     * 设置 [Organizer]
     */
    public void setOrganizer(String organizer){
        this.organizer = organizer ;
        this.modify("organizer",organizer);
    }

    /**
     * 设置 [优先级]
     */
    public void setPrioritycode(String prioritycode){
        this.prioritycode = prioritycode ;
        this.modify("prioritycode",prioritycode);
    }

    /**
     * 设置 [状态描述]
     */
    public void setStatuscode(Integer statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [流程]
     */
    public void setProcessid(String processid){
        this.processid = processid ;
        this.modify("processid",processid);
    }

    /**
     * 设置 [实际持续时间]
     */
    public void setActualdurationminutes(Integer actualdurationminutes){
        this.actualdurationminutes = actualdurationminutes ;
        this.modify("actualdurationminutes",actualdurationminutes);
    }

    /**
     * 设置 [时区规则版本号]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [Required Attendees]
     */
    public void setRequiredattendees(String requiredattendees){
        this.requiredattendees = requiredattendees ;
        this.modify("requiredattendees",requiredattendees);
    }

    /**
     * 设置 [关于]
     */
    public void setRegardingobjectid(String regardingobjectid){
        this.regardingobjectid = regardingobjectid ;
        this.modify("regardingobjectid",regardingobjectid);
    }

    /**
     * 设置 [电话]
     */
    public void setTelephone(String telephone){
        this.telephone = telephone ;
        this.modify("telephone",telephone);
    }

    /**
     * 设置 [负责人类型]
     */
    public void setOwnertype(String ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [计划开始时间]
     */
    public void setScheduledstart(Timestamp scheduledstart){
        this.scheduledstart = scheduledstart ;
        this.modify("scheduledstart",scheduledstart);
    }

    /**
     * 格式化日期 [计划开始时间]
     */
    public String formatScheduledstart(){
        if (this.scheduledstart == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(scheduledstart);
    }
    /**
     * 设置 [响应代码]
     */
    public void setResponsecode(String responsecode){
        this.responsecode = responsecode ;
        this.modify("responsecode",responsecode);
    }

    /**
     * 设置 [实际开始时间]
     */
    public void setActualstart(Timestamp actualstart){
        this.actualstart = actualstart ;
        this.modify("actualstart",actualstart);
    }

    /**
     * 格式化日期 [实际开始时间]
     */
    public String formatActualstart(){
        if (this.actualstart == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(actualstart);
    }
    /**
     * 设置 [Resources]
     */
    public void setResources(String resources){
        this.resources = resources ;
        this.modify("resources",resources);
    }

    /**
     * 设置 [服务]
     */
    public void setServiceid(String serviceid){
        this.serviceid = serviceid ;
        this.modify("serviceid",serviceid);
    }

    /**
     * 设置 [定期实例类型]
     */
    public void setInstancetypecode(String instancetypecode){
        this.instancetypecode = instancetypecode ;
        this.modify("instancetypecode",instancetypecode);
    }

    /**
     * 设置 [主题]
     */
    public void setSubject(String subject){
        this.subject = subject ;
        this.modify("subject",subject);
    }

    /**
     * 设置 [Exchange WebLink]
     */
    public void setExchangeweblink(String exchangeweblink){
        this.exchangeweblink = exchangeweblink ;
        this.modify("exchangeweblink",exchangeweblink);
    }

    /**
     * 设置 [由工作流创建]
     */
    public void setWorkflowcreated(Integer workflowcreated){
        this.workflowcreated = workflowcreated ;
        this.modify("workflowcreated",workflowcreated);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnername(String ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [名]
     */
    public void setFirstname(String firstname){
        this.firstname = firstname ;
        this.modify("firstname",firstname);
    }

    /**
     * 设置 [来自]
     */
    public void setFrom(String from){
        this.from = from ;
        this.modify("from",from);
    }

    /**
     * 设置 [版本号]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [活动类型]
     */
    public void setActivitytypecode(String activitytypecode){
        this.activitytypecode = activitytypecode ;
        this.modify("activitytypecode",activitytypecode);
    }

    /**
     * 设置 [促销代码]
     */
    public void setPromotioncodename(String promotioncodename){
        this.promotioncodename = promotioncodename ;
        this.modify("promotioncodename",promotioncodename);
    }

    /**
     * 设置 [CC]
     */
    public void setCc(String cc){
        this.cc = cc ;
        this.modify("cc",cc);
    }

    /**
     * 设置 [电子邮件]
     */
    public void setEmailaddress(String emailaddress){
        this.emailaddress = emailaddress ;
        this.modify("emailaddress",emailaddress);
    }

    /**
     * 设置 [上次尝试传递的日期]
     */
    public void setDeliverylastattemptedon(Timestamp deliverylastattemptedon){
        this.deliverylastattemptedon = deliverylastattemptedon ;
        this.modify("deliverylastattemptedon",deliverylastattemptedon);
    }

    /**
     * 格式化日期 [上次尝试传递的日期]
     */
    public String formatDeliverylastattemptedon(){
        if (this.deliverylastattemptedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(deliverylastattemptedon);
    }
    /**
     * 设置 [负责人]
     */
    public void setOwnerid(String ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [类别]
     */
    public void setCategory(String category){
        this.category = category ;
        this.modify("category",category);
    }

    /**
     * 设置 [暂候时间(分钟)]
     */
    public void setOnholdtime(Integer onholdtime){
        this.onholdtime = onholdtime ;
        this.modify("onholdtime",onholdtime);
    }

    /**
     * 设置 [接收时间]
     */
    public void setReceivedon(Timestamp receivedon){
        this.receivedon = receivedon ;
        this.modify("receivedon",receivedon);
    }

    /**
     * 格式化日期 [接收时间]
     */
    public String formatReceivedon(){
        if (this.receivedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(receivedon);
    }
    /**
     * 设置 [OriginatingActivityTypeCode]
     */
    public void setOriginatingactivitytypecode(String originatingactivitytypecode){
        this.originatingactivitytypecode = originatingactivitytypecode ;
        this.modify("originatingactivitytypecode",originatingactivitytypecode);
    }

    /**
     * 设置 [外包供应商]
     */
    public void setPartner(String partner){
        this.partner = partner ;
        this.modify("partner",partner);
    }

    /**
     * 设置 [UTC 转换时区代码]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [流程阶段]
     */
    public void setStageid(String stageid){
        this.stageid = stageid ;
        this.modify("stageid",stageid);
    }

    /**
     * 设置 [子类别]
     */
    public void setSubcategory(String subcategory){
        this.subcategory = subcategory ;
        this.modify("subcategory",subcategory);
    }

    /**
     * 设置 [实际结束时间]
     */
    public void setActualend(Timestamp actualend){
        this.actualend = actualend ;
        this.modify("actualend",actualend);
    }

    /**
     * 格式化日期 [实际结束时间]
     */
    public String formatActualend(){
        if (this.actualend == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(actualend);
    }
    /**
     * 设置 [传真]
     */
    public void setFax(String fax){
        this.fax = fax ;
        this.modify("fax",fax);
    }

    /**
     * 设置 [隐藏]
     */
    public void setMapiprivate(Integer mapiprivate){
        this.mapiprivate = mapiprivate ;
        this.modify("mapiprivate",mapiprivate);
    }

    /**
     * 设置 [原始活动]
     */
    public void setOriginatingactivityid(String originatingactivityid){
        this.originatingactivityid = originatingactivityid ;
        this.modify("originatingactivityid",originatingactivityid);
    }

    /**
     * 设置 [是定期活动]
     */
    public void setRegularactivity(Integer regularactivity){
        this.regularactivity = regularactivity ;
        this.modify("regularactivity",regularactivity);
    }

    /**
     * 设置 [BCC]
     */
    public void setBcc(String bcc){
        this.bcc = bcc ;
        this.modify("bcc",bcc);
    }

    /**
     * 设置 [Outsource Vendors]
     */
    public void setPartners(String partners){
        this.partners = partners ;
        this.modify("partners",partners);
    }

    /**
     * 设置 [发送日期]
     */
    public void setSenton(Timestamp senton){
        this.senton = senton ;
        this.modify("senton",senton);
    }

    /**
     * 格式化日期 [发送日期]
     */
    public String formatSenton(){
        if (this.senton == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(senton);
    }
    /**
     * 设置 [客户]
     */
    public void setCustomer(String customer){
        this.customer = customer ;
        this.modify("customer",customer);
    }

    /**
     * 设置 [关于]
     */
    public void setRegardingobjectname(String regardingobjectname){
        this.regardingobjectname = regardingobjectname ;
        this.modify("regardingobjectname",regardingobjectname);
    }

    /**
     * 设置 [Record Created On]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [Record Created On]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [Exchange 项目 ID]
     */
    public void setExchangeitemid(String exchangeitemid){
        this.exchangeitemid = exchangeitemid ;
        this.modify("exchangeitemid",exchangeitemid);
    }

    /**
     * 设置 [SLAName]
     */
    public void setSlaname(String slaname){
        this.slaname = slaname ;
        this.modify("slaname",slaname);
    }

    /**
     * 设置 [系列 ID]
     */
    public void setSeriesid(String seriesid){
        this.seriesid = seriesid ;
        this.modify("seriesid",seriesid);
    }

    /**
     * 设置 [社交渠道]
     */
    public void setCommunity(String community){
        this.community = community ;
        this.modify("community",community);
    }

    /**
     * 设置 [遍历的路径]
     */
    public void setTraversedpath(String traversedpath){
        this.traversedpath = traversedpath ;
        this.modify("traversedpath",traversedpath);
    }

    /**
     * 设置 [传递优先级]
     */
    public void setDeliveryprioritycode(String deliveryprioritycode){
        this.deliveryprioritycode = deliveryprioritycode ;
        this.modify("deliveryprioritycode",deliveryprioritycode);
    }

    /**
     * 设置 [公司名称]
     */
    public void setCompanyname(String companyname){
        this.companyname = companyname ;
        this.modify("companyname",companyname);
    }

    /**
     * 设置 [排序日期]
     */
    public void setSortdate(Timestamp sortdate){
        this.sortdate = sortdate ;
        this.modify("sortdate",sortdate);
    }

    /**
     * 格式化日期 [排序日期]
     */
    public String formatSortdate(){
        if (this.sortdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(sortdate);
    }
    /**
     * 设置 [附加参数]
     */
    public void setActivityadditionalparams(String activityadditionalparams){
        this.activityadditionalparams = activityadditionalparams ;
        this.modify("activityadditionalparams",activityadditionalparams);
    }

    /**
     * 设置 [状态]
     */
    public void setStatecode(Integer statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [计划持续时间]
     */
    public void setScheduleddurationminutes(Integer scheduleddurationminutes){
        this.scheduleddurationminutes = scheduleddurationminutes ;
        this.modify("scheduleddurationminutes",scheduleddurationminutes);
    }

    /**
     * 设置 [姓]
     */
    public void setLastname(String lastname){
        this.lastname = lastname ;
        this.modify("lastname",lastname);
    }

    /**
     * 设置 [汇率]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [To]
     */
    public void setTo(String to){
        this.to = to ;
        this.modify("to",to);
    }

    /**
     * 设置 [保留的语音邮件]
     */
    public void setLeftvoicemail(Integer leftvoicemail){
        this.leftvoicemail = leftvoicemail ;
        this.modify("leftvoicemail",leftvoicemail);
    }

    /**
     * 设置 [结束时间]
     */
    public void setScheduledend(Timestamp scheduledend){
        this.scheduledend = scheduledend ;
        this.modify("scheduledend",scheduledend);
    }

    /**
     * 格式化日期 [结束时间]
     */
    public String formatScheduledend(){
        if (this.scheduledend == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(scheduledend);
    }
    /**
     * 设置 [货币]
     */
    public void setTransactioncurrencyid(String transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }

    /**
     * 设置 [SLA]
     */
    public void setSlaid(String slaid){
        this.slaid = slaid ;
        this.modify("slaid",slaid);
    }


}



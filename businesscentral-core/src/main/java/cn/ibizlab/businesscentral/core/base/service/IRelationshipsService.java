package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.Relationships;
import cn.ibizlab.businesscentral.core.base.filter.RelationshipsSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Relationships] 服务对象接口
 */
public interface IRelationshipsService extends IService<Relationships>{

    boolean create(Relationships et) ;
    void createBatch(List<Relationships> list) ;
    boolean update(Relationships et) ;
    void updateBatch(List<Relationships> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Relationships get(String key) ;
    Relationships getDraft(Relationships et) ;
    boolean checkKey(Relationships et) ;
    boolean save(Relationships et) ;
    void saveBatch(List<Relationships> list) ;
    Page<Relationships> searchDefault(RelationshipsSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Relationships> getRelationshipsByIds(List<String> ids) ;
    List<Relationships> getRelationshipsByEntities(List<Relationships> entities) ;
}



package cn.ibizlab.businesscentral.core.base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.base.domain.Metric;
/**
 * 关系型数据实体[Metric] 查询条件对象
 */
@Slf4j
@Data
public class MetricSearchContext extends QueryWrapperContext<Metric> {

	private Integer n_statuscode_eq;//[状态描述]
	public void setN_statuscode_eq(Integer n_statuscode_eq) {
        this.n_statuscode_eq = n_statuscode_eq;
        if(!ObjectUtils.isEmpty(this.n_statuscode_eq)){
            this.getSearchCond().eq("statuscode", n_statuscode_eq);
        }
    }
	private Integer n_statecode_eq;//[状态]
	public void setN_statecode_eq(Integer n_statecode_eq) {
        this.n_statecode_eq = n_statecode_eq;
        if(!ObjectUtils.isEmpty(this.n_statecode_eq)){
            this.getSearchCond().eq("statecode", n_statecode_eq);
        }
    }
	private String n_metricname_like;//[公制名称]
	public void setN_metricname_like(String n_metricname_like) {
        this.n_metricname_like = n_metricname_like;
        if(!ObjectUtils.isEmpty(this.n_metricname_like)){
            this.getSearchCond().like("metricname", n_metricname_like);
        }
    }
	private String n_amountdatatype_eq;//[金额数据类型]
	public void setN_amountdatatype_eq(String n_amountdatatype_eq) {
        this.n_amountdatatype_eq = n_amountdatatype_eq;
        if(!ObjectUtils.isEmpty(this.n_amountdatatype_eq)){
            this.getSearchCond().eq("amountdatatype", n_amountdatatype_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("metricname", query)   
            );
		 }
	}
}




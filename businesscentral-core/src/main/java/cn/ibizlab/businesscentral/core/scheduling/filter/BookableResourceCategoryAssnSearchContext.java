package cn.ibizlab.businesscentral.core.scheduling.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.scheduling.domain.BookableResourceCategoryAssn;
/**
 * 关系型数据实体[BookableResourceCategoryAssn] 查询条件对象
 */
@Slf4j
@Data
public class BookableResourceCategoryAssnSearchContext extends QueryWrapperContext<BookableResourceCategoryAssn> {

	private Integer n_statuscode_eq;//[状态描述]
	public void setN_statuscode_eq(Integer n_statuscode_eq) {
        this.n_statuscode_eq = n_statuscode_eq;
        if(!ObjectUtils.isEmpty(this.n_statuscode_eq)){
            this.getSearchCond().eq("statuscode", n_statuscode_eq);
        }
    }
	private Integer n_statecode_eq;//[状态]
	public void setN_statecode_eq(Integer n_statecode_eq) {
        this.n_statecode_eq = n_statecode_eq;
        if(!ObjectUtils.isEmpty(this.n_statecode_eq)){
            this.getSearchCond().eq("statecode", n_statecode_eq);
        }
    }
	private String n_bookablerescategoryassnname_like;//[可预订资源的类别关联名称]
	public void setN_bookablerescategoryassnname_like(String n_bookablerescategoryassnname_like) {
        this.n_bookablerescategoryassnname_like = n_bookablerescategoryassnname_like;
        if(!ObjectUtils.isEmpty(this.n_bookablerescategoryassnname_like)){
            this.getSearchCond().like("bookablerescategoryassnname", n_bookablerescategoryassnname_like);
        }
    }
	private String n_resourcecategory_eq;//[资源类别]
	public void setN_resourcecategory_eq(String n_resourcecategory_eq) {
        this.n_resourcecategory_eq = n_resourcecategory_eq;
        if(!ObjectUtils.isEmpty(this.n_resourcecategory_eq)){
            this.getSearchCond().eq("resourcecategory", n_resourcecategory_eq);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }
	private String n_resource_eq;//[资源]
	public void setN_resource_eq(String n_resource_eq) {
        this.n_resource_eq = n_resource_eq;
        if(!ObjectUtils.isEmpty(this.n_resource_eq)){
            this.getSearchCond().eq("resource", n_resource_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("bookablerescategoryassnname", query)   
            );
		 }
	}
}




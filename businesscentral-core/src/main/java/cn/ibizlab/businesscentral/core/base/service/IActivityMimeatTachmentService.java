package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.ActivityMimeatTachment;
import cn.ibizlab.businesscentral.core.base.filter.ActivityMimeatTachmentSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[ActivityMimeatTachment] 服务对象接口
 */
public interface IActivityMimeatTachmentService extends IService<ActivityMimeatTachment>{

    boolean create(ActivityMimeatTachment et) ;
    void createBatch(List<ActivityMimeatTachment> list) ;
    boolean update(ActivityMimeatTachment et) ;
    void updateBatch(List<ActivityMimeatTachment> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    ActivityMimeatTachment get(String key) ;
    ActivityMimeatTachment getDraft(ActivityMimeatTachment et) ;
    boolean checkKey(ActivityMimeatTachment et) ;
    boolean save(ActivityMimeatTachment et) ;
    void saveBatch(List<ActivityMimeatTachment> list) ;
    Page<ActivityMimeatTachment> searchDefault(ActivityMimeatTachmentSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<ActivityMimeatTachment> getActivitymimeattachmentByIds(List<String> ids) ;
    List<ActivityMimeatTachment> getActivitymimeattachmentByEntities(List<ActivityMimeatTachment> entities) ;
}



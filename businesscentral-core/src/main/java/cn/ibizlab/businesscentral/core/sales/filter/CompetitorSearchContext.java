package cn.ibizlab.businesscentral.core.sales.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.sales.domain.Competitor;
/**
 * 关系型数据实体[Competitor] 查询条件对象
 */
@Slf4j
@Data
public class CompetitorSearchContext extends QueryWrapperContext<Competitor> {

	private String n_address2_shippingmethodcode_eq;//[地址 2: 送货方式]
	public void setN_address2_shippingmethodcode_eq(String n_address2_shippingmethodcode_eq) {
        this.n_address2_shippingmethodcode_eq = n_address2_shippingmethodcode_eq;
        if(!ObjectUtils.isEmpty(this.n_address2_shippingmethodcode_eq)){
            this.getSearchCond().eq("address2_shippingmethodcode", n_address2_shippingmethodcode_eq);
        }
    }
	private String n_address1_shippingmethodcode_eq;//[地址 1: 送货方式]
	public void setN_address1_shippingmethodcode_eq(String n_address1_shippingmethodcode_eq) {
        this.n_address1_shippingmethodcode_eq = n_address1_shippingmethodcode_eq;
        if(!ObjectUtils.isEmpty(this.n_address1_shippingmethodcode_eq)){
            this.getSearchCond().eq("address1_shippingmethodcode", n_address1_shippingmethodcode_eq);
        }
    }
	private String n_address2_addresstypecode_eq;//[地址 2: 地址类型]
	public void setN_address2_addresstypecode_eq(String n_address2_addresstypecode_eq) {
        this.n_address2_addresstypecode_eq = n_address2_addresstypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_address2_addresstypecode_eq)){
            this.getSearchCond().eq("address2_addresstypecode", n_address2_addresstypecode_eq);
        }
    }
	private String n_address1_addresstypecode_eq;//[地址 1: 地址类型]
	public void setN_address1_addresstypecode_eq(String n_address1_addresstypecode_eq) {
        this.n_address1_addresstypecode_eq = n_address1_addresstypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_address1_addresstypecode_eq)){
            this.getSearchCond().eq("address1_addresstypecode", n_address1_addresstypecode_eq);
        }
    }
	private String n_competitorname_like;//[竞争对手名称]
	public void setN_competitorname_like(String n_competitorname_like) {
        this.n_competitorname_like = n_competitorname_like;
        if(!ObjectUtils.isEmpty(this.n_competitorname_like)){
            this.getSearchCond().like("competitorname", n_competitorname_like);
        }
    }
	private String n_currencyname_eq;//[货币]
	public void setN_currencyname_eq(String n_currencyname_eq) {
        this.n_currencyname_eq = n_currencyname_eq;
        if(!ObjectUtils.isEmpty(this.n_currencyname_eq)){
            this.getSearchCond().eq("currencyname", n_currencyname_eq);
        }
    }
	private String n_currencyname_like;//[货币]
	public void setN_currencyname_like(String n_currencyname_like) {
        this.n_currencyname_like = n_currencyname_like;
        if(!ObjectUtils.isEmpty(this.n_currencyname_like)){
            this.getSearchCond().like("currencyname", n_currencyname_like);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("competitorname", query)   
            );
		 }
	}
}




package cn.ibizlab.businesscentral.core.scheduling.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.scheduling.domain.RatingModel;
import cn.ibizlab.businesscentral.core.scheduling.filter.RatingModelSearchContext;
import cn.ibizlab.businesscentral.core.scheduling.service.IRatingModelService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.scheduling.mapper.RatingModelMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[评分模型] 服务对象接口实现
 */
@Slf4j
@Service("RatingModelServiceImpl")
public class RatingModelServiceImpl extends ServiceImpl<RatingModelMapper, RatingModel> implements IRatingModelService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.scheduling.service.IRatingValueService ratingvalueService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(RatingModel et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getRatingmodelid()),et);
        return true;
    }

    @Override
    public void createBatch(List<RatingModel> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(RatingModel et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("ratingmodelid",et.getRatingmodelid())))
            return false;
        CachedBeanCopier.copy(get(et.getRatingmodelid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<RatingModel> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public RatingModel get(String key) {
        RatingModel et = getById(key);
        if(et==null){
            et=new RatingModel();
            et.setRatingmodelid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public RatingModel getDraft(RatingModel et) {
        return et;
    }

    @Override
    public boolean checkKey(RatingModel et) {
        return (!ObjectUtils.isEmpty(et.getRatingmodelid()))&&(!Objects.isNull(this.getById(et.getRatingmodelid())));
    }
    @Override
    @Transactional
    public boolean save(RatingModel et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(RatingModel et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<RatingModel> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<RatingModel> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<RatingModel> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<RatingModel>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<RatingModel> searchDefault(RatingModelSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<RatingModel> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<RatingModel>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<RatingModel> getRatingmodelByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<RatingModel> getRatingmodelByEntities(List<RatingModel> entities) {
        List ids =new ArrayList();
        for(RatingModel entity : entities){
            Serializable id=entity.getRatingmodelid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




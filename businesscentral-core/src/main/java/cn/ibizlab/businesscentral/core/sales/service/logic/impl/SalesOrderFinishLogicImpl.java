package cn.ibizlab.businesscentral.core.sales.service.logic.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;

import cn.ibizlab.businesscentral.core.sales.service.logic.ISalesOrderFinishLogic;
import cn.ibizlab.businesscentral.core.sales.domain.SalesOrder;

/**
 * 关系型数据实体[Finish] 对象
 */
@Slf4j
@Service
public class SalesOrderFinishLogicImpl implements ISalesOrderFinishLogic{

    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private cn.ibizlab.businesscentral.core.sales.service.ISalesOrderService salesorderservice;

    public cn.ibizlab.businesscentral.core.sales.service.ISalesOrderService getSalesorderService() {
        return this.salesorderservice;
    }


    @Autowired
    private cn.ibizlab.businesscentral.core.sales.service.ISalesOrderService iBzSysDefaultService;

    public cn.ibizlab.businesscentral.core.sales.service.ISalesOrderService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    public void execute(SalesOrder et){

          KieSession kieSession = null;
        try{
           kieSession=kieContainer.newKieSession();
           kieSession.insert(et); 
           kieSession.setGlobal("salesorderfinishdefault",et);
           kieSession.setGlobal("salesorderservice",salesorderservice);
           kieSession.setGlobal("iBzSysSalesorderDefaultService",iBzSysDefaultService);
           kieSession.setGlobal("curuser", cn.ibizlab.businesscentral.util.security.AuthenticationUser.getAuthenticationUser());
           kieSession.startProcess("cn.ibizlab.businesscentral.core.sales.service.logic.salesorderfinish");

        }catch(Exception e){
            throw new RuntimeException("执行[完成订单]处理逻辑发生异常"+e);
        }finally {
            if(kieSession!=null)
            kieSession.destroy();
        }
    }

}

package cn.ibizlab.businesscentral.core.sales.service.logic.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;

import cn.ibizlab.businesscentral.core.sales.service.logic.IGoalActiveLogic;
import cn.ibizlab.businesscentral.core.sales.domain.Goal;

/**
 * 关系型数据实体[Active] 对象
 */
@Slf4j
@Service
public class GoalActiveLogicImpl implements IGoalActiveLogic{

    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private cn.ibizlab.businesscentral.core.sales.service.IGoalService goalservice;

    public cn.ibizlab.businesscentral.core.sales.service.IGoalService getGoalService() {
        return this.goalservice;
    }


    @Autowired
    private cn.ibizlab.businesscentral.core.sales.service.IGoalService iBzSysDefaultService;

    public cn.ibizlab.businesscentral.core.sales.service.IGoalService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    public void execute(Goal et){

          KieSession kieSession = null;
        try{
           kieSession=kieContainer.newKieSession();
           kieSession.insert(et); 
           kieSession.setGlobal("goalactivedefault",et);
           kieSession.setGlobal("goalservice",goalservice);
           kieSession.setGlobal("iBzSysGoalDefaultService",iBzSysDefaultService);
           kieSession.setGlobal("curuser", cn.ibizlab.businesscentral.util.security.AuthenticationUser.getAuthenticationUser());
           kieSession.startProcess("cn.ibizlab.businesscentral.core.sales.service.logic.goalactive");

        }catch(Exception e){
            throw new RuntimeException("执行[激活]处理逻辑发生异常"+e);
        }finally {
            if(kieSession!=null)
            kieSession.destroy();
        }
    }

}

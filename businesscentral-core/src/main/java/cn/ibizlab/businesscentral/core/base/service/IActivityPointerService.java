package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.ActivityPointer;
import cn.ibizlab.businesscentral.core.base.filter.ActivityPointerSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[ActivityPointer] 服务对象接口
 */
public interface IActivityPointerService extends IService<ActivityPointer>{

    boolean create(ActivityPointer et) ;
    void createBatch(List<ActivityPointer> list) ;
    boolean update(ActivityPointer et) ;
    void updateBatch(List<ActivityPointer> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    ActivityPointer get(String key) ;
    ActivityPointer getDraft(ActivityPointer et) ;
    boolean checkKey(ActivityPointer et) ;
    boolean save(ActivityPointer et) ;
    void saveBatch(List<ActivityPointer> list) ;
    Page<ActivityPointer> searchByParentKey(ActivityPointerSearchContext context) ;
    Page<ActivityPointer> searchDefault(ActivityPointerSearchContext context) ;
    List<ActivityPointer> selectByServiceid(String serviceid) ;
    void removeByServiceid(String serviceid) ;
    List<ActivityPointer> selectBySlaid(String slaid) ;
    void removeBySlaid(String slaid) ;
    List<ActivityPointer> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<ActivityPointer> getActivitypointerByIds(List<String> ids) ;
    List<ActivityPointer> getActivitypointerByEntities(List<ActivityPointer> entities) ;
}



package cn.ibizlab.businesscentral.core.sales.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.sales.domain.OpportunityCompetitor;
import cn.ibizlab.businesscentral.core.sales.filter.OpportunityCompetitorSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[OpportunityCompetitor] 服务对象接口
 */
public interface IOpportunityCompetitorService extends IService<OpportunityCompetitor>{

    boolean create(OpportunityCompetitor et) ;
    void createBatch(List<OpportunityCompetitor> list) ;
    boolean update(OpportunityCompetitor et) ;
    void updateBatch(List<OpportunityCompetitor> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    OpportunityCompetitor get(String key) ;
    OpportunityCompetitor getDraft(OpportunityCompetitor et) ;
    boolean checkKey(OpportunityCompetitor et) ;
    boolean save(OpportunityCompetitor et) ;
    void saveBatch(List<OpportunityCompetitor> list) ;
    Page<OpportunityCompetitor> searchDefault(OpportunityCompetitorSearchContext context) ;
    List<OpportunityCompetitor> selectByEntity2id(String competitorid) ;
    void removeByEntity2id(String competitorid) ;
    List<OpportunityCompetitor> selectByEntityid(String opportunityid) ;
    void removeByEntityid(String opportunityid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<OpportunityCompetitor> getOpportunitycompetitorByIds(List<String> ids) ;
    List<OpportunityCompetitor> getOpportunitycompetitorByEntities(List<OpportunityCompetitor> entities) ;
}



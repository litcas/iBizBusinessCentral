package cn.ibizlab.businesscentral.core.service.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.service.domain.Entitlement;
import cn.ibizlab.businesscentral.core.service.filter.EntitlementSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Entitlement] 服务对象接口
 */
public interface IEntitlementService extends IService<Entitlement>{

    boolean create(Entitlement et) ;
    void createBatch(List<Entitlement> list) ;
    boolean update(Entitlement et) ;
    void updateBatch(List<Entitlement> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Entitlement get(String key) ;
    Entitlement getDraft(Entitlement et) ;
    boolean checkKey(Entitlement et) ;
    boolean save(Entitlement et) ;
    void saveBatch(List<Entitlement> list) ;
    Page<Entitlement> searchDefault(EntitlementSearchContext context) ;
    List<Entitlement> selectByEntitlementtemplateid(String entitlementtemplateid) ;
    void removeByEntitlementtemplateid(String entitlementtemplateid) ;
    List<Entitlement> selectBySlaid(String slaid) ;
    void removeBySlaid(String slaid) ;
    List<Entitlement> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Entitlement> getEntitlementByIds(List<String> ids) ;
    List<Entitlement> getEntitlementByEntities(List<Entitlement> entities) ;
}



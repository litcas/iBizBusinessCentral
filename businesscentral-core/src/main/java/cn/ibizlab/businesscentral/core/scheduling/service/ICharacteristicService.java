package cn.ibizlab.businesscentral.core.scheduling.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.scheduling.domain.Characteristic;
import cn.ibizlab.businesscentral.core.scheduling.filter.CharacteristicSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Characteristic] 服务对象接口
 */
public interface ICharacteristicService extends IService<Characteristic>{

    boolean create(Characteristic et) ;
    void createBatch(List<Characteristic> list) ;
    boolean update(Characteristic et) ;
    void updateBatch(List<Characteristic> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Characteristic get(String key) ;
    Characteristic getDraft(Characteristic et) ;
    boolean checkKey(Characteristic et) ;
    boolean save(Characteristic et) ;
    void saveBatch(List<Characteristic> list) ;
    Page<Characteristic> searchDefault(CharacteristicSearchContext context) ;
    List<Characteristic> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Characteristic> getCharacteristicByIds(List<String> ids) ;
    List<Characteristic> getCharacteristicByEntities(List<Characteristic> entities) ;
}



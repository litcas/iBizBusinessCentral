package cn.ibizlab.businesscentral.core.service.service.logic;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.businesscentral.core.service.domain.Incident;

/**
 * 关系型数据实体[Resolve] 对象
 */
public interface IIncidentResolveLogic {

    void execute(Incident et) ;

}

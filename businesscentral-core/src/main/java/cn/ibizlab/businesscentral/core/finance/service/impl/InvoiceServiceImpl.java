package cn.ibizlab.businesscentral.core.finance.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.finance.domain.Invoice;
import cn.ibizlab.businesscentral.core.finance.filter.InvoiceSearchContext;
import cn.ibizlab.businesscentral.core.finance.service.IInvoiceService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.finance.mapper.InvoiceMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[发票] 服务对象接口实现
 */
@Slf4j
@Service("InvoiceServiceImpl")
public class InvoiceServiceImpl extends ServiceImpl<InvoiceMapper, Invoice> implements IInvoiceService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.finance.service.IInvoiceDetailService invoicedetailService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IOpportunityService opportunityService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.product.service.IPriceLevelService pricelevelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.ISalesOrderService salesorderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ISlaService slaService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.finance.service.logic.IInvoiceCancelLogic cancelLogic;

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.finance.service.logic.IInvoicePaidLogic paidLogic;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(Invoice et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getInvoiceid()),et);
        return true;
    }

    @Override
    public void createBatch(List<Invoice> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Invoice et) {
        fillParentData(et);
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("invoiceid",et.getInvoiceid())))
            return false;
        CachedBeanCopier.copy(get(et.getInvoiceid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<Invoice> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Invoice get(String key) {
        Invoice et = getById(key);
        if(et==null){
            et=new Invoice();
            et.setInvoiceid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Invoice getDraft(Invoice et) {
        fillParentData(et);
        return et;
    }

    @Override
    @Transactional
    public Invoice cancel(Invoice et) {
        cancelLogic.execute(et);
         return et ;
    }

    @Override
    public boolean checkKey(Invoice et) {
        return (!ObjectUtils.isEmpty(et.getInvoiceid()))&&(!Objects.isNull(this.getById(et.getInvoiceid())));
    }
    @Override
    @Transactional
    public Invoice finish(Invoice et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public Invoice paid(Invoice et) {
        paidLogic.execute(et);
         return et ;
    }

    @Override
    @Transactional
    public boolean save(Invoice et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Invoice et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<Invoice> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<Invoice> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Invoice> selectByOpportunityid(String opportunityid) {
        return baseMapper.selectByOpportunityid(opportunityid);
    }

    @Override
    public void removeByOpportunityid(String opportunityid) {
        this.remove(new QueryWrapper<Invoice>().eq("opportunityid",opportunityid));
    }

	@Override
    public List<Invoice> selectByPricelevelid(String pricelevelid) {
        return baseMapper.selectByPricelevelid(pricelevelid);
    }

    @Override
    public void removeByPricelevelid(String pricelevelid) {
        this.remove(new QueryWrapper<Invoice>().eq("pricelevelid",pricelevelid));
    }

	@Override
    public List<Invoice> selectBySalesorderid(String salesorderid) {
        return baseMapper.selectBySalesorderid(salesorderid);
    }

    @Override
    public void removeBySalesorderid(String salesorderid) {
        this.remove(new QueryWrapper<Invoice>().eq("salesorderid",salesorderid));
    }

	@Override
    public List<Invoice> selectBySlaid(String slaid) {
        return baseMapper.selectBySlaid(slaid);
    }

    @Override
    public void removeBySlaid(String slaid) {
        this.remove(new QueryWrapper<Invoice>().eq("slaid",slaid));
    }

	@Override
    public List<Invoice> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<Invoice>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 ByParentKey
     */
    @Override
    public Page<Invoice> searchByParentKey(InvoiceSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Invoice> pages=baseMapper.searchByParentKey(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Invoice>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 已取消
     */
    @Override
    public Page<Invoice> searchCancel(InvoiceSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Invoice> pages=baseMapper.searchCancel(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Invoice>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<Invoice> searchDefault(InvoiceSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Invoice> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Invoice>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 已支付
     */
    @Override
    public Page<Invoice> searchPaid(InvoiceSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Invoice> pages=baseMapper.searchPaid(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Invoice>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Invoice et){
        //实体关系[DER1N_INVOICE__OPPORTUNITY__OPPORTUNITYID]
        if(!ObjectUtils.isEmpty(et.getOpportunityid())){
            cn.ibizlab.businesscentral.core.sales.domain.Opportunity opportunity=et.getOpportunity();
            if(ObjectUtils.isEmpty(opportunity)){
                cn.ibizlab.businesscentral.core.sales.domain.Opportunity majorEntity=opportunityService.get(et.getOpportunityid());
                et.setOpportunity(majorEntity);
                opportunity=majorEntity;
            }
            et.setOpportunityname(opportunity.getOpportunityname());
        }
        //实体关系[DER1N_INVOICE__PRICELEVEL__PRICELEVELID]
        if(!ObjectUtils.isEmpty(et.getPricelevelid())){
            cn.ibizlab.businesscentral.core.product.domain.PriceLevel pricelevel=et.getPricelevel();
            if(ObjectUtils.isEmpty(pricelevel)){
                cn.ibizlab.businesscentral.core.product.domain.PriceLevel majorEntity=pricelevelService.get(et.getPricelevelid());
                et.setPricelevel(majorEntity);
                pricelevel=majorEntity;
            }
            et.setPricelevelname(pricelevel.getPricelevelname());
        }
        //实体关系[DER1N_INVOICE__SALESORDER__SALESORDERID]
        if(!ObjectUtils.isEmpty(et.getSalesorderid())){
            cn.ibizlab.businesscentral.core.sales.domain.SalesOrder salesorder=et.getSalesorder();
            if(ObjectUtils.isEmpty(salesorder)){
                cn.ibizlab.businesscentral.core.sales.domain.SalesOrder majorEntity=salesorderService.get(et.getSalesorderid());
                et.setSalesorder(majorEntity);
                salesorder=majorEntity;
            }
            et.setSalesordername(salesorder.getSalesordername());
        }
        //实体关系[DER1N_INVOICE__SLA__SLAID]
        if(!ObjectUtils.isEmpty(et.getSlaid())){
            cn.ibizlab.businesscentral.core.base.domain.Sla sla=et.getSla();
            if(ObjectUtils.isEmpty(sla)){
                cn.ibizlab.businesscentral.core.base.domain.Sla majorEntity=slaService.get(et.getSlaid());
                et.setSla(majorEntity);
                sla=majorEntity;
            }
            et.setSlaname(sla.getSlaname());
        }
        //实体关系[DER1N_INVOICE__TRANSACTIONCURRENCY__TRANSACTIONCURRENCYID]
        if(!ObjectUtils.isEmpty(et.getTransactioncurrencyid())){
            cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency=et.getTransactioncurrency();
            if(ObjectUtils.isEmpty(transactioncurrency)){
                cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency majorEntity=transactioncurrencyService.get(et.getTransactioncurrencyid());
                et.setTransactioncurrency(majorEntity);
                transactioncurrency=majorEntity;
            }
            et.setCurrencyname(transactioncurrency.getCurrencyname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Invoice> getInvoiceByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Invoice> getInvoiceByEntities(List<Invoice> entities) {
        List ids =new ArrayList();
        for(Invoice entity : entities){
            Serializable id=entity.getInvoiceid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




package cn.ibizlab.businesscentral.core.base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[活动时间附件]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "ACTIVITYMIMEATTACHMENT",resultMap = "ActivityMimeatTachmentResultMap")
public class ActivityMimeatTachment extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * FileName
     */
    @TableField(value = "filename")
    @JSONField(name = "filename")
    @JsonProperty("filename")
    private String filename;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * ComponentState
     */
    @TableField(value = "componentstate")
    @JSONField(name = "componentstate")
    @JsonProperty("componentstate")
    private String componentstate;
    /**
     * SolutionId
     */
    @TableField(value = "solutionid")
    @JSONField(name = "solutionid")
    @JsonProperty("solutionid")
    private String solutionid;
    /**
     * ObjectId
     */
    @TableField(value = "objectid")
    @JSONField(name = "objectid")
    @JsonProperty("objectid")
    private String objectid;
    /**
     * Body
     */
    @TableField(value = "body")
    @JSONField(name = "body")
    @JsonProperty("body")
    private String body;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * AttachmentContentId
     */
    @TableField(value = "attachmentcontentid")
    @JSONField(name = "attachmentcontentid")
    @JsonProperty("attachmentcontentid")
    private String attachmentcontentid;
    /**
     * ActivityMimeAttachmentIdUnique
     */
    @TableField(value = "activitymimeattachmentidunique")
    @JSONField(name = "activitymimeattachmentidunique")
    @JsonProperty("activitymimeattachmentidunique")
    private String activitymimeattachmentidunique;
    /**
     * ActivityId
     */
    @TableField(value = "activityid")
    @JSONField(name = "activityid")
    @JsonProperty("activityid")
    private String activityid;
    /**
     * 负责人
     */
    @TableField(value = "ownerid")
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;
    /**
     * Managed
     */
    @DEField(defaultValue = "0")
    @TableField(value = "managed")
    @JSONField(name = "managed")
    @JsonProperty("managed")
    private Integer managed;
    /**
     * Subject
     */
    @TableField(value = "subject")
    @JSONField(name = "subject")
    @JsonProperty("subject")
    private String subject;
    /**
     * VersionNumber
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * AnonymousLink
     */
    @TableField(value = "anonymouslink")
    @JSONField(name = "anonymouslink")
    @JsonProperty("anonymouslink")
    private String anonymouslink;
    /**
     * MimeType
     */
    @TableField(value = "mimetype")
    @JSONField(name = "mimetype")
    @JsonProperty("mimetype")
    private String mimetype;
    /**
     * ObjectTypeCode
     */
    @TableField(value = "objecttypecode")
    @JSONField(name = "objecttypecode")
    @JsonProperty("objecttypecode")
    private String objecttypecode;
    /**
     * FileSize
     */
    @TableField(value = "filesize")
    @JSONField(name = "filesize")
    @JsonProperty("filesize")
    private Integer filesize;
    /**
     * OverwriteTime
     */
    @TableField(value = "overwritetime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overwritetime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overwritetime")
    private Timestamp overwritetime;
    /**
     * SupportingSolutionId
     */
    @TableField(value = "supportingsolutionid")
    @JSONField(name = "supportingsolutionid")
    @JsonProperty("supportingsolutionid")
    private String supportingsolutionid;
    /**
     * ActivityMimeAttachmentId
     */
    @DEField(isKeyField=true)
    @TableId(value= "activitymimeattachmentid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "activitymimeattachmentid")
    @JsonProperty("activitymimeattachmentid")
    private String activitymimeattachmentid;
    /**
     * ActivitySubject
     */
    @TableField(value = "activitysubject")
    @JSONField(name = "activitysubject")
    @JsonProperty("activitysubject")
    private String activitysubject;
    /**
     * 负责人类型
     */
    @TableField(value = "ownertype")
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;
    /**
     * AttachmentNumber
     */
    @TableField(value = "attachmentnumber")
    @JSONField(name = "attachmentnumber")
    @JsonProperty("attachmentnumber")
    private Integer attachmentnumber;
    /**
     * Followed
     */
    @DEField(defaultValue = "0")
    @TableField(value = "followed")
    @JSONField(name = "followed")
    @JsonProperty("followed")
    private Integer followed;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * AttachmentId
     */
    @TableField(value = "attachmentid")
    @JSONField(name = "attachmentid")
    @JsonProperty("attachmentid")
    private String attachmentid;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;



    /**
     * 设置 [FileName]
     */
    public void setFilename(String filename){
        this.filename = filename ;
        this.modify("filename",filename);
    }

    /**
     * 设置 [ComponentState]
     */
    public void setComponentstate(String componentstate){
        this.componentstate = componentstate ;
        this.modify("componentstate",componentstate);
    }

    /**
     * 设置 [SolutionId]
     */
    public void setSolutionid(String solutionid){
        this.solutionid = solutionid ;
        this.modify("solutionid",solutionid);
    }

    /**
     * 设置 [ObjectId]
     */
    public void setObjectid(String objectid){
        this.objectid = objectid ;
        this.modify("objectid",objectid);
    }

    /**
     * 设置 [Body]
     */
    public void setBody(String body){
        this.body = body ;
        this.modify("body",body);
    }

    /**
     * 设置 [AttachmentContentId]
     */
    public void setAttachmentcontentid(String attachmentcontentid){
        this.attachmentcontentid = attachmentcontentid ;
        this.modify("attachmentcontentid",attachmentcontentid);
    }

    /**
     * 设置 [ActivityMimeAttachmentIdUnique]
     */
    public void setActivitymimeattachmentidunique(String activitymimeattachmentidunique){
        this.activitymimeattachmentidunique = activitymimeattachmentidunique ;
        this.modify("activitymimeattachmentidunique",activitymimeattachmentidunique);
    }

    /**
     * 设置 [ActivityId]
     */
    public void setActivityid(String activityid){
        this.activityid = activityid ;
        this.modify("activityid",activityid);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnerid(String ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [Managed]
     */
    public void setManaged(Integer managed){
        this.managed = managed ;
        this.modify("managed",managed);
    }

    /**
     * 设置 [Subject]
     */
    public void setSubject(String subject){
        this.subject = subject ;
        this.modify("subject",subject);
    }

    /**
     * 设置 [VersionNumber]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [AnonymousLink]
     */
    public void setAnonymouslink(String anonymouslink){
        this.anonymouslink = anonymouslink ;
        this.modify("anonymouslink",anonymouslink);
    }

    /**
     * 设置 [MimeType]
     */
    public void setMimetype(String mimetype){
        this.mimetype = mimetype ;
        this.modify("mimetype",mimetype);
    }

    /**
     * 设置 [ObjectTypeCode]
     */
    public void setObjecttypecode(String objecttypecode){
        this.objecttypecode = objecttypecode ;
        this.modify("objecttypecode",objecttypecode);
    }

    /**
     * 设置 [FileSize]
     */
    public void setFilesize(Integer filesize){
        this.filesize = filesize ;
        this.modify("filesize",filesize);
    }

    /**
     * 设置 [OverwriteTime]
     */
    public void setOverwritetime(Timestamp overwritetime){
        this.overwritetime = overwritetime ;
        this.modify("overwritetime",overwritetime);
    }

    /**
     * 格式化日期 [OverwriteTime]
     */
    public String formatOverwritetime(){
        if (this.overwritetime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overwritetime);
    }
    /**
     * 设置 [SupportingSolutionId]
     */
    public void setSupportingsolutionid(String supportingsolutionid){
        this.supportingsolutionid = supportingsolutionid ;
        this.modify("supportingsolutionid",supportingsolutionid);
    }

    /**
     * 设置 [ActivitySubject]
     */
    public void setActivitysubject(String activitysubject){
        this.activitysubject = activitysubject ;
        this.modify("activitysubject",activitysubject);
    }

    /**
     * 设置 [负责人类型]
     */
    public void setOwnertype(String ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [AttachmentNumber]
     */
    public void setAttachmentnumber(Integer attachmentnumber){
        this.attachmentnumber = attachmentnumber ;
        this.modify("attachmentnumber",attachmentnumber);
    }

    /**
     * 设置 [Followed]
     */
    public void setFollowed(Integer followed){
        this.followed = followed ;
        this.modify("followed",followed);
    }

    /**
     * 设置 [AttachmentId]
     */
    public void setAttachmentid(String attachmentid){
        this.attachmentid = attachmentid ;
        this.modify("attachmentid",attachmentid);
    }


}



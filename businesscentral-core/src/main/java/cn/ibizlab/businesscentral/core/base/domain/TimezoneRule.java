package cn.ibizlab.businesscentral.core.base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[时区规则]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "TIMEZONERULE",resultMap = "TimezoneRuleResultMap")
public class TimezoneRule extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 标准时间中的年份
     */
    @TableField(value = "standardyear")
    @JSONField(name = "standardyear")
    @JsonProperty("standardyear")
    private Integer standardyear;
    /**
     * 夏令时中的年份
     */
    @TableField(value = "daylightyear")
    @JSONField(name = "daylightyear")
    @JsonProperty("daylightyear")
    private Integer daylightyear;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 标准偏差
     */
    @TableField(value = "standardbias")
    @JSONField(name = "standardbias")
    @JsonProperty("standardbias")
    private Integer standardbias;
    /**
     * 标准时间中的分钟
     */
    @TableField(value = "standardminute")
    @JSONField(name = "standardminute")
    @JsonProperty("standardminute")
    private Integer standardminute;
    /**
     * 标准时间中的某一天
     */
    @TableField(value = "standardday")
    @JSONField(name = "standardday")
    @JsonProperty("standardday")
    private Integer standardday;
    /**
     * 时区规则版本号
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * 夏令时中的月份
     */
    @TableField(value = "daylightmonth")
    @JSONField(name = "daylightmonth")
    @JsonProperty("daylightmonth")
    private Integer daylightmonth;
    /**
     * 夏令偏差
     */
    @TableField(value = "daylightbias")
    @JSONField(name = "daylightbias")
    @JsonProperty("daylightbias")
    private Integer daylightbias;
    /**
     * 标准时间中的秒钟
     */
    @TableField(value = "standardsecond")
    @JSONField(name = "standardsecond")
    @JsonProperty("standardsecond")
    private Integer standardsecond;
    /**
     * 偏差
     */
    @TableField(value = "bias")
    @JSONField(name = "bias")
    @JsonProperty("bias")
    private Integer bias;
    /**
     * 夏令时中的小时
     */
    @TableField(value = "daylighthour")
    @JSONField(name = "daylighthour")
    @JsonProperty("daylighthour")
    private Integer daylighthour;
    /**
     * 夏令时中的分钟
     */
    @TableField(value = "daylightminute")
    @JSONField(name = "daylightminute")
    @JsonProperty("daylightminute")
    private Integer daylightminute;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 夏令时中的秒钟
     */
    @TableField(value = "daylightsecond")
    @JSONField(name = "daylightsecond")
    @JsonProperty("daylightsecond")
    private Integer daylightsecond;
    /**
     * 生效日期时间
     */
    @TableField(value = "effectivedatetime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "effectivedatetime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("effectivedatetime")
    private Timestamp effectivedatetime;
    /**
     * 时区规则
     */
    @DEField(isKeyField=true)
    @TableId(value= "timezoneruleid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "timezoneruleid")
    @JsonProperty("timezoneruleid")
    private String timezoneruleid;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 标准时间中的小时
     */
    @TableField(value = "standardhour")
    @JSONField(name = "standardhour")
    @JsonProperty("standardhour")
    private Integer standardhour;
    /**
     * 夏令时中的某一天
     */
    @TableField(value = "daylightday")
    @JSONField(name = "daylightday")
    @JsonProperty("daylightday")
    private Integer daylightday;
    /**
     * VersionNumber
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 标准时间中的一周中的某一天
     */
    @TableField(value = "standarddayofweek")
    @JSONField(name = "standarddayofweek")
    @JsonProperty("standarddayofweek")
    private Integer standarddayofweek;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 夏令时中的一周中的某一天
     */
    @TableField(value = "daylightdayofweek")
    @JSONField(name = "daylightdayofweek")
    @JsonProperty("daylightdayofweek")
    private Integer daylightdayofweek;
    /**
     * 标准时间中的月份
     */
    @TableField(value = "standardmonth")
    @JSONField(name = "standardmonth")
    @JsonProperty("standardmonth")
    private Integer standardmonth;
    /**
     * 时区定义
     */
    @TableField(value = "timezonedefinitionid")
    @JSONField(name = "timezonedefinitionid")
    @JsonProperty("timezonedefinitionid")
    private String timezonedefinitionid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.TimezoneDefinition timezonedefinition;



    /**
     * 设置 [标准时间中的年份]
     */
    public void setStandardyear(Integer standardyear){
        this.standardyear = standardyear ;
        this.modify("standardyear",standardyear);
    }

    /**
     * 设置 [夏令时中的年份]
     */
    public void setDaylightyear(Integer daylightyear){
        this.daylightyear = daylightyear ;
        this.modify("daylightyear",daylightyear);
    }

    /**
     * 设置 [标准偏差]
     */
    public void setStandardbias(Integer standardbias){
        this.standardbias = standardbias ;
        this.modify("standardbias",standardbias);
    }

    /**
     * 设置 [标准时间中的分钟]
     */
    public void setStandardminute(Integer standardminute){
        this.standardminute = standardminute ;
        this.modify("standardminute",standardminute);
    }

    /**
     * 设置 [标准时间中的某一天]
     */
    public void setStandardday(Integer standardday){
        this.standardday = standardday ;
        this.modify("standardday",standardday);
    }

    /**
     * 设置 [时区规则版本号]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [夏令时中的月份]
     */
    public void setDaylightmonth(Integer daylightmonth){
        this.daylightmonth = daylightmonth ;
        this.modify("daylightmonth",daylightmonth);
    }

    /**
     * 设置 [夏令偏差]
     */
    public void setDaylightbias(Integer daylightbias){
        this.daylightbias = daylightbias ;
        this.modify("daylightbias",daylightbias);
    }

    /**
     * 设置 [标准时间中的秒钟]
     */
    public void setStandardsecond(Integer standardsecond){
        this.standardsecond = standardsecond ;
        this.modify("standardsecond",standardsecond);
    }

    /**
     * 设置 [偏差]
     */
    public void setBias(Integer bias){
        this.bias = bias ;
        this.modify("bias",bias);
    }

    /**
     * 设置 [夏令时中的小时]
     */
    public void setDaylighthour(Integer daylighthour){
        this.daylighthour = daylighthour ;
        this.modify("daylighthour",daylighthour);
    }

    /**
     * 设置 [夏令时中的分钟]
     */
    public void setDaylightminute(Integer daylightminute){
        this.daylightminute = daylightminute ;
        this.modify("daylightminute",daylightminute);
    }

    /**
     * 设置 [夏令时中的秒钟]
     */
    public void setDaylightsecond(Integer daylightsecond){
        this.daylightsecond = daylightsecond ;
        this.modify("daylightsecond",daylightsecond);
    }

    /**
     * 设置 [生效日期时间]
     */
    public void setEffectivedatetime(Timestamp effectivedatetime){
        this.effectivedatetime = effectivedatetime ;
        this.modify("effectivedatetime",effectivedatetime);
    }

    /**
     * 格式化日期 [生效日期时间]
     */
    public String formatEffectivedatetime(){
        if (this.effectivedatetime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(effectivedatetime);
    }
    /**
     * 设置 [标准时间中的小时]
     */
    public void setStandardhour(Integer standardhour){
        this.standardhour = standardhour ;
        this.modify("standardhour",standardhour);
    }

    /**
     * 设置 [夏令时中的某一天]
     */
    public void setDaylightday(Integer daylightday){
        this.daylightday = daylightday ;
        this.modify("daylightday",daylightday);
    }

    /**
     * 设置 [VersionNumber]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [标准时间中的一周中的某一天]
     */
    public void setStandarddayofweek(Integer standarddayofweek){
        this.standarddayofweek = standarddayofweek ;
        this.modify("standarddayofweek",standarddayofweek);
    }

    /**
     * 设置 [夏令时中的一周中的某一天]
     */
    public void setDaylightdayofweek(Integer daylightdayofweek){
        this.daylightdayofweek = daylightdayofweek ;
        this.modify("daylightdayofweek",daylightdayofweek);
    }

    /**
     * 设置 [标准时间中的月份]
     */
    public void setStandardmonth(Integer standardmonth){
        this.standardmonth = standardmonth ;
        this.modify("standardmonth",standardmonth);
    }

    /**
     * 设置 [时区定义]
     */
    public void setTimezonedefinitionid(String timezonedefinitionid){
        this.timezonedefinitionid = timezonedefinitionid ;
        this.modify("timezonedefinitionid",timezonedefinitionid);
    }


}



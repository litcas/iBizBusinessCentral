package cn.ibizlab.businesscentral.core.marketing.service.logic;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.businesscentral.core.marketing.domain.Campaign;

/**
 * 关系型数据实体[Stop] 对象
 */
public interface ICampaignStopLogic {

    void execute(Campaign et) ;

}

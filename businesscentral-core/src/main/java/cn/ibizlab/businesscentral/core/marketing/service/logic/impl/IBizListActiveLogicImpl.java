package cn.ibizlab.businesscentral.core.marketing.service.logic.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;

import cn.ibizlab.businesscentral.core.marketing.service.logic.IIBizListActiveLogic;
import cn.ibizlab.businesscentral.core.marketing.domain.IBizList;

/**
 * 关系型数据实体[Active] 对象
 */
@Slf4j
@Service
public class IBizListActiveLogicImpl implements IIBizListActiveLogic{

    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private cn.ibizlab.businesscentral.core.marketing.service.IIBizListService ibizlistservice;

    public cn.ibizlab.businesscentral.core.marketing.service.IIBizListService getIbizlistService() {
        return this.ibizlistservice;
    }


    @Autowired
    private cn.ibizlab.businesscentral.core.marketing.service.IIBizListService iBzSysDefaultService;

    public cn.ibizlab.businesscentral.core.marketing.service.IIBizListService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    public void execute(IBizList et){

          KieSession kieSession = null;
        try{
           kieSession=kieContainer.newKieSession();
           kieSession.insert(et); 
           kieSession.setGlobal("ibizlistactivedefault",et);
           kieSession.setGlobal("ibizlistservice",ibizlistservice);
           kieSession.setGlobal("iBzSysIbizlistDefaultService",iBzSysDefaultService);
           kieSession.setGlobal("curuser", cn.ibizlab.businesscentral.util.security.AuthenticationUser.getAuthenticationUser());
           kieSession.startProcess("cn.ibizlab.businesscentral.core.marketing.service.logic.ibizlistactive");

        }catch(Exception e){
            throw new RuntimeException("执行[激活]处理逻辑发生异常"+e);
        }finally {
            if(kieSession!=null)
            kieSession.destroy();
        }
    }

}

package cn.ibizlab.businesscentral.core.product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.product.domain.PriceLevel;
import cn.ibizlab.businesscentral.core.product.filter.PriceLevelSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[PriceLevel] 服务对象接口
 */
public interface IPriceLevelService extends IService<PriceLevel>{

    boolean create(PriceLevel et) ;
    void createBatch(List<PriceLevel> list) ;
    boolean update(PriceLevel et) ;
    void updateBatch(List<PriceLevel> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    PriceLevel get(String key) ;
    PriceLevel getDraft(PriceLevel et) ;
    boolean checkKey(PriceLevel et) ;
    boolean save(PriceLevel et) ;
    void saveBatch(List<PriceLevel> list) ;
    Page<PriceLevel> searchDefault(PriceLevelSearchContext context) ;
    List<PriceLevel> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<PriceLevel> getPricelevelByIds(List<String> ids) ;
    List<PriceLevel> getPricelevelByEntities(List<PriceLevel> entities) ;
}



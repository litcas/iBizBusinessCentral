package cn.ibizlab.businesscentral.core.marketing.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.marketing.domain.ListContact;
import cn.ibizlab.businesscentral.core.marketing.filter.ListContactSearchContext;
import cn.ibizlab.businesscentral.core.marketing.service.IListContactService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.marketing.mapper.ListContactMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[营销列表-联系人] 服务对象接口实现
 */
@Slf4j
@Service("ListContactServiceImpl")
public class ListContactServiceImpl extends ServiceImpl<ListContactMapper, ListContact> implements IListContactService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IContactService contactService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.marketing.service.IIBizListService ibizlistService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(ListContact et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getRelationshipsid()),et);
        return true;
    }

    @Override
    public void createBatch(List<ListContact> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(ListContact et) {
        fillParentData(et);
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("relationshipsid",et.getRelationshipsid())))
            return false;
        CachedBeanCopier.copy(get(et.getRelationshipsid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<ListContact> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public ListContact get(String key) {
        ListContact et = getById(key);
        if(et==null){
            et=new ListContact();
            et.setRelationshipsid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public ListContact getDraft(ListContact et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(ListContact et) {
        return (!ObjectUtils.isEmpty(et.getRelationshipsid()))&&(!Objects.isNull(this.getById(et.getRelationshipsid())));
    }
    @Override
    @Transactional
    public boolean save(ListContact et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(ListContact et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<ListContact> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<ListContact> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<ListContact> selectByEntity2id(String contactid) {
        return baseMapper.selectByEntity2id(contactid);
    }

    @Override
    public void removeByEntity2id(String contactid) {
        this.remove(new QueryWrapper<ListContact>().eq("entity2id",contactid));
    }

	@Override
    public List<ListContact> selectByEntityid(String listid) {
        return baseMapper.selectByEntityid(listid);
    }

    @Override
    public void removeByEntityid(String listid) {
        this.remove(new QueryWrapper<ListContact>().eq("entityid",listid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<ListContact> searchDefault(ListContactSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<ListContact> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<ListContact>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(ListContact et){
        //实体关系[DER1N_LISTCONTACT_CONTACT_CONTACTID]
        if(!ObjectUtils.isEmpty(et.getEntity2id())){
            cn.ibizlab.businesscentral.core.base.domain.Contact contact=et.getContact();
            if(ObjectUtils.isEmpty(contact)){
                cn.ibizlab.businesscentral.core.base.domain.Contact majorEntity=contactService.get(et.getEntity2id());
                et.setContact(majorEntity);
                contact=majorEntity;
            }
            et.setEmailaddress1(contact.getEmailaddress1());
            et.setStatecode(contact.getStatecode());
            et.setEntity2name(contact.getFullname());
            et.setParentcustomerid(contact.getParentcustomerid());
            et.setTelephone1(contact.getTelephone1());
        }
        //实体关系[DER1N_LISTCONTACT_LIST_LISTID]
        if(!ObjectUtils.isEmpty(et.getEntityid())){
            cn.ibizlab.businesscentral.core.marketing.domain.IBizList list=et.getList();
            if(ObjectUtils.isEmpty(list)){
                cn.ibizlab.businesscentral.core.marketing.domain.IBizList majorEntity=ibizlistService.get(et.getEntityid());
                et.setList(majorEntity);
                list=majorEntity;
            }
            et.setEntityname(list.getListname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<ListContact> getListcontactByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<ListContact> getListcontactByEntities(List<ListContact> entities) {
        List ids =new ArrayList();
        for(ListContact entity : entities){
            Serializable id=entity.getRelationshipsid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




package cn.ibizlab.businesscentral.core.sales.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.sales.domain.GoalRollupQuery;
import cn.ibizlab.businesscentral.core.sales.filter.GoalRollupQuerySearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[GoalRollupQuery] 服务对象接口
 */
public interface IGoalRollupQueryService extends IService<GoalRollupQuery>{

    boolean create(GoalRollupQuery et) ;
    void createBatch(List<GoalRollupQuery> list) ;
    boolean update(GoalRollupQuery et) ;
    void updateBatch(List<GoalRollupQuery> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    GoalRollupQuery get(String key) ;
    GoalRollupQuery getDraft(GoalRollupQuery et) ;
    boolean checkKey(GoalRollupQuery et) ;
    boolean save(GoalRollupQuery et) ;
    void saveBatch(List<GoalRollupQuery> list) ;
    Page<GoalRollupQuery> searchDefault(GoalRollupQuerySearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<GoalRollupQuery> getGoalrollupqueryByIds(List<String> ids) ;
    List<GoalRollupQuery> getGoalrollupqueryByEntities(List<GoalRollupQuery> entities) ;
}



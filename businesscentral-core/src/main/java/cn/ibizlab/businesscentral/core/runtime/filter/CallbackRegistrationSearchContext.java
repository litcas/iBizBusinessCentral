package cn.ibizlab.businesscentral.core.runtime.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.runtime.domain.CallbackRegistration;
/**
 * 关系型数据实体[CallbackRegistration] 查询条件对象
 */
@Slf4j
@Data
public class CallbackRegistrationSearchContext extends QueryWrapperContext<CallbackRegistration> {

	private String n_callbackregistrationname_like;//[回拨注册名称]
	public void setN_callbackregistrationname_like(String n_callbackregistrationname_like) {
        this.n_callbackregistrationname_like = n_callbackregistrationname_like;
        if(!ObjectUtils.isEmpty(this.n_callbackregistrationname_like)){
            this.getSearchCond().like("callbackregistrationname", n_callbackregistrationname_like);
        }
    }
	private String n_scope_eq;//[Scope]
	public void setN_scope_eq(String n_scope_eq) {
        this.n_scope_eq = n_scope_eq;
        if(!ObjectUtils.isEmpty(this.n_scope_eq)){
            this.getSearchCond().eq("scope", n_scope_eq);
        }
    }
	private String n_message_eq;//[Message]
	public void setN_message_eq(String n_message_eq) {
        this.n_message_eq = n_message_eq;
        if(!ObjectUtils.isEmpty(this.n_message_eq)){
            this.getSearchCond().eq("message", n_message_eq);
        }
    }
	private String n_version_eq;//[Version]
	public void setN_version_eq(String n_version_eq) {
        this.n_version_eq = n_version_eq;
        if(!ObjectUtils.isEmpty(this.n_version_eq)){
            this.getSearchCond().eq("version", n_version_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("callbackregistrationname", query)   
            );
		 }
	}
}




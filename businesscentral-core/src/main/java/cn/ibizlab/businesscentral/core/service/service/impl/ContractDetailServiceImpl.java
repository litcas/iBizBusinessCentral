package cn.ibizlab.businesscentral.core.service.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.service.domain.ContractDetail;
import cn.ibizlab.businesscentral.core.service.filter.ContractDetailSearchContext;
import cn.ibizlab.businesscentral.core.service.service.IContractDetailService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.service.mapper.ContractDetailMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[合同子项] 服务对象接口实现
 */
@Slf4j
@Service("ContractDetailServiceImpl")
public class ContractDetailServiceImpl extends ServiceImpl<ContractDetailMapper, ContractDetail> implements IContractDetailService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IIncidentService incidentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IContractService contractService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ICustomerAddressService customeraddressService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.product.service.IProductService productService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IUomScheduleService uomscheduleService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IUomService uomService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(ContractDetail et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getContractdetailid()),et);
        return true;
    }

    @Override
    public void createBatch(List<ContractDetail> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(ContractDetail et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("contractdetailid",et.getContractdetailid())))
            return false;
        CachedBeanCopier.copy(get(et.getContractdetailid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<ContractDetail> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public ContractDetail get(String key) {
        ContractDetail et = getById(key);
        if(et==null){
            et=new ContractDetail();
            et.setContractdetailid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public ContractDetail getDraft(ContractDetail et) {
        return et;
    }

    @Override
    public boolean checkKey(ContractDetail et) {
        return (!ObjectUtils.isEmpty(et.getContractdetailid()))&&(!Objects.isNull(this.getById(et.getContractdetailid())));
    }
    @Override
    @Transactional
    public boolean save(ContractDetail et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(ContractDetail et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<ContractDetail> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<ContractDetail> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<ContractDetail> selectByContractid(String contractid) {
        return baseMapper.selectByContractid(contractid);
    }

    @Override
    public void removeByContractid(String contractid) {
        this.remove(new QueryWrapper<ContractDetail>().eq("contractid",contractid));
    }

	@Override
    public List<ContractDetail> selectByServiceaddress(String customeraddressid) {
        return baseMapper.selectByServiceaddress(customeraddressid);
    }

    @Override
    public void removeByServiceaddress(String customeraddressid) {
        this.remove(new QueryWrapper<ContractDetail>().eq("serviceaddress",customeraddressid));
    }

	@Override
    public List<ContractDetail> selectByProductid(String productid) {
        return baseMapper.selectByProductid(productid);
    }

    @Override
    public void removeByProductid(String productid) {
        this.remove(new QueryWrapper<ContractDetail>().eq("productid",productid));
    }

	@Override
    public List<ContractDetail> selectByUomscheduleid(String uomscheduleid) {
        return baseMapper.selectByUomscheduleid(uomscheduleid);
    }

    @Override
    public void removeByUomscheduleid(String uomscheduleid) {
        this.remove(new QueryWrapper<ContractDetail>().eq("uomscheduleid",uomscheduleid));
    }

	@Override
    public List<ContractDetail> selectByUomid(String uomid) {
        return baseMapper.selectByUomid(uomid);
    }

    @Override
    public void removeByUomid(String uomid) {
        this.remove(new QueryWrapper<ContractDetail>().eq("uomid",uomid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<ContractDetail> searchDefault(ContractDetailSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<ContractDetail> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<ContractDetail>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<ContractDetail> getContractdetailByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<ContractDetail> getContractdetailByEntities(List<ContractDetail> entities) {
        List ids =new ArrayList();
        for(ContractDetail entity : entities){
            Serializable id=entity.getContractdetailid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




package cn.ibizlab.businesscentral.core.service.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.service.domain.IncidentreSolution;
import cn.ibizlab.businesscentral.core.service.filter.IncidentreSolutionSearchContext;
import cn.ibizlab.businesscentral.core.service.service.IIncidentreSolutionService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.service.mapper.IncidentreSolutionMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[案例解决] 服务对象接口实现
 */
@Slf4j
@Service("IncidentreSolutionServiceImpl")
public class IncidentreSolutionServiceImpl extends ServiceImpl<IncidentreSolutionMapper, IncidentreSolution> implements IIncidentreSolutionService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IIncidentService incidentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IIBizServiceService ibizserviceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ISlaService slaService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(IncidentreSolution et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getActivityid()),et);
        return true;
    }

    @Override
    public void createBatch(List<IncidentreSolution> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(IncidentreSolution et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("activityid",et.getActivityid())))
            return false;
        CachedBeanCopier.copy(get(et.getActivityid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<IncidentreSolution> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public IncidentreSolution get(String key) {
        IncidentreSolution et = getById(key);
        if(et==null){
            et=new IncidentreSolution();
            et.setActivityid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public IncidentreSolution getDraft(IncidentreSolution et) {
        return et;
    }

    @Override
    public boolean checkKey(IncidentreSolution et) {
        return (!ObjectUtils.isEmpty(et.getActivityid()))&&(!Objects.isNull(this.getById(et.getActivityid())));
    }
    @Override
    @Transactional
    public boolean save(IncidentreSolution et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(IncidentreSolution et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<IncidentreSolution> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<IncidentreSolution> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<IncidentreSolution> selectByIncidentid(String incidentid) {
        return baseMapper.selectByIncidentid(incidentid);
    }

    @Override
    public void removeByIncidentid(String incidentid) {
        this.remove(new QueryWrapper<IncidentreSolution>().eq("incidentid",incidentid));
    }

	@Override
    public List<IncidentreSolution> selectByServiceid(String serviceid) {
        return baseMapper.selectByServiceid(serviceid);
    }

    @Override
    public void removeByServiceid(String serviceid) {
        this.remove(new QueryWrapper<IncidentreSolution>().eq("serviceid",serviceid));
    }

	@Override
    public List<IncidentreSolution> selectBySlaid(String slaid) {
        return baseMapper.selectBySlaid(slaid);
    }

    @Override
    public void removeBySlaid(String slaid) {
        this.remove(new QueryWrapper<IncidentreSolution>().eq("slaid",slaid));
    }

	@Override
    public List<IncidentreSolution> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<IncidentreSolution>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<IncidentreSolution> searchDefault(IncidentreSolutionSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<IncidentreSolution> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<IncidentreSolution>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<IncidentreSolution> getIncidentresolutionByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<IncidentreSolution> getIncidentresolutionByEntities(List<IncidentreSolution> entities) {
        List ids =new ArrayList();
        for(IncidentreSolution entity : entities){
            Serializable id=entity.getActivityid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




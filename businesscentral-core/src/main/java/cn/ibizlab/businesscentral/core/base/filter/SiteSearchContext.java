package cn.ibizlab.businesscentral.core.base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.base.domain.Site;
/**
 * 关系型数据实体[Site] 查询条件对象
 */
@Slf4j
@Data
public class SiteSearchContext extends QueryWrapperContext<Site> {

	private String n_sitename_like;//[站点名称]
	public void setN_sitename_like(String n_sitename_like) {
        this.n_sitename_like = n_sitename_like;
        if(!ObjectUtils.isEmpty(this.n_sitename_like)){
            this.getSearchCond().like("sitename", n_sitename_like);
        }
    }
	private String n_address2_shippingmethodcode_eq;//[地址 2: 送货方式]
	public void setN_address2_shippingmethodcode_eq(String n_address2_shippingmethodcode_eq) {
        this.n_address2_shippingmethodcode_eq = n_address2_shippingmethodcode_eq;
        if(!ObjectUtils.isEmpty(this.n_address2_shippingmethodcode_eq)){
            this.getSearchCond().eq("address2_shippingmethodcode", n_address2_shippingmethodcode_eq);
        }
    }
	private String n_address1_shippingmethodcode_eq;//[地址 1: 送货方式]
	public void setN_address1_shippingmethodcode_eq(String n_address1_shippingmethodcode_eq) {
        this.n_address1_shippingmethodcode_eq = n_address1_shippingmethodcode_eq;
        if(!ObjectUtils.isEmpty(this.n_address1_shippingmethodcode_eq)){
            this.getSearchCond().eq("address1_shippingmethodcode", n_address1_shippingmethodcode_eq);
        }
    }
	private String n_address2_addresstypecode_eq;//[地址 2: 地址类型]
	public void setN_address2_addresstypecode_eq(String n_address2_addresstypecode_eq) {
        this.n_address2_addresstypecode_eq = n_address2_addresstypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_address2_addresstypecode_eq)){
            this.getSearchCond().eq("address2_addresstypecode", n_address2_addresstypecode_eq);
        }
    }
	private String n_address1_addresstypecode_eq;//[地址 1: 地址类型]
	public void setN_address1_addresstypecode_eq(String n_address1_addresstypecode_eq) {
        this.n_address1_addresstypecode_eq = n_address1_addresstypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_address1_addresstypecode_eq)){
            this.getSearchCond().eq("address1_addresstypecode", n_address1_addresstypecode_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("sitename", query)   
            );
		 }
	}
}




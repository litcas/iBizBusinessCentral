package cn.ibizlab.businesscentral.core.service.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.service.domain.KnowledgeArticleIncident;
import cn.ibizlab.businesscentral.core.service.filter.KnowledgeArticleIncidentSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[KnowledgeArticleIncident] 服务对象接口
 */
public interface IKnowledgeArticleIncidentService extends IService<KnowledgeArticleIncident>{

    boolean create(KnowledgeArticleIncident et) ;
    void createBatch(List<KnowledgeArticleIncident> list) ;
    boolean update(KnowledgeArticleIncident et) ;
    void updateBatch(List<KnowledgeArticleIncident> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    KnowledgeArticleIncident get(String key) ;
    KnowledgeArticleIncident getDraft(KnowledgeArticleIncident et) ;
    boolean checkKey(KnowledgeArticleIncident et) ;
    boolean save(KnowledgeArticleIncident et) ;
    void saveBatch(List<KnowledgeArticleIncident> list) ;
    Page<KnowledgeArticleIncident> searchDefault(KnowledgeArticleIncidentSearchContext context) ;
    List<KnowledgeArticleIncident> selectByIncidentid(String incidentid) ;
    void removeByIncidentid(String incidentid) ;
    List<KnowledgeArticleIncident> selectByKnowledgearticleid(String knowledgearticleid) ;
    void removeByKnowledgearticleid(String knowledgearticleid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<KnowledgeArticleIncident> getKnowledgearticleincidentByIds(List<String> ids) ;
    List<KnowledgeArticleIncident> getKnowledgearticleincidentByEntities(List<KnowledgeArticleIncident> entities) ;
}



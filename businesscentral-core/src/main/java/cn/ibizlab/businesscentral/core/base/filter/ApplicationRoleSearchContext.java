package cn.ibizlab.businesscentral.core.base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.base.domain.ApplicationRole;
/**
 * 关系型数据实体[ApplicationRole] 查询条件对象
 */
@Slf4j
@Data
public class ApplicationRoleSearchContext extends QueryWrapperContext<ApplicationRole> {

	private String n_applicationrolename_like;//[应用角色名称]
	public void setN_applicationrolename_like(String n_applicationrolename_like) {
        this.n_applicationrolename_like = n_applicationrolename_like;
        if(!ObjectUtils.isEmpty(this.n_applicationrolename_like)){
            this.getSearchCond().like("applicationrolename", n_applicationrolename_like);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("applicationrolename", query)   
            );
		 }
	}
}




package cn.ibizlab.businesscentral.core.extensions.service;

import cn.ibizlab.businesscentral.core.base.domain.Account;
import cn.ibizlab.businesscentral.core.base.service.impl.ContactServiceImpl;
import lombok.extern.slf4j.Slf4j;
import cn.ibizlab.businesscentral.core.base.domain.Contact;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Primary;
import java.util.*;

/**
 * 实体[联系人] 自定义服务对象
 */
@Slf4j
@Primary
@Service("ContactExService")
public class ContactExService extends ContactServiceImpl {

    @Override
    protected Class currentModelClass() {
        return com.baomidou.mybatisplus.core.toolkit.ReflectionKit.getSuperClassGenericType(this.getClass().getSuperclass(), 1);
    }

    /**
     * 自定义行为[SetPrimary]用户扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public Contact setPrimary(Contact et) {
        Account account = new Account() ;
        account.setAccountid(et.getCustomerid());
        account.setPrimarycontactid(et.getContactid());
        account.setPrimarycontactname(et.getFullname());
        accountService.update(account);
        return super.setPrimary(et);
    }
}


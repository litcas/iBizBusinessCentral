package cn.ibizlab.businesscentral.core.website.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.website.domain.WebSiteChannel;
import cn.ibizlab.businesscentral.core.website.filter.WebSiteChannelSearchContext;
import cn.ibizlab.businesscentral.core.website.service.IWebSiteChannelService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.website.mapper.WebSiteChannelMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[站点频道] 服务对象接口实现
 */
@Slf4j
@Service("WebSiteChannelServiceImpl")
public class WebSiteChannelServiceImpl extends ServiceImpl<WebSiteChannelMapper, WebSiteChannel> implements IWebSiteChannelService {


    protected cn.ibizlab.businesscentral.core.website.service.IWebSiteChannelService websitechannelService = this;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.website.service.IWebSiteContentService websitecontentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.website.service.IWebSiteService websiteService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(WebSiteChannel et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getWebsitechannelid()),et);
        return true;
    }

    @Override
    public void createBatch(List<WebSiteChannel> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(WebSiteChannel et) {
        fillParentData(et);
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("websitechannelid",et.getWebsitechannelid())))
            return false;
        CachedBeanCopier.copy(get(et.getWebsitechannelid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<WebSiteChannel> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public WebSiteChannel get(String key) {
        WebSiteChannel et = getById(key);
        if(et==null){
            et=new WebSiteChannel();
            et.setWebsitechannelid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public WebSiteChannel getDraft(WebSiteChannel et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(WebSiteChannel et) {
        return (!ObjectUtils.isEmpty(et.getWebsitechannelid()))&&(!Objects.isNull(this.getById(et.getWebsitechannelid())));
    }
    @Override
    @Transactional
    public boolean save(WebSiteChannel et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(WebSiteChannel et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<WebSiteChannel> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<WebSiteChannel> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<WebSiteChannel> selectByPwebsitechannelid(String websitechannelid) {
        return baseMapper.selectByPwebsitechannelid(websitechannelid);
    }

    @Override
    public void removeByPwebsitechannelid(String websitechannelid) {
        this.remove(new QueryWrapper<WebSiteChannel>().eq("pwebsitechannelid",websitechannelid));
    }

	@Override
    public List<WebSiteChannel> selectByWebsiteid(String websiteid) {
        return baseMapper.selectByWebsiteid(websiteid);
    }

    @Override
    public void removeByWebsiteid(String websiteid) {
        this.remove(new QueryWrapper<WebSiteChannel>().eq("websiteid",websiteid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<WebSiteChannel> searchDefault(WebSiteChannelSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<WebSiteChannel> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<WebSiteChannel>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 Root
     */
    @Override
    public Page<WebSiteChannel> searchRoot(WebSiteChannelSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<WebSiteChannel> pages=baseMapper.searchRoot(context.getPages(),context,context.getSelectCond());
        return new PageImpl<WebSiteChannel>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(WebSiteChannel et){
        //实体关系[DER1N_WEBSITECHANNEL_WEBSITECHANNEL_PWEBSITECHANNELID]
        if(!ObjectUtils.isEmpty(et.getPwebsitechannelid())){
            cn.ibizlab.businesscentral.core.website.domain.WebSiteChannel pwebsitechannel=et.getPwebsitechannel();
            if(ObjectUtils.isEmpty(pwebsitechannel)){
                cn.ibizlab.businesscentral.core.website.domain.WebSiteChannel majorEntity=websitechannelService.get(et.getPwebsitechannelid());
                et.setPwebsitechannel(majorEntity);
                pwebsitechannel=majorEntity;
            }
            et.setPwebsitechannelname(pwebsitechannel.getWebsitechannelname());
        }
        //实体关系[DER1N_WEBSITECHANNEL_WEBSITE_WEBSITEID]
        if(!ObjectUtils.isEmpty(et.getWebsiteid())){
            cn.ibizlab.businesscentral.core.website.domain.WebSite website=et.getWebsite();
            if(ObjectUtils.isEmpty(website)){
                cn.ibizlab.businesscentral.core.website.domain.WebSite majorEntity=websiteService.get(et.getWebsiteid());
                et.setWebsite(majorEntity);
                website=majorEntity;
            }
            et.setWebsitename(website.getWebsitename());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<WebSiteChannel> getWebsitechannelByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<WebSiteChannel> getWebsitechannelByEntities(List<WebSiteChannel> entities) {
        List ids =new ArrayList();
        for(WebSiteChannel entity : entities){
            Serializable id=entity.getWebsitechannelid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




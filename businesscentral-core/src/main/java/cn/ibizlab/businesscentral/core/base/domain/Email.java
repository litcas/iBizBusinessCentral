package cn.ibizlab.businesscentral.core.base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[电子邮件]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "EMAIL",resultMap = "EmailResultMap")
public class Email extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * MIME 类型
     */
    @TableField(value = "mimetype")
    @JSONField(name = "mimetype")
    @JsonProperty("mimetype")
    private String mimetype;
    /**
     * 收件人
     */
    @TableField(value = "to")
    @JSONField(name = "to")
    @JsonProperty("to")
    private String to;
    /**
     * 上次打开时间
     */
    @TableField(value = "lastopenedtime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "lastopenedtime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("lastopenedtime")
    private Timestamp lastopenedtime;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 截止日期
     */
    @TableField(value = "scheduledend")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "scheduledend" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("scheduledend")
    private Timestamp scheduledend;
    /**
     * 关于
     */
    @TableField(value = "regardingobjectid")
    @JSONField(name = "regardingobjectid")
    @JsonProperty("regardingobjectid")
    private String regardingobjectid;
    /**
     * 打开计数
     */
    @TableField(value = "opencount")
    @JSONField(name = "opencount")
    @JsonProperty("opencount")
    private Integer opencount;
    /**
     * 负责人类型
     */
    @TableField(value = "ownertype")
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;
    /**
     * 电子邮件发件人帐户名称
     */
    @TableField(value = "sendersaccountname")
    @JSONField(name = "sendersaccountname")
    @JsonProperty("sendersaccountname")
    private String sendersaccountname;
    /**
     * 电子邮件提醒类型
     */
    @TableField(value = "emailremindertype")
    @JSONField(name = "emailremindertype")
    @JsonProperty("emailremindertype")
    private String emailremindertype;
    /**
     * 相关性方法
     */
    @TableField(value = "correlationmethod")
    @JSONField(name = "correlationmethod")
    @JsonProperty("correlationmethod")
    private String correlationmethod;
    /**
     * 消息 ID 重复检测
     */
    @TableField(value = "messageiddupcheck")
    @JSONField(name = "messageiddupcheck")
    @JsonProperty("messageiddupcheck")
    private String messageiddupcheck;
    /**
     * 发送日期
     */
    @TableField(value = "senton")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "senton" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("senton")
    private Timestamp senton;
    /**
     * 链接单击计数
     */
    @TableField(value = "linksclickedcount")
    @JSONField(name = "linksclickedcount")
    @JsonProperty("linksclickedcount")
    private Integer linksclickedcount;
    /**
     * 压缩
     */
    @DEField(defaultValue = "0")
    @TableField(value = "compressed")
    @JSONField(name = "compressed")
    @JsonProperty("compressed")
    private Integer compressed;
    /**
     * 活动状态
     */
    @TableField(value = "statecode")
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;
    /**
     * 答复消息
     */
    @TableField(value = "inreplyto")
    @JSONField(name = "inreplyto")
    @JsonProperty("inreplyto")
    private String inreplyto;
    /**
     * 电子邮件提醒状态
     */
    @TableField(value = "emailreminderstatus")
    @JSONField(name = "emailreminderstatus")
    @JsonProperty("emailreminderstatus")
    private String emailreminderstatus;
    /**
     * 安全说明
     */
    @TableField(value = "safedescription")
    @JSONField(name = "safedescription")
    @JsonProperty("safedescription")
    private String safedescription;
    /**
     * 遍历的路径
     */
    @TableField(value = "traversedpath")
    @JSONField(name = "traversedpath")
    @JsonProperty("traversedpath")
    private String traversedpath;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 提醒操作卡 ID。
     */
    @TableField(value = "reminderactioncardid")
    @JSONField(name = "reminderactioncardid")
    @JsonProperty("reminderactioncardid")
    private String reminderactioncardid;
    /**
     * 版本号
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 暂候时间(分钟)
     */
    @TableField(value = "onholdtime")
    @JSONField(name = "onholdtime")
    @JsonProperty("onholdtime")
    private Integer onholdtime;
    /**
     * 子类别
     */
    @TableField(value = "subcategory")
    @JSONField(name = "subcategory")
    @JsonProperty("subcategory")
    private String subcategory;
    /**
     * 后续活动
     */
    @DEField(defaultValue = "0")
    @TableField(value = "emailfollowed")
    @JSONField(name = "emailfollowed")
    @JsonProperty("emailfollowed")
    private Integer emailfollowed;
    /**
     * 跟踪
     */
    @DEField(defaultValue = "0")
    @TableField(value = "followemailuserpreference")
    @JSONField(name = "followemailuserpreference")
    @JsonProperty("followemailuserpreference")
    private Integer followemailuserpreference;
    /**
     * 负责人
     */
    @TableField(value = "ownername")
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;
    /**
     * 电子邮件
     */
    @DEField(isKeyField=true)
    @TableId(value= "activityid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "activityid")
    @JsonProperty("activityid")
    private String activityid;
    /**
     * 电子邮件跟踪 ID
     */
    @TableField(value = "emailtrackingid")
    @JSONField(name = "emailtrackingid")
    @JsonProperty("emailtrackingid")
    private String emailtrackingid;
    /**
     * 关于
     */
    @TableField(value = "regardingobjectname")
    @JSONField(name = "regardingobjectname")
    @JsonProperty("regardingobjectname")
    private String regardingobjectname;
    /**
     * 开始日期
     */
    @TableField(value = "scheduledstart")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "scheduledstart" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("scheduledstart")
    private Timestamp scheduledstart;
    /**
     * 电子邮件提醒文本
     */
    @TableField(value = "emailremindertext")
    @JSONField(name = "emailremindertext")
    @JsonProperty("emailremindertext")
    private String emailremindertext;
    /**
     * 跟踪令牌
     */
    @TableField(value = "trackingtoken")
    @JSONField(name = "trackingtoken")
    @JsonProperty("trackingtoken")
    private String trackingtoken;
    /**
     * 活动类型
     */
    @TableField(value = "activitytypecode")
    @JSONField(name = "activitytypecode")
    @JsonProperty("activitytypecode")
    private String activitytypecode;
    /**
     * 尝试传送的次数
     */
    @TableField(value = "deliveryattempts")
    @JSONField(name = "deliveryattempts")
    @JsonProperty("deliveryattempts")
    private Integer deliveryattempts;
    /**
     * 发件人帐户类型
     */
    @TableField(value = "sendersaccountobjecttypecode")
    @JSONField(name = "sendersaccountobjecttypecode")
    @JsonProperty("sendersaccountobjecttypecode")
    private String sendersaccountobjecttypecode;
    /**
     * 需要“送达”回执
     */
    @DEField(defaultValue = "0")
    @TableField(value = "deliveryreceiptrequested")
    @JSONField(name = "deliveryreceiptrequested")
    @JsonProperty("deliveryreceiptrequested")
    private Integer deliveryreceiptrequested;
    /**
     * 电子邮件提醒到期时间
     */
    @TableField(value = "emailreminderexpirytime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "emailreminderexpirytime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("emailreminderexpirytime")
    private Timestamp emailreminderexpirytime;
    /**
     * 汇率
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * 创建记录的时间
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 负责人
     */
    @TableField(value = "ownerid")
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;
    /**
     * 已记帐
     */
    @DEField(defaultValue = "0")
    @TableField(value = "billed")
    @JSONField(name = "billed")
    @JsonProperty("billed")
    private Integer billed;
    /**
     * 类别
     */
    @TableField(value = "category")
    @JSONField(name = "category")
    @JsonProperty("category")
    private String category;
    /**
     * 对话索引(哈希)
     */
    @TableField(value = "baseconversationindexhash")
    @JSONField(name = "baseconversationindexhash")
    @JsonProperty("baseconversationindexhash")
    private Integer baseconversationindexhash;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 流程
     */
    @TableField(value = "processid")
    @JSONField(name = "processid")
    @JsonProperty("processid")
    private String processid;
    /**
     * 对话索引
     */
    @TableField(value = "conversationindex")
    @JSONField(name = "conversationindex")
    @JsonProperty("conversationindex")
    private String conversationindex;
    /**
     * 流程阶段
     */
    @TableField(value = "stageid")
    @JSONField(name = "stageid")
    @JsonProperty("stageid")
    private String stageid;
    /**
     * 延迟电子邮件处理，直至
     */
    @TableField(value = "postponeemailprocessinguntil")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "postponeemailprocessinguntil" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("postponeemailprocessinguntil")
    private Timestamp postponeemailprocessinguntil;
    /**
     * 方向
     */
    @DEField(defaultValue = "1")
    @TableField(value = "directioncode")
    @JSONField(name = "directioncode")
    @JsonProperty("directioncode")
    private Integer directioncode;
    /**
     * 通知
     */
    @TableField(value = "notifications")
    @JSONField(name = "notifications")
    @JsonProperty("notifications")
    private String notifications;
    /**
     * 电子邮件发件人类型
     */
    @TableField(value = "emailsenderobjecttypecode")
    @JSONField(name = "emailsenderobjecttypecode")
    @JsonProperty("emailsenderobjecttypecode")
    private String emailsenderobjecttypecode;
    /**
     * 排序日期
     */
    @TableField(value = "sortdate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "sortdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("sortdate")
    private Timestamp sortdate;
    /**
     * 消息 ID
     */
    @TableField(value = "messageid")
    @JSONField(name = "messageid")
    @JsonProperty("messageid")
    private String messageid;
    /**
     * 附加参数
     */
    @TableField(value = "activityadditionalparams")
    @JSONField(name = "activityadditionalparams")
    @JsonProperty("activityadditionalparams")
    private String activityadditionalparams;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 需要“已读”回执
     */
    @DEField(defaultValue = "0")
    @TableField(value = "readreceiptrequested")
    @JSONField(name = "readreceiptrequested")
    @JsonProperty("readreceiptrequested")
    private Integer readreceiptrequested;
    /**
     * 由工作流创建
     */
    @DEField(defaultValue = "0")
    @TableField(value = "workflowcreated")
    @JSONField(name = "workflowcreated")
    @JsonProperty("workflowcreated")
    private Integer workflowcreated;
    /**
     * IsUnsafe
     */
    @TableField(value = "unsafe")
    @JSONField(name = "unsafe")
    @JsonProperty("unsafe")
    private Integer unsafe;
    /**
     * RegardingObjectTypeCode
     */
    @TableField(value = "regardingobjecttypecode")
    @JSONField(name = "regardingobjecttypecode")
    @JsonProperty("regardingobjecttypecode")
    private String regardingobjecttypecode;
    /**
     * 优先级
     */
    @TableField(value = "prioritycode")
    @JSONField(name = "prioritycode")
    @JsonProperty("prioritycode")
    private String prioritycode;
    /**
     * 稍后发送
     */
    @TableField(value = "delayedemailsendtime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "delayedemailsendtime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("delayedemailsendtime")
    private Timestamp delayedemailsendtime;
    /**
     * 发件人
     */
    @TableField(value = "from")
    @JSONField(name = "from")
    @JsonProperty("from")
    private String from;
    /**
     * 电子邮件发件人名称
     */
    @TableField(value = "emailsendername")
    @JSONField(name = "emailsendername")
    @JsonProperty("emailsendername")
    private String emailsendername;
    /**
     * 密件抄送
     */
    @TableField(value = "bcc")
    @JSONField(name = "bcc")
    @JsonProperty("bcc")
    private String bcc;
    /**
     * 实际开始时间
     */
    @TableField(value = "actualstart")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "actualstart" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("actualstart")
    private Timestamp actualstart;
    /**
     * 实际结束时间
     */
    @TableField(value = "actualend")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "actualend" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("actualend")
    private Timestamp actualend;
    /**
     * SLAName
     */
    @TableField(value = "slaname")
    @JSONField(name = "slaname")
    @JsonProperty("slaname")
    private String slaname;
    /**
     * 对话跟踪 ID
     */
    @TableField(value = "conversationtrackingid")
    @JSONField(name = "conversationtrackingid")
    @JsonProperty("conversationtrackingid")
    private String conversationtrackingid;
    /**
     * 发件人
     */
    @TableField(value = "sender")
    @JSONField(name = "sender")
    @JsonProperty("sender")
    private String sender;
    /**
     * 上一暂候时间
     */
    @TableField(value = "lastonholdtime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "lastonholdtime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("lastonholdtime")
    private Timestamp lastonholdtime;
    /**
     * 提交者
     */
    @TableField(value = "submittedby")
    @JSONField(name = "submittedby")
    @JsonProperty("submittedby")
    private String submittedby;
    /**
     * 至收件人
     */
    @TableField(value = "torecipients")
    @JSONField(name = "torecipients")
    @JsonProperty("torecipients")
    private String torecipients;
    /**
     * 传递优先级
     */
    @TableField(value = "deliveryprioritycode")
    @JSONField(name = "deliveryprioritycode")
    @JsonProperty("deliveryprioritycode")
    private String deliveryprioritycode;
    /**
     * 附件计数
     */
    @TableField(value = "attachmentcount")
    @JSONField(name = "attachmentcount")
    @JsonProperty("attachmentcount")
    private Integer attachmentcount;
    /**
     * 附件打开计数
     */
    @TableField(value = "attachmentopencount")
    @JSONField(name = "attachmentopencount")
    @JsonProperty("attachmentopencount")
    private Integer attachmentopencount;
    /**
     * 计划持续时间
     */
    @TableField(value = "scheduleddurationminutes")
    @JSONField(name = "scheduleddurationminutes")
    @JsonProperty("scheduleddurationminutes")
    private Integer scheduleddurationminutes;
    /**
     * 导入序列号
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * 时区规则版本号
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * 回复计数
     */
    @TableField(value = "replycount")
    @JSONField(name = "replycount")
    @JsonProperty("replycount")
    private Integer replycount;
    /**
     * 是定期活动
     */
    @DEField(defaultValue = "0")
    @TableField(value = "regularactivity")
    @JSONField(name = "regularactivity")
    @JsonProperty("regularactivity")
    private Integer regularactivity;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 抄送
     */
    @TableField(value = "cc")
    @JSONField(name = "cc")
    @JsonProperty("cc")
    private String cc;
    /**
     * 状态描述
     */
    @TableField(value = "statuscode")
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;
    /**
     * 主题
     */
    @TableField(value = "subject")
    @JSONField(name = "subject")
    @JsonProperty("subject")
    private String subject;
    /**
     * UTC 转换时区代码
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * 已设置提醒
     */
    @DEField(defaultValue = "0")
    @TableField(value = "emailreminderset")
    @JSONField(name = "emailreminderset")
    @JsonProperty("emailreminderset")
    private Integer emailreminderset;
    /**
     * 持续时间
     */
    @TableField(value = "actualdurationminutes")
    @JSONField(name = "actualdurationminutes")
    @JsonProperty("actualdurationminutes")
    private Integer actualdurationminutes;
    /**
     * 父活动 ID
     */
    @TableField(value = "parentactivityid")
    @JSONField(name = "parentactivityid")
    @JsonProperty("parentactivityid")
    private String parentactivityid;
    /**
     * 服务
     */
    @TableField(value = "serviceid")
    @JSONField(name = "serviceid")
    @JsonProperty("serviceid")
    private String serviceid;
    /**
     * 使用的模板的 ID。
     */
    @TableField(value = "templateid")
    @JSONField(name = "templateid")
    @JsonProperty("templateid")
    private String templateid;
    /**
     * SLA
     */
    @TableField(value = "slaid")
    @JSONField(name = "slaid")
    @JsonProperty("slaid")
    private String slaid;
    /**
     * 货币
     */
    @TableField(value = "transactioncurrencyid")
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Email parentactivity;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.service.domain.IBizService service;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Sla sla;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Template template;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency;



    /**
     * 设置 [MIME 类型]
     */
    public void setMimetype(String mimetype){
        this.mimetype = mimetype ;
        this.modify("mimetype",mimetype);
    }

    /**
     * 设置 [收件人]
     */
    public void setTo(String to){
        this.to = to ;
        this.modify("to",to);
    }

    /**
     * 设置 [上次打开时间]
     */
    public void setLastopenedtime(Timestamp lastopenedtime){
        this.lastopenedtime = lastopenedtime ;
        this.modify("lastopenedtime",lastopenedtime);
    }

    /**
     * 格式化日期 [上次打开时间]
     */
    public String formatLastopenedtime(){
        if (this.lastopenedtime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(lastopenedtime);
    }
    /**
     * 设置 [截止日期]
     */
    public void setScheduledend(Timestamp scheduledend){
        this.scheduledend = scheduledend ;
        this.modify("scheduledend",scheduledend);
    }

    /**
     * 格式化日期 [截止日期]
     */
    public String formatScheduledend(){
        if (this.scheduledend == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(scheduledend);
    }
    /**
     * 设置 [关于]
     */
    public void setRegardingobjectid(String regardingobjectid){
        this.regardingobjectid = regardingobjectid ;
        this.modify("regardingobjectid",regardingobjectid);
    }

    /**
     * 设置 [打开计数]
     */
    public void setOpencount(Integer opencount){
        this.opencount = opencount ;
        this.modify("opencount",opencount);
    }

    /**
     * 设置 [负责人类型]
     */
    public void setOwnertype(String ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [电子邮件发件人帐户名称]
     */
    public void setSendersaccountname(String sendersaccountname){
        this.sendersaccountname = sendersaccountname ;
        this.modify("sendersaccountname",sendersaccountname);
    }

    /**
     * 设置 [电子邮件提醒类型]
     */
    public void setEmailremindertype(String emailremindertype){
        this.emailremindertype = emailremindertype ;
        this.modify("emailremindertype",emailremindertype);
    }

    /**
     * 设置 [相关性方法]
     */
    public void setCorrelationmethod(String correlationmethod){
        this.correlationmethod = correlationmethod ;
        this.modify("correlationmethod",correlationmethod);
    }

    /**
     * 设置 [消息 ID 重复检测]
     */
    public void setMessageiddupcheck(String messageiddupcheck){
        this.messageiddupcheck = messageiddupcheck ;
        this.modify("messageiddupcheck",messageiddupcheck);
    }

    /**
     * 设置 [发送日期]
     */
    public void setSenton(Timestamp senton){
        this.senton = senton ;
        this.modify("senton",senton);
    }

    /**
     * 格式化日期 [发送日期]
     */
    public String formatSenton(){
        if (this.senton == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(senton);
    }
    /**
     * 设置 [链接单击计数]
     */
    public void setLinksclickedcount(Integer linksclickedcount){
        this.linksclickedcount = linksclickedcount ;
        this.modify("linksclickedcount",linksclickedcount);
    }

    /**
     * 设置 [压缩]
     */
    public void setCompressed(Integer compressed){
        this.compressed = compressed ;
        this.modify("compressed",compressed);
    }

    /**
     * 设置 [活动状态]
     */
    public void setStatecode(Integer statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [答复消息]
     */
    public void setInreplyto(String inreplyto){
        this.inreplyto = inreplyto ;
        this.modify("inreplyto",inreplyto);
    }

    /**
     * 设置 [电子邮件提醒状态]
     */
    public void setEmailreminderstatus(String emailreminderstatus){
        this.emailreminderstatus = emailreminderstatus ;
        this.modify("emailreminderstatus",emailreminderstatus);
    }

    /**
     * 设置 [安全说明]
     */
    public void setSafedescription(String safedescription){
        this.safedescription = safedescription ;
        this.modify("safedescription",safedescription);
    }

    /**
     * 设置 [遍历的路径]
     */
    public void setTraversedpath(String traversedpath){
        this.traversedpath = traversedpath ;
        this.modify("traversedpath",traversedpath);
    }

    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [提醒操作卡 ID。]
     */
    public void setReminderactioncardid(String reminderactioncardid){
        this.reminderactioncardid = reminderactioncardid ;
        this.modify("reminderactioncardid",reminderactioncardid);
    }

    /**
     * 设置 [版本号]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [暂候时间(分钟)]
     */
    public void setOnholdtime(Integer onholdtime){
        this.onholdtime = onholdtime ;
        this.modify("onholdtime",onholdtime);
    }

    /**
     * 设置 [子类别]
     */
    public void setSubcategory(String subcategory){
        this.subcategory = subcategory ;
        this.modify("subcategory",subcategory);
    }

    /**
     * 设置 [后续活动]
     */
    public void setEmailfollowed(Integer emailfollowed){
        this.emailfollowed = emailfollowed ;
        this.modify("emailfollowed",emailfollowed);
    }

    /**
     * 设置 [跟踪]
     */
    public void setFollowemailuserpreference(Integer followemailuserpreference){
        this.followemailuserpreference = followemailuserpreference ;
        this.modify("followemailuserpreference",followemailuserpreference);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnername(String ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [电子邮件跟踪 ID]
     */
    public void setEmailtrackingid(String emailtrackingid){
        this.emailtrackingid = emailtrackingid ;
        this.modify("emailtrackingid",emailtrackingid);
    }

    /**
     * 设置 [关于]
     */
    public void setRegardingobjectname(String regardingobjectname){
        this.regardingobjectname = regardingobjectname ;
        this.modify("regardingobjectname",regardingobjectname);
    }

    /**
     * 设置 [开始日期]
     */
    public void setScheduledstart(Timestamp scheduledstart){
        this.scheduledstart = scheduledstart ;
        this.modify("scheduledstart",scheduledstart);
    }

    /**
     * 格式化日期 [开始日期]
     */
    public String formatScheduledstart(){
        if (this.scheduledstart == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(scheduledstart);
    }
    /**
     * 设置 [电子邮件提醒文本]
     */
    public void setEmailremindertext(String emailremindertext){
        this.emailremindertext = emailremindertext ;
        this.modify("emailremindertext",emailremindertext);
    }

    /**
     * 设置 [跟踪令牌]
     */
    public void setTrackingtoken(String trackingtoken){
        this.trackingtoken = trackingtoken ;
        this.modify("trackingtoken",trackingtoken);
    }

    /**
     * 设置 [活动类型]
     */
    public void setActivitytypecode(String activitytypecode){
        this.activitytypecode = activitytypecode ;
        this.modify("activitytypecode",activitytypecode);
    }

    /**
     * 设置 [尝试传送的次数]
     */
    public void setDeliveryattempts(Integer deliveryattempts){
        this.deliveryattempts = deliveryattempts ;
        this.modify("deliveryattempts",deliveryattempts);
    }

    /**
     * 设置 [发件人帐户类型]
     */
    public void setSendersaccountobjecttypecode(String sendersaccountobjecttypecode){
        this.sendersaccountobjecttypecode = sendersaccountobjecttypecode ;
        this.modify("sendersaccountobjecttypecode",sendersaccountobjecttypecode);
    }

    /**
     * 设置 [需要“送达”回执]
     */
    public void setDeliveryreceiptrequested(Integer deliveryreceiptrequested){
        this.deliveryreceiptrequested = deliveryreceiptrequested ;
        this.modify("deliveryreceiptrequested",deliveryreceiptrequested);
    }

    /**
     * 设置 [电子邮件提醒到期时间]
     */
    public void setEmailreminderexpirytime(Timestamp emailreminderexpirytime){
        this.emailreminderexpirytime = emailreminderexpirytime ;
        this.modify("emailreminderexpirytime",emailreminderexpirytime);
    }

    /**
     * 格式化日期 [电子邮件提醒到期时间]
     */
    public String formatEmailreminderexpirytime(){
        if (this.emailreminderexpirytime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(emailreminderexpirytime);
    }
    /**
     * 设置 [汇率]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [创建记录的时间]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [创建记录的时间]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [负责人]
     */
    public void setOwnerid(String ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [已记帐]
     */
    public void setBilled(Integer billed){
        this.billed = billed ;
        this.modify("billed",billed);
    }

    /**
     * 设置 [类别]
     */
    public void setCategory(String category){
        this.category = category ;
        this.modify("category",category);
    }

    /**
     * 设置 [对话索引(哈希)]
     */
    public void setBaseconversationindexhash(Integer baseconversationindexhash){
        this.baseconversationindexhash = baseconversationindexhash ;
        this.modify("baseconversationindexhash",baseconversationindexhash);
    }

    /**
     * 设置 [流程]
     */
    public void setProcessid(String processid){
        this.processid = processid ;
        this.modify("processid",processid);
    }

    /**
     * 设置 [对话索引]
     */
    public void setConversationindex(String conversationindex){
        this.conversationindex = conversationindex ;
        this.modify("conversationindex",conversationindex);
    }

    /**
     * 设置 [流程阶段]
     */
    public void setStageid(String stageid){
        this.stageid = stageid ;
        this.modify("stageid",stageid);
    }

    /**
     * 设置 [延迟电子邮件处理，直至]
     */
    public void setPostponeemailprocessinguntil(Timestamp postponeemailprocessinguntil){
        this.postponeemailprocessinguntil = postponeemailprocessinguntil ;
        this.modify("postponeemailprocessinguntil",postponeemailprocessinguntil);
    }

    /**
     * 格式化日期 [延迟电子邮件处理，直至]
     */
    public String formatPostponeemailprocessinguntil(){
        if (this.postponeemailprocessinguntil == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(postponeemailprocessinguntil);
    }
    /**
     * 设置 [方向]
     */
    public void setDirectioncode(Integer directioncode){
        this.directioncode = directioncode ;
        this.modify("directioncode",directioncode);
    }

    /**
     * 设置 [通知]
     */
    public void setNotifications(String notifications){
        this.notifications = notifications ;
        this.modify("notifications",notifications);
    }

    /**
     * 设置 [电子邮件发件人类型]
     */
    public void setEmailsenderobjecttypecode(String emailsenderobjecttypecode){
        this.emailsenderobjecttypecode = emailsenderobjecttypecode ;
        this.modify("emailsenderobjecttypecode",emailsenderobjecttypecode);
    }

    /**
     * 设置 [排序日期]
     */
    public void setSortdate(Timestamp sortdate){
        this.sortdate = sortdate ;
        this.modify("sortdate",sortdate);
    }

    /**
     * 格式化日期 [排序日期]
     */
    public String formatSortdate(){
        if (this.sortdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(sortdate);
    }
    /**
     * 设置 [消息 ID]
     */
    public void setMessageid(String messageid){
        this.messageid = messageid ;
        this.modify("messageid",messageid);
    }

    /**
     * 设置 [附加参数]
     */
    public void setActivityadditionalparams(String activityadditionalparams){
        this.activityadditionalparams = activityadditionalparams ;
        this.modify("activityadditionalparams",activityadditionalparams);
    }

    /**
     * 设置 [需要“已读”回执]
     */
    public void setReadreceiptrequested(Integer readreceiptrequested){
        this.readreceiptrequested = readreceiptrequested ;
        this.modify("readreceiptrequested",readreceiptrequested);
    }

    /**
     * 设置 [由工作流创建]
     */
    public void setWorkflowcreated(Integer workflowcreated){
        this.workflowcreated = workflowcreated ;
        this.modify("workflowcreated",workflowcreated);
    }

    /**
     * 设置 [IsUnsafe]
     */
    public void setUnsafe(Integer unsafe){
        this.unsafe = unsafe ;
        this.modify("unsafe",unsafe);
    }

    /**
     * 设置 [RegardingObjectTypeCode]
     */
    public void setRegardingobjecttypecode(String regardingobjecttypecode){
        this.regardingobjecttypecode = regardingobjecttypecode ;
        this.modify("regardingobjecttypecode",regardingobjecttypecode);
    }

    /**
     * 设置 [优先级]
     */
    public void setPrioritycode(String prioritycode){
        this.prioritycode = prioritycode ;
        this.modify("prioritycode",prioritycode);
    }

    /**
     * 设置 [稍后发送]
     */
    public void setDelayedemailsendtime(Timestamp delayedemailsendtime){
        this.delayedemailsendtime = delayedemailsendtime ;
        this.modify("delayedemailsendtime",delayedemailsendtime);
    }

    /**
     * 格式化日期 [稍后发送]
     */
    public String formatDelayedemailsendtime(){
        if (this.delayedemailsendtime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(delayedemailsendtime);
    }
    /**
     * 设置 [发件人]
     */
    public void setFrom(String from){
        this.from = from ;
        this.modify("from",from);
    }

    /**
     * 设置 [电子邮件发件人名称]
     */
    public void setEmailsendername(String emailsendername){
        this.emailsendername = emailsendername ;
        this.modify("emailsendername",emailsendername);
    }

    /**
     * 设置 [密件抄送]
     */
    public void setBcc(String bcc){
        this.bcc = bcc ;
        this.modify("bcc",bcc);
    }

    /**
     * 设置 [实际开始时间]
     */
    public void setActualstart(Timestamp actualstart){
        this.actualstart = actualstart ;
        this.modify("actualstart",actualstart);
    }

    /**
     * 格式化日期 [实际开始时间]
     */
    public String formatActualstart(){
        if (this.actualstart == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(actualstart);
    }
    /**
     * 设置 [实际结束时间]
     */
    public void setActualend(Timestamp actualend){
        this.actualend = actualend ;
        this.modify("actualend",actualend);
    }

    /**
     * 格式化日期 [实际结束时间]
     */
    public String formatActualend(){
        if (this.actualend == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(actualend);
    }
    /**
     * 设置 [SLAName]
     */
    public void setSlaname(String slaname){
        this.slaname = slaname ;
        this.modify("slaname",slaname);
    }

    /**
     * 设置 [对话跟踪 ID]
     */
    public void setConversationtrackingid(String conversationtrackingid){
        this.conversationtrackingid = conversationtrackingid ;
        this.modify("conversationtrackingid",conversationtrackingid);
    }

    /**
     * 设置 [发件人]
     */
    public void setSender(String sender){
        this.sender = sender ;
        this.modify("sender",sender);
    }

    /**
     * 设置 [上一暂候时间]
     */
    public void setLastonholdtime(Timestamp lastonholdtime){
        this.lastonholdtime = lastonholdtime ;
        this.modify("lastonholdtime",lastonholdtime);
    }

    /**
     * 格式化日期 [上一暂候时间]
     */
    public String formatLastonholdtime(){
        if (this.lastonholdtime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(lastonholdtime);
    }
    /**
     * 设置 [提交者]
     */
    public void setSubmittedby(String submittedby){
        this.submittedby = submittedby ;
        this.modify("submittedby",submittedby);
    }

    /**
     * 设置 [至收件人]
     */
    public void setTorecipients(String torecipients){
        this.torecipients = torecipients ;
        this.modify("torecipients",torecipients);
    }

    /**
     * 设置 [传递优先级]
     */
    public void setDeliveryprioritycode(String deliveryprioritycode){
        this.deliveryprioritycode = deliveryprioritycode ;
        this.modify("deliveryprioritycode",deliveryprioritycode);
    }

    /**
     * 设置 [附件计数]
     */
    public void setAttachmentcount(Integer attachmentcount){
        this.attachmentcount = attachmentcount ;
        this.modify("attachmentcount",attachmentcount);
    }

    /**
     * 设置 [附件打开计数]
     */
    public void setAttachmentopencount(Integer attachmentopencount){
        this.attachmentopencount = attachmentopencount ;
        this.modify("attachmentopencount",attachmentopencount);
    }

    /**
     * 设置 [计划持续时间]
     */
    public void setScheduleddurationminutes(Integer scheduleddurationminutes){
        this.scheduleddurationminutes = scheduleddurationminutes ;
        this.modify("scheduleddurationminutes",scheduleddurationminutes);
    }

    /**
     * 设置 [导入序列号]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [时区规则版本号]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [回复计数]
     */
    public void setReplycount(Integer replycount){
        this.replycount = replycount ;
        this.modify("replycount",replycount);
    }

    /**
     * 设置 [是定期活动]
     */
    public void setRegularactivity(Integer regularactivity){
        this.regularactivity = regularactivity ;
        this.modify("regularactivity",regularactivity);
    }

    /**
     * 设置 [抄送]
     */
    public void setCc(String cc){
        this.cc = cc ;
        this.modify("cc",cc);
    }

    /**
     * 设置 [状态描述]
     */
    public void setStatuscode(Integer statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [主题]
     */
    public void setSubject(String subject){
        this.subject = subject ;
        this.modify("subject",subject);
    }

    /**
     * 设置 [UTC 转换时区代码]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [已设置提醒]
     */
    public void setEmailreminderset(Integer emailreminderset){
        this.emailreminderset = emailreminderset ;
        this.modify("emailreminderset",emailreminderset);
    }

    /**
     * 设置 [持续时间]
     */
    public void setActualdurationminutes(Integer actualdurationminutes){
        this.actualdurationminutes = actualdurationminutes ;
        this.modify("actualdurationminutes",actualdurationminutes);
    }

    /**
     * 设置 [父活动 ID]
     */
    public void setParentactivityid(String parentactivityid){
        this.parentactivityid = parentactivityid ;
        this.modify("parentactivityid",parentactivityid);
    }

    /**
     * 设置 [服务]
     */
    public void setServiceid(String serviceid){
        this.serviceid = serviceid ;
        this.modify("serviceid",serviceid);
    }

    /**
     * 设置 [使用的模板的 ID。]
     */
    public void setTemplateid(String templateid){
        this.templateid = templateid ;
        this.modify("templateid",templateid);
    }

    /**
     * 设置 [SLA]
     */
    public void setSlaid(String slaid){
        this.slaid = slaid ;
        this.modify("slaid",slaid);
    }

    /**
     * 设置 [货币]
     */
    public void setTransactioncurrencyid(String transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }


}



package cn.ibizlab.businesscentral.core.sales.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.sales.domain.LeadAddress;
import cn.ibizlab.businesscentral.core.sales.filter.LeadAddressSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[LeadAddress] 服务对象接口
 */
public interface ILeadAddressService extends IService<LeadAddress>{

    boolean create(LeadAddress et) ;
    void createBatch(List<LeadAddress> list) ;
    boolean update(LeadAddress et) ;
    void updateBatch(List<LeadAddress> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    LeadAddress get(String key) ;
    LeadAddress getDraft(LeadAddress et) ;
    boolean checkKey(LeadAddress et) ;
    boolean save(LeadAddress et) ;
    void saveBatch(List<LeadAddress> list) ;
    Page<LeadAddress> searchDefault(LeadAddressSearchContext context) ;
    List<LeadAddress> selectByParentid(String leadid) ;
    void removeByParentid(String leadid) ;
    List<LeadAddress> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<LeadAddress> getLeadaddressByIds(List<String> ids) ;
    List<LeadAddress> getLeadaddressByEntities(List<LeadAddress> entities) ;
}



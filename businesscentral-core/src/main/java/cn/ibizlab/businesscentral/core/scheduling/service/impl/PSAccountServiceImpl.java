package cn.ibizlab.businesscentral.core.scheduling.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.scheduling.domain.PSAccount;
import cn.ibizlab.businesscentral.core.scheduling.filter.PSAccountSearchContext;
import cn.ibizlab.businesscentral.core.scheduling.service.IPSAccountService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import cn.ibizlab.businesscentral.core.scheduling.client.PSAccountFeignClient;

/**
 * 实体[PS账户] 服务对象接口实现
 */
@Slf4j
@Service
public class PSAccountServiceImpl implements IPSAccountService {

    @Autowired
    PSAccountFeignClient pSAccountFeignClient;


    @Override
    public boolean create(PSAccount et) {
        PSAccount rt = pSAccountFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<PSAccount> list){
        pSAccountFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(PSAccount et) {
        PSAccount rt = pSAccountFeignClient.update(et.getAccountid(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<PSAccount> list){
        pSAccountFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(String accountid) {
        boolean result=pSAccountFeignClient.remove(accountid) ;
        return result;
    }

    public void removeBatch(Collection<String> idList){
        pSAccountFeignClient.removeBatch(idList);
    }

    @Override
    public PSAccount get(String accountid) {
		PSAccount et=pSAccountFeignClient.get(accountid);
        if(et==null){
            et=new PSAccount();
            et.setAccountid(accountid);
        }
        else{
        }
        return  et;
    }

    @Override
    public PSAccount getDraft(PSAccount et) {
        et=pSAccountFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean checkKey(PSAccount et) {
        return pSAccountFeignClient.checkKey(et);
    }
    @Override
    @Transactional
    public boolean save(PSAccount et) {
        if(et.getAccountid()==null) et.setAccountid((String)et.getDefaultKey(true));
        if(!pSAccountFeignClient.save(et))
            return false;
        return true;
    }

    @Override
    public void saveBatch(List<PSAccount> list) {
        pSAccountFeignClient.saveBatch(list) ;
    }





    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<PSAccount> searchDefault(PSAccountSearchContext context) {
        Page<PSAccount> pSAccounts=pSAccountFeignClient.searchDefault(context);
        return pSAccounts;
    }



}




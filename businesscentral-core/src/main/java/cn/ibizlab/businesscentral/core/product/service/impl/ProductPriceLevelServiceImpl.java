package cn.ibizlab.businesscentral.core.product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.product.domain.ProductPriceLevel;
import cn.ibizlab.businesscentral.core.product.filter.ProductPriceLevelSearchContext;
import cn.ibizlab.businesscentral.core.product.service.IProductPriceLevelService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.product.mapper.ProductPriceLevelMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[价目表项] 服务对象接口实现
 */
@Slf4j
@Service("ProductPriceLevelServiceImpl")
public class ProductPriceLevelServiceImpl extends ServiceImpl<ProductPriceLevelMapper, ProductPriceLevel> implements IProductPriceLevelService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IDiscountTypeService discounttypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.product.service.IPriceLevelService pricelevelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.product.service.IProductService productService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IUomScheduleService uomscheduleService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IUomService uomService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(ProductPriceLevel et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getProductpricelevelid()),et);
        return true;
    }

    @Override
    public void createBatch(List<ProductPriceLevel> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(ProductPriceLevel et) {
        fillParentData(et);
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("productpricelevelid",et.getProductpricelevelid())))
            return false;
        CachedBeanCopier.copy(get(et.getProductpricelevelid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<ProductPriceLevel> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public ProductPriceLevel get(String key) {
        ProductPriceLevel et = getById(key);
        if(et==null){
            et=new ProductPriceLevel();
            et.setProductpricelevelid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public ProductPriceLevel getDraft(ProductPriceLevel et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(ProductPriceLevel et) {
        return (!ObjectUtils.isEmpty(et.getProductpricelevelid()))&&(!Objects.isNull(this.getById(et.getProductpricelevelid())));
    }
    @Override
    @Transactional
    public boolean save(ProductPriceLevel et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(ProductPriceLevel et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<ProductPriceLevel> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<ProductPriceLevel> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<ProductPriceLevel> selectByDiscounttypeid(String discounttypeid) {
        return baseMapper.selectByDiscounttypeid(discounttypeid);
    }

    @Override
    public void removeByDiscounttypeid(String discounttypeid) {
        this.remove(new QueryWrapper<ProductPriceLevel>().eq("discounttypeid",discounttypeid));
    }

	@Override
    public List<ProductPriceLevel> selectByPricelevelid(String pricelevelid) {
        return baseMapper.selectByPricelevelid(pricelevelid);
    }

    @Override
    public void removeByPricelevelid(String pricelevelid) {
        this.remove(new QueryWrapper<ProductPriceLevel>().eq("pricelevelid",pricelevelid));
    }

	@Override
    public List<ProductPriceLevel> selectByProductid(String productid) {
        return baseMapper.selectByProductid(productid);
    }

    @Override
    public void removeByProductid(String productid) {
        this.remove(new QueryWrapper<ProductPriceLevel>().eq("productid",productid));
    }

	@Override
    public List<ProductPriceLevel> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<ProductPriceLevel>().eq("transactioncurrencyid",transactioncurrencyid));
    }

	@Override
    public List<ProductPriceLevel> selectByUomscheduleid(String uomscheduleid) {
        return baseMapper.selectByUomscheduleid(uomscheduleid);
    }

    @Override
    public void removeByUomscheduleid(String uomscheduleid) {
        this.remove(new QueryWrapper<ProductPriceLevel>().eq("uomscheduleid",uomscheduleid));
    }

	@Override
    public List<ProductPriceLevel> selectByUomid(String uomid) {
        return baseMapper.selectByUomid(uomid);
    }

    @Override
    public void removeByUomid(String uomid) {
        this.remove(new QueryWrapper<ProductPriceLevel>().eq("uomid",uomid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<ProductPriceLevel> searchDefault(ProductPriceLevelSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<ProductPriceLevel> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<ProductPriceLevel>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(ProductPriceLevel et){
        //实体关系[DER1N_PRODUCTPRICELEVEL__DISCOUNTTYPE__DISCOUNTTYPEID]
        if(!ObjectUtils.isEmpty(et.getDiscounttypeid())){
            cn.ibizlab.businesscentral.core.sales.domain.DiscountType discounttype=et.getDiscounttype();
            if(ObjectUtils.isEmpty(discounttype)){
                cn.ibizlab.businesscentral.core.sales.domain.DiscountType majorEntity=discounttypeService.get(et.getDiscounttypeid());
                et.setDiscounttype(majorEntity);
                discounttype=majorEntity;
            }
            et.setDiscounttypename(discounttype.getDiscounttypename());
        }
        //实体关系[DER1N_PRODUCTPRICELEVEL__PRICELEVEL__PRICELEVELID]
        if(!ObjectUtils.isEmpty(et.getPricelevelid())){
            cn.ibizlab.businesscentral.core.product.domain.PriceLevel pricelevel=et.getPricelevel();
            if(ObjectUtils.isEmpty(pricelevel)){
                cn.ibizlab.businesscentral.core.product.domain.PriceLevel majorEntity=pricelevelService.get(et.getPricelevelid());
                et.setPricelevel(majorEntity);
                pricelevel=majorEntity;
            }
            et.setPricelevelname(pricelevel.getPricelevelname());
        }
        //实体关系[DER1N_PRODUCTPRICELEVEL__PRODUCT__PRODUCTID]
        if(!ObjectUtils.isEmpty(et.getProductid())){
            cn.ibizlab.businesscentral.core.product.domain.Product product=et.getProduct();
            if(ObjectUtils.isEmpty(product)){
                cn.ibizlab.businesscentral.core.product.domain.Product majorEntity=productService.get(et.getProductid());
                et.setProduct(majorEntity);
                product=majorEntity;
            }
            et.setProductname(product.getProductname());
        }
        //实体关系[DER1N_PRODUCTPRICELEVEL__TRANSACTIONCURRENCY__TRANSACTIONCURRENCYID]
        if(!ObjectUtils.isEmpty(et.getTransactioncurrencyid())){
            cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency=et.getTransactioncurrency();
            if(ObjectUtils.isEmpty(transactioncurrency)){
                cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency majorEntity=transactioncurrencyService.get(et.getTransactioncurrencyid());
                et.setTransactioncurrency(majorEntity);
                transactioncurrency=majorEntity;
            }
            et.setCurrencyname(transactioncurrency.getCurrencyname());
        }
        //实体关系[DER1N_PRODUCTPRICELEVEL__UOMSCHEDULE__UOMSCHEDULEID]
        if(!ObjectUtils.isEmpty(et.getUomscheduleid())){
            cn.ibizlab.businesscentral.core.base.domain.UomSchedule uomschedule=et.getUomschedule();
            if(ObjectUtils.isEmpty(uomschedule)){
                cn.ibizlab.businesscentral.core.base.domain.UomSchedule majorEntity=uomscheduleService.get(et.getUomscheduleid());
                et.setUomschedule(majorEntity);
                uomschedule=majorEntity;
            }
            et.setUomschedulename(uomschedule.getUomschedulename());
        }
        //实体关系[DER1N_PRODUCTPRICELEVEL__UOM__UOMID]
        if(!ObjectUtils.isEmpty(et.getUomid())){
            cn.ibizlab.businesscentral.core.base.domain.Uom uom=et.getUom();
            if(ObjectUtils.isEmpty(uom)){
                cn.ibizlab.businesscentral.core.base.domain.Uom majorEntity=uomService.get(et.getUomid());
                et.setUom(majorEntity);
                uom=majorEntity;
            }
            et.setUomname(uom.getUomname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<ProductPriceLevel> getProductpricelevelByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<ProductPriceLevel> getProductpricelevelByEntities(List<ProductPriceLevel> entities) {
        List ids =new ArrayList();
        for(ProductPriceLevel entity : entities){
            Serializable id=entity.getProductpricelevelid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




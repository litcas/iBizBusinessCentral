package cn.ibizlab.businesscentral.core.base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[注释]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "ANNOTATION",resultMap = "AnnotationResultMap")
public class Annotation extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 导入序列号
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * 是否私有
     */
    @DEField(defaultValue = "0")
    @TableField(value = "private")
    @JSONField(name = "ibizprivate")
    @JsonProperty("ibizprivate")
    private Integer ibizprivate;
    /**
     * 文件大小(字节)
     */
    @TableField(value = "filesize")
    @JSONField(name = "filesize")
    @JsonProperty("filesize")
    private Integer filesize;
    /**
     * StoragePointer
     */
    @TableField(value = "storagepointer")
    @JSONField(name = "storagepointer")
    @JsonProperty("storagepointer")
    private String storagepointer;
    /**
     * 说明
     */
    @TableField(value = "notetext")
    @JSONField(name = "notetext")
    @JsonProperty("notetext")
    private String notetext;
    /**
     * 是文档
     */
    @DEField(defaultValue = "0")
    @TableField(value = "document")
    @JSONField(name = "document")
    @JsonProperty("document")
    private Integer document;
    /**
     * 负责人
     */
    @TableField(value = "ownerid")
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;
    /**
     * 负责人类型
     */
    @TableField(value = "ownertype")
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 语言 ID
     */
    @TableField(value = "langid")
    @JSONField(name = "langid")
    @JsonProperty("langid")
    private String langid;
    /**
     * 负责人
     */
    @TableField(value = "ownername")
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;
    /**
     * 标题
     */
    @TableField(value = "subject")
    @JSONField(name = "subject")
    @JsonProperty("subject")
    private String subject;
    /**
     * 文件名
     */
    @TableField(value = "filename")
    @JSONField(name = "filename")
    @JsonProperty("filename")
    private String filename;
    /**
     * 关于
     */
    @TableField(value = "objectid")
    @JSONField(name = "objectid")
    @JsonProperty("objectid")
    private String objectid;
    /**
     * 注释
     */
    @DEField(isKeyField=true)
    @TableId(value= "annotationid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "annotationid")
    @JsonProperty("annotationid")
    private String annotationid;
    /**
     * 相关对象类型
     */
    @TableField(value = "objecttypecode")
    @JSONField(name = "objecttypecode")
    @JsonProperty("objecttypecode")
    private String objecttypecode;
    /**
     * 阶段 ID
     */
    @TableField(value = "stepid")
    @JSONField(name = "stepid")
    @JsonProperty("stepid")
    private String stepid;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 版本号
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * Prefix
     */
    @TableField(value = "prefix")
    @JSONField(name = "prefix")
    @JsonProperty("prefix")
    private String prefix;
    /**
     * MIME 类型
     */
    @TableField(value = "mimetype")
    @JSONField(name = "mimetype")
    @JsonProperty("mimetype")
    private String mimetype;
    /**
     * 创建记录的时间
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 文档
     */
    @TableField(value = "documentbody")
    @JSONField(name = "documentbody")
    @JsonProperty("documentbody")
    private String documentbody;
    /**
     * FilePointer
     */
    @TableField(value = "filepointer")
    @JSONField(name = "filepointer")
    @JsonProperty("filepointer")
    private String filepointer;



    /**
     * 设置 [导入序列号]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [是否私有]
     */
    public void setIbizprivate(Integer ibizprivate){
        this.ibizprivate = ibizprivate ;
        this.modify("private",ibizprivate);
    }

    /**
     * 设置 [文件大小(字节)]
     */
    public void setFilesize(Integer filesize){
        this.filesize = filesize ;
        this.modify("filesize",filesize);
    }

    /**
     * 设置 [StoragePointer]
     */
    public void setStoragepointer(String storagepointer){
        this.storagepointer = storagepointer ;
        this.modify("storagepointer",storagepointer);
    }

    /**
     * 设置 [说明]
     */
    public void setNotetext(String notetext){
        this.notetext = notetext ;
        this.modify("notetext",notetext);
    }

    /**
     * 设置 [是文档]
     */
    public void setDocument(Integer document){
        this.document = document ;
        this.modify("document",document);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnerid(String ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [负责人类型]
     */
    public void setOwnertype(String ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [语言 ID]
     */
    public void setLangid(String langid){
        this.langid = langid ;
        this.modify("langid",langid);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnername(String ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [标题]
     */
    public void setSubject(String subject){
        this.subject = subject ;
        this.modify("subject",subject);
    }

    /**
     * 设置 [文件名]
     */
    public void setFilename(String filename){
        this.filename = filename ;
        this.modify("filename",filename);
    }

    /**
     * 设置 [关于]
     */
    public void setObjectid(String objectid){
        this.objectid = objectid ;
        this.modify("objectid",objectid);
    }

    /**
     * 设置 [相关对象类型]
     */
    public void setObjecttypecode(String objecttypecode){
        this.objecttypecode = objecttypecode ;
        this.modify("objecttypecode",objecttypecode);
    }

    /**
     * 设置 [阶段 ID]
     */
    public void setStepid(String stepid){
        this.stepid = stepid ;
        this.modify("stepid",stepid);
    }

    /**
     * 设置 [版本号]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [Prefix]
     */
    public void setPrefix(String prefix){
        this.prefix = prefix ;
        this.modify("prefix",prefix);
    }

    /**
     * 设置 [MIME 类型]
     */
    public void setMimetype(String mimetype){
        this.mimetype = mimetype ;
        this.modify("mimetype",mimetype);
    }

    /**
     * 设置 [创建记录的时间]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [创建记录的时间]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [文档]
     */
    public void setDocumentbody(String documentbody){
        this.documentbody = documentbody ;
        this.modify("documentbody",documentbody);
    }

    /**
     * 设置 [FilePointer]
     */
    public void setFilepointer(String filepointer){
        this.filepointer = filepointer ;
        this.modify("filepointer",filepointer);
    }


}



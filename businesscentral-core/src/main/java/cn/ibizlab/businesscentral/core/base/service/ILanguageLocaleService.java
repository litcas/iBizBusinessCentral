package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.LanguageLocale;
import cn.ibizlab.businesscentral.core.base.filter.LanguageLocaleSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[LanguageLocale] 服务对象接口
 */
public interface ILanguageLocaleService extends IService<LanguageLocale>{

    boolean create(LanguageLocale et) ;
    void createBatch(List<LanguageLocale> list) ;
    boolean update(LanguageLocale et) ;
    void updateBatch(List<LanguageLocale> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    LanguageLocale get(String key) ;
    LanguageLocale getDraft(LanguageLocale et) ;
    boolean checkKey(LanguageLocale et) ;
    boolean save(LanguageLocale et) ;
    void saveBatch(List<LanguageLocale> list) ;
    Page<LanguageLocale> searchDefault(LanguageLocaleSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<LanguageLocale> getLanguagelocaleByIds(List<String> ids) ;
    List<LanguageLocale> getLanguagelocaleByEntities(List<LanguageLocale> entities) ;
}



package cn.ibizlab.businesscentral.core.sales.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.sales.domain.QuoteDetail;
/**
 * 关系型数据实体[QuoteDetail] 查询条件对象
 */
@Slf4j
@Data
public class QuoteDetailSearchContext extends QueryWrapperContext<QuoteDetail> {

	private String n_shipto_freighttermscode_eq;//[货运条款]
	public void setN_shipto_freighttermscode_eq(String n_shipto_freighttermscode_eq) {
        this.n_shipto_freighttermscode_eq = n_shipto_freighttermscode_eq;
        if(!ObjectUtils.isEmpty(this.n_shipto_freighttermscode_eq)){
            this.getSearchCond().eq("shipto_freighttermscode", n_shipto_freighttermscode_eq);
        }
    }
	private String n_pricingerrorcode_eq;//[定价错误]
	public void setN_pricingerrorcode_eq(String n_pricingerrorcode_eq) {
        this.n_pricingerrorcode_eq = n_pricingerrorcode_eq;
        if(!ObjectUtils.isEmpty(this.n_pricingerrorcode_eq)){
            this.getSearchCond().eq("pricingerrorcode", n_pricingerrorcode_eq);
        }
    }
	private String n_quotestatecode_eq;//[报价单状态]
	public void setN_quotestatecode_eq(String n_quotestatecode_eq) {
        this.n_quotestatecode_eq = n_quotestatecode_eq;
        if(!ObjectUtils.isEmpty(this.n_quotestatecode_eq)){
            this.getSearchCond().eq("quotestatecode", n_quotestatecode_eq);
        }
    }
	private String n_quotedetailname_like;//[名称]
	public void setN_quotedetailname_like(String n_quotedetailname_like) {
        this.n_quotedetailname_like = n_quotedetailname_like;
        if(!ObjectUtils.isEmpty(this.n_quotedetailname_like)){
            this.getSearchCond().like("quotedetailname", n_quotedetailname_like);
        }
    }
	private String n_propertyconfigurationstatus_eq;//[属性配置]
	public void setN_propertyconfigurationstatus_eq(String n_propertyconfigurationstatus_eq) {
        this.n_propertyconfigurationstatus_eq = n_propertyconfigurationstatus_eq;
        if(!ObjectUtils.isEmpty(this.n_propertyconfigurationstatus_eq)){
            this.getSearchCond().eq("propertyconfigurationstatus", n_propertyconfigurationstatus_eq);
        }
    }
	private String n_producttypecode_eq;//[产品类型]
	public void setN_producttypecode_eq(String n_producttypecode_eq) {
        this.n_producttypecode_eq = n_producttypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_producttypecode_eq)){
            this.getSearchCond().eq("producttypecode", n_producttypecode_eq);
        }
    }
	private String n_skippricecalculation_eq;//[SkipPriceCalculation]
	public void setN_skippricecalculation_eq(String n_skippricecalculation_eq) {
        this.n_skippricecalculation_eq = n_skippricecalculation_eq;
        if(!ObjectUtils.isEmpty(this.n_skippricecalculation_eq)){
            this.getSearchCond().eq("skippricecalculation", n_skippricecalculation_eq);
        }
    }
	private String n_productname_eq;//[产品名称]
	public void setN_productname_eq(String n_productname_eq) {
        this.n_productname_eq = n_productname_eq;
        if(!ObjectUtils.isEmpty(this.n_productname_eq)){
            this.getSearchCond().eq("productname", n_productname_eq);
        }
    }
	private String n_productname_like;//[产品名称]
	public void setN_productname_like(String n_productname_like) {
        this.n_productname_like = n_productname_like;
        if(!ObjectUtils.isEmpty(this.n_productname_like)){
            this.getSearchCond().like("productname", n_productname_like);
        }
    }
	private String n_parentbundleidref_eq;//[Parent bundle product]
	public void setN_parentbundleidref_eq(String n_parentbundleidref_eq) {
        this.n_parentbundleidref_eq = n_parentbundleidref_eq;
        if(!ObjectUtils.isEmpty(this.n_parentbundleidref_eq)){
            this.getSearchCond().eq("parentbundleidref", n_parentbundleidref_eq);
        }
    }
	private String n_quoteid_eq;//[报价单]
	public void setN_quoteid_eq(String n_quoteid_eq) {
        this.n_quoteid_eq = n_quoteid_eq;
        if(!ObjectUtils.isEmpty(this.n_quoteid_eq)){
            this.getSearchCond().eq("quoteid", n_quoteid_eq);
        }
    }
	private String n_productid_eq;//[现有产品]
	public void setN_productid_eq(String n_productid_eq) {
        this.n_productid_eq = n_productid_eq;
        if(!ObjectUtils.isEmpty(this.n_productid_eq)){
            this.getSearchCond().eq("productid", n_productid_eq);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }
	private String n_uomid_eq;//[计价单位]
	public void setN_uomid_eq(String n_uomid_eq) {
        this.n_uomid_eq = n_uomid_eq;
        if(!ObjectUtils.isEmpty(this.n_uomid_eq)){
            this.getSearchCond().eq("uomid", n_uomid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("quotedetailname", query)   
            );
		 }
	}
}




package cn.ibizlab.businesscentral.core.base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.base.domain.Uom;
import cn.ibizlab.businesscentral.core.base.filter.UomSearchContext;
import cn.ibizlab.businesscentral.core.base.service.IUomService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.base.mapper.UomMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[计价单位] 服务对象接口实现
 */
@Slf4j
@Service("UomServiceImpl")
public class UomServiceImpl extends ServiceImpl<UomMapper, Uom> implements IUomService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IContractDetailService contractdetailService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.stock.service.IInventoryService inventoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.finance.service.IInvoiceDetailService invoicedetailService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IOpportunityProductService opportunityproductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.product.service.IProductAssociationService productassociationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.product.service.IProductPriceLevelService productpricelevelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.product.service.IProductService productService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IQuoteDetailService quotedetailService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.ISalesOrderDetailService salesorderdetailService;

    protected cn.ibizlab.businesscentral.core.base.service.IUomService uomService = this;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IUomScheduleService uomscheduleService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(Uom et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getUomid()),et);
        return true;
    }

    @Override
    public void createBatch(List<Uom> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Uom et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("uomid",et.getUomid())))
            return false;
        CachedBeanCopier.copy(get(et.getUomid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<Uom> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Uom get(String key) {
        Uom et = getById(key);
        if(et==null){
            et=new Uom();
            et.setUomid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Uom getDraft(Uom et) {
        return et;
    }

    @Override
    public boolean checkKey(Uom et) {
        return (!ObjectUtils.isEmpty(et.getUomid()))&&(!Objects.isNull(this.getById(et.getUomid())));
    }
    @Override
    @Transactional
    public boolean save(Uom et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Uom et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<Uom> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<Uom> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Uom> selectByUomscheduleid(String uomscheduleid) {
        return baseMapper.selectByUomscheduleid(uomscheduleid);
    }

    @Override
    public void removeByUomscheduleid(String uomscheduleid) {
        this.remove(new QueryWrapper<Uom>().eq("uomscheduleid",uomscheduleid));
    }

	@Override
    public List<Uom> selectByBaseuom(String uomid) {
        return baseMapper.selectByBaseuom(uomid);
    }

    @Override
    public void removeByBaseuom(String uomid) {
        this.remove(new QueryWrapper<Uom>().eq("baseuom",uomid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<Uom> searchDefault(UomSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Uom> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Uom>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Uom> getUomByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Uom> getUomByEntities(List<Uom> entities) {
        List ids =new ArrayList();
        for(Uom entity : entities){
            Serializable id=entity.getUomid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




package cn.ibizlab.businesscentral.core.sales.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.sales.domain.Lead;
import cn.ibizlab.businesscentral.core.sales.filter.LeadSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface LeadMapper extends BaseMapper<Lead>{

    Page<Lead> searchDefault(IPage page, @Param("srf") LeadSearchContext context, @Param("ew") Wrapper<Lead> wrapper) ;
    Page<Lead> searchExcluded(IPage page, @Param("srf") LeadSearchContext context, @Param("ew") Wrapper<Lead> wrapper) ;
    Page<Lead> searchOn(IPage page, @Param("srf") LeadSearchContext context, @Param("ew") Wrapper<Lead> wrapper) ;
    @Override
    Lead selectById(Serializable id);
    @Override
    int insert(Lead entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Lead entity);
    @Override
    int update(@Param(Constants.ENTITY) Lead entity, @Param("ew") Wrapper<Lead> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Lead> selectByParentaccountid(@Param("accountid") Serializable accountid) ;

    List<Lead> selectByRelatedobjectid(@Param("activityid") Serializable activityid) ;

    List<Lead> selectByCampaignid(@Param("campaignid") Serializable campaignid) ;

    List<Lead> selectByParentcontactid(@Param("contactid") Serializable contactid) ;

    List<Lead> selectByOriginatingcaseid(@Param("incidentid") Serializable incidentid) ;

    List<Lead> selectByQualifyingopportunityid(@Param("opportunityid") Serializable opportunityid) ;

    List<Lead> selectBySlaid(@Param("slaid") Serializable slaid) ;

    List<Lead> selectByTransactioncurrencyid(@Param("transactioncurrencyid") Serializable transactioncurrencyid) ;

}

package cn.ibizlab.businesscentral.core.scheduling.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.scheduling.domain.BookableResourceGroup;
import cn.ibizlab.businesscentral.core.scheduling.filter.BookableResourceGroupSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[BookableResourceGroup] 服务对象接口
 */
public interface IBookableResourceGroupService extends IService<BookableResourceGroup>{

    boolean create(BookableResourceGroup et) ;
    void createBatch(List<BookableResourceGroup> list) ;
    boolean update(BookableResourceGroup et) ;
    void updateBatch(List<BookableResourceGroup> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    BookableResourceGroup get(String key) ;
    BookableResourceGroup getDraft(BookableResourceGroup et) ;
    boolean checkKey(BookableResourceGroup et) ;
    boolean save(BookableResourceGroup et) ;
    void saveBatch(List<BookableResourceGroup> list) ;
    Page<BookableResourceGroup> searchDefault(BookableResourceGroupSearchContext context) ;
    List<BookableResourceGroup> selectByChildresource(String bookableresourceid) ;
    void removeByChildresource(String bookableresourceid) ;
    List<BookableResourceGroup> selectByParentresource(String bookableresourceid) ;
    void removeByParentresource(String bookableresourceid) ;
    List<BookableResourceGroup> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<BookableResourceGroup> getBookableresourcegroupByIds(List<String> ids) ;
    List<BookableResourceGroup> getBookableresourcegroupByEntities(List<BookableResourceGroup> entities) ;
}



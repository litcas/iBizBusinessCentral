package cn.ibizlab.businesscentral.core.runtime.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[批量删除操作]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "BULKDELETEOPERATION",resultMap = "BulkDeleteOperationResultMap")
public class BulkDeleteOperation extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 仅供内部使用。
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * 状态描述
     */
    @TableField(value = "statuscode")
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;
    /**
     * 批量删除作业
     */
    @DEField(isKeyField=true)
    @TableId(value= "bulkdeleteoperationid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "bulkdeleteoperationid")
    @JsonProperty("bulkdeleteoperationid")
    private String bulkdeleteoperationid;
    /**
     * 状态
     */
    @TableField(value = "statecode")
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;
    /**
     * 按序查询集的 Fetch XML。
     */
    @TableField(value = "orderedquerysetxml")
    @JSONField(name = "orderedquerysetxml")
    @JsonProperty("orderedquerysetxml")
    private String orderedquerysetxml;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 负责人
     */
    @TableField(value = "ownerid")
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;
    /**
     * 查询索引
     */
    @TableField(value = "processingqeindex")
    @JSONField(name = "processingqeindex")
    @JsonProperty("processingqeindex")
    private Integer processingqeindex;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 已删除
     */
    @TableField(value = "successcount")
    @JSONField(name = "successcount")
    @JsonProperty("successcount")
    private Integer successcount;
    /**
     * 批量删除操作名称
     */
    @TableField(value = "bulkdeleteoperationname")
    @JSONField(name = "bulkdeleteoperationname")
    @JsonProperty("bulkdeleteoperationname")
    private String bulkdeleteoperationname;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 负责人类型
     */
    @TableField(value = "ownertype")
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 定期方式
     */
    @DEField(defaultValue = "0")
    @TableField(value = "recurring")
    @JSONField(name = "recurring")
    @JsonProperty("recurring")
    private Integer recurring;
    /**
     * 创建记录时所使用的时区代码。
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * 下次运行时间
     */
    @TableField(value = "nextrun")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "nextrun" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("nextrun")
    private Timestamp nextrun;
    /**
     * 失败数
     */
    @TableField(value = "failurecount")
    @JSONField(name = "failurecount")
    @JsonProperty("failurecount")
    private Integer failurecount;



    /**
     * 设置 [仅供内部使用。]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [状态描述]
     */
    public void setStatuscode(Integer statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [状态]
     */
    public void setStatecode(Integer statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [按序查询集的 Fetch XML。]
     */
    public void setOrderedquerysetxml(String orderedquerysetxml){
        this.orderedquerysetxml = orderedquerysetxml ;
        this.modify("orderedquerysetxml",orderedquerysetxml);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnerid(String ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [查询索引]
     */
    public void setProcessingqeindex(Integer processingqeindex){
        this.processingqeindex = processingqeindex ;
        this.modify("processingqeindex",processingqeindex);
    }

    /**
     * 设置 [已删除]
     */
    public void setSuccesscount(Integer successcount){
        this.successcount = successcount ;
        this.modify("successcount",successcount);
    }

    /**
     * 设置 [批量删除操作名称]
     */
    public void setBulkdeleteoperationname(String bulkdeleteoperationname){
        this.bulkdeleteoperationname = bulkdeleteoperationname ;
        this.modify("bulkdeleteoperationname",bulkdeleteoperationname);
    }

    /**
     * 设置 [负责人类型]
     */
    public void setOwnertype(String ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [定期方式]
     */
    public void setRecurring(Integer recurring){
        this.recurring = recurring ;
        this.modify("recurring",recurring);
    }

    /**
     * 设置 [创建记录时所使用的时区代码。]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [下次运行时间]
     */
    public void setNextrun(Timestamp nextrun){
        this.nextrun = nextrun ;
        this.modify("nextrun",nextrun);
    }

    /**
     * 格式化日期 [下次运行时间]
     */
    public String formatNextrun(){
        if (this.nextrun == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(nextrun);
    }
    /**
     * 设置 [失败数]
     */
    public void setFailurecount(Integer failurecount){
        this.failurecount = failurecount ;
        this.modify("failurecount",failurecount);
    }


}



package cn.ibizlab.businesscentral.core.base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency;
import cn.ibizlab.businesscentral.core.base.filter.TransactionCurrencySearchContext;
import cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.base.mapper.TransactionCurrencyMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[货币] 服务对象接口实现
 */
@Slf4j
@Service("TransactionCurrencyServiceImpl")
public class TransactionCurrencyServiceImpl extends ServiceImpl<TransactionCurrencyMapper, TransactionCurrency> implements ITransactionCurrencyService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IAccountService accountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IActivityPointerService activitypointerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.finance.service.IAnnualFiscalCalendarService annualfiscalcalendarService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IAppointmentService appointmentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.scheduling.service.IBookableResourceBookingHeaderService bookableresourcebookingheaderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.scheduling.service.IBookableResourceBookingService bookableresourcebookingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.scheduling.service.IBookableResourceCategoryAssnService bookableresourcecategoryassnService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.scheduling.service.IBookableResourceCategoryService bookableresourcecategoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.scheduling.service.IBookableResourceCharacteristicService bookableresourcecharacteristicService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.scheduling.service.IBookableResourceGroupService bookableresourcegroupService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.scheduling.service.IBookableResourceService bookableresourceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.scheduling.service.IBookingStatusService bookingstatusService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.runtime.service.IBulkOperationService bulkoperationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IBusinessUnitService businessunitService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.marketing.service.ICampaignResponseService campaignresponseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.marketing.service.ICampaignService campaignService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ICategoryService categoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.scheduling.service.ICharacteristicService characteristicService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.ICompetitorService competitorService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.runtime.service.IConnectionService connectionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IContactService contactService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IContractService contractService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ICustomerAddressService customeraddressService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IDiscountTypeService discounttypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IEmailService emailService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IEntitlementChannelService entitlementchannelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IEntitlementTemplateChannelService entitlementtemplatechannelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IEntitlementTemplateService entitlementtemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IEntitlementService entitlementService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IEquipmentService equipmentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.runtime.service.IExpiredprocessService expiredprocessService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IFaxService faxService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IFeedbackService feedbackService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.finance.service.IFixedMonthlyFiscalCalendarService fixedmonthlyfiscalcalendarService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IIncidentreSolutionService incidentresolutionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IIncidentService incidentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.finance.service.IInvoiceDetailService invoicedetailService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.finance.service.IInvoiceService invoiceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IKnowledgeArticleService knowledgearticleService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IKnowledgeBaseRecordService knowledgebaserecordService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.ILeadAddressService leadaddressService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.ILeadService leadService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ILetterService letterService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.marketing.service.IIBizListService ibizlistService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.finance.service.IMonthlyFiscalCalendarService monthlyfiscalcalendarService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IOpportunityCloseService opportunitycloseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IOpportunityProductService opportunityproductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IOpportunityService opportunityService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IOrderCloseService ordercloseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IPhoneCallService phonecallService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IPositionService positionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.product.service.IPriceLevelService pricelevelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.product.service.IProductAssociationService productassociationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.product.service.IProductPriceLevelService productpricelevelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.product.service.IProductSubstituteService productsubstituteService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.product.service.IProductService productService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.finance.service.IQuarterlyFiscalCalendarService quarterlyfiscalcalendarService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.runtime.service.IQueueItemService queueitemService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.runtime.service.IQueueService queueService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IQuoteCloseService quotecloseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IQuoteDetailService quotedetailService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IQuoteService quoteService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.scheduling.service.IRatingModelService ratingmodelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.scheduling.service.IRatingValueService ratingvalueService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.ISalesLiteratureService salesliteratureService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.ISalesOrderDetailService salesorderdetailService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.ISalesOrderService salesorderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.finance.service.ISemiannualFiscalCalendarService semiannualfiscalcalendarService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IServiceAppointmentService serviceappointmentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ISlaKpiInstanceService slakpiinstanceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITaskService taskService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITeamService teamService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITerritoryService territoryService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(TransactionCurrency et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getTransactioncurrencyid()),et);
        return true;
    }

    @Override
    public void createBatch(List<TransactionCurrency> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(TransactionCurrency et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("transactioncurrencyid",et.getTransactioncurrencyid())))
            return false;
        CachedBeanCopier.copy(get(et.getTransactioncurrencyid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<TransactionCurrency> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public TransactionCurrency get(String key) {
        TransactionCurrency et = getById(key);
        if(et==null){
            et=new TransactionCurrency();
            et.setTransactioncurrencyid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public TransactionCurrency getDraft(TransactionCurrency et) {
        return et;
    }

    @Override
    public boolean checkKey(TransactionCurrency et) {
        return (!ObjectUtils.isEmpty(et.getTransactioncurrencyid()))&&(!Objects.isNull(this.getById(et.getTransactioncurrencyid())));
    }
    @Override
    @Transactional
    public boolean save(TransactionCurrency et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(TransactionCurrency et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<TransactionCurrency> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<TransactionCurrency> list) {
        saveOrUpdateBatch(list,batchSize);
    }



    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<TransactionCurrency> searchDefault(TransactionCurrencySearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<TransactionCurrency> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<TransactionCurrency>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<TransactionCurrency> getTransactioncurrencyByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<TransactionCurrency> getTransactioncurrencyByEntities(List<TransactionCurrency> entities) {
        List ids =new ArrayList();
        for(TransactionCurrency entity : entities){
            Serializable id=entity.getTransactioncurrencyid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




package cn.ibizlab.businesscentral.core.sales.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.sales.domain.SalesOrderDetail;
import cn.ibizlab.businesscentral.core.sales.filter.SalesOrderDetailSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface SalesOrderDetailMapper extends BaseMapper<SalesOrderDetail>{

    Page<SalesOrderDetail> searchDefault(IPage page, @Param("srf") SalesOrderDetailSearchContext context, @Param("ew") Wrapper<SalesOrderDetail> wrapper) ;
    @Override
    SalesOrderDetail selectById(Serializable id);
    @Override
    int insert(SalesOrderDetail entity);
    @Override
    int updateById(@Param(Constants.ENTITY) SalesOrderDetail entity);
    @Override
    int update(@Param(Constants.ENTITY) SalesOrderDetail entity, @Param("ew") Wrapper<SalesOrderDetail> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<SalesOrderDetail> selectByProductid(@Param("productid") Serializable productid) ;

    List<SalesOrderDetail> selectByQuotedetailid(@Param("quotedetailid") Serializable quotedetailid) ;

    List<SalesOrderDetail> selectByParentbundleidref(@Param("salesorderdetailid") Serializable salesorderdetailid) ;

    List<SalesOrderDetail> selectBySalesorderid(@Param("salesorderid") Serializable salesorderid) ;

    List<SalesOrderDetail> selectByTransactioncurrencyid(@Param("transactioncurrencyid") Serializable transactioncurrencyid) ;

    List<SalesOrderDetail> selectByUomid(@Param("uomid") Serializable uomid) ;

}

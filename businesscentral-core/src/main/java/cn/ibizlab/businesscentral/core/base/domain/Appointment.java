package cn.ibizlab.businesscentral.core.base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[约会]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "APPOINTMENT",resultMap = "AppointmentResultMap")
public class Appointment extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 导入序列号
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * 预订
     */
    @TableField(value = "subscriptionid")
    @JSONField(name = "subscriptionid")
    @JsonProperty("subscriptionid")
    private String subscriptionid;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 附件计数
     */
    @TableField(value = "attachmentcount")
    @JSONField(name = "attachmentcount")
    @JsonProperty("attachmentcount")
    private Integer attachmentcount;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 最初开始日期
     */
    @TableField(value = "originalstartdate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "originalstartdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("originalstartdate")
    private Timestamp originalstartdate;
    /**
     * SLAName
     */
    @TableField(value = "slaname")
    @JSONField(name = "slaname")
    @JsonProperty("slaname")
    private String slaname;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 约会
     */
    @DEField(isKeyField=true)
    @TableId(value= "activityid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "activityid")
    @JsonProperty("activityid")
    private String activityid;
    /**
     * 实际开始时间
     */
    @TableField(value = "actualstart")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "actualstart" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("actualstart")
    private Timestamp actualstart;
    /**
     * 关于
     */
    @TableField(value = "regardingobjectid")
    @JSONField(name = "regardingobjectid")
    @JsonProperty("regardingobjectid")
    private String regardingobjectid;
    /**
     * 实际结束时间
     */
    @TableField(value = "actualend")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "actualend" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("actualend")
    private Timestamp actualend;
    /**
     * 子类别
     */
    @TableField(value = "subcategory")
    @JSONField(name = "subcategory")
    @JsonProperty("subcategory")
    private String subcategory;
    /**
     * 全天事件
     */
    @DEField(defaultValue = "0")
    @TableField(value = "alldayevent")
    @JSONField(name = "alldayevent")
    @JsonProperty("alldayevent")
    private Integer alldayevent;
    /**
     * RegardingObjectTypeCode
     */
    @TableField(value = "regardingobjecttypecode")
    @JSONField(name = "regardingobjecttypecode")
    @JsonProperty("regardingobjecttypecode")
    private String regardingobjecttypecode;
    /**
     * 约会结束时间
     */
    @TableField(value = "scheduledend")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "scheduledend" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("scheduledend")
    private Timestamp scheduledend;
    /**
     * Outlook 约会负责人
     */
    @TableField(value = "outlookownerapptid")
    @JSONField(name = "outlookownerapptid")
    @JsonProperty("outlookownerapptid")
    private Integer outlookownerapptid;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 是定期活动
     */
    @DEField(defaultValue = "0")
    @TableField(value = "regularactivity")
    @JSONField(name = "regularactivity")
    @JsonProperty("regularactivity")
    private Integer regularactivity;
    /**
     * IsUnsafe
     */
    @TableField(value = "unsafe")
    @JSONField(name = "unsafe")
    @JsonProperty("unsafe")
    private Integer unsafe;
    /**
     * 系列 ID
     */
    @TableField(value = "seriesid")
    @JSONField(name = "seriesid")
    @JsonProperty("seriesid")
    private String seriesid;
    /**
     * Outlook 约会
     */
    @TableField(value = "globalobjectid")
    @JSONField(name = "globalobjectid")
    @JsonProperty("globalobjectid")
    private String globalobjectid;
    /**
     * 隐藏
     */
    @DEField(defaultValue = "0")
    @TableField(value = "mapiprivate")
    @JSONField(name = "mapiprivate")
    @JsonProperty("mapiprivate")
    private Integer mapiprivate;
    /**
     * 安全说明
     */
    @TableField(value = "safedescription")
    @JSONField(name = "safedescription")
    @JsonProperty("safedescription")
    private String safedescription;
    /**
     * 主题
     */
    @TableField(value = "subject")
    @JSONField(name = "subject")
    @JsonProperty("subject")
    private String subject;
    /**
     * 由工作流创建
     */
    @DEField(defaultValue = "0")
    @TableField(value = "workflowcreated")
    @JSONField(name = "workflowcreated")
    @JsonProperty("workflowcreated")
    private Integer workflowcreated;
    /**
     * 关于
     */
    @TableField(value = "regardingobjectname")
    @JSONField(name = "regardingobjectname")
    @JsonProperty("regardingobjectname")
    private String regardingobjectname;
    /**
     * 类别
     */
    @TableField(value = "category")
    @JSONField(name = "category")
    @JsonProperty("category")
    private String category;
    /**
     * UTC 转换时区代码
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * 已修改的字段掩码
     */
    @TableField(value = "modifiedfieldsmask")
    @JSONField(name = "modifiedfieldsmask")
    @JsonProperty("modifiedfieldsmask")
    private String modifiedfieldsmask;
    /**
     * 状态
     */
    @TableField(value = "statecode")
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;
    /**
     * 必须出席的人员
     */
    @TableField(value = "requiredattendees")
    @JSONField(name = "requiredattendees")
    @JsonProperty("requiredattendees")
    private String requiredattendees;
    /**
     * 排序日期
     */
    @TableField(value = "sortdate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "sortdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("sortdate")
    private Timestamp sortdate;
    /**
     * 版本号
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 实际持续时间
     */
    @TableField(value = "actualdurationminutes")
    @JSONField(name = "actualdurationminutes")
    @JsonProperty("actualdurationminutes")
    private Integer actualdurationminutes;
    /**
     * 上一暂候时间
     */
    @TableField(value = "lastonholdtime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "lastonholdtime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("lastonholdtime")
    private Timestamp lastonholdtime;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 流程阶段
     */
    @TableField(value = "stageid")
    @JSONField(name = "stageid")
    @JsonProperty("stageid")
    private String stageid;
    /**
     * 遍历的路径
     */
    @TableField(value = "traversedpath")
    @JSONField(name = "traversedpath")
    @JsonProperty("traversedpath")
    private String traversedpath;
    /**
     * 优先级
     */
    @TableField(value = "prioritycode")
    @JSONField(name = "prioritycode")
    @JsonProperty("prioritycode")
    private String prioritycode;
    /**
     * AttachmentErrors
     */
    @TableField(value = "attachmenterrors")
    @JSONField(name = "attachmenterrors")
    @JsonProperty("attachmenterrors")
    private String attachmenterrors;
    /**
     * 状态描述
     */
    @TableField(value = "statuscode")
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;
    /**
     * 负责人
     */
    @TableField(value = "ownerid")
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;
    /**
     * 暂候时间(分钟)
     */
    @TableField(value = "onholdtime")
    @JSONField(name = "onholdtime")
    @JsonProperty("onholdtime")
    private Integer onholdtime;
    /**
     * 组织者
     */
    @TableField(value = "organizer")
    @JSONField(name = "organizer")
    @JsonProperty("organizer")
    private String organizer;
    /**
     * 时区规则版本号
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * 负责人
     */
    @TableField(value = "ownername")
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;
    /**
     * 已记帐
     */
    @DEField(defaultValue = "0")
    @TableField(value = "billed")
    @JSONField(name = "billed")
    @JsonProperty("billed")
    private Integer billed;
    /**
     * 活动类型
     */
    @TableField(value = "activitytypecode")
    @JSONField(name = "activitytypecode")
    @JsonProperty("activitytypecode")
    private String activitytypecode;
    /**
     * 创建记录的时间
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 开始时间
     */
    @TableField(value = "scheduledstart")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "scheduledstart" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("scheduledstart")
    private Timestamp scheduledstart;
    /**
     * 负责人类型
     */
    @TableField(value = "ownertype")
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;
    /**
     * 汇率
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * 赴约者(备选对象)
     */
    @TableField(value = "optionalattendees")
    @JSONField(name = "optionalattendees")
    @JsonProperty("optionalattendees")
    private String optionalattendees;
    /**
     * 附加参数
     */
    @TableField(value = "activityadditionalparams")
    @JSONField(name = "activityadditionalparams")
    @JsonProperty("activityadditionalparams")
    private String activityadditionalparams;
    /**
     * IsDraft
     */
    @DEField(defaultValue = "0")
    @TableField(value = "draft")
    @JSONField(name = "draft")
    @JsonProperty("draft")
    private Integer draft;
    /**
     * 约会类型
     */
    @TableField(value = "instancetypecode")
    @JSONField(name = "instancetypecode")
    @JsonProperty("instancetypecode")
    private String instancetypecode;
    /**
     * 地点
     */
    @TableField(value = "location")
    @JSONField(name = "location")
    @JsonProperty("location")
    private String location;
    /**
     * 持续时间
     */
    @TableField(value = "scheduleddurationminutes")
    @JSONField(name = "scheduleddurationminutes")
    @JsonProperty("scheduleddurationminutes")
    private Integer scheduleddurationminutes;
    /**
     * 流程
     */
    @TableField(value = "processid")
    @JSONField(name = "processid")
    @JsonProperty("processid")
    private String processid;
    /**
     * SLA
     */
    @TableField(value = "slaid")
    @JSONField(name = "slaid")
    @JsonProperty("slaid")
    private String slaid;
    /**
     * 货币
     */
    @TableField(value = "transactioncurrencyid")
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;
    /**
     * 服务
     */
    @TableField(value = "serviceid")
    @JSONField(name = "serviceid")
    @JsonProperty("serviceid")
    private String serviceid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.service.domain.IBizService service;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Sla sla;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency;



    /**
     * 设置 [导入序列号]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [预订]
     */
    public void setSubscriptionid(String subscriptionid){
        this.subscriptionid = subscriptionid ;
        this.modify("subscriptionid",subscriptionid);
    }

    /**
     * 设置 [附件计数]
     */
    public void setAttachmentcount(Integer attachmentcount){
        this.attachmentcount = attachmentcount ;
        this.modify("attachmentcount",attachmentcount);
    }

    /**
     * 设置 [最初开始日期]
     */
    public void setOriginalstartdate(Timestamp originalstartdate){
        this.originalstartdate = originalstartdate ;
        this.modify("originalstartdate",originalstartdate);
    }

    /**
     * 格式化日期 [最初开始日期]
     */
    public String formatOriginalstartdate(){
        if (this.originalstartdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(originalstartdate);
    }
    /**
     * 设置 [SLAName]
     */
    public void setSlaname(String slaname){
        this.slaname = slaname ;
        this.modify("slaname",slaname);
    }

    /**
     * 设置 [实际开始时间]
     */
    public void setActualstart(Timestamp actualstart){
        this.actualstart = actualstart ;
        this.modify("actualstart",actualstart);
    }

    /**
     * 格式化日期 [实际开始时间]
     */
    public String formatActualstart(){
        if (this.actualstart == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(actualstart);
    }
    /**
     * 设置 [关于]
     */
    public void setRegardingobjectid(String regardingobjectid){
        this.regardingobjectid = regardingobjectid ;
        this.modify("regardingobjectid",regardingobjectid);
    }

    /**
     * 设置 [实际结束时间]
     */
    public void setActualend(Timestamp actualend){
        this.actualend = actualend ;
        this.modify("actualend",actualend);
    }

    /**
     * 格式化日期 [实际结束时间]
     */
    public String formatActualend(){
        if (this.actualend == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(actualend);
    }
    /**
     * 设置 [子类别]
     */
    public void setSubcategory(String subcategory){
        this.subcategory = subcategory ;
        this.modify("subcategory",subcategory);
    }

    /**
     * 设置 [全天事件]
     */
    public void setAlldayevent(Integer alldayevent){
        this.alldayevent = alldayevent ;
        this.modify("alldayevent",alldayevent);
    }

    /**
     * 设置 [RegardingObjectTypeCode]
     */
    public void setRegardingobjecttypecode(String regardingobjecttypecode){
        this.regardingobjecttypecode = regardingobjecttypecode ;
        this.modify("regardingobjecttypecode",regardingobjecttypecode);
    }

    /**
     * 设置 [约会结束时间]
     */
    public void setScheduledend(Timestamp scheduledend){
        this.scheduledend = scheduledend ;
        this.modify("scheduledend",scheduledend);
    }

    /**
     * 格式化日期 [约会结束时间]
     */
    public String formatScheduledend(){
        if (this.scheduledend == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(scheduledend);
    }
    /**
     * 设置 [Outlook 约会负责人]
     */
    public void setOutlookownerapptid(Integer outlookownerapptid){
        this.outlookownerapptid = outlookownerapptid ;
        this.modify("outlookownerapptid",outlookownerapptid);
    }

    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [是定期活动]
     */
    public void setRegularactivity(Integer regularactivity){
        this.regularactivity = regularactivity ;
        this.modify("regularactivity",regularactivity);
    }

    /**
     * 设置 [IsUnsafe]
     */
    public void setUnsafe(Integer unsafe){
        this.unsafe = unsafe ;
        this.modify("unsafe",unsafe);
    }

    /**
     * 设置 [系列 ID]
     */
    public void setSeriesid(String seriesid){
        this.seriesid = seriesid ;
        this.modify("seriesid",seriesid);
    }

    /**
     * 设置 [Outlook 约会]
     */
    public void setGlobalobjectid(String globalobjectid){
        this.globalobjectid = globalobjectid ;
        this.modify("globalobjectid",globalobjectid);
    }

    /**
     * 设置 [隐藏]
     */
    public void setMapiprivate(Integer mapiprivate){
        this.mapiprivate = mapiprivate ;
        this.modify("mapiprivate",mapiprivate);
    }

    /**
     * 设置 [安全说明]
     */
    public void setSafedescription(String safedescription){
        this.safedescription = safedescription ;
        this.modify("safedescription",safedescription);
    }

    /**
     * 设置 [主题]
     */
    public void setSubject(String subject){
        this.subject = subject ;
        this.modify("subject",subject);
    }

    /**
     * 设置 [由工作流创建]
     */
    public void setWorkflowcreated(Integer workflowcreated){
        this.workflowcreated = workflowcreated ;
        this.modify("workflowcreated",workflowcreated);
    }

    /**
     * 设置 [关于]
     */
    public void setRegardingobjectname(String regardingobjectname){
        this.regardingobjectname = regardingobjectname ;
        this.modify("regardingobjectname",regardingobjectname);
    }

    /**
     * 设置 [类别]
     */
    public void setCategory(String category){
        this.category = category ;
        this.modify("category",category);
    }

    /**
     * 设置 [UTC 转换时区代码]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [已修改的字段掩码]
     */
    public void setModifiedfieldsmask(String modifiedfieldsmask){
        this.modifiedfieldsmask = modifiedfieldsmask ;
        this.modify("modifiedfieldsmask",modifiedfieldsmask);
    }

    /**
     * 设置 [状态]
     */
    public void setStatecode(Integer statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [必须出席的人员]
     */
    public void setRequiredattendees(String requiredattendees){
        this.requiredattendees = requiredattendees ;
        this.modify("requiredattendees",requiredattendees);
    }

    /**
     * 设置 [排序日期]
     */
    public void setSortdate(Timestamp sortdate){
        this.sortdate = sortdate ;
        this.modify("sortdate",sortdate);
    }

    /**
     * 格式化日期 [排序日期]
     */
    public String formatSortdate(){
        if (this.sortdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(sortdate);
    }
    /**
     * 设置 [版本号]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [实际持续时间]
     */
    public void setActualdurationminutes(Integer actualdurationminutes){
        this.actualdurationminutes = actualdurationminutes ;
        this.modify("actualdurationminutes",actualdurationminutes);
    }

    /**
     * 设置 [上一暂候时间]
     */
    public void setLastonholdtime(Timestamp lastonholdtime){
        this.lastonholdtime = lastonholdtime ;
        this.modify("lastonholdtime",lastonholdtime);
    }

    /**
     * 格式化日期 [上一暂候时间]
     */
    public String formatLastonholdtime(){
        if (this.lastonholdtime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(lastonholdtime);
    }
    /**
     * 设置 [流程阶段]
     */
    public void setStageid(String stageid){
        this.stageid = stageid ;
        this.modify("stageid",stageid);
    }

    /**
     * 设置 [遍历的路径]
     */
    public void setTraversedpath(String traversedpath){
        this.traversedpath = traversedpath ;
        this.modify("traversedpath",traversedpath);
    }

    /**
     * 设置 [优先级]
     */
    public void setPrioritycode(String prioritycode){
        this.prioritycode = prioritycode ;
        this.modify("prioritycode",prioritycode);
    }

    /**
     * 设置 [AttachmentErrors]
     */
    public void setAttachmenterrors(String attachmenterrors){
        this.attachmenterrors = attachmenterrors ;
        this.modify("attachmenterrors",attachmenterrors);
    }

    /**
     * 设置 [状态描述]
     */
    public void setStatuscode(Integer statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnerid(String ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [暂候时间(分钟)]
     */
    public void setOnholdtime(Integer onholdtime){
        this.onholdtime = onholdtime ;
        this.modify("onholdtime",onholdtime);
    }

    /**
     * 设置 [组织者]
     */
    public void setOrganizer(String organizer){
        this.organizer = organizer ;
        this.modify("organizer",organizer);
    }

    /**
     * 设置 [时区规则版本号]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnername(String ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [已记帐]
     */
    public void setBilled(Integer billed){
        this.billed = billed ;
        this.modify("billed",billed);
    }

    /**
     * 设置 [活动类型]
     */
    public void setActivitytypecode(String activitytypecode){
        this.activitytypecode = activitytypecode ;
        this.modify("activitytypecode",activitytypecode);
    }

    /**
     * 设置 [创建记录的时间]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [创建记录的时间]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [开始时间]
     */
    public void setScheduledstart(Timestamp scheduledstart){
        this.scheduledstart = scheduledstart ;
        this.modify("scheduledstart",scheduledstart);
    }

    /**
     * 格式化日期 [开始时间]
     */
    public String formatScheduledstart(){
        if (this.scheduledstart == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(scheduledstart);
    }
    /**
     * 设置 [负责人类型]
     */
    public void setOwnertype(String ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [汇率]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [赴约者(备选对象)]
     */
    public void setOptionalattendees(String optionalattendees){
        this.optionalattendees = optionalattendees ;
        this.modify("optionalattendees",optionalattendees);
    }

    /**
     * 设置 [附加参数]
     */
    public void setActivityadditionalparams(String activityadditionalparams){
        this.activityadditionalparams = activityadditionalparams ;
        this.modify("activityadditionalparams",activityadditionalparams);
    }

    /**
     * 设置 [IsDraft]
     */
    public void setDraft(Integer draft){
        this.draft = draft ;
        this.modify("draft",draft);
    }

    /**
     * 设置 [约会类型]
     */
    public void setInstancetypecode(String instancetypecode){
        this.instancetypecode = instancetypecode ;
        this.modify("instancetypecode",instancetypecode);
    }

    /**
     * 设置 [地点]
     */
    public void setLocation(String location){
        this.location = location ;
        this.modify("location",location);
    }

    /**
     * 设置 [持续时间]
     */
    public void setScheduleddurationminutes(Integer scheduleddurationminutes){
        this.scheduleddurationminutes = scheduleddurationminutes ;
        this.modify("scheduleddurationminutes",scheduleddurationminutes);
    }

    /**
     * 设置 [流程]
     */
    public void setProcessid(String processid){
        this.processid = processid ;
        this.modify("processid",processid);
    }

    /**
     * 设置 [SLA]
     */
    public void setSlaid(String slaid){
        this.slaid = slaid ;
        this.modify("slaid",slaid);
    }

    /**
     * 设置 [货币]
     */
    public void setTransactioncurrencyid(String transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }

    /**
     * 设置 [服务]
     */
    public void setServiceid(String serviceid){
        this.serviceid = serviceid ;
        this.modify("serviceid",serviceid);
    }


}



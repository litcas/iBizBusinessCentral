package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.Letter;
import cn.ibizlab.businesscentral.core.base.filter.LetterSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Letter] 服务对象接口
 */
public interface ILetterService extends IService<Letter>{

    boolean create(Letter et) ;
    void createBatch(List<Letter> list) ;
    boolean update(Letter et) ;
    void updateBatch(List<Letter> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Letter get(String key) ;
    Letter getDraft(Letter et) ;
    boolean checkKey(Letter et) ;
    boolean save(Letter et) ;
    void saveBatch(List<Letter> list) ;
    Page<Letter> searchDefault(LetterSearchContext context) ;
    List<Letter> selectByServiceid(String serviceid) ;
    void removeByServiceid(String serviceid) ;
    List<Letter> selectBySlaid(String slaid) ;
    void removeBySlaid(String slaid) ;
    List<Letter> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Letter> getLetterByIds(List<String> ids) ;
    List<Letter> getLetterByEntities(List<Letter> entities) ;
}



package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.SiteMap;
import cn.ibizlab.businesscentral.core.base.filter.SiteMapSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[SiteMap] 服务对象接口
 */
public interface ISiteMapService extends IService<SiteMap>{

    boolean create(SiteMap et) ;
    void createBatch(List<SiteMap> list) ;
    boolean update(SiteMap et) ;
    void updateBatch(List<SiteMap> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    SiteMap get(String key) ;
    SiteMap getDraft(SiteMap et) ;
    boolean checkKey(SiteMap et) ;
    boolean save(SiteMap et) ;
    void saveBatch(List<SiteMap> list) ;
    Page<SiteMap> searchDefault(SiteMapSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<SiteMap> getSitemapByIds(List<String> ids) ;
    List<SiteMap> getSitemapByEntities(List<SiteMap> entities) ;
}



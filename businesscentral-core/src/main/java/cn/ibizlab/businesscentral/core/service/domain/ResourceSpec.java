package cn.ibizlab.businesscentral.core.service.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[资源规格]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "RESOURCESPEC",resultMap = "ResourceSpecResultMap")
public class ResourceSpec extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Import Sequence Number
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * 特殊资源名称
     */
    @TableField(value = "resourcespecname")
    @JSONField(name = "resourcespecname")
    @JsonProperty("resourcespecname")
    private String resourcespecname;
    /**
     * Time Zone Rule Version Number
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * 对象类型
     */
    @TableField(value = "objecttypecode")
    @JSONField(name = "objecttypecode")
    @JsonProperty("objecttypecode")
    private String objecttypecode;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * UTC Conversion Time Zone Code
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 目标表达式
     */
    @TableField(value = "objectiveexpression")
    @JSONField(name = "objectiveexpression")
    @JsonProperty("objectiveexpression")
    private String objectiveexpression;
    /**
     * Record Created On
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 资源规格
     */
    @DEField(isKeyField=true)
    @TableId(value= "resourcespecid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "resourcespecid")
    @JsonProperty("resourcespecid")
    private String resourcespecid;
    /**
     * Version Number
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 所需数目
     */
    @TableField(value = "requiredcount")
    @JSONField(name = "requiredcount")
    @JsonProperty("requiredcount")
    private Integer requiredcount;
    /**
     * 组对象
     */
    @TableField(value = "groupobjectid")
    @JSONField(name = "groupobjectid")
    @JsonProperty("groupobjectid")
    private String groupobjectid;
    /**
     * 相同场所
     */
    @DEField(defaultValue = "0")
    @TableField(value = "samesite")
    @JSONField(name = "samesite")
    @JsonProperty("samesite")
    private Integer samesite;
    /**
     * 约束
     */
    @TableField(value = "constraints")
    @JSONField(name = "constraints")
    @JsonProperty("constraints")
    private String constraints;
    /**
     * 所需付出的精力
     */
    @TableField(value = "effortrequired")
    @JSONField(name = "effortrequired")
    @JsonProperty("effortrequired")
    private Double effortrequired;
    /**
     * 业务部门
     */
    @TableField(value = "businessunitid")
    @JSONField(name = "businessunitid")
    @JsonProperty("businessunitid")
    private String businessunitid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.BusinessUnit businessunit;



    /**
     * 设置 [Import Sequence Number]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [特殊资源名称]
     */
    public void setResourcespecname(String resourcespecname){
        this.resourcespecname = resourcespecname ;
        this.modify("resourcespecname",resourcespecname);
    }

    /**
     * 设置 [Time Zone Rule Version Number]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [对象类型]
     */
    public void setObjecttypecode(String objecttypecode){
        this.objecttypecode = objecttypecode ;
        this.modify("objecttypecode",objecttypecode);
    }

    /**
     * 设置 [UTC Conversion Time Zone Code]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [目标表达式]
     */
    public void setObjectiveexpression(String objectiveexpression){
        this.objectiveexpression = objectiveexpression ;
        this.modify("objectiveexpression",objectiveexpression);
    }

    /**
     * 设置 [Record Created On]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [Record Created On]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [Version Number]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [所需数目]
     */
    public void setRequiredcount(Integer requiredcount){
        this.requiredcount = requiredcount ;
        this.modify("requiredcount",requiredcount);
    }

    /**
     * 设置 [组对象]
     */
    public void setGroupobjectid(String groupobjectid){
        this.groupobjectid = groupobjectid ;
        this.modify("groupobjectid",groupobjectid);
    }

    /**
     * 设置 [相同场所]
     */
    public void setSamesite(Integer samesite){
        this.samesite = samesite ;
        this.modify("samesite",samesite);
    }

    /**
     * 设置 [约束]
     */
    public void setConstraints(String constraints){
        this.constraints = constraints ;
        this.modify("constraints",constraints);
    }

    /**
     * 设置 [所需付出的精力]
     */
    public void setEffortrequired(Double effortrequired){
        this.effortrequired = effortrequired ;
        this.modify("effortrequired",effortrequired);
    }

    /**
     * 设置 [业务部门]
     */
    public void setBusinessunitid(String businessunitid){
        this.businessunitid = businessunitid ;
        this.modify("businessunitid",businessunitid);
    }


}



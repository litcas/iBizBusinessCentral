package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.OMHierarchy;
import cn.ibizlab.businesscentral.core.base.filter.OMHierarchySearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[OMHierarchy] 服务对象接口
 */
public interface IOMHierarchyService extends IService<OMHierarchy>{

    boolean create(OMHierarchy et) ;
    void createBatch(List<OMHierarchy> list) ;
    boolean update(OMHierarchy et) ;
    void updateBatch(List<OMHierarchy> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    OMHierarchy get(String key) ;
    OMHierarchy getDraft(OMHierarchy et) ;
    boolean checkKey(OMHierarchy et) ;
    boolean save(OMHierarchy et) ;
    void saveBatch(List<OMHierarchy> list) ;
    Page<OMHierarchy> searchDefault(OMHierarchySearchContext context) ;
    Page<OMHierarchy> searchRootOrg(OMHierarchySearchContext context) ;
    List<OMHierarchy> selectByOmhierarchycatid(String omhierarchycatid) ;
    void removeByOmhierarchycatid(String omhierarchycatid) ;
    List<OMHierarchy> selectByPomhierarchyid(String omhierarchyid) ;
    void removeByPomhierarchyid(String omhierarchyid) ;
    List<OMHierarchy> selectByOrganizationid(String organizationid) ;
    void removeByOrganizationid(String organizationid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<OMHierarchy> getOmhierarchyByIds(List<String> ids) ;
    List<OMHierarchy> getOmhierarchyByEntities(List<OMHierarchy> entities) ;
}



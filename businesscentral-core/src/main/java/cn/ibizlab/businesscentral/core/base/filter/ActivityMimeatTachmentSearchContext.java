package cn.ibizlab.businesscentral.core.base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.base.domain.ActivityMimeatTachment;
/**
 * 关系型数据实体[ActivityMimeatTachment] 查询条件对象
 */
@Slf4j
@Data
public class ActivityMimeatTachmentSearchContext extends QueryWrapperContext<ActivityMimeatTachment> {

	private String n_filename_like;//[FileName]
	public void setN_filename_like(String n_filename_like) {
        this.n_filename_like = n_filename_like;
        if(!ObjectUtils.isEmpty(this.n_filename_like)){
            this.getSearchCond().like("filename", n_filename_like);
        }
    }
	private String n_componentstate_eq;//[ComponentState]
	public void setN_componentstate_eq(String n_componentstate_eq) {
        this.n_componentstate_eq = n_componentstate_eq;
        if(!ObjectUtils.isEmpty(this.n_componentstate_eq)){
            this.getSearchCond().eq("componentstate", n_componentstate_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("filename", query)   
            );
		 }
	}
}




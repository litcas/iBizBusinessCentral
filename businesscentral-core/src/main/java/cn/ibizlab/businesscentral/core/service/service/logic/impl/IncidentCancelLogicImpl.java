package cn.ibizlab.businesscentral.core.service.service.logic.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;

import cn.ibizlab.businesscentral.core.service.service.logic.IIncidentCancelLogic;
import cn.ibizlab.businesscentral.core.service.domain.Incident;

/**
 * 关系型数据实体[Cancel] 对象
 */
@Slf4j
@Service
public class IncidentCancelLogicImpl implements IIncidentCancelLogic{

    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private cn.ibizlab.businesscentral.core.service.service.IIncidentService incidentservice;

    public cn.ibizlab.businesscentral.core.service.service.IIncidentService getIncidentService() {
        return this.incidentservice;
    }


    @Autowired
    private cn.ibizlab.businesscentral.core.service.service.IIncidentService iBzSysDefaultService;

    public cn.ibizlab.businesscentral.core.service.service.IIncidentService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    public void execute(Incident et){

          KieSession kieSession = null;
        try{
           kieSession=kieContainer.newKieSession();
           kieSession.insert(et); 
           kieSession.setGlobal("incidentcanceldefault",et);
           kieSession.setGlobal("incidentservice",incidentservice);
           kieSession.setGlobal("iBzSysIncidentDefaultService",iBzSysDefaultService);
           kieSession.setGlobal("curuser", cn.ibizlab.businesscentral.util.security.AuthenticationUser.getAuthenticationUser());
           kieSession.startProcess("cn.ibizlab.businesscentral.core.service.service.logic.incidentcancel");

        }catch(Exception e){
            throw new RuntimeException("执行[取消案例]处理逻辑发生异常"+e);
        }finally {
            if(kieSession!=null)
            kieSession.destroy();
        }
    }

}

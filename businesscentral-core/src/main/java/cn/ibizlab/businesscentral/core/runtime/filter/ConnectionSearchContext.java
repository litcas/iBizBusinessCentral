package cn.ibizlab.businesscentral.core.runtime.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.runtime.domain.Connection;
/**
 * 关系型数据实体[Connection] 查询条件对象
 */
@Slf4j
@Data
public class ConnectionSearchContext extends QueryWrapperContext<Connection> {

	private String n_record2objecttypecode_eq;//[类型(目标)]
	public void setN_record2objecttypecode_eq(String n_record2objecttypecode_eq) {
        this.n_record2objecttypecode_eq = n_record2objecttypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_record2objecttypecode_eq)){
            this.getSearchCond().eq("record2objecttypecode", n_record2objecttypecode_eq);
        }
    }
	private String n_record1objecttypecode_eq;//[类型(源)]
	public void setN_record1objecttypecode_eq(String n_record1objecttypecode_eq) {
        this.n_record1objecttypecode_eq = n_record1objecttypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_record1objecttypecode_eq)){
            this.getSearchCond().eq("record1objecttypecode", n_record1objecttypecode_eq);
        }
    }
	private String n_connectionname_like;//[关联名称]
	public void setN_connectionname_like(String n_connectionname_like) {
        this.n_connectionname_like = n_connectionname_like;
        if(!ObjectUtils.isEmpty(this.n_connectionname_like)){
            this.getSearchCond().like("connectionname", n_connectionname_like);
        }
    }
	private Integer n_statuscode_eq;//[状态描述]
	public void setN_statuscode_eq(Integer n_statuscode_eq) {
        this.n_statuscode_eq = n_statuscode_eq;
        if(!ObjectUtils.isEmpty(this.n_statuscode_eq)){
            this.getSearchCond().eq("statuscode", n_statuscode_eq);
        }
    }
	private Integer n_statecode_eq;//[状态]
	public void setN_statecode_eq(Integer n_statecode_eq) {
        this.n_statecode_eq = n_statecode_eq;
        if(!ObjectUtils.isEmpty(this.n_statecode_eq)){
            this.getSearchCond().eq("statecode", n_statecode_eq);
        }
    }
	private String n_record1rolename_eq;//[角色]
	public void setN_record1rolename_eq(String n_record1rolename_eq) {
        this.n_record1rolename_eq = n_record1rolename_eq;
        if(!ObjectUtils.isEmpty(this.n_record1rolename_eq)){
            this.getSearchCond().eq("record1rolename", n_record1rolename_eq);
        }
    }
	private String n_record1rolename_like;//[角色]
	public void setN_record1rolename_like(String n_record1rolename_like) {
        this.n_record1rolename_like = n_record1rolename_like;
        if(!ObjectUtils.isEmpty(this.n_record1rolename_like)){
            this.getSearchCond().like("record1rolename", n_record1rolename_like);
        }
    }
	private String n_record2rolename_eq;//[角色（目标）]
	public void setN_record2rolename_eq(String n_record2rolename_eq) {
        this.n_record2rolename_eq = n_record2rolename_eq;
        if(!ObjectUtils.isEmpty(this.n_record2rolename_eq)){
            this.getSearchCond().eq("record2rolename", n_record2rolename_eq);
        }
    }
	private String n_record2rolename_like;//[角色（目标）]
	public void setN_record2rolename_like(String n_record2rolename_like) {
        this.n_record2rolename_like = n_record2rolename_like;
        if(!ObjectUtils.isEmpty(this.n_record2rolename_like)){
            this.getSearchCond().like("record2rolename", n_record2rolename_like);
        }
    }
	private String n_record1roleid_eq;//[角色(源)]
	public void setN_record1roleid_eq(String n_record1roleid_eq) {
        this.n_record1roleid_eq = n_record1roleid_eq;
        if(!ObjectUtils.isEmpty(this.n_record1roleid_eq)){
            this.getSearchCond().eq("record1roleid", n_record1roleid_eq);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }
	private String n_record2roleid_eq;//[角色(目标)]
	public void setN_record2roleid_eq(String n_record2roleid_eq) {
        this.n_record2roleid_eq = n_record2roleid_eq;
        if(!ObjectUtils.isEmpty(this.n_record2roleid_eq)){
            this.getSearchCond().eq("record2roleid", n_record2roleid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("connectionname", query)   
            );
		 }
	}
}




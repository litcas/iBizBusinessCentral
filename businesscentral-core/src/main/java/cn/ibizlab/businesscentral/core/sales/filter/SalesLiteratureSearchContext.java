package cn.ibizlab.businesscentral.core.sales.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.sales.domain.SalesLiterature;
/**
 * 关系型数据实体[SalesLiterature] 查询条件对象
 */
@Slf4j
@Data
public class SalesLiteratureSearchContext extends QueryWrapperContext<SalesLiterature> {

	private String n_salesliteraturename_like;//[销售资料名称]
	public void setN_salesliteraturename_like(String n_salesliteraturename_like) {
        this.n_salesliteraturename_like = n_salesliteraturename_like;
        if(!ObjectUtils.isEmpty(this.n_salesliteraturename_like)){
            this.getSearchCond().like("salesliteraturename", n_salesliteraturename_like);
        }
    }
	private String n_literaturetypecode_eq;//[类型]
	public void setN_literaturetypecode_eq(String n_literaturetypecode_eq) {
        this.n_literaturetypecode_eq = n_literaturetypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_literaturetypecode_eq)){
            this.getSearchCond().eq("literaturetypecode", n_literaturetypecode_eq);
        }
    }
	private String n_currencyname_eq;//[货币]
	public void setN_currencyname_eq(String n_currencyname_eq) {
        this.n_currencyname_eq = n_currencyname_eq;
        if(!ObjectUtils.isEmpty(this.n_currencyname_eq)){
            this.getSearchCond().eq("currencyname", n_currencyname_eq);
        }
    }
	private String n_currencyname_like;//[货币]
	public void setN_currencyname_like(String n_currencyname_like) {
        this.n_currencyname_like = n_currencyname_like;
        if(!ObjectUtils.isEmpty(this.n_currencyname_like)){
            this.getSearchCond().like("currencyname", n_currencyname_like);
        }
    }
	private String n_subjectname_eq;//[主题]
	public void setN_subjectname_eq(String n_subjectname_eq) {
        this.n_subjectname_eq = n_subjectname_eq;
        if(!ObjectUtils.isEmpty(this.n_subjectname_eq)){
            this.getSearchCond().eq("subjectname", n_subjectname_eq);
        }
    }
	private String n_subjectname_like;//[主题]
	public void setN_subjectname_like(String n_subjectname_like) {
        this.n_subjectname_like = n_subjectname_like;
        if(!ObjectUtils.isEmpty(this.n_subjectname_like)){
            this.getSearchCond().like("subjectname", n_subjectname_like);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }
	private String n_subjectid_eq;//[主题]
	public void setN_subjectid_eq(String n_subjectid_eq) {
        this.n_subjectid_eq = n_subjectid_eq;
        if(!ObjectUtils.isEmpty(this.n_subjectid_eq)){
            this.getSearchCond().eq("subjectid", n_subjectid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("salesliteraturename", query)   
            );
		 }
	}
}




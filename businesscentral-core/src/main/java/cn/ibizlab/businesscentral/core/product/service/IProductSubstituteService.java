package cn.ibizlab.businesscentral.core.product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.product.domain.ProductSubstitute;
import cn.ibizlab.businesscentral.core.product.filter.ProductSubstituteSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[ProductSubstitute] 服务对象接口
 */
public interface IProductSubstituteService extends IService<ProductSubstitute>{

    boolean create(ProductSubstitute et) ;
    void createBatch(List<ProductSubstitute> list) ;
    boolean update(ProductSubstitute et) ;
    void updateBatch(List<ProductSubstitute> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    ProductSubstitute get(String key) ;
    ProductSubstitute getDraft(ProductSubstitute et) ;
    boolean checkKey(ProductSubstitute et) ;
    boolean save(ProductSubstitute et) ;
    void saveBatch(List<ProductSubstitute> list) ;
    Page<ProductSubstitute> searchDefault(ProductSubstituteSearchContext context) ;
    List<ProductSubstitute> selectByProductid(String productid) ;
    void removeByProductid(String productid) ;
    List<ProductSubstitute> selectBySubstitutedproductid(String productid) ;
    void removeBySubstitutedproductid(String productid) ;
    List<ProductSubstitute> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<ProductSubstitute> getProductsubstituteByIds(List<String> ids) ;
    List<ProductSubstitute> getProductsubstituteByEntities(List<ProductSubstitute> entities) ;
}



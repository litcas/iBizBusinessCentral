package cn.ibizlab.businesscentral.core.product.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.product.domain.Product;
/**
 * 关系型数据实体[Product] 查询条件对象
 */
@Slf4j
@Data
public class ProductSearchContext extends QueryWrapperContext<Product> {

	private String n_productstructure_eq;//[产品结构]
	public void setN_productstructure_eq(String n_productstructure_eq) {
        this.n_productstructure_eq = n_productstructure_eq;
        if(!ObjectUtils.isEmpty(this.n_productstructure_eq)){
            this.getSearchCond().eq("productstructure", n_productstructure_eq);
        }
    }
	private Integer n_statuscode_eq;//[状态描述]
	public void setN_statuscode_eq(Integer n_statuscode_eq) {
        this.n_statuscode_eq = n_statuscode_eq;
        if(!ObjectUtils.isEmpty(this.n_statuscode_eq)){
            this.getSearchCond().eq("statuscode", n_statuscode_eq);
        }
    }
	private String n_producttypecode_eq;//[产品类型]
	public void setN_producttypecode_eq(String n_producttypecode_eq) {
        this.n_producttypecode_eq = n_producttypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_producttypecode_eq)){
            this.getSearchCond().eq("producttypecode", n_producttypecode_eq);
        }
    }
	private String n_productname_like;//[产品名称]
	public void setN_productname_like(String n_productname_like) {
        this.n_productname_like = n_productname_like;
        if(!ObjectUtils.isEmpty(this.n_productname_like)){
            this.getSearchCond().like("productname", n_productname_like);
        }
    }
	private Integer n_statecode_eq;//[状态]
	public void setN_statecode_eq(Integer n_statecode_eq) {
        this.n_statecode_eq = n_statecode_eq;
        if(!ObjectUtils.isEmpty(this.n_statecode_eq)){
            this.getSearchCond().eq("statecode", n_statecode_eq);
        }
    }
	private String n_defaultuomschedulename_eq;//[计价单位组]
	public void setN_defaultuomschedulename_eq(String n_defaultuomschedulename_eq) {
        this.n_defaultuomschedulename_eq = n_defaultuomschedulename_eq;
        if(!ObjectUtils.isEmpty(this.n_defaultuomschedulename_eq)){
            this.getSearchCond().eq("defaultuomschedulename", n_defaultuomschedulename_eq);
        }
    }
	private String n_defaultuomschedulename_like;//[计价单位组]
	public void setN_defaultuomschedulename_like(String n_defaultuomschedulename_like) {
        this.n_defaultuomschedulename_like = n_defaultuomschedulename_like;
        if(!ObjectUtils.isEmpty(this.n_defaultuomschedulename_like)){
            this.getSearchCond().like("defaultuomschedulename", n_defaultuomschedulename_like);
        }
    }
	private String n_subjectname_eq;//[主题]
	public void setN_subjectname_eq(String n_subjectname_eq) {
        this.n_subjectname_eq = n_subjectname_eq;
        if(!ObjectUtils.isEmpty(this.n_subjectname_eq)){
            this.getSearchCond().eq("subjectname", n_subjectname_eq);
        }
    }
	private String n_subjectname_like;//[主题]
	public void setN_subjectname_like(String n_subjectname_like) {
        this.n_subjectname_like = n_subjectname_like;
        if(!ObjectUtils.isEmpty(this.n_subjectname_like)){
            this.getSearchCond().like("subjectname", n_subjectname_like);
        }
    }
	private String n_currencyname_eq;//[货币]
	public void setN_currencyname_eq(String n_currencyname_eq) {
        this.n_currencyname_eq = n_currencyname_eq;
        if(!ObjectUtils.isEmpty(this.n_currencyname_eq)){
            this.getSearchCond().eq("currencyname", n_currencyname_eq);
        }
    }
	private String n_currencyname_like;//[货币]
	public void setN_currencyname_like(String n_currencyname_like) {
        this.n_currencyname_like = n_currencyname_like;
        if(!ObjectUtils.isEmpty(this.n_currencyname_like)){
            this.getSearchCond().like("currencyname", n_currencyname_like);
        }
    }
	private String n_pricelevelname_eq;//[默认价目表]
	public void setN_pricelevelname_eq(String n_pricelevelname_eq) {
        this.n_pricelevelname_eq = n_pricelevelname_eq;
        if(!ObjectUtils.isEmpty(this.n_pricelevelname_eq)){
            this.getSearchCond().eq("pricelevelname", n_pricelevelname_eq);
        }
    }
	private String n_pricelevelname_like;//[默认价目表]
	public void setN_pricelevelname_like(String n_pricelevelname_like) {
        this.n_pricelevelname_like = n_pricelevelname_like;
        if(!ObjectUtils.isEmpty(this.n_pricelevelname_like)){
            this.getSearchCond().like("pricelevelname", n_pricelevelname_like);
        }
    }
	private String n_defaultuomname_eq;//[默认计价单位]
	public void setN_defaultuomname_eq(String n_defaultuomname_eq) {
        this.n_defaultuomname_eq = n_defaultuomname_eq;
        if(!ObjectUtils.isEmpty(this.n_defaultuomname_eq)){
            this.getSearchCond().eq("defaultuomname", n_defaultuomname_eq);
        }
    }
	private String n_defaultuomname_like;//[默认计价单位]
	public void setN_defaultuomname_like(String n_defaultuomname_like) {
        this.n_defaultuomname_like = n_defaultuomname_like;
        if(!ObjectUtils.isEmpty(this.n_defaultuomname_like)){
            this.getSearchCond().like("defaultuomname", n_defaultuomname_like);
        }
    }
	private String n_parentproductname_eq;//[父级]
	public void setN_parentproductname_eq(String n_parentproductname_eq) {
        this.n_parentproductname_eq = n_parentproductname_eq;
        if(!ObjectUtils.isEmpty(this.n_parentproductname_eq)){
            this.getSearchCond().eq("parentproductname", n_parentproductname_eq);
        }
    }
	private String n_parentproductname_like;//[父级]
	public void setN_parentproductname_like(String n_parentproductname_like) {
        this.n_parentproductname_like = n_parentproductname_like;
        if(!ObjectUtils.isEmpty(this.n_parentproductname_like)){
            this.getSearchCond().like("parentproductname", n_parentproductname_like);
        }
    }
	private String n_parentproductid_eq;//[父级]
	public void setN_parentproductid_eq(String n_parentproductid_eq) {
        this.n_parentproductid_eq = n_parentproductid_eq;
        if(!ObjectUtils.isEmpty(this.n_parentproductid_eq)){
            this.getSearchCond().eq("parentproductid", n_parentproductid_eq);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }
	private String n_pricelevelid_eq;//[默认价目表]
	public void setN_pricelevelid_eq(String n_pricelevelid_eq) {
        this.n_pricelevelid_eq = n_pricelevelid_eq;
        if(!ObjectUtils.isEmpty(this.n_pricelevelid_eq)){
            this.getSearchCond().eq("pricelevelid", n_pricelevelid_eq);
        }
    }
	private String n_defaultuomid_eq;//[默认计价单位]
	public void setN_defaultuomid_eq(String n_defaultuomid_eq) {
        this.n_defaultuomid_eq = n_defaultuomid_eq;
        if(!ObjectUtils.isEmpty(this.n_defaultuomid_eq)){
            this.getSearchCond().eq("defaultuomid", n_defaultuomid_eq);
        }
    }
	private String n_defaultuomscheduleid_eq;//[计价单位组]
	public void setN_defaultuomscheduleid_eq(String n_defaultuomscheduleid_eq) {
        this.n_defaultuomscheduleid_eq = n_defaultuomscheduleid_eq;
        if(!ObjectUtils.isEmpty(this.n_defaultuomscheduleid_eq)){
            this.getSearchCond().eq("defaultuomscheduleid", n_defaultuomscheduleid_eq);
        }
    }
	private String n_subjectid_eq;//[主题]
	public void setN_subjectid_eq(String n_subjectid_eq) {
        this.n_subjectid_eq = n_subjectid_eq;
        if(!ObjectUtils.isEmpty(this.n_subjectid_eq)){
            this.getSearchCond().eq("subjectid", n_subjectid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("productname", query)   
            );
		 }
	}
}




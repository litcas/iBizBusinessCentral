package cn.ibizlab.businesscentral.core.humanresource.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.humanresource.domain.Attendance;
import cn.ibizlab.businesscentral.core.humanresource.filter.AttendanceSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Attendance] 服务对象接口
 */
public interface IAttendanceService extends IService<Attendance>{

    boolean create(Attendance et) ;
    void createBatch(List<Attendance> list) ;
    boolean update(Attendance et) ;
    void updateBatch(List<Attendance> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Attendance get(String key) ;
    Attendance getDraft(Attendance et) ;
    boolean checkKey(Attendance et) ;
    boolean save(Attendance et) ;
    void saveBatch(List<Attendance> list) ;
    Page<Attendance> searchDefault(AttendanceSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Attendance> getAttendanceByIds(List<String> ids) ;
    List<Attendance> getAttendanceByEntities(List<Attendance> entities) ;
}



package cn.ibizlab.businesscentral.core.base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.base.domain.Sla;
import cn.ibizlab.businesscentral.core.base.filter.SlaSearchContext;
import cn.ibizlab.businesscentral.core.base.service.ISlaService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.base.mapper.SlaMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[SLA] 服务对象接口实现
 */
@Slf4j
@Service("SlaServiceImpl")
public class SlaServiceImpl extends ServiceImpl<SlaMapper, Sla> implements ISlaService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IAccountService accountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IActivityPointerService activitypointerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IAppointmentService appointmentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.runtime.service.IBulkOperationService bulkoperationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.marketing.service.ICampaignActivityService campaignactivityService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.marketing.service.ICampaignResponseService campaignresponseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IContactService contactService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IEmailService emailService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IEntitlementTemplateService entitlementtemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IEntitlementService entitlementService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IFaxService faxService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IIncidentreSolutionService incidentresolutionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IIncidentService incidentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.finance.service.IInvoiceService invoiceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.ILeadService leadService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ILetterService letterService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IOpportunityCloseService opportunitycloseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IOpportunityService opportunityService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IOrderCloseService ordercloseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IPhoneCallService phonecallService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IQuoteCloseService quotecloseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IQuoteService quoteService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.ISalesOrderService salesorderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IServiceAppointmentService serviceappointmentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ISlaItemService slaitemService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITaskService taskService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IBusinessUnitService businessunitService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ICalendarService calendarService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITeamService teamService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(Sla et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getSlaid()),et);
        return true;
    }

    @Override
    public void createBatch(List<Sla> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Sla et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("slaid",et.getSlaid())))
            return false;
        CachedBeanCopier.copy(get(et.getSlaid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<Sla> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Sla get(String key) {
        Sla et = getById(key);
        if(et==null){
            et=new Sla();
            et.setSlaid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Sla getDraft(Sla et) {
        return et;
    }

    @Override
    public boolean checkKey(Sla et) {
        return (!ObjectUtils.isEmpty(et.getSlaid()))&&(!Objects.isNull(this.getById(et.getSlaid())));
    }
    @Override
    @Transactional
    public boolean save(Sla et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Sla et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<Sla> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<Sla> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Sla> selectByOwningbusinessunit(String businessunitid) {
        return baseMapper.selectByOwningbusinessunit(businessunitid);
    }

    @Override
    public void removeByOwningbusinessunit(String businessunitid) {
        this.remove(new QueryWrapper<Sla>().eq("owningbusinessunit",businessunitid));
    }

	@Override
    public List<Sla> selectByBusinesshoursid(String calendarid) {
        return baseMapper.selectByBusinesshoursid(calendarid);
    }

    @Override
    public void removeByBusinesshoursid(String calendarid) {
        this.remove(new QueryWrapper<Sla>().eq("businesshoursid",calendarid));
    }

	@Override
    public List<Sla> selectByOwningteam(String teamid) {
        return baseMapper.selectByOwningteam(teamid);
    }

    @Override
    public void removeByOwningteam(String teamid) {
        this.remove(new QueryWrapper<Sla>().eq("owningteam",teamid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<Sla> searchDefault(SlaSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Sla> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Sla>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Sla> getSlaByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Sla> getSlaByEntities(List<Sla> entities) {
        List ids =new ArrayList();
        for(Sla entity : entities){
            Serializable id=entity.getSlaid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




package cn.ibizlab.businesscentral.core.marketing.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.marketing.domain.Campaign;
import cn.ibizlab.businesscentral.core.marketing.filter.CampaignSearchContext;
import cn.ibizlab.businesscentral.core.marketing.service.ICampaignService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.marketing.mapper.CampaignMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[市场活动] 服务对象接口实现
 */
@Slf4j
@Service("CampaignServiceImpl")
public class CampaignServiceImpl extends ServiceImpl<CampaignMapper, Campaign> implements ICampaignService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.marketing.service.ICampaignCampaignService campaigncampaignService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.marketing.service.ICampaignListService campaignlistService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.ILeadService leadService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IOpportunityService opportunityService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IQuoteService quoteService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.ISalesOrderService salesorderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.product.service.IPriceLevelService pricelevelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.marketing.service.logic.ICampaignActiveLogic activeLogic;

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.marketing.service.logic.ICampaignStopLogic stopLogic;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(Campaign et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getCampaignid()),et);
        return true;
    }

    @Override
    public void createBatch(List<Campaign> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Campaign et) {
        fillParentData(et);
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("campaignid",et.getCampaignid())))
            return false;
        CachedBeanCopier.copy(get(et.getCampaignid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<Campaign> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Campaign get(String key) {
        Campaign et = getById(key);
        if(et==null){
            et=new Campaign();
            et.setCampaignid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Campaign getDraft(Campaign et) {
        fillParentData(et);
        return et;
    }

    @Override
    @Transactional
    public Campaign active(Campaign et) {
        activeLogic.execute(et);
         return et ;
    }

    @Override
    public boolean checkKey(Campaign et) {
        return (!ObjectUtils.isEmpty(et.getCampaignid()))&&(!Objects.isNull(this.getById(et.getCampaignid())));
    }
    @Override
    @Transactional
    public boolean save(Campaign et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Campaign et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<Campaign> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<Campaign> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }

    @Override
    @Transactional
    public Campaign stop(Campaign et) {
        stopLogic.execute(et);
         return et ;
    }


	@Override
    public List<Campaign> selectByPricelistid(String pricelevelid) {
        return baseMapper.selectByPricelistid(pricelevelid);
    }

    @Override
    public void removeByPricelistid(String pricelevelid) {
        this.remove(new QueryWrapper<Campaign>().eq("pricelistid",pricelevelid));
    }

	@Override
    public List<Campaign> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<Campaign>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<Campaign> searchDefault(CampaignSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Campaign> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Campaign>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 有效
     */
    @Override
    public Page<Campaign> searchEffective(CampaignSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Campaign> pages=baseMapper.searchEffective(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Campaign>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 停用
     */
    @Override
    public Page<Campaign> searchStop(CampaignSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Campaign> pages=baseMapper.searchStop(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Campaign>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Campaign et){
        //实体关系[DER1N_CAMPAIGN__TRANSACTIONCURRENCY__TRANSACTIONCURRENCYID]
        if(!ObjectUtils.isEmpty(et.getTransactioncurrencyid())){
            cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency=et.getTransactioncurrency();
            if(ObjectUtils.isEmpty(transactioncurrency)){
                cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency majorEntity=transactioncurrencyService.get(et.getTransactioncurrencyid());
                et.setTransactioncurrency(majorEntity);
                transactioncurrency=majorEntity;
            }
            et.setCurrencyname(transactioncurrency.getCurrencyname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Campaign> getCampaignByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Campaign> getCampaignByEntities(List<Campaign> entities) {
        List ids =new ArrayList();
        for(Campaign entity : entities){
            Serializable id=entity.getCampaignid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




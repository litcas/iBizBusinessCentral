package cn.ibizlab.businesscentral.core.service.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.service.domain.EntitlementTemplate;
import cn.ibizlab.businesscentral.core.service.filter.EntitlementTemplateSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EntitlementTemplate] 服务对象接口
 */
public interface IEntitlementTemplateService extends IService<EntitlementTemplate>{

    boolean create(EntitlementTemplate et) ;
    void createBatch(List<EntitlementTemplate> list) ;
    boolean update(EntitlementTemplate et) ;
    void updateBatch(List<EntitlementTemplate> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    EntitlementTemplate get(String key) ;
    EntitlementTemplate getDraft(EntitlementTemplate et) ;
    boolean checkKey(EntitlementTemplate et) ;
    boolean save(EntitlementTemplate et) ;
    void saveBatch(List<EntitlementTemplate> list) ;
    Page<EntitlementTemplate> searchDefault(EntitlementTemplateSearchContext context) ;
    List<EntitlementTemplate> selectBySlaid(String slaid) ;
    void removeBySlaid(String slaid) ;
    List<EntitlementTemplate> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EntitlementTemplate> getEntitlementtemplateByIds(List<String> ids) ;
    List<EntitlementTemplate> getEntitlementtemplateByEntities(List<EntitlementTemplate> entities) ;
}



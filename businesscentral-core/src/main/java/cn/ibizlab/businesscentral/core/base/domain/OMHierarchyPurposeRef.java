package cn.ibizlab.businesscentral.core.base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[组织层次结构分配]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "OMHIERARCHYPURPOSEREF",resultMap = "OMHierarchyPurposeRefResultMap")
public class OMHierarchyPurposeRef extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 组织层次结构分配名称
     */
    @TableField(value = "omhierarchypurposerefname")
    @JSONField(name = "omhierarchypurposerefname")
    @JsonProperty("omhierarchypurposerefname")
    private String omhierarchypurposerefname;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 组织层次结构分配标识
     */
    @DEField(isKeyField=true)
    @TableId(value= "omhierarchypurposerefid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "omhierarchypurposerefid")
    @JsonProperty("omhierarchypurposerefid")
    private String omhierarchypurposerefid;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 结构层次类别标识
     */
    @TableField(value = "omhierarchycatid")
    @JSONField(name = "omhierarchycatid")
    @JsonProperty("omhierarchycatid")
    private String omhierarchycatid;
    /**
     * 组织层次机构目的标识
     */
    @TableField(value = "omhierarchypurposeid")
    @JSONField(name = "omhierarchypurposeid")
    @JsonProperty("omhierarchypurposeid")
    private String omhierarchypurposeid;
    /**
     * 是否默认
     */
    @TableField(value = "isdefault")
    @JSONField(name = "isdefault")
    @JsonProperty("isdefault")
    private Integer isdefault;
    /**
     * 结束日期
     */
    @TableField(value = "enddate")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "enddate" , format="yyyy-MM-dd")
    @JsonProperty("enddate")
    private Timestamp enddate;
    /**
     * 开始日期
     */
    @TableField(value = "begindate")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "begindate" , format="yyyy-MM-dd")
    @JsonProperty("begindate")
    private Timestamp begindate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.OMHierarchyCat omhierarchycat;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.OMHierarchyPurpose omhierarchypurpose;



    /**
     * 设置 [组织层次结构分配名称]
     */
    public void setOmhierarchypurposerefname(String omhierarchypurposerefname){
        this.omhierarchypurposerefname = omhierarchypurposerefname ;
        this.modify("omhierarchypurposerefname",omhierarchypurposerefname);
    }

    /**
     * 设置 [结构层次类别标识]
     */
    public void setOmhierarchycatid(String omhierarchycatid){
        this.omhierarchycatid = omhierarchycatid ;
        this.modify("omhierarchycatid",omhierarchycatid);
    }

    /**
     * 设置 [组织层次机构目的标识]
     */
    public void setOmhierarchypurposeid(String omhierarchypurposeid){
        this.omhierarchypurposeid = omhierarchypurposeid ;
        this.modify("omhierarchypurposeid",omhierarchypurposeid);
    }

    /**
     * 设置 [是否默认]
     */
    public void setIsdefault(Integer isdefault){
        this.isdefault = isdefault ;
        this.modify("isdefault",isdefault);
    }

    /**
     * 设置 [结束日期]
     */
    public void setEnddate(Timestamp enddate){
        this.enddate = enddate ;
        this.modify("enddate",enddate);
    }

    /**
     * 格式化日期 [结束日期]
     */
    public String formatEnddate(){
        if (this.enddate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(enddate);
    }
    /**
     * 设置 [开始日期]
     */
    public void setBegindate(Timestamp begindate){
        this.begindate = begindate ;
        this.modify("begindate",begindate);
    }

    /**
     * 格式化日期 [开始日期]
     */
    public String formatBegindate(){
        if (this.begindate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(begindate);
    }

}



package cn.ibizlab.businesscentral.core.base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.base.domain.LanguageLocale;
import cn.ibizlab.businesscentral.core.base.filter.LanguageLocaleSearchContext;
import cn.ibizlab.businesscentral.core.base.service.ILanguageLocaleService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.base.mapper.LanguageLocaleMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[语言] 服务对象接口实现
 */
@Slf4j
@Service("LanguageLocaleServiceImpl")
public class LanguageLocaleServiceImpl extends ServiceImpl<LanguageLocaleMapper, LanguageLocale> implements ILanguageLocaleService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IKnowledgeArticleService knowledgearticleService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(LanguageLocale et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getLanguagelocaleid()),et);
        return true;
    }

    @Override
    public void createBatch(List<LanguageLocale> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(LanguageLocale et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("languagelocaleid",et.getLanguagelocaleid())))
            return false;
        CachedBeanCopier.copy(get(et.getLanguagelocaleid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<LanguageLocale> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public LanguageLocale get(String key) {
        LanguageLocale et = getById(key);
        if(et==null){
            et=new LanguageLocale();
            et.setLanguagelocaleid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public LanguageLocale getDraft(LanguageLocale et) {
        return et;
    }

    @Override
    public boolean checkKey(LanguageLocale et) {
        return (!ObjectUtils.isEmpty(et.getLanguagelocaleid()))&&(!Objects.isNull(this.getById(et.getLanguagelocaleid())));
    }
    @Override
    @Transactional
    public boolean save(LanguageLocale et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(LanguageLocale et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<LanguageLocale> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<LanguageLocale> list) {
        saveOrUpdateBatch(list,batchSize);
    }



    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<LanguageLocale> searchDefault(LanguageLocaleSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<LanguageLocale> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<LanguageLocale>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<LanguageLocale> getLanguagelocaleByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<LanguageLocale> getLanguagelocaleByEntities(List<LanguageLocale> entities) {
        List ids =new ArrayList();
        for(LanguageLocale entity : entities){
            Serializable id=entity.getLanguagelocaleid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




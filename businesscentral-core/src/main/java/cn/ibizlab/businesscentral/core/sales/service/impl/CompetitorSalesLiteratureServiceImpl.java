package cn.ibizlab.businesscentral.core.sales.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.sales.domain.CompetitorSalesLiterature;
import cn.ibizlab.businesscentral.core.sales.filter.CompetitorSalesLiteratureSearchContext;
import cn.ibizlab.businesscentral.core.sales.service.ICompetitorSalesLiteratureService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.sales.mapper.CompetitorSalesLiteratureMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[竞争对手宣传资料] 服务对象接口实现
 */
@Slf4j
@Service("CompetitorSalesLiteratureServiceImpl")
public class CompetitorSalesLiteratureServiceImpl extends ServiceImpl<CompetitorSalesLiteratureMapper, CompetitorSalesLiterature> implements ICompetitorSalesLiteratureService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.ICompetitorService competitorService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.ISalesLiteratureService salesliteratureService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(CompetitorSalesLiterature et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getRelationshipsid()),et);
        return true;
    }

    @Override
    public void createBatch(List<CompetitorSalesLiterature> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(CompetitorSalesLiterature et) {
        fillParentData(et);
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("relationshipsid",et.getRelationshipsid())))
            return false;
        CachedBeanCopier.copy(get(et.getRelationshipsid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<CompetitorSalesLiterature> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public CompetitorSalesLiterature get(String key) {
        CompetitorSalesLiterature et = getById(key);
        if(et==null){
            et=new CompetitorSalesLiterature();
            et.setRelationshipsid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public CompetitorSalesLiterature getDraft(CompetitorSalesLiterature et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(CompetitorSalesLiterature et) {
        return (!ObjectUtils.isEmpty(et.getRelationshipsid()))&&(!Objects.isNull(this.getById(et.getRelationshipsid())));
    }
    @Override
    @Transactional
    public boolean save(CompetitorSalesLiterature et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(CompetitorSalesLiterature et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<CompetitorSalesLiterature> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<CompetitorSalesLiterature> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<CompetitorSalesLiterature> selectByEntityid(String competitorid) {
        return baseMapper.selectByEntityid(competitorid);
    }

    @Override
    public void removeByEntityid(String competitorid) {
        this.remove(new QueryWrapper<CompetitorSalesLiterature>().eq("entityid",competitorid));
    }

	@Override
    public List<CompetitorSalesLiterature> selectByEntity2id(String salesliteratureid) {
        return baseMapper.selectByEntity2id(salesliteratureid);
    }

    @Override
    public void removeByEntity2id(String salesliteratureid) {
        this.remove(new QueryWrapper<CompetitorSalesLiterature>().eq("entity2id",salesliteratureid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<CompetitorSalesLiterature> searchDefault(CompetitorSalesLiteratureSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<CompetitorSalesLiterature> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<CompetitorSalesLiterature>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(CompetitorSalesLiterature et){
        //实体关系[DER1N_COMPETITORSALESLITERATURE_COMPETITOR_ENTITYID]
        if(!ObjectUtils.isEmpty(et.getEntityid())){
            cn.ibizlab.businesscentral.core.sales.domain.Competitor competitor=et.getCompetitor();
            if(ObjectUtils.isEmpty(competitor)){
                cn.ibizlab.businesscentral.core.sales.domain.Competitor majorEntity=competitorService.get(et.getEntityid());
                et.setCompetitor(majorEntity);
                competitor=majorEntity;
            }
            et.setEntityname(competitor.getCompetitorname());
        }
        //实体关系[DER1N_COMPETITORSALESLITERATURE_SALESLITERATURE_ENTITY2ID]
        if(!ObjectUtils.isEmpty(et.getEntity2id())){
            cn.ibizlab.businesscentral.core.sales.domain.SalesLiterature salesliterature=et.getSalesliterature();
            if(ObjectUtils.isEmpty(salesliterature)){
                cn.ibizlab.businesscentral.core.sales.domain.SalesLiterature majorEntity=salesliteratureService.get(et.getEntity2id());
                et.setSalesliterature(majorEntity);
                salesliterature=majorEntity;
            }
            et.setEntity2name(salesliterature.getSalesliteraturename());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<CompetitorSalesLiterature> getCompetitorsalesliteratureByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<CompetitorSalesLiterature> getCompetitorsalesliteratureByEntities(List<CompetitorSalesLiterature> entities) {
        List ids =new ArrayList();
        for(CompetitorSalesLiterature entity : entities){
            Serializable id=entity.getRelationshipsid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




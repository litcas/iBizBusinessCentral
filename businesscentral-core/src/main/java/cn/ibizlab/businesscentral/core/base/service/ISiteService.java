package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.Site;
import cn.ibizlab.businesscentral.core.base.filter.SiteSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Site] 服务对象接口
 */
public interface ISiteService extends IService<Site>{

    boolean create(Site et) ;
    void createBatch(List<Site> list) ;
    boolean update(Site et) ;
    void updateBatch(List<Site> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Site get(String key) ;
    Site getDraft(Site et) ;
    boolean checkKey(Site et) ;
    boolean save(Site et) ;
    void saveBatch(List<Site> list) ;
    Page<Site> searchDefault(SiteSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Site> getSiteByIds(List<String> ids) ;
    List<Site> getSiteByEntities(List<Site> entities) ;
}



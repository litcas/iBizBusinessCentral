package cn.ibizlab.businesscentral.core.finance.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.finance.domain.QuarterlyFiscalCalendar;
import cn.ibizlab.businesscentral.core.finance.filter.QuarterlyFiscalCalendarSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[QuarterlyFiscalCalendar] 服务对象接口
 */
public interface IQuarterlyFiscalCalendarService extends IService<QuarterlyFiscalCalendar>{

    boolean create(QuarterlyFiscalCalendar et) ;
    void createBatch(List<QuarterlyFiscalCalendar> list) ;
    boolean update(QuarterlyFiscalCalendar et) ;
    void updateBatch(List<QuarterlyFiscalCalendar> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    QuarterlyFiscalCalendar get(String key) ;
    QuarterlyFiscalCalendar getDraft(QuarterlyFiscalCalendar et) ;
    boolean checkKey(QuarterlyFiscalCalendar et) ;
    boolean save(QuarterlyFiscalCalendar et) ;
    void saveBatch(List<QuarterlyFiscalCalendar> list) ;
    Page<QuarterlyFiscalCalendar> searchDefault(QuarterlyFiscalCalendarSearchContext context) ;
    List<QuarterlyFiscalCalendar> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<QuarterlyFiscalCalendar> getQuarterlyfiscalcalendarByIds(List<String> ids) ;
    List<QuarterlyFiscalCalendar> getQuarterlyfiscalcalendarByEntities(List<QuarterlyFiscalCalendar> entities) ;
}



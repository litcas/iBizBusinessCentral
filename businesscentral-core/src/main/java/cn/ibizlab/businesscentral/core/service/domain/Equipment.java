package cn.ibizlab.businesscentral.core.service.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[设施/设备]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "EQUIPMENT",resultMap = "EquipmentResultMap")
public class Equipment extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * UTC Conversion Time Zone Code
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 已禁用
     */
    @DEField(defaultValue = "0")
    @TableField(value = "disabled")
    @JSONField(name = "disabled")
    @JsonProperty("disabled")
    private Integer disabled;
    /**
     * Time Zone Rule Version Number
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * Version Number
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 在服务视图上显示
     */
    @DEField(defaultValue = "0")
    @TableField(value = "displayinserviceviews")
    @JSONField(name = "displayinserviceviews")
    @JsonProperty("displayinserviceviews")
    private Integer displayinserviceviews;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * Import Sequence Number
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * 技能
     */
    @TableField(value = "skills")
    @JSONField(name = "skills")
    @JsonProperty("skills")
    private String skills;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 主要电子邮件
     */
    @TableField(value = "emailaddress")
    @JSONField(name = "emailaddress")
    @JsonProperty("emailaddress")
    private String emailaddress;
    /**
     * 汇率
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * 设备名称
     */
    @TableField(value = "equipmentname")
    @JSONField(name = "equipmentname")
    @JsonProperty("equipmentname")
    private String equipmentname;
    /**
     * 时区
     */
    @TableField(value = "timezonecode")
    @JSONField(name = "timezonecode")
    @JsonProperty("timezonecode")
    private Integer timezonecode;
    /**
     * 设施/设备
     */
    @DEField(isKeyField=true)
    @TableId(value= "equipmentid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "equipmentid")
    @JsonProperty("equipmentid")
    private String equipmentid;
    /**
     * Record Created On
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 货币
     */
    @TableField(value = "transactioncurrencyid")
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;
    /**
     * Business Unit Id
     */
    @TableField(value = "businessunitid")
    @JSONField(name = "businessunitid")
    @JsonProperty("businessunitid")
    private String businessunitid;
    /**
     * 日历
     */
    @TableField(value = "calendarid")
    @JSONField(name = "calendarid")
    @JsonProperty("calendarid")
    private String calendarid;
    /**
     * 场所
     */
    @TableField(value = "siteid")
    @JSONField(name = "siteid")
    @JsonProperty("siteid")
    private String siteid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.BusinessUnit businessunit;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Calendar calendar;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Site site;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency;



    /**
     * 设置 [UTC Conversion Time Zone Code]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [已禁用]
     */
    public void setDisabled(Integer disabled){
        this.disabled = disabled ;
        this.modify("disabled",disabled);
    }

    /**
     * 设置 [Time Zone Rule Version Number]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [Version Number]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [在服务视图上显示]
     */
    public void setDisplayinserviceviews(Integer displayinserviceviews){
        this.displayinserviceviews = displayinserviceviews ;
        this.modify("displayinserviceviews",displayinserviceviews);
    }

    /**
     * 设置 [Import Sequence Number]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [技能]
     */
    public void setSkills(String skills){
        this.skills = skills ;
        this.modify("skills",skills);
    }

    /**
     * 设置 [主要电子邮件]
     */
    public void setEmailaddress(String emailaddress){
        this.emailaddress = emailaddress ;
        this.modify("emailaddress",emailaddress);
    }

    /**
     * 设置 [汇率]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [设备名称]
     */
    public void setEquipmentname(String equipmentname){
        this.equipmentname = equipmentname ;
        this.modify("equipmentname",equipmentname);
    }

    /**
     * 设置 [时区]
     */
    public void setTimezonecode(Integer timezonecode){
        this.timezonecode = timezonecode ;
        this.modify("timezonecode",timezonecode);
    }

    /**
     * 设置 [Record Created On]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [Record Created On]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [货币]
     */
    public void setTransactioncurrencyid(String transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }

    /**
     * 设置 [Business Unit Id]
     */
    public void setBusinessunitid(String businessunitid){
        this.businessunitid = businessunitid ;
        this.modify("businessunitid",businessunitid);
    }

    /**
     * 设置 [日历]
     */
    public void setCalendarid(String calendarid){
        this.calendarid = calendarid ;
        this.modify("calendarid",calendarid);
    }

    /**
     * 设置 [场所]
     */
    public void setSiteid(String siteid){
        this.siteid = siteid ;
        this.modify("siteid",siteid);
    }


}



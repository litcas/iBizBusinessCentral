package cn.ibizlab.businesscentral.core.service.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.service.domain.ServiceAppointment;
import cn.ibizlab.businesscentral.core.service.filter.ServiceAppointmentSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface ServiceAppointmentMapper extends BaseMapper<ServiceAppointment>{

    Page<ServiceAppointment> searchDefault(IPage page, @Param("srf") ServiceAppointmentSearchContext context, @Param("ew") Wrapper<ServiceAppointment> wrapper) ;
    @Override
    ServiceAppointment selectById(Serializable id);
    @Override
    int insert(ServiceAppointment entity);
    @Override
    int updateById(@Param(Constants.ENTITY) ServiceAppointment entity);
    @Override
    int update(@Param(Constants.ENTITY) ServiceAppointment entity, @Param("ew") Wrapper<ServiceAppointment> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<ServiceAppointment> selectByServiceid(@Param("serviceid") Serializable serviceid) ;

    List<ServiceAppointment> selectBySiteid(@Param("siteid") Serializable siteid) ;

    List<ServiceAppointment> selectBySlaid(@Param("slaid") Serializable slaid) ;

    List<ServiceAppointment> selectByTransactioncurrencyid(@Param("transactioncurrencyid") Serializable transactioncurrencyid) ;

}

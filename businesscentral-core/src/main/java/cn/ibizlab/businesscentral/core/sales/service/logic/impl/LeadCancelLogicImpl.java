package cn.ibizlab.businesscentral.core.sales.service.logic.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;

import cn.ibizlab.businesscentral.core.sales.service.logic.ILeadCancelLogic;
import cn.ibizlab.businesscentral.core.sales.domain.Lead;

/**
 * 关系型数据实体[Cancel] 对象
 */
@Slf4j
@Service
public class LeadCancelLogicImpl implements ILeadCancelLogic{

    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private cn.ibizlab.businesscentral.core.sales.service.ILeadService leadservice;

    public cn.ibizlab.businesscentral.core.sales.service.ILeadService getLeadService() {
        return this.leadservice;
    }


    @Autowired
    private cn.ibizlab.businesscentral.core.sales.service.ILeadService iBzSysDefaultService;

    public cn.ibizlab.businesscentral.core.sales.service.ILeadService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    public void execute(Lead et){

          KieSession kieSession = null;
        try{
           kieSession=kieContainer.newKieSession();
           kieSession.insert(et); 
           kieSession.setGlobal("leadcanceldefault",et);
           kieSession.setGlobal("leadservice",leadservice);
           kieSession.setGlobal("iBzSysLeadDefaultService",iBzSysDefaultService);
           kieSession.setGlobal("curuser", cn.ibizlab.businesscentral.util.security.AuthenticationUser.getAuthenticationUser());
           kieSession.startProcess("cn.ibizlab.businesscentral.core.sales.service.logic.leadcancel");

        }catch(Exception e){
            throw new RuntimeException("执行[取消]处理逻辑发生异常"+e);
        }finally {
            if(kieSession!=null)
            kieSession.destroy();
        }
    }

}

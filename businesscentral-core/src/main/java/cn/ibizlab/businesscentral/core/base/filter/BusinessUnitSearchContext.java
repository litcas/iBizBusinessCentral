package cn.ibizlab.businesscentral.core.base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.base.domain.BusinessUnit;
/**
 * 关系型数据实体[BusinessUnit] 查询条件对象
 */
@Slf4j
@Data
public class BusinessUnitSearchContext extends QueryWrapperContext<BusinessUnit> {

	private String n_address1_addresstypecode_eq;//[地址 1: 地址类型]
	public void setN_address1_addresstypecode_eq(String n_address1_addresstypecode_eq) {
        this.n_address1_addresstypecode_eq = n_address1_addresstypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_address1_addresstypecode_eq)){
            this.getSearchCond().eq("address1_addresstypecode", n_address1_addresstypecode_eq);
        }
    }
	private String n_address1_shippingmethodcode_eq;//[地址 1: 送货方式]
	public void setN_address1_shippingmethodcode_eq(String n_address1_shippingmethodcode_eq) {
        this.n_address1_shippingmethodcode_eq = n_address1_shippingmethodcode_eq;
        if(!ObjectUtils.isEmpty(this.n_address1_shippingmethodcode_eq)){
            this.getSearchCond().eq("address1_shippingmethodcode", n_address1_shippingmethodcode_eq);
        }
    }
	private String n_address2_addresstypecode_eq;//[地址 2: 地址类型]
	public void setN_address2_addresstypecode_eq(String n_address2_addresstypecode_eq) {
        this.n_address2_addresstypecode_eq = n_address2_addresstypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_address2_addresstypecode_eq)){
            this.getSearchCond().eq("address2_addresstypecode", n_address2_addresstypecode_eq);
        }
    }
	private String n_businessunitname_like;//[业务部门名称]
	public void setN_businessunitname_like(String n_businessunitname_like) {
        this.n_businessunitname_like = n_businessunitname_like;
        if(!ObjectUtils.isEmpty(this.n_businessunitname_like)){
            this.getSearchCond().like("businessunitname", n_businessunitname_like);
        }
    }
	private String n_address2_shippingmethodcode_eq;//[地址 2: 送货方式]
	public void setN_address2_shippingmethodcode_eq(String n_address2_shippingmethodcode_eq) {
        this.n_address2_shippingmethodcode_eq = n_address2_shippingmethodcode_eq;
        if(!ObjectUtils.isEmpty(this.n_address2_shippingmethodcode_eq)){
            this.getSearchCond().eq("address2_shippingmethodcode", n_address2_shippingmethodcode_eq);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }
	private String n_parentbusinessunitid_eq;//[上级部门]
	public void setN_parentbusinessunitid_eq(String n_parentbusinessunitid_eq) {
        this.n_parentbusinessunitid_eq = n_parentbusinessunitid_eq;
        if(!ObjectUtils.isEmpty(this.n_parentbusinessunitid_eq)){
            this.getSearchCond().eq("parentbusinessunitid", n_parentbusinessunitid_eq);
        }
    }
	private String n_calendarid_eq;//[日历]
	public void setN_calendarid_eq(String n_calendarid_eq) {
        this.n_calendarid_eq = n_calendarid_eq;
        if(!ObjectUtils.isEmpty(this.n_calendarid_eq)){
            this.getSearchCond().eq("calendarid", n_calendarid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("businessunitname", query)   
            );
		 }
	}
}




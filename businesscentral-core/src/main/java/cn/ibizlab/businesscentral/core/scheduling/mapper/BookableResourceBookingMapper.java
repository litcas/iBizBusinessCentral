package cn.ibizlab.businesscentral.core.scheduling.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.scheduling.domain.BookableResourceBooking;
import cn.ibizlab.businesscentral.core.scheduling.filter.BookableResourceBookingSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface BookableResourceBookingMapper extends BaseMapper<BookableResourceBooking>{

    Page<BookableResourceBooking> searchDefault(IPage page, @Param("srf") BookableResourceBookingSearchContext context, @Param("ew") Wrapper<BookableResourceBooking> wrapper) ;
    @Override
    BookableResourceBooking selectById(Serializable id);
    @Override
    int insert(BookableResourceBooking entity);
    @Override
    int updateById(@Param(Constants.ENTITY) BookableResourceBooking entity);
    @Override
    int update(@Param(Constants.ENTITY) BookableResourceBooking entity, @Param("ew") Wrapper<BookableResourceBooking> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<BookableResourceBooking> selectByHeader(@Param("bookableresourcebookingheaderid") Serializable bookableresourcebookingheaderid) ;

    List<BookableResourceBooking> selectByResource(@Param("bookableresourceid") Serializable bookableresourceid) ;

    List<BookableResourceBooking> selectByBookingstatus(@Param("bookingstatusid") Serializable bookingstatusid) ;

    List<BookableResourceBooking> selectByTransactioncurrencyid(@Param("transactioncurrencyid") Serializable transactioncurrencyid) ;

}

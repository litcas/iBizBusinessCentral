package cn.ibizlab.businesscentral.core.base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.base.domain.OMHIERARCHYPURPOSE;
/**
 * 关系型数据实体[OMHIERARCHYPURPOSE] 查询条件对象
 */
@Slf4j
@Data
public class OMHIERARCHYPURPOSESearchContext extends QueryWrapperContext<OMHIERARCHYPURPOSE> {

	private String n_omhierarchypurposename_like;//[组织层次机构目的名称]
	public void setN_omhierarchypurposename_like(String n_omhierarchypurposename_like) {
        this.n_omhierarchypurposename_like = n_omhierarchypurposename_like;
        if(!ObjectUtils.isEmpty(this.n_omhierarchypurposename_like)){
            this.getSearchCond().like("omhierarchypurposename", n_omhierarchypurposename_like);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("omhierarchypurposename", query)   
            );
		 }
	}
}




package cn.ibizlab.businesscentral.core.sales.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[商机对手]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "RELATIONSHIPS",resultMap = "OpportunityCompetitorResultMap")
public class OpportunityCompetitor extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 关系标识
     */
    @DEField(isKeyField=true)
    @TableId(value= "relationshipsid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "relationshipsid")
    @JsonProperty("relationshipsid")
    private String relationshipsid;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 关系类型
     */
    @DEField(defaultValue = "OPPORTUNITYCOMPETITOR")
    @TableField(value = "relationshipstype")
    @JSONField(name = "relationshipstype")
    @JsonProperty("relationshipstype")
    private String relationshipstype;
    /**
     * 关系名称
     */
    @TableField(value = "relationshipsname")
    @JSONField(name = "relationshipsname")
    @JsonProperty("relationshipsname")
    private String relationshipsname;
    /**
     * 潜在顾客
     */
    @TableField(exist = false)
    @JSONField(name = "originatingleadid")
    @JsonProperty("originatingleadid")
    private String originatingleadid;
    /**
     * 商机
     */
    @TableField(value = "entityname")
    @JSONField(name = "entityname")
    @JsonProperty("entityname")
    private String entityname;
    /**
     * 预计收入
     */
    @TableField(exist = false)
    @JSONField(name = "estimatedvalue")
    @JsonProperty("estimatedvalue")
    private BigDecimal estimatedvalue;
    /**
     * 潜在顾客
     */
    @TableField(exist = false)
    @JSONField(name = "originatingleadname")
    @JsonProperty("originatingleadname")
    private String originatingleadname;
    /**
     * 预计结束日期
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "estimatedclosedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("estimatedclosedate")
    private Timestamp estimatedclosedate;
    /**
     * 实际收入
     */
    @TableField(exist = false)
    @JSONField(name = "actualvalue")
    @JsonProperty("actualvalue")
    private BigDecimal actualvalue;
    /**
     * 状态
     */
    @TableField(exist = false)
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;
    /**
     * 竞争对手
     */
    @TableField(value = "entity2name")
    @JSONField(name = "entity2name")
    @JsonProperty("entity2name")
    private String entity2name;
    /**
     * 竞争对手
     */
    @TableField(value = "entity2id")
    @JSONField(name = "entity2id")
    @JsonProperty("entity2id")
    private String entity2id;
    /**
     * 商机
     */
    @TableField(value = "entityid")
    @JSONField(name = "entityid")
    @JsonProperty("entityid")
    private String entityid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.sales.domain.Competitor competitor;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.sales.domain.Opportunity opportunity;



    /**
     * 设置 [关系类型]
     */
    public void setRelationshipstype(String relationshipstype){
        this.relationshipstype = relationshipstype ;
        this.modify("relationshipstype",relationshipstype);
    }

    /**
     * 设置 [关系名称]
     */
    public void setRelationshipsname(String relationshipsname){
        this.relationshipsname = relationshipsname ;
        this.modify("relationshipsname",relationshipsname);
    }

    /**
     * 设置 [商机]
     */
    public void setEntityname(String entityname){
        this.entityname = entityname ;
        this.modify("entityname",entityname);
    }

    /**
     * 设置 [竞争对手]
     */
    public void setEntity2name(String entity2name){
        this.entity2name = entity2name ;
        this.modify("entity2name",entity2name);
    }

    /**
     * 设置 [竞争对手]
     */
    public void setEntity2id(String entity2id){
        this.entity2id = entity2id ;
        this.modify("entity2id",entity2id);
    }

    /**
     * 设置 [商机]
     */
    public void setEntityid(String entityid){
        this.entityid = entityid ;
        this.modify("entityid",entityid);
    }


}



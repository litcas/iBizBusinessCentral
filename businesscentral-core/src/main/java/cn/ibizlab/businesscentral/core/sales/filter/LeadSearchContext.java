package cn.ibizlab.businesscentral.core.sales.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.sales.domain.Lead;
/**
 * 关系型数据实体[Lead] 查询条件对象
 */
@Slf4j
@Data
public class LeadSearchContext extends QueryWrapperContext<Lead> {

	private String n_leadqualitycode_eq;//[等级]
	public void setN_leadqualitycode_eq(String n_leadqualitycode_eq) {
        this.n_leadqualitycode_eq = n_leadqualitycode_eq;
        if(!ObjectUtils.isEmpty(this.n_leadqualitycode_eq)){
            this.getSearchCond().eq("leadqualitycode", n_leadqualitycode_eq);
        }
    }
	private Integer n_statuscode_eq;//[状态描述]
	public void setN_statuscode_eq(Integer n_statuscode_eq) {
        this.n_statuscode_eq = n_statuscode_eq;
        if(!ObjectUtils.isEmpty(this.n_statuscode_eq)){
            this.getSearchCond().eq("statuscode", n_statuscode_eq);
        }
    }
	private String n_address1_addresstypecode_eq;//[地址 1: 地址类型]
	public void setN_address1_addresstypecode_eq(String n_address1_addresstypecode_eq) {
        this.n_address1_addresstypecode_eq = n_address1_addresstypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_address1_addresstypecode_eq)){
            this.getSearchCond().eq("address1_addresstypecode", n_address1_addresstypecode_eq);
        }
    }
	private String n_fullname_like;//[姓名]
	public void setN_fullname_like(String n_fullname_like) {
        this.n_fullname_like = n_fullname_like;
        if(!ObjectUtils.isEmpty(this.n_fullname_like)){
            this.getSearchCond().like("fullname", n_fullname_like);
        }
    }
	private String n_budgetstatus_eq;//[预算]
	public void setN_budgetstatus_eq(String n_budgetstatus_eq) {
        this.n_budgetstatus_eq = n_budgetstatus_eq;
        if(!ObjectUtils.isEmpty(this.n_budgetstatus_eq)){
            this.getSearchCond().eq("budgetstatus", n_budgetstatus_eq);
        }
    }
	private String n_industrycode_eq;//[行业]
	public void setN_industrycode_eq(String n_industrycode_eq) {
        this.n_industrycode_eq = n_industrycode_eq;
        if(!ObjectUtils.isEmpty(this.n_industrycode_eq)){
            this.getSearchCond().eq("industrycode", n_industrycode_eq);
        }
    }
	private String n_initialcommunication_eq;//[初始通信]
	public void setN_initialcommunication_eq(String n_initialcommunication_eq) {
        this.n_initialcommunication_eq = n_initialcommunication_eq;
        if(!ObjectUtils.isEmpty(this.n_initialcommunication_eq)){
            this.getSearchCond().eq("initialcommunication", n_initialcommunication_eq);
        }
    }
	private String n_address1_shippingmethodcode_eq;//[地址 1: 送货方式]
	public void setN_address1_shippingmethodcode_eq(String n_address1_shippingmethodcode_eq) {
        this.n_address1_shippingmethodcode_eq = n_address1_shippingmethodcode_eq;
        if(!ObjectUtils.isEmpty(this.n_address1_shippingmethodcode_eq)){
            this.getSearchCond().eq("address1_shippingmethodcode", n_address1_shippingmethodcode_eq);
        }
    }
	private String n_address2_shippingmethodcode_eq;//[地址 2: 送货方式]
	public void setN_address2_shippingmethodcode_eq(String n_address2_shippingmethodcode_eq) {
        this.n_address2_shippingmethodcode_eq = n_address2_shippingmethodcode_eq;
        if(!ObjectUtils.isEmpty(this.n_address2_shippingmethodcode_eq)){
            this.getSearchCond().eq("address2_shippingmethodcode", n_address2_shippingmethodcode_eq);
        }
    }
	private String n_salesstage_eq;//[销售阶段]
	public void setN_salesstage_eq(String n_salesstage_eq) {
        this.n_salesstage_eq = n_salesstage_eq;
        if(!ObjectUtils.isEmpty(this.n_salesstage_eq)){
            this.getSearchCond().eq("salesstage", n_salesstage_eq);
        }
    }
	private String n_need_eq;//[需求]
	public void setN_need_eq(String n_need_eq) {
        this.n_need_eq = n_need_eq;
        if(!ObjectUtils.isEmpty(this.n_need_eq)){
            this.getSearchCond().eq("need", n_need_eq);
        }
    }
	private String n_prioritycode_eq;//[优先级]
	public void setN_prioritycode_eq(String n_prioritycode_eq) {
        this.n_prioritycode_eq = n_prioritycode_eq;
        if(!ObjectUtils.isEmpty(this.n_prioritycode_eq)){
            this.getSearchCond().eq("prioritycode", n_prioritycode_eq);
        }
    }
	private String n_salesstagecode_eq;//[销售阶段代码]
	public void setN_salesstagecode_eq(String n_salesstagecode_eq) {
        this.n_salesstagecode_eq = n_salesstagecode_eq;
        if(!ObjectUtils.isEmpty(this.n_salesstagecode_eq)){
            this.getSearchCond().eq("salesstagecode", n_salesstagecode_eq);
        }
    }
	private String n_purchasetimeframe_eq;//[购买时间范围]
	public void setN_purchasetimeframe_eq(String n_purchasetimeframe_eq) {
        this.n_purchasetimeframe_eq = n_purchasetimeframe_eq;
        if(!ObjectUtils.isEmpty(this.n_purchasetimeframe_eq)){
            this.getSearchCond().eq("purchasetimeframe", n_purchasetimeframe_eq);
        }
    }
	private String n_leadsourcecode_eq;//[潜在顾客来源]
	public void setN_leadsourcecode_eq(String n_leadsourcecode_eq) {
        this.n_leadsourcecode_eq = n_leadsourcecode_eq;
        if(!ObjectUtils.isEmpty(this.n_leadsourcecode_eq)){
            this.getSearchCond().eq("leadsourcecode", n_leadsourcecode_eq);
        }
    }
	private String n_preferredcontactmethodcode_eq;//[首选联系方式]
	public void setN_preferredcontactmethodcode_eq(String n_preferredcontactmethodcode_eq) {
        this.n_preferredcontactmethodcode_eq = n_preferredcontactmethodcode_eq;
        if(!ObjectUtils.isEmpty(this.n_preferredcontactmethodcode_eq)){
            this.getSearchCond().eq("preferredcontactmethodcode", n_preferredcontactmethodcode_eq);
        }
    }
	private String n_purchaseprocess_eq;//[采购程序]
	public void setN_purchaseprocess_eq(String n_purchaseprocess_eq) {
        this.n_purchaseprocess_eq = n_purchaseprocess_eq;
        if(!ObjectUtils.isEmpty(this.n_purchaseprocess_eq)){
            this.getSearchCond().eq("purchaseprocess", n_purchaseprocess_eq);
        }
    }
	private Integer n_statecode_eq;//[状态]
	public void setN_statecode_eq(Integer n_statecode_eq) {
        this.n_statecode_eq = n_statecode_eq;
        if(!ObjectUtils.isEmpty(this.n_statecode_eq)){
            this.getSearchCond().eq("statecode", n_statecode_eq);
        }
    }
	private String n_address2_addresstypecode_eq;//[地址 2: 地址类型]
	public void setN_address2_addresstypecode_eq(String n_address2_addresstypecode_eq) {
        this.n_address2_addresstypecode_eq = n_address2_addresstypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_address2_addresstypecode_eq)){
            this.getSearchCond().eq("address2_addresstypecode", n_address2_addresstypecode_eq);
        }
    }
	private String n_relatedobjectname_eq;//[相关市场活动响应]
	public void setN_relatedobjectname_eq(String n_relatedobjectname_eq) {
        this.n_relatedobjectname_eq = n_relatedobjectname_eq;
        if(!ObjectUtils.isEmpty(this.n_relatedobjectname_eq)){
            this.getSearchCond().eq("relatedobjectname", n_relatedobjectname_eq);
        }
    }
	private String n_relatedobjectname_like;//[相关市场活动响应]
	public void setN_relatedobjectname_like(String n_relatedobjectname_like) {
        this.n_relatedobjectname_like = n_relatedobjectname_like;
        if(!ObjectUtils.isEmpty(this.n_relatedobjectname_like)){
            this.getSearchCond().like("relatedobjectname", n_relatedobjectname_like);
        }
    }
	private String n_currencyname_eq;//[货币]
	public void setN_currencyname_eq(String n_currencyname_eq) {
        this.n_currencyname_eq = n_currencyname_eq;
        if(!ObjectUtils.isEmpty(this.n_currencyname_eq)){
            this.getSearchCond().eq("currencyname", n_currencyname_eq);
        }
    }
	private String n_currencyname_like;//[货币]
	public void setN_currencyname_like(String n_currencyname_like) {
        this.n_currencyname_like = n_currencyname_like;
        if(!ObjectUtils.isEmpty(this.n_currencyname_like)){
            this.getSearchCond().like("currencyname", n_currencyname_like);
        }
    }
	private String n_originatingcasename_eq;//[原始案例]
	public void setN_originatingcasename_eq(String n_originatingcasename_eq) {
        this.n_originatingcasename_eq = n_originatingcasename_eq;
        if(!ObjectUtils.isEmpty(this.n_originatingcasename_eq)){
            this.getSearchCond().eq("originatingcasename", n_originatingcasename_eq);
        }
    }
	private String n_originatingcasename_like;//[原始案例]
	public void setN_originatingcasename_like(String n_originatingcasename_like) {
        this.n_originatingcasename_like = n_originatingcasename_like;
        if(!ObjectUtils.isEmpty(this.n_originatingcasename_like)){
            this.getSearchCond().like("originatingcasename", n_originatingcasename_like);
        }
    }
	private String n_campaignname_eq;//[源市场活动]
	public void setN_campaignname_eq(String n_campaignname_eq) {
        this.n_campaignname_eq = n_campaignname_eq;
        if(!ObjectUtils.isEmpty(this.n_campaignname_eq)){
            this.getSearchCond().eq("campaignname", n_campaignname_eq);
        }
    }
	private String n_campaignname_like;//[源市场活动]
	public void setN_campaignname_like(String n_campaignname_like) {
        this.n_campaignname_like = n_campaignname_like;
        if(!ObjectUtils.isEmpty(this.n_campaignname_like)){
            this.getSearchCond().like("campaignname", n_campaignname_like);
        }
    }
	private String n_parentaccountname_eq;//[识别客户]
	public void setN_parentaccountname_eq(String n_parentaccountname_eq) {
        this.n_parentaccountname_eq = n_parentaccountname_eq;
        if(!ObjectUtils.isEmpty(this.n_parentaccountname_eq)){
            this.getSearchCond().eq("parentaccountname", n_parentaccountname_eq);
        }
    }
	private String n_parentaccountname_like;//[识别客户]
	public void setN_parentaccountname_like(String n_parentaccountname_like) {
        this.n_parentaccountname_like = n_parentaccountname_like;
        if(!ObjectUtils.isEmpty(this.n_parentaccountname_like)){
            this.getSearchCond().like("parentaccountname", n_parentaccountname_like);
        }
    }
	private String n_parentcontactname_eq;//[识别联系人]
	public void setN_parentcontactname_eq(String n_parentcontactname_eq) {
        this.n_parentcontactname_eq = n_parentcontactname_eq;
        if(!ObjectUtils.isEmpty(this.n_parentcontactname_eq)){
            this.getSearchCond().eq("parentcontactname", n_parentcontactname_eq);
        }
    }
	private String n_parentcontactname_like;//[识别联系人]
	public void setN_parentcontactname_like(String n_parentcontactname_like) {
        this.n_parentcontactname_like = n_parentcontactname_like;
        if(!ObjectUtils.isEmpty(this.n_parentcontactname_like)){
            this.getSearchCond().like("parentcontactname", n_parentcontactname_like);
        }
    }
	private String n_qualifyingopportunityname_eq;//[授予商机资格]
	public void setN_qualifyingopportunityname_eq(String n_qualifyingopportunityname_eq) {
        this.n_qualifyingopportunityname_eq = n_qualifyingopportunityname_eq;
        if(!ObjectUtils.isEmpty(this.n_qualifyingopportunityname_eq)){
            this.getSearchCond().eq("qualifyingopportunityname", n_qualifyingopportunityname_eq);
        }
    }
	private String n_qualifyingopportunityname_like;//[授予商机资格]
	public void setN_qualifyingopportunityname_like(String n_qualifyingopportunityname_like) {
        this.n_qualifyingopportunityname_like = n_qualifyingopportunityname_like;
        if(!ObjectUtils.isEmpty(this.n_qualifyingopportunityname_like)){
            this.getSearchCond().like("qualifyingopportunityname", n_qualifyingopportunityname_like);
        }
    }
	private String n_qualifyingopportunityid_eq;//[授予商机资格]
	public void setN_qualifyingopportunityid_eq(String n_qualifyingopportunityid_eq) {
        this.n_qualifyingopportunityid_eq = n_qualifyingopportunityid_eq;
        if(!ObjectUtils.isEmpty(this.n_qualifyingopportunityid_eq)){
            this.getSearchCond().eq("qualifyingopportunityid", n_qualifyingopportunityid_eq);
        }
    }
	private String n_slaid_eq;//[SLA]
	public void setN_slaid_eq(String n_slaid_eq) {
        this.n_slaid_eq = n_slaid_eq;
        if(!ObjectUtils.isEmpty(this.n_slaid_eq)){
            this.getSearchCond().eq("slaid", n_slaid_eq);
        }
    }
	private String n_campaignid_eq;//[源市场活动]
	public void setN_campaignid_eq(String n_campaignid_eq) {
        this.n_campaignid_eq = n_campaignid_eq;
        if(!ObjectUtils.isEmpty(this.n_campaignid_eq)){
            this.getSearchCond().eq("campaignid", n_campaignid_eq);
        }
    }
	private String n_relatedobjectid_eq;//[相关市场活动响应]
	public void setN_relatedobjectid_eq(String n_relatedobjectid_eq) {
        this.n_relatedobjectid_eq = n_relatedobjectid_eq;
        if(!ObjectUtils.isEmpty(this.n_relatedobjectid_eq)){
            this.getSearchCond().eq("relatedobjectid", n_relatedobjectid_eq);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }
	private String n_parentaccountid_eq;//[潜在顾客的上级单位]
	public void setN_parentaccountid_eq(String n_parentaccountid_eq) {
        this.n_parentaccountid_eq = n_parentaccountid_eq;
        if(!ObjectUtils.isEmpty(this.n_parentaccountid_eq)){
            this.getSearchCond().eq("parentaccountid", n_parentaccountid_eq);
        }
    }
	private String n_originatingcaseid_eq;//[原始案例]
	public void setN_originatingcaseid_eq(String n_originatingcaseid_eq) {
        this.n_originatingcaseid_eq = n_originatingcaseid_eq;
        if(!ObjectUtils.isEmpty(this.n_originatingcaseid_eq)){
            this.getSearchCond().eq("originatingcaseid", n_originatingcaseid_eq);
        }
    }
	private String n_parentcontactid_eq;//[潜在顾客的上司]
	public void setN_parentcontactid_eq(String n_parentcontactid_eq) {
        this.n_parentcontactid_eq = n_parentcontactid_eq;
        if(!ObjectUtils.isEmpty(this.n_parentcontactid_eq)){
            this.getSearchCond().eq("parentcontactid", n_parentcontactid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("fullname", query)   
            );
		 }
	}
}




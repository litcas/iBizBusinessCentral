package cn.ibizlab.businesscentral.core.scheduling.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.scheduling.domain.BookableResource;
import cn.ibizlab.businesscentral.core.scheduling.filter.BookableResourceSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface BookableResourceMapper extends BaseMapper<BookableResource>{

    Page<BookableResource> searchDefault(IPage page, @Param("srf") BookableResourceSearchContext context, @Param("ew") Wrapper<BookableResource> wrapper) ;
    @Override
    BookableResource selectById(Serializable id);
    @Override
    int insert(BookableResource entity);
    @Override
    int updateById(@Param(Constants.ENTITY) BookableResource entity);
    @Override
    int update(@Param(Constants.ENTITY) BookableResource entity, @Param("ew") Wrapper<BookableResource> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<BookableResource> selectByAccountid(@Param("accountid") Serializable accountid) ;

    List<BookableResource> selectByCalendarid(@Param("calendarid") Serializable calendarid) ;

    List<BookableResource> selectByContactid(@Param("contactid") Serializable contactid) ;

    List<BookableResource> selectByTransactioncurrencyid(@Param("transactioncurrencyid") Serializable transactioncurrencyid) ;

}

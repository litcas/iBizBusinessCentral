package cn.ibizlab.businesscentral.core.humanresource.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.humanresource.domain.Leave;
/**
 * 关系型数据实体[Leave] 查询条件对象
 */
@Slf4j
@Data
public class LeaveSearchContext extends QueryWrapperContext<Leave> {

	private String n_leavename_like;//[请假名称]
	public void setN_leavename_like(String n_leavename_like) {
        this.n_leavename_like = n_leavename_like;
        if(!ObjectUtils.isEmpty(this.n_leavename_like)){
            this.getSearchCond().like("leavename", n_leavename_like);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("leavename", query)   
            );
		 }
	}
}




package cn.ibizlab.businesscentral.core.base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.base.domain.KnowledgeArticleViews;
import cn.ibizlab.businesscentral.core.base.filter.KnowledgeArticleViewsSearchContext;
import cn.ibizlab.businesscentral.core.base.service.IKnowledgeArticleViewsService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.base.mapper.KnowledgeArticleViewsMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[知识文章视图] 服务对象接口实现
 */
@Slf4j
@Service("KnowledgeArticleViewsServiceImpl")
public class KnowledgeArticleViewsServiceImpl extends ServiceImpl<KnowledgeArticleViewsMapper, KnowledgeArticleViews> implements IKnowledgeArticleViewsService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IKnowledgeArticleService knowledgearticleService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(KnowledgeArticleViews et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getKnowledgearticleviewsid()),et);
        return true;
    }

    @Override
    public void createBatch(List<KnowledgeArticleViews> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(KnowledgeArticleViews et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("knowledgearticleviewsid",et.getKnowledgearticleviewsid())))
            return false;
        CachedBeanCopier.copy(get(et.getKnowledgearticleviewsid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<KnowledgeArticleViews> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public KnowledgeArticleViews get(String key) {
        KnowledgeArticleViews et = getById(key);
        if(et==null){
            et=new KnowledgeArticleViews();
            et.setKnowledgearticleviewsid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public KnowledgeArticleViews getDraft(KnowledgeArticleViews et) {
        return et;
    }

    @Override
    public boolean checkKey(KnowledgeArticleViews et) {
        return (!ObjectUtils.isEmpty(et.getKnowledgearticleviewsid()))&&(!Objects.isNull(this.getById(et.getKnowledgearticleviewsid())));
    }
    @Override
    @Transactional
    public boolean save(KnowledgeArticleViews et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(KnowledgeArticleViews et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<KnowledgeArticleViews> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<KnowledgeArticleViews> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<KnowledgeArticleViews> selectByKnowledgearticleid(String knowledgearticleid) {
        return baseMapper.selectByKnowledgearticleid(knowledgearticleid);
    }

    @Override
    public void removeByKnowledgearticleid(String knowledgearticleid) {
        this.remove(new QueryWrapper<KnowledgeArticleViews>().eq("knowledgearticleid",knowledgearticleid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<KnowledgeArticleViews> searchDefault(KnowledgeArticleViewsSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<KnowledgeArticleViews> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<KnowledgeArticleViews>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<KnowledgeArticleViews> getKnowledgearticleviewsByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<KnowledgeArticleViews> getKnowledgearticleviewsByEntities(List<KnowledgeArticleViews> entities) {
        List ids =new ArrayList();
        for(KnowledgeArticleViews entity : entities){
            Serializable id=entity.getKnowledgearticleviewsid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




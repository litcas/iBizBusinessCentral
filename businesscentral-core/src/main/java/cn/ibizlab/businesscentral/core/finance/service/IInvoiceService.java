package cn.ibizlab.businesscentral.core.finance.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.finance.domain.Invoice;
import cn.ibizlab.businesscentral.core.finance.filter.InvoiceSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Invoice] 服务对象接口
 */
public interface IInvoiceService extends IService<Invoice>{

    boolean create(Invoice et) ;
    void createBatch(List<Invoice> list) ;
    boolean update(Invoice et) ;
    void updateBatch(List<Invoice> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Invoice get(String key) ;
    Invoice getDraft(Invoice et) ;
    Invoice cancel(Invoice et) ;
    boolean checkKey(Invoice et) ;
    Invoice finish(Invoice et) ;
    Invoice paid(Invoice et) ;
    boolean save(Invoice et) ;
    void saveBatch(List<Invoice> list) ;
    Page<Invoice> searchByParentKey(InvoiceSearchContext context) ;
    Page<Invoice> searchCancel(InvoiceSearchContext context) ;
    Page<Invoice> searchDefault(InvoiceSearchContext context) ;
    Page<Invoice> searchPaid(InvoiceSearchContext context) ;
    List<Invoice> selectByOpportunityid(String opportunityid) ;
    void removeByOpportunityid(String opportunityid) ;
    List<Invoice> selectByPricelevelid(String pricelevelid) ;
    void removeByPricelevelid(String pricelevelid) ;
    List<Invoice> selectBySalesorderid(String salesorderid) ;
    void removeBySalesorderid(String salesorderid) ;
    List<Invoice> selectBySlaid(String slaid) ;
    void removeBySlaid(String slaid) ;
    List<Invoice> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Invoice> getInvoiceByIds(List<String> ids) ;
    List<Invoice> getInvoiceByEntities(List<Invoice> entities) ;
}





package cn.ibizlab.businesscentral.core.service.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.service.domain.ServiceAppointment;
import cn.ibizlab.businesscentral.core.base.domain.ActivityPointer;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface ServiceAppointmentInheritMapping {

    @Mappings({
        @Mapping(source ="activityid",target = "activityid"),
        @Mapping(source ="subject",target = "subject"),
        @Mapping(target ="focusNull",ignore = true),
    })
    ActivityPointer toActivitypointer(ServiceAppointment serviceappointment);

    @Mappings({
        @Mapping(source ="activityid" ,target = "activityid"),
        @Mapping(source ="subject" ,target = "subject"),
        @Mapping(target ="focusNull",ignore = true),
    })
    ServiceAppointment toServiceappointment(ActivityPointer activitypointer);

    List<ActivityPointer> toActivitypointer(List<ServiceAppointment> serviceappointment);

    List<ServiceAppointment> toServiceappointment(List<ActivityPointer> activitypointer);

}



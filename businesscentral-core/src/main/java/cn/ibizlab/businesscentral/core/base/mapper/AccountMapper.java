package cn.ibizlab.businesscentral.core.base.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.base.domain.Account;
import cn.ibizlab.businesscentral.core.base.filter.AccountSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface AccountMapper extends BaseMapper<Account>{

    Page<Account> searchByParentKey(IPage page, @Param("srf") AccountSearchContext context, @Param("ew") Wrapper<Account> wrapper) ;
    Page<Account> searchDefault(IPage page, @Param("srf") AccountSearchContext context, @Param("ew") Wrapper<Account> wrapper) ;
    Page<Account> searchRoot(IPage page, @Param("srf") AccountSearchContext context, @Param("ew") Wrapper<Account> wrapper) ;
    Page<Account> searchStop(IPage page, @Param("srf") AccountSearchContext context, @Param("ew") Wrapper<Account> wrapper) ;
    Page<Account> searchUsable(IPage page, @Param("srf") AccountSearchContext context, @Param("ew") Wrapper<Account> wrapper) ;
    @Override
    Account selectById(Serializable id);
    @Override
    int insert(Account entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Account entity);
    @Override
    int update(@Param(Constants.ENTITY) Account entity, @Param("ew") Wrapper<Account> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Account> selectByParentaccountid(@Param("accountid") Serializable accountid) ;

    List<Account> selectByPrimarycontactid(@Param("contactid") Serializable contactid) ;

    List<Account> selectByPreferredequipmentid(@Param("equipmentid") Serializable equipmentid) ;

    List<Account> selectByOriginatingleadid(@Param("leadid") Serializable leadid) ;

    List<Account> selectByDefaultpricelevelid(@Param("pricelevelid") Serializable pricelevelid) ;

    List<Account> selectByPreferredserviceid(@Param("serviceid") Serializable serviceid) ;

    List<Account> selectBySlaid(@Param("slaid") Serializable slaid) ;

    List<Account> selectByTerritoryid(@Param("territoryid") Serializable territoryid) ;

    List<Account> selectByTransactioncurrencyid(@Param("transactioncurrencyid") Serializable transactioncurrencyid) ;

}

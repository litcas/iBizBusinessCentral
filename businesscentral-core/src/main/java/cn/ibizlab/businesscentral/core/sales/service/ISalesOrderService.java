package cn.ibizlab.businesscentral.core.sales.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.sales.domain.SalesOrder;
import cn.ibizlab.businesscentral.core.sales.filter.SalesOrderSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[SalesOrder] 服务对象接口
 */
public interface ISalesOrderService extends IService<SalesOrder>{

    boolean create(SalesOrder et) ;
    void createBatch(List<SalesOrder> list) ;
    boolean update(SalesOrder et) ;
    void updateBatch(List<SalesOrder> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    SalesOrder get(String key) ;
    SalesOrder getDraft(SalesOrder et) ;
    SalesOrder cancel(SalesOrder et) ;
    boolean checkKey(SalesOrder et) ;
    SalesOrder finish(SalesOrder et) ;
    SalesOrder genInvoice(SalesOrder et) ;
    boolean save(SalesOrder et) ;
    void saveBatch(List<SalesOrder> list) ;
    Page<SalesOrder> searchByParentKey(SalesOrderSearchContext context) ;
    Page<SalesOrder> searchCancel(SalesOrderSearchContext context) ;
    Page<SalesOrder> searchDefault(SalesOrderSearchContext context) ;
    Page<SalesOrder> searchFinish(SalesOrderSearchContext context) ;
    Page<SalesOrder> searchInvoiced(SalesOrderSearchContext context) ;
    List<SalesOrder> selectByCampaignid(String campaignid) ;
    void removeByCampaignid(String campaignid) ;
    List<SalesOrder> selectByOpportunityid(String opportunityid) ;
    void removeByOpportunityid(String opportunityid) ;
    List<SalesOrder> selectByPricelevelid(String pricelevelid) ;
    void removeByPricelevelid(String pricelevelid) ;
    List<SalesOrder> selectByQuoteid(String quoteid) ;
    void removeByQuoteid(String quoteid) ;
    List<SalesOrder> selectBySlaid(String slaid) ;
    void removeBySlaid(String slaid) ;
    List<SalesOrder> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<SalesOrder> getSalesorderByIds(List<String> ids) ;
    List<SalesOrder> getSalesorderByEntities(List<SalesOrder> entities) ;
}



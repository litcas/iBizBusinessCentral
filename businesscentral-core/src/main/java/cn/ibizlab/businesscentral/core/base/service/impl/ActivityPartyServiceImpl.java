package cn.ibizlab.businesscentral.core.base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.base.domain.ActivityParty;
import cn.ibizlab.businesscentral.core.base.filter.ActivityPartySearchContext;
import cn.ibizlab.businesscentral.core.base.service.IActivityPartyService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.base.mapper.ActivityPartyMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[活动方] 服务对象接口实现
 */
@Slf4j
@Service("ActivityPartyServiceImpl")
public class ActivityPartyServiceImpl extends ServiceImpl<ActivityPartyMapper, ActivityParty> implements IActivityPartyService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IActivityPointerService activitypointerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IResourceSpecService resourcespecService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(ActivityParty et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getActivitypartyid()),et);
        return true;
    }

    @Override
    public void createBatch(List<ActivityParty> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(ActivityParty et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("activitypartyid",et.getActivitypartyid())))
            return false;
        CachedBeanCopier.copy(get(et.getActivitypartyid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<ActivityParty> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public ActivityParty get(String key) {
        ActivityParty et = getById(key);
        if(et==null){
            et=new ActivityParty();
            et.setActivitypartyid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public ActivityParty getDraft(ActivityParty et) {
        return et;
    }

    @Override
    public boolean checkKey(ActivityParty et) {
        return (!ObjectUtils.isEmpty(et.getActivitypartyid()))&&(!Objects.isNull(this.getById(et.getActivitypartyid())));
    }
    @Override
    @Transactional
    public boolean save(ActivityParty et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(ActivityParty et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<ActivityParty> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<ActivityParty> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<ActivityParty> selectByActivityid(String activityid) {
        return baseMapper.selectByActivityid(activityid);
    }

    @Override
    public void removeByActivityid(String activityid) {
        this.remove(new QueryWrapper<ActivityParty>().eq("activityid",activityid));
    }

	@Override
    public List<ActivityParty> selectByResourcespecid(String resourcespecid) {
        return baseMapper.selectByResourcespecid(resourcespecid);
    }

    @Override
    public void removeByResourcespecid(String resourcespecid) {
        this.remove(new QueryWrapper<ActivityParty>().eq("resourcespecid",resourcespecid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<ActivityParty> searchDefault(ActivityPartySearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<ActivityParty> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<ActivityParty>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<ActivityParty> getActivitypartyByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<ActivityParty> getActivitypartyByEntities(List<ActivityParty> entities) {
        List ids =new ArrayList();
        for(ActivityParty entity : entities){
            Serializable id=entity.getActivitypartyid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




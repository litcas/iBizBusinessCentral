package cn.ibizlab.businesscentral.core.sales.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.sales.domain.QuoteClose;
import cn.ibizlab.businesscentral.core.sales.filter.QuoteCloseSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[QuoteClose] 服务对象接口
 */
public interface IQuoteCloseService extends IService<QuoteClose>{

    boolean create(QuoteClose et) ;
    void createBatch(List<QuoteClose> list) ;
    boolean update(QuoteClose et) ;
    void updateBatch(List<QuoteClose> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    QuoteClose get(String key) ;
    QuoteClose getDraft(QuoteClose et) ;
    boolean checkKey(QuoteClose et) ;
    boolean save(QuoteClose et) ;
    void saveBatch(List<QuoteClose> list) ;
    Page<QuoteClose> searchDefault(QuoteCloseSearchContext context) ;
    List<QuoteClose> selectByQuoteid(String quoteid) ;
    void removeByQuoteid(String quoteid) ;
    List<QuoteClose> selectByServiceid(String serviceid) ;
    void removeByServiceid(String serviceid) ;
    List<QuoteClose> selectBySlaid(String slaid) ;
    void removeBySlaid(String slaid) ;
    List<QuoteClose> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<QuoteClose> getQuotecloseByIds(List<String> ids) ;
    List<QuoteClose> getQuotecloseByEntities(List<QuoteClose> entities) ;
}



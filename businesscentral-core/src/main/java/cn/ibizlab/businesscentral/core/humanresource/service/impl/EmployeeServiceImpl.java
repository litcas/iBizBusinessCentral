package cn.ibizlab.businesscentral.core.humanresource.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.humanresource.domain.Employee;
import cn.ibizlab.businesscentral.core.humanresource.filter.EmployeeSearchContext;
import cn.ibizlab.businesscentral.core.humanresource.service.IEmployeeService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.humanresource.mapper.EmployeeMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[员工] 服务对象接口实现
 */
@Slf4j
@Service("EmployeeServiceImpl")
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper, Employee> implements IEmployeeService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IOrganizationService organizationService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(Employee et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getEmployeeid()),et);
        return true;
    }

    @Override
    public void createBatch(List<Employee> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Employee et) {
        fillParentData(et);
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("employeeid",et.getEmployeeid())))
            return false;
        CachedBeanCopier.copy(get(et.getEmployeeid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<Employee> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Employee get(String key) {
        Employee et = getById(key);
        if(et==null){
            et=new Employee();
            et.setEmployeeid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Employee getDraft(Employee et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Employee et) {
        return (!ObjectUtils.isEmpty(et.getEmployeeid()))&&(!Objects.isNull(this.getById(et.getEmployeeid())));
    }
    @Override
    @Transactional
    public boolean save(Employee et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Employee et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<Employee> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<Employee> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Employee> selectByOrganizationid(String organizationid) {
        return baseMapper.selectByOrganizationid(organizationid);
    }

    @Override
    public void removeByOrganizationid(String organizationid) {
        this.remove(new QueryWrapper<Employee>().eq("organizationid",organizationid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<Employee> searchDefault(EmployeeSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Employee> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Employee>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Employee et){
        //实体关系[DER1N_EMPLOYEE_ORGANIZATION_ORGANIZATIONID]
        if(!ObjectUtils.isEmpty(et.getOrganizationid())){
            cn.ibizlab.businesscentral.core.base.domain.Organization organization=et.getOrganization();
            if(ObjectUtils.isEmpty(organization)){
                cn.ibizlab.businesscentral.core.base.domain.Organization majorEntity=organizationService.get(et.getOrganizationid());
                et.setOrganization(majorEntity);
                organization=majorEntity;
            }
            et.setOrganizationname(organization.getOrganizationname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Employee> getEmployeeByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Employee> getEmployeeByEntities(List<Employee> entities) {
        List ids =new ArrayList();
        for(Employee entity : entities){
            Serializable id=entity.getEmployeeid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




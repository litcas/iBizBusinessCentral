package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.Account;
import cn.ibizlab.businesscentral.core.base.filter.AccountSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account] 服务对象接口
 */
public interface IAccountService extends IService<Account>{

    boolean create(Account et) ;
    void createBatch(List<Account> list) ;
    boolean update(Account et) ;
    void updateBatch(List<Account> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Account get(String key) ;
    Account getDraft(Account et) ;
    Account active(Account et) ;
    Account addList(Account et) ;
    boolean checkKey(Account et) ;
    Account inactive(Account et) ;
    boolean save(Account et) ;
    void saveBatch(List<Account> list) ;
    Page<Account> searchByParentKey(AccountSearchContext context) ;
    Page<Account> searchDefault(AccountSearchContext context) ;
    Page<Account> searchRoot(AccountSearchContext context) ;
    Page<Account> searchStop(AccountSearchContext context) ;
    Page<Account> searchUsable(AccountSearchContext context) ;
    List<Account> selectByParentaccountid(String accountid) ;
    void removeByParentaccountid(String accountid) ;
    List<Account> selectByPrimarycontactid(String contactid) ;
    void removeByPrimarycontactid(String contactid) ;
    List<Account> selectByPreferredequipmentid(String equipmentid) ;
    void removeByPreferredequipmentid(String equipmentid) ;
    List<Account> selectByOriginatingleadid(String leadid) ;
    void removeByOriginatingleadid(String leadid) ;
    List<Account> selectByDefaultpricelevelid(String pricelevelid) ;
    void removeByDefaultpricelevelid(String pricelevelid) ;
    List<Account> selectByPreferredserviceid(String serviceid) ;
    void removeByPreferredserviceid(String serviceid) ;
    List<Account> selectBySlaid(String slaid) ;
    void removeBySlaid(String slaid) ;
    List<Account> selectByTerritoryid(String territoryid) ;
    void removeByTerritoryid(String territoryid) ;
    List<Account> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account> getAccountByIds(List<String> ids) ;
    List<Account> getAccountByEntities(List<Account> entities) ;
}



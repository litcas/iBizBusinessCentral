package cn.ibizlab.businesscentral.core.marketing.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.marketing.domain.IBizList;
import cn.ibizlab.businesscentral.core.marketing.filter.IBizListSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[IBizList] 服务对象接口
 */
public interface IIBizListService extends IService<IBizList>{

    boolean create(IBizList et) ;
    void createBatch(List<IBizList> list) ;
    boolean update(IBizList et) ;
    void updateBatch(List<IBizList> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    IBizList get(String key) ;
    IBizList getDraft(IBizList et) ;
    IBizList active(IBizList et) ;
    boolean checkKey(IBizList et) ;
    boolean save(IBizList et) ;
    void saveBatch(List<IBizList> list) ;
    IBizList stop(IBizList et) ;
    Page<IBizList> searchDefault(IBizListSearchContext context) ;
    Page<IBizList> searchEffective(IBizListSearchContext context) ;
    Page<IBizList> searchStop(IBizListSearchContext context) ;
    List<IBizList> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<IBizList> getIbizlistByIds(List<String> ids) ;
    List<IBizList> getIbizlistByEntities(List<IBizList> entities) ;
}



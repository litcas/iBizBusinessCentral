package cn.ibizlab.businesscentral.core.scheduling.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.scheduling.domain.BookableResourceCharacteristic;
import cn.ibizlab.businesscentral.core.scheduling.filter.BookableResourceCharacteristicSearchContext;
import cn.ibizlab.businesscentral.core.scheduling.service.IBookableResourceCharacteristicService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.scheduling.mapper.BookableResourceCharacteristicMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[可预订资源的特征] 服务对象接口实现
 */
@Slf4j
@Service("BookableResourceCharacteristicServiceImpl")
public class BookableResourceCharacteristicServiceImpl extends ServiceImpl<BookableResourceCharacteristicMapper, BookableResourceCharacteristic> implements IBookableResourceCharacteristicService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.scheduling.service.IBookableResourceService bookableresourceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.scheduling.service.ICharacteristicService characteristicService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.scheduling.service.IRatingValueService ratingvalueService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(BookableResourceCharacteristic et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getBookableresourcecharacteristicid()),et);
        return true;
    }

    @Override
    public void createBatch(List<BookableResourceCharacteristic> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(BookableResourceCharacteristic et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("bookablerescharacteristicid",et.getBookableresourcecharacteristicid())))
            return false;
        CachedBeanCopier.copy(get(et.getBookableresourcecharacteristicid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<BookableResourceCharacteristic> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public BookableResourceCharacteristic get(String key) {
        BookableResourceCharacteristic et = getById(key);
        if(et==null){
            et=new BookableResourceCharacteristic();
            et.setBookableresourcecharacteristicid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public BookableResourceCharacteristic getDraft(BookableResourceCharacteristic et) {
        return et;
    }

    @Override
    public boolean checkKey(BookableResourceCharacteristic et) {
        return (!ObjectUtils.isEmpty(et.getBookableresourcecharacteristicid()))&&(!Objects.isNull(this.getById(et.getBookableresourcecharacteristicid())));
    }
    @Override
    @Transactional
    public boolean save(BookableResourceCharacteristic et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(BookableResourceCharacteristic et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<BookableResourceCharacteristic> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<BookableResourceCharacteristic> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<BookableResourceCharacteristic> selectByResource(String bookableresourceid) {
        return baseMapper.selectByResource(bookableresourceid);
    }

    @Override
    public void removeByResource(String bookableresourceid) {
        this.remove(new QueryWrapper<BookableResourceCharacteristic>().eq("resource",bookableresourceid));
    }

	@Override
    public List<BookableResourceCharacteristic> selectByCharacteristic(String characteristicid) {
        return baseMapper.selectByCharacteristic(characteristicid);
    }

    @Override
    public void removeByCharacteristic(String characteristicid) {
        this.remove(new QueryWrapper<BookableResourceCharacteristic>().eq("characteristic",characteristicid));
    }

	@Override
    public List<BookableResourceCharacteristic> selectByRatingvalue(String ratingvalueid) {
        return baseMapper.selectByRatingvalue(ratingvalueid);
    }

    @Override
    public void removeByRatingvalue(String ratingvalueid) {
        this.remove(new QueryWrapper<BookableResourceCharacteristic>().eq("ratingvalue",ratingvalueid));
    }

	@Override
    public List<BookableResourceCharacteristic> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<BookableResourceCharacteristic>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<BookableResourceCharacteristic> searchDefault(BookableResourceCharacteristicSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<BookableResourceCharacteristic> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<BookableResourceCharacteristic>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<BookableResourceCharacteristic> getBookableresourcecharacteristicByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<BookableResourceCharacteristic> getBookableresourcecharacteristicByEntities(List<BookableResourceCharacteristic> entities) {
        List ids =new ArrayList();
        for(BookableResourceCharacteristic entity : entities){
            Serializable id=entity.getBookableresourcecharacteristicid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




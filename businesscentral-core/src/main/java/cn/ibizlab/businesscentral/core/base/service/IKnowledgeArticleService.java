package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.KnowledgeArticle;
import cn.ibizlab.businesscentral.core.base.filter.KnowledgeArticleSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[KnowledgeArticle] 服务对象接口
 */
public interface IKnowledgeArticleService extends IService<KnowledgeArticle>{

    boolean create(KnowledgeArticle et) ;
    void createBatch(List<KnowledgeArticle> list) ;
    boolean update(KnowledgeArticle et) ;
    void updateBatch(List<KnowledgeArticle> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    KnowledgeArticle get(String key) ;
    KnowledgeArticle getDraft(KnowledgeArticle et) ;
    boolean checkKey(KnowledgeArticle et) ;
    boolean save(KnowledgeArticle et) ;
    void saveBatch(List<KnowledgeArticle> list) ;
    Page<KnowledgeArticle> searchDefault(KnowledgeArticleSearchContext context) ;
    List<KnowledgeArticle> selectByParentarticlecontentid(String knowledgearticleid) ;
    void removeByParentarticlecontentid(String knowledgearticleid) ;
    List<KnowledgeArticle> selectByPreviousarticlecontentid(String knowledgearticleid) ;
    void removeByPreviousarticlecontentid(String knowledgearticleid) ;
    List<KnowledgeArticle> selectByRootarticleid(String knowledgearticleid) ;
    void removeByRootarticleid(String knowledgearticleid) ;
    List<KnowledgeArticle> selectByLanguagelocaleid(String languagelocaleid) ;
    void removeByLanguagelocaleid(String languagelocaleid) ;
    List<KnowledgeArticle> selectBySubjectid(String subjectid) ;
    void removeBySubjectid(String subjectid) ;
    List<KnowledgeArticle> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<KnowledgeArticle> getKnowledgearticleByIds(List<String> ids) ;
    List<KnowledgeArticle> getKnowledgearticleByEntities(List<KnowledgeArticle> entities) ;
}



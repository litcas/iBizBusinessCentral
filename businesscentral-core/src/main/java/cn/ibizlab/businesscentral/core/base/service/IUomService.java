package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.Uom;
import cn.ibizlab.businesscentral.core.base.filter.UomSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Uom] 服务对象接口
 */
public interface IUomService extends IService<Uom>{

    boolean create(Uom et) ;
    void createBatch(List<Uom> list) ;
    boolean update(Uom et) ;
    void updateBatch(List<Uom> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Uom get(String key) ;
    Uom getDraft(Uom et) ;
    boolean checkKey(Uom et) ;
    boolean save(Uom et) ;
    void saveBatch(List<Uom> list) ;
    Page<Uom> searchDefault(UomSearchContext context) ;
    List<Uom> selectByUomscheduleid(String uomscheduleid) ;
    void removeByUomscheduleid(String uomscheduleid) ;
    List<Uom> selectByBaseuom(String uomid) ;
    void removeByBaseuom(String uomid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Uom> getUomByIds(List<String> ids) ;
    List<Uom> getUomByEntities(List<Uom> entities) ;
}



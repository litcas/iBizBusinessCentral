package cn.ibizlab.businesscentral.core.service.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.service.domain.Incident;
/**
 * 关系型数据实体[Incident] 查询条件对象
 */
@Slf4j
@Data
public class IncidentSearchContext extends QueryWrapperContext<Incident> {

	private String n_firstresponseslastatus_eq;//[第一个响应 SLA 状态]
	public void setN_firstresponseslastatus_eq(String n_firstresponseslastatus_eq) {
        this.n_firstresponseslastatus_eq = n_firstresponseslastatus_eq;
        if(!ObjectUtils.isEmpty(this.n_firstresponseslastatus_eq)){
            this.getSearchCond().eq("firstresponseslastatus", n_firstresponseslastatus_eq);
        }
    }
	private String n_incidentstagecode_eq;//[案例阶段]
	public void setN_incidentstagecode_eq(String n_incidentstagecode_eq) {
        this.n_incidentstagecode_eq = n_incidentstagecode_eq;
        if(!ObjectUtils.isEmpty(this.n_incidentstagecode_eq)){
            this.getSearchCond().eq("incidentstagecode", n_incidentstagecode_eq);
        }
    }
	private String n_casetypecode_eq;//[案例类型]
	public void setN_casetypecode_eq(String n_casetypecode_eq) {
        this.n_casetypecode_eq = n_casetypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_casetypecode_eq)){
            this.getSearchCond().eq("casetypecode", n_casetypecode_eq);
        }
    }
	private String n_severitycode_eq;//[严重性]
	public void setN_severitycode_eq(String n_severitycode_eq) {
        this.n_severitycode_eq = n_severitycode_eq;
        if(!ObjectUtils.isEmpty(this.n_severitycode_eq)){
            this.getSearchCond().eq("severitycode", n_severitycode_eq);
        }
    }
	private String n_prioritycode_eq;//[优先级]
	public void setN_prioritycode_eq(String n_prioritycode_eq) {
        this.n_prioritycode_eq = n_prioritycode_eq;
        if(!ObjectUtils.isEmpty(this.n_prioritycode_eq)){
            this.getSearchCond().eq("prioritycode", n_prioritycode_eq);
        }
    }
	private String n_contractservicelevelcode_eq;//[服务级别]
	public void setN_contractservicelevelcode_eq(String n_contractservicelevelcode_eq) {
        this.n_contractservicelevelcode_eq = n_contractservicelevelcode_eq;
        if(!ObjectUtils.isEmpty(this.n_contractservicelevelcode_eq)){
            this.getSearchCond().eq("contractservicelevelcode", n_contractservicelevelcode_eq);
        }
    }
	private Integer n_statuscode_eq;//[状态描述]
	public void setN_statuscode_eq(Integer n_statuscode_eq) {
        this.n_statuscode_eq = n_statuscode_eq;
        if(!ObjectUtils.isEmpty(this.n_statuscode_eq)){
            this.getSearchCond().eq("statuscode", n_statuscode_eq);
        }
    }
	private String n_servicestage_eq;//[服务阶段]
	public void setN_servicestage_eq(String n_servicestage_eq) {
        this.n_servicestage_eq = n_servicestage_eq;
        if(!ObjectUtils.isEmpty(this.n_servicestage_eq)){
            this.getSearchCond().eq("servicestage", n_servicestage_eq);
        }
    }
	private String n_messagetypecode_eq;//[接收为]
	public void setN_messagetypecode_eq(String n_messagetypecode_eq) {
        this.n_messagetypecode_eq = n_messagetypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_messagetypecode_eq)){
            this.getSearchCond().eq("messagetypecode", n_messagetypecode_eq);
        }
    }
	private Integer n_statecode_eq;//[状态]
	public void setN_statecode_eq(Integer n_statecode_eq) {
        this.n_statecode_eq = n_statecode_eq;
        if(!ObjectUtils.isEmpty(this.n_statecode_eq)){
            this.getSearchCond().eq("statecode", n_statecode_eq);
        }
    }
	private String n_resolvebyslastatus_eq;//[解决方 SLA 状态]
	public void setN_resolvebyslastatus_eq(String n_resolvebyslastatus_eq) {
        this.n_resolvebyslastatus_eq = n_resolvebyslastatus_eq;
        if(!ObjectUtils.isEmpty(this.n_resolvebyslastatus_eq)){
            this.getSearchCond().eq("resolvebyslastatus", n_resolvebyslastatus_eq);
        }
    }
	private String n_caseorigincode_eq;//[起源]
	public void setN_caseorigincode_eq(String n_caseorigincode_eq) {
        this.n_caseorigincode_eq = n_caseorigincode_eq;
        if(!ObjectUtils.isEmpty(this.n_caseorigincode_eq)){
            this.getSearchCond().eq("caseorigincode", n_caseorigincode_eq);
        }
    }
	private String n_customersatisfactioncode_eq;//[满意度]
	public void setN_customersatisfactioncode_eq(String n_customersatisfactioncode_eq) {
        this.n_customersatisfactioncode_eq = n_customersatisfactioncode_eq;
        if(!ObjectUtils.isEmpty(this.n_customersatisfactioncode_eq)){
            this.getSearchCond().eq("customersatisfactioncode", n_customersatisfactioncode_eq);
        }
    }
	private String n_title_like;//[案例标题]
	public void setN_title_like(String n_title_like) {
        this.n_title_like = n_title_like;
        if(!ObjectUtils.isEmpty(this.n_title_like)){
            this.getSearchCond().like("title", n_title_like);
        }
    }
	private String n_existingcasename_eq;//[现有服务案例]
	public void setN_existingcasename_eq(String n_existingcasename_eq) {
        this.n_existingcasename_eq = n_existingcasename_eq;
        if(!ObjectUtils.isEmpty(this.n_existingcasename_eq)){
            this.getSearchCond().eq("existingcasename", n_existingcasename_eq);
        }
    }
	private String n_existingcasename_like;//[现有服务案例]
	public void setN_existingcasename_like(String n_existingcasename_like) {
        this.n_existingcasename_like = n_existingcasename_like;
        if(!ObjectUtils.isEmpty(this.n_existingcasename_like)){
            this.getSearchCond().like("existingcasename", n_existingcasename_like);
        }
    }
	private String n_subjectname_eq;//[主题]
	public void setN_subjectname_eq(String n_subjectname_eq) {
        this.n_subjectname_eq = n_subjectname_eq;
        if(!ObjectUtils.isEmpty(this.n_subjectname_eq)){
            this.getSearchCond().eq("subjectname", n_subjectname_eq);
        }
    }
	private String n_subjectname_like;//[主题]
	public void setN_subjectname_like(String n_subjectname_like) {
        this.n_subjectname_like = n_subjectname_like;
        if(!ObjectUtils.isEmpty(this.n_subjectname_like)){
            this.getSearchCond().like("subjectname", n_subjectname_like);
        }
    }
	private String n_contractdetailname_eq;//[合同子项]
	public void setN_contractdetailname_eq(String n_contractdetailname_eq) {
        this.n_contractdetailname_eq = n_contractdetailname_eq;
        if(!ObjectUtils.isEmpty(this.n_contractdetailname_eq)){
            this.getSearchCond().eq("contractdetailname", n_contractdetailname_eq);
        }
    }
	private String n_contractdetailname_like;//[合同子项]
	public void setN_contractdetailname_like(String n_contractdetailname_like) {
        this.n_contractdetailname_like = n_contractdetailname_like;
        if(!ObjectUtils.isEmpty(this.n_contractdetailname_like)){
            this.getSearchCond().like("contractdetailname", n_contractdetailname_like);
        }
    }
	private String n_parentcasename_eq;//[上级案例]
	public void setN_parentcasename_eq(String n_parentcasename_eq) {
        this.n_parentcasename_eq = n_parentcasename_eq;
        if(!ObjectUtils.isEmpty(this.n_parentcasename_eq)){
            this.getSearchCond().eq("parentcasename", n_parentcasename_eq);
        }
    }
	private String n_parentcasename_like;//[上级案例]
	public void setN_parentcasename_like(String n_parentcasename_like) {
        this.n_parentcasename_like = n_parentcasename_like;
        if(!ObjectUtils.isEmpty(this.n_parentcasename_like)){
            this.getSearchCond().like("parentcasename", n_parentcasename_like);
        }
    }
	private String n_entitlementname_eq;//[权利]
	public void setN_entitlementname_eq(String n_entitlementname_eq) {
        this.n_entitlementname_eq = n_entitlementname_eq;
        if(!ObjectUtils.isEmpty(this.n_entitlementname_eq)){
            this.getSearchCond().eq("entitlementname", n_entitlementname_eq);
        }
    }
	private String n_entitlementname_like;//[权利]
	public void setN_entitlementname_like(String n_entitlementname_like) {
        this.n_entitlementname_like = n_entitlementname_like;
        if(!ObjectUtils.isEmpty(this.n_entitlementname_like)){
            this.getSearchCond().like("entitlementname", n_entitlementname_like);
        }
    }
	private String n_mastername_eq;//[主案例]
	public void setN_mastername_eq(String n_mastername_eq) {
        this.n_mastername_eq = n_mastername_eq;
        if(!ObjectUtils.isEmpty(this.n_mastername_eq)){
            this.getSearchCond().eq("mastername", n_mastername_eq);
        }
    }
	private String n_mastername_like;//[主案例]
	public void setN_mastername_like(String n_mastername_like) {
        this.n_mastername_like = n_mastername_like;
        if(!ObjectUtils.isEmpty(this.n_mastername_like)){
            this.getSearchCond().like("mastername", n_mastername_like);
        }
    }
	private String n_contractname_eq;//[合同]
	public void setN_contractname_eq(String n_contractname_eq) {
        this.n_contractname_eq = n_contractname_eq;
        if(!ObjectUtils.isEmpty(this.n_contractname_eq)){
            this.getSearchCond().eq("contractname", n_contractname_eq);
        }
    }
	private String n_contractname_like;//[合同]
	public void setN_contractname_like(String n_contractname_like) {
        this.n_contractname_like = n_contractname_like;
        if(!ObjectUtils.isEmpty(this.n_contractname_like)){
            this.getSearchCond().like("contractname", n_contractname_like);
        }
    }
	private String n_customername_eq;//[客户]
	public void setN_customername_eq(String n_customername_eq) {
        this.n_customername_eq = n_customername_eq;
        if(!ObjectUtils.isEmpty(this.n_customername_eq)){
            this.getSearchCond().eq("customername", n_customername_eq);
        }
    }
	private String n_customername_like;//[客户]
	public void setN_customername_like(String n_customername_like) {
        this.n_customername_like = n_customername_like;
        if(!ObjectUtils.isEmpty(this.n_customername_like)){
            this.getSearchCond().like("customername", n_customername_like);
        }
    }
	private String n_currencyname_eq;//[货币]
	public void setN_currencyname_eq(String n_currencyname_eq) {
        this.n_currencyname_eq = n_currencyname_eq;
        if(!ObjectUtils.isEmpty(this.n_currencyname_eq)){
            this.getSearchCond().eq("currencyname", n_currencyname_eq);
        }
    }
	private String n_currencyname_like;//[货币]
	public void setN_currencyname_like(String n_currencyname_like) {
        this.n_currencyname_like = n_currencyname_like;
        if(!ObjectUtils.isEmpty(this.n_currencyname_like)){
            this.getSearchCond().like("currencyname", n_currencyname_like);
        }
    }
	private String n_productname_eq;//[产品]
	public void setN_productname_eq(String n_productname_eq) {
        this.n_productname_eq = n_productname_eq;
        if(!ObjectUtils.isEmpty(this.n_productname_eq)){
            this.getSearchCond().eq("productname", n_productname_eq);
        }
    }
	private String n_productname_like;//[产品]
	public void setN_productname_like(String n_productname_like) {
        this.n_productname_like = n_productname_like;
        if(!ObjectUtils.isEmpty(this.n_productname_like)){
            this.getSearchCond().like("productname", n_productname_like);
        }
    }
	private String n_slaname_eq;//[SLA]
	public void setN_slaname_eq(String n_slaname_eq) {
        this.n_slaname_eq = n_slaname_eq;
        if(!ObjectUtils.isEmpty(this.n_slaname_eq)){
            this.getSearchCond().eq("slaname", n_slaname_eq);
        }
    }
	private String n_slaname_like;//[SLA]
	public void setN_slaname_like(String n_slaname_like) {
        this.n_slaname_like = n_slaname_like;
        if(!ObjectUtils.isEmpty(this.n_slaname_like)){
            this.getSearchCond().like("slaname", n_slaname_like);
        }
    }
	private String n_firstresponsebykpiname_eq;//[第一个响应者（按KPI）]
	public void setN_firstresponsebykpiname_eq(String n_firstresponsebykpiname_eq) {
        this.n_firstresponsebykpiname_eq = n_firstresponsebykpiname_eq;
        if(!ObjectUtils.isEmpty(this.n_firstresponsebykpiname_eq)){
            this.getSearchCond().eq("firstresponsebykpiname", n_firstresponsebykpiname_eq);
        }
    }
	private String n_firstresponsebykpiname_like;//[第一个响应者（按KPI）]
	public void setN_firstresponsebykpiname_like(String n_firstresponsebykpiname_like) {
        this.n_firstresponsebykpiname_like = n_firstresponsebykpiname_like;
        if(!ObjectUtils.isEmpty(this.n_firstresponsebykpiname_like)){
            this.getSearchCond().like("firstresponsebykpiname", n_firstresponsebykpiname_like);
        }
    }
	private String n_resolvebykpiname_eq;//[解决方KPI]
	public void setN_resolvebykpiname_eq(String n_resolvebykpiname_eq) {
        this.n_resolvebykpiname_eq = n_resolvebykpiname_eq;
        if(!ObjectUtils.isEmpty(this.n_resolvebykpiname_eq)){
            this.getSearchCond().eq("resolvebykpiname", n_resolvebykpiname_eq);
        }
    }
	private String n_resolvebykpiname_like;//[解决方KPI]
	public void setN_resolvebykpiname_like(String n_resolvebykpiname_like) {
        this.n_resolvebykpiname_like = n_resolvebykpiname_like;
        if(!ObjectUtils.isEmpty(this.n_resolvebykpiname_like)){
            this.getSearchCond().like("resolvebykpiname", n_resolvebykpiname_like);
        }
    }
	private String n_primarycontactname_eq;//[联系人]
	public void setN_primarycontactname_eq(String n_primarycontactname_eq) {
        this.n_primarycontactname_eq = n_primarycontactname_eq;
        if(!ObjectUtils.isEmpty(this.n_primarycontactname_eq)){
            this.getSearchCond().eq("primarycontactname", n_primarycontactname_eq);
        }
    }
	private String n_primarycontactname_like;//[联系人]
	public void setN_primarycontactname_like(String n_primarycontactname_like) {
        this.n_primarycontactname_like = n_primarycontactname_like;
        if(!ObjectUtils.isEmpty(this.n_primarycontactname_like)){
            this.getSearchCond().like("primarycontactname", n_primarycontactname_like);
        }
    }
	private String n_responsiblecontactname_eq;//[责任联系人]
	public void setN_responsiblecontactname_eq(String n_responsiblecontactname_eq) {
        this.n_responsiblecontactname_eq = n_responsiblecontactname_eq;
        if(!ObjectUtils.isEmpty(this.n_responsiblecontactname_eq)){
            this.getSearchCond().eq("responsiblecontactname", n_responsiblecontactname_eq);
        }
    }
	private String n_responsiblecontactname_like;//[责任联系人]
	public void setN_responsiblecontactname_like(String n_responsiblecontactname_like) {
        this.n_responsiblecontactname_like = n_responsiblecontactname_like;
        if(!ObjectUtils.isEmpty(this.n_responsiblecontactname_like)){
            this.getSearchCond().like("responsiblecontactname", n_responsiblecontactname_like);
        }
    }
	private String n_entitlementid_eq;//[权利]
	public void setN_entitlementid_eq(String n_entitlementid_eq) {
        this.n_entitlementid_eq = n_entitlementid_eq;
        if(!ObjectUtils.isEmpty(this.n_entitlementid_eq)){
            this.getSearchCond().eq("entitlementid", n_entitlementid_eq);
        }
    }
	private String n_customerid_eq;//[客户]
	public void setN_customerid_eq(String n_customerid_eq) {
        this.n_customerid_eq = n_customerid_eq;
        if(!ObjectUtils.isEmpty(this.n_customerid_eq)){
            this.getSearchCond().eq("customerid", n_customerid_eq);
        }
    }
	private String n_firstresponsebykpiid_eq;//[第一个响应者(按 KPI)]
	public void setN_firstresponsebykpiid_eq(String n_firstresponsebykpiid_eq) {
        this.n_firstresponsebykpiid_eq = n_firstresponsebykpiid_eq;
        if(!ObjectUtils.isEmpty(this.n_firstresponsebykpiid_eq)){
            this.getSearchCond().eq("firstresponsebykpiid", n_firstresponsebykpiid_eq);
        }
    }
	private String n_slaid_eq;//[SLA]
	public void setN_slaid_eq(String n_slaid_eq) {
        this.n_slaid_eq = n_slaid_eq;
        if(!ObjectUtils.isEmpty(this.n_slaid_eq)){
            this.getSearchCond().eq("slaid", n_slaid_eq);
        }
    }
	private String n_existingcase_eq;//[现有服务案例]
	public void setN_existingcase_eq(String n_existingcase_eq) {
        this.n_existingcase_eq = n_existingcase_eq;
        if(!ObjectUtils.isEmpty(this.n_existingcase_eq)){
            this.getSearchCond().eq("existingcase", n_existingcase_eq);
        }
    }
	private String n_productid_eq;//[产品]
	public void setN_productid_eq(String n_productid_eq) {
        this.n_productid_eq = n_productid_eq;
        if(!ObjectUtils.isEmpty(this.n_productid_eq)){
            this.getSearchCond().eq("productid", n_productid_eq);
        }
    }
	private String n_contractid_eq;//[合同]
	public void setN_contractid_eq(String n_contractid_eq) {
        this.n_contractid_eq = n_contractid_eq;
        if(!ObjectUtils.isEmpty(this.n_contractid_eq)){
            this.getSearchCond().eq("contractid", n_contractid_eq);
        }
    }
	private String n_responsiblecontactid_eq;//[责任联系人]
	public void setN_responsiblecontactid_eq(String n_responsiblecontactid_eq) {
        this.n_responsiblecontactid_eq = n_responsiblecontactid_eq;
        if(!ObjectUtils.isEmpty(this.n_responsiblecontactid_eq)){
            this.getSearchCond().eq("responsiblecontactid", n_responsiblecontactid_eq);
        }
    }
	private String n_resolvebykpiid_eq;//[解决方 KPI]
	public void setN_resolvebykpiid_eq(String n_resolvebykpiid_eq) {
        this.n_resolvebykpiid_eq = n_resolvebykpiid_eq;
        if(!ObjectUtils.isEmpty(this.n_resolvebykpiid_eq)){
            this.getSearchCond().eq("resolvebykpiid", n_resolvebykpiid_eq);
        }
    }
	private String n_contractdetailid_eq;//[合同子项]
	public void setN_contractdetailid_eq(String n_contractdetailid_eq) {
        this.n_contractdetailid_eq = n_contractdetailid_eq;
        if(!ObjectUtils.isEmpty(this.n_contractdetailid_eq)){
            this.getSearchCond().eq("contractdetailid", n_contractdetailid_eq);
        }
    }
	private String n_subjectid_eq;//[主题]
	public void setN_subjectid_eq(String n_subjectid_eq) {
        this.n_subjectid_eq = n_subjectid_eq;
        if(!ObjectUtils.isEmpty(this.n_subjectid_eq)){
            this.getSearchCond().eq("subjectid", n_subjectid_eq);
        }
    }
	private String n_masterid_eq;//[主案例]
	public void setN_masterid_eq(String n_masterid_eq) {
        this.n_masterid_eq = n_masterid_eq;
        if(!ObjectUtils.isEmpty(this.n_masterid_eq)){
            this.getSearchCond().eq("masterid", n_masterid_eq);
        }
    }
	private String n_parentcaseid_eq;//[上级案例]
	public void setN_parentcaseid_eq(String n_parentcaseid_eq) {
        this.n_parentcaseid_eq = n_parentcaseid_eq;
        if(!ObjectUtils.isEmpty(this.n_parentcaseid_eq)){
            this.getSearchCond().eq("parentcaseid", n_parentcaseid_eq);
        }
    }
	private String n_primarycontactid_eq;//[联系人]
	public void setN_primarycontactid_eq(String n_primarycontactid_eq) {
        this.n_primarycontactid_eq = n_primarycontactid_eq;
        if(!ObjectUtils.isEmpty(this.n_primarycontactid_eq)){
            this.getSearchCond().eq("primarycontactid", n_primarycontactid_eq);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("title", query)   
            );
		 }
	}
}




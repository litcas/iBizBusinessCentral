package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.TimezoneDefinition;
import cn.ibizlab.businesscentral.core.base.filter.TimezoneDefinitionSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[TimezoneDefinition] 服务对象接口
 */
public interface ITimezoneDefinitionService extends IService<TimezoneDefinition>{

    boolean create(TimezoneDefinition et) ;
    void createBatch(List<TimezoneDefinition> list) ;
    boolean update(TimezoneDefinition et) ;
    void updateBatch(List<TimezoneDefinition> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    TimezoneDefinition get(String key) ;
    TimezoneDefinition getDraft(TimezoneDefinition et) ;
    boolean checkKey(TimezoneDefinition et) ;
    boolean save(TimezoneDefinition et) ;
    void saveBatch(List<TimezoneDefinition> list) ;
    Page<TimezoneDefinition> searchDefault(TimezoneDefinitionSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<TimezoneDefinition> getTimezonedefinitionByIds(List<String> ids) ;
    List<TimezoneDefinition> getTimezonedefinitionByEntities(List<TimezoneDefinition> entities) ;
}



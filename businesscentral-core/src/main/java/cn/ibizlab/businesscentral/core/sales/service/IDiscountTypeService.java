package cn.ibizlab.businesscentral.core.sales.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.sales.domain.DiscountType;
import cn.ibizlab.businesscentral.core.sales.filter.DiscountTypeSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[DiscountType] 服务对象接口
 */
public interface IDiscountTypeService extends IService<DiscountType>{

    boolean create(DiscountType et) ;
    void createBatch(List<DiscountType> list) ;
    boolean update(DiscountType et) ;
    void updateBatch(List<DiscountType> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    DiscountType get(String key) ;
    DiscountType getDraft(DiscountType et) ;
    boolean checkKey(DiscountType et) ;
    boolean save(DiscountType et) ;
    void saveBatch(List<DiscountType> list) ;
    Page<DiscountType> searchDefault(DiscountTypeSearchContext context) ;
    List<DiscountType> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<DiscountType> getDiscounttypeByIds(List<String> ids) ;
    List<DiscountType> getDiscounttypeByEntities(List<DiscountType> entities) ;
}



package cn.ibizlab.businesscentral.core.service.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.service.domain.ContractDetail;
/**
 * 关系型数据实体[ContractDetail] 查询条件对象
 */
@Slf4j
@Data
public class ContractDetailSearchContext extends QueryWrapperContext<ContractDetail> {

	private String n_servicecontractunitscode_eq;//[服务合同部门]
	public void setN_servicecontractunitscode_eq(String n_servicecontractunitscode_eq) {
        this.n_servicecontractunitscode_eq = n_servicecontractunitscode_eq;
        if(!ObjectUtils.isEmpty(this.n_servicecontractunitscode_eq)){
            this.getSearchCond().eq("servicecontractunitscode", n_servicecontractunitscode_eq);
        }
    }
	private String n_contractstatecode_eq;//[合同状态]
	public void setN_contractstatecode_eq(String n_contractstatecode_eq) {
        this.n_contractstatecode_eq = n_contractstatecode_eq;
        if(!ObjectUtils.isEmpty(this.n_contractstatecode_eq)){
            this.getSearchCond().eq("contractstatecode", n_contractstatecode_eq);
        }
    }
	private Integer n_statecode_eq;//[状态]
	public void setN_statecode_eq(Integer n_statecode_eq) {
        this.n_statecode_eq = n_statecode_eq;
        if(!ObjectUtils.isEmpty(this.n_statecode_eq)){
            this.getSearchCond().eq("statecode", n_statecode_eq);
        }
    }
	private String n_title_like;//[标题]
	public void setN_title_like(String n_title_like) {
        this.n_title_like = n_title_like;
        if(!ObjectUtils.isEmpty(this.n_title_like)){
            this.getSearchCond().like("title", n_title_like);
        }
    }
	private Integer n_statuscode_eq;//[状态描述]
	public void setN_statuscode_eq(Integer n_statuscode_eq) {
        this.n_statuscode_eq = n_statuscode_eq;
        if(!ObjectUtils.isEmpty(this.n_statuscode_eq)){
            this.getSearchCond().eq("statuscode", n_statuscode_eq);
        }
    }
	private String n_serviceaddress_eq;//[位置]
	public void setN_serviceaddress_eq(String n_serviceaddress_eq) {
        this.n_serviceaddress_eq = n_serviceaddress_eq;
        if(!ObjectUtils.isEmpty(this.n_serviceaddress_eq)){
            this.getSearchCond().eq("serviceaddress", n_serviceaddress_eq);
        }
    }
	private String n_productid_eq;//[产品]
	public void setN_productid_eq(String n_productid_eq) {
        this.n_productid_eq = n_productid_eq;
        if(!ObjectUtils.isEmpty(this.n_productid_eq)){
            this.getSearchCond().eq("productid", n_productid_eq);
        }
    }
	private String n_uomscheduleid_eq;//[单位计划]
	public void setN_uomscheduleid_eq(String n_uomscheduleid_eq) {
        this.n_uomscheduleid_eq = n_uomscheduleid_eq;
        if(!ObjectUtils.isEmpty(this.n_uomscheduleid_eq)){
            this.getSearchCond().eq("uomscheduleid", n_uomscheduleid_eq);
        }
    }
	private String n_contractid_eq;//[合同]
	public void setN_contractid_eq(String n_contractid_eq) {
        this.n_contractid_eq = n_contractid_eq;
        if(!ObjectUtils.isEmpty(this.n_contractid_eq)){
            this.getSearchCond().eq("contractid", n_contractid_eq);
        }
    }
	private String n_uomid_eq;//[计价单位]
	public void setN_uomid_eq(String n_uomid_eq) {
        this.n_uomid_eq = n_uomid_eq;
        if(!ObjectUtils.isEmpty(this.n_uomid_eq)){
            this.getSearchCond().eq("uomid", n_uomid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("title", query)   
            );
		 }
	}
}




package cn.ibizlab.businesscentral.core.product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.product.domain.PriceLevel;
import cn.ibizlab.businesscentral.core.product.filter.PriceLevelSearchContext;
import cn.ibizlab.businesscentral.core.product.service.IPriceLevelService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.product.mapper.PriceLevelMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[价目表] 服务对象接口实现
 */
@Slf4j
@Service("PriceLevelServiceImpl")
public class PriceLevelServiceImpl extends ServiceImpl<PriceLevelMapper, PriceLevel> implements IPriceLevelService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IAccountService accountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.marketing.service.ICampaignService campaignService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IContactService contactService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.finance.service.IInvoiceService invoiceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IOpportunityService opportunityService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.product.service.IProductPriceLevelService productpricelevelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.product.service.IProductService productService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IQuoteService quoteService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.ISalesOrderService salesorderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(PriceLevel et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getPricelevelid()),et);
        return true;
    }

    @Override
    public void createBatch(List<PriceLevel> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(PriceLevel et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("pricelevelid",et.getPricelevelid())))
            return false;
        CachedBeanCopier.copy(get(et.getPricelevelid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<PriceLevel> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public PriceLevel get(String key) {
        PriceLevel et = getById(key);
        if(et==null){
            et=new PriceLevel();
            et.setPricelevelid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public PriceLevel getDraft(PriceLevel et) {
        return et;
    }

    @Override
    public boolean checkKey(PriceLevel et) {
        return (!ObjectUtils.isEmpty(et.getPricelevelid()))&&(!Objects.isNull(this.getById(et.getPricelevelid())));
    }
    @Override
    @Transactional
    public boolean save(PriceLevel et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(PriceLevel et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<PriceLevel> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<PriceLevel> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<PriceLevel> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<PriceLevel>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<PriceLevel> searchDefault(PriceLevelSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<PriceLevel> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<PriceLevel>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<PriceLevel> getPricelevelByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<PriceLevel> getPricelevelByEntities(List<PriceLevel> entities) {
        List ids =new ArrayList();
        for(PriceLevel entity : entities){
            Serializable id=entity.getPricelevelid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




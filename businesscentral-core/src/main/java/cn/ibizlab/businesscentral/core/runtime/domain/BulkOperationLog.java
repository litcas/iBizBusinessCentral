package cn.ibizlab.businesscentral.core.runtime.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[批量操作日志]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "BULKOPERATIONLOG",resultMap = "BulkOperationLogResultMap")
public class BulkOperationLog extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 错误号
     */
    @TableField(value = "errornumber")
    @JSONField(name = "errornumber")
    @JsonProperty("errornumber")
    private Integer errornumber;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * CreatedObjectTypeCode
     */
    @TableField(value = "createdobjecttypecode")
    @JSONField(name = "createdobjecttypecode")
    @JsonProperty("createdobjecttypecode")
    private String createdobjecttypecode;
    /**
     * 负责人
     */
    @TableField(value = "ownerid")
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;
    /**
     * 负责人
     */
    @TableField(value = "ownername")
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;
    /**
     * 操作人
     */
    @TableField(value = "bulkoperationname")
    @JSONField(name = "bulkoperationname")
    @JsonProperty("bulkoperationname")
    private String bulkoperationname;
    /**
     * Time Zone Rule Version Number
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * Record Created On
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * BulkOperationType
     */
    @TableField(value = "bulkoperationtype")
    @JSONField(name = "bulkoperationtype")
    @JsonProperty("bulkoperationtype")
    private String bulkoperationtype;
    /**
     * UTC Conversion Time Zone Code
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * ErrorDescriptionFormatted
     */
    @TableField(value = "errordescriptionformatted")
    @JSONField(name = "errordescriptionformatted")
    @JsonProperty("errordescriptionformatted")
    private String errordescriptionformatted;
    /**
     * ErrorNumberFormatted
     */
    @TableField(value = "errornumberformatted")
    @JSONField(name = "errornumberformatted")
    @JsonProperty("errornumberformatted")
    private String errornumberformatted;
    /**
     * 负责人类型
     */
    @TableField(value = "ownertype")
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;
    /**
     * 被排除的客户
     */
    @TableField(value = "regardingobjectid")
    @JSONField(name = "regardingobjectid")
    @JsonProperty("regardingobjectid")
    private String regardingobjectid;
    /**
     * 在以下行失败
     */
    @TableField(value = "additionalinfo")
    @JSONField(name = "additionalinfo")
    @JsonProperty("additionalinfo")
    private String additionalinfo;
    /**
     * Import Sequence Number
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * 批量操作日志编号
     */
    @DEField(isKeyField=true)
    @TableId(value= "bulkoperationlogid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "bulkoperationlogid")
    @JsonProperty("bulkoperationlogid")
    private String bulkoperationlogid;
    /**
     * 关于
     */
    @TableField(value = "regardingobjecttypecode")
    @JSONField(name = "regardingobjecttypecode")
    @JsonProperty("regardingobjecttypecode")
    private String regardingobjecttypecode;
    /**
     * Version Number
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 关于
     */
    @TableField(value = "regardingobjectname")
    @JSONField(name = "regardingobjectname")
    @JsonProperty("regardingobjectname")
    private String regardingobjectname;
    /**
     * 创建的对象
     */
    @TableField(value = "createdobjectid")
    @JSONField(name = "createdobjectid")
    @JsonProperty("createdobjectid")
    private String createdobjectid;
    /**
     * 建立者
     */
    @TableField(value = "createdobjectname")
    @JSONField(name = "createdobjectname")
    @JsonProperty("createdobjectname")
    private String createdobjectname;
    /**
     * 批除操作日志名称
     */
    @TableField(value = "bulkoperationlogname")
    @JSONField(name = "bulkoperationlogname")
    @JsonProperty("bulkoperationlogname")
    private String bulkoperationlogname;
    /**
     * 批量操作号
     */
    @TableField(value = "bulkoperationid")
    @JSONField(name = "bulkoperationid")
    @JsonProperty("bulkoperationid")
    private String bulkoperationid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.ActivityPointer bulkoperation;



    /**
     * 设置 [错误号]
     */
    public void setErrornumber(Integer errornumber){
        this.errornumber = errornumber ;
        this.modify("errornumber",errornumber);
    }

    /**
     * 设置 [CreatedObjectTypeCode]
     */
    public void setCreatedobjecttypecode(String createdobjecttypecode){
        this.createdobjecttypecode = createdobjecttypecode ;
        this.modify("createdobjecttypecode",createdobjecttypecode);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnerid(String ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnername(String ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [操作人]
     */
    public void setBulkoperationname(String bulkoperationname){
        this.bulkoperationname = bulkoperationname ;
        this.modify("bulkoperationname",bulkoperationname);
    }

    /**
     * 设置 [Time Zone Rule Version Number]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [Record Created On]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [Record Created On]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [BulkOperationType]
     */
    public void setBulkoperationtype(String bulkoperationtype){
        this.bulkoperationtype = bulkoperationtype ;
        this.modify("bulkoperationtype",bulkoperationtype);
    }

    /**
     * 设置 [UTC Conversion Time Zone Code]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [ErrorDescriptionFormatted]
     */
    public void setErrordescriptionformatted(String errordescriptionformatted){
        this.errordescriptionformatted = errordescriptionformatted ;
        this.modify("errordescriptionformatted",errordescriptionformatted);
    }

    /**
     * 设置 [ErrorNumberFormatted]
     */
    public void setErrornumberformatted(String errornumberformatted){
        this.errornumberformatted = errornumberformatted ;
        this.modify("errornumberformatted",errornumberformatted);
    }

    /**
     * 设置 [负责人类型]
     */
    public void setOwnertype(String ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [被排除的客户]
     */
    public void setRegardingobjectid(String regardingobjectid){
        this.regardingobjectid = regardingobjectid ;
        this.modify("regardingobjectid",regardingobjectid);
    }

    /**
     * 设置 [在以下行失败]
     */
    public void setAdditionalinfo(String additionalinfo){
        this.additionalinfo = additionalinfo ;
        this.modify("additionalinfo",additionalinfo);
    }

    /**
     * 设置 [Import Sequence Number]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [关于]
     */
    public void setRegardingobjecttypecode(String regardingobjecttypecode){
        this.regardingobjecttypecode = regardingobjecttypecode ;
        this.modify("regardingobjecttypecode",regardingobjecttypecode);
    }

    /**
     * 设置 [Version Number]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [关于]
     */
    public void setRegardingobjectname(String regardingobjectname){
        this.regardingobjectname = regardingobjectname ;
        this.modify("regardingobjectname",regardingobjectname);
    }

    /**
     * 设置 [创建的对象]
     */
    public void setCreatedobjectid(String createdobjectid){
        this.createdobjectid = createdobjectid ;
        this.modify("createdobjectid",createdobjectid);
    }

    /**
     * 设置 [建立者]
     */
    public void setCreatedobjectname(String createdobjectname){
        this.createdobjectname = createdobjectname ;
        this.modify("createdobjectname",createdobjectname);
    }

    /**
     * 设置 [批除操作日志名称]
     */
    public void setBulkoperationlogname(String bulkoperationlogname){
        this.bulkoperationlogname = bulkoperationlogname ;
        this.modify("bulkoperationlogname",bulkoperationlogname);
    }

    /**
     * 设置 [批量操作号]
     */
    public void setBulkoperationid(String bulkoperationid){
        this.bulkoperationid = bulkoperationid ;
        this.modify("bulkoperationid",bulkoperationid);
    }


}



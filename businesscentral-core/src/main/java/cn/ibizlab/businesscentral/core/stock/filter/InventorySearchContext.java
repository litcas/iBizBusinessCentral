package cn.ibizlab.businesscentral.core.stock.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.stock.domain.Inventory;
/**
 * 关系型数据实体[Inventory] 查询条件对象
 */
@Slf4j
@Data
public class InventorySearchContext extends QueryWrapperContext<Inventory> {

	private String n_inventoryname_like;//[库存名称]
	public void setN_inventoryname_like(String n_inventoryname_like) {
        this.n_inventoryname_like = n_inventoryname_like;
        if(!ObjectUtils.isEmpty(this.n_inventoryname_like)){
            this.getSearchCond().like("inventoryname", n_inventoryname_like);
        }
    }
	private String n_uomname_eq;//[计价单位]
	public void setN_uomname_eq(String n_uomname_eq) {
        this.n_uomname_eq = n_uomname_eq;
        if(!ObjectUtils.isEmpty(this.n_uomname_eq)){
            this.getSearchCond().eq("uomname", n_uomname_eq);
        }
    }
	private String n_uomname_like;//[计价单位]
	public void setN_uomname_like(String n_uomname_like) {
        this.n_uomname_like = n_uomname_like;
        if(!ObjectUtils.isEmpty(this.n_uomname_like)){
            this.getSearchCond().like("uomname", n_uomname_like);
        }
    }
	private String n_productname_eq;//[产品]
	public void setN_productname_eq(String n_productname_eq) {
        this.n_productname_eq = n_productname_eq;
        if(!ObjectUtils.isEmpty(this.n_productname_eq)){
            this.getSearchCond().eq("productname", n_productname_eq);
        }
    }
	private String n_productname_like;//[产品]
	public void setN_productname_like(String n_productname_like) {
        this.n_productname_like = n_productname_like;
        if(!ObjectUtils.isEmpty(this.n_productname_like)){
            this.getSearchCond().like("productname", n_productname_like);
        }
    }
	private String n_productid_eq;//[产品]
	public void setN_productid_eq(String n_productid_eq) {
        this.n_productid_eq = n_productid_eq;
        if(!ObjectUtils.isEmpty(this.n_productid_eq)){
            this.getSearchCond().eq("productid", n_productid_eq);
        }
    }
	private String n_uomid_eq;//[计价单位]
	public void setN_uomid_eq(String n_uomid_eq) {
        this.n_uomid_eq = n_uomid_eq;
        if(!ObjectUtils.isEmpty(this.n_uomid_eq)){
            this.getSearchCond().eq("uomid", n_uomid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("inventoryname", query)   
            );
		 }
	}
}




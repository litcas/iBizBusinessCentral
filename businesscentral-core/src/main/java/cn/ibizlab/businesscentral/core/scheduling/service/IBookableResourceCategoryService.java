package cn.ibizlab.businesscentral.core.scheduling.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.scheduling.domain.BookableResourceCategory;
import cn.ibizlab.businesscentral.core.scheduling.filter.BookableResourceCategorySearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[BookableResourceCategory] 服务对象接口
 */
public interface IBookableResourceCategoryService extends IService<BookableResourceCategory>{

    boolean create(BookableResourceCategory et) ;
    void createBatch(List<BookableResourceCategory> list) ;
    boolean update(BookableResourceCategory et) ;
    void updateBatch(List<BookableResourceCategory> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    BookableResourceCategory get(String key) ;
    BookableResourceCategory getDraft(BookableResourceCategory et) ;
    boolean checkKey(BookableResourceCategory et) ;
    boolean save(BookableResourceCategory et) ;
    void saveBatch(List<BookableResourceCategory> list) ;
    Page<BookableResourceCategory> searchDefault(BookableResourceCategorySearchContext context) ;
    List<BookableResourceCategory> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<BookableResourceCategory> getBookableresourcecategoryByIds(List<String> ids) ;
    List<BookableResourceCategory> getBookableresourcecategoryByEntities(List<BookableResourceCategory> entities) ;
}



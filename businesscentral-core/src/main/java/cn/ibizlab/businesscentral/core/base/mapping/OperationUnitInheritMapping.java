

package cn.ibizlab.businesscentral.core.base.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.base.domain.OperationUnit;
import cn.ibizlab.businesscentral.core.base.domain.Organization;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface OperationUnitInheritMapping {

    @Mappings({
        @Mapping(source ="operationunitid",target = "organizationid"),
        @Mapping(source ="operationunitname",target = "organizationname"),
        @Mapping(target ="focusNull",ignore = true),
    })
    Organization toOrganization(OperationUnit operationunit);

    @Mappings({
        @Mapping(source ="organizationid" ,target = "operationunitid"),
        @Mapping(source ="organizationname" ,target = "operationunitname"),
        @Mapping(target ="focusNull",ignore = true),
    })
    OperationUnit toOperationunit(Organization organization);

    List<Organization> toOrganization(List<OperationUnit> operationunit);

    List<OperationUnit> toOperationunit(List<Organization> organization);

}



package cn.ibizlab.businesscentral.core.product.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.product.domain.PriceLevel;
/**
 * 关系型数据实体[PriceLevel] 查询条件对象
 */
@Slf4j
@Data
public class PriceLevelSearchContext extends QueryWrapperContext<PriceLevel> {

	private Integer n_statecode_eq;//[状态]
	public void setN_statecode_eq(Integer n_statecode_eq) {
        this.n_statecode_eq = n_statecode_eq;
        if(!ObjectUtils.isEmpty(this.n_statecode_eq)){
            this.getSearchCond().eq("statecode", n_statecode_eq);
        }
    }
	private String n_paymentmethodcode_eq;//[付款方式]
	public void setN_paymentmethodcode_eq(String n_paymentmethodcode_eq) {
        this.n_paymentmethodcode_eq = n_paymentmethodcode_eq;
        if(!ObjectUtils.isEmpty(this.n_paymentmethodcode_eq)){
            this.getSearchCond().eq("paymentmethodcode", n_paymentmethodcode_eq);
        }
    }
	private String n_shippingmethodcode_eq;//[送货方式]
	public void setN_shippingmethodcode_eq(String n_shippingmethodcode_eq) {
        this.n_shippingmethodcode_eq = n_shippingmethodcode_eq;
        if(!ObjectUtils.isEmpty(this.n_shippingmethodcode_eq)){
            this.getSearchCond().eq("shippingmethodcode", n_shippingmethodcode_eq);
        }
    }
	private String n_pricelevelname_like;//[价目表名称]
	public void setN_pricelevelname_like(String n_pricelevelname_like) {
        this.n_pricelevelname_like = n_pricelevelname_like;
        if(!ObjectUtils.isEmpty(this.n_pricelevelname_like)){
            this.getSearchCond().like("pricelevelname", n_pricelevelname_like);
        }
    }
	private Integer n_statuscode_eq;//[状态描述]
	public void setN_statuscode_eq(Integer n_statuscode_eq) {
        this.n_statuscode_eq = n_statuscode_eq;
        if(!ObjectUtils.isEmpty(this.n_statuscode_eq)){
            this.getSearchCond().eq("statuscode", n_statuscode_eq);
        }
    }
	private String n_freighttermscode_eq;//[货运条款]
	public void setN_freighttermscode_eq(String n_freighttermscode_eq) {
        this.n_freighttermscode_eq = n_freighttermscode_eq;
        if(!ObjectUtils.isEmpty(this.n_freighttermscode_eq)){
            this.getSearchCond().eq("freighttermscode", n_freighttermscode_eq);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("pricelevelname", query)   
            );
		 }
	}
}




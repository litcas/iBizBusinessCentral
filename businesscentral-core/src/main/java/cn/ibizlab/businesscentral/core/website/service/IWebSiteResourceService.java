package cn.ibizlab.businesscentral.core.website.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.website.domain.WebSiteResource;
import cn.ibizlab.businesscentral.core.website.filter.WebSiteResourceSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[WebSiteResource] 服务对象接口
 */
public interface IWebSiteResourceService extends IService<WebSiteResource>{

    boolean create(WebSiteResource et) ;
    void createBatch(List<WebSiteResource> list) ;
    boolean update(WebSiteResource et) ;
    void updateBatch(List<WebSiteResource> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    WebSiteResource get(String key) ;
    WebSiteResource getDraft(WebSiteResource et) ;
    boolean checkKey(WebSiteResource et) ;
    boolean save(WebSiteResource et) ;
    void saveBatch(List<WebSiteResource> list) ;
    Page<WebSiteResource> searchDefault(WebSiteResourceSearchContext context) ;
    List<WebSiteResource> selectByWebsiteid(String websiteid) ;
    void removeByWebsiteid(String websiteid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<WebSiteResource> getWebsiteresourceByIds(List<String> ids) ;
    List<WebSiteResource> getWebsiteresourceByEntities(List<WebSiteResource> entities) ;
}



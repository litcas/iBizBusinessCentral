package cn.ibizlab.businesscentral.core.service.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[合同子项]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "CONTRACTDETAIL",resultMap = "ContractDetailResultMap")
public class ContractDetail extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 服务合同部门
     */
    @TableField(value = "servicecontractunitscode")
    @JSONField(name = "servicecontractunitscode")
    @JsonProperty("servicecontractunitscode")
    private String servicecontractunitscode;
    /**
     * 服务配额平均数
     */
    @TableField(value = "allotmentsoverage")
    @JSONField(name = "allotmentsoverage")
    @JsonProperty("allotmentsoverage")
    private Integer allotmentsoverage;
    /**
     * 合同状态
     */
    @TableField(value = "contractstatecode")
    @JSONField(name = "contractstatecode")
    @JsonProperty("contractstatecode")
    private String contractstatecode;
    /**
     * 状态
     */
    @TableField(value = "statecode")
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;
    /**
     * 净价
     */
    @TableField(value = "net")
    @JSONField(name = "net")
    @JsonProperty("net")
    private BigDecimal net;
    /**
     * 费率 (Base)
     */
    @DEField(name = "rate_base")
    @TableField(value = "rate_base")
    @JSONField(name = "rate_base")
    @JsonProperty("rate_base")
    private BigDecimal rateBase;
    /**
     * 总价
     */
    @TableField(value = "price")
    @JSONField(name = "price")
    @JsonProperty("price")
    private BigDecimal price;
    /**
     * Time Zone Rule Version Number
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 客户
     */
    @TableField(value = "customerid")
    @JSONField(name = "customerid")
    @JsonProperty("customerid")
    private String customerid;
    /**
     * 标题
     */
    @TableField(value = "title")
    @JSONField(name = "title")
    @JsonProperty("title")
    private String title;
    /**
     * 明细项目顺序
     */
    @TableField(value = "lineitemorder")
    @JSONField(name = "lineitemorder")
    @JsonProperty("lineitemorder")
    private Integer lineitemorder;
    /**
     * 剩余服务配额
     */
    @TableField(value = "allotmentsremaining")
    @JSONField(name = "allotmentsremaining")
    @JsonProperty("allotmentsremaining")
    private Integer allotmentsremaining;
    /**
     * ServiceAddressName
     */
    @TableField(value = "serviceaddressname")
    @JSONField(name = "serviceaddressname")
    @JsonProperty("serviceaddressname")
    private String serviceaddressname;
    /**
     * 客户类型
     */
    @TableField(value = "customertype")
    @JSONField(name = "customertype")
    @JsonProperty("customertype")
    private String customertype;
    /**
     * 总服务配额
     */
    @TableField(value = "totalallotments")
    @JSONField(name = "totalallotments")
    @JsonProperty("totalallotments")
    private Integer totalallotments;
    /**
     * Version Number
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 开始日期
     */
    @TableField(value = "activeon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activeon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("activeon")
    private Timestamp activeon;
    /**
     * 负责人类型
     */
    @TableField(value = "ownertype")
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 净价 (Base)
     */
    @DEField(name = "net_base")
    @TableField(value = "net_base")
    @JSONField(name = "net_base")
    @JsonProperty("net_base")
    private BigDecimal netBase;
    /**
     * 支持日历
     */
    @TableField(value = "effectivitycalendar")
    @JSONField(name = "effectivitycalendar")
    @JsonProperty("effectivitycalendar")
    private String effectivitycalendar;
    /**
     * 客户
     */
    @TableField(value = "customername")
    @JSONField(name = "customername")
    @JsonProperty("customername")
    private String customername;
    /**
     * 结束日期
     */
    @TableField(value = "expireson")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "expireson" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("expireson")
    private Timestamp expireson;
    /**
     * 折扣(%)
     */
    @TableField(value = "discountpercentage")
    @JSONField(name = "discountpercentage")
    @JsonProperty("discountpercentage")
    private BigDecimal discountpercentage;
    /**
     * 负责人
     */
    @TableField(value = "ownerid")
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;
    /**
     * Import Sequence Number
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * Record Created On
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 合同子项
     */
    @DEField(isKeyField=true)
    @TableId(value= "contractdetailid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "contractdetailid")
    @JsonProperty("contractdetailid")
    private String contractdetailid;
    /**
     * 已用服务配额
     */
    @TableField(value = "allotmentsused")
    @JSONField(name = "allotmentsused")
    @JsonProperty("allotmentsused")
    private Integer allotmentsused;
    /**
     * UTC Conversion Time Zone Code
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * 数量
     */
    @TableField(value = "initialquantity")
    @JSONField(name = "initialquantity")
    @JsonProperty("initialquantity")
    private Integer initialquantity;
    /**
     * 费率
     */
    @TableField(value = "rate")
    @JSONField(name = "rate")
    @JsonProperty("rate")
    private BigDecimal rate;
    /**
     * 汇率
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * 序列号
     */
    @TableField(value = "productserialnumber")
    @JSONField(name = "productserialnumber")
    @JsonProperty("productserialnumber")
    private String productserialnumber;
    /**
     * 负责人
     */
    @TableField(value = "ownername")
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;
    /**
     * 折扣
     */
    @TableField(value = "discount")
    @JSONField(name = "discount")
    @JsonProperty("discount")
    private BigDecimal discount;
    /**
     * 状态描述
     */
    @TableField(value = "statuscode")
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;
    /**
     * 总价 (Base)
     */
    @DEField(name = "price_base")
    @TableField(value = "price_base")
    @JSONField(name = "price_base")
    @JsonProperty("price_base")
    private BigDecimal priceBase;
    /**
     * 折扣 (Base)
     */
    @DEField(name = "discount_base")
    @TableField(value = "discount_base")
    @JSONField(name = "discount_base")
    @JsonProperty("discount_base")
    private BigDecimal discountBase;
    /**
     * 位置
     */
    @TableField(value = "serviceaddress")
    @JSONField(name = "serviceaddress")
    @JsonProperty("serviceaddress")
    private String serviceaddress;
    /**
     * 产品
     */
    @TableField(value = "productid")
    @JSONField(name = "productid")
    @JsonProperty("productid")
    private String productid;
    /**
     * 单位计划
     */
    @TableField(value = "uomscheduleid")
    @JSONField(name = "uomscheduleid")
    @JsonProperty("uomscheduleid")
    private String uomscheduleid;
    /**
     * 合同
     */
    @TableField(value = "contractid")
    @JSONField(name = "contractid")
    @JsonProperty("contractid")
    private String contractid;
    /**
     * 计价单位
     */
    @TableField(value = "uomid")
    @JSONField(name = "uomid")
    @JsonProperty("uomid")
    private String uomid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.service.domain.Contract contract;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.CustomerAddress serviceaddre;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.product.domain.Product product;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.UomSchedule uomschedule;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Uom uom;



    /**
     * 设置 [服务合同部门]
     */
    public void setServicecontractunitscode(String servicecontractunitscode){
        this.servicecontractunitscode = servicecontractunitscode ;
        this.modify("servicecontractunitscode",servicecontractunitscode);
    }

    /**
     * 设置 [服务配额平均数]
     */
    public void setAllotmentsoverage(Integer allotmentsoverage){
        this.allotmentsoverage = allotmentsoverage ;
        this.modify("allotmentsoverage",allotmentsoverage);
    }

    /**
     * 设置 [合同状态]
     */
    public void setContractstatecode(String contractstatecode){
        this.contractstatecode = contractstatecode ;
        this.modify("contractstatecode",contractstatecode);
    }

    /**
     * 设置 [状态]
     */
    public void setStatecode(Integer statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [净价]
     */
    public void setNet(BigDecimal net){
        this.net = net ;
        this.modify("net",net);
    }

    /**
     * 设置 [费率 (Base)]
     */
    public void setRateBase(BigDecimal rateBase){
        this.rateBase = rateBase ;
        this.modify("rate_base",rateBase);
    }

    /**
     * 设置 [总价]
     */
    public void setPrice(BigDecimal price){
        this.price = price ;
        this.modify("price",price);
    }

    /**
     * 设置 [Time Zone Rule Version Number]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [客户]
     */
    public void setCustomerid(String customerid){
        this.customerid = customerid ;
        this.modify("customerid",customerid);
    }

    /**
     * 设置 [标题]
     */
    public void setTitle(String title){
        this.title = title ;
        this.modify("title",title);
    }

    /**
     * 设置 [明细项目顺序]
     */
    public void setLineitemorder(Integer lineitemorder){
        this.lineitemorder = lineitemorder ;
        this.modify("lineitemorder",lineitemorder);
    }

    /**
     * 设置 [剩余服务配额]
     */
    public void setAllotmentsremaining(Integer allotmentsremaining){
        this.allotmentsremaining = allotmentsremaining ;
        this.modify("allotmentsremaining",allotmentsremaining);
    }

    /**
     * 设置 [ServiceAddressName]
     */
    public void setServiceaddressname(String serviceaddressname){
        this.serviceaddressname = serviceaddressname ;
        this.modify("serviceaddressname",serviceaddressname);
    }

    /**
     * 设置 [客户类型]
     */
    public void setCustomertype(String customertype){
        this.customertype = customertype ;
        this.modify("customertype",customertype);
    }

    /**
     * 设置 [总服务配额]
     */
    public void setTotalallotments(Integer totalallotments){
        this.totalallotments = totalallotments ;
        this.modify("totalallotments",totalallotments);
    }

    /**
     * 设置 [Version Number]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [开始日期]
     */
    public void setActiveon(Timestamp activeon){
        this.activeon = activeon ;
        this.modify("activeon",activeon);
    }

    /**
     * 格式化日期 [开始日期]
     */
    public String formatActiveon(){
        if (this.activeon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(activeon);
    }
    /**
     * 设置 [负责人类型]
     */
    public void setOwnertype(String ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [净价 (Base)]
     */
    public void setNetBase(BigDecimal netBase){
        this.netBase = netBase ;
        this.modify("net_base",netBase);
    }

    /**
     * 设置 [支持日历]
     */
    public void setEffectivitycalendar(String effectivitycalendar){
        this.effectivitycalendar = effectivitycalendar ;
        this.modify("effectivitycalendar",effectivitycalendar);
    }

    /**
     * 设置 [客户]
     */
    public void setCustomername(String customername){
        this.customername = customername ;
        this.modify("customername",customername);
    }

    /**
     * 设置 [结束日期]
     */
    public void setExpireson(Timestamp expireson){
        this.expireson = expireson ;
        this.modify("expireson",expireson);
    }

    /**
     * 格式化日期 [结束日期]
     */
    public String formatExpireson(){
        if (this.expireson == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(expireson);
    }
    /**
     * 设置 [折扣(%)]
     */
    public void setDiscountpercentage(BigDecimal discountpercentage){
        this.discountpercentage = discountpercentage ;
        this.modify("discountpercentage",discountpercentage);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnerid(String ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [Import Sequence Number]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [Record Created On]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [Record Created On]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [已用服务配额]
     */
    public void setAllotmentsused(Integer allotmentsused){
        this.allotmentsused = allotmentsused ;
        this.modify("allotmentsused",allotmentsused);
    }

    /**
     * 设置 [UTC Conversion Time Zone Code]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [数量]
     */
    public void setInitialquantity(Integer initialquantity){
        this.initialquantity = initialquantity ;
        this.modify("initialquantity",initialquantity);
    }

    /**
     * 设置 [费率]
     */
    public void setRate(BigDecimal rate){
        this.rate = rate ;
        this.modify("rate",rate);
    }

    /**
     * 设置 [汇率]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [序列号]
     */
    public void setProductserialnumber(String productserialnumber){
        this.productserialnumber = productserialnumber ;
        this.modify("productserialnumber",productserialnumber);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnername(String ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [折扣]
     */
    public void setDiscount(BigDecimal discount){
        this.discount = discount ;
        this.modify("discount",discount);
    }

    /**
     * 设置 [状态描述]
     */
    public void setStatuscode(Integer statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [总价 (Base)]
     */
    public void setPriceBase(BigDecimal priceBase){
        this.priceBase = priceBase ;
        this.modify("price_base",priceBase);
    }

    /**
     * 设置 [折扣 (Base)]
     */
    public void setDiscountBase(BigDecimal discountBase){
        this.discountBase = discountBase ;
        this.modify("discount_base",discountBase);
    }

    /**
     * 设置 [位置]
     */
    public void setServiceaddress(String serviceaddress){
        this.serviceaddress = serviceaddress ;
        this.modify("serviceaddress",serviceaddress);
    }

    /**
     * 设置 [产品]
     */
    public void setProductid(String productid){
        this.productid = productid ;
        this.modify("productid",productid);
    }

    /**
     * 设置 [单位计划]
     */
    public void setUomscheduleid(String uomscheduleid){
        this.uomscheduleid = uomscheduleid ;
        this.modify("uomscheduleid",uomscheduleid);
    }

    /**
     * 设置 [合同]
     */
    public void setContractid(String contractid){
        this.contractid = contractid ;
        this.modify("contractid",contractid);
    }

    /**
     * 设置 [计价单位]
     */
    public void setUomid(String uomid){
        this.uomid = uomid ;
        this.modify("uomid",uomid);
    }


}



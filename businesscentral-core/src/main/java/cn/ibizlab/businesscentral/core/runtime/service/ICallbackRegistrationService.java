package cn.ibizlab.businesscentral.core.runtime.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.runtime.domain.CallbackRegistration;
import cn.ibizlab.businesscentral.core.runtime.filter.CallbackRegistrationSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[CallbackRegistration] 服务对象接口
 */
public interface ICallbackRegistrationService extends IService<CallbackRegistration>{

    boolean create(CallbackRegistration et) ;
    void createBatch(List<CallbackRegistration> list) ;
    boolean update(CallbackRegistration et) ;
    void updateBatch(List<CallbackRegistration> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    CallbackRegistration get(String key) ;
    CallbackRegistration getDraft(CallbackRegistration et) ;
    boolean checkKey(CallbackRegistration et) ;
    boolean save(CallbackRegistration et) ;
    void saveBatch(List<CallbackRegistration> list) ;
    Page<CallbackRegistration> searchDefault(CallbackRegistrationSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<CallbackRegistration> getCallbackregistrationByIds(List<String> ids) ;
    List<CallbackRegistration> getCallbackregistrationByEntities(List<CallbackRegistration> entities) ;
}



package cn.ibizlab.businesscentral.core.base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.base.domain.Calendar;
/**
 * 关系型数据实体[Calendar] 查询条件对象
 */
@Slf4j
@Data
public class CalendarSearchContext extends QueryWrapperContext<Calendar> {

	private String n_calendarname_like;//[日历名称]
	public void setN_calendarname_like(String n_calendarname_like) {
        this.n_calendarname_like = n_calendarname_like;
        if(!ObjectUtils.isEmpty(this.n_calendarname_like)){
            this.getSearchCond().like("calendarname", n_calendarname_like);
        }
    }
	private String n_type_eq;//[日历类型]
	public void setN_type_eq(String n_type_eq) {
        this.n_type_eq = n_type_eq;
        if(!ObjectUtils.isEmpty(this.n_type_eq)){
            this.getSearchCond().eq("type", n_type_eq);
        }
    }
	private String n_holidayschedulecalendarid_eq;//[节日时间表 CalendarId]
	public void setN_holidayschedulecalendarid_eq(String n_holidayschedulecalendarid_eq) {
        this.n_holidayschedulecalendarid_eq = n_holidayschedulecalendarid_eq;
        if(!ObjectUtils.isEmpty(this.n_holidayschedulecalendarid_eq)){
            this.getSearchCond().eq("holidayschedulecalendarid", n_holidayschedulecalendarid_eq);
        }
    }
	private String n_businessunitid_eq;//[业务部门]
	public void setN_businessunitid_eq(String n_businessunitid_eq) {
        this.n_businessunitid_eq = n_businessunitid_eq;
        if(!ObjectUtils.isEmpty(this.n_businessunitid_eq)){
            this.getSearchCond().eq("businessunitid", n_businessunitid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("calendarname", query)   
            );
		 }
	}
}




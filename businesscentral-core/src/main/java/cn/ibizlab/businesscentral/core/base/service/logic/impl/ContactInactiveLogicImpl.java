package cn.ibizlab.businesscentral.core.base.service.logic.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;

import cn.ibizlab.businesscentral.core.base.service.logic.IContactInactiveLogic;
import cn.ibizlab.businesscentral.core.base.domain.Contact;

/**
 * 关系型数据实体[Inactive] 对象
 */
@Slf4j
@Service
public class ContactInactiveLogicImpl implements IContactInactiveLogic{

    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private cn.ibizlab.businesscentral.core.base.service.IContactService contactservice;

    public cn.ibizlab.businesscentral.core.base.service.IContactService getContactService() {
        return this.contactservice;
    }


    @Autowired
    private cn.ibizlab.businesscentral.core.base.service.IContactService iBzSysDefaultService;

    public cn.ibizlab.businesscentral.core.base.service.IContactService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    public void execute(Contact et){

          KieSession kieSession = null;
        try{
           kieSession=kieContainer.newKieSession();
           kieSession.insert(et); 
           kieSession.setGlobal("contactinactivedefault",et);
           kieSession.setGlobal("contactservice",contactservice);
           kieSession.setGlobal("iBzSysContactDefaultService",iBzSysDefaultService);
           kieSession.setGlobal("curuser", cn.ibizlab.businesscentral.util.security.AuthenticationUser.getAuthenticationUser());
           kieSession.startProcess("cn.ibizlab.businesscentral.core.base.service.logic.contactinactive");

        }catch(Exception e){
            throw new RuntimeException("执行[停用]处理逻辑发生异常"+e);
        }finally {
            if(kieSession!=null)
            kieSession.destroy();
        }
    }

}

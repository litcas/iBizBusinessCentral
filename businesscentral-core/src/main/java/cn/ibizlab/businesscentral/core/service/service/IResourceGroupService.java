package cn.ibizlab.businesscentral.core.service.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.service.domain.ResourceGroup;
import cn.ibizlab.businesscentral.core.service.filter.ResourceGroupSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[ResourceGroup] 服务对象接口
 */
public interface IResourceGroupService extends IService<ResourceGroup>{

    boolean create(ResourceGroup et) ;
    void createBatch(List<ResourceGroup> list) ;
    boolean update(ResourceGroup et) ;
    void updateBatch(List<ResourceGroup> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    ResourceGroup get(String key) ;
    ResourceGroup getDraft(ResourceGroup et) ;
    boolean checkKey(ResourceGroup et) ;
    boolean save(ResourceGroup et) ;
    void saveBatch(List<ResourceGroup> list) ;
    Page<ResourceGroup> searchDefault(ResourceGroupSearchContext context) ;
    List<ResourceGroup> selectByBusinessunitid(String businessunitid) ;
    void removeByBusinessunitid(String businessunitid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<ResourceGroup> getResourcegroupByIds(List<String> ids) ;
    List<ResourceGroup> getResourcegroupByEntities(List<ResourceGroup> entities) ;
}



package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.SlaKpiInstance;
import cn.ibizlab.businesscentral.core.base.filter.SlaKpiInstanceSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[SlaKpiInstance] 服务对象接口
 */
public interface ISlaKpiInstanceService extends IService<SlaKpiInstance>{

    boolean create(SlaKpiInstance et) ;
    void createBatch(List<SlaKpiInstance> list) ;
    boolean update(SlaKpiInstance et) ;
    void updateBatch(List<SlaKpiInstance> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    SlaKpiInstance get(String key) ;
    SlaKpiInstance getDraft(SlaKpiInstance et) ;
    boolean checkKey(SlaKpiInstance et) ;
    boolean save(SlaKpiInstance et) ;
    void saveBatch(List<SlaKpiInstance> list) ;
    Page<SlaKpiInstance> searchDefault(SlaKpiInstanceSearchContext context) ;
    List<SlaKpiInstance> selectByRegarding(String accountid) ;
    void removeByRegarding(String accountid) ;
    List<SlaKpiInstance> selectByOwningbusinessunit(String businessunitid) ;
    void removeByOwningbusinessunit(String businessunitid) ;
    List<SlaKpiInstance> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<SlaKpiInstance> getSlakpiinstanceByIds(List<String> ids) ;
    List<SlaKpiInstance> getSlakpiinstanceByEntities(List<SlaKpiInstance> entities) ;
}



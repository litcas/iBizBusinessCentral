package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.IBizAudit;
import cn.ibizlab.businesscentral.core.base.filter.IBizAuditSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[IBizAudit] 服务对象接口
 */
public interface IIBizAuditService extends IService<IBizAudit>{

    boolean create(IBizAudit et) ;
    void createBatch(List<IBizAudit> list) ;
    boolean update(IBizAudit et) ;
    void updateBatch(List<IBizAudit> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    IBizAudit get(String key) ;
    IBizAudit getDraft(IBizAudit et) ;
    boolean checkKey(IBizAudit et) ;
    boolean save(IBizAudit et) ;
    void saveBatch(List<IBizAudit> list) ;
    Page<IBizAudit> searchDefault(IBizAuditSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<IBizAudit> getIbizauditByIds(List<String> ids) ;
    List<IBizAudit> getIbizauditByEntities(List<IBizAudit> entities) ;
}



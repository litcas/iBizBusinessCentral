package cn.ibizlab.businesscentral.core.product.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[价目表项]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "PRODUCTPRICELEVEL",resultMap = "ProductPriceLevelResultMap")
public class ProductPriceLevel extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Import Sequence Number
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * 舍入金额
     */
    @TableField(value = "roundingoptionamount")
    @JSONField(name = "roundingoptionamount")
    @JsonProperty("roundingoptionamount")
    private BigDecimal roundingoptionamount;
    /**
     * Version Number
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 金额 (Base)
     */
    @DEField(name = "amount_base")
    @TableField(value = "amount_base")
    @JSONField(name = "amount_base")
    @JsonProperty("amount_base")
    private BigDecimal amountBase;
    /**
     * Process Id
     */
    @TableField(value = "processid")
    @JSONField(name = "processid")
    @JsonProperty("processid")
    private String processid;
    /**
     * 金额
     */
    @TableField(value = "amount")
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private BigDecimal amount;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 舍入金额 (Base)
     */
    @DEField(name = "roundingoptionamount_base")
    @TableField(value = "roundingoptionamount_base")
    @JSONField(name = "roundingoptionamount_base")
    @JsonProperty("roundingoptionamount_base")
    private BigDecimal roundingoptionamountBase;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * Record Created On
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 汇率
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * 组织
     */
    @TableField(value = "organizationid")
    @JSONField(name = "organizationid")
    @JsonProperty("organizationid")
    private String organizationid;
    /**
     * 百分比
     */
    @TableField(value = "percentage")
    @JSONField(name = "percentage")
    @JsonProperty("percentage")
    private BigDecimal percentage;
    /**
     * Traversed Path
     */
    @TableField(value = "traversedpath")
    @JSONField(name = "traversedpath")
    @JsonProperty("traversedpath")
    private String traversedpath;
    /**
     * 产品 ID
     */
    @TableField(value = "productnumber")
    @JSONField(name = "productnumber")
    @JsonProperty("productnumber")
    private String productnumber;
    /**
     * 定价方式
     */
    @TableField(value = "pricingmethodcode")
    @JSONField(name = "pricingmethodcode")
    @JsonProperty("pricingmethodcode")
    private String pricingmethodcode;
    /**
     * 舍入选项
     */
    @TableField(value = "roundingoptioncode")
    @JSONField(name = "roundingoptioncode")
    @JsonProperty("roundingoptioncode")
    private String roundingoptioncode;
    /**
     * Time Zone Rule Version Number
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * UTC Conversion Time Zone Code
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * 产品价目表
     */
    @DEField(isKeyField=true)
    @TableId(value= "productpricelevelid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "productpricelevelid")
    @JsonProperty("productpricelevelid")
    private String productpricelevelid;
    /**
     * 销售数量控制
     */
    @TableField(value = "quantitysellingcode")
    @JSONField(name = "quantitysellingcode")
    @JsonProperty("quantitysellingcode")
    private String quantitysellingcode;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 舍入原则
     */
    @TableField(value = "roundingpolicycode")
    @JSONField(name = "roundingpolicycode")
    @JsonProperty("roundingpolicycode")
    private String roundingpolicycode;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * Stage Id
     */
    @TableField(value = "stageid")
    @JSONField(name = "stageid")
    @JsonProperty("stageid")
    private String stageid;
    /**
     * 计量单位组
     */
    @TableField(value = "uomschedulename")
    @JSONField(name = "uomschedulename")
    @JsonProperty("uomschedulename")
    private String uomschedulename;
    /**
     * 产品名称
     */
    @TableField(value = "productname")
    @JSONField(name = "productname")
    @JsonProperty("productname")
    private String productname;
    /**
     * 货币
     */
    @TableField(value = "currencyname")
    @JSONField(name = "currencyname")
    @JsonProperty("currencyname")
    private String currencyname;
    /**
     * 计量单位
     */
    @TableField(value = "uomname")
    @JSONField(name = "uomname")
    @JsonProperty("uomname")
    private String uomname;
    /**
     * 折扣表
     */
    @TableField(value = "discounttypename")
    @JSONField(name = "discounttypename")
    @JsonProperty("discounttypename")
    private String discounttypename;
    /**
     * 价目表
     */
    @TableField(value = "pricelevelname")
    @JSONField(name = "pricelevelname")
    @JsonProperty("pricelevelname")
    private String pricelevelname;
    /**
     * 产品
     */
    @TableField(value = "productid")
    @JSONField(name = "productid")
    @JsonProperty("productid")
    private String productid;
    /**
     * 单位计划 ID
     */
    @TableField(value = "uomscheduleid")
    @JSONField(name = "uomscheduleid")
    @JsonProperty("uomscheduleid")
    private String uomscheduleid;
    /**
     * 货币
     */
    @TableField(value = "transactioncurrencyid")
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;
    /**
     * 计价单位
     */
    @TableField(value = "uomid")
    @JSONField(name = "uomid")
    @JsonProperty("uomid")
    private String uomid;
    /**
     * 折扣表
     */
    @TableField(value = "discounttypeid")
    @JSONField(name = "discounttypeid")
    @JsonProperty("discounttypeid")
    private String discounttypeid;
    /**
     * 价目表
     */
    @TableField(value = "pricelevelid")
    @JSONField(name = "pricelevelid")
    @JsonProperty("pricelevelid")
    private String pricelevelid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.sales.domain.DiscountType discounttype;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.product.domain.PriceLevel pricelevel;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.product.domain.Product product;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.UomSchedule uomschedule;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Uom uom;



    /**
     * 设置 [Import Sequence Number]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [舍入金额]
     */
    public void setRoundingoptionamount(BigDecimal roundingoptionamount){
        this.roundingoptionamount = roundingoptionamount ;
        this.modify("roundingoptionamount",roundingoptionamount);
    }

    /**
     * 设置 [Version Number]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [金额 (Base)]
     */
    public void setAmountBase(BigDecimal amountBase){
        this.amountBase = amountBase ;
        this.modify("amount_base",amountBase);
    }

    /**
     * 设置 [Process Id]
     */
    public void setProcessid(String processid){
        this.processid = processid ;
        this.modify("processid",processid);
    }

    /**
     * 设置 [金额]
     */
    public void setAmount(BigDecimal amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }

    /**
     * 设置 [舍入金额 (Base)]
     */
    public void setRoundingoptionamountBase(BigDecimal roundingoptionamountBase){
        this.roundingoptionamountBase = roundingoptionamountBase ;
        this.modify("roundingoptionamount_base",roundingoptionamountBase);
    }

    /**
     * 设置 [Record Created On]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [Record Created On]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [汇率]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [组织]
     */
    public void setOrganizationid(String organizationid){
        this.organizationid = organizationid ;
        this.modify("organizationid",organizationid);
    }

    /**
     * 设置 [百分比]
     */
    public void setPercentage(BigDecimal percentage){
        this.percentage = percentage ;
        this.modify("percentage",percentage);
    }

    /**
     * 设置 [Traversed Path]
     */
    public void setTraversedpath(String traversedpath){
        this.traversedpath = traversedpath ;
        this.modify("traversedpath",traversedpath);
    }

    /**
     * 设置 [产品 ID]
     */
    public void setProductnumber(String productnumber){
        this.productnumber = productnumber ;
        this.modify("productnumber",productnumber);
    }

    /**
     * 设置 [定价方式]
     */
    public void setPricingmethodcode(String pricingmethodcode){
        this.pricingmethodcode = pricingmethodcode ;
        this.modify("pricingmethodcode",pricingmethodcode);
    }

    /**
     * 设置 [舍入选项]
     */
    public void setRoundingoptioncode(String roundingoptioncode){
        this.roundingoptioncode = roundingoptioncode ;
        this.modify("roundingoptioncode",roundingoptioncode);
    }

    /**
     * 设置 [Time Zone Rule Version Number]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [UTC Conversion Time Zone Code]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [销售数量控制]
     */
    public void setQuantitysellingcode(String quantitysellingcode){
        this.quantitysellingcode = quantitysellingcode ;
        this.modify("quantitysellingcode",quantitysellingcode);
    }

    /**
     * 设置 [舍入原则]
     */
    public void setRoundingpolicycode(String roundingpolicycode){
        this.roundingpolicycode = roundingpolicycode ;
        this.modify("roundingpolicycode",roundingpolicycode);
    }

    /**
     * 设置 [Stage Id]
     */
    public void setStageid(String stageid){
        this.stageid = stageid ;
        this.modify("stageid",stageid);
    }

    /**
     * 设置 [计量单位组]
     */
    public void setUomschedulename(String uomschedulename){
        this.uomschedulename = uomschedulename ;
        this.modify("uomschedulename",uomschedulename);
    }

    /**
     * 设置 [产品名称]
     */
    public void setProductname(String productname){
        this.productname = productname ;
        this.modify("productname",productname);
    }

    /**
     * 设置 [货币]
     */
    public void setCurrencyname(String currencyname){
        this.currencyname = currencyname ;
        this.modify("currencyname",currencyname);
    }

    /**
     * 设置 [计量单位]
     */
    public void setUomname(String uomname){
        this.uomname = uomname ;
        this.modify("uomname",uomname);
    }

    /**
     * 设置 [折扣表]
     */
    public void setDiscounttypename(String discounttypename){
        this.discounttypename = discounttypename ;
        this.modify("discounttypename",discounttypename);
    }

    /**
     * 设置 [价目表]
     */
    public void setPricelevelname(String pricelevelname){
        this.pricelevelname = pricelevelname ;
        this.modify("pricelevelname",pricelevelname);
    }

    /**
     * 设置 [产品]
     */
    public void setProductid(String productid){
        this.productid = productid ;
        this.modify("productid",productid);
    }

    /**
     * 设置 [单位计划 ID]
     */
    public void setUomscheduleid(String uomscheduleid){
        this.uomscheduleid = uomscheduleid ;
        this.modify("uomscheduleid",uomscheduleid);
    }

    /**
     * 设置 [货币]
     */
    public void setTransactioncurrencyid(String transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }

    /**
     * 设置 [计价单位]
     */
    public void setUomid(String uomid){
        this.uomid = uomid ;
        this.modify("uomid",uomid);
    }

    /**
     * 设置 [折扣表]
     */
    public void setDiscounttypeid(String discounttypeid){
        this.discounttypeid = discounttypeid ;
        this.modify("discounttypeid",discounttypeid);
    }

    /**
     * 设置 [价目表]
     */
    public void setPricelevelid(String pricelevelid){
        this.pricelevelid = pricelevelid ;
        this.modify("pricelevelid",pricelevelid);
    }


}



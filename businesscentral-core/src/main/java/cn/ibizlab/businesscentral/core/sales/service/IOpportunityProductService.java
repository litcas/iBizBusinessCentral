package cn.ibizlab.businesscentral.core.sales.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.sales.domain.OpportunityProduct;
import cn.ibizlab.businesscentral.core.sales.filter.OpportunityProductSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[OpportunityProduct] 服务对象接口
 */
public interface IOpportunityProductService extends IService<OpportunityProduct>{

    boolean create(OpportunityProduct et) ;
    void createBatch(List<OpportunityProduct> list) ;
    boolean update(OpportunityProduct et) ;
    void updateBatch(List<OpportunityProduct> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    OpportunityProduct get(String key) ;
    OpportunityProduct getDraft(OpportunityProduct et) ;
    boolean checkKey(OpportunityProduct et) ;
    boolean save(OpportunityProduct et) ;
    void saveBatch(List<OpportunityProduct> list) ;
    Page<OpportunityProduct> searchDefault(OpportunityProductSearchContext context) ;
    List<OpportunityProduct> selectByParentbundleidref(String opportunityproductid) ;
    void removeByParentbundleidref(String opportunityproductid) ;
    List<OpportunityProduct> selectByOpportunityid(String opportunityid) ;
    void removeByOpportunityid(String opportunityid) ;
    List<OpportunityProduct> selectByProductid(String productid) ;
    void removeByProductid(String productid) ;
    List<OpportunityProduct> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    List<OpportunityProduct> selectByUomid(String uomid) ;
    void removeByUomid(String uomid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<OpportunityProduct> getOpportunityproductByIds(List<String> ids) ;
    List<OpportunityProduct> getOpportunityproductByEntities(List<OpportunityProduct> entities) ;
}



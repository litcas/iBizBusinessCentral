package cn.ibizlab.businesscentral.core.runtime.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.runtime.domain.Connection;
import cn.ibizlab.businesscentral.core.runtime.filter.ConnectionSearchContext;
import cn.ibizlab.businesscentral.core.runtime.service.IConnectionService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.runtime.mapper.ConnectionMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[连接] 服务对象接口实现
 */
@Slf4j
@Service("ConnectionServiceImpl")
public class ConnectionServiceImpl extends ServiceImpl<ConnectionMapper, Connection> implements IConnectionService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.runtime.service.IConnectionRoleService connectionroleService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(Connection et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getConnectionid()),et);
        return true;
    }

    @Override
    public void createBatch(List<Connection> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Connection et) {
        fillParentData(et);
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("connectionid",et.getConnectionid())))
            return false;
        CachedBeanCopier.copy(get(et.getConnectionid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<Connection> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Connection get(String key) {
        Connection et = getById(key);
        if(et==null){
            et=new Connection();
            et.setConnectionid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Connection getDraft(Connection et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Connection et) {
        return (!ObjectUtils.isEmpty(et.getConnectionid()))&&(!Objects.isNull(this.getById(et.getConnectionid())));
    }
    @Override
    @Transactional
    public boolean save(Connection et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Connection et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<Connection> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<Connection> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Connection> selectByRecord1roleid(String connectionroleid) {
        return baseMapper.selectByRecord1roleid(connectionroleid);
    }

    @Override
    public void removeByRecord1roleid(String connectionroleid) {
        this.remove(new QueryWrapper<Connection>().eq("record1roleid",connectionroleid));
    }

	@Override
    public List<Connection> selectByRecord2roleid(String connectionroleid) {
        return baseMapper.selectByRecord2roleid(connectionroleid);
    }

    @Override
    public void removeByRecord2roleid(String connectionroleid) {
        this.remove(new QueryWrapper<Connection>().eq("record2roleid",connectionroleid));
    }

	@Override
    public List<Connection> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<Connection>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 ByParentKey
     */
    @Override
    public Page<Connection> searchByParentKey(ConnectionSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Connection> pages=baseMapper.searchByParentKey(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Connection>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<Connection> searchDefault(ConnectionSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Connection> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Connection>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Connection et){
        //实体关系[DER1N_CONNECTION__CONNECTIONROLE__RECORD1ROLEID]
        if(!ObjectUtils.isEmpty(et.getRecord1roleid())){
            cn.ibizlab.businesscentral.core.runtime.domain.ConnectionRole record1role=et.getRecord1role();
            if(ObjectUtils.isEmpty(record1role)){
                cn.ibizlab.businesscentral.core.runtime.domain.ConnectionRole majorEntity=connectionroleService.get(et.getRecord1roleid());
                et.setRecord1role(majorEntity);
                record1role=majorEntity;
            }
            et.setRecord1rolename(record1role.getConnectionrolename());
        }
        //实体关系[DER1N_CONNECTION__CONNECTIONROLE__RECORD2ROLEID]
        if(!ObjectUtils.isEmpty(et.getRecord2roleid())){
            cn.ibizlab.businesscentral.core.runtime.domain.ConnectionRole record2role=et.getRecord2role();
            if(ObjectUtils.isEmpty(record2role)){
                cn.ibizlab.businesscentral.core.runtime.domain.ConnectionRole majorEntity=connectionroleService.get(et.getRecord2roleid());
                et.setRecord2role(majorEntity);
                record2role=majorEntity;
            }
            et.setRecord2rolename(record2role.getConnectionrolename());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Connection> getConnectionByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Connection> getConnectionByEntities(List<Connection> entities) {
        List ids =new ArrayList();
        for(Connection entity : entities){
            Serializable id=entity.getConnectionid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




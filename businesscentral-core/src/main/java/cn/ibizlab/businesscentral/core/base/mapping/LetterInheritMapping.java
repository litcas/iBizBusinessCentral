

package cn.ibizlab.businesscentral.core.base.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.base.domain.Letter;
import cn.ibizlab.businesscentral.core.base.domain.ActivityPointer;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface LetterInheritMapping {

    @Mappings({
        @Mapping(source ="activityid",target = "activityid"),
        @Mapping(source ="subject",target = "subject"),
        @Mapping(target ="focusNull",ignore = true),
    })
    ActivityPointer toActivitypointer(Letter letter);

    @Mappings({
        @Mapping(source ="activityid" ,target = "activityid"),
        @Mapping(source ="subject" ,target = "subject"),
        @Mapping(target ="focusNull",ignore = true),
    })
    Letter toLetter(ActivityPointer activitypointer);

    List<ActivityPointer> toActivitypointer(List<Letter> letter);

    List<Letter> toLetter(List<ActivityPointer> activitypointer);

}



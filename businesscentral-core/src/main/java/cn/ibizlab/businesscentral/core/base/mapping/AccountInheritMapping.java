

package cn.ibizlab.businesscentral.core.base.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.base.domain.Account;
import cn.ibizlab.businesscentral.core.service.domain.IncidentCustomer;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface AccountInheritMapping {

    @Mappings({
        @Mapping(source ="accountid",target = "customerid"),
        @Mapping(source ="accountname",target = "customername"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="businesstypecode",target = "businesstypecode"),
    })
    IncidentCustomer toIncidentcustomer(Account account);

    @Mappings({
        @Mapping(source ="customerid" ,target = "accountid"),
        @Mapping(source ="customername" ,target = "accountname"),
        @Mapping(target ="focusNull",ignore = true),
    })
    Account toAccount(IncidentCustomer incidentcustomer);

    List<IncidentCustomer> toIncidentcustomer(List<Account> account);

    List<Account> toAccount(List<IncidentCustomer> incidentcustomer);

}



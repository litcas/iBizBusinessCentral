package cn.ibizlab.businesscentral.core.sales.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.sales.domain.Quote;
import cn.ibizlab.businesscentral.core.sales.filter.QuoteSearchContext;
import cn.ibizlab.businesscentral.core.sales.service.IQuoteService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.sales.mapper.QuoteMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[报价单] 服务对象接口实现
 */
@Slf4j
@Service("QuoteServiceImpl")
public class QuoteServiceImpl extends ServiceImpl<QuoteMapper, Quote> implements IQuoteService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IQuoteCloseService quotecloseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IQuoteDetailService quotedetailService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.ISalesOrderService salesorderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.marketing.service.ICampaignService campaignService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IOpportunityService opportunityService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.product.service.IPriceLevelService pricelevelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ISlaService slaService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.logic.IQuoteActiveLogic activeLogic;

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.logic.IQuoteClosedLogic closedLogic;

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.logic.IQuoteWinLogic winLogic;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(Quote et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getQuoteid()),et);
        return true;
    }

    @Override
    public void createBatch(List<Quote> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Quote et) {
        fillParentData(et);
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("quoteid",et.getQuoteid())))
            return false;
        CachedBeanCopier.copy(get(et.getQuoteid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<Quote> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Quote get(String key) {
        Quote et = getById(key);
        if(et==null){
            et=new Quote();
            et.setQuoteid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Quote getDraft(Quote et) {
        fillParentData(et);
        return et;
    }

    @Override
    @Transactional
    public Quote active(Quote et) {
        activeLogic.execute(et);
         return et ;
    }

    @Override
    public boolean checkKey(Quote et) {
        return (!ObjectUtils.isEmpty(et.getQuoteid()))&&(!Objects.isNull(this.getById(et.getQuoteid())));
    }
    @Override
    @Transactional
    public Quote close(Quote et) {
        closedLogic.execute(et);
         return et ;
    }

    @Override
    @Transactional
    public Quote genSalesOrder(Quote et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public boolean save(Quote et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Quote et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<Quote> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<Quote> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }

    @Override
    @Transactional
    public Quote win(Quote et) {
        winLogic.execute(et);
         return et ;
    }


	@Override
    public List<Quote> selectByCampaignid(String campaignid) {
        return baseMapper.selectByCampaignid(campaignid);
    }

    @Override
    public void removeByCampaignid(String campaignid) {
        this.remove(new QueryWrapper<Quote>().eq("campaignid",campaignid));
    }

	@Override
    public List<Quote> selectByOpportunityid(String opportunityid) {
        return baseMapper.selectByOpportunityid(opportunityid);
    }

    @Override
    public void removeByOpportunityid(String opportunityid) {
        this.remove(new QueryWrapper<Quote>().eq("opportunityid",opportunityid));
    }

	@Override
    public List<Quote> selectByPricelevelid(String pricelevelid) {
        return baseMapper.selectByPricelevelid(pricelevelid);
    }

    @Override
    public void removeByPricelevelid(String pricelevelid) {
        this.remove(new QueryWrapper<Quote>().eq("pricelevelid",pricelevelid));
    }

	@Override
    public List<Quote> selectBySlaid(String slaid) {
        return baseMapper.selectBySlaid(slaid);
    }

    @Override
    public void removeBySlaid(String slaid) {
        this.remove(new QueryWrapper<Quote>().eq("slaid",slaid));
    }

	@Override
    public List<Quote> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<Quote>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 ByParentKey
     */
    @Override
    public Page<Quote> searchByParentKey(QuoteSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Quote> pages=baseMapper.searchByParentKey(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Quote>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 已关闭
     */
    @Override
    public Page<Quote> searchClosed(QuoteSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Quote> pages=baseMapper.searchClosed(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Quote>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<Quote> searchDefault(QuoteSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Quote> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Quote>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 草稿
     */
    @Override
    public Page<Quote> searchDraft(QuoteSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Quote> pages=baseMapper.searchDraft(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Quote>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 有效报价单
     */
    @Override
    public Page<Quote> searchEffective(QuoteSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Quote> pages=baseMapper.searchEffective(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Quote>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 赢单
     */
    @Override
    public Page<Quote> searchWin(QuoteSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Quote> pages=baseMapper.searchWin(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Quote>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Quote et){
        //实体关系[DER1N_QUOTE__CAMPAIGN__CAMPAIGNID]
        if(!ObjectUtils.isEmpty(et.getCampaignid())){
            cn.ibizlab.businesscentral.core.marketing.domain.Campaign campaign=et.getCampaign();
            if(ObjectUtils.isEmpty(campaign)){
                cn.ibizlab.businesscentral.core.marketing.domain.Campaign majorEntity=campaignService.get(et.getCampaignid());
                et.setCampaign(majorEntity);
                campaign=majorEntity;
            }
            et.setCampaignname(campaign.getCampaignname());
        }
        //实体关系[DER1N_QUOTE__OPPORTUNITY__OPPORTUNITYID]
        if(!ObjectUtils.isEmpty(et.getOpportunityid())){
            cn.ibizlab.businesscentral.core.sales.domain.Opportunity opportunity=et.getOpportunity();
            if(ObjectUtils.isEmpty(opportunity)){
                cn.ibizlab.businesscentral.core.sales.domain.Opportunity majorEntity=opportunityService.get(et.getOpportunityid());
                et.setOpportunity(majorEntity);
                opportunity=majorEntity;
            }
            et.setOpportunityname(opportunity.getOpportunityname());
        }
        //实体关系[DER1N_QUOTE__PRICELEVEL__PRICELEVELID]
        if(!ObjectUtils.isEmpty(et.getPricelevelid())){
            cn.ibizlab.businesscentral.core.product.domain.PriceLevel pricelevel=et.getPricelevel();
            if(ObjectUtils.isEmpty(pricelevel)){
                cn.ibizlab.businesscentral.core.product.domain.PriceLevel majorEntity=pricelevelService.get(et.getPricelevelid());
                et.setPricelevel(majorEntity);
                pricelevel=majorEntity;
            }
            et.setPricelevelname(pricelevel.getPricelevelname());
        }
        //实体关系[DER1N_QUOTE__SLA__SLAID]
        if(!ObjectUtils.isEmpty(et.getSlaid())){
            cn.ibizlab.businesscentral.core.base.domain.Sla sla=et.getSla();
            if(ObjectUtils.isEmpty(sla)){
                cn.ibizlab.businesscentral.core.base.domain.Sla majorEntity=slaService.get(et.getSlaid());
                et.setSla(majorEntity);
                sla=majorEntity;
            }
            et.setSlaname(sla.getSlaname());
        }
        //实体关系[DER1N_QUOTE__TRANSACTIONCURRENCY__TRANSACTIONCURRENCYID]
        if(!ObjectUtils.isEmpty(et.getTransactioncurrencyid())){
            cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency=et.getTransactioncurrency();
            if(ObjectUtils.isEmpty(transactioncurrency)){
                cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency majorEntity=transactioncurrencyService.get(et.getTransactioncurrencyid());
                et.setTransactioncurrency(majorEntity);
                transactioncurrency=majorEntity;
            }
            et.setCurrencyname(transactioncurrency.getCurrencyname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Quote> getQuoteByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Quote> getQuoteByEntities(List<Quote> entities) {
        List ids =new ArrayList();
        for(Quote entity : entities){
            Serializable id=entity.getQuoteid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




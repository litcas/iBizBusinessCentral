package cn.ibizlab.businesscentral.core.service.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.service.domain.ContractTemplate;
/**
 * 关系型数据实体[ContractTemplate] 查询条件对象
 */
@Slf4j
@Data
public class ContractTemplateSearchContext extends QueryWrapperContext<ContractTemplate> {

	private String n_billingfrequencycode_eq;//[记帐频率]
	public void setN_billingfrequencycode_eq(String n_billingfrequencycode_eq) {
        this.n_billingfrequencycode_eq = n_billingfrequencycode_eq;
        if(!ObjectUtils.isEmpty(this.n_billingfrequencycode_eq)){
            this.getSearchCond().eq("billingfrequencycode", n_billingfrequencycode_eq);
        }
    }
	private String n_componentstate_eq;//[组件状态]
	public void setN_componentstate_eq(String n_componentstate_eq) {
        this.n_componentstate_eq = n_componentstate_eq;
        if(!ObjectUtils.isEmpty(this.n_componentstate_eq)){
            this.getSearchCond().eq("componentstate", n_componentstate_eq);
        }
    }
	private String n_contractservicelevelcode_eq;//[合同服务级别]
	public void setN_contractservicelevelcode_eq(String n_contractservicelevelcode_eq) {
        this.n_contractservicelevelcode_eq = n_contractservicelevelcode_eq;
        if(!ObjectUtils.isEmpty(this.n_contractservicelevelcode_eq)){
            this.getSearchCond().eq("contractservicelevelcode", n_contractservicelevelcode_eq);
        }
    }
	private String n_contracttemplatename_like;//[合同模板名称]
	public void setN_contracttemplatename_like(String n_contracttemplatename_like) {
        this.n_contracttemplatename_like = n_contracttemplatename_like;
        if(!ObjectUtils.isEmpty(this.n_contracttemplatename_like)){
            this.getSearchCond().like("contracttemplatename", n_contracttemplatename_like);
        }
    }
	private String n_allotmenttypecode_eq;//[服务配额类型]
	public void setN_allotmenttypecode_eq(String n_allotmenttypecode_eq) {
        this.n_allotmenttypecode_eq = n_allotmenttypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_allotmenttypecode_eq)){
            this.getSearchCond().eq("allotmenttypecode", n_allotmenttypecode_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("contracttemplatename", query)   
            );
		 }
	}
}




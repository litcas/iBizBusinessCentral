package cn.ibizlab.businesscentral.core.scheduling.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.scheduling.domain.BookingStatus;
import cn.ibizlab.businesscentral.core.scheduling.filter.BookingStatusSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[BookingStatus] 服务对象接口
 */
public interface IBookingStatusService extends IService<BookingStatus>{

    boolean create(BookingStatus et) ;
    void createBatch(List<BookingStatus> list) ;
    boolean update(BookingStatus et) ;
    void updateBatch(List<BookingStatus> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    BookingStatus get(String key) ;
    BookingStatus getDraft(BookingStatus et) ;
    boolean checkKey(BookingStatus et) ;
    boolean save(BookingStatus et) ;
    void saveBatch(List<BookingStatus> list) ;
    Page<BookingStatus> searchDefault(BookingStatusSearchContext context) ;
    List<BookingStatus> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<BookingStatus> getBookingstatusByIds(List<String> ids) ;
    List<BookingStatus> getBookingstatusByEntities(List<BookingStatus> entities) ;
}



package cn.ibizlab.businesscentral.core.base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.base.domain.OMHierarchyPurposeRef;
/**
 * 关系型数据实体[OMHierarchyPurposeRef] 查询条件对象
 */
@Slf4j
@Data
public class OMHierarchyPurposeRefSearchContext extends QueryWrapperContext<OMHierarchyPurposeRef> {

	private String n_omhierarchypurposerefname_like;//[组织层次结构分配名称]
	public void setN_omhierarchypurposerefname_like(String n_omhierarchypurposerefname_like) {
        this.n_omhierarchypurposerefname_like = n_omhierarchypurposerefname_like;
        if(!ObjectUtils.isEmpty(this.n_omhierarchypurposerefname_like)){
            this.getSearchCond().like("omhierarchypurposerefname", n_omhierarchypurposerefname_like);
        }
    }
	private String n_omhierarchycatid_eq;//[结构层次类别标识]
	public void setN_omhierarchycatid_eq(String n_omhierarchycatid_eq) {
        this.n_omhierarchycatid_eq = n_omhierarchycatid_eq;
        if(!ObjectUtils.isEmpty(this.n_omhierarchycatid_eq)){
            this.getSearchCond().eq("omhierarchycatid", n_omhierarchycatid_eq);
        }
    }
	private String n_omhierarchypurposeid_eq;//[组织层次机构目的标识]
	public void setN_omhierarchypurposeid_eq(String n_omhierarchypurposeid_eq) {
        this.n_omhierarchypurposeid_eq = n_omhierarchypurposeid_eq;
        if(!ObjectUtils.isEmpty(this.n_omhierarchypurposeid_eq)){
            this.getSearchCond().eq("omhierarchypurposeid", n_omhierarchypurposeid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("omhierarchypurposerefname", query)   
            );
		 }
	}
}




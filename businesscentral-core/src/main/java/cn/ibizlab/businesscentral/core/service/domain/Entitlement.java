package cn.ibizlab.businesscentral.core.service.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[权利]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "ENTITLEMENT",resultMap = "EntitlementResultMap")
public class Entitlement extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 开始日期
     */
    @TableField(value = "startdate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "startdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("startdate")
    private Timestamp startdate;
    /**
     * Process Id
     */
    @TableField(value = "processid")
    @JSONField(name = "processid")
    @JsonProperty("processid")
    private String processid;
    /**
     * Traversed Path
     */
    @TableField(value = "traversedpath")
    @JSONField(name = "traversedpath")
    @JsonProperty("traversedpath")
    private String traversedpath;
    /**
     * 缩短剩余期限
     */
    @TableField(value = "decreaseremainingon")
    @JSONField(name = "decreaseremainingon")
    @JsonProperty("decreaseremainingon")
    private String decreaseremainingon;
    /**
     * entitytype
     */
    @TableField(value = "entitytype")
    @JSONField(name = "entitytype")
    @JsonProperty("entitytype")
    private String entitytype;
    /**
     * 联系人
     */
    @TableField(value = "contactname")
    @JSONField(name = "contactname")
    @JsonProperty("contactname")
    private String contactname;
    /**
     * Import Sequence Number
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * 结束日期
     */
    @TableField(value = "enddate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "enddate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("enddate")
    private Timestamp enddate;
    /**
     * 状态
     */
    @TableField(value = "statecode")
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 客户
     */
    @TableField(value = "customername")
    @JSONField(name = "customername")
    @JsonProperty("customername")
    private String customername;
    /**
     * KB 访问级别
     */
    @TableField(value = "kbaccesslevel")
    @JSONField(name = "kbaccesslevel")
    @JsonProperty("kbaccesslevel")
    private String kbaccesslevel;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * Email Address
     */
    @TableField(value = "emailaddress")
    @JSONField(name = "emailaddress")
    @JsonProperty("emailaddress")
    private String emailaddress;
    /**
     * Time Zone Rule Version Number
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * 汇率
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * 客户类型
     */
    @TableField(value = "customertype")
    @JSONField(name = "customertype")
    @JsonProperty("customertype")
    private String customertype;
    /**
     * Version Number
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 负责人
     */
    @TableField(value = "ownerid")
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;
    /**
     * UTC Conversion Time Zone Code
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 分配类型
     */
    @TableField(value = "allocationtypecode")
    @JSONField(name = "allocationtypecode")
    @JsonProperty("allocationtypecode")
    private String allocationtypecode;
    /**
     * 状态代码
     */
    @TableField(value = "statuscode")
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;
    /**
     * 总期数
     */
    @TableField(value = "totalterms")
    @JSONField(name = "totalterms")
    @JsonProperty("totalterms")
    private BigDecimal totalterms;
    /**
     * 客户
     */
    @TableField(value = "customerid")
    @JSONField(name = "customerid")
    @JsonProperty("customerid")
    private String customerid;
    /**
     * 负责人
     */
    @TableField(value = "ownername")
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;
    /**
     * 根据权利期限进行限制
     */
    @DEField(defaultValue = "0")
    @TableField(value = "restrictcasecreation")
    @JSONField(name = "restrictcasecreation")
    @JsonProperty("restrictcasecreation")
    private Integer restrictcasecreation;
    /**
     * 权利
     */
    @DEField(isKeyField=true)
    @TableId(value= "entitlementid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "entitlementid")
    @JsonProperty("entitlementid")
    private String entitlementid;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 剩余期数
     */
    @TableField(value = "remainingterms")
    @JSONField(name = "remainingterms")
    @JsonProperty("remainingterms")
    private BigDecimal remainingterms;
    /**
     * 客户
     */
    @TableField(value = "accountname")
    @JSONField(name = "accountname")
    @JsonProperty("accountname")
    private String accountname;
    /**
     * Record Created On
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 负责人类型
     */
    @TableField(value = "ownertype")
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;
    /**
     * Stage Id
     */
    @TableField(value = "stageid")
    @JSONField(name = "stageid")
    @JsonProperty("stageid")
    private String stageid;
    /**
     * 授权名称
     */
    @TableField(value = "entitlementname")
    @JSONField(name = "entitlementname")
    @JsonProperty("entitlementname")
    private String entitlementname;
    /**
     * 是否默认
     */
    @DEField(defaultValue = "0")
    @TableField(value = "default")
    @JSONField(name = "ibizdefault")
    @JsonProperty("ibizdefault")
    private Integer ibizdefault;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 货币
     */
    @TableField(value = "transactioncurrencyid")
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;
    /**
     * SLA
     */
    @TableField(value = "slaid")
    @JSONField(name = "slaid")
    @JsonProperty("slaid")
    private String slaid;
    /**
     * 权利模板
     */
    @TableField(value = "entitlementtemplateid")
    @JSONField(name = "entitlementtemplateid")
    @JsonProperty("entitlementtemplateid")
    private String entitlementtemplateid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.service.domain.EntitlementTemplate entitlementtemplate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Sla sla;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency;



    /**
     * 设置 [开始日期]
     */
    public void setStartdate(Timestamp startdate){
        this.startdate = startdate ;
        this.modify("startdate",startdate);
    }

    /**
     * 格式化日期 [开始日期]
     */
    public String formatStartdate(){
        if (this.startdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(startdate);
    }
    /**
     * 设置 [Process Id]
     */
    public void setProcessid(String processid){
        this.processid = processid ;
        this.modify("processid",processid);
    }

    /**
     * 设置 [Traversed Path]
     */
    public void setTraversedpath(String traversedpath){
        this.traversedpath = traversedpath ;
        this.modify("traversedpath",traversedpath);
    }

    /**
     * 设置 [缩短剩余期限]
     */
    public void setDecreaseremainingon(String decreaseremainingon){
        this.decreaseremainingon = decreaseremainingon ;
        this.modify("decreaseremainingon",decreaseremainingon);
    }

    /**
     * 设置 [entitytype]
     */
    public void setEntitytype(String entitytype){
        this.entitytype = entitytype ;
        this.modify("entitytype",entitytype);
    }

    /**
     * 设置 [联系人]
     */
    public void setContactname(String contactname){
        this.contactname = contactname ;
        this.modify("contactname",contactname);
    }

    /**
     * 设置 [Import Sequence Number]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [结束日期]
     */
    public void setEnddate(Timestamp enddate){
        this.enddate = enddate ;
        this.modify("enddate",enddate);
    }

    /**
     * 格式化日期 [结束日期]
     */
    public String formatEnddate(){
        if (this.enddate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(enddate);
    }
    /**
     * 设置 [状态]
     */
    public void setStatecode(Integer statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [客户]
     */
    public void setCustomername(String customername){
        this.customername = customername ;
        this.modify("customername",customername);
    }

    /**
     * 设置 [KB 访问级别]
     */
    public void setKbaccesslevel(String kbaccesslevel){
        this.kbaccesslevel = kbaccesslevel ;
        this.modify("kbaccesslevel",kbaccesslevel);
    }

    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [Email Address]
     */
    public void setEmailaddress(String emailaddress){
        this.emailaddress = emailaddress ;
        this.modify("emailaddress",emailaddress);
    }

    /**
     * 设置 [Time Zone Rule Version Number]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [汇率]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [客户类型]
     */
    public void setCustomertype(String customertype){
        this.customertype = customertype ;
        this.modify("customertype",customertype);
    }

    /**
     * 设置 [Version Number]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnerid(String ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [UTC Conversion Time Zone Code]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [分配类型]
     */
    public void setAllocationtypecode(String allocationtypecode){
        this.allocationtypecode = allocationtypecode ;
        this.modify("allocationtypecode",allocationtypecode);
    }

    /**
     * 设置 [状态代码]
     */
    public void setStatuscode(Integer statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [总期数]
     */
    public void setTotalterms(BigDecimal totalterms){
        this.totalterms = totalterms ;
        this.modify("totalterms",totalterms);
    }

    /**
     * 设置 [客户]
     */
    public void setCustomerid(String customerid){
        this.customerid = customerid ;
        this.modify("customerid",customerid);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnername(String ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [根据权利期限进行限制]
     */
    public void setRestrictcasecreation(Integer restrictcasecreation){
        this.restrictcasecreation = restrictcasecreation ;
        this.modify("restrictcasecreation",restrictcasecreation);
    }

    /**
     * 设置 [剩余期数]
     */
    public void setRemainingterms(BigDecimal remainingterms){
        this.remainingterms = remainingterms ;
        this.modify("remainingterms",remainingterms);
    }

    /**
     * 设置 [客户]
     */
    public void setAccountname(String accountname){
        this.accountname = accountname ;
        this.modify("accountname",accountname);
    }

    /**
     * 设置 [Record Created On]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [Record Created On]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [负责人类型]
     */
    public void setOwnertype(String ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [Stage Id]
     */
    public void setStageid(String stageid){
        this.stageid = stageid ;
        this.modify("stageid",stageid);
    }

    /**
     * 设置 [授权名称]
     */
    public void setEntitlementname(String entitlementname){
        this.entitlementname = entitlementname ;
        this.modify("entitlementname",entitlementname);
    }

    /**
     * 设置 [是否默认]
     */
    public void setIbizdefault(Integer ibizdefault){
        this.ibizdefault = ibizdefault ;
        this.modify("default",ibizdefault);
    }

    /**
     * 设置 [货币]
     */
    public void setTransactioncurrencyid(String transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }

    /**
     * 设置 [SLA]
     */
    public void setSlaid(String slaid){
        this.slaid = slaid ;
        this.modify("slaid",slaid);
    }

    /**
     * 设置 [权利模板]
     */
    public void setEntitlementtemplateid(String entitlementtemplateid){
        this.entitlementtemplateid = entitlementtemplateid ;
        this.modify("entitlementtemplateid",entitlementtemplateid);
    }


}



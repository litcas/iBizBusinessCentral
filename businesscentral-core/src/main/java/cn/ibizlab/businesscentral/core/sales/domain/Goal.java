package cn.ibizlab.businesscentral.core.sales.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[目标]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "GOAL",resultMap = "GoalResultMap")
public class Goal extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 过程值(十进制)
     */
    @TableField(value = "inprogressdecimal")
    @JSONField(name = "inprogressdecimal")
    @JsonProperty("inprogressdecimal")
    private BigDecimal inprogressdecimal;
    /**
     * 目标
     */
    @DEField(isKeyField=true)
    @TableId(value= "goalid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "goalid")
    @JsonProperty("goalid")
    private String goalid;
    /**
     * 负责人 ID 类型
     */
    @TableField(value = "ownertype")
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;
    /**
     * 扩展目标值(金额)
     */
    @TableField(value = "stretchtargetmoney")
    @JSONField(name = "stretchtargetmoney")
    @JsonProperty("stretchtargetmoney")
    private BigDecimal stretchtargetmoney;
    /**
     * 实际值(十进制)
     */
    @TableField(value = "actualdecimal")
    @JSONField(name = "actualdecimal")
    @JsonProperty("actualdecimal")
    private BigDecimal actualdecimal;
    /**
     * 今天的目标值(整数)
     */
    @TableField(value = "computedtargetasoftodayinteger")
    @JSONField(name = "computedtargetasoftodayinteger")
    @JsonProperty("computedtargetasoftodayinteger")
    private Integer computedtargetasoftodayinteger;
    /**
     * 时区规则版本号
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * 汇率
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * 实体图像 ID
     */
    @TableField(value = "entityimageid")
    @JSONField(name = "entityimageid")
    @JsonProperty("entityimageid")
    private String entityimageid;
    /**
     * 目标值(整数)
     */
    @TableField(value = "targetinteger")
    @JSONField(name = "targetinteger")
    @JsonProperty("targetinteger")
    private Integer targetinteger;
    /**
     * 自定义汇总字段(金额) (基础)
     */
    @DEField(name = "customrollupfieldmoney_base")
    @TableField(value = "customrollupfieldmoney_base")
    @JSONField(name = "customrollupfieldmoney_base")
    @JsonProperty("customrollupfieldmoney_base")
    private BigDecimal customrollupfieldmoneyBase;
    /**
     * EntityImage_URL
     */
    @DEField(name = "entityimage_url")
    @TableField(value = "entityimage_url")
    @JSONField(name = "entityimage_url")
    @JsonProperty("entityimage_url")
    private String entityimageUrl;
    /**
     * 实际值(金额) (基础)
     */
    @DEField(name = "actualmoney_base")
    @TableField(value = "actualmoney_base")
    @JSONField(name = "actualmoney_base")
    @JsonProperty("actualmoney_base")
    private BigDecimal actualmoneyBase;
    /**
     * UTC 转换时区代码
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * 过程值(整数)
     */
    @TableField(value = "inprogressinteger")
    @JSONField(name = "inprogressinteger")
    @JsonProperty("inprogressinteger")
    private Integer inprogressinteger;
    /**
     * 状态
     */
    @TableField(value = "statecode")
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;
    /**
     * 目标值(金额) (基础)
     */
    @DEField(name = "targetmoney_base")
    @TableField(value = "targetmoney_base")
    @JSONField(name = "targetmoney_base")
    @JsonProperty("targetmoney_base")
    private BigDecimal targetmoneyBase;
    /**
     * 过程值
     */
    @TableField(value = "inprogressstring")
    @JSONField(name = "inprogressstring")
    @JsonProperty("inprogressstring")
    private String inprogressstring;
    /**
     * EntityImage_Timestamp
     */
    @DEField(name = "entityimage_timestamp")
    @TableField(value = "entityimage_timestamp")
    @JSONField(name = "entityimage_timestamp")
    @JsonProperty("entityimage_timestamp")
    private BigInteger entityimageTimestamp;
    /**
     * 从
     */
    @TableField(value = "goalstartdate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "goalstartdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("goalstartdate")
    private Timestamp goalstartdate;
    /**
     * 已实现百分比
     */
    @TableField(value = "percentage")
    @JSONField(name = "percentage")
    @JsonProperty("percentage")
    private BigDecimal percentage;
    /**
     * 经理
     */
    @TableField(value = "ownerid")
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;
    /**
     * 目标值(十进制)
     */
    @TableField(value = "targetdecimal")
    @JSONField(name = "targetdecimal")
    @JsonProperty("targetdecimal")
    private BigDecimal targetdecimal;
    /**
     * 度量类型
     */
    @DEField(defaultValue = "1")
    @TableField(value = "amount")
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Integer amount;
    /**
     * 状态描述
     */
    @TableField(value = "statuscode")
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;
    /**
     * 金额数据类型
     */
    @TableField(value = "amountdatatype")
    @JSONField(name = "amountdatatype")
    @JsonProperty("amountdatatype")
    private String amountdatatype;
    /**
     * 自定义汇总字段(整数)
     */
    @TableField(value = "customrollupfieldinteger")
    @JSONField(name = "customrollupfieldinteger")
    @JsonProperty("customrollupfieldinteger")
    private Integer customrollupfieldinteger;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 负责人
     */
    @TableField(value = "goalownername")
    @JSONField(name = "goalownername")
    @JsonProperty("goalownername")
    private String goalownername;
    /**
     * 过程值(金额)
     */
    @TableField(value = "inprogressmoney")
    @JSONField(name = "inprogressmoney")
    @JsonProperty("inprogressmoney")
    private BigDecimal inprogressmoney;
    /**
     * 会计年度
     */
    @TableField(value = "fiscalyear")
    @JSONField(name = "fiscalyear")
    @JsonProperty("fiscalyear")
    private String fiscalyear;
    /**
     * 会计期间
     */
    @TableField(value = "fiscalperiod")
    @JSONField(name = "fiscalperiod")
    @JsonProperty("fiscalperiod")
    private String fiscalperiod;
    /**
     * 名称
     */
    @TableField(value = "title")
    @JSONField(name = "title")
    @JsonProperty("title")
    private String title;
    /**
     * 扩展目标值(十进制)
     */
    @TableField(value = "stretchtargetdecimal")
    @JSONField(name = "stretchtargetdecimal")
    @JsonProperty("stretchtargetdecimal")
    private BigDecimal stretchtargetdecimal;
    /**
     * 自定义汇总字段(十进制)
     */
    @TableField(value = "customrollupfielddecimal")
    @JSONField(name = "customrollupfielddecimal")
    @JsonProperty("customrollupfielddecimal")
    private BigDecimal customrollupfielddecimal;
    /**
     * 供汇总的记录集
     */
    @DEField(defaultValue = "1")
    @TableField(value = "consideronlygoalownersrecords")
    @JSONField(name = "consideronlygoalownersrecords")
    @JsonProperty("consideronlygoalownersrecords")
    private Integer consideronlygoalownersrecords;
    /**
     * 创建记录的时间
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 实际值(金额)
     */
    @TableField(value = "actualmoney")
    @JSONField(name = "actualmoney")
    @JsonProperty("actualmoney")
    private BigDecimal actualmoney;
    /**
     * 负责人
     */
    @TableField(value = "ownername")
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;
    /**
     * 仅从子目标汇总
     */
    @DEField(defaultValue = "0")
    @TableField(value = "rolluponlyfromchildgoals")
    @JSONField(name = "rolluponlyfromchildgoals")
    @JsonProperty("rolluponlyfromchildgoals")
    private Integer rolluponlyfromchildgoals;
    /**
     * 到
     */
    @TableField(value = "goalenddate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "goalenddate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("goalenddate")
    private Timestamp goalenddate;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 自定义汇总字段(金额)
     */
    @TableField(value = "customrollupfieldmoney")
    @JSONField(name = "customrollupfieldmoney")
    @JsonProperty("customrollupfieldmoney")
    private BigDecimal customrollupfieldmoney;
    /**
     * 汇总错误代码
     */
    @TableField(value = "rolluperrorcode")
    @JSONField(name = "rolluperrorcode")
    @JsonProperty("rolluperrorcode")
    private Integer rolluperrorcode;
    /**
     * 版本号
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 扩展目标值(金额) (基础)
     */
    @DEField(name = "stretchtargetmoney_base")
    @TableField(value = "stretchtargetmoney_base")
    @JSONField(name = "stretchtargetmoney_base")
    @JsonProperty("stretchtargetmoney_base")
    private BigDecimal stretchtargetmoneyBase;
    /**
     * 树 ID
     */
    @TableField(value = "treeid")
    @JSONField(name = "treeid")
    @JsonProperty("treeid")
    private String treeid;
    /**
     * 目标负责人类型
     */
    @TableField(value = "goalownertype")
    @JSONField(name = "goalownertype")
    @JsonProperty("goalownertype")
    private String goalownertype;
    /**
     * 实体图像
     */
    @TableField(value = "entityimage")
    @JSONField(name = "entityimage")
    @JsonProperty("entityimage")
    private String entityimage;
    /**
     * 已扩展目标值
     */
    @TableField(value = "stretchtargetstring")
    @JSONField(name = "stretchtargetstring")
    @JsonProperty("stretchtargetstring")
    private String stretchtargetstring;
    /**
     * 扩展目标值(整数)
     */
    @TableField(value = "stretchtargetinteger")
    @JSONField(name = "stretchtargetinteger")
    @JsonProperty("stretchtargetinteger")
    private Integer stretchtargetinteger;
    /**
     * 目标期间类型
     */
    @DEField(defaultValue = "1")
    @TableField(value = "fiscalperiodgoal")
    @JSONField(name = "fiscalperiodgoal")
    @JsonProperty("fiscalperiodgoal")
    private Integer fiscalperiodgoal;
    /**
     * 过程值(金额) (基础)
     */
    @DEField(name = "inprogressmoney_base")
    @TableField(value = "inprogressmoney_base")
    @JSONField(name = "inprogressmoney_base")
    @JsonProperty("inprogressmoney_base")
    private BigDecimal inprogressmoneyBase;
    /**
     * 已替代
     */
    @DEField(defaultValue = "0")
    @TableField(value = "overridden")
    @JSONField(name = "overridden")
    @JsonProperty("overridden")
    private Integer overridden;
    /**
     * 实际
     */
    @TableField(value = "actualstring")
    @JSONField(name = "actualstring")
    @JsonProperty("actualstring")
    private String actualstring;
    /**
     * 实际值(整数)
     */
    @TableField(value = "actualinteger")
    @JSONField(name = "actualinteger")
    @JsonProperty("actualinteger")
    private Integer actualinteger;
    /**
     * 导入序列号
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * 目标负责人
     */
    @TableField(value = "goalownerid")
    @JSONField(name = "goalownerid")
    @JsonProperty("goalownerid")
    private String goalownerid;
    /**
     * 今天的目标值(十进制)
     */
    @TableField(value = "computedtargetasoftodaydecimal")
    @JSONField(name = "computedtargetasoftodaydecimal")
    @JsonProperty("computedtargetasoftodaydecimal")
    private BigDecimal computedtargetasoftodaydecimal;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 目标
     */
    @TableField(value = "targetstring")
    @JSONField(name = "targetstring")
    @JsonProperty("targetstring")
    private String targetstring;
    /**
     * 自定义汇总字段
     */
    @TableField(value = "customrollupfieldstring")
    @JSONField(name = "customrollupfieldstring")
    @JsonProperty("customrollupfieldstring")
    private String customrollupfieldstring;
    /**
     * 目标值(金额)
     */
    @TableField(value = "targetmoney")
    @JSONField(name = "targetmoney")
    @JsonProperty("targetmoney")
    private BigDecimal targetmoney;
    /**
     * 上次汇总日期
     */
    @TableField(value = "lastrolledupdate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "lastrolledupdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("lastrolledupdate")
    private Timestamp lastrolledupdate;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 替代
     */
    @DEField(defaultValue = "0")
    @TableField(value = "override")
    @JSONField(name = "override")
    @JsonProperty("override")
    private Integer override;
    /**
     * 今天的目标值(金额)
     */
    @TableField(value = "computedtargetasoftodaymoney")
    @JSONField(name = "computedtargetasoftodaymoney")
    @JsonProperty("computedtargetasoftodaymoney")
    private BigDecimal computedtargetasoftodaymoney;
    /**
     * 目标度量
     */
    @TableField(value = "metricname")
    @JSONField(name = "metricname")
    @JsonProperty("metricname")
    private String metricname;
    /**
     * 上级目标
     */
    @TableField(value = "parentgoalname")
    @JSONField(name = "parentgoalname")
    @JsonProperty("parentgoalname")
    private String parentgoalname;
    /**
     * 上级目标
     */
    @TableField(value = "parentgoalid")
    @JSONField(name = "parentgoalid")
    @JsonProperty("parentgoalid")
    private String parentgoalid;
    /**
     * 汇总查询 - 过程值(金额)
     */
    @TableField(value = "rollupqueryinprogressmoneyid")
    @JSONField(name = "rollupqueryinprogressmoneyid")
    @JsonProperty("rollupqueryinprogressmoneyid")
    private String rollupqueryinprogressmoneyid;
    /**
     * 汇总查询 - 自定义汇总字段(金额)
     */
    @TableField(value = "rollupquerycustommoneyid")
    @JSONField(name = "rollupquerycustommoneyid")
    @JsonProperty("rollupquerycustommoneyid")
    private String rollupquerycustommoneyid;
    /**
     * 汇总查询 - 过程值(整数)
     */
    @TableField(value = "rollupqueryinprogressintegerid")
    @JSONField(name = "rollupqueryinprogressintegerid")
    @JsonProperty("rollupqueryinprogressintegerid")
    private String rollupqueryinprogressintegerid;
    /**
     * 汇总查询 - 自定义汇总字段(十进制)
     */
    @TableField(value = "rollupquerycustomdecimalid")
    @JSONField(name = "rollupquerycustomdecimalid")
    @JsonProperty("rollupquerycustomdecimalid")
    private String rollupquerycustomdecimalid;
    /**
     * 汇总查询 - 实际值(金额)
     */
    @TableField(value = "rollupqueryactualmoneyid")
    @JSONField(name = "rollupqueryactualmoneyid")
    @JsonProperty("rollupqueryactualmoneyid")
    private String rollupqueryactualmoneyid;
    /**
     * 汇总查询 - 自定义汇总字段(整数)
     */
    @TableField(value = "rollupquerycustomintegerid")
    @JSONField(name = "rollupquerycustomintegerid")
    @JsonProperty("rollupquerycustomintegerid")
    private String rollupquerycustomintegerid;
    /**
     * 出现错误的目标
     */
    @TableField(value = "goalwitherrorid")
    @JSONField(name = "goalwitherrorid")
    @JsonProperty("goalwitherrorid")
    private String goalwitherrorid;
    /**
     * 目标度量
     */
    @TableField(value = "metricid")
    @JSONField(name = "metricid")
    @JsonProperty("metricid")
    private String metricid;
    /**
     * 汇总查询 - 过程值(十进制)
     */
    @TableField(value = "rollupqueryinprogressdecimalid")
    @JSONField(name = "rollupqueryinprogressdecimalid")
    @JsonProperty("rollupqueryinprogressdecimalid")
    private String rollupqueryinprogressdecimalid;
    /**
     * 汇总查询 - 实际值(整数)
     */
    @TableField(value = "rollupqueryactualintegerid")
    @JSONField(name = "rollupqueryactualintegerid")
    @JsonProperty("rollupqueryactualintegerid")
    private String rollupqueryactualintegerid;
    /**
     * 汇总查询 - 实际值(十进制)
     */
    @TableField(value = "rollupqueryactualdecimalid")
    @JSONField(name = "rollupqueryactualdecimalid")
    @JsonProperty("rollupqueryactualdecimalid")
    private String rollupqueryactualdecimalid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.sales.domain.GoalRollupQuery rollupqueryactualdecimal;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.sales.domain.GoalRollupQuery rollupqueryactualinteger;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.sales.domain.GoalRollupQuery rollupqueryactualmoney;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.sales.domain.GoalRollupQuery rollupquerycustomdecimal;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.sales.domain.GoalRollupQuery rollupquerycustominteger;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.sales.domain.GoalRollupQuery rollupquerycustommoney;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.sales.domain.GoalRollupQuery rollupqueryinprogressdecimal;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.sales.domain.GoalRollupQuery rollupqueryinprogressinteger;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.sales.domain.GoalRollupQuery rollupqueryinprogressmoney;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.sales.domain.Goal goalwitherror;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.sales.domain.Goal parentgoal;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Metric metric;



    /**
     * 设置 [过程值(十进制)]
     */
    public void setInprogressdecimal(BigDecimal inprogressdecimal){
        this.inprogressdecimal = inprogressdecimal ;
        this.modify("inprogressdecimal",inprogressdecimal);
    }

    /**
     * 设置 [负责人 ID 类型]
     */
    public void setOwnertype(String ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [扩展目标值(金额)]
     */
    public void setStretchtargetmoney(BigDecimal stretchtargetmoney){
        this.stretchtargetmoney = stretchtargetmoney ;
        this.modify("stretchtargetmoney",stretchtargetmoney);
    }

    /**
     * 设置 [实际值(十进制)]
     */
    public void setActualdecimal(BigDecimal actualdecimal){
        this.actualdecimal = actualdecimal ;
        this.modify("actualdecimal",actualdecimal);
    }

    /**
     * 设置 [今天的目标值(整数)]
     */
    public void setComputedtargetasoftodayinteger(Integer computedtargetasoftodayinteger){
        this.computedtargetasoftodayinteger = computedtargetasoftodayinteger ;
        this.modify("computedtargetasoftodayinteger",computedtargetasoftodayinteger);
    }

    /**
     * 设置 [时区规则版本号]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [汇率]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [实体图像 ID]
     */
    public void setEntityimageid(String entityimageid){
        this.entityimageid = entityimageid ;
        this.modify("entityimageid",entityimageid);
    }

    /**
     * 设置 [目标值(整数)]
     */
    public void setTargetinteger(Integer targetinteger){
        this.targetinteger = targetinteger ;
        this.modify("targetinteger",targetinteger);
    }

    /**
     * 设置 [自定义汇总字段(金额) (基础)]
     */
    public void setCustomrollupfieldmoneyBase(BigDecimal customrollupfieldmoneyBase){
        this.customrollupfieldmoneyBase = customrollupfieldmoneyBase ;
        this.modify("customrollupfieldmoney_base",customrollupfieldmoneyBase);
    }

    /**
     * 设置 [EntityImage_URL]
     */
    public void setEntityimageUrl(String entityimageUrl){
        this.entityimageUrl = entityimageUrl ;
        this.modify("entityimage_url",entityimageUrl);
    }

    /**
     * 设置 [实际值(金额) (基础)]
     */
    public void setActualmoneyBase(BigDecimal actualmoneyBase){
        this.actualmoneyBase = actualmoneyBase ;
        this.modify("actualmoney_base",actualmoneyBase);
    }

    /**
     * 设置 [UTC 转换时区代码]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [过程值(整数)]
     */
    public void setInprogressinteger(Integer inprogressinteger){
        this.inprogressinteger = inprogressinteger ;
        this.modify("inprogressinteger",inprogressinteger);
    }

    /**
     * 设置 [状态]
     */
    public void setStatecode(Integer statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [目标值(金额) (基础)]
     */
    public void setTargetmoneyBase(BigDecimal targetmoneyBase){
        this.targetmoneyBase = targetmoneyBase ;
        this.modify("targetmoney_base",targetmoneyBase);
    }

    /**
     * 设置 [过程值]
     */
    public void setInprogressstring(String inprogressstring){
        this.inprogressstring = inprogressstring ;
        this.modify("inprogressstring",inprogressstring);
    }

    /**
     * 设置 [EntityImage_Timestamp]
     */
    public void setEntityimageTimestamp(BigInteger entityimageTimestamp){
        this.entityimageTimestamp = entityimageTimestamp ;
        this.modify("entityimage_timestamp",entityimageTimestamp);
    }

    /**
     * 设置 [从]
     */
    public void setGoalstartdate(Timestamp goalstartdate){
        this.goalstartdate = goalstartdate ;
        this.modify("goalstartdate",goalstartdate);
    }

    /**
     * 格式化日期 [从]
     */
    public String formatGoalstartdate(){
        if (this.goalstartdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(goalstartdate);
    }
    /**
     * 设置 [已实现百分比]
     */
    public void setPercentage(BigDecimal percentage){
        this.percentage = percentage ;
        this.modify("percentage",percentage);
    }

    /**
     * 设置 [经理]
     */
    public void setOwnerid(String ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [目标值(十进制)]
     */
    public void setTargetdecimal(BigDecimal targetdecimal){
        this.targetdecimal = targetdecimal ;
        this.modify("targetdecimal",targetdecimal);
    }

    /**
     * 设置 [度量类型]
     */
    public void setAmount(Integer amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }

    /**
     * 设置 [状态描述]
     */
    public void setStatuscode(Integer statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [金额数据类型]
     */
    public void setAmountdatatype(String amountdatatype){
        this.amountdatatype = amountdatatype ;
        this.modify("amountdatatype",amountdatatype);
    }

    /**
     * 设置 [自定义汇总字段(整数)]
     */
    public void setCustomrollupfieldinteger(Integer customrollupfieldinteger){
        this.customrollupfieldinteger = customrollupfieldinteger ;
        this.modify("customrollupfieldinteger",customrollupfieldinteger);
    }

    /**
     * 设置 [负责人]
     */
    public void setGoalownername(String goalownername){
        this.goalownername = goalownername ;
        this.modify("goalownername",goalownername);
    }

    /**
     * 设置 [过程值(金额)]
     */
    public void setInprogressmoney(BigDecimal inprogressmoney){
        this.inprogressmoney = inprogressmoney ;
        this.modify("inprogressmoney",inprogressmoney);
    }

    /**
     * 设置 [会计年度]
     */
    public void setFiscalyear(String fiscalyear){
        this.fiscalyear = fiscalyear ;
        this.modify("fiscalyear",fiscalyear);
    }

    /**
     * 设置 [会计期间]
     */
    public void setFiscalperiod(String fiscalperiod){
        this.fiscalperiod = fiscalperiod ;
        this.modify("fiscalperiod",fiscalperiod);
    }

    /**
     * 设置 [名称]
     */
    public void setTitle(String title){
        this.title = title ;
        this.modify("title",title);
    }

    /**
     * 设置 [扩展目标值(十进制)]
     */
    public void setStretchtargetdecimal(BigDecimal stretchtargetdecimal){
        this.stretchtargetdecimal = stretchtargetdecimal ;
        this.modify("stretchtargetdecimal",stretchtargetdecimal);
    }

    /**
     * 设置 [自定义汇总字段(十进制)]
     */
    public void setCustomrollupfielddecimal(BigDecimal customrollupfielddecimal){
        this.customrollupfielddecimal = customrollupfielddecimal ;
        this.modify("customrollupfielddecimal",customrollupfielddecimal);
    }

    /**
     * 设置 [供汇总的记录集]
     */
    public void setConsideronlygoalownersrecords(Integer consideronlygoalownersrecords){
        this.consideronlygoalownersrecords = consideronlygoalownersrecords ;
        this.modify("consideronlygoalownersrecords",consideronlygoalownersrecords);
    }

    /**
     * 设置 [创建记录的时间]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [创建记录的时间]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [实际值(金额)]
     */
    public void setActualmoney(BigDecimal actualmoney){
        this.actualmoney = actualmoney ;
        this.modify("actualmoney",actualmoney);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnername(String ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [仅从子目标汇总]
     */
    public void setRolluponlyfromchildgoals(Integer rolluponlyfromchildgoals){
        this.rolluponlyfromchildgoals = rolluponlyfromchildgoals ;
        this.modify("rolluponlyfromchildgoals",rolluponlyfromchildgoals);
    }

    /**
     * 设置 [到]
     */
    public void setGoalenddate(Timestamp goalenddate){
        this.goalenddate = goalenddate ;
        this.modify("goalenddate",goalenddate);
    }

    /**
     * 格式化日期 [到]
     */
    public String formatGoalenddate(){
        if (this.goalenddate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(goalenddate);
    }
    /**
     * 设置 [自定义汇总字段(金额)]
     */
    public void setCustomrollupfieldmoney(BigDecimal customrollupfieldmoney){
        this.customrollupfieldmoney = customrollupfieldmoney ;
        this.modify("customrollupfieldmoney",customrollupfieldmoney);
    }

    /**
     * 设置 [汇总错误代码]
     */
    public void setRolluperrorcode(Integer rolluperrorcode){
        this.rolluperrorcode = rolluperrorcode ;
        this.modify("rolluperrorcode",rolluperrorcode);
    }

    /**
     * 设置 [版本号]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [扩展目标值(金额) (基础)]
     */
    public void setStretchtargetmoneyBase(BigDecimal stretchtargetmoneyBase){
        this.stretchtargetmoneyBase = stretchtargetmoneyBase ;
        this.modify("stretchtargetmoney_base",stretchtargetmoneyBase);
    }

    /**
     * 设置 [树 ID]
     */
    public void setTreeid(String treeid){
        this.treeid = treeid ;
        this.modify("treeid",treeid);
    }

    /**
     * 设置 [目标负责人类型]
     */
    public void setGoalownertype(String goalownertype){
        this.goalownertype = goalownertype ;
        this.modify("goalownertype",goalownertype);
    }

    /**
     * 设置 [实体图像]
     */
    public void setEntityimage(String entityimage){
        this.entityimage = entityimage ;
        this.modify("entityimage",entityimage);
    }

    /**
     * 设置 [已扩展目标值]
     */
    public void setStretchtargetstring(String stretchtargetstring){
        this.stretchtargetstring = stretchtargetstring ;
        this.modify("stretchtargetstring",stretchtargetstring);
    }

    /**
     * 设置 [扩展目标值(整数)]
     */
    public void setStretchtargetinteger(Integer stretchtargetinteger){
        this.stretchtargetinteger = stretchtargetinteger ;
        this.modify("stretchtargetinteger",stretchtargetinteger);
    }

    /**
     * 设置 [目标期间类型]
     */
    public void setFiscalperiodgoal(Integer fiscalperiodgoal){
        this.fiscalperiodgoal = fiscalperiodgoal ;
        this.modify("fiscalperiodgoal",fiscalperiodgoal);
    }

    /**
     * 设置 [过程值(金额) (基础)]
     */
    public void setInprogressmoneyBase(BigDecimal inprogressmoneyBase){
        this.inprogressmoneyBase = inprogressmoneyBase ;
        this.modify("inprogressmoney_base",inprogressmoneyBase);
    }

    /**
     * 设置 [已替代]
     */
    public void setOverridden(Integer overridden){
        this.overridden = overridden ;
        this.modify("overridden",overridden);
    }

    /**
     * 设置 [实际]
     */
    public void setActualstring(String actualstring){
        this.actualstring = actualstring ;
        this.modify("actualstring",actualstring);
    }

    /**
     * 设置 [实际值(整数)]
     */
    public void setActualinteger(Integer actualinteger){
        this.actualinteger = actualinteger ;
        this.modify("actualinteger",actualinteger);
    }

    /**
     * 设置 [导入序列号]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [目标负责人]
     */
    public void setGoalownerid(String goalownerid){
        this.goalownerid = goalownerid ;
        this.modify("goalownerid",goalownerid);
    }

    /**
     * 设置 [今天的目标值(十进制)]
     */
    public void setComputedtargetasoftodaydecimal(BigDecimal computedtargetasoftodaydecimal){
        this.computedtargetasoftodaydecimal = computedtargetasoftodaydecimal ;
        this.modify("computedtargetasoftodaydecimal",computedtargetasoftodaydecimal);
    }

    /**
     * 设置 [目标]
     */
    public void setTargetstring(String targetstring){
        this.targetstring = targetstring ;
        this.modify("targetstring",targetstring);
    }

    /**
     * 设置 [自定义汇总字段]
     */
    public void setCustomrollupfieldstring(String customrollupfieldstring){
        this.customrollupfieldstring = customrollupfieldstring ;
        this.modify("customrollupfieldstring",customrollupfieldstring);
    }

    /**
     * 设置 [目标值(金额)]
     */
    public void setTargetmoney(BigDecimal targetmoney){
        this.targetmoney = targetmoney ;
        this.modify("targetmoney",targetmoney);
    }

    /**
     * 设置 [上次汇总日期]
     */
    public void setLastrolledupdate(Timestamp lastrolledupdate){
        this.lastrolledupdate = lastrolledupdate ;
        this.modify("lastrolledupdate",lastrolledupdate);
    }

    /**
     * 格式化日期 [上次汇总日期]
     */
    public String formatLastrolledupdate(){
        if (this.lastrolledupdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(lastrolledupdate);
    }
    /**
     * 设置 [替代]
     */
    public void setOverride(Integer override){
        this.override = override ;
        this.modify("override",override);
    }

    /**
     * 设置 [今天的目标值(金额)]
     */
    public void setComputedtargetasoftodaymoney(BigDecimal computedtargetasoftodaymoney){
        this.computedtargetasoftodaymoney = computedtargetasoftodaymoney ;
        this.modify("computedtargetasoftodaymoney",computedtargetasoftodaymoney);
    }

    /**
     * 设置 [目标度量]
     */
    public void setMetricname(String metricname){
        this.metricname = metricname ;
        this.modify("metricname",metricname);
    }

    /**
     * 设置 [上级目标]
     */
    public void setParentgoalname(String parentgoalname){
        this.parentgoalname = parentgoalname ;
        this.modify("parentgoalname",parentgoalname);
    }

    /**
     * 设置 [上级目标]
     */
    public void setParentgoalid(String parentgoalid){
        this.parentgoalid = parentgoalid ;
        this.modify("parentgoalid",parentgoalid);
    }

    /**
     * 设置 [汇总查询 - 过程值(金额)]
     */
    public void setRollupqueryinprogressmoneyid(String rollupqueryinprogressmoneyid){
        this.rollupqueryinprogressmoneyid = rollupqueryinprogressmoneyid ;
        this.modify("rollupqueryinprogressmoneyid",rollupqueryinprogressmoneyid);
    }

    /**
     * 设置 [汇总查询 - 自定义汇总字段(金额)]
     */
    public void setRollupquerycustommoneyid(String rollupquerycustommoneyid){
        this.rollupquerycustommoneyid = rollupquerycustommoneyid ;
        this.modify("rollupquerycustommoneyid",rollupquerycustommoneyid);
    }

    /**
     * 设置 [汇总查询 - 过程值(整数)]
     */
    public void setRollupqueryinprogressintegerid(String rollupqueryinprogressintegerid){
        this.rollupqueryinprogressintegerid = rollupqueryinprogressintegerid ;
        this.modify("rollupqueryinprogressintegerid",rollupqueryinprogressintegerid);
    }

    /**
     * 设置 [汇总查询 - 自定义汇总字段(十进制)]
     */
    public void setRollupquerycustomdecimalid(String rollupquerycustomdecimalid){
        this.rollupquerycustomdecimalid = rollupquerycustomdecimalid ;
        this.modify("rollupquerycustomdecimalid",rollupquerycustomdecimalid);
    }

    /**
     * 设置 [汇总查询 - 实际值(金额)]
     */
    public void setRollupqueryactualmoneyid(String rollupqueryactualmoneyid){
        this.rollupqueryactualmoneyid = rollupqueryactualmoneyid ;
        this.modify("rollupqueryactualmoneyid",rollupqueryactualmoneyid);
    }

    /**
     * 设置 [汇总查询 - 自定义汇总字段(整数)]
     */
    public void setRollupquerycustomintegerid(String rollupquerycustomintegerid){
        this.rollupquerycustomintegerid = rollupquerycustomintegerid ;
        this.modify("rollupquerycustomintegerid",rollupquerycustomintegerid);
    }

    /**
     * 设置 [出现错误的目标]
     */
    public void setGoalwitherrorid(String goalwitherrorid){
        this.goalwitherrorid = goalwitherrorid ;
        this.modify("goalwitherrorid",goalwitherrorid);
    }

    /**
     * 设置 [目标度量]
     */
    public void setMetricid(String metricid){
        this.metricid = metricid ;
        this.modify("metricid",metricid);
    }

    /**
     * 设置 [汇总查询 - 过程值(十进制)]
     */
    public void setRollupqueryinprogressdecimalid(String rollupqueryinprogressdecimalid){
        this.rollupqueryinprogressdecimalid = rollupqueryinprogressdecimalid ;
        this.modify("rollupqueryinprogressdecimalid",rollupqueryinprogressdecimalid);
    }

    /**
     * 设置 [汇总查询 - 实际值(整数)]
     */
    public void setRollupqueryactualintegerid(String rollupqueryactualintegerid){
        this.rollupqueryactualintegerid = rollupqueryactualintegerid ;
        this.modify("rollupqueryactualintegerid",rollupqueryactualintegerid);
    }

    /**
     * 设置 [汇总查询 - 实际值(十进制)]
     */
    public void setRollupqueryactualdecimalid(String rollupqueryactualdecimalid){
        this.rollupqueryactualdecimalid = rollupqueryactualdecimalid ;
        this.modify("rollupqueryactualdecimalid",rollupqueryactualdecimalid);
    }


}



package cn.ibizlab.businesscentral.core.finance.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[季度会计日历]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "QUARTERLYFISCALCALENDAR",resultMap = "QuarterlyFiscalCalendarResultMap")
public class QuarterlyFiscalCalendar extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Period4_Base
     */
    @DEField(name = "period4_base")
    @TableField(value = "period4_base")
    @JSONField(name = "period4_base")
    @JsonProperty("period4_base")
    private BigDecimal period4Base;
    /**
     * Period4
     */
    @TableField(value = "period4")
    @JSONField(name = "period4")
    @JsonProperty("period4")
    private BigDecimal period4;
    /**
     * Period1
     */
    @TableField(value = "period1")
    @JSONField(name = "period1")
    @JsonProperty("period1")
    private BigDecimal period1;
    /**
     * Period1_Base
     */
    @DEField(name = "period1_base")
    @TableField(value = "period1_base")
    @JSONField(name = "period1_base")
    @JsonProperty("period1_base")
    private BigDecimal period1Base;
    /**
     * 销售员
     */
    @TableField(value = "salespersonname")
    @JSONField(name = "salespersonname")
    @JsonProperty("salespersonname")
    private String salespersonname;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * UTCConversionTimeZoneCode
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * Period10
     */
    @TableField(value = "period10")
    @JSONField(name = "period10")
    @JsonProperty("period10")
    private BigDecimal period10;
    /**
     * UserFiscalCalendarId
     */
    @DEField(isKeyField=true)
    @TableId(value= "userfiscalcalendarid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "userfiscalcalendarid")
    @JsonProperty("userfiscalcalendarid")
    private String userfiscalcalendarid;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * Period7
     */
    @TableField(value = "period7")
    @JSONField(name = "period7")
    @JsonProperty("period7")
    private BigDecimal period7;
    /**
     * FiscalPeriodType
     */
    @TableField(value = "fiscalperiodtype")
    @JSONField(name = "fiscalperiodtype")
    @JsonProperty("fiscalperiodtype")
    private Integer fiscalperiodtype;
    /**
     * TimeZoneRuleVersionNumber
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * Period10_Base
     */
    @DEField(name = "period10_base")
    @TableField(value = "period10_base")
    @JSONField(name = "period10_base")
    @JsonProperty("period10_base")
    private BigDecimal period10Base;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * SalesPersonId
     */
    @TableField(value = "salespersonid")
    @JSONField(name = "salespersonid")
    @JsonProperty("salespersonid")
    private String salespersonid;
    /**
     * EffectiveOn
     */
    @TableField(value = "effectiveon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "effectiveon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("effectiveon")
    private Timestamp effectiveon;
    /**
     * Period7_Base
     */
    @DEField(name = "period7_base")
    @TableField(value = "period7_base")
    @JSONField(name = "period7_base")
    @JsonProperty("period7_base")
    private BigDecimal period7Base;
    /**
     * ExchangeRate
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * TransactionCurrencyId
     */
    @TableField(value = "transactioncurrencyid")
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency;



    /**
     * 设置 [Period4_Base]
     */
    public void setPeriod4Base(BigDecimal period4Base){
        this.period4Base = period4Base ;
        this.modify("period4_base",period4Base);
    }

    /**
     * 设置 [Period4]
     */
    public void setPeriod4(BigDecimal period4){
        this.period4 = period4 ;
        this.modify("period4",period4);
    }

    /**
     * 设置 [Period1]
     */
    public void setPeriod1(BigDecimal period1){
        this.period1 = period1 ;
        this.modify("period1",period1);
    }

    /**
     * 设置 [Period1_Base]
     */
    public void setPeriod1Base(BigDecimal period1Base){
        this.period1Base = period1Base ;
        this.modify("period1_base",period1Base);
    }

    /**
     * 设置 [销售员]
     */
    public void setSalespersonname(String salespersonname){
        this.salespersonname = salespersonname ;
        this.modify("salespersonname",salespersonname);
    }

    /**
     * 设置 [UTCConversionTimeZoneCode]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [Period10]
     */
    public void setPeriod10(BigDecimal period10){
        this.period10 = period10 ;
        this.modify("period10",period10);
    }

    /**
     * 设置 [Period7]
     */
    public void setPeriod7(BigDecimal period7){
        this.period7 = period7 ;
        this.modify("period7",period7);
    }

    /**
     * 设置 [FiscalPeriodType]
     */
    public void setFiscalperiodtype(Integer fiscalperiodtype){
        this.fiscalperiodtype = fiscalperiodtype ;
        this.modify("fiscalperiodtype",fiscalperiodtype);
    }

    /**
     * 设置 [TimeZoneRuleVersionNumber]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [Period10_Base]
     */
    public void setPeriod10Base(BigDecimal period10Base){
        this.period10Base = period10Base ;
        this.modify("period10_base",period10Base);
    }

    /**
     * 设置 [SalesPersonId]
     */
    public void setSalespersonid(String salespersonid){
        this.salespersonid = salespersonid ;
        this.modify("salespersonid",salespersonid);
    }

    /**
     * 设置 [EffectiveOn]
     */
    public void setEffectiveon(Timestamp effectiveon){
        this.effectiveon = effectiveon ;
        this.modify("effectiveon",effectiveon);
    }

    /**
     * 格式化日期 [EffectiveOn]
     */
    public String formatEffectiveon(){
        if (this.effectiveon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(effectiveon);
    }
    /**
     * 设置 [Period7_Base]
     */
    public void setPeriod7Base(BigDecimal period7Base){
        this.period7Base = period7Base ;
        this.modify("period7_base",period7Base);
    }

    /**
     * 设置 [ExchangeRate]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [TransactionCurrencyId]
     */
    public void setTransactioncurrencyid(String transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }


}



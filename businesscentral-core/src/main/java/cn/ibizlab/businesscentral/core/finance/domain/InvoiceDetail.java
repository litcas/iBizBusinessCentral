package cn.ibizlab.businesscentral.core.finance.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[发票产品]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "INVOICEDETAIL",resultMap = "InvoiceDetailResultMap")
public class InvoiceDetail extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 名称
     */
    @TableField(value = "invoicedetailname")
    @JSONField(name = "invoicedetailname")
    @JsonProperty("invoicedetailname")
    private String invoicedetailname;
    /**
     * 延期交货数量
     */
    @TableField(value = "quantitybackordered")
    @JSONField(name = "quantitybackordered")
    @JsonProperty("quantitybackordered")
    private BigDecimal quantitybackordered;
    /**
     * 产品类型
     */
    @TableField(value = "producttypecode")
    @JSONField(name = "producttypecode")
    @JsonProperty("producttypecode")
    private String producttypecode;
    /**
     * 发票状态
     */
    @TableField(value = "invoicestatecode")
    @JSONField(name = "invoicestatecode")
    @JsonProperty("invoicestatecode")
    private String invoicestatecode;
    /**
     * Time Zone Rule Version Number
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * 送至省/市/自治区
     */
    @DEField(name = "shipto_stateorprovince")
    @TableField(value = "shipto_stateorprovince")
    @JSONField(name = "shipto_stateorprovince")
    @JsonProperty("shipto_stateorprovince")
    private String shiptoStateorprovince;
    /**
     * 金额 (Base)
     */
    @DEField(name = "baseamount_base")
    @TableField(value = "baseamount_base")
    @JSONField(name = "baseamount_base")
    @JsonProperty("baseamount_base")
    private BigDecimal baseamountBase;
    /**
     * 送货地址
     */
    @DEField(defaultValue = "0")
    @TableField(value = "willcall")
    @JSONField(name = "willcall")
    @JsonProperty("willcall")
    private Integer willcall;
    /**
     * Record Created On
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 父捆绑销售
     */
    @TableField(value = "parentbundleid")
    @JSONField(name = "parentbundleid")
    @JsonProperty("parentbundleid")
    private String parentbundleid;
    /**
     * Import Sequence Number
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * 属性配置
     */
    @TableField(value = "propertyconfigurationstatus")
    @JSONField(name = "propertyconfigurationstatus")
    @JsonProperty("propertyconfigurationstatus")
    private String propertyconfigurationstatus;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 单价 (Base)
     */
    @DEField(name = "priceperunit_base")
    @TableField(value = "priceperunit_base")
    @JSONField(name = "priceperunit_base")
    @JsonProperty("priceperunit_base")
    private BigDecimal priceperunitBase;
    /**
     * 送至国家/地区
     */
    @DEField(name = "shipto_country")
    @TableField(value = "shipto_country")
    @JSONField(name = "shipto_country")
    @JsonProperty("shipto_country")
    private String shiptoCountry;
    /**
     * 已复制
     */
    @DEField(defaultValue = "0")
    @TableField(value = "copied")
    @JSONField(name = "copied")
    @JsonProperty("copied")
    private Integer copied;
    /**
     * 负责人类型
     */
    @TableField(value = "ownertype")
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;
    /**
     * 明细项目编号
     */
    @TableField(value = "lineitemnumber")
    @JSONField(name = "lineitemnumber")
    @JsonProperty("lineitemnumber")
    private Integer lineitemnumber;
    /**
     * 单价
     */
    @TableField(value = "priceperunit")
    @JSONField(name = "priceperunit")
    @JsonProperty("priceperunit")
    private BigDecimal priceperunit;
    /**
     * 销售员
     */
    @TableField(value = "salesrepname")
    @JSONField(name = "salesrepname")
    @JsonProperty("salesrepname")
    private String salesrepname;
    /**
     * 批发折扣 (Base)
     */
    @DEField(name = "volumediscountamount_base")
    @TableField(value = "volumediscountamount_base")
    @JSONField(name = "volumediscountamount_base")
    @JsonProperty("volumediscountamount_base")
    private BigDecimal volumediscountamountBase;
    /**
     * 送货地的传真号码
     */
    @DEField(name = "shipto_fax")
    @TableField(value = "shipto_fax")
    @JSONField(name = "shipto_fax")
    @JsonProperty("shipto_fax")
    private String shiptoFax;
    /**
     * 送至街道 1
     */
    @DEField(name = "shipto_line1")
    @TableField(value = "shipto_line1")
    @JSONField(name = "shipto_line1")
    @JsonProperty("shipto_line1")
    private String shiptoLine1;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 货运条款
     */
    @DEField(name = "shipto_freighttermscode")
    @TableField(value = "shipto_freighttermscode")
    @JSONField(name = "shipto_freighttermscode")
    @JsonProperty("shipto_freighttermscode")
    private String shiptoFreighttermscode;
    /**
     * 负责人
     */
    @TableField(value = "ownerid")
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;
    /**
     * 税 (Base)
     */
    @DEField(name = "tax_base")
    @TableField(value = "tax_base")
    @JSONField(name = "tax_base")
    @JsonProperty("tax_base")
    private BigDecimal taxBase;
    /**
     * 送货地的电话号码
     */
    @DEField(name = "shipto_telephone")
    @TableField(value = "shipto_telephone")
    @JSONField(name = "shipto_telephone")
    @JsonProperty("shipto_telephone")
    private String shiptoTelephone;
    /**
     * 定价
     */
    @DEField(defaultValue = "0")
    @TableField(value = "priceoverridden")
    @JSONField(name = "priceoverridden")
    @JsonProperty("priceoverridden")
    private Integer priceoverridden;
    /**
     * 交付时间
     */
    @TableField(value = "actualdeliveryon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "actualdeliveryon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("actualdeliveryon")
    private Timestamp actualdeliveryon;
    /**
     * 送货地的名称
     */
    @DEField(name = "shipto_name")
    @TableField(value = "shipto_name")
    @JSONField(name = "shipto_name")
    @JsonProperty("shipto_name")
    private String shiptoName;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 金额
     */
    @TableField(value = "baseamount")
    @JSONField(name = "baseamount")
    @JsonProperty("baseamount")
    private BigDecimal baseamount;
    /**
     * 运货跟踪号码
     */
    @TableField(value = "shippingtrackingnumber")
    @JSONField(name = "shippingtrackingnumber")
    @JsonProperty("shippingtrackingnumber")
    private String shippingtrackingnumber;
    /**
     * 发票产品
     */
    @DEField(isKeyField=true)
    @TableId(value= "invoicedetailid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "invoicedetailid")
    @JsonProperty("invoicedetailid")
    private String invoicedetailid;
    /**
     * 负责人
     */
    @TableField(value = "ownername")
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;
    /**
     * 汇率
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * 送至街道 3
     */
    @DEField(name = "shipto_line3")
    @TableField(value = "shipto_line3")
    @JSONField(name = "shipto_line3")
    @JsonProperty("shipto_line3")
    private String shiptoLine3;
    /**
     * 捆绑销售项关联
     */
    @TableField(value = "productassociationid")
    @JSONField(name = "productassociationid")
    @JsonProperty("productassociationid")
    private String productassociationid;
    /**
     * 送货地的邮政编码
     */
    @DEField(name = "shipto_postalcode")
    @TableField(value = "shipto_postalcode")
    @JSONField(name = "shipto_postalcode")
    @JsonProperty("shipto_postalcode")
    private String shiptoPostalcode;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 选择产品
     */
    @DEField(defaultValue = "0")
    @TableField(value = "productoverridden")
    @JSONField(name = "productoverridden")
    @JsonProperty("productoverridden")
    private Integer productoverridden;
    /**
     * 销售员
     */
    @TableField(value = "salesrepid")
    @JSONField(name = "salesrepid")
    @JsonProperty("salesrepid")
    private String salesrepid;
    /**
     * UTC Conversion Time Zone Code
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * 应收净额
     */
    @TableField(value = "extendedamount")
    @JSONField(name = "extendedamount")
    @JsonProperty("extendedamount")
    private BigDecimal extendedamount;
    /**
     * 目录外产品
     */
    @TableField(value = "productdescription")
    @JSONField(name = "productdescription")
    @JsonProperty("productdescription")
    private String productdescription;
    /**
     * 送至街道 2
     */
    @DEField(name = "shipto_line2")
    @TableField(value = "shipto_line2")
    @JSONField(name = "shipto_line2")
    @JsonProperty("shipto_line2")
    private String shiptoLine2;
    /**
     * 零售折扣
     */
    @TableField(value = "manualdiscountamount")
    @JSONField(name = "manualdiscountamount")
    @JsonProperty("manualdiscountamount")
    private BigDecimal manualdiscountamount;
    /**
     * 序号
     */
    @TableField(value = "sequencenumber")
    @JSONField(name = "sequencenumber")
    @JsonProperty("sequencenumber")
    private Integer sequencenumber;
    /**
     * 已发货数量
     */
    @TableField(value = "quantityshipped")
    @JSONField(name = "quantityshipped")
    @JsonProperty("quantityshipped")
    private BigDecimal quantityshipped;
    /**
     * 已取消数量
     */
    @TableField(value = "quantitycancelled")
    @JSONField(name = "quantitycancelled")
    @JsonProperty("quantitycancelled")
    private BigDecimal quantitycancelled;
    /**
     * 定价错误
     */
    @TableField(value = "pricingerrorcode")
    @JSONField(name = "pricingerrorcode")
    @JsonProperty("pricingerrorcode")
    private String pricingerrorcode;
    /**
     * Version Number
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * SkipPriceCalculation
     */
    @TableField(value = "skippricecalculation")
    @JSONField(name = "skippricecalculation")
    @JsonProperty("skippricecalculation")
    private String skippricecalculation;
    /**
     * 数量
     */
    @TableField(value = "quantity")
    @JSONField(name = "quantity")
    @JsonProperty("quantity")
    private BigDecimal quantity;
    /**
     * 税
     */
    @TableField(value = "tax")
    @JSONField(name = "tax")
    @JsonProperty("tax")
    private BigDecimal tax;
    /**
     * 应收净额 (Base)
     */
    @DEField(name = "extendedamount_base")
    @TableField(value = "extendedamount_base")
    @JSONField(name = "extendedamount_base")
    @JsonProperty("extendedamount_base")
    private BigDecimal extendedamountBase;
    /**
     * 批发折扣
     */
    @TableField(value = "volumediscountamount")
    @JSONField(name = "volumediscountamount")
    @JsonProperty("volumediscountamount")
    private BigDecimal volumediscountamount;
    /**
     * 送至市/县
     */
    @DEField(name = "shipto_city")
    @TableField(value = "shipto_city")
    @JSONField(name = "shipto_city")
    @JsonProperty("shipto_city")
    private String shiptoCity;
    /**
     * 发票价格已锁定
     */
    @DEField(defaultValue = "0")
    @TableField(value = "invoiceispricelocked")
    @JSONField(name = "invoiceispricelocked")
    @JsonProperty("invoiceispricelocked")
    private Integer invoiceispricelocked;
    /**
     * 产品名称
     */
    @TableField(value = "productname")
    @JSONField(name = "productname")
    @JsonProperty("productname")
    private String productname;
    /**
     * 计价单位
     */
    @TableField(value = "uomname")
    @JSONField(name = "uomname")
    @JsonProperty("uomname")
    private String uomname;
    /**
     * 货币
     */
    @TableField(value = "transactioncurrencyid")
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;
    /**
     * Order Product Id
     */
    @TableField(value = "salesorderdetailid")
    @JSONField(name = "salesorderdetailid")
    @JsonProperty("salesorderdetailid")
    private String salesorderdetailid;
    /**
     * Parent bundle product
     */
    @TableField(value = "parentbundleidref")
    @JSONField(name = "parentbundleidref")
    @JsonProperty("parentbundleidref")
    private String parentbundleidref;
    /**
     * 现有产品
     */
    @TableField(value = "productid")
    @JSONField(name = "productid")
    @JsonProperty("productid")
    private String productid;
    /**
     * 发票编码
     */
    @TableField(value = "invoiceid")
    @JSONField(name = "invoiceid")
    @JsonProperty("invoiceid")
    private String invoiceid;
    /**
     * 计价单位
     */
    @TableField(value = "uomid")
    @JSONField(name = "uomid")
    @JsonProperty("uomid")
    private String uomid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.finance.domain.InvoiceDetail parentbundleidr;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.finance.domain.Invoice invoice;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.product.domain.Product product;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.sales.domain.SalesOrderDetail salesorderdetail;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Uom uom;



    /**
     * 设置 [名称]
     */
    public void setInvoicedetailname(String invoicedetailname){
        this.invoicedetailname = invoicedetailname ;
        this.modify("invoicedetailname",invoicedetailname);
    }

    /**
     * 设置 [延期交货数量]
     */
    public void setQuantitybackordered(BigDecimal quantitybackordered){
        this.quantitybackordered = quantitybackordered ;
        this.modify("quantitybackordered",quantitybackordered);
    }

    /**
     * 设置 [产品类型]
     */
    public void setProducttypecode(String producttypecode){
        this.producttypecode = producttypecode ;
        this.modify("producttypecode",producttypecode);
    }

    /**
     * 设置 [发票状态]
     */
    public void setInvoicestatecode(String invoicestatecode){
        this.invoicestatecode = invoicestatecode ;
        this.modify("invoicestatecode",invoicestatecode);
    }

    /**
     * 设置 [Time Zone Rule Version Number]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [送至省/市/自治区]
     */
    public void setShiptoStateorprovince(String shiptoStateorprovince){
        this.shiptoStateorprovince = shiptoStateorprovince ;
        this.modify("shipto_stateorprovince",shiptoStateorprovince);
    }

    /**
     * 设置 [金额 (Base)]
     */
    public void setBaseamountBase(BigDecimal baseamountBase){
        this.baseamountBase = baseamountBase ;
        this.modify("baseamount_base",baseamountBase);
    }

    /**
     * 设置 [送货地址]
     */
    public void setWillcall(Integer willcall){
        this.willcall = willcall ;
        this.modify("willcall",willcall);
    }

    /**
     * 设置 [Record Created On]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [Record Created On]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [父捆绑销售]
     */
    public void setParentbundleid(String parentbundleid){
        this.parentbundleid = parentbundleid ;
        this.modify("parentbundleid",parentbundleid);
    }

    /**
     * 设置 [Import Sequence Number]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [属性配置]
     */
    public void setPropertyconfigurationstatus(String propertyconfigurationstatus){
        this.propertyconfigurationstatus = propertyconfigurationstatus ;
        this.modify("propertyconfigurationstatus",propertyconfigurationstatus);
    }

    /**
     * 设置 [单价 (Base)]
     */
    public void setPriceperunitBase(BigDecimal priceperunitBase){
        this.priceperunitBase = priceperunitBase ;
        this.modify("priceperunit_base",priceperunitBase);
    }

    /**
     * 设置 [送至国家/地区]
     */
    public void setShiptoCountry(String shiptoCountry){
        this.shiptoCountry = shiptoCountry ;
        this.modify("shipto_country",shiptoCountry);
    }

    /**
     * 设置 [已复制]
     */
    public void setCopied(Integer copied){
        this.copied = copied ;
        this.modify("copied",copied);
    }

    /**
     * 设置 [负责人类型]
     */
    public void setOwnertype(String ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [明细项目编号]
     */
    public void setLineitemnumber(Integer lineitemnumber){
        this.lineitemnumber = lineitemnumber ;
        this.modify("lineitemnumber",lineitemnumber);
    }

    /**
     * 设置 [单价]
     */
    public void setPriceperunit(BigDecimal priceperunit){
        this.priceperunit = priceperunit ;
        this.modify("priceperunit",priceperunit);
    }

    /**
     * 设置 [销售员]
     */
    public void setSalesrepname(String salesrepname){
        this.salesrepname = salesrepname ;
        this.modify("salesrepname",salesrepname);
    }

    /**
     * 设置 [批发折扣 (Base)]
     */
    public void setVolumediscountamountBase(BigDecimal volumediscountamountBase){
        this.volumediscountamountBase = volumediscountamountBase ;
        this.modify("volumediscountamount_base",volumediscountamountBase);
    }

    /**
     * 设置 [送货地的传真号码]
     */
    public void setShiptoFax(String shiptoFax){
        this.shiptoFax = shiptoFax ;
        this.modify("shipto_fax",shiptoFax);
    }

    /**
     * 设置 [送至街道 1]
     */
    public void setShiptoLine1(String shiptoLine1){
        this.shiptoLine1 = shiptoLine1 ;
        this.modify("shipto_line1",shiptoLine1);
    }

    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [货运条款]
     */
    public void setShiptoFreighttermscode(String shiptoFreighttermscode){
        this.shiptoFreighttermscode = shiptoFreighttermscode ;
        this.modify("shipto_freighttermscode",shiptoFreighttermscode);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnerid(String ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [税 (Base)]
     */
    public void setTaxBase(BigDecimal taxBase){
        this.taxBase = taxBase ;
        this.modify("tax_base",taxBase);
    }

    /**
     * 设置 [送货地的电话号码]
     */
    public void setShiptoTelephone(String shiptoTelephone){
        this.shiptoTelephone = shiptoTelephone ;
        this.modify("shipto_telephone",shiptoTelephone);
    }

    /**
     * 设置 [定价]
     */
    public void setPriceoverridden(Integer priceoverridden){
        this.priceoverridden = priceoverridden ;
        this.modify("priceoverridden",priceoverridden);
    }

    /**
     * 设置 [交付时间]
     */
    public void setActualdeliveryon(Timestamp actualdeliveryon){
        this.actualdeliveryon = actualdeliveryon ;
        this.modify("actualdeliveryon",actualdeliveryon);
    }

    /**
     * 格式化日期 [交付时间]
     */
    public String formatActualdeliveryon(){
        if (this.actualdeliveryon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(actualdeliveryon);
    }
    /**
     * 设置 [送货地的名称]
     */
    public void setShiptoName(String shiptoName){
        this.shiptoName = shiptoName ;
        this.modify("shipto_name",shiptoName);
    }

    /**
     * 设置 [金额]
     */
    public void setBaseamount(BigDecimal baseamount){
        this.baseamount = baseamount ;
        this.modify("baseamount",baseamount);
    }

    /**
     * 设置 [运货跟踪号码]
     */
    public void setShippingtrackingnumber(String shippingtrackingnumber){
        this.shippingtrackingnumber = shippingtrackingnumber ;
        this.modify("shippingtrackingnumber",shippingtrackingnumber);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnername(String ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [汇率]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [送至街道 3]
     */
    public void setShiptoLine3(String shiptoLine3){
        this.shiptoLine3 = shiptoLine3 ;
        this.modify("shipto_line3",shiptoLine3);
    }

    /**
     * 设置 [捆绑销售项关联]
     */
    public void setProductassociationid(String productassociationid){
        this.productassociationid = productassociationid ;
        this.modify("productassociationid",productassociationid);
    }

    /**
     * 设置 [送货地的邮政编码]
     */
    public void setShiptoPostalcode(String shiptoPostalcode){
        this.shiptoPostalcode = shiptoPostalcode ;
        this.modify("shipto_postalcode",shiptoPostalcode);
    }

    /**
     * 设置 [选择产品]
     */
    public void setProductoverridden(Integer productoverridden){
        this.productoverridden = productoverridden ;
        this.modify("productoverridden",productoverridden);
    }

    /**
     * 设置 [销售员]
     */
    public void setSalesrepid(String salesrepid){
        this.salesrepid = salesrepid ;
        this.modify("salesrepid",salesrepid);
    }

    /**
     * 设置 [UTC Conversion Time Zone Code]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [应收净额]
     */
    public void setExtendedamount(BigDecimal extendedamount){
        this.extendedamount = extendedamount ;
        this.modify("extendedamount",extendedamount);
    }

    /**
     * 设置 [目录外产品]
     */
    public void setProductdescription(String productdescription){
        this.productdescription = productdescription ;
        this.modify("productdescription",productdescription);
    }

    /**
     * 设置 [送至街道 2]
     */
    public void setShiptoLine2(String shiptoLine2){
        this.shiptoLine2 = shiptoLine2 ;
        this.modify("shipto_line2",shiptoLine2);
    }

    /**
     * 设置 [零售折扣]
     */
    public void setManualdiscountamount(BigDecimal manualdiscountamount){
        this.manualdiscountamount = manualdiscountamount ;
        this.modify("manualdiscountamount",manualdiscountamount);
    }

    /**
     * 设置 [序号]
     */
    public void setSequencenumber(Integer sequencenumber){
        this.sequencenumber = sequencenumber ;
        this.modify("sequencenumber",sequencenumber);
    }

    /**
     * 设置 [已发货数量]
     */
    public void setQuantityshipped(BigDecimal quantityshipped){
        this.quantityshipped = quantityshipped ;
        this.modify("quantityshipped",quantityshipped);
    }

    /**
     * 设置 [已取消数量]
     */
    public void setQuantitycancelled(BigDecimal quantitycancelled){
        this.quantitycancelled = quantitycancelled ;
        this.modify("quantitycancelled",quantitycancelled);
    }

    /**
     * 设置 [定价错误]
     */
    public void setPricingerrorcode(String pricingerrorcode){
        this.pricingerrorcode = pricingerrorcode ;
        this.modify("pricingerrorcode",pricingerrorcode);
    }

    /**
     * 设置 [Version Number]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [SkipPriceCalculation]
     */
    public void setSkippricecalculation(String skippricecalculation){
        this.skippricecalculation = skippricecalculation ;
        this.modify("skippricecalculation",skippricecalculation);
    }

    /**
     * 设置 [数量]
     */
    public void setQuantity(BigDecimal quantity){
        this.quantity = quantity ;
        this.modify("quantity",quantity);
    }

    /**
     * 设置 [税]
     */
    public void setTax(BigDecimal tax){
        this.tax = tax ;
        this.modify("tax",tax);
    }

    /**
     * 设置 [应收净额 (Base)]
     */
    public void setExtendedamountBase(BigDecimal extendedamountBase){
        this.extendedamountBase = extendedamountBase ;
        this.modify("extendedamount_base",extendedamountBase);
    }

    /**
     * 设置 [批发折扣]
     */
    public void setVolumediscountamount(BigDecimal volumediscountamount){
        this.volumediscountamount = volumediscountamount ;
        this.modify("volumediscountamount",volumediscountamount);
    }

    /**
     * 设置 [送至市/县]
     */
    public void setShiptoCity(String shiptoCity){
        this.shiptoCity = shiptoCity ;
        this.modify("shipto_city",shiptoCity);
    }

    /**
     * 设置 [发票价格已锁定]
     */
    public void setInvoiceispricelocked(Integer invoiceispricelocked){
        this.invoiceispricelocked = invoiceispricelocked ;
        this.modify("invoiceispricelocked",invoiceispricelocked);
    }

    /**
     * 设置 [产品名称]
     */
    public void setProductname(String productname){
        this.productname = productname ;
        this.modify("productname",productname);
    }

    /**
     * 设置 [计价单位]
     */
    public void setUomname(String uomname){
        this.uomname = uomname ;
        this.modify("uomname",uomname);
    }

    /**
     * 设置 [货币]
     */
    public void setTransactioncurrencyid(String transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }

    /**
     * 设置 [Order Product Id]
     */
    public void setSalesorderdetailid(String salesorderdetailid){
        this.salesorderdetailid = salesorderdetailid ;
        this.modify("salesorderdetailid",salesorderdetailid);
    }

    /**
     * 设置 [Parent bundle product]
     */
    public void setParentbundleidref(String parentbundleidref){
        this.parentbundleidref = parentbundleidref ;
        this.modify("parentbundleidref",parentbundleidref);
    }

    /**
     * 设置 [现有产品]
     */
    public void setProductid(String productid){
        this.productid = productid ;
        this.modify("productid",productid);
    }

    /**
     * 设置 [发票编码]
     */
    public void setInvoiceid(String invoiceid){
        this.invoiceid = invoiceid ;
        this.modify("invoiceid",invoiceid);
    }

    /**
     * 设置 [计价单位]
     */
    public void setUomid(String uomid){
        this.uomid = uomid ;
        this.modify("uomid",uomid);
    }


}



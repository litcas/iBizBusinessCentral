package cn.ibizlab.businesscentral.core.marketing.service.logic.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;

import cn.ibizlab.businesscentral.core.marketing.service.logic.ICampaignActiveLogic;
import cn.ibizlab.businesscentral.core.marketing.domain.Campaign;

/**
 * 关系型数据实体[Active] 对象
 */
@Slf4j
@Service
public class CampaignActiveLogicImpl implements ICampaignActiveLogic{

    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private cn.ibizlab.businesscentral.core.marketing.service.ICampaignService campaignservice;

    public cn.ibizlab.businesscentral.core.marketing.service.ICampaignService getCampaignService() {
        return this.campaignservice;
    }


    @Autowired
    private cn.ibizlab.businesscentral.core.marketing.service.ICampaignService iBzSysDefaultService;

    public cn.ibizlab.businesscentral.core.marketing.service.ICampaignService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    public void execute(Campaign et){

          KieSession kieSession = null;
        try{
           kieSession=kieContainer.newKieSession();
           kieSession.insert(et); 
           kieSession.setGlobal("campaignactivedefault",et);
           kieSession.setGlobal("campaignservice",campaignservice);
           kieSession.setGlobal("iBzSysCampaignDefaultService",iBzSysDefaultService);
           kieSession.setGlobal("curuser", cn.ibizlab.businesscentral.util.security.AuthenticationUser.getAuthenticationUser());
           kieSession.startProcess("cn.ibizlab.businesscentral.core.marketing.service.logic.campaignactive");

        }catch(Exception e){
            throw new RuntimeException("执行[激活]处理逻辑发生异常"+e);
        }finally {
            if(kieSession!=null)
            kieSession.destroy();
        }
    }

}

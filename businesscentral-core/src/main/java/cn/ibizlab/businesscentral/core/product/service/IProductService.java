package cn.ibizlab.businesscentral.core.product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.product.domain.Product;
import cn.ibizlab.businesscentral.core.product.filter.ProductSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Product] 服务对象接口
 */
public interface IProductService extends IService<Product>{

    boolean create(Product et) ;
    void createBatch(List<Product> list) ;
    boolean update(Product et) ;
    void updateBatch(List<Product> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Product get(String key) ;
    Product getDraft(Product et) ;
    boolean checkKey(Product et) ;
    Product publish(Product et) ;
    Product revise(Product et) ;
    boolean save(Product et) ;
    void saveBatch(List<Product> list) ;
    Product stop(Product et) ;
    Page<Product> searchDefault(ProductSearchContext context) ;
    Page<Product> searchEffective(ProductSearchContext context) ;
    Page<Product> searchRevise(ProductSearchContext context) ;
    Page<Product> searchStop(ProductSearchContext context) ;
    List<Product> selectByPricelevelid(String pricelevelid) ;
    void removeByPricelevelid(String pricelevelid) ;
    List<Product> selectByParentproductid(String productid) ;
    void removeByParentproductid(String productid) ;
    List<Product> selectBySubjectid(String subjectid) ;
    void removeBySubjectid(String subjectid) ;
    List<Product> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    List<Product> selectByDefaultuomscheduleid(String uomscheduleid) ;
    void removeByDefaultuomscheduleid(String uomscheduleid) ;
    List<Product> selectByDefaultuomid(String uomid) ;
    void removeByDefaultuomid(String uomid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Product> getProductByIds(List<String> ids) ;
    List<Product> getProductByEntities(List<Product> entities) ;
}



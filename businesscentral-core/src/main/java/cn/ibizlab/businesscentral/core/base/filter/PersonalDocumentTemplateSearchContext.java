package cn.ibizlab.businesscentral.core.base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.base.domain.PersonalDocumentTemplate;
/**
 * 关系型数据实体[PersonalDocumentTemplate] 查询条件对象
 */
@Slf4j
@Data
public class PersonalDocumentTemplateSearchContext extends QueryWrapperContext<PersonalDocumentTemplate> {

	private String n_documenttype_eq;//[类型]
	public void setN_documenttype_eq(String n_documenttype_eq) {
        this.n_documenttype_eq = n_documenttype_eq;
        if(!ObjectUtils.isEmpty(this.n_documenttype_eq)){
            this.getSearchCond().eq("documenttype", n_documenttype_eq);
        }
    }
	private String n_personaldocumenttemplatename_like;//[个人文档模板名称]
	public void setN_personaldocumenttemplatename_like(String n_personaldocumenttemplatename_like) {
        this.n_personaldocumenttemplatename_like = n_personaldocumenttemplatename_like;
        if(!ObjectUtils.isEmpty(this.n_personaldocumenttemplatename_like)){
            this.getSearchCond().like("personaldocumenttemplatename", n_personaldocumenttemplatename_like);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("personaldocumenttemplatename", query)   
            );
		 }
	}
}




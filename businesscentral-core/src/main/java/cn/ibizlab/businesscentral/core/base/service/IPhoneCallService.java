package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.PhoneCall;
import cn.ibizlab.businesscentral.core.base.filter.PhoneCallSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[PhoneCall] 服务对象接口
 */
public interface IPhoneCallService extends IService<PhoneCall>{

    boolean create(PhoneCall et) ;
    void createBatch(List<PhoneCall> list) ;
    boolean update(PhoneCall et) ;
    void updateBatch(List<PhoneCall> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    PhoneCall get(String key) ;
    PhoneCall getDraft(PhoneCall et) ;
    boolean checkKey(PhoneCall et) ;
    boolean save(PhoneCall et) ;
    void saveBatch(List<PhoneCall> list) ;
    Page<PhoneCall> searchDefault(PhoneCallSearchContext context) ;
    List<PhoneCall> selectByServiceid(String serviceid) ;
    void removeByServiceid(String serviceid) ;
    List<PhoneCall> selectBySlaid(String slaid) ;
    void removeBySlaid(String slaid) ;
    List<PhoneCall> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<PhoneCall> getPhonecallByIds(List<String> ids) ;
    List<PhoneCall> getPhonecallByEntities(List<PhoneCall> entities) ;
}



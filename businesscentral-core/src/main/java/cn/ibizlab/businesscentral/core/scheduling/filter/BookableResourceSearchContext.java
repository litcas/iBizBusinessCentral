package cn.ibizlab.businesscentral.core.scheduling.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.scheduling.domain.BookableResource;
/**
 * 关系型数据实体[BookableResource] 查询条件对象
 */
@Slf4j
@Data
public class BookableResourceSearchContext extends QueryWrapperContext<BookableResource> {

	private String n_bookableresname_like;//[可预订的资源名称]
	public void setN_bookableresname_like(String n_bookableresname_like) {
        this.n_bookableresname_like = n_bookableresname_like;
        if(!ObjectUtils.isEmpty(this.n_bookableresname_like)){
            this.getSearchCond().like("bookableresname", n_bookableresname_like);
        }
    }
	private Integer n_statuscode_eq;//[状态描述]
	public void setN_statuscode_eq(Integer n_statuscode_eq) {
        this.n_statuscode_eq = n_statuscode_eq;
        if(!ObjectUtils.isEmpty(this.n_statuscode_eq)){
            this.getSearchCond().eq("statuscode", n_statuscode_eq);
        }
    }
	private String n_resourcetype_eq;//[资源类型]
	public void setN_resourcetype_eq(String n_resourcetype_eq) {
        this.n_resourcetype_eq = n_resourcetype_eq;
        if(!ObjectUtils.isEmpty(this.n_resourcetype_eq)){
            this.getSearchCond().eq("resourcetype", n_resourcetype_eq);
        }
    }
	private Integer n_statecode_eq;//[状态]
	public void setN_statecode_eq(Integer n_statecode_eq) {
        this.n_statecode_eq = n_statecode_eq;
        if(!ObjectUtils.isEmpty(this.n_statecode_eq)){
            this.getSearchCond().eq("statecode", n_statecode_eq);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }
	private String n_contactid_eq;//[联系人]
	public void setN_contactid_eq(String n_contactid_eq) {
        this.n_contactid_eq = n_contactid_eq;
        if(!ObjectUtils.isEmpty(this.n_contactid_eq)){
            this.getSearchCond().eq("contactid", n_contactid_eq);
        }
    }
	private String n_calendarid_eq;//[日历]
	public void setN_calendarid_eq(String n_calendarid_eq) {
        this.n_calendarid_eq = n_calendarid_eq;
        if(!ObjectUtils.isEmpty(this.n_calendarid_eq)){
            this.getSearchCond().eq("calendarid", n_calendarid_eq);
        }
    }
	private String n_accountid_eq;//[帐户]
	public void setN_accountid_eq(String n_accountid_eq) {
        this.n_accountid_eq = n_accountid_eq;
        if(!ObjectUtils.isEmpty(this.n_accountid_eq)){
            this.getSearchCond().eq("accountid", n_accountid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("bookableresname", query)   
            );
		 }
	}
}




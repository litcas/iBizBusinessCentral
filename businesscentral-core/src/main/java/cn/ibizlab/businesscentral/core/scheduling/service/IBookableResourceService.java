package cn.ibizlab.businesscentral.core.scheduling.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.scheduling.domain.BookableResource;
import cn.ibizlab.businesscentral.core.scheduling.filter.BookableResourceSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[BookableResource] 服务对象接口
 */
public interface IBookableResourceService extends IService<BookableResource>{

    boolean create(BookableResource et) ;
    void createBatch(List<BookableResource> list) ;
    boolean update(BookableResource et) ;
    void updateBatch(List<BookableResource> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    BookableResource get(String key) ;
    BookableResource getDraft(BookableResource et) ;
    boolean checkKey(BookableResource et) ;
    boolean save(BookableResource et) ;
    void saveBatch(List<BookableResource> list) ;
    Page<BookableResource> searchDefault(BookableResourceSearchContext context) ;
    List<BookableResource> selectByAccountid(String accountid) ;
    void removeByAccountid(String accountid) ;
    List<BookableResource> selectByCalendarid(String calendarid) ;
    void removeByCalendarid(String calendarid) ;
    List<BookableResource> selectByContactid(String contactid) ;
    void removeByContactid(String contactid) ;
    List<BookableResource> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<BookableResource> getBookableresourceByIds(List<String> ids) ;
    List<BookableResource> getBookableresourceByEntities(List<BookableResource> entities) ;
}



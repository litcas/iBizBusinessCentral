package cn.ibizlab.businesscentral.core.runtime.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.runtime.domain.ConnectionRole;
import cn.ibizlab.businesscentral.core.runtime.filter.ConnectionRoleSearchContext;
import cn.ibizlab.businesscentral.core.runtime.service.IConnectionRoleService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.runtime.mapper.ConnectionRoleMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[连接角色] 服务对象接口实现
 */
@Slf4j
@Service("ConnectionRoleServiceImpl")
public class ConnectionRoleServiceImpl extends ServiceImpl<ConnectionRoleMapper, ConnectionRole> implements IConnectionRoleService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.runtime.service.IConnectionService connectionService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(ConnectionRole et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getConnectionroleid()),et);
        return true;
    }

    @Override
    public void createBatch(List<ConnectionRole> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(ConnectionRole et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("connectionroleid",et.getConnectionroleid())))
            return false;
        CachedBeanCopier.copy(get(et.getConnectionroleid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<ConnectionRole> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public ConnectionRole get(String key) {
        ConnectionRole et = getById(key);
        if(et==null){
            et=new ConnectionRole();
            et.setConnectionroleid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public ConnectionRole getDraft(ConnectionRole et) {
        return et;
    }

    @Override
    public boolean checkKey(ConnectionRole et) {
        return (!ObjectUtils.isEmpty(et.getConnectionroleid()))&&(!Objects.isNull(this.getById(et.getConnectionroleid())));
    }
    @Override
    @Transactional
    public boolean save(ConnectionRole et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(ConnectionRole et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<ConnectionRole> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<ConnectionRole> list) {
        saveOrUpdateBatch(list,batchSize);
    }



    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<ConnectionRole> searchDefault(ConnectionRoleSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<ConnectionRole> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<ConnectionRole>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<ConnectionRole> getConnectionroleByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<ConnectionRole> getConnectionroleByEntities(List<ConnectionRole> entities) {
        List ids =new ArrayList();
        for(ConnectionRole entity : entities){
            Serializable id=entity.getConnectionroleid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




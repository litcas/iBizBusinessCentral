package cn.ibizlab.businesscentral.core.sales.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.sales.domain.Competitor;
import cn.ibizlab.businesscentral.core.sales.filter.CompetitorSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Competitor] 服务对象接口
 */
public interface ICompetitorService extends IService<Competitor>{

    boolean create(Competitor et) ;
    void createBatch(List<Competitor> list) ;
    boolean update(Competitor et) ;
    void updateBatch(List<Competitor> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Competitor get(String key) ;
    Competitor getDraft(Competitor et) ;
    boolean checkKey(Competitor et) ;
    boolean save(Competitor et) ;
    void saveBatch(List<Competitor> list) ;
    Page<Competitor> searchDefault(CompetitorSearchContext context) ;
    List<Competitor> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Competitor> getCompetitorByIds(List<String> ids) ;
    List<Competitor> getCompetitorByEntities(List<Competitor> entities) ;
}



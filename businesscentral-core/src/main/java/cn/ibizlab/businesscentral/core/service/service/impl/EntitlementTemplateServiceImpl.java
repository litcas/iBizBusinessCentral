package cn.ibizlab.businesscentral.core.service.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.service.domain.EntitlementTemplate;
import cn.ibizlab.businesscentral.core.service.filter.EntitlementTemplateSearchContext;
import cn.ibizlab.businesscentral.core.service.service.IEntitlementTemplateService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.service.mapper.EntitlementTemplateMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[权利模板] 服务对象接口实现
 */
@Slf4j
@Service("EntitlementTemplateServiceImpl")
public class EntitlementTemplateServiceImpl extends ServiceImpl<EntitlementTemplateMapper, EntitlementTemplate> implements IEntitlementTemplateService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IEntitlementTemplateChannelService entitlementtemplatechannelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IEntitlementService entitlementService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ISlaService slaService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EntitlementTemplate et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getEntitlementtemplateid()),et);
        return true;
    }

    @Override
    public void createBatch(List<EntitlementTemplate> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(EntitlementTemplate et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("entitlementtemplateid",et.getEntitlementtemplateid())))
            return false;
        CachedBeanCopier.copy(get(et.getEntitlementtemplateid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<EntitlementTemplate> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EntitlementTemplate get(String key) {
        EntitlementTemplate et = getById(key);
        if(et==null){
            et=new EntitlementTemplate();
            et.setEntitlementtemplateid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public EntitlementTemplate getDraft(EntitlementTemplate et) {
        return et;
    }

    @Override
    public boolean checkKey(EntitlementTemplate et) {
        return (!ObjectUtils.isEmpty(et.getEntitlementtemplateid()))&&(!Objects.isNull(this.getById(et.getEntitlementtemplateid())));
    }
    @Override
    @Transactional
    public boolean save(EntitlementTemplate et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EntitlementTemplate et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<EntitlementTemplate> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<EntitlementTemplate> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<EntitlementTemplate> selectBySlaid(String slaid) {
        return baseMapper.selectBySlaid(slaid);
    }

    @Override
    public void removeBySlaid(String slaid) {
        this.remove(new QueryWrapper<EntitlementTemplate>().eq("slaid",slaid));
    }

	@Override
    public List<EntitlementTemplate> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<EntitlementTemplate>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EntitlementTemplate> searchDefault(EntitlementTemplateSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EntitlementTemplate> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EntitlementTemplate>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EntitlementTemplate> getEntitlementtemplateByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EntitlementTemplate> getEntitlementtemplateByEntities(List<EntitlementTemplate> entities) {
        List ids =new ArrayList();
        for(EntitlementTemplate entity : entities){
            Serializable id=entity.getEntitlementtemplateid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




package cn.ibizlab.businesscentral.core.stock.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[库存]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "INVENTORY",resultMap = "InventoryResultMap")
public class Inventory extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 库存名称
     */
    @TableField(value = "inventoryname")
    @JSONField(name = "inventoryname")
    @JsonProperty("inventoryname")
    private String inventoryname;
    /**
     * 库存标识
     */
    @DEField(isKeyField=true)
    @TableId(value= "inventoryid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "inventoryid")
    @JsonProperty("inventoryid")
    private String inventoryid;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 计价单位
     */
    @TableField(value = "uomname")
    @JSONField(name = "uomname")
    @JsonProperty("uomname")
    private String uomname;
    /**
     * 产品
     */
    @TableField(value = "productname")
    @JSONField(name = "productname")
    @JsonProperty("productname")
    private String productname;
    /**
     * 产品
     */
    @TableField(value = "productid")
    @JSONField(name = "productid")
    @JsonProperty("productid")
    private String productid;
    /**
     * 计价单位
     */
    @TableField(value = "uomid")
    @JSONField(name = "uomid")
    @JsonProperty("uomid")
    private String uomid;

    /**
     * 产品
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.product.domain.Product product;

    /**
     * 计价单位
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Uom uom;



    /**
     * 设置 [库存名称]
     */
    public void setInventoryname(String inventoryname){
        this.inventoryname = inventoryname ;
        this.modify("inventoryname",inventoryname);
    }

    /**
     * 设置 [计价单位]
     */
    public void setUomname(String uomname){
        this.uomname = uomname ;
        this.modify("uomname",uomname);
    }

    /**
     * 设置 [产品]
     */
    public void setProductname(String productname){
        this.productname = productname ;
        this.modify("productname",productname);
    }

    /**
     * 设置 [产品]
     */
    public void setProductid(String productid){
        this.productid = productid ;
        this.modify("productid",productid);
    }

    /**
     * 设置 [计价单位]
     */
    public void setUomid(String uomid){
        this.uomid = uomid ;
        this.modify("uomid",uomid);
    }


}



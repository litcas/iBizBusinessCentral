package cn.ibizlab.businesscentral.core.base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[客户]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "ACCOUNT",resultMap = "AccountResultMap")
public class Account extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 地址 1: 主要联系人姓名
     */
    @DEField(name = "address1_primarycontactname")
    @TableField(value = "address1_primarycontactname")
    @JSONField(name = "address1_primarycontactname")
    @JsonProperty("address1_primarycontactname")
    private String address1Primarycontactname;
    /**
     * 开启的收入 (Last Updated On)
     */
    @DEField(name = "openrevenue_date")
    @TableField(value = "openrevenue_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "openrevenue_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("openrevenue_date")
    private Timestamp openrevenueDate;
    /**
     * 地址 2: UTC 时差
     */
    @DEField(name = "address2_utcoffset")
    @TableField(value = "address2_utcoffset")
    @JSONField(name = "address2_utcoffset")
    @JsonProperty("address2_utcoffset")
    private Integer address2Utcoffset;
    /**
     * 传真
     */
    @TableField(value = "fax")
    @JSONField(name = "fax")
    @JsonProperty("fax")
    private String fax;
    /**
     * 地址 1
     */
    @DEField(name = "address1_composite")
    @TableField(value = "address1_composite")
    @JSONField(name = "address1_composite")
    @JsonProperty("address1_composite")
    private String address1Composite;
    /**
     * 默认图像
     */
    @TableField(value = "entityimage")
    @JSONField(name = "entityimage")
    @JsonProperty("entityimage")
    private String entityimage;
    /**
     * 电子邮件
     */
    @TableField(value = "emailaddress1")
    @JSONField(name = "emailaddress1")
    @JsonProperty("emailaddress1")
    private String emailaddress1;
    /**
     * 创建记录的时间
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 地址 1: 省/市/自治区
     */
    @DEField(name = "address1_stateorprovince")
    @TableField(value = "address1_stateorprovince")
    @JSONField(name = "address1_stateorprovince")
    @JsonProperty("address1_stateorprovince")
    private String address1Stateorprovince;
    /**
     * 开启的收入
     */
    @TableField(value = "openrevenue")
    @JSONField(name = "openrevenue")
    @JsonProperty("openrevenue")
    private BigDecimal openrevenue;
    /**
     * 地址 1: UPS 区域
     */
    @DEField(name = "address1_upszone")
    @TableField(value = "address1_upszone")
    @JSONField(name = "address1_upszone")
    @JsonProperty("address1_upszone")
    private String address1Upszone;
    /**
     * 跟踪电子邮件活动
     */
    @DEField(defaultValue = "1")
    @TableField(value = "followemail")
    @JSONField(name = "followemail")
    @JsonProperty("followemail")
    private Integer followemail;
    /**
     * 仅限市场营销
     */
    @DEField(defaultValue = "0")
    @TableField(value = "marketingonly")
    @JSONField(name = "marketingonly")
    @JsonProperty("marketingonly")
    private Integer marketingonly;
    /**
     * 员工人数
     */
    @TableField(value = "numberofemployees")
    @JSONField(name = "numberofemployees")
    @JsonProperty("numberofemployees")
    private Integer numberofemployees;
    /**
     * 主要电话
     */
    @TableField(value = "telephone1")
    @JSONField(name = "telephone1")
    @JsonProperty("telephone1")
    private String telephone1;
    /**
     * 时效 60
     */
    @TableField(value = "aging60")
    @JSONField(name = "aging60")
    @JsonProperty("aging60")
    private BigDecimal aging60;
    /**
     * 已发行股票
     */
    @TableField(value = "sharesoutstanding")
    @JSONField(name = "sharesoutstanding")
    @JsonProperty("sharesoutstanding")
    private Integer sharesoutstanding;
    /**
     * 地址 2: 货运条款
     */
    @DEField(name = "address2_freighttermscode")
    @TableField(value = "address2_freighttermscode")
    @JSONField(name = "address2_freighttermscode")
    @JsonProperty("address2_freighttermscode")
    private String address2Freighttermscode;
    /**
     * 实体图片时间戳
     */
    @DEField(name = "entityimage_timestamp")
    @TableField(value = "entityimage_timestamp")
    @JSONField(name = "entityimage_timestamp")
    @JsonProperty("entityimage_timestamp")
    private BigInteger entityimageTimestamp;
    /**
     * 年收入(基础货币)
     */
    @DEField(name = "revenue_base")
    @TableField(value = "revenue_base")
    @JSONField(name = "revenue_base")
    @JsonProperty("revenue_base")
    private BigDecimal revenueBase;
    /**
     * 客户等级
     */
    @TableField(value = "accountratingcode")
    @JSONField(name = "accountratingcode")
    @JsonProperty("accountratingcode")
    private String accountratingcode;
    /**
     * 不允许使用批量电子邮件
     */
    @DEField(defaultValue = "0")
    @TableField(value = "donotbulkemail")
    @JSONField(name = "donotbulkemail")
    @JsonProperty("donotbulkemail")
    private Integer donotbulkemail;
    /**
     * 时效 30
     */
    @TableField(value = "aging30")
    @JSONField(name = "aging30")
    @JsonProperty("aging30")
    private BigDecimal aging30;
    /**
     * 不允许使用批量邮件
     */
    @DEField(defaultValue = "0")
    @TableField(value = "donotbulkpostalmail")
    @JSONField(name = "donotbulkpostalmail")
    @JsonProperty("donotbulkpostalmail")
    private Integer donotbulkpostalmail;
    /**
     * 信用额度(基础货币)
     */
    @DEField(name = "creditlimit_base")
    @TableField(value = "creditlimit_base")
    @JSONField(name = "creditlimit_base")
    @JsonProperty("creditlimit_base")
    private BigDecimal creditlimitBase;
    /**
     * 时区规则版本号
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * 关系类型
     */
    @TableField(value = "customertypecode")
    @JSONField(name = "customertypecode")
    @JsonProperty("customertypecode")
    private String customertypecode;
    /**
     * 地址 2: 经度
     */
    @DEField(name = "address2_longitude")
    @TableField(value = "address2_longitude")
    @JSONField(name = "address2_longitude")
    @JsonProperty("address2_longitude")
    private Double address2Longitude;
    /**
     * 客户名称
     */
    @TableField(value = "accountname")
    @JSONField(name = "accountname")
    @JsonProperty("accountname")
    private String accountname;
    /**
     * 参与工作流
     */
    @DEField(defaultValue = "0")
    @TableField(value = "participatesinworkflow")
    @JSONField(name = "participatesinworkflow")
    @JsonProperty("participatesinworkflow")
    private Integer participatesinworkflow;
    /**
     * 首选联系方式
     */
    @TableField(value = "preferredcontactmethodcode")
    @JSONField(name = "preferredcontactmethodcode")
    @JsonProperty("preferredcontactmethodcode")
    private String preferredcontactmethodcode;
    /**
     * 首选日
     */
    @TableField(value = "preferredappointmentdaycode")
    @JSONField(name = "preferredappointmentdaycode")
    @JsonProperty("preferredappointmentdaycode")
    private String preferredappointmentdaycode;
    /**
     * 市值
     */
    @TableField(value = "marketcap")
    @JSONField(name = "marketcap")
    @JsonProperty("marketcap")
    private BigDecimal marketcap;
    /**
     * 地址 2: 电话 1
     */
    @DEField(name = "address2_telephone1")
    @TableField(value = "address2_telephone1")
    @JSONField(name = "address2_telephone1")
    @JsonProperty("address2_telephone1")
    private String address2Telephone1;
    /**
     * 商业类型
     */
    @TableField(value = "businesstypecode")
    @JSONField(name = "businesstypecode")
    @JsonProperty("businesstypecode")
    private String businesstypecode;
    /**
     * 实体图像 ID
     */
    @TableField(value = "entityimageid")
    @JSONField(name = "entityimageid")
    @JsonProperty("entityimageid")
    private String entityimageid;
    /**
     * 地址 2: 市/县
     */
    @DEField(name = "address2_city")
    @TableField(value = "address2_city")
    @JSONField(name = "address2_city")
    @JsonProperty("address2_city")
    private String address2City;
    /**
     * 所有权
     */
    @TableField(value = "ownershipcode")
    @JSONField(name = "ownershipcode")
    @JsonProperty("ownershipcode")
    private String ownershipcode;
    /**
     * UTC 转换时区代码
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * 客户编号
     */
    @TableField(value = "accountnumber")
    @JSONField(name = "accountnumber")
    @JsonProperty("accountnumber")
    private String accountnumber;
    /**
     * 地址 1: 电话 2
     */
    @DEField(name = "address1_telephone2")
    @TableField(value = "address1_telephone2")
    @JSONField(name = "address1_telephone2")
    @JsonProperty("address1_telephone2")
    private String address1Telephone2;
    /**
     * 地址 1: 街道 1
     */
    @DEField(name = "address1_line1")
    @TableField(value = "address1_line1")
    @JSONField(name = "address1_line1")
    @JsonProperty("address1_line1")
    private String address1Line1;
    /**
     * 不允许电话联络
     */
    @DEField(defaultValue = "0")
    @TableField(value = "donotphone")
    @JSONField(name = "donotphone")
    @JsonProperty("donotphone")
    private Integer donotphone;
    /**
     * 地址 1: 电话 3
     */
    @DEField(name = "address1_telephone3")
    @TableField(value = "address1_telephone3")
    @JSONField(name = "address1_telephone3")
    @JsonProperty("address1_telephone3")
    private String address1Telephone3;
    /**
     * 汇率
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * 地址 2: 街道 2
     */
    @DEField(name = "address2_line2")
    @TableField(value = "address2_line2")
    @JSONField(name = "address2_line2")
    @JsonProperty("address2_line2")
    private String address2Line2;
    /**
     * 地址 2: 电话 3
     */
    @DEField(name = "address2_telephone3")
    @TableField(value = "address2_telephone3")
    @JSONField(name = "address2_telephone3")
    @JsonProperty("address2_telephone3")
    private String address2Telephone3;
    /**
     * 联络电话
     */
    @DEField(name = "address1_telephone1")
    @TableField(value = "address1_telephone1")
    @JSONField(name = "address1_telephone1")
    @JsonProperty("address1_telephone1")
    private String address1Telephone1;
    /**
     * 地址 1: 传真
     */
    @DEField(name = "address1_fax")
    @TableField(value = "address1_fax")
    @JSONField(name = "address1_fax")
    @JsonProperty("address1_fax")
    private String address1Fax;
    /**
     * 地址 1: 货运条款
     */
    @DEField(name = "address1_freighttermscode")
    @TableField(value = "address1_freighttermscode")
    @JSONField(name = "address1_freighttermscode")
    @JsonProperty("address1_freighttermscode")
    private String address1Freighttermscode;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 合并
     */
    @DEField(defaultValue = "0")
    @TableField(value = "merged")
    @JSONField(name = "merged")
    @JsonProperty("merged")
    private Integer merged;
    /**
     * 地址 1: 送货方式
     */
    @DEField(name = "address1_shippingmethodcode")
    @TableField(value = "address1_shippingmethodcode")
    @JSONField(name = "address1_shippingmethodcode")
    @JsonProperty("address1_shippingmethodcode")
    private String address1Shippingmethodcode;
    /**
     * 地址 1: 名称
     */
    @DEField(name = "address1_name")
    @TableField(value = "address1_name")
    @JSONField(name = "address1_name")
    @JsonProperty("address1_name")
    private String address1Name;
    /**
     * 时效 30 (基础货币)
     */
    @DEField(name = "aging30_base")
    @TableField(value = "aging30_base")
    @JSONField(name = "aging30_base")
    @JsonProperty("aging30_base")
    private BigDecimal aging30Base;
    /**
     * 地址 2: 电话 2
     */
    @DEField(name = "address2_telephone2")
    @TableField(value = "address2_telephone2")
    @JSONField(name = "address2_telephone2")
    @JsonProperty("address2_telephone2")
    private String address2Telephone2;
    /**
     * 相关客户数量
     */
    @DEField(defaultValue = "0")
    @TableField(value = "childaccountcount")
    @JSONField(name = "childaccountcount")
    @JsonProperty("childaccountcount")
    private Integer childaccountcount;
    /**
     * 客户
     */
    @DEField(isKeyField=true)
    @TableId(value= "accountid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "accountid")
    @JsonProperty("accountid")
    private String accountid;
    /**
     * 主要 Twitter ID
     */
    @TableField(value = "primarytwitterid")
    @JSONField(name = "primarytwitterid")
    @JsonProperty("primarytwitterid")
    private String primarytwitterid;
    /**
     * 股票代号
     */
    @TableField(value = "tickersymbol")
    @JSONField(name = "tickersymbol")
    @JsonProperty("tickersymbol")
    private String tickersymbol;
    /**
     * 证券交易所
     */
    @TableField(value = "stockexchange")
    @JSONField(name = "stockexchange")
    @JsonProperty("stockexchange")
    private String stockexchange;
    /**
     * 地址 2: 省/市/自治区
     */
    @DEField(name = "address2_stateorprovince")
    @TableField(value = "address2_stateorprovince")
    @JSONField(name = "address2_stateorprovince")
    @JsonProperty("address2_stateorprovince")
    private String address2Stateorprovince;
    /**
     * FTP 站点
     */
    @TableField(value = "ftpsiteurl")
    @JSONField(name = "ftpsiteurl")
    @JsonProperty("ftpsiteurl")
    private String ftpsiteurl;
    /**
     * 状态描述
     */
    @DEField(defaultValue = "1")
    @TableField(value = "statuscode")
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;
    /**
     * 网站
     */
    @TableField(value = "websiteurl")
    @JSONField(name = "websiteurl")
    @JsonProperty("websiteurl")
    private String websiteurl;
    /**
     * 不允许使用邮件
     */
    @DEField(defaultValue = "0")
    @TableField(value = "donotpostalmail")
    @JSONField(name = "donotpostalmail")
    @JsonProperty("donotpostalmail")
    private Integer donotpostalmail;
    /**
     * 地址 1: 市/县
     */
    @DEField(name = "address1_city")
    @TableField(value = "address1_city")
    @JSONField(name = "address1_city")
    @JsonProperty("address1_city")
    private String address1City;
    /**
     * 地址 2: 传真
     */
    @DEField(name = "address2_fax")
    @TableField(value = "address2_fax")
    @JSONField(name = "address2_fax")
    @JsonProperty("address2_fax")
    private String address2Fax;
    /**
     * 时效 90 (基础货币)
     */
    @DEField(name = "aging90_base")
    @TableField(value = "aging90_base")
    @JSONField(name = "aging90_base")
    @JsonProperty("aging90_base")
    private BigDecimal aging90Base;
    /**
     * 开启的交易 (Last Updated On)
     */
    @DEField(name = "opendeals_date")
    @TableField(value = "opendeals_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "opendeals_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("opendeals_date")
    private Timestamp opendealsDate;
    /**
     * 遍历的路径
     */
    @TableField(value = "traversedpath")
    @JSONField(name = "traversedpath")
    @JsonProperty("traversedpath")
    private String traversedpath;
    /**
     * 送货方式
     */
    @TableField(value = "shippingmethodcode")
    @JSONField(name = "shippingmethodcode")
    @JsonProperty("shippingmethodcode")
    private String shippingmethodcode;
    /**
     * 地址 1: 街道 2
     */
    @DEField(name = "address1_line2")
    @TableField(value = "address1_line2")
    @JSONField(name = "address1_line2")
    @JsonProperty("address1_line2")
    private String address1Line2;
    /**
     * 开启的收入 (Base)
     */
    @DEField(name = "openrevenue_base")
    @TableField(value = "openrevenue_base")
    @JSONField(name = "openrevenue_base")
    @JsonProperty("openrevenue_base")
    private BigDecimal openrevenueBase;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 地址 2: 街道 3
     */
    @DEField(name = "address2_line3")
    @TableField(value = "address2_line3")
    @JSONField(name = "address2_line3")
    @JsonProperty("address2_line3")
    private String address2Line3;
    /**
     * 地址 1: 邮政编码
     */
    @DEField(name = "address1_postalcode")
    @TableField(value = "address1_postalcode")
    @JSONField(name = "address1_postalcode")
    @JsonProperty("address1_postalcode")
    private String address1Postalcode;
    /**
     * 地址 2: 纬度
     */
    @DEField(name = "address2_latitude")
    @TableField(value = "address2_latitude")
    @JSONField(name = "address2_latitude")
    @JsonProperty("address2_latitude")
    private Double address2Latitude;
    /**
     * 是否私有
     */
    @DEField(defaultValue = "0")
    @TableField(value = "private")
    @JSONField(name = "ibizprivate")
    @JsonProperty("ibizprivate")
    private Integer ibizprivate;
    /**
     * 区域代码
     */
    @TableField(value = "territorycode")
    @JSONField(name = "territorycode")
    @JsonProperty("territorycode")
    private String territorycode;
    /**
     * TeamsFollowed
     */
    @TableField(value = "teamsfollowed")
    @JSONField(name = "teamsfollowed")
    @JsonProperty("teamsfollowed")
    private Integer teamsfollowed;
    /**
     * 地址 1: 纬度
     */
    @DEField(name = "address1_latitude")
    @TableField(value = "address1_latitude")
    @JSONField(name = "address1_latitude")
    @JsonProperty("address1_latitude")
    private Double address1Latitude;
    /**
     * 电话 3
     */
    @TableField(value = "telephone3")
    @JSONField(name = "telephone3")
    @JsonProperty("telephone3")
    private String telephone3;
    /**
     * 负责人
     */
    @TableField(value = "ownerid")
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;
    /**
     * 地址 2: 国家/地区
     */
    @DEField(name = "address2_country")
    @TableField(value = "address2_country")
    @JSONField(name = "address2_country")
    @JsonProperty("address2_country")
    private String address2Country;
    /**
     * 主客户
     */
    @TableField(value = "masteraccountname")
    @JSONField(name = "masteraccountname")
    @JsonProperty("masteraccountname")
    private String masteraccountname;
    /**
     * 负责人
     */
    @TableField(value = "ownername")
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;
    /**
     * 行业
     */
    @TableField(value = "industrycode")
    @JSONField(name = "industrycode")
    @JsonProperty("industrycode")
    private String industrycode;
    /**
     * 客户
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 上一暂候时间
     */
    @TableField(value = "lastonholdtime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "lastonholdtime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("lastonholdtime")
    private Timestamp lastonholdtime;
    /**
     * 地址 2: ID
     */
    @DEField(name = "address2_addressid")
    @TableField(value = "address2_addressid")
    @JSONField(name = "address2_addressid")
    @JsonProperty("address2_addressid")
    private String address2Addressid;
    /**
     * 暂候时间(分钟)
     */
    @TableField(value = "onholdtime")
    @JSONField(name = "onholdtime")
    @JsonProperty("onholdtime")
    private Integer onholdtime;
    /**
     * 分类
     */
    @TableField(value = "accountclassificationcode")
    @JSONField(name = "accountclassificationcode")
    @JsonProperty("accountclassificationcode")
    private String accountclassificationcode;
    /**
     * 地址 1: 国家/地区
     */
    @DEField(name = "address1_country")
    @TableField(value = "address1_country")
    @JSONField(name = "address1_country")
    @JsonProperty("address1_country")
    private String address1Country;
    /**
     * 地址 1: 地址类型
     */
    @DEField(name = "address1_addresstypecode")
    @TableField(value = "address1_addresstypecode")
    @JSONField(name = "address1_addresstypecode")
    @JsonProperty("address1_addresstypecode")
    private String address1Addresstypecode;
    /**
     * 状态
     */
    @DEField(defaultValue = "0")
    @TableField(value = "statecode")
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;
    /**
     * 地址 2: 地址类型
     */
    @DEField(name = "address2_addresstypecode")
    @TableField(value = "address2_addresstypecode")
    @JSONField(name = "address2_addresstypecode")
    @JsonProperty("address2_addresstypecode")
    private String address2Addresstypecode;
    /**
     * 类别
     */
    @TableField(value = "accountcategorycode")
    @JSONField(name = "accountcategorycode")
    @JsonProperty("accountcategorycode")
    private String accountcategorycode;
    /**
     * 电子邮件地址 2
     */
    @TableField(value = "emailaddress2")
    @JSONField(name = "emailaddress2")
    @JsonProperty("emailaddress2")
    private String emailaddress2;
    /**
     * 地址 2: 送货方式
     */
    @DEField(name = "address2_shippingmethodcode")
    @TableField(value = "address2_shippingmethodcode")
    @JSONField(name = "address2_shippingmethodcode")
    @JsonProperty("address2_shippingmethodcode")
    private String address2Shippingmethodcode;
    /**
     * 导入序列号
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * 主要 Satori ID
     */
    @TableField(value = "primarysatoriid")
    @JSONField(name = "primarysatoriid")
    @JsonProperty("primarysatoriid")
    private String primarysatoriid;
    /**
     * 客户规模
     */
    @TableField(value = "customersizecode")
    @JSONField(name = "customersizecode")
    @JsonProperty("customersizecode")
    private String customersizecode;
    /**
     * 开启的交易
     */
    @TableField(value = "opendeals")
    @JSONField(name = "opendeals")
    @JsonProperty("opendeals")
    private Integer opendeals;
    /**
     * 发送市场营销资料
     */
    @DEField(defaultValue = "0")
    @TableField(value = "donotsendmm")
    @JSONField(name = "donotsendmm")
    @JsonProperty("donotsendmm")
    private Integer donotsendmm;
    /**
     * 流程
     */
    @TableField(value = "processid")
    @JSONField(name = "processid")
    @JsonProperty("processid")
    private String processid;
    /**
     * 付款方式
     */
    @TableField(value = "paymenttermscode")
    @JSONField(name = "paymenttermscode")
    @JsonProperty("paymenttermscode")
    private String paymenttermscode;
    /**
     * 行业编码
     */
    @TableField(value = "sic")
    @JSONField(name = "sic")
    @JsonProperty("sic")
    private String sic;
    /**
     * 地址 2: 主要联系人姓名
     */
    @DEField(name = "address2_primarycontactname")
    @TableField(value = "address2_primarycontactname")
    @JSONField(name = "address2_primarycontactname")
    @JsonProperty("address2_primarycontactname")
    private String address2Primarycontactname;
    /**
     * 地址 1: UTC 时差
     */
    @DEField(name = "address1_utcoffset")
    @TableField(value = "address1_utcoffset")
    @JSONField(name = "address1_utcoffset")
    @JsonProperty("address1_utcoffset")
    private Integer address1Utcoffset;
    /**
     * 上次参与市场活动的日期
     */
    @TableField(value = "lastusedincampaign")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "lastusedincampaign" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("lastusedincampaign")
    private Timestamp lastusedincampaign;
    /**
     * 时效 60 (基础货币)
     */
    @DEField(name = "aging60_base")
    @TableField(value = "aging60_base")
    @JSONField(name = "aging60_base")
    @JsonProperty("aging60_base")
    private BigDecimal aging60Base;
    /**
     * 不允许使用传真
     */
    @DEField(defaultValue = "0")
    @TableField(value = "donotfax")
    @JSONField(name = "donotfax")
    @JsonProperty("donotfax")
    private Integer donotfax;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 开启的收入 (State)
     */
    @DEField(name = "openrevenue_state")
    @TableField(value = "openrevenue_state")
    @JSONField(name = "openrevenue_state")
    @JsonProperty("openrevenue_state")
    private Integer openrevenueState;
    /**
     * 开启的交易 (State)
     */
    @DEField(name = "opendeals_state")
    @TableField(value = "opendeals_state")
    @JSONField(name = "opendeals_state")
    @JsonProperty("opendeals_state")
    private Integer opendealsState;
    /**
     * 版本号
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 地址 1: 经度
     */
    @DEField(name = "address1_longitude")
    @TableField(value = "address1_longitude")
    @JSONField(name = "address1_longitude")
    @JsonProperty("address1_longitude")
    private Double address1Longitude;
    /**
     * 时效 90
     */
    @TableField(value = "aging90")
    @JSONField(name = "aging90")
    @JsonProperty("aging90")
    private BigDecimal aging90;
    /**
     * 实体图片路径
     */
    @DEField(name = "entityimage_url")
    @TableField(value = "entityimage_url")
    @JSONField(name = "entityimage_url")
    @JsonProperty("entityimage_url")
    private String entityimageUrl;
    /**
     * 信用额度
     */
    @TableField(value = "creditlimit")
    @JSONField(name = "creditlimit")
    @JsonProperty("creditlimit")
    private BigDecimal creditlimit;
    /**
     * 首选用户
     */
    @TableField(value = "preferredsystemuserid")
    @JSONField(name = "preferredsystemuserid")
    @JsonProperty("preferredsystemuserid")
    private String preferredsystemuserid;
    /**
     * 年收入
     */
    @TableField(value = "revenue")
    @JSONField(name = "revenue")
    @JsonProperty("revenue")
    private BigDecimal revenue;
    /**
     * 地址 1: 县
     */
    @DEField(name = "address1_county")
    @TableField(value = "address1_county")
    @JSONField(name = "address1_county")
    @JsonProperty("address1_county")
    private String address1County;
    /**
     * 地址 1: 街道 3
     */
    @DEField(name = "address1_line3")
    @TableField(value = "address1_line3")
    @JSONField(name = "address1_line3")
    @JsonProperty("address1_line3")
    private String address1Line3;
    /**
     * 首选用户
     */
    @TableField(value = "preferredsystemusername")
    @JSONField(name = "preferredsystemusername")
    @JsonProperty("preferredsystemusername")
    private String preferredsystemusername;
    /**
     * 流程阶段
     */
    @TableField(value = "stageid")
    @JSONField(name = "stageid")
    @JsonProperty("stageid")
    private String stageid;
    /**
     * 不允许使用电子邮件
     */
    @DEField(defaultValue = "0")
    @TableField(value = "donotemail")
    @JSONField(name = "donotemail")
    @JsonProperty("donotemail")
    private Integer donotemail;
    /**
     * 地址 2: 邮政编码
     */
    @DEField(name = "address2_postalcode")
    @TableField(value = "address2_postalcode")
    @JSONField(name = "address2_postalcode")
    @JsonProperty("address2_postalcode")
    private String address2Postalcode;
    /**
     * 首选时间
     */
    @TableField(value = "preferredappointmenttimecode")
    @JSONField(name = "preferredappointmenttimecode")
    @JsonProperty("preferredappointmenttimecode")
    private String preferredappointmenttimecode;
    /**
     * 地址 2: UPS 区域
     */
    @DEField(name = "address2_upszone")
    @TableField(value = "address2_upszone")
    @JSONField(name = "address2_upszone")
    @JsonProperty("address2_upszone")
    private String address2Upszone;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 地址 2: 县
     */
    @DEField(name = "address2_county")
    @TableField(value = "address2_county")
    @JSONField(name = "address2_county")
    @JsonProperty("address2_county")
    private String address2County;
    /**
     * 地址 2
     */
    @DEField(name = "address2_composite")
    @TableField(value = "address2_composite")
    @JSONField(name = "address2_composite")
    @JsonProperty("address2_composite")
    private String address2Composite;
    /**
     * 地址 2: 名称
     */
    @DEField(name = "address2_name")
    @TableField(value = "address2_name")
    @JSONField(name = "address2_name")
    @JsonProperty("address2_name")
    private String address2Name;
    /**
     * 地址 2: 街道 1
     */
    @DEField(name = "address2_line1")
    @TableField(value = "address2_line1")
    @JSONField(name = "address2_line1")
    @JsonProperty("address2_line1")
    private String address2Line1;
    /**
     * 市值(基础货币)
     */
    @DEField(name = "marketcap_base")
    @TableField(value = "marketcap_base")
    @JSONField(name = "marketcap_base")
    @JsonProperty("marketcap_base")
    private BigDecimal marketcapBase;
    /**
     * 电子邮件地址 3
     */
    @TableField(value = "emailaddress3")
    @JSONField(name = "emailaddress3")
    @JsonProperty("emailaddress3")
    private String emailaddress3;
    /**
     * 所有者类型
     */
    @TableField(value = "ownertype")
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;
    /**
     * 其他电话
     */
    @TableField(value = "telephone2")
    @JSONField(name = "telephone2")
    @JsonProperty("telephone2")
    private String telephone2;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 地址 1: 邮政信箱
     */
    @DEField(name = "address1_postofficebox")
    @TableField(value = "address1_postofficebox")
    @JSONField(name = "address1_postofficebox")
    @JsonProperty("address1_postofficebox")
    private String address1Postofficebox;
    /**
     * 信用冻结
     */
    @DEField(defaultValue = "0")
    @TableField(value = "creditonhold")
    @JSONField(name = "creditonhold")
    @JsonProperty("creditonhold")
    private Integer creditonhold;
    /**
     * 地址 1: ID
     */
    @DEField(name = "address1_addressid")
    @TableField(value = "address1_addressid")
    @JSONField(name = "address1_addressid")
    @JsonProperty("address1_addressid")
    private String address1Addressid;
    /**
     * 地址 2: 邮政信箱
     */
    @DEField(name = "address2_postofficebox")
    @TableField(value = "address2_postofficebox")
    @JSONField(name = "address2_postofficebox")
    @JsonProperty("address2_postofficebox")
    private String address2Postofficebox;
    /**
     * 货币
     */
    @TableField(value = "currencyname")
    @JSONField(name = "currencyname")
    @JsonProperty("currencyname")
    private String currencyname;
    /**
     * 价目表
     */
    @TableField(value = "defaultpricelevelname")
    @JSONField(name = "defaultpricelevelname")
    @JsonProperty("defaultpricelevelname")
    private String defaultpricelevelname;
    /**
     * SLAName
     */
    @TableField(value = "slaname")
    @JSONField(name = "slaname")
    @JsonProperty("slaname")
    private String slaname;
    /**
     * 首选设施/设备
     */
    @TableField(value = "preferredequipmentname")
    @JSONField(name = "preferredequipmentname")
    @JsonProperty("preferredequipmentname")
    private String preferredequipmentname;
    /**
     * 首选服务
     */
    @TableField(value = "preferredservicename")
    @JSONField(name = "preferredservicename")
    @JsonProperty("preferredservicename")
    private String preferredservicename;
    /**
     * 区域
     */
    @TableField(value = "territoryname")
    @JSONField(name = "territoryname")
    @JsonProperty("territoryname")
    private String territoryname;
    /**
     * 主要联系人
     */
    @TableField(value = "primarycontactname")
    @JSONField(name = "primarycontactname")
    @JsonProperty("primarycontactname")
    private String primarycontactname;
    /**
     * 上级客户
     */
    @TableField(value = "parentaccountname")
    @JSONField(name = "parentaccountname")
    @JsonProperty("parentaccountname")
    private String parentaccountname;
    /**
     * 原始潜在顾客
     */
    @TableField(value = "originatingleadname")
    @JSONField(name = "originatingleadname")
    @JsonProperty("originatingleadname")
    private String originatingleadname;
    /**
     * 原始潜在顾客
     */
    @TableField(value = "originatingleadid")
    @JSONField(name = "originatingleadid")
    @JsonProperty("originatingleadid")
    private String originatingleadid;
    /**
     * 货币
     */
    @TableField(value = "transactioncurrencyid")
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;
    /**
     * 价目表
     */
    @TableField(value = "defaultpricelevelid")
    @JSONField(name = "defaultpricelevelid")
    @JsonProperty("defaultpricelevelid")
    private String defaultpricelevelid;
    /**
     * 上级单位
     */
    @TableField(value = "parentaccountid")
    @JSONField(name = "parentaccountid")
    @JsonProperty("parentaccountid")
    private String parentaccountid;
    /**
     * 首选设施/设备
     */
    @TableField(value = "preferredequipmentid")
    @JSONField(name = "preferredequipmentid")
    @JsonProperty("preferredequipmentid")
    private String preferredequipmentid;
    /**
     * 区域
     */
    @TableField(value = "territoryid")
    @JSONField(name = "territoryid")
    @JsonProperty("territoryid")
    private String territoryid;
    /**
     * 主要联系人
     */
    @TableField(value = "primarycontactid")
    @JSONField(name = "primarycontactid")
    @JsonProperty("primarycontactid")
    private String primarycontactid;
    /**
     * SLA
     */
    @TableField(value = "slaid")
    @JSONField(name = "slaid")
    @JsonProperty("slaid")
    private String slaid;
    /**
     * 首选服务
     */
    @TableField(value = "preferredserviceid")
    @JSONField(name = "preferredserviceid")
    @JsonProperty("preferredserviceid")
    private String preferredserviceid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Account parentaccount;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Contact primarycontact;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.service.domain.Equipment preferredequipment;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.sales.domain.Lead originatinglead;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.product.domain.PriceLevel defaultpricelevel;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.service.domain.IBizService preferredservice;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Sla sla;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Territory territory;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency;



    /**
     * 设置 [地址 1: 主要联系人姓名]
     */
    public void setAddress1Primarycontactname(String address1Primarycontactname){
        this.address1Primarycontactname = address1Primarycontactname ;
        this.modify("address1_primarycontactname",address1Primarycontactname);
    }

    /**
     * 设置 [开启的收入 (Last Updated On)]
     */
    public void setOpenrevenueDate(Timestamp openrevenueDate){
        this.openrevenueDate = openrevenueDate ;
        this.modify("openrevenue_date",openrevenueDate);
    }

    /**
     * 格式化日期 [开启的收入 (Last Updated On)]
     */
    public String formatOpenrevenueDate(){
        if (this.openrevenueDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(openrevenueDate);
    }
    /**
     * 设置 [地址 2: UTC 时差]
     */
    public void setAddress2Utcoffset(Integer address2Utcoffset){
        this.address2Utcoffset = address2Utcoffset ;
        this.modify("address2_utcoffset",address2Utcoffset);
    }

    /**
     * 设置 [传真]
     */
    public void setFax(String fax){
        this.fax = fax ;
        this.modify("fax",fax);
    }

    /**
     * 设置 [地址 1]
     */
    public void setAddress1Composite(String address1Composite){
        this.address1Composite = address1Composite ;
        this.modify("address1_composite",address1Composite);
    }

    /**
     * 设置 [默认图像]
     */
    public void setEntityimage(String entityimage){
        this.entityimage = entityimage ;
        this.modify("entityimage",entityimage);
    }

    /**
     * 设置 [电子邮件]
     */
    public void setEmailaddress1(String emailaddress1){
        this.emailaddress1 = emailaddress1 ;
        this.modify("emailaddress1",emailaddress1);
    }

    /**
     * 设置 [创建记录的时间]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [创建记录的时间]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [地址 1: 省/市/自治区]
     */
    public void setAddress1Stateorprovince(String address1Stateorprovince){
        this.address1Stateorprovince = address1Stateorprovince ;
        this.modify("address1_stateorprovince",address1Stateorprovince);
    }

    /**
     * 设置 [开启的收入]
     */
    public void setOpenrevenue(BigDecimal openrevenue){
        this.openrevenue = openrevenue ;
        this.modify("openrevenue",openrevenue);
    }

    /**
     * 设置 [地址 1: UPS 区域]
     */
    public void setAddress1Upszone(String address1Upszone){
        this.address1Upszone = address1Upszone ;
        this.modify("address1_upszone",address1Upszone);
    }

    /**
     * 设置 [跟踪电子邮件活动]
     */
    public void setFollowemail(Integer followemail){
        this.followemail = followemail ;
        this.modify("followemail",followemail);
    }

    /**
     * 设置 [仅限市场营销]
     */
    public void setMarketingonly(Integer marketingonly){
        this.marketingonly = marketingonly ;
        this.modify("marketingonly",marketingonly);
    }

    /**
     * 设置 [员工人数]
     */
    public void setNumberofemployees(Integer numberofemployees){
        this.numberofemployees = numberofemployees ;
        this.modify("numberofemployees",numberofemployees);
    }

    /**
     * 设置 [主要电话]
     */
    public void setTelephone1(String telephone1){
        this.telephone1 = telephone1 ;
        this.modify("telephone1",telephone1);
    }

    /**
     * 设置 [时效 60]
     */
    public void setAging60(BigDecimal aging60){
        this.aging60 = aging60 ;
        this.modify("aging60",aging60);
    }

    /**
     * 设置 [已发行股票]
     */
    public void setSharesoutstanding(Integer sharesoutstanding){
        this.sharesoutstanding = sharesoutstanding ;
        this.modify("sharesoutstanding",sharesoutstanding);
    }

    /**
     * 设置 [地址 2: 货运条款]
     */
    public void setAddress2Freighttermscode(String address2Freighttermscode){
        this.address2Freighttermscode = address2Freighttermscode ;
        this.modify("address2_freighttermscode",address2Freighttermscode);
    }

    /**
     * 设置 [实体图片时间戳]
     */
    public void setEntityimageTimestamp(BigInteger entityimageTimestamp){
        this.entityimageTimestamp = entityimageTimestamp ;
        this.modify("entityimage_timestamp",entityimageTimestamp);
    }

    /**
     * 设置 [年收入(基础货币)]
     */
    public void setRevenueBase(BigDecimal revenueBase){
        this.revenueBase = revenueBase ;
        this.modify("revenue_base",revenueBase);
    }

    /**
     * 设置 [客户等级]
     */
    public void setAccountratingcode(String accountratingcode){
        this.accountratingcode = accountratingcode ;
        this.modify("accountratingcode",accountratingcode);
    }

    /**
     * 设置 [不允许使用批量电子邮件]
     */
    public void setDonotbulkemail(Integer donotbulkemail){
        this.donotbulkemail = donotbulkemail ;
        this.modify("donotbulkemail",donotbulkemail);
    }

    /**
     * 设置 [时效 30]
     */
    public void setAging30(BigDecimal aging30){
        this.aging30 = aging30 ;
        this.modify("aging30",aging30);
    }

    /**
     * 设置 [不允许使用批量邮件]
     */
    public void setDonotbulkpostalmail(Integer donotbulkpostalmail){
        this.donotbulkpostalmail = donotbulkpostalmail ;
        this.modify("donotbulkpostalmail",donotbulkpostalmail);
    }

    /**
     * 设置 [信用额度(基础货币)]
     */
    public void setCreditlimitBase(BigDecimal creditlimitBase){
        this.creditlimitBase = creditlimitBase ;
        this.modify("creditlimit_base",creditlimitBase);
    }

    /**
     * 设置 [时区规则版本号]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [关系类型]
     */
    public void setCustomertypecode(String customertypecode){
        this.customertypecode = customertypecode ;
        this.modify("customertypecode",customertypecode);
    }

    /**
     * 设置 [地址 2: 经度]
     */
    public void setAddress2Longitude(Double address2Longitude){
        this.address2Longitude = address2Longitude ;
        this.modify("address2_longitude",address2Longitude);
    }

    /**
     * 设置 [客户名称]
     */
    public void setAccountname(String accountname){
        this.accountname = accountname ;
        this.modify("accountname",accountname);
    }

    /**
     * 设置 [参与工作流]
     */
    public void setParticipatesinworkflow(Integer participatesinworkflow){
        this.participatesinworkflow = participatesinworkflow ;
        this.modify("participatesinworkflow",participatesinworkflow);
    }

    /**
     * 设置 [首选联系方式]
     */
    public void setPreferredcontactmethodcode(String preferredcontactmethodcode){
        this.preferredcontactmethodcode = preferredcontactmethodcode ;
        this.modify("preferredcontactmethodcode",preferredcontactmethodcode);
    }

    /**
     * 设置 [首选日]
     */
    public void setPreferredappointmentdaycode(String preferredappointmentdaycode){
        this.preferredappointmentdaycode = preferredappointmentdaycode ;
        this.modify("preferredappointmentdaycode",preferredappointmentdaycode);
    }

    /**
     * 设置 [市值]
     */
    public void setMarketcap(BigDecimal marketcap){
        this.marketcap = marketcap ;
        this.modify("marketcap",marketcap);
    }

    /**
     * 设置 [地址 2: 电话 1]
     */
    public void setAddress2Telephone1(String address2Telephone1){
        this.address2Telephone1 = address2Telephone1 ;
        this.modify("address2_telephone1",address2Telephone1);
    }

    /**
     * 设置 [商业类型]
     */
    public void setBusinesstypecode(String businesstypecode){
        this.businesstypecode = businesstypecode ;
        this.modify("businesstypecode",businesstypecode);
    }

    /**
     * 设置 [实体图像 ID]
     */
    public void setEntityimageid(String entityimageid){
        this.entityimageid = entityimageid ;
        this.modify("entityimageid",entityimageid);
    }

    /**
     * 设置 [地址 2: 市/县]
     */
    public void setAddress2City(String address2City){
        this.address2City = address2City ;
        this.modify("address2_city",address2City);
    }

    /**
     * 设置 [所有权]
     */
    public void setOwnershipcode(String ownershipcode){
        this.ownershipcode = ownershipcode ;
        this.modify("ownershipcode",ownershipcode);
    }

    /**
     * 设置 [UTC 转换时区代码]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [客户编号]
     */
    public void setAccountnumber(String accountnumber){
        this.accountnumber = accountnumber ;
        this.modify("accountnumber",accountnumber);
    }

    /**
     * 设置 [地址 1: 电话 2]
     */
    public void setAddress1Telephone2(String address1Telephone2){
        this.address1Telephone2 = address1Telephone2 ;
        this.modify("address1_telephone2",address1Telephone2);
    }

    /**
     * 设置 [地址 1: 街道 1]
     */
    public void setAddress1Line1(String address1Line1){
        this.address1Line1 = address1Line1 ;
        this.modify("address1_line1",address1Line1);
    }

    /**
     * 设置 [不允许电话联络]
     */
    public void setDonotphone(Integer donotphone){
        this.donotphone = donotphone ;
        this.modify("donotphone",donotphone);
    }

    /**
     * 设置 [地址 1: 电话 3]
     */
    public void setAddress1Telephone3(String address1Telephone3){
        this.address1Telephone3 = address1Telephone3 ;
        this.modify("address1_telephone3",address1Telephone3);
    }

    /**
     * 设置 [汇率]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [地址 2: 街道 2]
     */
    public void setAddress2Line2(String address2Line2){
        this.address2Line2 = address2Line2 ;
        this.modify("address2_line2",address2Line2);
    }

    /**
     * 设置 [地址 2: 电话 3]
     */
    public void setAddress2Telephone3(String address2Telephone3){
        this.address2Telephone3 = address2Telephone3 ;
        this.modify("address2_telephone3",address2Telephone3);
    }

    /**
     * 设置 [联络电话]
     */
    public void setAddress1Telephone1(String address1Telephone1){
        this.address1Telephone1 = address1Telephone1 ;
        this.modify("address1_telephone1",address1Telephone1);
    }

    /**
     * 设置 [地址 1: 传真]
     */
    public void setAddress1Fax(String address1Fax){
        this.address1Fax = address1Fax ;
        this.modify("address1_fax",address1Fax);
    }

    /**
     * 设置 [地址 1: 货运条款]
     */
    public void setAddress1Freighttermscode(String address1Freighttermscode){
        this.address1Freighttermscode = address1Freighttermscode ;
        this.modify("address1_freighttermscode",address1Freighttermscode);
    }

    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [合并]
     */
    public void setMerged(Integer merged){
        this.merged = merged ;
        this.modify("merged",merged);
    }

    /**
     * 设置 [地址 1: 送货方式]
     */
    public void setAddress1Shippingmethodcode(String address1Shippingmethodcode){
        this.address1Shippingmethodcode = address1Shippingmethodcode ;
        this.modify("address1_shippingmethodcode",address1Shippingmethodcode);
    }

    /**
     * 设置 [地址 1: 名称]
     */
    public void setAddress1Name(String address1Name){
        this.address1Name = address1Name ;
        this.modify("address1_name",address1Name);
    }

    /**
     * 设置 [时效 30 (基础货币)]
     */
    public void setAging30Base(BigDecimal aging30Base){
        this.aging30Base = aging30Base ;
        this.modify("aging30_base",aging30Base);
    }

    /**
     * 设置 [地址 2: 电话 2]
     */
    public void setAddress2Telephone2(String address2Telephone2){
        this.address2Telephone2 = address2Telephone2 ;
        this.modify("address2_telephone2",address2Telephone2);
    }

    /**
     * 设置 [相关客户数量]
     */
    public void setChildaccountcount(Integer childaccountcount){
        this.childaccountcount = childaccountcount ;
        this.modify("childaccountcount",childaccountcount);
    }

    /**
     * 设置 [主要 Twitter ID]
     */
    public void setPrimarytwitterid(String primarytwitterid){
        this.primarytwitterid = primarytwitterid ;
        this.modify("primarytwitterid",primarytwitterid);
    }

    /**
     * 设置 [股票代号]
     */
    public void setTickersymbol(String tickersymbol){
        this.tickersymbol = tickersymbol ;
        this.modify("tickersymbol",tickersymbol);
    }

    /**
     * 设置 [证券交易所]
     */
    public void setStockexchange(String stockexchange){
        this.stockexchange = stockexchange ;
        this.modify("stockexchange",stockexchange);
    }

    /**
     * 设置 [地址 2: 省/市/自治区]
     */
    public void setAddress2Stateorprovince(String address2Stateorprovince){
        this.address2Stateorprovince = address2Stateorprovince ;
        this.modify("address2_stateorprovince",address2Stateorprovince);
    }

    /**
     * 设置 [FTP 站点]
     */
    public void setFtpsiteurl(String ftpsiteurl){
        this.ftpsiteurl = ftpsiteurl ;
        this.modify("ftpsiteurl",ftpsiteurl);
    }

    /**
     * 设置 [状态描述]
     */
    public void setStatuscode(Integer statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [网站]
     */
    public void setWebsiteurl(String websiteurl){
        this.websiteurl = websiteurl ;
        this.modify("websiteurl",websiteurl);
    }

    /**
     * 设置 [不允许使用邮件]
     */
    public void setDonotpostalmail(Integer donotpostalmail){
        this.donotpostalmail = donotpostalmail ;
        this.modify("donotpostalmail",donotpostalmail);
    }

    /**
     * 设置 [地址 1: 市/县]
     */
    public void setAddress1City(String address1City){
        this.address1City = address1City ;
        this.modify("address1_city",address1City);
    }

    /**
     * 设置 [地址 2: 传真]
     */
    public void setAddress2Fax(String address2Fax){
        this.address2Fax = address2Fax ;
        this.modify("address2_fax",address2Fax);
    }

    /**
     * 设置 [时效 90 (基础货币)]
     */
    public void setAging90Base(BigDecimal aging90Base){
        this.aging90Base = aging90Base ;
        this.modify("aging90_base",aging90Base);
    }

    /**
     * 设置 [开启的交易 (Last Updated On)]
     */
    public void setOpendealsDate(Timestamp opendealsDate){
        this.opendealsDate = opendealsDate ;
        this.modify("opendeals_date",opendealsDate);
    }

    /**
     * 格式化日期 [开启的交易 (Last Updated On)]
     */
    public String formatOpendealsDate(){
        if (this.opendealsDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(opendealsDate);
    }
    /**
     * 设置 [遍历的路径]
     */
    public void setTraversedpath(String traversedpath){
        this.traversedpath = traversedpath ;
        this.modify("traversedpath",traversedpath);
    }

    /**
     * 设置 [送货方式]
     */
    public void setShippingmethodcode(String shippingmethodcode){
        this.shippingmethodcode = shippingmethodcode ;
        this.modify("shippingmethodcode",shippingmethodcode);
    }

    /**
     * 设置 [地址 1: 街道 2]
     */
    public void setAddress1Line2(String address1Line2){
        this.address1Line2 = address1Line2 ;
        this.modify("address1_line2",address1Line2);
    }

    /**
     * 设置 [开启的收入 (Base)]
     */
    public void setOpenrevenueBase(BigDecimal openrevenueBase){
        this.openrevenueBase = openrevenueBase ;
        this.modify("openrevenue_base",openrevenueBase);
    }

    /**
     * 设置 [地址 2: 街道 3]
     */
    public void setAddress2Line3(String address2Line3){
        this.address2Line3 = address2Line3 ;
        this.modify("address2_line3",address2Line3);
    }

    /**
     * 设置 [地址 1: 邮政编码]
     */
    public void setAddress1Postalcode(String address1Postalcode){
        this.address1Postalcode = address1Postalcode ;
        this.modify("address1_postalcode",address1Postalcode);
    }

    /**
     * 设置 [地址 2: 纬度]
     */
    public void setAddress2Latitude(Double address2Latitude){
        this.address2Latitude = address2Latitude ;
        this.modify("address2_latitude",address2Latitude);
    }

    /**
     * 设置 [是否私有]
     */
    public void setIbizprivate(Integer ibizprivate){
        this.ibizprivate = ibizprivate ;
        this.modify("private",ibizprivate);
    }

    /**
     * 设置 [区域代码]
     */
    public void setTerritorycode(String territorycode){
        this.territorycode = territorycode ;
        this.modify("territorycode",territorycode);
    }

    /**
     * 设置 [TeamsFollowed]
     */
    public void setTeamsfollowed(Integer teamsfollowed){
        this.teamsfollowed = teamsfollowed ;
        this.modify("teamsfollowed",teamsfollowed);
    }

    /**
     * 设置 [地址 1: 纬度]
     */
    public void setAddress1Latitude(Double address1Latitude){
        this.address1Latitude = address1Latitude ;
        this.modify("address1_latitude",address1Latitude);
    }

    /**
     * 设置 [电话 3]
     */
    public void setTelephone3(String telephone3){
        this.telephone3 = telephone3 ;
        this.modify("telephone3",telephone3);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnerid(String ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [地址 2: 国家/地区]
     */
    public void setAddress2Country(String address2Country){
        this.address2Country = address2Country ;
        this.modify("address2_country",address2Country);
    }

    /**
     * 设置 [主客户]
     */
    public void setMasteraccountname(String masteraccountname){
        this.masteraccountname = masteraccountname ;
        this.modify("masteraccountname",masteraccountname);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnername(String ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [行业]
     */
    public void setIndustrycode(String industrycode){
        this.industrycode = industrycode ;
        this.modify("industrycode",industrycode);
    }

    /**
     * 设置 [客户]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [上一暂候时间]
     */
    public void setLastonholdtime(Timestamp lastonholdtime){
        this.lastonholdtime = lastonholdtime ;
        this.modify("lastonholdtime",lastonholdtime);
    }

    /**
     * 格式化日期 [上一暂候时间]
     */
    public String formatLastonholdtime(){
        if (this.lastonholdtime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(lastonholdtime);
    }
    /**
     * 设置 [地址 2: ID]
     */
    public void setAddress2Addressid(String address2Addressid){
        this.address2Addressid = address2Addressid ;
        this.modify("address2_addressid",address2Addressid);
    }

    /**
     * 设置 [暂候时间(分钟)]
     */
    public void setOnholdtime(Integer onholdtime){
        this.onholdtime = onholdtime ;
        this.modify("onholdtime",onholdtime);
    }

    /**
     * 设置 [分类]
     */
    public void setAccountclassificationcode(String accountclassificationcode){
        this.accountclassificationcode = accountclassificationcode ;
        this.modify("accountclassificationcode",accountclassificationcode);
    }

    /**
     * 设置 [地址 1: 国家/地区]
     */
    public void setAddress1Country(String address1Country){
        this.address1Country = address1Country ;
        this.modify("address1_country",address1Country);
    }

    /**
     * 设置 [地址 1: 地址类型]
     */
    public void setAddress1Addresstypecode(String address1Addresstypecode){
        this.address1Addresstypecode = address1Addresstypecode ;
        this.modify("address1_addresstypecode",address1Addresstypecode);
    }

    /**
     * 设置 [状态]
     */
    public void setStatecode(Integer statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [地址 2: 地址类型]
     */
    public void setAddress2Addresstypecode(String address2Addresstypecode){
        this.address2Addresstypecode = address2Addresstypecode ;
        this.modify("address2_addresstypecode",address2Addresstypecode);
    }

    /**
     * 设置 [类别]
     */
    public void setAccountcategorycode(String accountcategorycode){
        this.accountcategorycode = accountcategorycode ;
        this.modify("accountcategorycode",accountcategorycode);
    }

    /**
     * 设置 [电子邮件地址 2]
     */
    public void setEmailaddress2(String emailaddress2){
        this.emailaddress2 = emailaddress2 ;
        this.modify("emailaddress2",emailaddress2);
    }

    /**
     * 设置 [地址 2: 送货方式]
     */
    public void setAddress2Shippingmethodcode(String address2Shippingmethodcode){
        this.address2Shippingmethodcode = address2Shippingmethodcode ;
        this.modify("address2_shippingmethodcode",address2Shippingmethodcode);
    }

    /**
     * 设置 [导入序列号]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [主要 Satori ID]
     */
    public void setPrimarysatoriid(String primarysatoriid){
        this.primarysatoriid = primarysatoriid ;
        this.modify("primarysatoriid",primarysatoriid);
    }

    /**
     * 设置 [客户规模]
     */
    public void setCustomersizecode(String customersizecode){
        this.customersizecode = customersizecode ;
        this.modify("customersizecode",customersizecode);
    }

    /**
     * 设置 [开启的交易]
     */
    public void setOpendeals(Integer opendeals){
        this.opendeals = opendeals ;
        this.modify("opendeals",opendeals);
    }

    /**
     * 设置 [发送市场营销资料]
     */
    public void setDonotsendmm(Integer donotsendmm){
        this.donotsendmm = donotsendmm ;
        this.modify("donotsendmm",donotsendmm);
    }

    /**
     * 设置 [流程]
     */
    public void setProcessid(String processid){
        this.processid = processid ;
        this.modify("processid",processid);
    }

    /**
     * 设置 [付款方式]
     */
    public void setPaymenttermscode(String paymenttermscode){
        this.paymenttermscode = paymenttermscode ;
        this.modify("paymenttermscode",paymenttermscode);
    }

    /**
     * 设置 [行业编码]
     */
    public void setSic(String sic){
        this.sic = sic ;
        this.modify("sic",sic);
    }

    /**
     * 设置 [地址 2: 主要联系人姓名]
     */
    public void setAddress2Primarycontactname(String address2Primarycontactname){
        this.address2Primarycontactname = address2Primarycontactname ;
        this.modify("address2_primarycontactname",address2Primarycontactname);
    }

    /**
     * 设置 [地址 1: UTC 时差]
     */
    public void setAddress1Utcoffset(Integer address1Utcoffset){
        this.address1Utcoffset = address1Utcoffset ;
        this.modify("address1_utcoffset",address1Utcoffset);
    }

    /**
     * 设置 [上次参与市场活动的日期]
     */
    public void setLastusedincampaign(Timestamp lastusedincampaign){
        this.lastusedincampaign = lastusedincampaign ;
        this.modify("lastusedincampaign",lastusedincampaign);
    }

    /**
     * 格式化日期 [上次参与市场活动的日期]
     */
    public String formatLastusedincampaign(){
        if (this.lastusedincampaign == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(lastusedincampaign);
    }
    /**
     * 设置 [时效 60 (基础货币)]
     */
    public void setAging60Base(BigDecimal aging60Base){
        this.aging60Base = aging60Base ;
        this.modify("aging60_base",aging60Base);
    }

    /**
     * 设置 [不允许使用传真]
     */
    public void setDonotfax(Integer donotfax){
        this.donotfax = donotfax ;
        this.modify("donotfax",donotfax);
    }

    /**
     * 设置 [开启的收入 (State)]
     */
    public void setOpenrevenueState(Integer openrevenueState){
        this.openrevenueState = openrevenueState ;
        this.modify("openrevenue_state",openrevenueState);
    }

    /**
     * 设置 [开启的交易 (State)]
     */
    public void setOpendealsState(Integer opendealsState){
        this.opendealsState = opendealsState ;
        this.modify("opendeals_state",opendealsState);
    }

    /**
     * 设置 [版本号]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [地址 1: 经度]
     */
    public void setAddress1Longitude(Double address1Longitude){
        this.address1Longitude = address1Longitude ;
        this.modify("address1_longitude",address1Longitude);
    }

    /**
     * 设置 [时效 90]
     */
    public void setAging90(BigDecimal aging90){
        this.aging90 = aging90 ;
        this.modify("aging90",aging90);
    }

    /**
     * 设置 [实体图片路径]
     */
    public void setEntityimageUrl(String entityimageUrl){
        this.entityimageUrl = entityimageUrl ;
        this.modify("entityimage_url",entityimageUrl);
    }

    /**
     * 设置 [信用额度]
     */
    public void setCreditlimit(BigDecimal creditlimit){
        this.creditlimit = creditlimit ;
        this.modify("creditlimit",creditlimit);
    }

    /**
     * 设置 [首选用户]
     */
    public void setPreferredsystemuserid(String preferredsystemuserid){
        this.preferredsystemuserid = preferredsystemuserid ;
        this.modify("preferredsystemuserid",preferredsystemuserid);
    }

    /**
     * 设置 [年收入]
     */
    public void setRevenue(BigDecimal revenue){
        this.revenue = revenue ;
        this.modify("revenue",revenue);
    }

    /**
     * 设置 [地址 1: 县]
     */
    public void setAddress1County(String address1County){
        this.address1County = address1County ;
        this.modify("address1_county",address1County);
    }

    /**
     * 设置 [地址 1: 街道 3]
     */
    public void setAddress1Line3(String address1Line3){
        this.address1Line3 = address1Line3 ;
        this.modify("address1_line3",address1Line3);
    }

    /**
     * 设置 [首选用户]
     */
    public void setPreferredsystemusername(String preferredsystemusername){
        this.preferredsystemusername = preferredsystemusername ;
        this.modify("preferredsystemusername",preferredsystemusername);
    }

    /**
     * 设置 [流程阶段]
     */
    public void setStageid(String stageid){
        this.stageid = stageid ;
        this.modify("stageid",stageid);
    }

    /**
     * 设置 [不允许使用电子邮件]
     */
    public void setDonotemail(Integer donotemail){
        this.donotemail = donotemail ;
        this.modify("donotemail",donotemail);
    }

    /**
     * 设置 [地址 2: 邮政编码]
     */
    public void setAddress2Postalcode(String address2Postalcode){
        this.address2Postalcode = address2Postalcode ;
        this.modify("address2_postalcode",address2Postalcode);
    }

    /**
     * 设置 [首选时间]
     */
    public void setPreferredappointmenttimecode(String preferredappointmenttimecode){
        this.preferredappointmenttimecode = preferredappointmenttimecode ;
        this.modify("preferredappointmenttimecode",preferredappointmenttimecode);
    }

    /**
     * 设置 [地址 2: UPS 区域]
     */
    public void setAddress2Upszone(String address2Upszone){
        this.address2Upszone = address2Upszone ;
        this.modify("address2_upszone",address2Upszone);
    }

    /**
     * 设置 [地址 2: 县]
     */
    public void setAddress2County(String address2County){
        this.address2County = address2County ;
        this.modify("address2_county",address2County);
    }

    /**
     * 设置 [地址 2]
     */
    public void setAddress2Composite(String address2Composite){
        this.address2Composite = address2Composite ;
        this.modify("address2_composite",address2Composite);
    }

    /**
     * 设置 [地址 2: 名称]
     */
    public void setAddress2Name(String address2Name){
        this.address2Name = address2Name ;
        this.modify("address2_name",address2Name);
    }

    /**
     * 设置 [地址 2: 街道 1]
     */
    public void setAddress2Line1(String address2Line1){
        this.address2Line1 = address2Line1 ;
        this.modify("address2_line1",address2Line1);
    }

    /**
     * 设置 [市值(基础货币)]
     */
    public void setMarketcapBase(BigDecimal marketcapBase){
        this.marketcapBase = marketcapBase ;
        this.modify("marketcap_base",marketcapBase);
    }

    /**
     * 设置 [电子邮件地址 3]
     */
    public void setEmailaddress3(String emailaddress3){
        this.emailaddress3 = emailaddress3 ;
        this.modify("emailaddress3",emailaddress3);
    }

    /**
     * 设置 [所有者类型]
     */
    public void setOwnertype(String ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [其他电话]
     */
    public void setTelephone2(String telephone2){
        this.telephone2 = telephone2 ;
        this.modify("telephone2",telephone2);
    }

    /**
     * 设置 [地址 1: 邮政信箱]
     */
    public void setAddress1Postofficebox(String address1Postofficebox){
        this.address1Postofficebox = address1Postofficebox ;
        this.modify("address1_postofficebox",address1Postofficebox);
    }

    /**
     * 设置 [信用冻结]
     */
    public void setCreditonhold(Integer creditonhold){
        this.creditonhold = creditonhold ;
        this.modify("creditonhold",creditonhold);
    }

    /**
     * 设置 [地址 1: ID]
     */
    public void setAddress1Addressid(String address1Addressid){
        this.address1Addressid = address1Addressid ;
        this.modify("address1_addressid",address1Addressid);
    }

    /**
     * 设置 [地址 2: 邮政信箱]
     */
    public void setAddress2Postofficebox(String address2Postofficebox){
        this.address2Postofficebox = address2Postofficebox ;
        this.modify("address2_postofficebox",address2Postofficebox);
    }

    /**
     * 设置 [货币]
     */
    public void setCurrencyname(String currencyname){
        this.currencyname = currencyname ;
        this.modify("currencyname",currencyname);
    }

    /**
     * 设置 [价目表]
     */
    public void setDefaultpricelevelname(String defaultpricelevelname){
        this.defaultpricelevelname = defaultpricelevelname ;
        this.modify("defaultpricelevelname",defaultpricelevelname);
    }

    /**
     * 设置 [SLAName]
     */
    public void setSlaname(String slaname){
        this.slaname = slaname ;
        this.modify("slaname",slaname);
    }

    /**
     * 设置 [首选设施/设备]
     */
    public void setPreferredequipmentname(String preferredequipmentname){
        this.preferredequipmentname = preferredequipmentname ;
        this.modify("preferredequipmentname",preferredequipmentname);
    }

    /**
     * 设置 [首选服务]
     */
    public void setPreferredservicename(String preferredservicename){
        this.preferredservicename = preferredservicename ;
        this.modify("preferredservicename",preferredservicename);
    }

    /**
     * 设置 [区域]
     */
    public void setTerritoryname(String territoryname){
        this.territoryname = territoryname ;
        this.modify("territoryname",territoryname);
    }

    /**
     * 设置 [主要联系人]
     */
    public void setPrimarycontactname(String primarycontactname){
        this.primarycontactname = primarycontactname ;
        this.modify("primarycontactname",primarycontactname);
    }

    /**
     * 设置 [上级客户]
     */
    public void setParentaccountname(String parentaccountname){
        this.parentaccountname = parentaccountname ;
        this.modify("parentaccountname",parentaccountname);
    }

    /**
     * 设置 [原始潜在顾客]
     */
    public void setOriginatingleadname(String originatingleadname){
        this.originatingleadname = originatingleadname ;
        this.modify("originatingleadname",originatingleadname);
    }

    /**
     * 设置 [原始潜在顾客]
     */
    public void setOriginatingleadid(String originatingleadid){
        this.originatingleadid = originatingleadid ;
        this.modify("originatingleadid",originatingleadid);
    }

    /**
     * 设置 [货币]
     */
    public void setTransactioncurrencyid(String transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }

    /**
     * 设置 [价目表]
     */
    public void setDefaultpricelevelid(String defaultpricelevelid){
        this.defaultpricelevelid = defaultpricelevelid ;
        this.modify("defaultpricelevelid",defaultpricelevelid);
    }

    /**
     * 设置 [上级单位]
     */
    public void setParentaccountid(String parentaccountid){
        this.parentaccountid = parentaccountid ;
        this.modify("parentaccountid",parentaccountid);
    }

    /**
     * 设置 [首选设施/设备]
     */
    public void setPreferredequipmentid(String preferredequipmentid){
        this.preferredequipmentid = preferredequipmentid ;
        this.modify("preferredequipmentid",preferredequipmentid);
    }

    /**
     * 设置 [区域]
     */
    public void setTerritoryid(String territoryid){
        this.territoryid = territoryid ;
        this.modify("territoryid",territoryid);
    }

    /**
     * 设置 [主要联系人]
     */
    public void setPrimarycontactid(String primarycontactid){
        this.primarycontactid = primarycontactid ;
        this.modify("primarycontactid",primarycontactid);
    }

    /**
     * 设置 [SLA]
     */
    public void setSlaid(String slaid){
        this.slaid = slaid ;
        this.modify("slaid",slaid);
    }

    /**
     * 设置 [首选服务]
     */
    public void setPreferredserviceid(String preferredserviceid){
        this.preferredserviceid = preferredserviceid ;
        this.modify("preferredserviceid",preferredserviceid);
    }


}



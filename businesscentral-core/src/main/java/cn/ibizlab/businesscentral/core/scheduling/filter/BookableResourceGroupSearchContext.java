package cn.ibizlab.businesscentral.core.scheduling.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.scheduling.domain.BookableResourceGroup;
/**
 * 关系型数据实体[BookableResourceGroup] 查询条件对象
 */
@Slf4j
@Data
public class BookableResourceGroupSearchContext extends QueryWrapperContext<BookableResourceGroup> {

	private String n_bookableresgroupname_like;//[可预订资源组名称]
	public void setN_bookableresgroupname_like(String n_bookableresgroupname_like) {
        this.n_bookableresgroupname_like = n_bookableresgroupname_like;
        if(!ObjectUtils.isEmpty(this.n_bookableresgroupname_like)){
            this.getSearchCond().like("bookableresgroupname", n_bookableresgroupname_like);
        }
    }
	private Integer n_statuscode_eq;//[状态描述]
	public void setN_statuscode_eq(Integer n_statuscode_eq) {
        this.n_statuscode_eq = n_statuscode_eq;
        if(!ObjectUtils.isEmpty(this.n_statuscode_eq)){
            this.getSearchCond().eq("statuscode", n_statuscode_eq);
        }
    }
	private Integer n_statecode_eq;//[状态]
	public void setN_statecode_eq(Integer n_statecode_eq) {
        this.n_statecode_eq = n_statecode_eq;
        if(!ObjectUtils.isEmpty(this.n_statecode_eq)){
            this.getSearchCond().eq("statecode", n_statecode_eq);
        }
    }
	private String n_parentresource_eq;//[父资源]
	public void setN_parentresource_eq(String n_parentresource_eq) {
        this.n_parentresource_eq = n_parentresource_eq;
        if(!ObjectUtils.isEmpty(this.n_parentresource_eq)){
            this.getSearchCond().eq("parentresource", n_parentresource_eq);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }
	private String n_childresource_eq;//[子资源]
	public void setN_childresource_eq(String n_childresource_eq) {
        this.n_childresource_eq = n_childresource_eq;
        if(!ObjectUtils.isEmpty(this.n_childresource_eq)){
            this.getSearchCond().eq("childresource", n_childresource_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("bookableresgroupname", query)   
            );
		 }
	}
}




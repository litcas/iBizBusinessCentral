package cn.ibizlab.businesscentral.core.service.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.service.domain.ResourceSpec;
import cn.ibizlab.businesscentral.core.service.filter.ResourceSpecSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[ResourceSpec] 服务对象接口
 */
public interface IResourceSpecService extends IService<ResourceSpec>{

    boolean create(ResourceSpec et) ;
    void createBatch(List<ResourceSpec> list) ;
    boolean update(ResourceSpec et) ;
    void updateBatch(List<ResourceSpec> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    ResourceSpec get(String key) ;
    ResourceSpec getDraft(ResourceSpec et) ;
    boolean checkKey(ResourceSpec et) ;
    boolean save(ResourceSpec et) ;
    void saveBatch(List<ResourceSpec> list) ;
    Page<ResourceSpec> searchDefault(ResourceSpecSearchContext context) ;
    List<ResourceSpec> selectByBusinessunitid(String businessunitid) ;
    void removeByBusinessunitid(String businessunitid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<ResourceSpec> getResourcespecByIds(List<String> ids) ;
    List<ResourceSpec> getResourcespecByEntities(List<ResourceSpec> entities) ;
}



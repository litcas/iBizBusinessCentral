package cn.ibizlab.businesscentral.core.base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.base.domain.ActivityParty;
/**
 * 关系型数据实体[ActivityParty] 查询条件对象
 */
@Slf4j
@Data
public class ActivityPartySearchContext extends QueryWrapperContext<ActivityParty> {

	private String n_instancetypecode_eq;//[约会类型]
	public void setN_instancetypecode_eq(String n_instancetypecode_eq) {
        this.n_instancetypecode_eq = n_instancetypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_instancetypecode_eq)){
            this.getSearchCond().eq("instancetypecode", n_instancetypecode_eq);
        }
    }
	private String n_partyidname_like;//[PartyIdName]
	public void setN_partyidname_like(String n_partyidname_like) {
        this.n_partyidname_like = n_partyidname_like;
        if(!ObjectUtils.isEmpty(this.n_partyidname_like)){
            this.getSearchCond().like("partyidname", n_partyidname_like);
        }
    }
	private String n_participationtypemask_eq;//[参与类型]
	public void setN_participationtypemask_eq(String n_participationtypemask_eq) {
        this.n_participationtypemask_eq = n_participationtypemask_eq;
        if(!ObjectUtils.isEmpty(this.n_participationtypemask_eq)){
            this.getSearchCond().eq("participationtypemask", n_participationtypemask_eq);
        }
    }
	private String n_resourcespecid_eq;//[资源规格]
	public void setN_resourcespecid_eq(String n_resourcespecid_eq) {
        this.n_resourcespecid_eq = n_resourcespecid_eq;
        if(!ObjectUtils.isEmpty(this.n_resourcespecid_eq)){
            this.getSearchCond().eq("resourcespecid", n_resourcespecid_eq);
        }
    }
	private String n_activityid_eq;//[活动]
	public void setN_activityid_eq(String n_activityid_eq) {
        this.n_activityid_eq = n_activityid_eq;
        if(!ObjectUtils.isEmpty(this.n_activityid_eq)){
            this.getSearchCond().eq("activityid", n_activityid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("partyidname", query)   
            );
		 }
	}
}




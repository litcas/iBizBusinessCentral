package cn.ibizlab.businesscentral.core.service.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.service.domain.IncidentCustomer;
import cn.ibizlab.businesscentral.core.service.filter.IncidentCustomerSearchContext;
import cn.ibizlab.businesscentral.core.service.service.IIncidentCustomerService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.service.mapper.IncidentCustomerMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[案例客户] 服务对象接口实现
 */
@Slf4j
@Service("IncidentCustomerServiceImpl")
public class IncidentCustomerServiceImpl extends ServiceImpl<IncidentCustomerMapper, IncidentCustomer> implements IIncidentCustomerService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IIncidentService incidentService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(IncidentCustomer et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getCustomerid()),et);
        return true;
    }

    @Override
    public void createBatch(List<IncidentCustomer> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(IncidentCustomer et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("customerid",et.getCustomerid())))
            return false;
        CachedBeanCopier.copy(get(et.getCustomerid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<IncidentCustomer> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public IncidentCustomer get(String key) {
        IncidentCustomer et = getById(key);
        if(et==null){
            et=new IncidentCustomer();
            et.setCustomerid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public IncidentCustomer getDraft(IncidentCustomer et) {
        return et;
    }

    @Override
    public boolean checkKey(IncidentCustomer et) {
        return (!ObjectUtils.isEmpty(et.getCustomerid()))&&(!Objects.isNull(this.getById(et.getCustomerid())));
    }
    @Override
    @Transactional
    public boolean save(IncidentCustomer et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(IncidentCustomer et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<IncidentCustomer> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<IncidentCustomer> list) {
        saveOrUpdateBatch(list,batchSize);
    }



    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<IncidentCustomer> searchDefault(IncidentCustomerSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<IncidentCustomer> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<IncidentCustomer>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 IndexDER
     */
    @Override
    public Page<IncidentCustomer> searchIndexDER(IncidentCustomerSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<IncidentCustomer> pages=baseMapper.searchIndexDER(context.getPages(),context,context.getSelectCond());
        return new PageImpl<IncidentCustomer>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }


}




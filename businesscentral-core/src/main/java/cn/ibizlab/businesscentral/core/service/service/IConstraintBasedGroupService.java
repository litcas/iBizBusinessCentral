package cn.ibizlab.businesscentral.core.service.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.service.domain.ConstraintBasedGroup;
import cn.ibizlab.businesscentral.core.service.filter.ConstraintBasedGroupSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[ConstraintBasedGroup] 服务对象接口
 */
public interface IConstraintBasedGroupService extends IService<ConstraintBasedGroup>{

    boolean create(ConstraintBasedGroup et) ;
    void createBatch(List<ConstraintBasedGroup> list) ;
    boolean update(ConstraintBasedGroup et) ;
    void updateBatch(List<ConstraintBasedGroup> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    ConstraintBasedGroup get(String key) ;
    ConstraintBasedGroup getDraft(ConstraintBasedGroup et) ;
    boolean checkKey(ConstraintBasedGroup et) ;
    boolean save(ConstraintBasedGroup et) ;
    void saveBatch(List<ConstraintBasedGroup> list) ;
    Page<ConstraintBasedGroup> searchDefault(ConstraintBasedGroupSearchContext context) ;
    List<ConstraintBasedGroup> selectByBusinessunitid(String businessunitid) ;
    void removeByBusinessunitid(String businessunitid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<ConstraintBasedGroup> getConstraintbasedgroupByIds(List<String> ids) ;
    List<ConstraintBasedGroup> getConstraintbasedgroupByEntities(List<ConstraintBasedGroup> entities) ;
}



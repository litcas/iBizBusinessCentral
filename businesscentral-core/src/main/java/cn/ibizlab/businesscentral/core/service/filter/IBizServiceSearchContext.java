package cn.ibizlab.businesscentral.core.service.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.service.domain.IBizService;
/**
 * 关系型数据实体[IBizService] 查询条件对象
 */
@Slf4j
@Data
public class IBizServiceSearchContext extends QueryWrapperContext<IBizService> {

	private Integer n_initialstatuscode_eq;//[初始状态描述]
	public void setN_initialstatuscode_eq(Integer n_initialstatuscode_eq) {
        this.n_initialstatuscode_eq = n_initialstatuscode_eq;
        if(!ObjectUtils.isEmpty(this.n_initialstatuscode_eq)){
            this.getSearchCond().eq("initialstatuscode", n_initialstatuscode_eq);
        }
    }
	private String n_servicename_like;//[service名称]
	public void setN_servicename_like(String n_servicename_like) {
        this.n_servicename_like = n_servicename_like;
        if(!ObjectUtils.isEmpty(this.n_servicename_like)){
            this.getSearchCond().like("servicename", n_servicename_like);
        }
    }
	private String n_resourcespecid_eq;//[所需资源]
	public void setN_resourcespecid_eq(String n_resourcespecid_eq) {
        this.n_resourcespecid_eq = n_resourcespecid_eq;
        if(!ObjectUtils.isEmpty(this.n_resourcespecid_eq)){
            this.getSearchCond().eq("resourcespecid", n_resourcespecid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("servicename", query)   
            );
		 }
	}
}




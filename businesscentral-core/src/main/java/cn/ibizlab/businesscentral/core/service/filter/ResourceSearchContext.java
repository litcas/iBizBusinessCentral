package cn.ibizlab.businesscentral.core.service.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.service.domain.Resource;
/**
 * 关系型数据实体[Resource] 查询条件对象
 */
@Slf4j
@Data
public class ResourceSearchContext extends QueryWrapperContext<Resource> {

	private String n_resourcename_like;//[资源名称]
	public void setN_resourcename_like(String n_resourcename_like) {
        this.n_resourcename_like = n_resourcename_like;
        if(!ObjectUtils.isEmpty(this.n_resourcename_like)){
            this.getSearchCond().like("resourcename", n_resourcename_like);
        }
    }
	private String n_organizationid_eq;//[组织]
	public void setN_organizationid_eq(String n_organizationid_eq) {
        this.n_organizationid_eq = n_organizationid_eq;
        if(!ObjectUtils.isEmpty(this.n_organizationid_eq)){
            this.getSearchCond().eq("organizationid", n_organizationid_eq);
        }
    }
	private String n_businessunitid_eq;//[Business Unit Id]
	public void setN_businessunitid_eq(String n_businessunitid_eq) {
        this.n_businessunitid_eq = n_businessunitid_eq;
        if(!ObjectUtils.isEmpty(this.n_businessunitid_eq)){
            this.getSearchCond().eq("businessunitid", n_businessunitid_eq);
        }
    }
	private String n_siteid_eq;//[场所]
	public void setN_siteid_eq(String n_siteid_eq) {
        this.n_siteid_eq = n_siteid_eq;
        if(!ObjectUtils.isEmpty(this.n_siteid_eq)){
            this.getSearchCond().eq("siteid", n_siteid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("resourcename", query)   
            );
		 }
	}
}




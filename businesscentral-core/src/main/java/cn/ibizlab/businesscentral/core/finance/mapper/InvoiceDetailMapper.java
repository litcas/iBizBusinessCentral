package cn.ibizlab.businesscentral.core.finance.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.finance.domain.InvoiceDetail;
import cn.ibizlab.businesscentral.core.finance.filter.InvoiceDetailSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface InvoiceDetailMapper extends BaseMapper<InvoiceDetail>{

    Page<InvoiceDetail> searchDefault(IPage page, @Param("srf") InvoiceDetailSearchContext context, @Param("ew") Wrapper<InvoiceDetail> wrapper) ;
    @Override
    InvoiceDetail selectById(Serializable id);
    @Override
    int insert(InvoiceDetail entity);
    @Override
    int updateById(@Param(Constants.ENTITY) InvoiceDetail entity);
    @Override
    int update(@Param(Constants.ENTITY) InvoiceDetail entity, @Param("ew") Wrapper<InvoiceDetail> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<InvoiceDetail> selectByParentbundleidref(@Param("invoicedetailid") Serializable invoicedetailid) ;

    List<InvoiceDetail> selectByInvoiceid(@Param("invoiceid") Serializable invoiceid) ;

    List<InvoiceDetail> selectByProductid(@Param("productid") Serializable productid) ;

    List<InvoiceDetail> selectBySalesorderdetailid(@Param("salesorderdetailid") Serializable salesorderdetailid) ;

    List<InvoiceDetail> selectByTransactioncurrencyid(@Param("transactioncurrencyid") Serializable transactioncurrencyid) ;

    List<InvoiceDetail> selectByUomid(@Param("uomid") Serializable uomid) ;

}

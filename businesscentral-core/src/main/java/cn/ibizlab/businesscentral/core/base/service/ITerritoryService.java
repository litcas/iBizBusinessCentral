package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.Territory;
import cn.ibizlab.businesscentral.core.base.filter.TerritorySearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Territory] 服务对象接口
 */
public interface ITerritoryService extends IService<Territory>{

    boolean create(Territory et) ;
    void createBatch(List<Territory> list) ;
    boolean update(Territory et) ;
    void updateBatch(List<Territory> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Territory get(String key) ;
    Territory getDraft(Territory et) ;
    boolean checkKey(Territory et) ;
    boolean save(Territory et) ;
    void saveBatch(List<Territory> list) ;
    Page<Territory> searchDefault(TerritorySearchContext context) ;
    List<Territory> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Territory> getTerritoryByIds(List<String> ids) ;
    List<Territory> getTerritoryByEntities(List<Territory> entities) ;
}



package cn.ibizlab.businesscentral.core.service.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.service.domain.Contract;
/**
 * 关系型数据实体[Contract] 查询条件对象
 */
@Slf4j
@Data
public class ContractSearchContext extends QueryWrapperContext<Contract> {

	private String n_billingfrequencycode_eq;//[记帐频率]
	public void setN_billingfrequencycode_eq(String n_billingfrequencycode_eq) {
        this.n_billingfrequencycode_eq = n_billingfrequencycode_eq;
        if(!ObjectUtils.isEmpty(this.n_billingfrequencycode_eq)){
            this.getSearchCond().eq("billingfrequencycode", n_billingfrequencycode_eq);
        }
    }
	private Integer n_statuscode_eq;//[状态描述]
	public void setN_statuscode_eq(Integer n_statuscode_eq) {
        this.n_statuscode_eq = n_statuscode_eq;
        if(!ObjectUtils.isEmpty(this.n_statuscode_eq)){
            this.getSearchCond().eq("statuscode", n_statuscode_eq);
        }
    }
	private String n_contractservicelevelcode_eq;//[服务级别]
	public void setN_contractservicelevelcode_eq(String n_contractservicelevelcode_eq) {
        this.n_contractservicelevelcode_eq = n_contractservicelevelcode_eq;
        if(!ObjectUtils.isEmpty(this.n_contractservicelevelcode_eq)){
            this.getSearchCond().eq("contractservicelevelcode", n_contractservicelevelcode_eq);
        }
    }
	private String n_title_like;//[合同名称]
	public void setN_title_like(String n_title_like) {
        this.n_title_like = n_title_like;
        if(!ObjectUtils.isEmpty(this.n_title_like)){
            this.getSearchCond().like("title", n_title_like);
        }
    }
	private Integer n_statecode_eq;//[状态]
	public void setN_statecode_eq(Integer n_statecode_eq) {
        this.n_statecode_eq = n_statecode_eq;
        if(!ObjectUtils.isEmpty(this.n_statecode_eq)){
            this.getSearchCond().eq("statecode", n_statecode_eq);
        }
    }
	private String n_allotmenttypecode_eq;//[服务配额类型]
	public void setN_allotmenttypecode_eq(String n_allotmenttypecode_eq) {
        this.n_allotmenttypecode_eq = n_allotmenttypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_allotmenttypecode_eq)){
            this.getSearchCond().eq("allotmenttypecode", n_allotmenttypecode_eq);
        }
    }
	private String n_billtoaddress_eq;//[帐单寄往地址]
	public void setN_billtoaddress_eq(String n_billtoaddress_eq) {
        this.n_billtoaddress_eq = n_billtoaddress_eq;
        if(!ObjectUtils.isEmpty(this.n_billtoaddress_eq)){
            this.getSearchCond().eq("billtoaddress", n_billtoaddress_eq);
        }
    }
	private String n_contracttemplateid_eq;//[合同模板]
	public void setN_contracttemplateid_eq(String n_contracttemplateid_eq) {
        this.n_contracttemplateid_eq = n_contracttemplateid_eq;
        if(!ObjectUtils.isEmpty(this.n_contracttemplateid_eq)){
            this.getSearchCond().eq("contracttemplateid", n_contracttemplateid_eq);
        }
    }
	private String n_serviceaddress_eq;//[合同地址]
	public void setN_serviceaddress_eq(String n_serviceaddress_eq) {
        this.n_serviceaddress_eq = n_serviceaddress_eq;
        if(!ObjectUtils.isEmpty(this.n_serviceaddress_eq)){
            this.getSearchCond().eq("serviceaddress", n_serviceaddress_eq);
        }
    }
	private String n_originatingcontract_eq;//[原始合同]
	public void setN_originatingcontract_eq(String n_originatingcontract_eq) {
        this.n_originatingcontract_eq = n_originatingcontract_eq;
        if(!ObjectUtils.isEmpty(this.n_originatingcontract_eq)){
            this.getSearchCond().eq("originatingcontract", n_originatingcontract_eq);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("title", query)   
            );
		 }
	}
}




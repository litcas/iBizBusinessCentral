package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.Calendar;
import cn.ibizlab.businesscentral.core.base.filter.CalendarSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Calendar] 服务对象接口
 */
public interface ICalendarService extends IService<Calendar>{

    boolean create(Calendar et) ;
    void createBatch(List<Calendar> list) ;
    boolean update(Calendar et) ;
    void updateBatch(List<Calendar> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Calendar get(String key) ;
    Calendar getDraft(Calendar et) ;
    boolean checkKey(Calendar et) ;
    boolean save(Calendar et) ;
    void saveBatch(List<Calendar> list) ;
    Page<Calendar> searchDefault(CalendarSearchContext context) ;
    List<Calendar> selectByBusinessunitid(String businessunitid) ;
    void removeByBusinessunitid(String businessunitid) ;
    List<Calendar> selectByHolidayschedulecalendarid(String calendarid) ;
    void removeByHolidayschedulecalendarid(String calendarid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Calendar> getCalendarByIds(List<String> ids) ;
    List<Calendar> getCalendarByEntities(List<Calendar> entities) ;
}



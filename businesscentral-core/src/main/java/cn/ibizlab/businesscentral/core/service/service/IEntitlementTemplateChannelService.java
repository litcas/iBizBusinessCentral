package cn.ibizlab.businesscentral.core.service.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.service.domain.EntitlementTemplateChannel;
import cn.ibizlab.businesscentral.core.service.filter.EntitlementTemplateChannelSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EntitlementTemplateChannel] 服务对象接口
 */
public interface IEntitlementTemplateChannelService extends IService<EntitlementTemplateChannel>{

    boolean create(EntitlementTemplateChannel et) ;
    void createBatch(List<EntitlementTemplateChannel> list) ;
    boolean update(EntitlementTemplateChannel et) ;
    void updateBatch(List<EntitlementTemplateChannel> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    EntitlementTemplateChannel get(String key) ;
    EntitlementTemplateChannel getDraft(EntitlementTemplateChannel et) ;
    boolean checkKey(EntitlementTemplateChannel et) ;
    boolean save(EntitlementTemplateChannel et) ;
    void saveBatch(List<EntitlementTemplateChannel> list) ;
    Page<EntitlementTemplateChannel> searchDefault(EntitlementTemplateChannelSearchContext context) ;
    List<EntitlementTemplateChannel> selectByEntitlementtemplateid(String entitlementtemplateid) ;
    void removeByEntitlementtemplateid(String entitlementtemplateid) ;
    List<EntitlementTemplateChannel> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EntitlementTemplateChannel> getEntitlementtemplatechannelByIds(List<String> ids) ;
    List<EntitlementTemplateChannel> getEntitlementtemplatechannelByEntities(List<EntitlementTemplateChannel> entities) ;
}



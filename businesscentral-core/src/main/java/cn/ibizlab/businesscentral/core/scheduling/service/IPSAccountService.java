package cn.ibizlab.businesscentral.core.scheduling.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.scheduling.domain.PSAccount;
import cn.ibizlab.businesscentral.core.scheduling.filter.PSAccountSearchContext;


/**
 * 实体[PSAccount] 服务对象接口
 */
public interface IPSAccountService{

    boolean create(PSAccount et) ;
    void createBatch(List<PSAccount> list) ;
    boolean update(PSAccount et) ;
    void updateBatch(List<PSAccount> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    PSAccount get(String key) ;
    PSAccount getDraft(PSAccount et) ;
    boolean checkKey(PSAccount et) ;
    boolean save(PSAccount et) ;
    void saveBatch(List<PSAccount> list) ;
    Page<PSAccount> searchDefault(PSAccountSearchContext context) ;

}




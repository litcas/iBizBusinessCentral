package cn.ibizlab.businesscentral.core.base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.base.domain.Contact;
import cn.ibizlab.businesscentral.core.base.filter.ContactSearchContext;
import cn.ibizlab.businesscentral.core.base.service.IContactService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.base.mapper.ContactMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[联系人] 服务对象接口实现
 */
@Slf4j
@Service("ContactServiceImpl")
public class ContactServiceImpl extends ServiceImpl<ContactMapper, Contact> implements IContactService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IAccountService accountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.scheduling.service.IBookableResourceService bookableresourceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IFeedbackService feedbackService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IIncidentService incidentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.ILeadService leadService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.marketing.service.IListContactService listcontactService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IOpportunityService opportunityService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IEquipmentService equipmentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.product.service.IPriceLevelService pricelevelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IIBizServiceService ibizserviceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ISlaService slaService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.logic.IContactActiveLogic activeLogic;

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.logic.IContactInactiveLogic inactiveLogic;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(Contact et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getContactid()),et);
        return true;
    }

    @Override
    public void createBatch(List<Contact> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Contact et) {
        fillParentData(et);
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("contactid",et.getContactid())))
            return false;
        CachedBeanCopier.copy(get(et.getContactid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<Contact> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Contact get(String key) {
        Contact et = getById(key);
        if(et==null){
            et=new Contact();
            et.setContactid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Contact getDraft(Contact et) {
        fillParentData(et);
        return et;
    }

    @Override
    @Transactional
    public Contact active(Contact et) {
        activeLogic.execute(et);
         return et ;
    }

    @Override
    @Transactional
    public Contact addList(Contact et) {
        //自定义代码
        return et;
    }

    @Override
    public boolean checkKey(Contact et) {
        return (!ObjectUtils.isEmpty(et.getContactid()))&&(!Objects.isNull(this.getById(et.getContactid())));
    }
    @Override
    @Transactional
    public Contact inactive(Contact et) {
        inactiveLogic.execute(et);
         return et ;
    }

    @Override
    @Transactional
    public boolean save(Contact et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Contact et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<Contact> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<Contact> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }

    @Override
    @GlobalTransactional
    public Contact setPrimary(Contact et) {
        //自定义代码
        return et;
    }


	@Override
    public List<Contact> selectByCustomerid(String accountid) {
        return baseMapper.selectByCustomerid(accountid);
    }

    @Override
    public void removeByCustomerid(String accountid) {
        this.remove(new QueryWrapper<Contact>().eq("customerid",accountid));
    }

	@Override
    public List<Contact> selectByPreferredequipmentid(String equipmentid) {
        return baseMapper.selectByPreferredequipmentid(equipmentid);
    }

    @Override
    public void removeByPreferredequipmentid(String equipmentid) {
        this.remove(new QueryWrapper<Contact>().eq("preferredequipmentid",equipmentid));
    }

	@Override
    public List<Contact> selectByOriginatingleadid(String leadid) {
        return baseMapper.selectByOriginatingleadid(leadid);
    }

    @Override
    public void removeByOriginatingleadid(String leadid) {
        this.remove(new QueryWrapper<Contact>().eq("originatingleadid",leadid));
    }

	@Override
    public List<Contact> selectByDefaultpricelevelid(String pricelevelid) {
        return baseMapper.selectByDefaultpricelevelid(pricelevelid);
    }

    @Override
    public void removeByDefaultpricelevelid(String pricelevelid) {
        this.remove(new QueryWrapper<Contact>().eq("defaultpricelevelid",pricelevelid));
    }

	@Override
    public List<Contact> selectByPreferredserviceid(String serviceid) {
        return baseMapper.selectByPreferredserviceid(serviceid);
    }

    @Override
    public void removeByPreferredserviceid(String serviceid) {
        this.remove(new QueryWrapper<Contact>().eq("preferredserviceid",serviceid));
    }

	@Override
    public List<Contact> selectBySlaid(String slaid) {
        return baseMapper.selectBySlaid(slaid);
    }

    @Override
    public void removeBySlaid(String slaid) {
        this.remove(new QueryWrapper<Contact>().eq("slaid",slaid));
    }

	@Override
    public List<Contact> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<Contact>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<Contact> searchDefault(ContactSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Contact> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Contact>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 停用联系人
     */
    @Override
    public Page<Contact> searchStop(ContactSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Contact> pages=baseMapper.searchStop(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Contact>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 可用联系人
     */
    @Override
    public Page<Contact> searchUsable(ContactSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Contact> pages=baseMapper.searchUsable(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Contact>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Contact et){
        //实体关系[DER1N_CONTACT_ACCOUNT_CUSTOMERID]
        if(!ObjectUtils.isEmpty(et.getCustomerid())){
            cn.ibizlab.businesscentral.core.base.domain.Account customer=et.getCustomer();
            if(ObjectUtils.isEmpty(customer)){
                cn.ibizlab.businesscentral.core.base.domain.Account majorEntity=accountService.get(et.getCustomerid());
                et.setCustomer(majorEntity);
                customer=majorEntity;
            }
            et.setCustomername(customer.getAccountname());
        }
        //实体关系[DER1N_CONTACT__EQUIPMENT__PREFERREDEQUIPMENTID]
        if(!ObjectUtils.isEmpty(et.getPreferredequipmentid())){
            cn.ibizlab.businesscentral.core.service.domain.Equipment preferredequipment=et.getPreferredequipment();
            if(ObjectUtils.isEmpty(preferredequipment)){
                cn.ibizlab.businesscentral.core.service.domain.Equipment majorEntity=equipmentService.get(et.getPreferredequipmentid());
                et.setPreferredequipment(majorEntity);
                preferredequipment=majorEntity;
            }
            et.setPreferredequipmentname(preferredequipment.getEquipmentname());
        }
        //实体关系[DER1N_CONTACT__LEAD__ORIGINATINGLEADID]
        if(!ObjectUtils.isEmpty(et.getOriginatingleadid())){
            cn.ibizlab.businesscentral.core.sales.domain.Lead originatinglead=et.getOriginatinglead();
            if(ObjectUtils.isEmpty(originatinglead)){
                cn.ibizlab.businesscentral.core.sales.domain.Lead majorEntity=leadService.get(et.getOriginatingleadid());
                et.setOriginatinglead(majorEntity);
                originatinglead=majorEntity;
            }
            et.setOriginatingleadname(originatinglead.getFullname());
        }
        //实体关系[DER1N_CONTACT__PRICELEVEL__DEFAULTPRICELEVELID]
        if(!ObjectUtils.isEmpty(et.getDefaultpricelevelid())){
            cn.ibizlab.businesscentral.core.product.domain.PriceLevel defaultpricelevel=et.getDefaultpricelevel();
            if(ObjectUtils.isEmpty(defaultpricelevel)){
                cn.ibizlab.businesscentral.core.product.domain.PriceLevel majorEntity=pricelevelService.get(et.getDefaultpricelevelid());
                et.setDefaultpricelevel(majorEntity);
                defaultpricelevel=majorEntity;
            }
            et.setDefaultpricelevelname(defaultpricelevel.getPricelevelname());
        }
        //实体关系[DER1N_CONTACT__SERVICE__PREFERREDSERVICEID]
        if(!ObjectUtils.isEmpty(et.getPreferredserviceid())){
            cn.ibizlab.businesscentral.core.service.domain.IBizService preferredservice=et.getPreferredservice();
            if(ObjectUtils.isEmpty(preferredservice)){
                cn.ibizlab.businesscentral.core.service.domain.IBizService majorEntity=ibizserviceService.get(et.getPreferredserviceid());
                et.setPreferredservice(majorEntity);
                preferredservice=majorEntity;
            }
            et.setPreferredservicename(preferredservice.getServicename());
        }
        //实体关系[DER1N_CONTACT__SLA__SLAID]
        if(!ObjectUtils.isEmpty(et.getSlaid())){
            cn.ibizlab.businesscentral.core.base.domain.Sla sla=et.getSla();
            if(ObjectUtils.isEmpty(sla)){
                cn.ibizlab.businesscentral.core.base.domain.Sla majorEntity=slaService.get(et.getSlaid());
                et.setSla(majorEntity);
                sla=majorEntity;
            }
            et.setSlaname(sla.getSlaname());
        }
        //实体关系[DER1N_CONTACT__TRANSACTIONCURRENCY__TRANSACTIONCURRENCYID]
        if(!ObjectUtils.isEmpty(et.getTransactioncurrencyid())){
            cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency=et.getTransactioncurrency();
            if(ObjectUtils.isEmpty(transactioncurrency)){
                cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency majorEntity=transactioncurrencyService.get(et.getTransactioncurrencyid());
                et.setTransactioncurrency(majorEntity);
                transactioncurrency=majorEntity;
            }
            et.setCurrencyname(transactioncurrency.getCurrencyname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Contact> getContactByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Contact> getContactByEntities(List<Contact> entities) {
        List ids =new ArrayList();
        for(Contact entity : entities){
            Serializable id=entity.getContactid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




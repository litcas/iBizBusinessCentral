package cn.ibizlab.businesscentral.core.sales.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.sales.domain.Competitor;
import cn.ibizlab.businesscentral.core.sales.filter.CompetitorSearchContext;
import cn.ibizlab.businesscentral.core.sales.service.ICompetitorService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.sales.mapper.CompetitorMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[竞争对手] 服务对象接口实现
 */
@Slf4j
@Service("CompetitorServiceImpl")
public class CompetitorServiceImpl extends ServiceImpl<CompetitorMapper, Competitor> implements ICompetitorService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.ICompetitorProductService competitorproductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.ICompetitorSalesLiteratureService competitorsalesliteratureService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.ILeadCompetitorService leadcompetitorService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IOpportunityCloseService opportunitycloseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IOpportunityCompetitorService opportunitycompetitorService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(Competitor et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getCompetitorid()),et);
        return true;
    }

    @Override
    public void createBatch(List<Competitor> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Competitor et) {
        fillParentData(et);
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("competitorid",et.getCompetitorid())))
            return false;
        CachedBeanCopier.copy(get(et.getCompetitorid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<Competitor> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Competitor get(String key) {
        Competitor et = getById(key);
        if(et==null){
            et=new Competitor();
            et.setCompetitorid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Competitor getDraft(Competitor et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Competitor et) {
        return (!ObjectUtils.isEmpty(et.getCompetitorid()))&&(!Objects.isNull(this.getById(et.getCompetitorid())));
    }
    @Override
    @Transactional
    public boolean save(Competitor et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Competitor et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<Competitor> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<Competitor> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Competitor> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<Competitor>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<Competitor> searchDefault(CompetitorSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Competitor> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Competitor>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Competitor et){
        //实体关系[DER1N_COMPETITOR__TRANSACTIONCURRENCY__TRANSACTIONCURRENCYID]
        if(!ObjectUtils.isEmpty(et.getTransactioncurrencyid())){
            cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency=et.getTransactioncurrency();
            if(ObjectUtils.isEmpty(transactioncurrency)){
                cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency majorEntity=transactioncurrencyService.get(et.getTransactioncurrencyid());
                et.setTransactioncurrency(majorEntity);
                transactioncurrency=majorEntity;
            }
            et.setCurrencyname(transactioncurrency.getCurrencyname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Competitor> getCompetitorByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Competitor> getCompetitorByEntities(List<Competitor> entities) {
        List ids =new ArrayList();
        for(Competitor entity : entities){
            Serializable id=entity.getCompetitorid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




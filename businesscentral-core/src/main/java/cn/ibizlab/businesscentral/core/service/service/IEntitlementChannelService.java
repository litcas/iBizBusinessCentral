package cn.ibizlab.businesscentral.core.service.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.service.domain.EntitlementChannel;
import cn.ibizlab.businesscentral.core.service.filter.EntitlementChannelSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EntitlementChannel] 服务对象接口
 */
public interface IEntitlementChannelService extends IService<EntitlementChannel>{

    boolean create(EntitlementChannel et) ;
    void createBatch(List<EntitlementChannel> list) ;
    boolean update(EntitlementChannel et) ;
    void updateBatch(List<EntitlementChannel> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    EntitlementChannel get(String key) ;
    EntitlementChannel getDraft(EntitlementChannel et) ;
    boolean checkKey(EntitlementChannel et) ;
    boolean save(EntitlementChannel et) ;
    void saveBatch(List<EntitlementChannel> list) ;
    Page<EntitlementChannel> searchDefault(EntitlementChannelSearchContext context) ;
    List<EntitlementChannel> selectByEntitlementid(String entitlementid) ;
    void removeByEntitlementid(String entitlementid) ;
    List<EntitlementChannel> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EntitlementChannel> getEntitlementchannelByIds(List<String> ids) ;
    List<EntitlementChannel> getEntitlementchannelByEntities(List<EntitlementChannel> entities) ;
}



package cn.ibizlab.businesscentral.core.runtime.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.runtime.domain.BulkOperationLog;
import cn.ibizlab.businesscentral.core.runtime.filter.BulkOperationLogSearchContext;
import cn.ibizlab.businesscentral.core.runtime.service.IBulkOperationLogService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.runtime.mapper.BulkOperationLogMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[批量操作日志] 服务对象接口实现
 */
@Slf4j
@Service("BulkOperationLogServiceImpl")
public class BulkOperationLogServiceImpl extends ServiceImpl<BulkOperationLogMapper, BulkOperationLog> implements IBulkOperationLogService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IActivityPointerService activitypointerService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(BulkOperationLog et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getBulkoperationlogid()),et);
        return true;
    }

    @Override
    public void createBatch(List<BulkOperationLog> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(BulkOperationLog et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("bulkoperationlogid",et.getBulkoperationlogid())))
            return false;
        CachedBeanCopier.copy(get(et.getBulkoperationlogid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<BulkOperationLog> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public BulkOperationLog get(String key) {
        BulkOperationLog et = getById(key);
        if(et==null){
            et=new BulkOperationLog();
            et.setBulkoperationlogid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public BulkOperationLog getDraft(BulkOperationLog et) {
        return et;
    }

    @Override
    public boolean checkKey(BulkOperationLog et) {
        return (!ObjectUtils.isEmpty(et.getBulkoperationlogid()))&&(!Objects.isNull(this.getById(et.getBulkoperationlogid())));
    }
    @Override
    @Transactional
    public boolean save(BulkOperationLog et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(BulkOperationLog et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<BulkOperationLog> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<BulkOperationLog> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<BulkOperationLog> selectByBulkoperationid(String activityid) {
        return baseMapper.selectByBulkoperationid(activityid);
    }

    @Override
    public void removeByBulkoperationid(String activityid) {
        this.remove(new QueryWrapper<BulkOperationLog>().eq("bulkoperationid",activityid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<BulkOperationLog> searchDefault(BulkOperationLogSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<BulkOperationLog> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<BulkOperationLog>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<BulkOperationLog> getBulkoperationlogByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<BulkOperationLog> getBulkoperationlogByEntities(List<BulkOperationLog> entities) {
        List ids =new ArrayList();
        for(BulkOperationLog entity : entities){
            Serializable id=entity.getBulkoperationlogid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




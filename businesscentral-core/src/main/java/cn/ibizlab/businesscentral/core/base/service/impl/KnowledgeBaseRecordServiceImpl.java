package cn.ibizlab.businesscentral.core.base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.base.domain.KnowledgeBaseRecord;
import cn.ibizlab.businesscentral.core.base.filter.KnowledgeBaseRecordSearchContext;
import cn.ibizlab.businesscentral.core.base.service.IKnowledgeBaseRecordService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.base.mapper.KnowledgeBaseRecordMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[知识库记录] 服务对象接口实现
 */
@Slf4j
@Service("KnowledgeBaseRecordServiceImpl")
public class KnowledgeBaseRecordServiceImpl extends ServiceImpl<KnowledgeBaseRecordMapper, KnowledgeBaseRecord> implements IKnowledgeBaseRecordService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(KnowledgeBaseRecord et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getKnowledgebaserecordid()),et);
        return true;
    }

    @Override
    public void createBatch(List<KnowledgeBaseRecord> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(KnowledgeBaseRecord et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("knowledgebaserecordid",et.getKnowledgebaserecordid())))
            return false;
        CachedBeanCopier.copy(get(et.getKnowledgebaserecordid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<KnowledgeBaseRecord> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public KnowledgeBaseRecord get(String key) {
        KnowledgeBaseRecord et = getById(key);
        if(et==null){
            et=new KnowledgeBaseRecord();
            et.setKnowledgebaserecordid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public KnowledgeBaseRecord getDraft(KnowledgeBaseRecord et) {
        return et;
    }

    @Override
    public boolean checkKey(KnowledgeBaseRecord et) {
        return (!ObjectUtils.isEmpty(et.getKnowledgebaserecordid()))&&(!Objects.isNull(this.getById(et.getKnowledgebaserecordid())));
    }
    @Override
    @Transactional
    public boolean save(KnowledgeBaseRecord et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(KnowledgeBaseRecord et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<KnowledgeBaseRecord> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<KnowledgeBaseRecord> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<KnowledgeBaseRecord> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<KnowledgeBaseRecord>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<KnowledgeBaseRecord> searchDefault(KnowledgeBaseRecordSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<KnowledgeBaseRecord> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<KnowledgeBaseRecord>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<KnowledgeBaseRecord> getKnowledgebaserecordByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<KnowledgeBaseRecord> getKnowledgebaserecordByEntities(List<KnowledgeBaseRecord> entities) {
        List ids =new ArrayList();
        for(KnowledgeBaseRecord entity : entities){
            Serializable id=entity.getKnowledgebaserecordid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.Contact;
import cn.ibizlab.businesscentral.core.base.filter.ContactSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Contact] 服务对象接口
 */
public interface IContactService extends IService<Contact>{

    boolean create(Contact et) ;
    void createBatch(List<Contact> list) ;
    boolean update(Contact et) ;
    void updateBatch(List<Contact> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Contact get(String key) ;
    Contact getDraft(Contact et) ;
    Contact active(Contact et) ;
    Contact addList(Contact et) ;
    boolean checkKey(Contact et) ;
    Contact inactive(Contact et) ;
    boolean save(Contact et) ;
    void saveBatch(List<Contact> list) ;
    Contact setPrimary(Contact et) ;
    Page<Contact> searchDefault(ContactSearchContext context) ;
    Page<Contact> searchStop(ContactSearchContext context) ;
    Page<Contact> searchUsable(ContactSearchContext context) ;
    List<Contact> selectByCustomerid(String accountid) ;
    void removeByCustomerid(String accountid) ;
    List<Contact> selectByPreferredequipmentid(String equipmentid) ;
    void removeByPreferredequipmentid(String equipmentid) ;
    List<Contact> selectByOriginatingleadid(String leadid) ;
    void removeByOriginatingleadid(String leadid) ;
    List<Contact> selectByDefaultpricelevelid(String pricelevelid) ;
    void removeByDefaultpricelevelid(String pricelevelid) ;
    List<Contact> selectByPreferredserviceid(String serviceid) ;
    void removeByPreferredserviceid(String serviceid) ;
    List<Contact> selectBySlaid(String slaid) ;
    void removeBySlaid(String slaid) ;
    List<Contact> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Contact> getContactByIds(List<String> ids) ;
    List<Contact> getContactByEntities(List<Contact> entities) ;
}



package cn.ibizlab.businesscentral.core.service.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[权利模板]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "ENTITLEMENTTEMPLATE",resultMap = "EntitlementTemplateResultMap")
public class EntitlementTemplate extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * KB 访问级别
     */
    @TableField(value = "kbaccesslevel")
    @JSONField(name = "kbaccesslevel")
    @JsonProperty("kbaccesslevel")
    private String kbaccesslevel;
    /**
     * 权利模板
     */
    @DEField(isKeyField=true)
    @TableId(value= "entitlementtemplateid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "entitlementtemplateid")
    @JsonProperty("entitlementtemplateid")
    private String entitlementtemplateid;
    /**
     * Version Number
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 开始日期
     */
    @TableField(value = "startdate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "startdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("startdate")
    private Timestamp startdate;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 分配类型
     */
    @TableField(value = "allocationtypecode")
    @JSONField(name = "allocationtypecode")
    @JsonProperty("allocationtypecode")
    private String allocationtypecode;
    /**
     * 缩短剩余期限
     */
    @TableField(value = "decreaseremainingon")
    @JSONField(name = "decreaseremainingon")
    @JsonProperty("decreaseremainingon")
    private String decreaseremainingon;
    /**
     * UTC Conversion Time Zone Code
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 授权模板名称
     */
    @TableField(value = "entitlementtemplatename")
    @JSONField(name = "entitlementtemplatename")
    @JsonProperty("entitlementtemplatename")
    private String entitlementtemplatename;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 总期数
     */
    @TableField(value = "totalterms")
    @JSONField(name = "totalterms")
    @JsonProperty("totalterms")
    private BigDecimal totalterms;
    /**
     * 根据权利期限进行限制
     */
    @DEField(defaultValue = "0")
    @TableField(value = "restrictcasecreation")
    @JSONField(name = "restrictcasecreation")
    @JsonProperty("restrictcasecreation")
    private Integer restrictcasecreation;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * Time Zone Rule Version Number
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * entitytype
     */
    @TableField(value = "entitytype")
    @JSONField(name = "entitytype")
    @JsonProperty("entitytype")
    private String entitytype;
    /**
     * 汇率
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * Record Created On
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 结束日期
     */
    @TableField(value = "enddate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "enddate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("enddate")
    private Timestamp enddate;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * Import Sequence Number
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * SLA
     */
    @TableField(value = "slaid")
    @JSONField(name = "slaid")
    @JsonProperty("slaid")
    private String slaid;
    /**
     * 货币
     */
    @TableField(value = "transactioncurrencyid")
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Sla sla;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency;



    /**
     * 设置 [KB 访问级别]
     */
    public void setKbaccesslevel(String kbaccesslevel){
        this.kbaccesslevel = kbaccesslevel ;
        this.modify("kbaccesslevel",kbaccesslevel);
    }

    /**
     * 设置 [Version Number]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [开始日期]
     */
    public void setStartdate(Timestamp startdate){
        this.startdate = startdate ;
        this.modify("startdate",startdate);
    }

    /**
     * 格式化日期 [开始日期]
     */
    public String formatStartdate(){
        if (this.startdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(startdate);
    }
    /**
     * 设置 [分配类型]
     */
    public void setAllocationtypecode(String allocationtypecode){
        this.allocationtypecode = allocationtypecode ;
        this.modify("allocationtypecode",allocationtypecode);
    }

    /**
     * 设置 [缩短剩余期限]
     */
    public void setDecreaseremainingon(String decreaseremainingon){
        this.decreaseremainingon = decreaseremainingon ;
        this.modify("decreaseremainingon",decreaseremainingon);
    }

    /**
     * 设置 [UTC Conversion Time Zone Code]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [授权模板名称]
     */
    public void setEntitlementtemplatename(String entitlementtemplatename){
        this.entitlementtemplatename = entitlementtemplatename ;
        this.modify("entitlementtemplatename",entitlementtemplatename);
    }

    /**
     * 设置 [总期数]
     */
    public void setTotalterms(BigDecimal totalterms){
        this.totalterms = totalterms ;
        this.modify("totalterms",totalterms);
    }

    /**
     * 设置 [根据权利期限进行限制]
     */
    public void setRestrictcasecreation(Integer restrictcasecreation){
        this.restrictcasecreation = restrictcasecreation ;
        this.modify("restrictcasecreation",restrictcasecreation);
    }

    /**
     * 设置 [Time Zone Rule Version Number]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [entitytype]
     */
    public void setEntitytype(String entitytype){
        this.entitytype = entitytype ;
        this.modify("entitytype",entitytype);
    }

    /**
     * 设置 [汇率]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [Record Created On]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [Record Created On]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [结束日期]
     */
    public void setEnddate(Timestamp enddate){
        this.enddate = enddate ;
        this.modify("enddate",enddate);
    }

    /**
     * 格式化日期 [结束日期]
     */
    public String formatEnddate(){
        if (this.enddate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(enddate);
    }
    /**
     * 设置 [Import Sequence Number]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [SLA]
     */
    public void setSlaid(String slaid){
        this.slaid = slaid ;
        this.modify("slaid",slaid);
    }

    /**
     * 设置 [货币]
     */
    public void setTransactioncurrencyid(String transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }


}



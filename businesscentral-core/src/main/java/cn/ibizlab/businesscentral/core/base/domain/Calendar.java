package cn.ibizlab.businesscentral.core.base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[日历]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "CALENDAR",resultMap = "CalendarResultMap")
public class Calendar extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 日历名称
     */
    @TableField(value = "calendarname")
    @JSONField(name = "calendarname")
    @JsonProperty("calendarname")
    private String calendarname;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 日历
     */
    @DEField(isKeyField=true)
    @TableId(value= "calendarid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "calendarid")
    @JsonProperty("calendarid")
    private String calendarid;
    /**
     * VersionNumber
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 是共享
     */
    @DEField(defaultValue = "0")
    @TableField(value = "shared")
    @JSONField(name = "shared")
    @JsonProperty("shared")
    private Integer shared;
    /**
     * 主要用户
     */
    @TableField(value = "primaryuserid")
    @JSONField(name = "primaryuserid")
    @JsonProperty("primaryuserid")
    private String primaryuserid;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 日历类型
     */
    @TableField(value = "type")
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 节日时间表 CalendarId
     */
    @TableField(value = "holidayschedulecalendarid")
    @JSONField(name = "holidayschedulecalendarid")
    @JsonProperty("holidayschedulecalendarid")
    private String holidayschedulecalendarid;
    /**
     * 业务部门
     */
    @TableField(value = "businessunitid")
    @JSONField(name = "businessunitid")
    @JsonProperty("businessunitid")
    private String businessunitid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.BusinessUnit businessunit;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Calendar holidayschedulecalendar;



    /**
     * 设置 [日历名称]
     */
    public void setCalendarname(String calendarname){
        this.calendarname = calendarname ;
        this.modify("calendarname",calendarname);
    }

    /**
     * 设置 [VersionNumber]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [是共享]
     */
    public void setShared(Integer shared){
        this.shared = shared ;
        this.modify("shared",shared);
    }

    /**
     * 设置 [主要用户]
     */
    public void setPrimaryuserid(String primaryuserid){
        this.primaryuserid = primaryuserid ;
        this.modify("primaryuserid",primaryuserid);
    }

    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [日历类型]
     */
    public void setType(String type){
        this.type = type ;
        this.modify("type",type);
    }

    /**
     * 设置 [节日时间表 CalendarId]
     */
    public void setHolidayschedulecalendarid(String holidayschedulecalendarid){
        this.holidayschedulecalendarid = holidayschedulecalendarid ;
        this.modify("holidayschedulecalendarid",holidayschedulecalendarid);
    }

    /**
     * 设置 [业务部门]
     */
    public void setBusinessunitid(String businessunitid){
        this.businessunitid = businessunitid ;
        this.modify("businessunitid",businessunitid);
    }


}



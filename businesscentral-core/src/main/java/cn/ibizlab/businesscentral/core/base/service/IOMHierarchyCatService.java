package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.OMHierarchyCat;
import cn.ibizlab.businesscentral.core.base.filter.OMHierarchyCatSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[OMHierarchyCat] 服务对象接口
 */
public interface IOMHierarchyCatService extends IService<OMHierarchyCat>{

    boolean create(OMHierarchyCat et) ;
    void createBatch(List<OMHierarchyCat> list) ;
    boolean update(OMHierarchyCat et) ;
    void updateBatch(List<OMHierarchyCat> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    OMHierarchyCat get(String key) ;
    OMHierarchyCat getDraft(OMHierarchyCat et) ;
    boolean checkKey(OMHierarchyCat et) ;
    boolean save(OMHierarchyCat et) ;
    void saveBatch(List<OMHierarchyCat> list) ;
    Page<OMHierarchyCat> searchDefault(OMHierarchyCatSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<OMHierarchyCat> getOmhierarchycatByIds(List<String> ids) ;
    List<OMHierarchyCat> getOmhierarchycatByEntities(List<OMHierarchyCat> entities) ;
}



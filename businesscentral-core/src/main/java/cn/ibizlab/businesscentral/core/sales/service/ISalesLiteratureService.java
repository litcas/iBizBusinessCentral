package cn.ibizlab.businesscentral.core.sales.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.sales.domain.SalesLiterature;
import cn.ibizlab.businesscentral.core.sales.filter.SalesLiteratureSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[SalesLiterature] 服务对象接口
 */
public interface ISalesLiteratureService extends IService<SalesLiterature>{

    boolean create(SalesLiterature et) ;
    void createBatch(List<SalesLiterature> list) ;
    boolean update(SalesLiterature et) ;
    void updateBatch(List<SalesLiterature> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    SalesLiterature get(String key) ;
    SalesLiterature getDraft(SalesLiterature et) ;
    boolean checkKey(SalesLiterature et) ;
    boolean save(SalesLiterature et) ;
    void saveBatch(List<SalesLiterature> list) ;
    Page<SalesLiterature> searchDefault(SalesLiteratureSearchContext context) ;
    List<SalesLiterature> selectBySubjectid(String subjectid) ;
    void removeBySubjectid(String subjectid) ;
    List<SalesLiterature> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<SalesLiterature> getSalesliteratureByIds(List<String> ids) ;
    List<SalesLiterature> getSalesliteratureByEntities(List<SalesLiterature> entities) ;
}



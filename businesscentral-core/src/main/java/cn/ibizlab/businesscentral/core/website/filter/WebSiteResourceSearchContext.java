package cn.ibizlab.businesscentral.core.website.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.website.domain.WebSiteResource;
/**
 * 关系型数据实体[WebSiteResource] 查询条件对象
 */
@Slf4j
@Data
public class WebSiteResourceSearchContext extends QueryWrapperContext<WebSiteResource> {

	private String n_websiteresourcename_like;//[网站资源名称]
	public void setN_websiteresourcename_like(String n_websiteresourcename_like) {
        this.n_websiteresourcename_like = n_websiteresourcename_like;
        if(!ObjectUtils.isEmpty(this.n_websiteresourcename_like)){
            this.getSearchCond().like("websiteresourcename", n_websiteresourcename_like);
        }
    }
	private String n_websitename_eq;//[站点]
	public void setN_websitename_eq(String n_websitename_eq) {
        this.n_websitename_eq = n_websitename_eq;
        if(!ObjectUtils.isEmpty(this.n_websitename_eq)){
            this.getSearchCond().eq("websitename", n_websitename_eq);
        }
    }
	private String n_websitename_like;//[站点]
	public void setN_websitename_like(String n_websitename_like) {
        this.n_websitename_like = n_websitename_like;
        if(!ObjectUtils.isEmpty(this.n_websitename_like)){
            this.getSearchCond().like("websitename", n_websitename_like);
        }
    }
	private String n_websiteid_eq;//[网站标识]
	public void setN_websiteid_eq(String n_websiteid_eq) {
        this.n_websiteid_eq = n_websiteid_eq;
        if(!ObjectUtils.isEmpty(this.n_websiteid_eq)){
            this.getSearchCond().eq("websiteid", n_websiteid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("websiteresourcename", query)   
            );
		 }
	}
}




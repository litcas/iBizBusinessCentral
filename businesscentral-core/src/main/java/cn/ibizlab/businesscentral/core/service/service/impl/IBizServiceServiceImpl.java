package cn.ibizlab.businesscentral.core.service.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.service.domain.IBizService;
import cn.ibizlab.businesscentral.core.service.filter.IBizServiceSearchContext;
import cn.ibizlab.businesscentral.core.service.service.IIBizServiceService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.service.mapper.IBizServiceMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[服务] 服务对象接口实现
 */
@Slf4j
@Service("IBizServiceServiceImpl")
public class IBizServiceServiceImpl extends ServiceImpl<IBizServiceMapper, IBizService> implements IIBizServiceService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IAccountService accountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IActivityPointerService activitypointerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IAppointmentService appointmentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IContactService contactService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IEmailService emailService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IFaxService faxService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IIncidentreSolutionService incidentresolutionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ILetterService letterService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IOpportunityCloseService opportunitycloseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IOrderCloseService ordercloseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IPhoneCallService phonecallService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IQuoteCloseService quotecloseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IServiceAppointmentService serviceappointmentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITaskService taskService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IResourceSpecService resourcespecService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(IBizService et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getServiceid()),et);
        return true;
    }

    @Override
    public void createBatch(List<IBizService> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(IBizService et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("serviceid",et.getServiceid())))
            return false;
        CachedBeanCopier.copy(get(et.getServiceid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<IBizService> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public IBizService get(String key) {
        IBizService et = getById(key);
        if(et==null){
            et=new IBizService();
            et.setServiceid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public IBizService getDraft(IBizService et) {
        return et;
    }

    @Override
    public boolean checkKey(IBizService et) {
        return (!ObjectUtils.isEmpty(et.getServiceid()))&&(!Objects.isNull(this.getById(et.getServiceid())));
    }
    @Override
    @Transactional
    public boolean save(IBizService et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(IBizService et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<IBizService> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<IBizService> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<IBizService> selectByResourcespecid(String resourcespecid) {
        return baseMapper.selectByResourcespecid(resourcespecid);
    }

    @Override
    public void removeByResourcespecid(String resourcespecid) {
        this.remove(new QueryWrapper<IBizService>().eq("resourcespecid",resourcespecid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<IBizService> searchDefault(IBizServiceSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<IBizService> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<IBizService>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<IBizService> getIbizserviceByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<IBizService> getIbizserviceByEntities(List<IBizService> entities) {
        List ids =new ArrayList();
        for(IBizService entity : entities){
            Serializable id=entity.getServiceid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




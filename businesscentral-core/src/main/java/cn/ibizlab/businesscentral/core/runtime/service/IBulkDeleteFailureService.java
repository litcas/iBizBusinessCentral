package cn.ibizlab.businesscentral.core.runtime.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.runtime.domain.BulkDeleteFailure;
import cn.ibizlab.businesscentral.core.runtime.filter.BulkDeleteFailureSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[BulkDeleteFailure] 服务对象接口
 */
public interface IBulkDeleteFailureService extends IService<BulkDeleteFailure>{

    boolean create(BulkDeleteFailure et) ;
    void createBatch(List<BulkDeleteFailure> list) ;
    boolean update(BulkDeleteFailure et) ;
    void updateBatch(List<BulkDeleteFailure> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    BulkDeleteFailure get(String key) ;
    BulkDeleteFailure getDraft(BulkDeleteFailure et) ;
    boolean checkKey(BulkDeleteFailure et) ;
    boolean save(BulkDeleteFailure et) ;
    void saveBatch(List<BulkDeleteFailure> list) ;
    Page<BulkDeleteFailure> searchDefault(BulkDeleteFailureSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<BulkDeleteFailure> getBulkdeletefailureByIds(List<String> ids) ;
    List<BulkDeleteFailure> getBulkdeletefailureByEntities(List<BulkDeleteFailure> entities) ;
}



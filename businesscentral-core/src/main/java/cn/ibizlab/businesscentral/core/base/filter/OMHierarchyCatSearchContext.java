package cn.ibizlab.businesscentral.core.base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.base.domain.OMHierarchyCat;
/**
 * 关系型数据实体[OMHierarchyCat] 查询条件对象
 */
@Slf4j
@Data
public class OMHierarchyCatSearchContext extends QueryWrapperContext<OMHierarchyCat> {

	private String n_omhierarchycatname_like;//[ 结构层次类别名称]
	public void setN_omhierarchycatname_like(String n_omhierarchycatname_like) {
        this.n_omhierarchycatname_like = n_omhierarchycatname_like;
        if(!ObjectUtils.isEmpty(this.n_omhierarchycatname_like)){
            this.getSearchCond().like("omhierarchycatname", n_omhierarchycatname_like);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("omhierarchycatname", query)   
            );
		 }
	}
}




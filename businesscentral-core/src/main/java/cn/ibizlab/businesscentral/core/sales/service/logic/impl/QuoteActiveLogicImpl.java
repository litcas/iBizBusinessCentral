package cn.ibizlab.businesscentral.core.sales.service.logic.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;

import cn.ibizlab.businesscentral.core.sales.service.logic.IQuoteActiveLogic;
import cn.ibizlab.businesscentral.core.sales.domain.Quote;

/**
 * 关系型数据实体[Active] 对象
 */
@Slf4j
@Service
public class QuoteActiveLogicImpl implements IQuoteActiveLogic{

    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private cn.ibizlab.businesscentral.core.sales.service.IQuoteService quoteservice;

    public cn.ibizlab.businesscentral.core.sales.service.IQuoteService getQuoteService() {
        return this.quoteservice;
    }


    @Autowired
    private cn.ibizlab.businesscentral.core.sales.service.IQuoteService iBzSysDefaultService;

    public cn.ibizlab.businesscentral.core.sales.service.IQuoteService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    public void execute(Quote et){

          KieSession kieSession = null;
        try{
           kieSession=kieContainer.newKieSession();
           kieSession.insert(et); 
           kieSession.setGlobal("quoteactivedefault",et);
           kieSession.setGlobal("quoteservice",quoteservice);
           kieSession.setGlobal("iBzSysQuoteDefaultService",iBzSysDefaultService);
           kieSession.setGlobal("curuser", cn.ibizlab.businesscentral.util.security.AuthenticationUser.getAuthenticationUser());
           kieSession.startProcess("cn.ibizlab.businesscentral.core.sales.service.logic.quoteactive");

        }catch(Exception e){
            throw new RuntimeException("执行[激活报价单]处理逻辑发生异常"+e);
        }finally {
            if(kieSession!=null)
            kieSession.destroy();
        }
    }

}

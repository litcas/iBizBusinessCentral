package cn.ibizlab.businesscentral.core.marketing.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.marketing.domain.ListLead;
import cn.ibizlab.businesscentral.core.marketing.filter.ListLeadSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[ListLead] 服务对象接口
 */
public interface IListLeadService extends IService<ListLead>{

    boolean create(ListLead et) ;
    void createBatch(List<ListLead> list) ;
    boolean update(ListLead et) ;
    void updateBatch(List<ListLead> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    ListLead get(String key) ;
    ListLead getDraft(ListLead et) ;
    boolean checkKey(ListLead et) ;
    boolean save(ListLead et) ;
    void saveBatch(List<ListLead> list) ;
    Page<ListLead> searchDefault(ListLeadSearchContext context) ;
    List<ListLead> selectByEntity2id(String leadid) ;
    void removeByEntity2id(String leadid) ;
    List<ListLead> selectByEntityid(String listid) ;
    void removeByEntityid(String listid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<ListLead> getListleadByIds(List<String> ids) ;
    List<ListLead> getListleadByEntities(List<ListLead> entities) ;
}





package cn.ibizlab.businesscentral.core.base.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.base.domain.Task;
import cn.ibizlab.businesscentral.core.base.domain.ActivityPointer;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface TaskInheritMapping {

    @Mappings({
        @Mapping(source ="activityid",target = "activityid"),
        @Mapping(source ="subject",target = "subject"),
        @Mapping(target ="focusNull",ignore = true),
    })
    ActivityPointer toActivitypointer(Task task);

    @Mappings({
        @Mapping(source ="activityid" ,target = "activityid"),
        @Mapping(source ="subject" ,target = "subject"),
        @Mapping(target ="focusNull",ignore = true),
    })
    Task toTask(ActivityPointer activitypointer);

    List<ActivityPointer> toActivitypointer(List<Task> task);

    List<Task> toTask(List<ActivityPointer> activitypointer);

}



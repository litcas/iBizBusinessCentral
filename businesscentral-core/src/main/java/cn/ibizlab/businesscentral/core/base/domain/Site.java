package cn.ibizlab.businesscentral.core.base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[场所]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SITE",resultMap = "SiteResultMap")
public class Site extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主要电话
     */
    @DEField(name = "address1_telephone1")
    @TableField(value = "address1_telephone1")
    @JSONField(name = "address1_telephone1")
    @JsonProperty("address1_telephone1")
    private String address1Telephone1;
    /**
     * Version Number
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 时区
     */
    @TableField(value = "timezonecode")
    @JSONField(name = "timezonecode")
    @JsonProperty("timezonecode")
    private Integer timezonecode;
    /**
     * 地址 1: 电话 3
     */
    @DEField(name = "address1_telephone3")
    @TableField(value = "address1_telephone3")
    @JSONField(name = "address1_telephone3")
    @JsonProperty("address1_telephone3")
    private String address1Telephone3;
    /**
     * 地址 1: 经度
     */
    @DEField(name = "address1_longitude")
    @TableField(value = "address1_longitude")
    @JSONField(name = "address1_longitude")
    @JsonProperty("address1_longitude")
    private Double address1Longitude;
    /**
     * 地址 1: ID
     */
    @DEField(name = "address1_addressid")
    @TableField(value = "address1_addressid")
    @JSONField(name = "address1_addressid")
    @JsonProperty("address1_addressid")
    private String address1Addressid;
    /**
     * 站点名称
     */
    @TableField(value = "sitename")
    @JSONField(name = "sitename")
    @JsonProperty("sitename")
    private String sitename;
    /**
     * 地址 2: 传真
     */
    @DEField(name = "address2_fax")
    @TableField(value = "address2_fax")
    @JSONField(name = "address2_fax")
    @JsonProperty("address2_fax")
    private String address2Fax;
    /**
     * 地址 2: 电话 2
     */
    @DEField(name = "address2_telephone2")
    @TableField(value = "address2_telephone2")
    @JSONField(name = "address2_telephone2")
    @JsonProperty("address2_telephone2")
    private String address2Telephone2;
    /**
     * 地址 1: UTC 时差
     */
    @DEField(name = "address1_utcoffset")
    @TableField(value = "address1_utcoffset")
    @JSONField(name = "address1_utcoffset")
    @JsonProperty("address1_utcoffset")
    private Integer address1Utcoffset;
    /**
     * 电子邮件
     */
    @TableField(value = "emailaddress")
    @JSONField(name = "emailaddress")
    @JsonProperty("emailaddress")
    private String emailaddress;
    /**
     * 市/县
     */
    @DEField(name = "address1_city")
    @TableField(value = "address1_city")
    @JSONField(name = "address1_city")
    @JsonProperty("address1_city")
    private String address1City;
    /**
     * 地址 2: ID
     */
    @DEField(name = "address2_addressid")
    @TableField(value = "address2_addressid")
    @JSONField(name = "address2_addressid")
    @JsonProperty("address2_addressid")
    private String address2Addressid;
    /**
     * Import Sequence Number
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * 地址 2: 县
     */
    @DEField(name = "address2_county")
    @TableField(value = "address2_county")
    @JSONField(name = "address2_county")
    @JsonProperty("address2_county")
    private String address2County;
    /**
     * 地址 2: 经度
     */
    @DEField(name = "address2_longitude")
    @TableField(value = "address2_longitude")
    @JSONField(name = "address2_longitude")
    @JsonProperty("address2_longitude")
    private Double address2Longitude;
    /**
     * 街道 2
     */
    @DEField(name = "address1_line2")
    @TableField(value = "address1_line2")
    @JSONField(name = "address1_line2")
    @JsonProperty("address1_line2")
    private String address1Line2;
    /**
     * 传真
     */
    @DEField(name = "address1_fax")
    @TableField(value = "address1_fax")
    @JSONField(name = "address1_fax")
    @JsonProperty("address1_fax")
    private String address1Fax;
    /**
     * 邮政编码
     */
    @DEField(name = "address1_postalcode")
    @TableField(value = "address1_postalcode")
    @JSONField(name = "address1_postalcode")
    @JsonProperty("address1_postalcode")
    private String address1Postalcode;
    /**
     * 地址 2: 邮政信箱
     */
    @DEField(name = "address2_postofficebox")
    @TableField(value = "address2_postofficebox")
    @JSONField(name = "address2_postofficebox")
    @JsonProperty("address2_postofficebox")
    private String address2Postofficebox;
    /**
     * 地址 1: 邮政信箱
     */
    @DEField(name = "address1_postofficebox")
    @TableField(value = "address1_postofficebox")
    @JSONField(name = "address1_postofficebox")
    @JsonProperty("address1_postofficebox")
    private String address1Postofficebox;
    /**
     * 地址 2: UPS 区域
     */
    @DEField(name = "address2_upszone")
    @TableField(value = "address2_upszone")
    @JSONField(name = "address2_upszone")
    @JsonProperty("address2_upszone")
    private String address2Upszone;
    /**
     * UTC Conversion Time Zone Code
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * 街道 3
     */
    @DEField(name = "address1_line3")
    @TableField(value = "address1_line3")
    @JSONField(name = "address1_line3")
    @JsonProperty("address1_line3")
    private String address1Line3;
    /**
     * 国家/地区
     */
    @DEField(name = "address1_country")
    @TableField(value = "address1_country")
    @JSONField(name = "address1_country")
    @JsonProperty("address1_country")
    private String address1Country;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * Record Created On
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 地址 2: 省/市/自治区
     */
    @DEField(name = "address2_stateorprovince")
    @TableField(value = "address2_stateorprovince")
    @JSONField(name = "address2_stateorprovince")
    @JsonProperty("address2_stateorprovince")
    private String address2Stateorprovince;
    /**
     * 街道 1
     */
    @DEField(name = "address1_line1")
    @TableField(value = "address1_line1")
    @JSONField(name = "address1_line1")
    @JsonProperty("address1_line1")
    private String address1Line1;
    /**
     * 地址 2: 纬度
     */
    @DEField(name = "address2_latitude")
    @TableField(value = "address2_latitude")
    @JSONField(name = "address2_latitude")
    @JsonProperty("address2_latitude")
    private Double address2Latitude;
    /**
     * 地址 1: 县
     */
    @DEField(name = "address1_county")
    @TableField(value = "address1_county")
    @JSONField(name = "address1_county")
    @JsonProperty("address1_county")
    private String address1County;
    /**
     * 省/直辖市/自治区
     */
    @DEField(name = "address1_stateorprovince")
    @TableField(value = "address1_stateorprovince")
    @JSONField(name = "address1_stateorprovince")
    @JsonProperty("address1_stateorprovince")
    private String address1Stateorprovince;
    /**
     * 地址 1: UPS 区域
     */
    @DEField(name = "address1_upszone")
    @TableField(value = "address1_upszone")
    @JSONField(name = "address1_upszone")
    @JsonProperty("address1_upszone")
    private String address1Upszone;
    /**
     * 地址 1: 纬度
     */
    @DEField(name = "address1_latitude")
    @TableField(value = "address1_latitude")
    @JSONField(name = "address1_latitude")
    @JsonProperty("address1_latitude")
    private Double address1Latitude;
    /**
     * 地址 2: 送货方式
     */
    @DEField(name = "address2_shippingmethodcode")
    @TableField(value = "address2_shippingmethodcode")
    @JSONField(name = "address2_shippingmethodcode")
    @JsonProperty("address2_shippingmethodcode")
    private String address2Shippingmethodcode;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 地址 2: 街道 2
     */
    @DEField(name = "address2_line2")
    @TableField(value = "address2_line2")
    @JSONField(name = "address2_line2")
    @JsonProperty("address2_line2")
    private String address2Line2;
    /**
     * 地址 2: 街道 1
     */
    @DEField(name = "address2_line1")
    @TableField(value = "address2_line1")
    @JSONField(name = "address2_line1")
    @JsonProperty("address2_line1")
    private String address2Line1;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 地址 2: 电话 1
     */
    @DEField(name = "address2_telephone1")
    @TableField(value = "address2_telephone1")
    @JSONField(name = "address2_telephone1")
    @JsonProperty("address2_telephone1")
    private String address2Telephone1;
    /**
     * 地址 1: 送货方式
     */
    @DEField(name = "address1_shippingmethodcode")
    @TableField(value = "address1_shippingmethodcode")
    @JSONField(name = "address1_shippingmethodcode")
    @JsonProperty("address1_shippingmethodcode")
    private String address1Shippingmethodcode;
    /**
     * 地址 2: 国家/地区
     */
    @DEField(name = "address2_country")
    @TableField(value = "address2_country")
    @JSONField(name = "address2_country")
    @JsonProperty("address2_country")
    private String address2Country;
    /**
     * 地址 2: 市/县
     */
    @DEField(name = "address2_city")
    @TableField(value = "address2_city")
    @JSONField(name = "address2_city")
    @JsonProperty("address2_city")
    private String address2City;
    /**
     * 地址 2: 电话 3
     */
    @DEField(name = "address2_telephone3")
    @TableField(value = "address2_telephone3")
    @JSONField(name = "address2_telephone3")
    @JsonProperty("address2_telephone3")
    private String address2Telephone3;
    /**
     * 地址 2: 名称
     */
    @DEField(name = "address2_name")
    @TableField(value = "address2_name")
    @JSONField(name = "address2_name")
    @JsonProperty("address2_name")
    private String address2Name;
    /**
     * 地址 2: UTC 时差
     */
    @DEField(name = "address2_utcoffset")
    @TableField(value = "address2_utcoffset")
    @JSONField(name = "address2_utcoffset")
    @JsonProperty("address2_utcoffset")
    private Integer address2Utcoffset;
    /**
     * 其他电话
     */
    @DEField(name = "address1_telephone2")
    @TableField(value = "address1_telephone2")
    @JSONField(name = "address1_telephone2")
    @JsonProperty("address1_telephone2")
    private String address1Telephone2;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 地址 1: 名称
     */
    @DEField(name = "address1_name")
    @TableField(value = "address1_name")
    @JSONField(name = "address1_name")
    @JsonProperty("address1_name")
    private String address1Name;
    /**
     * 地址 2: 街道 3
     */
    @DEField(name = "address2_line3")
    @TableField(value = "address2_line3")
    @JSONField(name = "address2_line3")
    @JsonProperty("address2_line3")
    private String address2Line3;
    /**
     * 地址 2: 地址类型
     */
    @DEField(name = "address2_addresstypecode")
    @TableField(value = "address2_addresstypecode")
    @JSONField(name = "address2_addresstypecode")
    @JsonProperty("address2_addresstypecode")
    private String address2Addresstypecode;
    /**
     * 场所
     */
    @DEField(isKeyField=true)
    @TableId(value= "siteid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "siteid")
    @JsonProperty("siteid")
    private String siteid;
    /**
     * Time Zone Rule Version Number
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * 地址 2: 邮政编码
     */
    @DEField(name = "address2_postalcode")
    @TableField(value = "address2_postalcode")
    @JSONField(name = "address2_postalcode")
    @JsonProperty("address2_postalcode")
    private String address2Postalcode;
    /**
     * 地址 1: 地址类型
     */
    @DEField(name = "address1_addresstypecode")
    @TableField(value = "address1_addresstypecode")
    @JSONField(name = "address1_addresstypecode")
    @JsonProperty("address1_addresstypecode")
    private String address1Addresstypecode;



    /**
     * 设置 [主要电话]
     */
    public void setAddress1Telephone1(String address1Telephone1){
        this.address1Telephone1 = address1Telephone1 ;
        this.modify("address1_telephone1",address1Telephone1);
    }

    /**
     * 设置 [Version Number]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [时区]
     */
    public void setTimezonecode(Integer timezonecode){
        this.timezonecode = timezonecode ;
        this.modify("timezonecode",timezonecode);
    }

    /**
     * 设置 [地址 1: 电话 3]
     */
    public void setAddress1Telephone3(String address1Telephone3){
        this.address1Telephone3 = address1Telephone3 ;
        this.modify("address1_telephone3",address1Telephone3);
    }

    /**
     * 设置 [地址 1: 经度]
     */
    public void setAddress1Longitude(Double address1Longitude){
        this.address1Longitude = address1Longitude ;
        this.modify("address1_longitude",address1Longitude);
    }

    /**
     * 设置 [地址 1: ID]
     */
    public void setAddress1Addressid(String address1Addressid){
        this.address1Addressid = address1Addressid ;
        this.modify("address1_addressid",address1Addressid);
    }

    /**
     * 设置 [站点名称]
     */
    public void setSitename(String sitename){
        this.sitename = sitename ;
        this.modify("sitename",sitename);
    }

    /**
     * 设置 [地址 2: 传真]
     */
    public void setAddress2Fax(String address2Fax){
        this.address2Fax = address2Fax ;
        this.modify("address2_fax",address2Fax);
    }

    /**
     * 设置 [地址 2: 电话 2]
     */
    public void setAddress2Telephone2(String address2Telephone2){
        this.address2Telephone2 = address2Telephone2 ;
        this.modify("address2_telephone2",address2Telephone2);
    }

    /**
     * 设置 [地址 1: UTC 时差]
     */
    public void setAddress1Utcoffset(Integer address1Utcoffset){
        this.address1Utcoffset = address1Utcoffset ;
        this.modify("address1_utcoffset",address1Utcoffset);
    }

    /**
     * 设置 [电子邮件]
     */
    public void setEmailaddress(String emailaddress){
        this.emailaddress = emailaddress ;
        this.modify("emailaddress",emailaddress);
    }

    /**
     * 设置 [市/县]
     */
    public void setAddress1City(String address1City){
        this.address1City = address1City ;
        this.modify("address1_city",address1City);
    }

    /**
     * 设置 [地址 2: ID]
     */
    public void setAddress2Addressid(String address2Addressid){
        this.address2Addressid = address2Addressid ;
        this.modify("address2_addressid",address2Addressid);
    }

    /**
     * 设置 [Import Sequence Number]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [地址 2: 县]
     */
    public void setAddress2County(String address2County){
        this.address2County = address2County ;
        this.modify("address2_county",address2County);
    }

    /**
     * 设置 [地址 2: 经度]
     */
    public void setAddress2Longitude(Double address2Longitude){
        this.address2Longitude = address2Longitude ;
        this.modify("address2_longitude",address2Longitude);
    }

    /**
     * 设置 [街道 2]
     */
    public void setAddress1Line2(String address1Line2){
        this.address1Line2 = address1Line2 ;
        this.modify("address1_line2",address1Line2);
    }

    /**
     * 设置 [传真]
     */
    public void setAddress1Fax(String address1Fax){
        this.address1Fax = address1Fax ;
        this.modify("address1_fax",address1Fax);
    }

    /**
     * 设置 [邮政编码]
     */
    public void setAddress1Postalcode(String address1Postalcode){
        this.address1Postalcode = address1Postalcode ;
        this.modify("address1_postalcode",address1Postalcode);
    }

    /**
     * 设置 [地址 2: 邮政信箱]
     */
    public void setAddress2Postofficebox(String address2Postofficebox){
        this.address2Postofficebox = address2Postofficebox ;
        this.modify("address2_postofficebox",address2Postofficebox);
    }

    /**
     * 设置 [地址 1: 邮政信箱]
     */
    public void setAddress1Postofficebox(String address1Postofficebox){
        this.address1Postofficebox = address1Postofficebox ;
        this.modify("address1_postofficebox",address1Postofficebox);
    }

    /**
     * 设置 [地址 2: UPS 区域]
     */
    public void setAddress2Upszone(String address2Upszone){
        this.address2Upszone = address2Upszone ;
        this.modify("address2_upszone",address2Upszone);
    }

    /**
     * 设置 [UTC Conversion Time Zone Code]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [街道 3]
     */
    public void setAddress1Line3(String address1Line3){
        this.address1Line3 = address1Line3 ;
        this.modify("address1_line3",address1Line3);
    }

    /**
     * 设置 [国家/地区]
     */
    public void setAddress1Country(String address1Country){
        this.address1Country = address1Country ;
        this.modify("address1_country",address1Country);
    }

    /**
     * 设置 [Record Created On]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [Record Created On]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [地址 2: 省/市/自治区]
     */
    public void setAddress2Stateorprovince(String address2Stateorprovince){
        this.address2Stateorprovince = address2Stateorprovince ;
        this.modify("address2_stateorprovince",address2Stateorprovince);
    }

    /**
     * 设置 [街道 1]
     */
    public void setAddress1Line1(String address1Line1){
        this.address1Line1 = address1Line1 ;
        this.modify("address1_line1",address1Line1);
    }

    /**
     * 设置 [地址 2: 纬度]
     */
    public void setAddress2Latitude(Double address2Latitude){
        this.address2Latitude = address2Latitude ;
        this.modify("address2_latitude",address2Latitude);
    }

    /**
     * 设置 [地址 1: 县]
     */
    public void setAddress1County(String address1County){
        this.address1County = address1County ;
        this.modify("address1_county",address1County);
    }

    /**
     * 设置 [省/直辖市/自治区]
     */
    public void setAddress1Stateorprovince(String address1Stateorprovince){
        this.address1Stateorprovince = address1Stateorprovince ;
        this.modify("address1_stateorprovince",address1Stateorprovince);
    }

    /**
     * 设置 [地址 1: UPS 区域]
     */
    public void setAddress1Upszone(String address1Upszone){
        this.address1Upszone = address1Upszone ;
        this.modify("address1_upszone",address1Upszone);
    }

    /**
     * 设置 [地址 1: 纬度]
     */
    public void setAddress1Latitude(Double address1Latitude){
        this.address1Latitude = address1Latitude ;
        this.modify("address1_latitude",address1Latitude);
    }

    /**
     * 设置 [地址 2: 送货方式]
     */
    public void setAddress2Shippingmethodcode(String address2Shippingmethodcode){
        this.address2Shippingmethodcode = address2Shippingmethodcode ;
        this.modify("address2_shippingmethodcode",address2Shippingmethodcode);
    }

    /**
     * 设置 [地址 2: 街道 2]
     */
    public void setAddress2Line2(String address2Line2){
        this.address2Line2 = address2Line2 ;
        this.modify("address2_line2",address2Line2);
    }

    /**
     * 设置 [地址 2: 街道 1]
     */
    public void setAddress2Line1(String address2Line1){
        this.address2Line1 = address2Line1 ;
        this.modify("address2_line1",address2Line1);
    }

    /**
     * 设置 [地址 2: 电话 1]
     */
    public void setAddress2Telephone1(String address2Telephone1){
        this.address2Telephone1 = address2Telephone1 ;
        this.modify("address2_telephone1",address2Telephone1);
    }

    /**
     * 设置 [地址 1: 送货方式]
     */
    public void setAddress1Shippingmethodcode(String address1Shippingmethodcode){
        this.address1Shippingmethodcode = address1Shippingmethodcode ;
        this.modify("address1_shippingmethodcode",address1Shippingmethodcode);
    }

    /**
     * 设置 [地址 2: 国家/地区]
     */
    public void setAddress2Country(String address2Country){
        this.address2Country = address2Country ;
        this.modify("address2_country",address2Country);
    }

    /**
     * 设置 [地址 2: 市/县]
     */
    public void setAddress2City(String address2City){
        this.address2City = address2City ;
        this.modify("address2_city",address2City);
    }

    /**
     * 设置 [地址 2: 电话 3]
     */
    public void setAddress2Telephone3(String address2Telephone3){
        this.address2Telephone3 = address2Telephone3 ;
        this.modify("address2_telephone3",address2Telephone3);
    }

    /**
     * 设置 [地址 2: 名称]
     */
    public void setAddress2Name(String address2Name){
        this.address2Name = address2Name ;
        this.modify("address2_name",address2Name);
    }

    /**
     * 设置 [地址 2: UTC 时差]
     */
    public void setAddress2Utcoffset(Integer address2Utcoffset){
        this.address2Utcoffset = address2Utcoffset ;
        this.modify("address2_utcoffset",address2Utcoffset);
    }

    /**
     * 设置 [其他电话]
     */
    public void setAddress1Telephone2(String address1Telephone2){
        this.address1Telephone2 = address1Telephone2 ;
        this.modify("address1_telephone2",address1Telephone2);
    }

    /**
     * 设置 [地址 1: 名称]
     */
    public void setAddress1Name(String address1Name){
        this.address1Name = address1Name ;
        this.modify("address1_name",address1Name);
    }

    /**
     * 设置 [地址 2: 街道 3]
     */
    public void setAddress2Line3(String address2Line3){
        this.address2Line3 = address2Line3 ;
        this.modify("address2_line3",address2Line3);
    }

    /**
     * 设置 [地址 2: 地址类型]
     */
    public void setAddress2Addresstypecode(String address2Addresstypecode){
        this.address2Addresstypecode = address2Addresstypecode ;
        this.modify("address2_addresstypecode",address2Addresstypecode);
    }

    /**
     * 设置 [Time Zone Rule Version Number]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [地址 2: 邮政编码]
     */
    public void setAddress2Postalcode(String address2Postalcode){
        this.address2Postalcode = address2Postalcode ;
        this.modify("address2_postalcode",address2Postalcode);
    }

    /**
     * 设置 [地址 1: 地址类型]
     */
    public void setAddress1Addresstypecode(String address1Addresstypecode){
        this.address1Addresstypecode = address1Addresstypecode ;
        this.modify("address1_addresstypecode",address1Addresstypecode);
    }


}



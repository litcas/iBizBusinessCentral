package cn.ibizlab.businesscentral.core.marketing.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.marketing.domain.CampaignActivity;
import cn.ibizlab.businesscentral.core.marketing.filter.CampaignActivitySearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[CampaignActivity] 服务对象接口
 */
public interface ICampaignActivityService extends IService<CampaignActivity>{

    boolean create(CampaignActivity et) ;
    void createBatch(List<CampaignActivity> list) ;
    boolean update(CampaignActivity et) ;
    void updateBatch(List<CampaignActivity> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    CampaignActivity get(String key) ;
    CampaignActivity getDraft(CampaignActivity et) ;
    boolean checkKey(CampaignActivity et) ;
    boolean save(CampaignActivity et) ;
    void saveBatch(List<CampaignActivity> list) ;
    Page<CampaignActivity> searchByParentKey(CampaignActivitySearchContext context) ;
    Page<CampaignActivity> searchDefault(CampaignActivitySearchContext context) ;
    List<CampaignActivity> selectBySlaid(String slaid) ;
    void removeBySlaid(String slaid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<CampaignActivity> getCampaignactivityByIds(List<String> ids) ;
    List<CampaignActivity> getCampaignactivityByEntities(List<CampaignActivity> entities) ;
}



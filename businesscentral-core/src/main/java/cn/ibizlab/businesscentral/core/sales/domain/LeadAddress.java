package cn.ibizlab.businesscentral.core.sales.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[潜在顾客地址]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "LEADADDRESS",resultMap = "LeadAddressResultMap")
public class LeadAddress extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 主要电话
     */
    @TableField(value = "telephone1")
    @JSONField(name = "telephone1")
    @JsonProperty("telephone1")
    private String telephone1;
    /**
     * Import Sequence Number
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * Record Created On
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * ParentName
     */
    @TableField(value = "parentname")
    @JSONField(name = "parentname")
    @JsonProperty("parentname")
    private String parentname;
    /**
     * 地址类型
     */
    @TableField(value = "addresstypecode")
    @JSONField(name = "addresstypecode")
    @JsonProperty("addresstypecode")
    private String addresstypecode;
    /**
     * Time Zone Rule Version Number
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * 负责人
     */
    @TableField(value = "ownerid")
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;
    /**
     * UPS 区域
     */
    @TableField(value = "upszone")
    @JSONField(name = "upszone")
    @JsonProperty("upszone")
    private String upszone;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 街道 3
     */
    @TableField(value = "line3")
    @JSONField(name = "line3")
    @JsonProperty("line3")
    private String line3;
    /**
     * 县
     */
    @TableField(value = "county")
    @JSONField(name = "county")
    @JsonProperty("county")
    private String county;
    /**
     * 电话 3
     */
    @TableField(value = "telephone3")
    @JSONField(name = "telephone3")
    @JsonProperty("telephone3")
    private String telephone3;
    /**
     * 传真
     */
    @TableField(value = "fax")
    @JSONField(name = "fax")
    @JsonProperty("fax")
    private String fax;
    /**
     * 街道 1
     */
    @TableField(value = "line1")
    @JSONField(name = "line1")
    @JsonProperty("line1")
    private String line1;
    /**
     * 电话 2
     */
    @TableField(value = "telephone2")
    @JSONField(name = "telephone2")
    @JsonProperty("telephone2")
    private String telephone2;
    /**
     * 地址编号
     */
    @TableField(value = "addressnumber")
    @JSONField(name = "addressnumber")
    @JsonProperty("addressnumber")
    private Integer addressnumber;
    /**
     * UTC Conversion Time Zone Code
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * Version Number
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 街道 2
     */
    @TableField(value = "line2")
    @JSONField(name = "line2")
    @JsonProperty("line2")
    private String line2;
    /**
     * 国家/地区
     */
    @TableField(value = "country")
    @JSONField(name = "country")
    @JsonProperty("country")
    private String country;
    /**
     * 邮政编码
     */
    @TableField(value = "postalcode")
    @JSONField(name = "postalcode")
    @JsonProperty("postalcode")
    private String postalcode;
    /**
     * 经度
     */
    @TableField(value = "longitude")
    @JSONField(name = "longitude")
    @JsonProperty("longitude")
    private Double longitude;
    /**
     * 地址
     */
    @DEField(isKeyField=true)
    @TableId(value= "leadaddressid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "leadaddressid")
    @JsonProperty("leadaddressid")
    private String leadaddressid;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 邮政信箱
     */
    @TableField(value = "postofficebox")
    @JSONField(name = "postofficebox")
    @JsonProperty("postofficebox")
    private String postofficebox;
    /**
     * 潜在客户地址名称
     */
    @TableField(value = "leadaddressname")
    @JSONField(name = "leadaddressname")
    @JsonProperty("leadaddressname")
    private String leadaddressname;
    /**
     * 地址
     */
    @TableField(value = "composite")
    @JSONField(name = "composite")
    @JsonProperty("composite")
    private String composite;
    /**
     * OwningBusinessUnit
     */
    @TableField(value = "owningbusinessunit")
    @JSONField(name = "owningbusinessunit")
    @JsonProperty("owningbusinessunit")
    private String owningbusinessunit;
    /**
     * 送货方式
     */
    @TableField(value = "shippingmethodcode")
    @JSONField(name = "shippingmethodcode")
    @JsonProperty("shippingmethodcode")
    private String shippingmethodcode;
    /**
     * UTC 时差
     */
    @TableField(value = "utcoffset")
    @JSONField(name = "utcoffset")
    @JsonProperty("utcoffset")
    private Integer utcoffset;
    /**
     * 纬度
     */
    @TableField(value = "latitude")
    @JSONField(name = "latitude")
    @JsonProperty("latitude")
    private Double latitude;
    /**
     * 汇率
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * 父级
     */
    @TableField(value = "parentid")
    @JSONField(name = "parentid")
    @JsonProperty("parentid")
    private String parentid;
    /**
     * 货币
     */
    @TableField(value = "transactioncurrencyid")
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.sales.domain.Lead parent;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency;



    /**
     * 设置 [主要电话]
     */
    public void setTelephone1(String telephone1){
        this.telephone1 = telephone1 ;
        this.modify("telephone1",telephone1);
    }

    /**
     * 设置 [Import Sequence Number]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [Record Created On]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [Record Created On]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [ParentName]
     */
    public void setParentname(String parentname){
        this.parentname = parentname ;
        this.modify("parentname",parentname);
    }

    /**
     * 设置 [地址类型]
     */
    public void setAddresstypecode(String addresstypecode){
        this.addresstypecode = addresstypecode ;
        this.modify("addresstypecode",addresstypecode);
    }

    /**
     * 设置 [Time Zone Rule Version Number]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnerid(String ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [UPS 区域]
     */
    public void setUpszone(String upszone){
        this.upszone = upszone ;
        this.modify("upszone",upszone);
    }

    /**
     * 设置 [街道 3]
     */
    public void setLine3(String line3){
        this.line3 = line3 ;
        this.modify("line3",line3);
    }

    /**
     * 设置 [县]
     */
    public void setCounty(String county){
        this.county = county ;
        this.modify("county",county);
    }

    /**
     * 设置 [电话 3]
     */
    public void setTelephone3(String telephone3){
        this.telephone3 = telephone3 ;
        this.modify("telephone3",telephone3);
    }

    /**
     * 设置 [传真]
     */
    public void setFax(String fax){
        this.fax = fax ;
        this.modify("fax",fax);
    }

    /**
     * 设置 [街道 1]
     */
    public void setLine1(String line1){
        this.line1 = line1 ;
        this.modify("line1",line1);
    }

    /**
     * 设置 [电话 2]
     */
    public void setTelephone2(String telephone2){
        this.telephone2 = telephone2 ;
        this.modify("telephone2",telephone2);
    }

    /**
     * 设置 [地址编号]
     */
    public void setAddressnumber(Integer addressnumber){
        this.addressnumber = addressnumber ;
        this.modify("addressnumber",addressnumber);
    }

    /**
     * 设置 [UTC Conversion Time Zone Code]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [Version Number]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [街道 2]
     */
    public void setLine2(String line2){
        this.line2 = line2 ;
        this.modify("line2",line2);
    }

    /**
     * 设置 [国家/地区]
     */
    public void setCountry(String country){
        this.country = country ;
        this.modify("country",country);
    }

    /**
     * 设置 [邮政编码]
     */
    public void setPostalcode(String postalcode){
        this.postalcode = postalcode ;
        this.modify("postalcode",postalcode);
    }

    /**
     * 设置 [经度]
     */
    public void setLongitude(Double longitude){
        this.longitude = longitude ;
        this.modify("longitude",longitude);
    }

    /**
     * 设置 [邮政信箱]
     */
    public void setPostofficebox(String postofficebox){
        this.postofficebox = postofficebox ;
        this.modify("postofficebox",postofficebox);
    }

    /**
     * 设置 [潜在客户地址名称]
     */
    public void setLeadaddressname(String leadaddressname){
        this.leadaddressname = leadaddressname ;
        this.modify("leadaddressname",leadaddressname);
    }

    /**
     * 设置 [地址]
     */
    public void setComposite(String composite){
        this.composite = composite ;
        this.modify("composite",composite);
    }

    /**
     * 设置 [OwningBusinessUnit]
     */
    public void setOwningbusinessunit(String owningbusinessunit){
        this.owningbusinessunit = owningbusinessunit ;
        this.modify("owningbusinessunit",owningbusinessunit);
    }

    /**
     * 设置 [送货方式]
     */
    public void setShippingmethodcode(String shippingmethodcode){
        this.shippingmethodcode = shippingmethodcode ;
        this.modify("shippingmethodcode",shippingmethodcode);
    }

    /**
     * 设置 [UTC 时差]
     */
    public void setUtcoffset(Integer utcoffset){
        this.utcoffset = utcoffset ;
        this.modify("utcoffset",utcoffset);
    }

    /**
     * 设置 [纬度]
     */
    public void setLatitude(Double latitude){
        this.latitude = latitude ;
        this.modify("latitude",latitude);
    }

    /**
     * 设置 [汇率]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [父级]
     */
    public void setParentid(String parentid){
        this.parentid = parentid ;
        this.modify("parentid",parentid);
    }

    /**
     * 设置 [货币]
     */
    public void setTransactioncurrencyid(String transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }


}



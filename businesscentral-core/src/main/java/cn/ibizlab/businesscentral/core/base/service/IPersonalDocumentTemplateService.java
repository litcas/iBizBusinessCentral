package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.PersonalDocumentTemplate;
import cn.ibizlab.businesscentral.core.base.filter.PersonalDocumentTemplateSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[PersonalDocumentTemplate] 服务对象接口
 */
public interface IPersonalDocumentTemplateService extends IService<PersonalDocumentTemplate>{

    boolean create(PersonalDocumentTemplate et) ;
    void createBatch(List<PersonalDocumentTemplate> list) ;
    boolean update(PersonalDocumentTemplate et) ;
    void updateBatch(List<PersonalDocumentTemplate> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    PersonalDocumentTemplate get(String key) ;
    PersonalDocumentTemplate getDraft(PersonalDocumentTemplate et) ;
    boolean checkKey(PersonalDocumentTemplate et) ;
    boolean save(PersonalDocumentTemplate et) ;
    void saveBatch(List<PersonalDocumentTemplate> list) ;
    Page<PersonalDocumentTemplate> searchDefault(PersonalDocumentTemplateSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<PersonalDocumentTemplate> getPersonaldocumenttemplateByIds(List<String> ids) ;
    List<PersonalDocumentTemplate> getPersonaldocumenttemplateByEntities(List<PersonalDocumentTemplate> entities) ;
}



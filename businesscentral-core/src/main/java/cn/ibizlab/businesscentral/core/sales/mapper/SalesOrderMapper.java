package cn.ibizlab.businesscentral.core.sales.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.sales.domain.SalesOrder;
import cn.ibizlab.businesscentral.core.sales.filter.SalesOrderSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface SalesOrderMapper extends BaseMapper<SalesOrder>{

    Page<SalesOrder> searchByParentKey(IPage page, @Param("srf") SalesOrderSearchContext context, @Param("ew") Wrapper<SalesOrder> wrapper) ;
    Page<SalesOrder> searchCancel(IPage page, @Param("srf") SalesOrderSearchContext context, @Param("ew") Wrapper<SalesOrder> wrapper) ;
    Page<SalesOrder> searchDefault(IPage page, @Param("srf") SalesOrderSearchContext context, @Param("ew") Wrapper<SalesOrder> wrapper) ;
    Page<SalesOrder> searchFinish(IPage page, @Param("srf") SalesOrderSearchContext context, @Param("ew") Wrapper<SalesOrder> wrapper) ;
    Page<SalesOrder> searchInvoiced(IPage page, @Param("srf") SalesOrderSearchContext context, @Param("ew") Wrapper<SalesOrder> wrapper) ;
    @Override
    SalesOrder selectById(Serializable id);
    @Override
    int insert(SalesOrder entity);
    @Override
    int updateById(@Param(Constants.ENTITY) SalesOrder entity);
    @Override
    int update(@Param(Constants.ENTITY) SalesOrder entity, @Param("ew") Wrapper<SalesOrder> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<SalesOrder> selectByCampaignid(@Param("campaignid") Serializable campaignid) ;

    List<SalesOrder> selectByOpportunityid(@Param("opportunityid") Serializable opportunityid) ;

    List<SalesOrder> selectByPricelevelid(@Param("pricelevelid") Serializable pricelevelid) ;

    List<SalesOrder> selectByQuoteid(@Param("quoteid") Serializable quoteid) ;

    List<SalesOrder> selectBySlaid(@Param("slaid") Serializable slaid) ;

    List<SalesOrder> selectByTransactioncurrencyid(@Param("transactioncurrencyid") Serializable transactioncurrencyid) ;

}

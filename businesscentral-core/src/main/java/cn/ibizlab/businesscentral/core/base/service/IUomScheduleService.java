package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.UomSchedule;
import cn.ibizlab.businesscentral.core.base.filter.UomScheduleSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[UomSchedule] 服务对象接口
 */
public interface IUomScheduleService extends IService<UomSchedule>{

    boolean create(UomSchedule et) ;
    void createBatch(List<UomSchedule> list) ;
    boolean update(UomSchedule et) ;
    void updateBatch(List<UomSchedule> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    UomSchedule get(String key) ;
    UomSchedule getDraft(UomSchedule et) ;
    boolean checkKey(UomSchedule et) ;
    boolean save(UomSchedule et) ;
    void saveBatch(List<UomSchedule> list) ;
    Page<UomSchedule> searchDefault(UomScheduleSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<UomSchedule> getUomscheduleByIds(List<String> ids) ;
    List<UomSchedule> getUomscheduleByEntities(List<UomSchedule> entities) ;
}



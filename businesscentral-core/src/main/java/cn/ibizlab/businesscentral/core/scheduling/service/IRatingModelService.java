package cn.ibizlab.businesscentral.core.scheduling.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.scheduling.domain.RatingModel;
import cn.ibizlab.businesscentral.core.scheduling.filter.RatingModelSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[RatingModel] 服务对象接口
 */
public interface IRatingModelService extends IService<RatingModel>{

    boolean create(RatingModel et) ;
    void createBatch(List<RatingModel> list) ;
    boolean update(RatingModel et) ;
    void updateBatch(List<RatingModel> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    RatingModel get(String key) ;
    RatingModel getDraft(RatingModel et) ;
    boolean checkKey(RatingModel et) ;
    boolean save(RatingModel et) ;
    void saveBatch(List<RatingModel> list) ;
    Page<RatingModel> searchDefault(RatingModelSearchContext context) ;
    List<RatingModel> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<RatingModel> getRatingmodelByIds(List<String> ids) ;
    List<RatingModel> getRatingmodelByEntities(List<RatingModel> entities) ;
}



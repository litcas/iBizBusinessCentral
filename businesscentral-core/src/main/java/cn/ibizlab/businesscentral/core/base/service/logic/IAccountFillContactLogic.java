package cn.ibizlab.businesscentral.core.base.service.logic;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.businesscentral.core.base.domain.Account;

/**
 * 关系型数据实体[FillContact] 对象
 */
public interface IAccountFillContactLogic {

    void execute(Account et) ;

}

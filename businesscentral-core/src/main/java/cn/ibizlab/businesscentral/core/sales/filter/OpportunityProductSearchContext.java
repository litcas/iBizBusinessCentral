package cn.ibizlab.businesscentral.core.sales.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.sales.domain.OpportunityProduct;
/**
 * 关系型数据实体[OpportunityProduct] 查询条件对象
 */
@Slf4j
@Data
public class OpportunityProductSearchContext extends QueryWrapperContext<OpportunityProduct> {

	private String n_skippricecalculation_eq;//[SkipPriceCalculation]
	public void setN_skippricecalculation_eq(String n_skippricecalculation_eq) {
        this.n_skippricecalculation_eq = n_skippricecalculation_eq;
        if(!ObjectUtils.isEmpty(this.n_skippricecalculation_eq)){
            this.getSearchCond().eq("skippricecalculation", n_skippricecalculation_eq);
        }
    }
	private String n_opportunitystatecode_eq;//[商机状态]
	public void setN_opportunitystatecode_eq(String n_opportunitystatecode_eq) {
        this.n_opportunitystatecode_eq = n_opportunitystatecode_eq;
        if(!ObjectUtils.isEmpty(this.n_opportunitystatecode_eq)){
            this.getSearchCond().eq("opportunitystatecode", n_opportunitystatecode_eq);
        }
    }
	private String n_opportunityproductname_like;//[名称]
	public void setN_opportunityproductname_like(String n_opportunityproductname_like) {
        this.n_opportunityproductname_like = n_opportunityproductname_like;
        if(!ObjectUtils.isEmpty(this.n_opportunityproductname_like)){
            this.getSearchCond().like("opportunityproductname", n_opportunityproductname_like);
        }
    }
	private String n_propertyconfigurationstatus_eq;//[属性配置]
	public void setN_propertyconfigurationstatus_eq(String n_propertyconfigurationstatus_eq) {
        this.n_propertyconfigurationstatus_eq = n_propertyconfigurationstatus_eq;
        if(!ObjectUtils.isEmpty(this.n_propertyconfigurationstatus_eq)){
            this.getSearchCond().eq("propertyconfigurationstatus", n_propertyconfigurationstatus_eq);
        }
    }
	private String n_producttypecode_eq;//[产品类型]
	public void setN_producttypecode_eq(String n_producttypecode_eq) {
        this.n_producttypecode_eq = n_producttypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_producttypecode_eq)){
            this.getSearchCond().eq("producttypecode", n_producttypecode_eq);
        }
    }
	private String n_pricingerrorcode_eq;//[定价错误]
	public void setN_pricingerrorcode_eq(String n_pricingerrorcode_eq) {
        this.n_pricingerrorcode_eq = n_pricingerrorcode_eq;
        if(!ObjectUtils.isEmpty(this.n_pricingerrorcode_eq)){
            this.getSearchCond().eq("pricingerrorcode", n_pricingerrorcode_eq);
        }
    }
	private String n_productid_eq;//[现有产品]
	public void setN_productid_eq(String n_productid_eq) {
        this.n_productid_eq = n_productid_eq;
        if(!ObjectUtils.isEmpty(this.n_productid_eq)){
            this.getSearchCond().eq("productid", n_productid_eq);
        }
    }
	private String n_parentbundleidref_eq;//[Parent bundle product]
	public void setN_parentbundleidref_eq(String n_parentbundleidref_eq) {
        this.n_parentbundleidref_eq = n_parentbundleidref_eq;
        if(!ObjectUtils.isEmpty(this.n_parentbundleidref_eq)){
            this.getSearchCond().eq("parentbundleidref", n_parentbundleidref_eq);
        }
    }
	private String n_uomid_eq;//[计价单位]
	public void setN_uomid_eq(String n_uomid_eq) {
        this.n_uomid_eq = n_uomid_eq;
        if(!ObjectUtils.isEmpty(this.n_uomid_eq)){
            this.getSearchCond().eq("uomid", n_uomid_eq);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }
	private String n_opportunityid_eq;//[商机]
	public void setN_opportunityid_eq(String n_opportunityid_eq) {
        this.n_opportunityid_eq = n_opportunityid_eq;
        if(!ObjectUtils.isEmpty(this.n_opportunityid_eq)){
            this.getSearchCond().eq("opportunityid", n_opportunityid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("opportunityproductname", query)   
            );
		 }
	}
}




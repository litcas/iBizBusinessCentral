package cn.ibizlab.businesscentral.core.base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[反馈]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "FEEDBACK",resultMap = "FeedbackResultMap")
public class Feedback extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * CreatedByContactName
     */
    @TableField(value = "createdbycontactname")
    @JSONField(name = "createdbycontactname")
    @JsonProperty("createdbycontactname")
    private String createdbycontactname;
    /**
     * 评论
     */
    @TableField(value = "comments")
    @JSONField(name = "comments")
    @JsonProperty("comments")
    private String comments;
    /**
     * 标准化评分
     */
    @TableField(value = "normalizedrating")
    @JSONField(name = "normalizedrating")
    @JsonProperty("normalizedrating")
    private BigDecimal normalizedrating;
    /**
     * 记录创建日期
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 最高评分
     */
    @TableField(value = "maxrating")
    @JSONField(name = "maxrating")
    @JsonProperty("maxrating")
    private Integer maxrating;
    /**
     * 版本号
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * CreatedOnBehalfByContactName
     */
    @TableField(value = "createdonbehalfbycontactname")
    @JSONField(name = "createdonbehalfbycontactname")
    @JsonProperty("createdonbehalfbycontactname")
    private String createdonbehalfbycontactname;
    /**
     * 汇率
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * 关闭人
     */
    @TableField(value = "closedbyname")
    @JSONField(name = "closedbyname")
    @JsonProperty("closedbyname")
    private String closedbyname;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 相关项
     */
    @TableField(value = "regardingobjectid")
    @JSONField(name = "regardingobjectid")
    @JsonProperty("regardingobjectid")
    private String regardingobjectid;
    /**
     * 关于
     */
    @TableField(value = "regardingobjectname")
    @JSONField(name = "regardingobjectname")
    @JsonProperty("regardingobjectname")
    private String regardingobjectname;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * RegardingObjectTypeCode
     */
    @TableField(value = "regardingobjecttypecode")
    @JSONField(name = "regardingobjecttypecode")
    @JsonProperty("regardingobjecttypecode")
    private String regardingobjecttypecode;
    /**
     * 负责人类型
     */
    @TableField(value = "ownertype")
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 反馈
     */
    @DEField(isKeyField=true)
    @TableId(value= "feedbackid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "feedbackid")
    @JsonProperty("feedbackid")
    private String feedbackid;
    /**
     * 状态
     */
    @TableField(value = "statecode")
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;
    /**
     * 关闭日期
     */
    @TableField(value = "closedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "closedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("closedon")
    private Timestamp closedon;
    /**
     * 状态描述
     */
    @TableField(value = "statuscode")
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;
    /**
     * 负责人
     */
    @TableField(value = "ownername")
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;
    /**
     * 评分
     */
    @TableField(value = "rating")
    @JSONField(name = "rating")
    @JsonProperty("rating")
    private Integer rating;
    /**
     * 来源
     */
    @TableField(value = "source")
    @JSONField(name = "source")
    @JsonProperty("source")
    private String source;
    /**
     * 负责人
     */
    @TableField(value = "ownerid")
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 导入序号
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * 最低评分
     */
    @TableField(value = "minrating")
    @JSONField(name = "minrating")
    @JsonProperty("minrating")
    private Integer minrating;
    /**
     * 标题
     */
    @TableField(value = "title")
    @JSONField(name = "title")
    @JsonProperty("title")
    private String title;
    /**
     * 创建者(联系人)
     */
    @TableField(value = "createdbycontact")
    @JSONField(name = "createdbycontact")
    @JsonProperty("createdbycontact")
    private String createdbycontact;
    /**
     * 货币
     */
    @TableField(value = "transactioncurrencyid")
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;
    /**
     * 已创建 OnBelhalfBy (联系人)
     */
    @TableField(value = "createdonbehalfbycontact")
    @JSONField(name = "createdonbehalfbycontact")
    @JsonProperty("createdonbehalfbycontact")
    private String createdonbehalfbycontact;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Contact createdbyconta;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Contact createdonbehalfbyconta;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency;



    /**
     * 设置 [CreatedByContactName]
     */
    public void setCreatedbycontactname(String createdbycontactname){
        this.createdbycontactname = createdbycontactname ;
        this.modify("createdbycontactname",createdbycontactname);
    }

    /**
     * 设置 [评论]
     */
    public void setComments(String comments){
        this.comments = comments ;
        this.modify("comments",comments);
    }

    /**
     * 设置 [标准化评分]
     */
    public void setNormalizedrating(BigDecimal normalizedrating){
        this.normalizedrating = normalizedrating ;
        this.modify("normalizedrating",normalizedrating);
    }

    /**
     * 设置 [记录创建日期]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [记录创建日期]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [最高评分]
     */
    public void setMaxrating(Integer maxrating){
        this.maxrating = maxrating ;
        this.modify("maxrating",maxrating);
    }

    /**
     * 设置 [版本号]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [CreatedOnBehalfByContactName]
     */
    public void setCreatedonbehalfbycontactname(String createdonbehalfbycontactname){
        this.createdonbehalfbycontactname = createdonbehalfbycontactname ;
        this.modify("createdonbehalfbycontactname",createdonbehalfbycontactname);
    }

    /**
     * 设置 [汇率]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [关闭人]
     */
    public void setClosedbyname(String closedbyname){
        this.closedbyname = closedbyname ;
        this.modify("closedbyname",closedbyname);
    }

    /**
     * 设置 [相关项]
     */
    public void setRegardingobjectid(String regardingobjectid){
        this.regardingobjectid = regardingobjectid ;
        this.modify("regardingobjectid",regardingobjectid);
    }

    /**
     * 设置 [关于]
     */
    public void setRegardingobjectname(String regardingobjectname){
        this.regardingobjectname = regardingobjectname ;
        this.modify("regardingobjectname",regardingobjectname);
    }

    /**
     * 设置 [RegardingObjectTypeCode]
     */
    public void setRegardingobjecttypecode(String regardingobjecttypecode){
        this.regardingobjecttypecode = regardingobjecttypecode ;
        this.modify("regardingobjecttypecode",regardingobjecttypecode);
    }

    /**
     * 设置 [负责人类型]
     */
    public void setOwnertype(String ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [状态]
     */
    public void setStatecode(Integer statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [关闭日期]
     */
    public void setClosedon(Timestamp closedon){
        this.closedon = closedon ;
        this.modify("closedon",closedon);
    }

    /**
     * 格式化日期 [关闭日期]
     */
    public String formatClosedon(){
        if (this.closedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(closedon);
    }
    /**
     * 设置 [状态描述]
     */
    public void setStatuscode(Integer statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnername(String ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [评分]
     */
    public void setRating(Integer rating){
        this.rating = rating ;
        this.modify("rating",rating);
    }

    /**
     * 设置 [来源]
     */
    public void setSource(String source){
        this.source = source ;
        this.modify("source",source);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnerid(String ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [导入序号]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [最低评分]
     */
    public void setMinrating(Integer minrating){
        this.minrating = minrating ;
        this.modify("minrating",minrating);
    }

    /**
     * 设置 [标题]
     */
    public void setTitle(String title){
        this.title = title ;
        this.modify("title",title);
    }

    /**
     * 设置 [创建者(联系人)]
     */
    public void setCreatedbycontact(String createdbycontact){
        this.createdbycontact = createdbycontact ;
        this.modify("createdbycontact",createdbycontact);
    }

    /**
     * 设置 [货币]
     */
    public void setTransactioncurrencyid(String transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }

    /**
     * 设置 [已创建 OnBelhalfBy (联系人)]
     */
    public void setCreatedonbehalfbycontact(String createdonbehalfbycontact){
        this.createdonbehalfbycontact = createdonbehalfbycontact ;
        this.modify("createdonbehalfbycontact",createdonbehalfbycontact);
    }


}



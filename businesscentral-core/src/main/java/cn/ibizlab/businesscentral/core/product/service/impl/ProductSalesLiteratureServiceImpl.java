package cn.ibizlab.businesscentral.core.product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.product.domain.ProductSalesLiterature;
import cn.ibizlab.businesscentral.core.product.filter.ProductSalesLiteratureSearchContext;
import cn.ibizlab.businesscentral.core.product.service.IProductSalesLiteratureService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.product.mapper.ProductSalesLiteratureMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[产品宣传资料] 服务对象接口实现
 */
@Slf4j
@Service("ProductSalesLiteratureServiceImpl")
public class ProductSalesLiteratureServiceImpl extends ServiceImpl<ProductSalesLiteratureMapper, ProductSalesLiterature> implements IProductSalesLiteratureService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.product.service.IProductService productService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.ISalesLiteratureService salesliteratureService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(ProductSalesLiterature et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getRelationshipsid()),et);
        return true;
    }

    @Override
    public void createBatch(List<ProductSalesLiterature> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(ProductSalesLiterature et) {
        fillParentData(et);
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("relationshipsid",et.getRelationshipsid())))
            return false;
        CachedBeanCopier.copy(get(et.getRelationshipsid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<ProductSalesLiterature> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public ProductSalesLiterature get(String key) {
        ProductSalesLiterature et = getById(key);
        if(et==null){
            et=new ProductSalesLiterature();
            et.setRelationshipsid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public ProductSalesLiterature getDraft(ProductSalesLiterature et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(ProductSalesLiterature et) {
        return (!ObjectUtils.isEmpty(et.getRelationshipsid()))&&(!Objects.isNull(this.getById(et.getRelationshipsid())));
    }
    @Override
    @Transactional
    public boolean save(ProductSalesLiterature et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(ProductSalesLiterature et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<ProductSalesLiterature> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<ProductSalesLiterature> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<ProductSalesLiterature> selectByEntityid(String productid) {
        return baseMapper.selectByEntityid(productid);
    }

    @Override
    public void removeByEntityid(String productid) {
        this.remove(new QueryWrapper<ProductSalesLiterature>().eq("entityid",productid));
    }

	@Override
    public List<ProductSalesLiterature> selectByEntity2id(String salesliteratureid) {
        return baseMapper.selectByEntity2id(salesliteratureid);
    }

    @Override
    public void removeByEntity2id(String salesliteratureid) {
        this.remove(new QueryWrapper<ProductSalesLiterature>().eq("entity2id",salesliteratureid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<ProductSalesLiterature> searchDefault(ProductSalesLiteratureSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<ProductSalesLiterature> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<ProductSalesLiterature>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(ProductSalesLiterature et){
        //实体关系[DER1N_PRODUCTSALESLITERATURE_PRODUCT_ENTITYID]
        if(!ObjectUtils.isEmpty(et.getEntityid())){
            cn.ibizlab.businesscentral.core.product.domain.Product product=et.getProduct();
            if(ObjectUtils.isEmpty(product)){
                cn.ibizlab.businesscentral.core.product.domain.Product majorEntity=productService.get(et.getEntityid());
                et.setProduct(majorEntity);
                product=majorEntity;
            }
            et.setEntityname(product.getProductname());
        }
        //实体关系[DER1N_PRODUCTSALESLITERATURE_SALESLITERATURE_ENTITY2ID]
        if(!ObjectUtils.isEmpty(et.getEntity2id())){
            cn.ibizlab.businesscentral.core.sales.domain.SalesLiterature salesliterature=et.getSalesliterature();
            if(ObjectUtils.isEmpty(salesliterature)){
                cn.ibizlab.businesscentral.core.sales.domain.SalesLiterature majorEntity=salesliteratureService.get(et.getEntity2id());
                et.setSalesliterature(majorEntity);
                salesliterature=majorEntity;
            }
            et.setEntity2name(salesliterature.getSalesliteraturename());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<ProductSalesLiterature> getProductsalesliteratureByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<ProductSalesLiterature> getProductsalesliteratureByEntities(List<ProductSalesLiterature> entities) {
        List ids =new ArrayList();
        for(ProductSalesLiterature entity : entities){
            Serializable id=entity.getRelationshipsid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




package cn.ibizlab.businesscentral.core.extensions.service;

import cn.ibizlab.businesscentral.core.finance.service.impl.InvoiceServiceImpl;
import lombok.extern.slf4j.Slf4j;
import cn.ibizlab.businesscentral.core.finance.domain.Invoice;
import org.springframework.stereotype.Service;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Primary;
import java.util.*;

/**
 * 实体[发票] 自定义服务对象
 */
@Slf4j
@Primary
@Service("InvoiceExService")
public class InvoiceExService extends InvoiceServiceImpl {

    @Override
    protected Class currentModelClass() {
        return com.baomidou.mybatisplus.core.toolkit.ReflectionKit.getSuperClassGenericType(this.getClass().getSuperclass(), 1);
    }

    /**
     * 自定义行为[Finish]用户扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public Invoice finish(Invoice et) {
        return super.finish(et);
    }
}


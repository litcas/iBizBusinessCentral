package cn.ibizlab.businesscentral.core.base.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.base.domain.Email;
import cn.ibizlab.businesscentral.core.base.filter.EmailSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface EmailMapper extends BaseMapper<Email>{

    Page<Email> searchDefault(IPage page, @Param("srf") EmailSearchContext context, @Param("ew") Wrapper<Email> wrapper) ;
    @Override
    Email selectById(Serializable id);
    @Override
    int insert(Email entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Email entity);
    @Override
    int update(@Param(Constants.ENTITY) Email entity, @Param("ew") Wrapper<Email> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Email> selectByParentactivityid(@Param("activityid") Serializable activityid) ;

    List<Email> selectByServiceid(@Param("serviceid") Serializable serviceid) ;

    List<Email> selectBySlaid(@Param("slaid") Serializable slaid) ;

    List<Email> selectByTemplateid(@Param("templateid") Serializable templateid) ;

    List<Email> selectByTransactioncurrencyid(@Param("transactioncurrencyid") Serializable transactioncurrencyid) ;

}

package cn.ibizlab.businesscentral.core.sales.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.sales.domain.Quote;
import cn.ibizlab.businesscentral.core.sales.filter.QuoteSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Quote] 服务对象接口
 */
public interface IQuoteService extends IService<Quote>{

    boolean create(Quote et) ;
    void createBatch(List<Quote> list) ;
    boolean update(Quote et) ;
    void updateBatch(List<Quote> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Quote get(String key) ;
    Quote getDraft(Quote et) ;
    Quote active(Quote et) ;
    boolean checkKey(Quote et) ;
    Quote close(Quote et) ;
    Quote genSalesOrder(Quote et) ;
    boolean save(Quote et) ;
    void saveBatch(List<Quote> list) ;
    Quote win(Quote et) ;
    Page<Quote> searchByParentKey(QuoteSearchContext context) ;
    Page<Quote> searchClosed(QuoteSearchContext context) ;
    Page<Quote> searchDefault(QuoteSearchContext context) ;
    Page<Quote> searchDraft(QuoteSearchContext context) ;
    Page<Quote> searchEffective(QuoteSearchContext context) ;
    Page<Quote> searchWin(QuoteSearchContext context) ;
    List<Quote> selectByCampaignid(String campaignid) ;
    void removeByCampaignid(String campaignid) ;
    List<Quote> selectByOpportunityid(String opportunityid) ;
    void removeByOpportunityid(String opportunityid) ;
    List<Quote> selectByPricelevelid(String pricelevelid) ;
    void removeByPricelevelid(String pricelevelid) ;
    List<Quote> selectBySlaid(String slaid) ;
    void removeBySlaid(String slaid) ;
    List<Quote> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Quote> getQuoteByIds(List<String> ids) ;
    List<Quote> getQuoteByEntities(List<Quote> entities) ;
}



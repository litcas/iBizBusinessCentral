package cn.ibizlab.businesscentral.core.finance.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.finance.domain.InvoiceDetail;
import cn.ibizlab.businesscentral.core.finance.filter.InvoiceDetailSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[InvoiceDetail] 服务对象接口
 */
public interface IInvoiceDetailService extends IService<InvoiceDetail>{

    boolean create(InvoiceDetail et) ;
    void createBatch(List<InvoiceDetail> list) ;
    boolean update(InvoiceDetail et) ;
    void updateBatch(List<InvoiceDetail> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    InvoiceDetail get(String key) ;
    InvoiceDetail getDraft(InvoiceDetail et) ;
    boolean checkKey(InvoiceDetail et) ;
    boolean save(InvoiceDetail et) ;
    void saveBatch(List<InvoiceDetail> list) ;
    Page<InvoiceDetail> searchDefault(InvoiceDetailSearchContext context) ;
    List<InvoiceDetail> selectByParentbundleidref(String invoicedetailid) ;
    void removeByParentbundleidref(String invoicedetailid) ;
    List<InvoiceDetail> selectByInvoiceid(String invoiceid) ;
    void removeByInvoiceid(String invoiceid) ;
    List<InvoiceDetail> selectByProductid(String productid) ;
    void removeByProductid(String productid) ;
    List<InvoiceDetail> selectBySalesorderdetailid(String salesorderdetailid) ;
    void removeBySalesorderdetailid(String salesorderdetailid) ;
    List<InvoiceDetail> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    List<InvoiceDetail> selectByUomid(String uomid) ;
    void removeByUomid(String uomid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<InvoiceDetail> getInvoicedetailByIds(List<String> ids) ;
    List<InvoiceDetail> getInvoicedetailByEntities(List<InvoiceDetail> entities) ;
}



package cn.ibizlab.businesscentral.core.scheduling.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.scheduling.domain.FISAccount;
import cn.ibizlab.businesscentral.core.scheduling.filter.FISAccountSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[FISAccount] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.ibizprojectoperations-poejectoperationsapi:ibizprojectoperations-poejectoperationsapi}", contextId = "FISAccount", fallback = FISAccountFallback.class)
public interface FISAccountFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/fisaccounts/select")
    Page<FISAccount> select();


    @RequestMapping(method = RequestMethod.POST, value = "/fisaccounts")
    FISAccount create(@RequestBody FISAccount fisaccount);

    @RequestMapping(method = RequestMethod.POST, value = "/fisaccounts/batch")
    Boolean createBatch(@RequestBody List<FISAccount> fisaccounts);


    @RequestMapping(method = RequestMethod.PUT, value = "/fisaccounts/{accountid}")
    FISAccount update(@PathVariable("accountid") String accountid,@RequestBody FISAccount fisaccount);

    @RequestMapping(method = RequestMethod.PUT, value = "/fisaccounts/batch")
    Boolean updateBatch(@RequestBody List<FISAccount> fisaccounts);


    @RequestMapping(method = RequestMethod.DELETE, value = "/fisaccounts/{accountid}")
    Boolean remove(@PathVariable("accountid") String accountid);

    @RequestMapping(method = RequestMethod.DELETE, value = "/fisaccounts/batch}")
    Boolean removeBatch(@RequestBody Collection<String> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/fisaccounts/{accountid}")
    FISAccount get(@PathVariable("accountid") String accountid);


    @RequestMapping(method = RequestMethod.GET, value = "/fisaccounts/getdraft")
    FISAccount getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/fisaccounts/checkkey")
    Boolean checkKey(@RequestBody FISAccount fisaccount);


    @RequestMapping(method = RequestMethod.POST, value = "/fisaccounts/save")
    Boolean save(@RequestBody FISAccount fisaccount);

    @RequestMapping(method = RequestMethod.POST, value = "/fisaccounts/save")
    Boolean saveBatch(@RequestBody List<FISAccount> fisaccounts);



    @RequestMapping(method = RequestMethod.POST, value = "/fisaccounts/searchdefault")
    Page<FISAccount> searchDefault(@RequestBody FISAccountSearchContext context);


}

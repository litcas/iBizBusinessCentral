package cn.ibizlab.businesscentral.core.service.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.service.domain.Incident;
import cn.ibizlab.businesscentral.core.service.filter.IncidentSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Incident] 服务对象接口
 */
public interface IIncidentService extends IService<Incident>{

    boolean create(Incident et) ;
    void createBatch(List<Incident> list) ;
    boolean update(Incident et) ;
    void updateBatch(List<Incident> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Incident get(String key) ;
    Incident getDraft(Incident et) ;
    Incident active(Incident et) ;
    Incident cancel(Incident et) ;
    boolean checkKey(Incident et) ;
    Incident resolve(Incident et) ;
    boolean save(Incident et) ;
    void saveBatch(List<Incident> list) ;
    Page<Incident> searchByParentKey(IncidentSearchContext context) ;
    Page<Incident> searchCancel(IncidentSearchContext context) ;
    Page<Incident> searchDefault(IncidentSearchContext context) ;
    Page<Incident> searchEffective(IncidentSearchContext context) ;
    Page<Incident> searchResolved(IncidentSearchContext context) ;
    List<Incident> selectByCustomerid(String customerid) ;
    void removeByCustomerid(String customerid) ;
    List<Incident> selectByPrimarycontactid(String contactid) ;
    void removeByPrimarycontactid(String contactid) ;
    List<Incident> selectByResponsiblecontactid(String contactid) ;
    void removeByResponsiblecontactid(String contactid) ;
    List<Incident> selectByContractdetailid(String contractdetailid) ;
    void removeByContractdetailid(String contractdetailid) ;
    List<Incident> selectByContractid(String contractid) ;
    void removeByContractid(String contractid) ;
    List<Incident> selectByEntitlementid(String entitlementid) ;
    void removeByEntitlementid(String entitlementid) ;
    List<Incident> selectByExistingcase(String incidentid) ;
    void removeByExistingcase(String incidentid) ;
    List<Incident> selectByMasterid(String incidentid) ;
    void removeByMasterid(String incidentid) ;
    List<Incident> selectByParentcaseid(String incidentid) ;
    void removeByParentcaseid(String incidentid) ;
    List<Incident> selectByProductid(String productid) ;
    void removeByProductid(String productid) ;
    List<Incident> selectByFirstresponsebykpiid(String slakpiinstanceid) ;
    void removeByFirstresponsebykpiid(String slakpiinstanceid) ;
    List<Incident> selectByResolvebykpiid(String slakpiinstanceid) ;
    void removeByResolvebykpiid(String slakpiinstanceid) ;
    List<Incident> selectBySlaid(String slaid) ;
    void removeBySlaid(String slaid) ;
    List<Incident> selectBySubjectid(String subjectid) ;
    void removeBySubjectid(String subjectid) ;
    List<Incident> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Incident> getIncidentByIds(List<String> ids) ;
    List<Incident> getIncidentByEntities(List<Incident> entities) ;
}



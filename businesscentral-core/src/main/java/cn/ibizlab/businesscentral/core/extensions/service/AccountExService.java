package cn.ibizlab.businesscentral.core.extensions.service;

import cn.ibizlab.businesscentral.core.base.service.impl.AccountServiceImpl;
import cn.ibizlab.businesscentral.core.scheduling.domain.FISAccount;
import cn.ibizlab.businesscentral.core.scheduling.domain.PSAccount;
import cn.ibizlab.businesscentral.core.scheduling.service.IFISAccountService;
import cn.ibizlab.businesscentral.core.scheduling.service.IPSAccountService;
import liquibase.pro.packaged.A;
import lombok.extern.slf4j.Slf4j;
import cn.ibizlab.businesscentral.core.base.domain.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Primary;
import java.util.*;

/**
 * 实体[客户] 自定义服务对象
 */
@Slf4j
@Primary
@Service("AccountExService")
public class AccountExService extends AccountServiceImpl {

    @Override
    protected Class currentModelClass() {
        return com.baomidou.mybatisplus.core.toolkit.ReflectionKit.getSuperClassGenericType(this.getClass().getSuperclass(), 1);
    }

    /**
     * 自定义行为[AddList]用户扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public Account addList(Account et) {
        return super.addList(et);
    }

    @Autowired
    IPSAccountService ipsAccountService ;

    @Autowired
    IFISAccountService ifisAccountService ;

    @Override
    public boolean update(Account et) {
        //更新 account
        boolean bOk = super.update(et);

        //调用服务 project  更新 psaccount
        //调用失败、错误 --> 回滚 account 的更新
//        PSAccount psAccount = new PSAccount() ;
//        psAccount.setAccountid(et.getAccountid());
//        psAccount.setPsaexternalaccountid("pssaexternalaccountid");
//
//        ipsAccountService.update(psAccount) ;

        //调用服务 filed 更新 fisaccount
        //调用失败、错误 --> 回滚 project 的 psaccount 更新 ； 回滚 account 的更新
//        FISAccount fisAccount = new FISAccount() ;
//        fisAccount.setAccountid(et.getAccountid());
//        fisAccount.setFisworkorderinstructions("fisworkorderinstructions");
//
//        ifisAccountService.update(fisAccount) ;

        return bOk ;
    }
}


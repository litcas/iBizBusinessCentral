package cn.ibizlab.businesscentral.core.base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.base.domain.Category;
import cn.ibizlab.businesscentral.core.base.filter.CategorySearchContext;
import cn.ibizlab.businesscentral.core.base.service.ICategoryService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.base.mapper.CategoryMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[类别] 服务对象接口实现
 */
@Slf4j
@Service("CategoryServiceImpl")
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements ICategoryService {


    protected cn.ibizlab.businesscentral.core.base.service.ICategoryService categoryService = this;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(Category et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getCategoryid()),et);
        return true;
    }

    @Override
    public void createBatch(List<Category> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Category et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("categoryid",et.getCategoryid())))
            return false;
        CachedBeanCopier.copy(get(et.getCategoryid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<Category> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Category get(String key) {
        Category et = getById(key);
        if(et==null){
            et=new Category();
            et.setCategoryid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Category getDraft(Category et) {
        return et;
    }

    @Override
    public boolean checkKey(Category et) {
        return (!ObjectUtils.isEmpty(et.getCategoryid()))&&(!Objects.isNull(this.getById(et.getCategoryid())));
    }
    @Override
    @Transactional
    public boolean save(Category et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Category et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<Category> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<Category> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Category> selectByParentcategoryid(String categoryid) {
        return baseMapper.selectByParentcategoryid(categoryid);
    }

    @Override
    public void removeByParentcategoryid(String categoryid) {
        this.remove(new QueryWrapper<Category>().eq("parentcategoryid",categoryid));
    }

	@Override
    public List<Category> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<Category>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<Category> searchDefault(CategorySearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Category> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Category>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Category> getCategoryByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Category> getCategoryByEntities(List<Category> entities) {
        List ids =new ArrayList();
        for(Category entity : entities){
            Serializable id=entity.getCategoryid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}




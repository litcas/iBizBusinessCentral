package cn.ibizlab.businesscentral.core.ou.service.logic;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.businesscentral.core.ou.domain.IBZEmployee;

/**
 * 关系型数据实体[saveDeptMember] 对象
 */
public interface IIBZEmployeesaveDeptMemberLogic {

    void execute(IBZEmployee et) ;

}

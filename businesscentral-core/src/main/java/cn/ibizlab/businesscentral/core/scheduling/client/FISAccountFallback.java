package cn.ibizlab.businesscentral.core.scheduling.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.scheduling.domain.FISAccount;
import cn.ibizlab.businesscentral.core.scheduling.filter.FISAccountSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[FISAccount] 服务对象接口
 */
@Component
public class FISAccountFallback implements FISAccountFeignClient{

    public Page<FISAccount> select(){
            return null;
     }

    public FISAccount create(FISAccount fisaccount){
            return null;
     }
    public Boolean createBatch(List<FISAccount> fisaccounts){
            return false;
     }

    public FISAccount update(String accountid, FISAccount fisaccount){
            return null;
     }
    public Boolean updateBatch(List<FISAccount> fisaccounts){
            return false;
     }


    public Boolean remove(String accountid){
            return false;
     }
    public Boolean removeBatch(Collection<String> idList){
            return false;
     }

    public FISAccount get(String accountid){
            return null;
     }


    public FISAccount getDraft(){
            return null;
    }



    public Boolean checkKey(FISAccount fisaccount){
            return false;
     }


    public Boolean save(FISAccount fisaccount){
            return false;
     }
    public Boolean saveBatch(List<FISAccount> fisaccounts){
            return false;
     }

    public Page<FISAccount> searchDefault(FISAccountSearchContext context){
            return null;
     }


}

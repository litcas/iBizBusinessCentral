package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.Sla;
import cn.ibizlab.businesscentral.core.base.filter.SlaSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Sla] 服务对象接口
 */
public interface ISlaService extends IService<Sla>{

    boolean create(Sla et) ;
    void createBatch(List<Sla> list) ;
    boolean update(Sla et) ;
    void updateBatch(List<Sla> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Sla get(String key) ;
    Sla getDraft(Sla et) ;
    boolean checkKey(Sla et) ;
    boolean save(Sla et) ;
    void saveBatch(List<Sla> list) ;
    Page<Sla> searchDefault(SlaSearchContext context) ;
    List<Sla> selectByOwningbusinessunit(String businessunitid) ;
    void removeByOwningbusinessunit(String businessunitid) ;
    List<Sla> selectByBusinesshoursid(String calendarid) ;
    void removeByBusinesshoursid(String calendarid) ;
    List<Sla> selectByOwningteam(String teamid) ;
    void removeByOwningteam(String teamid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Sla> getSlaByIds(List<String> ids) ;
    List<Sla> getSlaByEntities(List<Sla> entities) ;
}



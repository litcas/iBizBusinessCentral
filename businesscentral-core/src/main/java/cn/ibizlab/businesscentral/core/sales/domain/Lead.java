package cn.ibizlab.businesscentral.core.sales.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[潜在顾客]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "LEAD",resultMap = "LeadResultMap")
public class Lead extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 地址 1: 传真
     */
    @DEField(name = "address1_fax")
    @TableField(value = "address1_fax")
    @JSONField(name = "address1_fax")
    @JsonProperty("address1_fax")
    private String address1Fax;
    /**
     * 地址 2: UTC 时差
     */
    @DEField(name = "address2_utcoffset")
    @TableField(value = "address2_utcoffset")
    @JSONField(name = "address2_utcoffset")
    @JsonProperty("address2_utcoffset")
    private Integer address2Utcoffset;
    /**
     * 职务
     */
    @TableField(value = "jobtitle")
    @JSONField(name = "jobtitle")
    @JsonProperty("jobtitle")
    private String jobtitle;
    /**
     * 地址 2: 国家/地区
     */
    @DEField(name = "address2_country")
    @TableField(value = "address2_country")
    @JSONField(name = "address2_country")
    @JsonProperty("address2_country")
    private String address2Country;
    /**
     * 预算金额
     */
    @TableField(value = "budgetamount")
    @JSONField(name = "budgetamount")
    @JsonProperty("budgetamount")
    private BigDecimal budgetamount;
    /**
     * 地址 2: 传真
     */
    @DEField(name = "address2_fax")
    @TableField(value = "address2_fax")
    @JSONField(name = "address2_fax")
    @JsonProperty("address2_fax")
    private String address2Fax;
    /**
     * 暂候时间(分钟)
     */
    @TableField(value = "onholdtime")
    @JSONField(name = "onholdtime")
    @JsonProperty("onholdtime")
    private Integer onholdtime;
    /**
     * 姓
     */
    @TableField(value = "lastname")
    @JSONField(name = "lastname")
    @JsonProperty("lastname")
    private String lastname;
    /**
     * 地址 1: 电话 2
     */
    @DEField(name = "address1_telephone2")
    @TableField(value = "address1_telephone2")
    @JSONField(name = "address1_telephone2")
    @JsonProperty("address1_telephone2")
    private String address1Telephone2;
    /**
     * 省/直辖市/自治区
     */
    @DEField(name = "address1_stateorprovince")
    @TableField(value = "address1_stateorprovince")
    @JSONField(name = "address1_stateorprovince")
    @JsonProperty("address1_stateorprovince")
    private String address1Stateorprovince;
    /**
     * 预计值(已弃用)
     */
    @TableField(value = "estimatedvalue")
    @JSONField(name = "estimatedvalue")
    @JsonProperty("estimatedvalue")
    private Double estimatedvalue;
    /**
     * 潜在顾客
     */
    @DEField(isKeyField=true)
    @TableId(value= "leadid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "leadid")
    @JsonProperty("leadid")
    private String leadid;
    /**
     * 地址 1: 经度
     */
    @DEField(name = "address1_longitude")
    @TableField(value = "address1_longitude")
    @JSONField(name = "address1_longitude")
    @JsonProperty("address1_longitude")
    private Double address1Longitude;
    /**
     * 街道 1
     */
    @DEField(name = "address1_line1")
    @TableField(value = "address1_line1")
    @JSONField(name = "address1_line1")
    @JsonProperty("address1_line1")
    private String address1Line1;
    /**
     * 等级
     */
    @TableField(value = "leadqualitycode")
    @JSONField(name = "leadqualitycode")
    @JsonProperty("leadqualitycode")
    private String leadqualitycode;
    /**
     * 不允许电话联络
     */
    @DEField(defaultValue = "0")
    @TableField(value = "donotphone")
    @JSONField(name = "donotphone")
    @JsonProperty("donotphone")
    private Integer donotphone;
    /**
     * 汇率
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * EntityImage_URL
     */
    @DEField(name = "entityimage_url")
    @TableField(value = "entityimage_url")
    @JSONField(name = "entityimage_url")
    @JsonProperty("entityimage_url")
    private String entityimageUrl;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 地址 2: 省/市/自治区
     */
    @DEField(name = "address2_stateorprovince")
    @TableField(value = "address2_stateorprovince")
    @JSONField(name = "address2_stateorprovince")
    @JsonProperty("address2_stateorprovince")
    private String address2Stateorprovince;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 员工数
     */
    @TableField(value = "numberofemployees")
    @JSONField(name = "numberofemployees")
    @JsonProperty("numberofemployees")
    private Integer numberofemployees;
    /**
     * 市/县
     */
    @DEField(name = "address1_city")
    @TableField(value = "address1_city")
    @JSONField(name = "address1_city")
    @JsonProperty("address1_city")
    private String address1City;
    /**
     * EntityImageId
     */
    @TableField(value = "entityimageid")
    @JSONField(name = "entityimageid")
    @JsonProperty("entityimageid")
    private String entityimageid;
    /**
     * 地址 2: 街道 3
     */
    @DEField(name = "address2_line3")
    @TableField(value = "address2_line3")
    @JSONField(name = "address2_line3")
    @JsonProperty("address2_line3")
    private String address2Line3;
    /**
     * 状态描述
     */
    @DEField(defaultValue = "1")
    @TableField(value = "statuscode")
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;
    /**
     * 地址 2: 街道 1
     */
    @DEField(name = "address2_line1")
    @TableField(value = "address2_line1")
    @JSONField(name = "address2_line1")
    @JsonProperty("address2_line1")
    private String address2Line1;
    /**
     * 客户
     */
    @TableField(value = "customerid")
    @JSONField(name = "customerid")
    @JsonProperty("customerid")
    private String customerid;
    /**
     * 公司名称
     */
    @TableField(value = "companyname")
    @JSONField(name = "companyname")
    @JsonProperty("companyname")
    private String companyname;
    /**
     * Version Number
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 资格注释
     */
    @TableField(value = "qualificationcomments")
    @JSONField(name = "qualificationcomments")
    @JsonProperty("qualificationcomments")
    private String qualificationcomments;
    /**
     * 地址 2: 名称
     */
    @DEField(name = "address2_name")
    @TableField(value = "address2_name")
    @JSONField(name = "address2_name")
    @JsonProperty("address2_name")
    private String address2Name;
    /**
     * 电子邮件
     */
    @TableField(value = "emailaddress1")
    @JSONField(name = "emailaddress1")
    @JsonProperty("emailaddress1")
    private String emailaddress1;
    /**
     * 跟踪电子邮件活动
     */
    @DEField(defaultValue = "1")
    @TableField(value = "followemail")
    @JSONField(name = "followemail")
    @JsonProperty("followemail")
    private Integer followemail;
    /**
     * 国家/地区
     */
    @DEField(name = "address1_country")
    @TableField(value = "address1_country")
    @JSONField(name = "address1_country")
    @JsonProperty("address1_country")
    private String address1Country;
    /**
     * 网站
     */
    @TableField(value = "websiteurl")
    @JSONField(name = "websiteurl")
    @JsonProperty("websiteurl")
    private String websiteurl;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 街道 3
     */
    @DEField(name = "address1_line3")
    @TableField(value = "address1_line3")
    @JSONField(name = "address1_line3")
    @JsonProperty("address1_line3")
    private String address1Line3;
    /**
     * 地址 2: ID
     */
    @DEField(name = "address2_addressid")
    @TableField(value = "address2_addressid")
    @JSONField(name = "address2_addressid")
    @JsonProperty("address2_addressid")
    private String address2Addressid;
    /**
     * 地址 1
     */
    @DEField(name = "address1_composite")
    @TableField(value = "address1_composite")
    @JSONField(name = "address1_composite")
    @JsonProperty("address1_composite")
    private String address1Composite;
    /**
     * 主题
     */
    @TableField(value = "subject")
    @JSONField(name = "subject")
    @JsonProperty("subject")
    private String subject;
    /**
     * 地址 1: UTC 时差
     */
    @DEField(name = "address1_utcoffset")
    @TableField(value = "address1_utcoffset")
    @JSONField(name = "address1_utcoffset")
    @JsonProperty("address1_utcoffset")
    private Integer address1Utcoffset;
    /**
     * 预算金额 (Base)
     */
    @DEField(name = "budgetamount_base")
    @TableField(value = "budgetamount_base")
    @JSONField(name = "budgetamount_base")
    @JsonProperty("budgetamount_base")
    private BigDecimal budgetamountBase;
    /**
     * 地址 1: 地址类型
     */
    @DEField(name = "address1_addresstypecode")
    @TableField(value = "address1_addresstypecode")
    @JSONField(name = "address1_addresstypecode")
    @JsonProperty("address1_addresstypecode")
    private String address1Addresstypecode;
    /**
     * 地址 2: 电话 3
     */
    @DEField(name = "address2_telephone3")
    @TableField(value = "address2_telephone3")
    @JSONField(name = "address2_telephone3")
    @JsonProperty("address2_telephone3")
    private String address2Telephone3;
    /**
     * 客户
     */
    @TableField(value = "customername")
    @JSONField(name = "customername")
    @JsonProperty("customername")
    private String customername;
    /**
     * 潜在客户
     */
    @TableField(value = "masterleadname")
    @JSONField(name = "masterleadname")
    @JsonProperty("masterleadname")
    private String masterleadname;
    /**
     * 评估相符程度
     */
    @DEField(defaultValue = "0")
    @TableField(value = "evaluatefit")
    @JSONField(name = "evaluatefit")
    @JsonProperty("evaluatefit")
    private Integer evaluatefit;
    /**
     * 地址 1: 纬度
     */
    @DEField(name = "address1_latitude")
    @TableField(value = "address1_latitude")
    @JSONField(name = "address1_latitude")
    @JsonProperty("address1_latitude")
    private Double address1Latitude;
    /**
     * 地址 1: 电话 3
     */
    @DEField(name = "address1_telephone3")
    @TableField(value = "address1_telephone3")
    @JSONField(name = "address1_telephone3")
    @JsonProperty("address1_telephone3")
    private String address1Telephone3;
    /**
     * 姓名
     */
    @TableField(value = "fullname")
    @JSONField(name = "fullname")
    @JsonProperty("fullname")
    private String fullname;
    /**
     * 预计值 (Base)
     */
    @DEField(name = "estimatedamount_base")
    @TableField(value = "estimatedamount_base")
    @JSONField(name = "estimatedamount_base")
    @JsonProperty("estimatedamount_base")
    private BigDecimal estimatedamountBase;
    /**
     * 预算
     */
    @TableField(value = "budgetstatus")
    @JSONField(name = "budgetstatus")
    @JsonProperty("budgetstatus")
    private String budgetstatus;
    /**
     * 行业
     */
    @TableField(value = "industrycode")
    @JSONField(name = "industrycode")
    @JsonProperty("industrycode")
    private String industrycode;
    /**
     * 街道 2
     */
    @DEField(name = "address1_line2")
    @TableField(value = "address1_line2")
    @JSONField(name = "address1_line2")
    @JsonProperty("address1_line2")
    private String address1Line2;
    /**
     * 负责人
     */
    @TableField(value = "ownername")
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;
    /**
     * 初始通信
     */
    @TableField(value = "initialcommunication")
    @JSONField(name = "initialcommunication")
    @JsonProperty("initialcommunication")
    private String initialcommunication;
    /**
     * 地址 1: 邮政信箱
     */
    @DEField(name = "address1_postofficebox")
    @TableField(value = "address1_postofficebox")
    @JSONField(name = "address1_postofficebox")
    @JsonProperty("address1_postofficebox")
    private String address1Postofficebox;
    /**
     * 地址 1: 电话 1
     */
    @DEField(name = "address1_telephone1")
    @TableField(value = "address1_telephone1")
    @JSONField(name = "address1_telephone1")
    @JsonProperty("address1_telephone1")
    private String address1Telephone1;
    /**
     * 年收入
     */
    @TableField(value = "revenue")
    @JSONField(name = "revenue")
    @JsonProperty("revenue")
    private BigDecimal revenue;
    /**
     * 地址 2: 县
     */
    @DEField(name = "address2_county")
    @TableField(value = "address2_county")
    @JSONField(name = "address2_county")
    @JsonProperty("address2_county")
    private String address2County;
    /**
     * Stage Id
     */
    @TableField(value = "stageid")
    @JSONField(name = "stageid")
    @JsonProperty("stageid")
    private String stageid;
    /**
     * 地址 1: 送货方式
     */
    @DEField(name = "address1_shippingmethodcode")
    @TableField(value = "address1_shippingmethodcode")
    @JSONField(name = "address1_shippingmethodcode")
    @JsonProperty("address1_shippingmethodcode")
    private String address1Shippingmethodcode;
    /**
     * 预计值
     */
    @TableField(value = "estimatedamount")
    @JSONField(name = "estimatedamount")
    @JsonProperty("estimatedamount")
    private BigDecimal estimatedamount;
    /**
     * 地址 1: 县
     */
    @DEField(name = "address1_county")
    @TableField(value = "address1_county")
    @JSONField(name = "address1_county")
    @JsonProperty("address1_county")
    private String address1County;
    /**
     * UTC Conversion Time Zone Code
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * 上一暂候时间
     */
    @TableField(value = "lastonholdtime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "lastonholdtime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("lastonholdtime")
    private Timestamp lastonholdtime;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * TeamsFollowed
     */
    @TableField(value = "teamsfollowed")
    @JSONField(name = "teamsfollowed")
    @JsonProperty("teamsfollowed")
    private Integer teamsfollowed;
    /**
     * 称呼
     */
    @TableField(value = "salutation")
    @JSONField(name = "salutation")
    @JsonProperty("salutation")
    private String salutation;
    /**
     * 地址 2: 送货方式
     */
    @DEField(name = "address2_shippingmethodcode")
    @TableField(value = "address2_shippingmethodcode")
    @JSONField(name = "address2_shippingmethodcode")
    @JsonProperty("address2_shippingmethodcode")
    private String address2Shippingmethodcode;
    /**
     * 地址 2: 纬度
     */
    @DEField(name = "address2_latitude")
    @TableField(value = "address2_latitude")
    @JSONField(name = "address2_latitude")
    @JsonProperty("address2_latitude")
    private Double address2Latitude;
    /**
     * 参与工作流
     */
    @DEField(defaultValue = "0")
    @TableField(value = "participatesinworkflow")
    @JSONField(name = "participatesinworkflow")
    @JsonProperty("participatesinworkflow")
    private Integer participatesinworkflow;
    /**
     * 地址 2
     */
    @DEField(name = "address2_composite")
    @TableField(value = "address2_composite")
    @JSONField(name = "address2_composite")
    @JsonProperty("address2_composite")
    private String address2Composite;
    /**
     * 销售阶段
     */
    @TableField(value = "salesstage")
    @JSONField(name = "salesstage")
    @JsonProperty("salesstage")
    private String salesstage;
    /**
     * 不允许使用邮件
     */
    @DEField(defaultValue = "0")
    @TableField(value = "donotpostalmail")
    @JSONField(name = "donotpostalmail")
    @JsonProperty("donotpostalmail")
    private Integer donotpostalmail;
    /**
     * 年收入 (Base)
     */
    @DEField(name = "revenue_base")
    @TableField(value = "revenue_base")
    @JSONField(name = "revenue_base")
    @JsonProperty("revenue_base")
    private BigDecimal revenueBase;
    /**
     * Record Created On
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 安排跟进(潜在客户)
     */
    @DEField(name = "schedulefollowup_prospect")
    @TableField(value = "schedulefollowup_prospect")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "schedulefollowup_prospect" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("schedulefollowup_prospect")
    private Timestamp schedulefollowupProspect;
    /**
     * 地址 2: 市/县
     */
    @DEField(name = "address2_city")
    @TableField(value = "address2_city")
    @JSONField(name = "address2_city")
    @JsonProperty("address2_city")
    private String address2City;
    /**
     * 商务电话
     */
    @TableField(value = "telephone1")
    @JSONField(name = "telephone1")
    @JsonProperty("telephone1")
    private String telephone1;
    /**
     * 是否私有
     */
    @DEField(defaultValue = "0")
    @TableField(value = "private")
    @JSONField(name = "ibizprivate")
    @JsonProperty("ibizprivate")
    private Integer ibizprivate;
    /**
     * 移动电话
     */
    @TableField(value = "mobilephone")
    @JSONField(name = "mobilephone")
    @JsonProperty("mobilephone")
    private String mobilephone;
    /**
     * 需求
     */
    @TableField(value = "need")
    @JSONField(name = "need")
    @JsonProperty("need")
    private String need;
    /**
     * 优先级
     */
    @TableField(value = "prioritycode")
    @JSONField(name = "prioritycode")
    @JsonProperty("prioritycode")
    private String prioritycode;
    /**
     * 地址 1: ID
     */
    @DEField(name = "address1_addressid")
    @TableField(value = "address1_addressid")
    @JSONField(name = "address1_addressid")
    @JsonProperty("address1_addressid")
    private String address1Addressid;
    /**
     * 销售阶段代码
     */
    @TableField(value = "salesstagecode")
    @JSONField(name = "salesstagecode")
    @JsonProperty("salesstagecode")
    private String salesstagecode;
    /**
     * Import Sequence Number
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * 地址 2: 电话 1
     */
    @DEField(name = "address2_telephone1")
    @TableField(value = "address2_telephone1")
    @JSONField(name = "address2_telephone1")
    @JsonProperty("address2_telephone1")
    private String address2Telephone1;
    /**
     * 负责人
     */
    @TableField(value = "ownerid")
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;
    /**
     * 中间名
     */
    @TableField(value = "middlename")
    @JSONField(name = "middlename")
    @JsonProperty("middlename")
    private String middlename;
    /**
     * 住宅电话
     */
    @TableField(value = "telephone2")
    @JSONField(name = "telephone2")
    @JsonProperty("telephone2")
    private String telephone2;
    /**
     * 购买时间范围
     */
    @TableField(value = "purchasetimeframe")
    @JSONField(name = "purchasetimeframe")
    @JsonProperty("purchasetimeframe")
    private String purchasetimeframe;
    /**
     * 潜在顾客来源
     */
    @TableField(value = "leadsourcecode")
    @JSONField(name = "leadsourcecode")
    @JsonProperty("leadsourcecode")
    private String leadsourcecode;
    /**
     * EntityImage_Timestamp
     */
    @DEField(name = "entityimage_timestamp")
    @TableField(value = "entityimage_timestamp")
    @JSONField(name = "entityimage_timestamp")
    @JsonProperty("entityimage_timestamp")
    private BigInteger entityimageTimestamp;
    /**
     * 首选联系方式
     */
    @TableField(value = "preferredcontactmethodcode")
    @JSONField(name = "preferredcontactmethodcode")
    @JsonProperty("preferredcontactmethodcode")
    private String preferredcontactmethodcode;
    /**
     * 实体图像
     */
    @TableField(value = "entityimage")
    @JSONField(name = "entityimage")
    @JsonProperty("entityimage")
    private String entityimage;
    /**
     * 地址 2: 电话 2
     */
    @DEField(name = "address2_telephone2")
    @TableField(value = "address2_telephone2")
    @JSONField(name = "address2_telephone2")
    @JsonProperty("address2_telephone2")
    private String address2Telephone2;
    /**
     * 市场营销资料
     */
    @DEField(defaultValue = "0")
    @TableField(value = "donotsendmm")
    @JSONField(name = "donotsendmm")
    @JsonProperty("donotsendmm")
    private Integer donotsendmm;
    /**
     * 采购程序
     */
    @TableField(value = "purchaseprocess")
    @JSONField(name = "purchaseprocess")
    @JsonProperty("purchaseprocess")
    private String purchaseprocess;
    /**
     * 不允许使用批量电子邮件
     */
    @DEField(defaultValue = "0")
    @TableField(value = "donotbulkemail")
    @JSONField(name = "donotbulkemail")
    @JsonProperty("donotbulkemail")
    private Integer donotbulkemail;
    /**
     * 行业编码
     */
    @TableField(value = "sic")
    @JSONField(name = "sic")
    @JsonProperty("sic")
    private String sic;
    /**
     * 联系人
     */
    @TableField(value = "contactname")
    @JSONField(name = "contactname")
    @JsonProperty("contactname")
    private String contactname;
    /**
     * 不允许使用电子邮件
     */
    @DEField(defaultValue = "0")
    @TableField(value = "donotemail")
    @JSONField(name = "donotemail")
    @JsonProperty("donotemail")
    private Integer donotemail;
    /**
     * 地址 2: 经度
     */
    @DEField(name = "address2_longitude")
    @TableField(value = "address2_longitude")
    @JSONField(name = "address2_longitude")
    @JsonProperty("address2_longitude")
    private Double address2Longitude;
    /**
     * 确认有兴趣
     */
    @DEField(defaultValue = "0")
    @TableField(value = "confirminterest")
    @JSONField(name = "confirminterest")
    @JsonProperty("confirminterest")
    private Integer confirminterest;
    /**
     * 负责人类型
     */
    @TableField(value = "ownertype")
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;
    /**
     * 地址 2: 邮政信箱
     */
    @DEField(name = "address2_postofficebox")
    @TableField(value = "address2_postofficebox")
    @JSONField(name = "address2_postofficebox")
    @JsonProperty("address2_postofficebox")
    private String address2Postofficebox;
    /**
     * 状态
     */
    @DEField(defaultValue = "0")
    @TableField(value = "statecode")
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;
    /**
     * 自动创建
     */
    @DEField(defaultValue = "0")
    @TableField(value = "autocreate")
    @JSONField(name = "autocreate")
    @JsonProperty("autocreate")
    private Integer autocreate;
    /**
     * 地址 1: 名称
     */
    @DEField(name = "address1_name")
    @TableField(value = "address1_name")
    @JSONField(name = "address1_name")
    @JsonProperty("address1_name")
    private String address1Name;
    /**
     * Time Zone Rule Version Number
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * 上次市场活动日期
     */
    @TableField(value = "lastusedincampaign")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "lastusedincampaign" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("lastusedincampaign")
    private Timestamp lastusedincampaign;
    /**
     * 预计结束日期
     */
    @TableField(value = "estimatedclosedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "estimatedclosedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("estimatedclosedate")
    private Timestamp estimatedclosedate;
    /**
     * 地址 2: 街道 2
     */
    @DEField(name = "address2_line2")
    @TableField(value = "address2_line2")
    @JSONField(name = "address2_line2")
    @JsonProperty("address2_line2")
    private String address2Line2;
    /**
     * 电子邮件地址 3
     */
    @TableField(value = "emailaddress3")
    @JSONField(name = "emailaddress3")
    @JsonProperty("emailaddress3")
    private String emailaddress3;
    /**
     * 寻呼机
     */
    @TableField(value = "pager")
    @JSONField(name = "pager")
    @JsonProperty("pager")
    private String pager;
    /**
     * 地址 2: UPS 区域
     */
    @DEField(name = "address2_upszone")
    @TableField(value = "address2_upszone")
    @JSONField(name = "address2_upszone")
    @JsonProperty("address2_upszone")
    private String address2Upszone;
    /**
     * Traversed Path
     */
    @TableField(value = "traversedpath")
    @JSONField(name = "traversedpath")
    @JsonProperty("traversedpath")
    private String traversedpath;
    /**
     * 传真
     */
    @TableField(value = "fax")
    @JSONField(name = "fax")
    @JsonProperty("fax")
    private String fax;
    /**
     * 安排跟进(授予资格)
     */
    @DEField(name = "schedulefollowup_qualify")
    @TableField(value = "schedulefollowup_qualify")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "schedulefollowup_qualify" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("schedulefollowup_qualify")
    private Timestamp schedulefollowupQualify;
    /**
     * 其他电话
     */
    @TableField(value = "telephone3")
    @JSONField(name = "telephone3")
    @JsonProperty("telephone3")
    private String telephone3;
    /**
     * Process Id
     */
    @TableField(value = "processid")
    @JSONField(name = "processid")
    @JsonProperty("processid")
    private String processid;
    /**
     * 电子邮件地址 2
     */
    @TableField(value = "emailaddress2")
    @JSONField(name = "emailaddress2")
    @JsonProperty("emailaddress2")
    private String emailaddress2;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 决策者?
     */
    @DEField(defaultValue = "0")
    @TableField(value = "decisionmaker")
    @JSONField(name = "decisionmaker")
    @JsonProperty("decisionmaker")
    private Integer decisionmaker;
    /**
     * 邮政编码
     */
    @DEField(name = "address1_postalcode")
    @TableField(value = "address1_postalcode")
    @JSONField(name = "address1_postalcode")
    @JsonProperty("address1_postalcode")
    private String address1Postalcode;
    /**
     * 地址 2: 邮政编码
     */
    @DEField(name = "address2_postalcode")
    @TableField(value = "address2_postalcode")
    @JSONField(name = "address2_postalcode")
    @JsonProperty("address2_postalcode")
    private String address2Postalcode;
    /**
     * 不允许使用传真
     */
    @DEField(defaultValue = "0")
    @TableField(value = "donotfax")
    @JSONField(name = "donotfax")
    @JsonProperty("donotfax")
    private Integer donotfax;
    /**
     * 客户类型
     */
    @TableField(value = "customertype")
    @JSONField(name = "customertype")
    @JsonProperty("customertype")
    private String customertype;
    /**
     * 名
     */
    @TableField(value = "firstname")
    @JSONField(name = "firstname")
    @JsonProperty("firstname")
    private String firstname;
    /**
     * 合并
     */
    @DEField(defaultValue = "0")
    @TableField(value = "merged")
    @JSONField(name = "merged")
    @JsonProperty("merged")
    private Integer merged;
    /**
     * 地址 2: 地址类型
     */
    @DEField(name = "address2_addresstypecode")
    @TableField(value = "address2_addresstypecode")
    @JSONField(name = "address2_addresstypecode")
    @JsonProperty("address2_addresstypecode")
    private String address2Addresstypecode;
    /**
     * 地址 1: UPS 区域
     */
    @DEField(name = "address1_upszone")
    @TableField(value = "address1_upszone")
    @JSONField(name = "address1_upszone")
    @JsonProperty("address1_upszone")
    private String address1Upszone;
    /**
     * SLA
     */
    @TableField(value = "slaname")
    @JSONField(name = "slaname")
    @JsonProperty("slaname")
    private String slaname;
    /**
     * 相关市场活动响应
     */
    @TableField(value = "relatedobjectname")
    @JSONField(name = "relatedobjectname")
    @JsonProperty("relatedobjectname")
    private String relatedobjectname;
    /**
     * 货币
     */
    @TableField(value = "currencyname")
    @JSONField(name = "currencyname")
    @JsonProperty("currencyname")
    private String currencyname;
    /**
     * 原始案例
     */
    @TableField(value = "originatingcasename")
    @JSONField(name = "originatingcasename")
    @JsonProperty("originatingcasename")
    private String originatingcasename;
    /**
     * 源市场活动
     */
    @TableField(value = "campaignname")
    @JSONField(name = "campaignname")
    @JsonProperty("campaignname")
    private String campaignname;
    /**
     * 识别客户
     */
    @TableField(value = "parentaccountname")
    @JSONField(name = "parentaccountname")
    @JsonProperty("parentaccountname")
    private String parentaccountname;
    /**
     * 识别联系人
     */
    @TableField(value = "parentcontactname")
    @JSONField(name = "parentcontactname")
    @JsonProperty("parentcontactname")
    private String parentcontactname;
    /**
     * 授予商机资格
     */
    @TableField(value = "qualifyingopportunityname")
    @JSONField(name = "qualifyingopportunityname")
    @JsonProperty("qualifyingopportunityname")
    private String qualifyingopportunityname;
    /**
     * 授予商机资格
     */
    @TableField(value = "qualifyingopportunityid")
    @JSONField(name = "qualifyingopportunityid")
    @JsonProperty("qualifyingopportunityid")
    private String qualifyingopportunityid;
    /**
     * SLA
     */
    @TableField(value = "slaid")
    @JSONField(name = "slaid")
    @JsonProperty("slaid")
    private String slaid;
    /**
     * 源市场活动
     */
    @TableField(value = "campaignid")
    @JSONField(name = "campaignid")
    @JsonProperty("campaignid")
    private String campaignid;
    /**
     * 相关市场活动响应
     */
    @TableField(value = "relatedobjectid")
    @JSONField(name = "relatedobjectid")
    @JsonProperty("relatedobjectid")
    private String relatedobjectid;
    /**
     * 货币
     */
    @TableField(value = "transactioncurrencyid")
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;
    /**
     * 潜在顾客的上级单位
     */
    @TableField(value = "parentaccountid")
    @JSONField(name = "parentaccountid")
    @JsonProperty("parentaccountid")
    private String parentaccountid;
    /**
     * 原始案例
     */
    @TableField(value = "originatingcaseid")
    @JSONField(name = "originatingcaseid")
    @JsonProperty("originatingcaseid")
    private String originatingcaseid;
    /**
     * 潜在顾客的上司
     */
    @TableField(value = "parentcontactid")
    @JSONField(name = "parentcontactid")
    @JsonProperty("parentcontactid")
    private String parentcontactid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Account parentaccount;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.marketing.domain.CampaignResponse relatedobject;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.marketing.domain.Campaign campaign;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Contact parentcontact;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.service.domain.Incident originatingcase;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.sales.domain.Opportunity qualifyingopportunity;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Sla sla;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency;



    /**
     * 设置 [地址 1: 传真]
     */
    public void setAddress1Fax(String address1Fax){
        this.address1Fax = address1Fax ;
        this.modify("address1_fax",address1Fax);
    }

    /**
     * 设置 [地址 2: UTC 时差]
     */
    public void setAddress2Utcoffset(Integer address2Utcoffset){
        this.address2Utcoffset = address2Utcoffset ;
        this.modify("address2_utcoffset",address2Utcoffset);
    }

    /**
     * 设置 [职务]
     */
    public void setJobtitle(String jobtitle){
        this.jobtitle = jobtitle ;
        this.modify("jobtitle",jobtitle);
    }

    /**
     * 设置 [地址 2: 国家/地区]
     */
    public void setAddress2Country(String address2Country){
        this.address2Country = address2Country ;
        this.modify("address2_country",address2Country);
    }

    /**
     * 设置 [预算金额]
     */
    public void setBudgetamount(BigDecimal budgetamount){
        this.budgetamount = budgetamount ;
        this.modify("budgetamount",budgetamount);
    }

    /**
     * 设置 [地址 2: 传真]
     */
    public void setAddress2Fax(String address2Fax){
        this.address2Fax = address2Fax ;
        this.modify("address2_fax",address2Fax);
    }

    /**
     * 设置 [暂候时间(分钟)]
     */
    public void setOnholdtime(Integer onholdtime){
        this.onholdtime = onholdtime ;
        this.modify("onholdtime",onholdtime);
    }

    /**
     * 设置 [姓]
     */
    public void setLastname(String lastname){
        this.lastname = lastname ;
        this.modify("lastname",lastname);
    }

    /**
     * 设置 [地址 1: 电话 2]
     */
    public void setAddress1Telephone2(String address1Telephone2){
        this.address1Telephone2 = address1Telephone2 ;
        this.modify("address1_telephone2",address1Telephone2);
    }

    /**
     * 设置 [省/直辖市/自治区]
     */
    public void setAddress1Stateorprovince(String address1Stateorprovince){
        this.address1Stateorprovince = address1Stateorprovince ;
        this.modify("address1_stateorprovince",address1Stateorprovince);
    }

    /**
     * 设置 [预计值(已弃用)]
     */
    public void setEstimatedvalue(Double estimatedvalue){
        this.estimatedvalue = estimatedvalue ;
        this.modify("estimatedvalue",estimatedvalue);
    }

    /**
     * 设置 [地址 1: 经度]
     */
    public void setAddress1Longitude(Double address1Longitude){
        this.address1Longitude = address1Longitude ;
        this.modify("address1_longitude",address1Longitude);
    }

    /**
     * 设置 [街道 1]
     */
    public void setAddress1Line1(String address1Line1){
        this.address1Line1 = address1Line1 ;
        this.modify("address1_line1",address1Line1);
    }

    /**
     * 设置 [等级]
     */
    public void setLeadqualitycode(String leadqualitycode){
        this.leadqualitycode = leadqualitycode ;
        this.modify("leadqualitycode",leadqualitycode);
    }

    /**
     * 设置 [不允许电话联络]
     */
    public void setDonotphone(Integer donotphone){
        this.donotphone = donotphone ;
        this.modify("donotphone",donotphone);
    }

    /**
     * 设置 [汇率]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [EntityImage_URL]
     */
    public void setEntityimageUrl(String entityimageUrl){
        this.entityimageUrl = entityimageUrl ;
        this.modify("entityimage_url",entityimageUrl);
    }

    /**
     * 设置 [地址 2: 省/市/自治区]
     */
    public void setAddress2Stateorprovince(String address2Stateorprovince){
        this.address2Stateorprovince = address2Stateorprovince ;
        this.modify("address2_stateorprovince",address2Stateorprovince);
    }

    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [员工数]
     */
    public void setNumberofemployees(Integer numberofemployees){
        this.numberofemployees = numberofemployees ;
        this.modify("numberofemployees",numberofemployees);
    }

    /**
     * 设置 [市/县]
     */
    public void setAddress1City(String address1City){
        this.address1City = address1City ;
        this.modify("address1_city",address1City);
    }

    /**
     * 设置 [EntityImageId]
     */
    public void setEntityimageid(String entityimageid){
        this.entityimageid = entityimageid ;
        this.modify("entityimageid",entityimageid);
    }

    /**
     * 设置 [地址 2: 街道 3]
     */
    public void setAddress2Line3(String address2Line3){
        this.address2Line3 = address2Line3 ;
        this.modify("address2_line3",address2Line3);
    }

    /**
     * 设置 [状态描述]
     */
    public void setStatuscode(Integer statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [地址 2: 街道 1]
     */
    public void setAddress2Line1(String address2Line1){
        this.address2Line1 = address2Line1 ;
        this.modify("address2_line1",address2Line1);
    }

    /**
     * 设置 [客户]
     */
    public void setCustomerid(String customerid){
        this.customerid = customerid ;
        this.modify("customerid",customerid);
    }

    /**
     * 设置 [公司名称]
     */
    public void setCompanyname(String companyname){
        this.companyname = companyname ;
        this.modify("companyname",companyname);
    }

    /**
     * 设置 [Version Number]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [资格注释]
     */
    public void setQualificationcomments(String qualificationcomments){
        this.qualificationcomments = qualificationcomments ;
        this.modify("qualificationcomments",qualificationcomments);
    }

    /**
     * 设置 [地址 2: 名称]
     */
    public void setAddress2Name(String address2Name){
        this.address2Name = address2Name ;
        this.modify("address2_name",address2Name);
    }

    /**
     * 设置 [电子邮件]
     */
    public void setEmailaddress1(String emailaddress1){
        this.emailaddress1 = emailaddress1 ;
        this.modify("emailaddress1",emailaddress1);
    }

    /**
     * 设置 [跟踪电子邮件活动]
     */
    public void setFollowemail(Integer followemail){
        this.followemail = followemail ;
        this.modify("followemail",followemail);
    }

    /**
     * 设置 [国家/地区]
     */
    public void setAddress1Country(String address1Country){
        this.address1Country = address1Country ;
        this.modify("address1_country",address1Country);
    }

    /**
     * 设置 [网站]
     */
    public void setWebsiteurl(String websiteurl){
        this.websiteurl = websiteurl ;
        this.modify("websiteurl",websiteurl);
    }

    /**
     * 设置 [街道 3]
     */
    public void setAddress1Line3(String address1Line3){
        this.address1Line3 = address1Line3 ;
        this.modify("address1_line3",address1Line3);
    }

    /**
     * 设置 [地址 2: ID]
     */
    public void setAddress2Addressid(String address2Addressid){
        this.address2Addressid = address2Addressid ;
        this.modify("address2_addressid",address2Addressid);
    }

    /**
     * 设置 [地址 1]
     */
    public void setAddress1Composite(String address1Composite){
        this.address1Composite = address1Composite ;
        this.modify("address1_composite",address1Composite);
    }

    /**
     * 设置 [主题]
     */
    public void setSubject(String subject){
        this.subject = subject ;
        this.modify("subject",subject);
    }

    /**
     * 设置 [地址 1: UTC 时差]
     */
    public void setAddress1Utcoffset(Integer address1Utcoffset){
        this.address1Utcoffset = address1Utcoffset ;
        this.modify("address1_utcoffset",address1Utcoffset);
    }

    /**
     * 设置 [预算金额 (Base)]
     */
    public void setBudgetamountBase(BigDecimal budgetamountBase){
        this.budgetamountBase = budgetamountBase ;
        this.modify("budgetamount_base",budgetamountBase);
    }

    /**
     * 设置 [地址 1: 地址类型]
     */
    public void setAddress1Addresstypecode(String address1Addresstypecode){
        this.address1Addresstypecode = address1Addresstypecode ;
        this.modify("address1_addresstypecode",address1Addresstypecode);
    }

    /**
     * 设置 [地址 2: 电话 3]
     */
    public void setAddress2Telephone3(String address2Telephone3){
        this.address2Telephone3 = address2Telephone3 ;
        this.modify("address2_telephone3",address2Telephone3);
    }

    /**
     * 设置 [客户]
     */
    public void setCustomername(String customername){
        this.customername = customername ;
        this.modify("customername",customername);
    }

    /**
     * 设置 [潜在客户]
     */
    public void setMasterleadname(String masterleadname){
        this.masterleadname = masterleadname ;
        this.modify("masterleadname",masterleadname);
    }

    /**
     * 设置 [评估相符程度]
     */
    public void setEvaluatefit(Integer evaluatefit){
        this.evaluatefit = evaluatefit ;
        this.modify("evaluatefit",evaluatefit);
    }

    /**
     * 设置 [地址 1: 纬度]
     */
    public void setAddress1Latitude(Double address1Latitude){
        this.address1Latitude = address1Latitude ;
        this.modify("address1_latitude",address1Latitude);
    }

    /**
     * 设置 [地址 1: 电话 3]
     */
    public void setAddress1Telephone3(String address1Telephone3){
        this.address1Telephone3 = address1Telephone3 ;
        this.modify("address1_telephone3",address1Telephone3);
    }

    /**
     * 设置 [姓名]
     */
    public void setFullname(String fullname){
        this.fullname = fullname ;
        this.modify("fullname",fullname);
    }

    /**
     * 设置 [预计值 (Base)]
     */
    public void setEstimatedamountBase(BigDecimal estimatedamountBase){
        this.estimatedamountBase = estimatedamountBase ;
        this.modify("estimatedamount_base",estimatedamountBase);
    }

    /**
     * 设置 [预算]
     */
    public void setBudgetstatus(String budgetstatus){
        this.budgetstatus = budgetstatus ;
        this.modify("budgetstatus",budgetstatus);
    }

    /**
     * 设置 [行业]
     */
    public void setIndustrycode(String industrycode){
        this.industrycode = industrycode ;
        this.modify("industrycode",industrycode);
    }

    /**
     * 设置 [街道 2]
     */
    public void setAddress1Line2(String address1Line2){
        this.address1Line2 = address1Line2 ;
        this.modify("address1_line2",address1Line2);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnername(String ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [初始通信]
     */
    public void setInitialcommunication(String initialcommunication){
        this.initialcommunication = initialcommunication ;
        this.modify("initialcommunication",initialcommunication);
    }

    /**
     * 设置 [地址 1: 邮政信箱]
     */
    public void setAddress1Postofficebox(String address1Postofficebox){
        this.address1Postofficebox = address1Postofficebox ;
        this.modify("address1_postofficebox",address1Postofficebox);
    }

    /**
     * 设置 [地址 1: 电话 1]
     */
    public void setAddress1Telephone1(String address1Telephone1){
        this.address1Telephone1 = address1Telephone1 ;
        this.modify("address1_telephone1",address1Telephone1);
    }

    /**
     * 设置 [年收入]
     */
    public void setRevenue(BigDecimal revenue){
        this.revenue = revenue ;
        this.modify("revenue",revenue);
    }

    /**
     * 设置 [地址 2: 县]
     */
    public void setAddress2County(String address2County){
        this.address2County = address2County ;
        this.modify("address2_county",address2County);
    }

    /**
     * 设置 [Stage Id]
     */
    public void setStageid(String stageid){
        this.stageid = stageid ;
        this.modify("stageid",stageid);
    }

    /**
     * 设置 [地址 1: 送货方式]
     */
    public void setAddress1Shippingmethodcode(String address1Shippingmethodcode){
        this.address1Shippingmethodcode = address1Shippingmethodcode ;
        this.modify("address1_shippingmethodcode",address1Shippingmethodcode);
    }

    /**
     * 设置 [预计值]
     */
    public void setEstimatedamount(BigDecimal estimatedamount){
        this.estimatedamount = estimatedamount ;
        this.modify("estimatedamount",estimatedamount);
    }

    /**
     * 设置 [地址 1: 县]
     */
    public void setAddress1County(String address1County){
        this.address1County = address1County ;
        this.modify("address1_county",address1County);
    }

    /**
     * 设置 [UTC Conversion Time Zone Code]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [上一暂候时间]
     */
    public void setLastonholdtime(Timestamp lastonholdtime){
        this.lastonholdtime = lastonholdtime ;
        this.modify("lastonholdtime",lastonholdtime);
    }

    /**
     * 格式化日期 [上一暂候时间]
     */
    public String formatLastonholdtime(){
        if (this.lastonholdtime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(lastonholdtime);
    }
    /**
     * 设置 [TeamsFollowed]
     */
    public void setTeamsfollowed(Integer teamsfollowed){
        this.teamsfollowed = teamsfollowed ;
        this.modify("teamsfollowed",teamsfollowed);
    }

    /**
     * 设置 [称呼]
     */
    public void setSalutation(String salutation){
        this.salutation = salutation ;
        this.modify("salutation",salutation);
    }

    /**
     * 设置 [地址 2: 送货方式]
     */
    public void setAddress2Shippingmethodcode(String address2Shippingmethodcode){
        this.address2Shippingmethodcode = address2Shippingmethodcode ;
        this.modify("address2_shippingmethodcode",address2Shippingmethodcode);
    }

    /**
     * 设置 [地址 2: 纬度]
     */
    public void setAddress2Latitude(Double address2Latitude){
        this.address2Latitude = address2Latitude ;
        this.modify("address2_latitude",address2Latitude);
    }

    /**
     * 设置 [参与工作流]
     */
    public void setParticipatesinworkflow(Integer participatesinworkflow){
        this.participatesinworkflow = participatesinworkflow ;
        this.modify("participatesinworkflow",participatesinworkflow);
    }

    /**
     * 设置 [地址 2]
     */
    public void setAddress2Composite(String address2Composite){
        this.address2Composite = address2Composite ;
        this.modify("address2_composite",address2Composite);
    }

    /**
     * 设置 [销售阶段]
     */
    public void setSalesstage(String salesstage){
        this.salesstage = salesstage ;
        this.modify("salesstage",salesstage);
    }

    /**
     * 设置 [不允许使用邮件]
     */
    public void setDonotpostalmail(Integer donotpostalmail){
        this.donotpostalmail = donotpostalmail ;
        this.modify("donotpostalmail",donotpostalmail);
    }

    /**
     * 设置 [年收入 (Base)]
     */
    public void setRevenueBase(BigDecimal revenueBase){
        this.revenueBase = revenueBase ;
        this.modify("revenue_base",revenueBase);
    }

    /**
     * 设置 [Record Created On]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [Record Created On]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [安排跟进(潜在客户)]
     */
    public void setSchedulefollowupProspect(Timestamp schedulefollowupProspect){
        this.schedulefollowupProspect = schedulefollowupProspect ;
        this.modify("schedulefollowup_prospect",schedulefollowupProspect);
    }

    /**
     * 格式化日期 [安排跟进(潜在客户)]
     */
    public String formatSchedulefollowupProspect(){
        if (this.schedulefollowupProspect == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(schedulefollowupProspect);
    }
    /**
     * 设置 [地址 2: 市/县]
     */
    public void setAddress2City(String address2City){
        this.address2City = address2City ;
        this.modify("address2_city",address2City);
    }

    /**
     * 设置 [商务电话]
     */
    public void setTelephone1(String telephone1){
        this.telephone1 = telephone1 ;
        this.modify("telephone1",telephone1);
    }

    /**
     * 设置 [是否私有]
     */
    public void setIbizprivate(Integer ibizprivate){
        this.ibizprivate = ibizprivate ;
        this.modify("private",ibizprivate);
    }

    /**
     * 设置 [移动电话]
     */
    public void setMobilephone(String mobilephone){
        this.mobilephone = mobilephone ;
        this.modify("mobilephone",mobilephone);
    }

    /**
     * 设置 [需求]
     */
    public void setNeed(String need){
        this.need = need ;
        this.modify("need",need);
    }

    /**
     * 设置 [优先级]
     */
    public void setPrioritycode(String prioritycode){
        this.prioritycode = prioritycode ;
        this.modify("prioritycode",prioritycode);
    }

    /**
     * 设置 [地址 1: ID]
     */
    public void setAddress1Addressid(String address1Addressid){
        this.address1Addressid = address1Addressid ;
        this.modify("address1_addressid",address1Addressid);
    }

    /**
     * 设置 [销售阶段代码]
     */
    public void setSalesstagecode(String salesstagecode){
        this.salesstagecode = salesstagecode ;
        this.modify("salesstagecode",salesstagecode);
    }

    /**
     * 设置 [Import Sequence Number]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [地址 2: 电话 1]
     */
    public void setAddress2Telephone1(String address2Telephone1){
        this.address2Telephone1 = address2Telephone1 ;
        this.modify("address2_telephone1",address2Telephone1);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnerid(String ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [中间名]
     */
    public void setMiddlename(String middlename){
        this.middlename = middlename ;
        this.modify("middlename",middlename);
    }

    /**
     * 设置 [住宅电话]
     */
    public void setTelephone2(String telephone2){
        this.telephone2 = telephone2 ;
        this.modify("telephone2",telephone2);
    }

    /**
     * 设置 [购买时间范围]
     */
    public void setPurchasetimeframe(String purchasetimeframe){
        this.purchasetimeframe = purchasetimeframe ;
        this.modify("purchasetimeframe",purchasetimeframe);
    }

    /**
     * 设置 [潜在顾客来源]
     */
    public void setLeadsourcecode(String leadsourcecode){
        this.leadsourcecode = leadsourcecode ;
        this.modify("leadsourcecode",leadsourcecode);
    }

    /**
     * 设置 [EntityImage_Timestamp]
     */
    public void setEntityimageTimestamp(BigInteger entityimageTimestamp){
        this.entityimageTimestamp = entityimageTimestamp ;
        this.modify("entityimage_timestamp",entityimageTimestamp);
    }

    /**
     * 设置 [首选联系方式]
     */
    public void setPreferredcontactmethodcode(String preferredcontactmethodcode){
        this.preferredcontactmethodcode = preferredcontactmethodcode ;
        this.modify("preferredcontactmethodcode",preferredcontactmethodcode);
    }

    /**
     * 设置 [实体图像]
     */
    public void setEntityimage(String entityimage){
        this.entityimage = entityimage ;
        this.modify("entityimage",entityimage);
    }

    /**
     * 设置 [地址 2: 电话 2]
     */
    public void setAddress2Telephone2(String address2Telephone2){
        this.address2Telephone2 = address2Telephone2 ;
        this.modify("address2_telephone2",address2Telephone2);
    }

    /**
     * 设置 [市场营销资料]
     */
    public void setDonotsendmm(Integer donotsendmm){
        this.donotsendmm = donotsendmm ;
        this.modify("donotsendmm",donotsendmm);
    }

    /**
     * 设置 [采购程序]
     */
    public void setPurchaseprocess(String purchaseprocess){
        this.purchaseprocess = purchaseprocess ;
        this.modify("purchaseprocess",purchaseprocess);
    }

    /**
     * 设置 [不允许使用批量电子邮件]
     */
    public void setDonotbulkemail(Integer donotbulkemail){
        this.donotbulkemail = donotbulkemail ;
        this.modify("donotbulkemail",donotbulkemail);
    }

    /**
     * 设置 [行业编码]
     */
    public void setSic(String sic){
        this.sic = sic ;
        this.modify("sic",sic);
    }

    /**
     * 设置 [联系人]
     */
    public void setContactname(String contactname){
        this.contactname = contactname ;
        this.modify("contactname",contactname);
    }

    /**
     * 设置 [不允许使用电子邮件]
     */
    public void setDonotemail(Integer donotemail){
        this.donotemail = donotemail ;
        this.modify("donotemail",donotemail);
    }

    /**
     * 设置 [地址 2: 经度]
     */
    public void setAddress2Longitude(Double address2Longitude){
        this.address2Longitude = address2Longitude ;
        this.modify("address2_longitude",address2Longitude);
    }

    /**
     * 设置 [确认有兴趣]
     */
    public void setConfirminterest(Integer confirminterest){
        this.confirminterest = confirminterest ;
        this.modify("confirminterest",confirminterest);
    }

    /**
     * 设置 [负责人类型]
     */
    public void setOwnertype(String ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [地址 2: 邮政信箱]
     */
    public void setAddress2Postofficebox(String address2Postofficebox){
        this.address2Postofficebox = address2Postofficebox ;
        this.modify("address2_postofficebox",address2Postofficebox);
    }

    /**
     * 设置 [状态]
     */
    public void setStatecode(Integer statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [自动创建]
     */
    public void setAutocreate(Integer autocreate){
        this.autocreate = autocreate ;
        this.modify("autocreate",autocreate);
    }

    /**
     * 设置 [地址 1: 名称]
     */
    public void setAddress1Name(String address1Name){
        this.address1Name = address1Name ;
        this.modify("address1_name",address1Name);
    }

    /**
     * 设置 [Time Zone Rule Version Number]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [上次市场活动日期]
     */
    public void setLastusedincampaign(Timestamp lastusedincampaign){
        this.lastusedincampaign = lastusedincampaign ;
        this.modify("lastusedincampaign",lastusedincampaign);
    }

    /**
     * 格式化日期 [上次市场活动日期]
     */
    public String formatLastusedincampaign(){
        if (this.lastusedincampaign == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(lastusedincampaign);
    }
    /**
     * 设置 [预计结束日期]
     */
    public void setEstimatedclosedate(Timestamp estimatedclosedate){
        this.estimatedclosedate = estimatedclosedate ;
        this.modify("estimatedclosedate",estimatedclosedate);
    }

    /**
     * 格式化日期 [预计结束日期]
     */
    public String formatEstimatedclosedate(){
        if (this.estimatedclosedate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(estimatedclosedate);
    }
    /**
     * 设置 [地址 2: 街道 2]
     */
    public void setAddress2Line2(String address2Line2){
        this.address2Line2 = address2Line2 ;
        this.modify("address2_line2",address2Line2);
    }

    /**
     * 设置 [电子邮件地址 3]
     */
    public void setEmailaddress3(String emailaddress3){
        this.emailaddress3 = emailaddress3 ;
        this.modify("emailaddress3",emailaddress3);
    }

    /**
     * 设置 [寻呼机]
     */
    public void setPager(String pager){
        this.pager = pager ;
        this.modify("pager",pager);
    }

    /**
     * 设置 [地址 2: UPS 区域]
     */
    public void setAddress2Upszone(String address2Upszone){
        this.address2Upszone = address2Upszone ;
        this.modify("address2_upszone",address2Upszone);
    }

    /**
     * 设置 [Traversed Path]
     */
    public void setTraversedpath(String traversedpath){
        this.traversedpath = traversedpath ;
        this.modify("traversedpath",traversedpath);
    }

    /**
     * 设置 [传真]
     */
    public void setFax(String fax){
        this.fax = fax ;
        this.modify("fax",fax);
    }

    /**
     * 设置 [安排跟进(授予资格)]
     */
    public void setSchedulefollowupQualify(Timestamp schedulefollowupQualify){
        this.schedulefollowupQualify = schedulefollowupQualify ;
        this.modify("schedulefollowup_qualify",schedulefollowupQualify);
    }

    /**
     * 格式化日期 [安排跟进(授予资格)]
     */
    public String formatSchedulefollowupQualify(){
        if (this.schedulefollowupQualify == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(schedulefollowupQualify);
    }
    /**
     * 设置 [其他电话]
     */
    public void setTelephone3(String telephone3){
        this.telephone3 = telephone3 ;
        this.modify("telephone3",telephone3);
    }

    /**
     * 设置 [Process Id]
     */
    public void setProcessid(String processid){
        this.processid = processid ;
        this.modify("processid",processid);
    }

    /**
     * 设置 [电子邮件地址 2]
     */
    public void setEmailaddress2(String emailaddress2){
        this.emailaddress2 = emailaddress2 ;
        this.modify("emailaddress2",emailaddress2);
    }

    /**
     * 设置 [决策者?]
     */
    public void setDecisionmaker(Integer decisionmaker){
        this.decisionmaker = decisionmaker ;
        this.modify("decisionmaker",decisionmaker);
    }

    /**
     * 设置 [邮政编码]
     */
    public void setAddress1Postalcode(String address1Postalcode){
        this.address1Postalcode = address1Postalcode ;
        this.modify("address1_postalcode",address1Postalcode);
    }

    /**
     * 设置 [地址 2: 邮政编码]
     */
    public void setAddress2Postalcode(String address2Postalcode){
        this.address2Postalcode = address2Postalcode ;
        this.modify("address2_postalcode",address2Postalcode);
    }

    /**
     * 设置 [不允许使用传真]
     */
    public void setDonotfax(Integer donotfax){
        this.donotfax = donotfax ;
        this.modify("donotfax",donotfax);
    }

    /**
     * 设置 [客户类型]
     */
    public void setCustomertype(String customertype){
        this.customertype = customertype ;
        this.modify("customertype",customertype);
    }

    /**
     * 设置 [名]
     */
    public void setFirstname(String firstname){
        this.firstname = firstname ;
        this.modify("firstname",firstname);
    }

    /**
     * 设置 [合并]
     */
    public void setMerged(Integer merged){
        this.merged = merged ;
        this.modify("merged",merged);
    }

    /**
     * 设置 [地址 2: 地址类型]
     */
    public void setAddress2Addresstypecode(String address2Addresstypecode){
        this.address2Addresstypecode = address2Addresstypecode ;
        this.modify("address2_addresstypecode",address2Addresstypecode);
    }

    /**
     * 设置 [地址 1: UPS 区域]
     */
    public void setAddress1Upszone(String address1Upszone){
        this.address1Upszone = address1Upszone ;
        this.modify("address1_upszone",address1Upszone);
    }

    /**
     * 设置 [SLA]
     */
    public void setSlaname(String slaname){
        this.slaname = slaname ;
        this.modify("slaname",slaname);
    }

    /**
     * 设置 [相关市场活动响应]
     */
    public void setRelatedobjectname(String relatedobjectname){
        this.relatedobjectname = relatedobjectname ;
        this.modify("relatedobjectname",relatedobjectname);
    }

    /**
     * 设置 [货币]
     */
    public void setCurrencyname(String currencyname){
        this.currencyname = currencyname ;
        this.modify("currencyname",currencyname);
    }

    /**
     * 设置 [原始案例]
     */
    public void setOriginatingcasename(String originatingcasename){
        this.originatingcasename = originatingcasename ;
        this.modify("originatingcasename",originatingcasename);
    }

    /**
     * 设置 [源市场活动]
     */
    public void setCampaignname(String campaignname){
        this.campaignname = campaignname ;
        this.modify("campaignname",campaignname);
    }

    /**
     * 设置 [识别客户]
     */
    public void setParentaccountname(String parentaccountname){
        this.parentaccountname = parentaccountname ;
        this.modify("parentaccountname",parentaccountname);
    }

    /**
     * 设置 [识别联系人]
     */
    public void setParentcontactname(String parentcontactname){
        this.parentcontactname = parentcontactname ;
        this.modify("parentcontactname",parentcontactname);
    }

    /**
     * 设置 [授予商机资格]
     */
    public void setQualifyingopportunityname(String qualifyingopportunityname){
        this.qualifyingopportunityname = qualifyingopportunityname ;
        this.modify("qualifyingopportunityname",qualifyingopportunityname);
    }

    /**
     * 设置 [授予商机资格]
     */
    public void setQualifyingopportunityid(String qualifyingopportunityid){
        this.qualifyingopportunityid = qualifyingopportunityid ;
        this.modify("qualifyingopportunityid",qualifyingopportunityid);
    }

    /**
     * 设置 [SLA]
     */
    public void setSlaid(String slaid){
        this.slaid = slaid ;
        this.modify("slaid",slaid);
    }

    /**
     * 设置 [源市场活动]
     */
    public void setCampaignid(String campaignid){
        this.campaignid = campaignid ;
        this.modify("campaignid",campaignid);
    }

    /**
     * 设置 [相关市场活动响应]
     */
    public void setRelatedobjectid(String relatedobjectid){
        this.relatedobjectid = relatedobjectid ;
        this.modify("relatedobjectid",relatedobjectid);
    }

    /**
     * 设置 [货币]
     */
    public void setTransactioncurrencyid(String transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }

    /**
     * 设置 [潜在顾客的上级单位]
     */
    public void setParentaccountid(String parentaccountid){
        this.parentaccountid = parentaccountid ;
        this.modify("parentaccountid",parentaccountid);
    }

    /**
     * 设置 [原始案例]
     */
    public void setOriginatingcaseid(String originatingcaseid){
        this.originatingcaseid = originatingcaseid ;
        this.modify("originatingcaseid",originatingcaseid);
    }

    /**
     * 设置 [潜在顾客的上司]
     */
    public void setParentcontactid(String parentcontactid){
        this.parentcontactid = parentcontactid ;
        this.modify("parentcontactid",parentcontactid);
    }


}



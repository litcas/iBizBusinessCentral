package cn.ibizlab.businesscentral.core.sales.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.sales.domain.QuoteDetail;
import cn.ibizlab.businesscentral.core.sales.filter.QuoteDetailSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[QuoteDetail] 服务对象接口
 */
public interface IQuoteDetailService extends IService<QuoteDetail>{

    boolean create(QuoteDetail et) ;
    void createBatch(List<QuoteDetail> list) ;
    boolean update(QuoteDetail et) ;
    void updateBatch(List<QuoteDetail> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    QuoteDetail get(String key) ;
    QuoteDetail getDraft(QuoteDetail et) ;
    boolean checkKey(QuoteDetail et) ;
    boolean save(QuoteDetail et) ;
    void saveBatch(List<QuoteDetail> list) ;
    Page<QuoteDetail> searchDefault(QuoteDetailSearchContext context) ;
    List<QuoteDetail> selectByProductid(String productid) ;
    void removeByProductid(String productid) ;
    List<QuoteDetail> selectByParentbundleidref(String quotedetailid) ;
    void removeByParentbundleidref(String quotedetailid) ;
    List<QuoteDetail> selectByQuoteid(String quoteid) ;
    void removeByQuoteid(String quoteid) ;
    List<QuoteDetail> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    List<QuoteDetail> selectByUomid(String uomid) ;
    void removeByUomid(String uomid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<QuoteDetail> getQuotedetailByIds(List<String> ids) ;
    List<QuoteDetail> getQuotedetailByEntities(List<QuoteDetail> entities) ;
}



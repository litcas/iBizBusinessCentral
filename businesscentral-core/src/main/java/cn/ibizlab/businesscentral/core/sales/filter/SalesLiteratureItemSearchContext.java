package cn.ibizlab.businesscentral.core.sales.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.sales.domain.SalesLiteratureItem;
/**
 * 关系型数据实体[SalesLiteratureItem] 查询条件对象
 */
@Slf4j
@Data
public class SalesLiteratureItemSearchContext extends QueryWrapperContext<SalesLiteratureItem> {

	private String n_title_like;//[标题]
	public void setN_title_like(String n_title_like) {
        this.n_title_like = n_title_like;
        if(!ObjectUtils.isEmpty(this.n_title_like)){
            this.getSearchCond().like("title", n_title_like);
        }
    }
	private String n_filetypecode_eq;//[文件类型]
	public void setN_filetypecode_eq(String n_filetypecode_eq) {
        this.n_filetypecode_eq = n_filetypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_filetypecode_eq)){
            this.getSearchCond().eq("filetypecode", n_filetypecode_eq);
        }
    }
	private String n_salesliteratureid_eq;//[销售宣传资料]
	public void setN_salesliteratureid_eq(String n_salesliteratureid_eq) {
        this.n_salesliteratureid_eq = n_salesliteratureid_eq;
        if(!ObjectUtils.isEmpty(this.n_salesliteratureid_eq)){
            this.getSearchCond().eq("salesliteratureid", n_salesliteratureid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("title", query)   
            );
		 }
	}
}




package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.base.domain.ActivityPointer;
import cn.ibizlab.businesscentral.core.base.service.IActivityPointerService;
import cn.ibizlab.businesscentral.core.base.filter.ActivityPointerSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"活动" })
@RestController("CentralApi-activitypointer")
@RequestMapping("")
public class ActivityPointerResource {

    @Autowired
    public IActivityPointerService activitypointerService;

    @Autowired
    @Lazy
    public ActivityPointerMapping activitypointerMapping;

    @PreAuthorize("hasPermission(this.activitypointerMapping.toDomain(#activitypointerdto),'iBizBusinessCentral-ActivityPointer-Create')")
    @ApiOperation(value = "新建活动", tags = {"活动" },  notes = "新建活动")
	@RequestMapping(method = RequestMethod.POST, value = "/activitypointers")
    public ResponseEntity<ActivityPointerDTO> create(@RequestBody ActivityPointerDTO activitypointerdto) {
        ActivityPointer domain = activitypointerMapping.toDomain(activitypointerdto);
		activitypointerService.create(domain);
        ActivityPointerDTO dto = activitypointerMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.activitypointerMapping.toDomain(#activitypointerdtos),'iBizBusinessCentral-ActivityPointer-Create')")
    @ApiOperation(value = "批量新建活动", tags = {"活动" },  notes = "批量新建活动")
	@RequestMapping(method = RequestMethod.POST, value = "/activitypointers/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<ActivityPointerDTO> activitypointerdtos) {
        activitypointerService.createBatch(activitypointerMapping.toDomain(activitypointerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "activitypointer" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.activitypointerService.get(#activitypointer_id),'iBizBusinessCentral-ActivityPointer-Update')")
    @ApiOperation(value = "更新活动", tags = {"活动" },  notes = "更新活动")
	@RequestMapping(method = RequestMethod.PUT, value = "/activitypointers/{activitypointer_id}")
    public ResponseEntity<ActivityPointerDTO> update(@PathVariable("activitypointer_id") String activitypointer_id, @RequestBody ActivityPointerDTO activitypointerdto) {
		ActivityPointer domain  = activitypointerMapping.toDomain(activitypointerdto);
        domain .setActivityid(activitypointer_id);
		activitypointerService.update(domain );
		ActivityPointerDTO dto = activitypointerMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.activitypointerService.getActivitypointerByEntities(this.activitypointerMapping.toDomain(#activitypointerdtos)),'iBizBusinessCentral-ActivityPointer-Update')")
    @ApiOperation(value = "批量更新活动", tags = {"活动" },  notes = "批量更新活动")
	@RequestMapping(method = RequestMethod.PUT, value = "/activitypointers/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<ActivityPointerDTO> activitypointerdtos) {
        activitypointerService.updateBatch(activitypointerMapping.toDomain(activitypointerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.activitypointerService.get(#activitypointer_id),'iBizBusinessCentral-ActivityPointer-Remove')")
    @ApiOperation(value = "删除活动", tags = {"活动" },  notes = "删除活动")
	@RequestMapping(method = RequestMethod.DELETE, value = "/activitypointers/{activitypointer_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("activitypointer_id") String activitypointer_id) {
         return ResponseEntity.status(HttpStatus.OK).body(activitypointerService.remove(activitypointer_id));
    }

    @PreAuthorize("hasPermission(this.activitypointerService.getActivitypointerByIds(#ids),'iBizBusinessCentral-ActivityPointer-Remove')")
    @ApiOperation(value = "批量删除活动", tags = {"活动" },  notes = "批量删除活动")
	@RequestMapping(method = RequestMethod.DELETE, value = "/activitypointers/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        activitypointerService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.activitypointerMapping.toDomain(returnObject.body),'iBizBusinessCentral-ActivityPointer-Get')")
    @ApiOperation(value = "获取活动", tags = {"活动" },  notes = "获取活动")
	@RequestMapping(method = RequestMethod.GET, value = "/activitypointers/{activitypointer_id}")
    public ResponseEntity<ActivityPointerDTO> get(@PathVariable("activitypointer_id") String activitypointer_id) {
        ActivityPointer domain = activitypointerService.get(activitypointer_id);
        ActivityPointerDTO dto = activitypointerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取活动草稿", tags = {"活动" },  notes = "获取活动草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/activitypointers/getdraft")
    public ResponseEntity<ActivityPointerDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(activitypointerMapping.toDto(activitypointerService.getDraft(new ActivityPointer())));
    }

    @ApiOperation(value = "检查活动", tags = {"活动" },  notes = "检查活动")
	@RequestMapping(method = RequestMethod.POST, value = "/activitypointers/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody ActivityPointerDTO activitypointerdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(activitypointerService.checkKey(activitypointerMapping.toDomain(activitypointerdto)));
    }

    @PreAuthorize("hasPermission(this.activitypointerMapping.toDomain(#activitypointerdto),'iBizBusinessCentral-ActivityPointer-Save')")
    @ApiOperation(value = "保存活动", tags = {"活动" },  notes = "保存活动")
	@RequestMapping(method = RequestMethod.POST, value = "/activitypointers/save")
    public ResponseEntity<Boolean> save(@RequestBody ActivityPointerDTO activitypointerdto) {
        return ResponseEntity.status(HttpStatus.OK).body(activitypointerService.save(activitypointerMapping.toDomain(activitypointerdto)));
    }

    @PreAuthorize("hasPermission(this.activitypointerMapping.toDomain(#activitypointerdtos),'iBizBusinessCentral-ActivityPointer-Save')")
    @ApiOperation(value = "批量保存活动", tags = {"活动" },  notes = "批量保存活动")
	@RequestMapping(method = RequestMethod.POST, value = "/activitypointers/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<ActivityPointerDTO> activitypointerdtos) {
        activitypointerService.saveBatch(activitypointerMapping.toDomain(activitypointerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ActivityPointer-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-ActivityPointer-Get')")
	@ApiOperation(value = "获取ByParentKey", tags = {"活动" } ,notes = "获取ByParentKey")
    @RequestMapping(method= RequestMethod.GET , value="/activitypointers/fetchbyparentkey")
	public ResponseEntity<List<ActivityPointerDTO>> fetchByParentKey(ActivityPointerSearchContext context) {
        Page<ActivityPointer> domains = activitypointerService.searchByParentKey(context) ;
        List<ActivityPointerDTO> list = activitypointerMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ActivityPointer-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-ActivityPointer-Get')")
	@ApiOperation(value = "查询ByParentKey", tags = {"活动" } ,notes = "查询ByParentKey")
    @RequestMapping(method= RequestMethod.POST , value="/activitypointers/searchbyparentkey")
	public ResponseEntity<Page<ActivityPointerDTO>> searchByParentKey(@RequestBody ActivityPointerSearchContext context) {
        Page<ActivityPointer> domains = activitypointerService.searchByParentKey(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(activitypointerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ActivityPointer-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ActivityPointer-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"活动" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/activitypointers/fetchdefault")
	public ResponseEntity<List<ActivityPointerDTO>> fetchDefault(ActivityPointerSearchContext context) {
        Page<ActivityPointer> domains = activitypointerService.searchDefault(context) ;
        List<ActivityPointerDTO> list = activitypointerMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ActivityPointer-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ActivityPointer-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"活动" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/activitypointers/searchdefault")
	public ResponseEntity<Page<ActivityPointerDTO>> searchDefault(@RequestBody ActivityPointerSearchContext context) {
        Page<ActivityPointer> domains = activitypointerService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(activitypointerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}


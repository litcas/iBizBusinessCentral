package cn.ibizlab.businesscentral.centralapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[BulkOperationDTO]
 */
@Data
public class BulkOperationDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [PARAMETERS]
     *
     */
    @JSONField(name = "parameters")
    @JsonProperty("parameters")
    private String parameters;

    /**
     * 属性 [REGARDINGOBJECTNAME]
     *
     */
    @JSONField(name = "regardingobjectname")
    @JsonProperty("regardingobjectname")
    private String regardingobjectname;

    /**
     * 属性 [CUSTOMERS]
     *
     */
    @JSONField(name = "customers")
    @JsonProperty("customers")
    private String customers;

    /**
     * 属性 [TRAVERSEDPATH]
     *
     */
    @JSONField(name = "traversedpath")
    @JsonProperty("traversedpath")
    private String traversedpath;

    /**
     * 属性 [SCHEDULEDSTART]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "scheduledstart" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("scheduledstart")
    private Timestamp scheduledstart;

    /**
     * 属性 [TARGETMEMBERSCOUNT]
     *
     */
    @JSONField(name = "targetmemberscount")
    @JsonProperty("targetmemberscount")
    private Integer targetmemberscount;

    /**
     * 属性 [BCC]
     *
     */
    @JSONField(name = "bcc")
    @JsonProperty("bcc")
    private String bcc;

    /**
     * 属性 [ACTUALDURATIONMINUTES]
     *
     */
    @JSONField(name = "actualdurationminutes")
    @JsonProperty("actualdurationminutes")
    private Integer actualdurationminutes;

    /**
     * 属性 [EXCHANGERATE]
     *
     */
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;

    /**
     * 属性 [BULKOPERATIONNUMBER]
     *
     */
    @JSONField(name = "bulkoperationnumber")
    @JsonProperty("bulkoperationnumber")
    private String bulkoperationnumber;

    /**
     * 属性 [SCHEDULEDEND]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "scheduledend" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("scheduledend")
    private Timestamp scheduledend;

    /**
     * 属性 [ONHOLDTIME]
     *
     */
    @JSONField(name = "onholdtime")
    @JsonProperty("onholdtime")
    private Integer onholdtime;

    /**
     * 属性 [LEFTVOICEMAIL]
     *
     */
    @JSONField(name = "leftvoicemail")
    @JsonProperty("leftvoicemail")
    private Integer leftvoicemail;

    /**
     * 属性 [OWNERNAME]
     *
     */
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;

    /**
     * 属性 [TIMEZONERULEVERSIONNUMBER]
     *
     */
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 属性 [PARTNERS]
     *
     */
    @JSONField(name = "partners")
    @JsonProperty("partners")
    private String partners;

    /**
     * 属性 [WORKFLOWINFO]
     *
     */
    @JSONField(name = "workflowinfo")
    @JsonProperty("workflowinfo")
    private String workflowinfo;

    /**
     * 属性 [SERVICEID]
     *
     */
    @JSONField(name = "serviceid")
    @JsonProperty("serviceid")
    private String serviceid;

    /**
     * 属性 [COMMUNITY]
     *
     */
    @JSONField(name = "community")
    @JsonProperty("community")
    private String community;

    /**
     * 属性 [WORKFLOWCREATED]
     *
     */
    @JSONField(name = "workflowcreated")
    @JsonProperty("workflowcreated")
    private Integer workflowcreated;

    /**
     * 属性 [OWNERTYPE]
     *
     */
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;

    /**
     * 属性 [CREATEDRECORDTYPECODE]
     *
     */
    @JSONField(name = "createdrecordtypecode")
    @JsonProperty("createdrecordtypecode")
    private String createdrecordtypecode;

    /**
     * 属性 [ERRORNUMBER]
     *
     */
    @JSONField(name = "errornumber")
    @JsonProperty("errornumber")
    private Integer errornumber;

    /**
     * 属性 [OVERRIDDENCREATEDON]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;

    /**
     * 属性 [REGARDINGOBJECTID]
     *
     */
    @JSONField(name = "regardingobjectid")
    @JsonProperty("regardingobjectid")
    private String regardingobjectid;

    /**
     * 属性 [STATECODE]
     *
     */
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;

    /**
     * 属性 [DELIVERYLASTATTEMPTEDON]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "deliverylastattemptedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("deliverylastattemptedon")
    private Timestamp deliverylastattemptedon;

    /**
     * 属性 [DELIVERYPRIORITYCODE]
     *
     */
    @JSONField(name = "deliveryprioritycode")
    @JsonProperty("deliveryprioritycode")
    private String deliveryprioritycode;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;

    /**
     * 属性 [ORGANIZER]
     *
     */
    @JSONField(name = "organizer")
    @JsonProperty("organizer")
    private String organizer;

    /**
     * 属性 [STAGEID]
     *
     */
    @JSONField(name = "stageid")
    @JsonProperty("stageid")
    private String stageid;

    /**
     * 属性 [TARGETEDRECORDTYPECODE]
     *
     */
    @JSONField(name = "targetedrecordtypecode")
    @JsonProperty("targetedrecordtypecode")
    private String targetedrecordtypecode;

    /**
     * 属性 [INSTANCETYPECODE]
     *
     */
    @JSONField(name = "instancetypecode")
    @JsonProperty("instancetypecode")
    private String instancetypecode;

    /**
     * 属性 [TO]
     *
     */
    @JSONField(name = "to")
    @JsonProperty("to")
    private String to;

    /**
     * 属性 [SCHEDULEDDURATIONMINUTES]
     *
     */
    @JSONField(name = "scheduleddurationminutes")
    @JsonProperty("scheduleddurationminutes")
    private Integer scheduleddurationminutes;

    /**
     * 属性 [EXCHANGEITEMID]
     *
     */
    @JSONField(name = "exchangeitemid")
    @JsonProperty("exchangeitemid")
    private String exchangeitemid;

    /**
     * 属性 [ACTIVITYID]
     *
     */
    @JSONField(name = "activityid")
    @JsonProperty("activityid")
    private String activityid;

    /**
     * 属性 [FROM]
     *
     */
    @JSONField(name = "from")
    @JsonProperty("from")
    private String from;

    /**
     * 属性 [MAPIPRIVATE]
     *
     */
    @JSONField(name = "mapiprivate")
    @JsonProperty("mapiprivate")
    private Integer mapiprivate;

    /**
     * 属性 [OWNERID]
     *
     */
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;

    /**
     * 属性 [STATUSCODE]
     *
     */
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;

    /**
     * 属性 [OPERATIONTYPECODE]
     *
     */
    @JSONField(name = "operationtypecode")
    @JsonProperty("operationtypecode")
    private String operationtypecode;

    /**
     * 属性 [ACTIVITYADDITIONALPARAMS]
     *
     */
    @JSONField(name = "activityadditionalparams")
    @JsonProperty("activityadditionalparams")
    private String activityadditionalparams;

    /**
     * 属性 [UTCCONVERSIONTIMEZONECODE]
     *
     */
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;

    /**
     * 属性 [IMPORTSEQUENCENUMBER]
     *
     */
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;

    /**
     * 属性 [SERIESID]
     *
     */
    @JSONField(name = "seriesid")
    @JsonProperty("seriesid")
    private String seriesid;

    /**
     * 属性 [REGULARACTIVITY]
     *
     */
    @JSONField(name = "regularactivity")
    @JsonProperty("regularactivity")
    private Integer regularactivity;

    /**
     * 属性 [ACTUALEND]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "actualend" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("actualend")
    private Timestamp actualend;

    /**
     * 属性 [PROCESSID]
     *
     */
    @JSONField(name = "processid")
    @JsonProperty("processid")
    private String processid;

    /**
     * 属性 [REQUIREDATTENDEES]
     *
     */
    @JSONField(name = "requiredattendees")
    @JsonProperty("requiredattendees")
    private String requiredattendees;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;

    /**
     * 属性 [LASTONHOLDTIME]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "lastonholdtime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("lastonholdtime")
    private Timestamp lastonholdtime;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [EXCHANGEWEBLINK]
     *
     */
    @JSONField(name = "exchangeweblink")
    @JsonProperty("exchangeweblink")
    private String exchangeweblink;

    /**
     * 属性 [SENTON]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "senton" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("senton")
    private Timestamp senton;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [FAILURECOUNT]
     *
     */
    @JSONField(name = "failurecount")
    @JsonProperty("failurecount")
    private Integer failurecount;

    /**
     * 属性 [SLANAME]
     *
     */
    @JSONField(name = "slaname")
    @JsonProperty("slaname")
    private String slaname;

    /**
     * 属性 [ACTIVITYTYPECODE]
     *
     */
    @JSONField(name = "activitytypecode")
    @JsonProperty("activitytypecode")
    private String activitytypecode;

    /**
     * 属性 [SUCCESSCOUNT]
     *
     */
    @JSONField(name = "successcount")
    @JsonProperty("successcount")
    private Integer successcount;

    /**
     * 属性 [CC]
     *
     */
    @JSONField(name = "cc")
    @JsonProperty("cc")
    private String cc;

    /**
     * 属性 [OPTIONALATTENDEES]
     *
     */
    @JSONField(name = "optionalattendees")
    @JsonProperty("optionalattendees")
    private String optionalattendees;

    /**
     * 属性 [ACTUALSTART]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "actualstart" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("actualstart")
    private Timestamp actualstart;

    /**
     * 属性 [SORTDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "sortdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("sortdate")
    private Timestamp sortdate;

    /**
     * 属性 [REGARDINGOBJECTTYPECODE]
     *
     */
    @JSONField(name = "regardingobjecttypecode")
    @JsonProperty("regardingobjecttypecode")
    private String regardingobjecttypecode;

    /**
     * 属性 [BILLED]
     *
     */
    @JSONField(name = "billed")
    @JsonProperty("billed")
    private Integer billed;

    /**
     * 属性 [PRIORITYCODE]
     *
     */
    @JSONField(name = "prioritycode")
    @JsonProperty("prioritycode")
    private String prioritycode;

    /**
     * 属性 [VERSIONNUMBER]
     *
     */
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;

    /**
     * 属性 [SUBJECT]
     *
     */
    @JSONField(name = "subject")
    @JsonProperty("subject")
    private String subject;

    /**
     * 属性 [RESOURCES]
     *
     */
    @JSONField(name = "resources")
    @JsonProperty("resources")
    private String resources;

    /**
     * 属性 [TRANSACTIONCURRENCYID]
     *
     */
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;

    /**
     * 属性 [SLAID]
     *
     */
    @JSONField(name = "slaid")
    @JsonProperty("slaid")
    private String slaid;


    /**
     * 设置 [PARAMETERS]
     */
    public void setParameters(String  parameters){
        this.parameters = parameters ;
        this.modify("parameters",parameters);
    }

    /**
     * 设置 [REGARDINGOBJECTNAME]
     */
    public void setRegardingobjectname(String  regardingobjectname){
        this.regardingobjectname = regardingobjectname ;
        this.modify("regardingobjectname",regardingobjectname);
    }

    /**
     * 设置 [CUSTOMERS]
     */
    public void setCustomers(String  customers){
        this.customers = customers ;
        this.modify("customers",customers);
    }

    /**
     * 设置 [TRAVERSEDPATH]
     */
    public void setTraversedpath(String  traversedpath){
        this.traversedpath = traversedpath ;
        this.modify("traversedpath",traversedpath);
    }

    /**
     * 设置 [SCHEDULEDSTART]
     */
    public void setScheduledstart(Timestamp  scheduledstart){
        this.scheduledstart = scheduledstart ;
        this.modify("scheduledstart",scheduledstart);
    }

    /**
     * 设置 [TARGETMEMBERSCOUNT]
     */
    public void setTargetmemberscount(Integer  targetmemberscount){
        this.targetmemberscount = targetmemberscount ;
        this.modify("targetmemberscount",targetmemberscount);
    }

    /**
     * 设置 [BCC]
     */
    public void setBcc(String  bcc){
        this.bcc = bcc ;
        this.modify("bcc",bcc);
    }

    /**
     * 设置 [ACTUALDURATIONMINUTES]
     */
    public void setActualdurationminutes(Integer  actualdurationminutes){
        this.actualdurationminutes = actualdurationminutes ;
        this.modify("actualdurationminutes",actualdurationminutes);
    }

    /**
     * 设置 [EXCHANGERATE]
     */
    public void setExchangerate(BigDecimal  exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [BULKOPERATIONNUMBER]
     */
    public void setBulkoperationnumber(String  bulkoperationnumber){
        this.bulkoperationnumber = bulkoperationnumber ;
        this.modify("bulkoperationnumber",bulkoperationnumber);
    }

    /**
     * 设置 [SCHEDULEDEND]
     */
    public void setScheduledend(Timestamp  scheduledend){
        this.scheduledend = scheduledend ;
        this.modify("scheduledend",scheduledend);
    }

    /**
     * 设置 [ONHOLDTIME]
     */
    public void setOnholdtime(Integer  onholdtime){
        this.onholdtime = onholdtime ;
        this.modify("onholdtime",onholdtime);
    }

    /**
     * 设置 [LEFTVOICEMAIL]
     */
    public void setLeftvoicemail(Integer  leftvoicemail){
        this.leftvoicemail = leftvoicemail ;
        this.modify("leftvoicemail",leftvoicemail);
    }

    /**
     * 设置 [OWNERNAME]
     */
    public void setOwnername(String  ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [TIMEZONERULEVERSIONNUMBER]
     */
    public void setTimezoneruleversionnumber(Integer  timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [PARTNERS]
     */
    public void setPartners(String  partners){
        this.partners = partners ;
        this.modify("partners",partners);
    }

    /**
     * 设置 [WORKFLOWINFO]
     */
    public void setWorkflowinfo(String  workflowinfo){
        this.workflowinfo = workflowinfo ;
        this.modify("workflowinfo",workflowinfo);
    }

    /**
     * 设置 [SERVICEID]
     */
    public void setServiceid(String  serviceid){
        this.serviceid = serviceid ;
        this.modify("serviceid",serviceid);
    }

    /**
     * 设置 [COMMUNITY]
     */
    public void setCommunity(String  community){
        this.community = community ;
        this.modify("community",community);
    }

    /**
     * 设置 [WORKFLOWCREATED]
     */
    public void setWorkflowcreated(Integer  workflowcreated){
        this.workflowcreated = workflowcreated ;
        this.modify("workflowcreated",workflowcreated);
    }

    /**
     * 设置 [OWNERTYPE]
     */
    public void setOwnertype(String  ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [CREATEDRECORDTYPECODE]
     */
    public void setCreatedrecordtypecode(String  createdrecordtypecode){
        this.createdrecordtypecode = createdrecordtypecode ;
        this.modify("createdrecordtypecode",createdrecordtypecode);
    }

    /**
     * 设置 [ERRORNUMBER]
     */
    public void setErrornumber(Integer  errornumber){
        this.errornumber = errornumber ;
        this.modify("errornumber",errornumber);
    }

    /**
     * 设置 [OVERRIDDENCREATEDON]
     */
    public void setOverriddencreatedon(Timestamp  overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 设置 [REGARDINGOBJECTID]
     */
    public void setRegardingobjectid(String  regardingobjectid){
        this.regardingobjectid = regardingobjectid ;
        this.modify("regardingobjectid",regardingobjectid);
    }

    /**
     * 设置 [STATECODE]
     */
    public void setStatecode(Integer  statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [DELIVERYLASTATTEMPTEDON]
     */
    public void setDeliverylastattemptedon(Timestamp  deliverylastattemptedon){
        this.deliverylastattemptedon = deliverylastattemptedon ;
        this.modify("deliverylastattemptedon",deliverylastattemptedon);
    }

    /**
     * 设置 [DELIVERYPRIORITYCODE]
     */
    public void setDeliveryprioritycode(String  deliveryprioritycode){
        this.deliveryprioritycode = deliveryprioritycode ;
        this.modify("deliveryprioritycode",deliveryprioritycode);
    }

    /**
     * 设置 [ORGANIZER]
     */
    public void setOrganizer(String  organizer){
        this.organizer = organizer ;
        this.modify("organizer",organizer);
    }

    /**
     * 设置 [STAGEID]
     */
    public void setStageid(String  stageid){
        this.stageid = stageid ;
        this.modify("stageid",stageid);
    }

    /**
     * 设置 [TARGETEDRECORDTYPECODE]
     */
    public void setTargetedrecordtypecode(String  targetedrecordtypecode){
        this.targetedrecordtypecode = targetedrecordtypecode ;
        this.modify("targetedrecordtypecode",targetedrecordtypecode);
    }

    /**
     * 设置 [INSTANCETYPECODE]
     */
    public void setInstancetypecode(String  instancetypecode){
        this.instancetypecode = instancetypecode ;
        this.modify("instancetypecode",instancetypecode);
    }

    /**
     * 设置 [TO]
     */
    public void setTo(String  to){
        this.to = to ;
        this.modify("to",to);
    }

    /**
     * 设置 [SCHEDULEDDURATIONMINUTES]
     */
    public void setScheduleddurationminutes(Integer  scheduleddurationminutes){
        this.scheduleddurationminutes = scheduleddurationminutes ;
        this.modify("scheduleddurationminutes",scheduleddurationminutes);
    }

    /**
     * 设置 [EXCHANGEITEMID]
     */
    public void setExchangeitemid(String  exchangeitemid){
        this.exchangeitemid = exchangeitemid ;
        this.modify("exchangeitemid",exchangeitemid);
    }

    /**
     * 设置 [FROM]
     */
    public void setFrom(String  from){
        this.from = from ;
        this.modify("from",from);
    }

    /**
     * 设置 [MAPIPRIVATE]
     */
    public void setMapiprivate(Integer  mapiprivate){
        this.mapiprivate = mapiprivate ;
        this.modify("mapiprivate",mapiprivate);
    }

    /**
     * 设置 [OWNERID]
     */
    public void setOwnerid(String  ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [STATUSCODE]
     */
    public void setStatuscode(Integer  statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [OPERATIONTYPECODE]
     */
    public void setOperationtypecode(String  operationtypecode){
        this.operationtypecode = operationtypecode ;
        this.modify("operationtypecode",operationtypecode);
    }

    /**
     * 设置 [ACTIVITYADDITIONALPARAMS]
     */
    public void setActivityadditionalparams(String  activityadditionalparams){
        this.activityadditionalparams = activityadditionalparams ;
        this.modify("activityadditionalparams",activityadditionalparams);
    }

    /**
     * 设置 [UTCCONVERSIONTIMEZONECODE]
     */
    public void setUtcconversiontimezonecode(Integer  utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [IMPORTSEQUENCENUMBER]
     */
    public void setImportsequencenumber(Integer  importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [SERIESID]
     */
    public void setSeriesid(String  seriesid){
        this.seriesid = seriesid ;
        this.modify("seriesid",seriesid);
    }

    /**
     * 设置 [REGULARACTIVITY]
     */
    public void setRegularactivity(Integer  regularactivity){
        this.regularactivity = regularactivity ;
        this.modify("regularactivity",regularactivity);
    }

    /**
     * 设置 [ACTUALEND]
     */
    public void setActualend(Timestamp  actualend){
        this.actualend = actualend ;
        this.modify("actualend",actualend);
    }

    /**
     * 设置 [PROCESSID]
     */
    public void setProcessid(String  processid){
        this.processid = processid ;
        this.modify("processid",processid);
    }

    /**
     * 设置 [REQUIREDATTENDEES]
     */
    public void setRequiredattendees(String  requiredattendees){
        this.requiredattendees = requiredattendees ;
        this.modify("requiredattendees",requiredattendees);
    }

    /**
     * 设置 [LASTONHOLDTIME]
     */
    public void setLastonholdtime(Timestamp  lastonholdtime){
        this.lastonholdtime = lastonholdtime ;
        this.modify("lastonholdtime",lastonholdtime);
    }

    /**
     * 设置 [EXCHANGEWEBLINK]
     */
    public void setExchangeweblink(String  exchangeweblink){
        this.exchangeweblink = exchangeweblink ;
        this.modify("exchangeweblink",exchangeweblink);
    }

    /**
     * 设置 [SENTON]
     */
    public void setSenton(Timestamp  senton){
        this.senton = senton ;
        this.modify("senton",senton);
    }

    /**
     * 设置 [FAILURECOUNT]
     */
    public void setFailurecount(Integer  failurecount){
        this.failurecount = failurecount ;
        this.modify("failurecount",failurecount);
    }

    /**
     * 设置 [SLANAME]
     */
    public void setSlaname(String  slaname){
        this.slaname = slaname ;
        this.modify("slaname",slaname);
    }

    /**
     * 设置 [ACTIVITYTYPECODE]
     */
    public void setActivitytypecode(String  activitytypecode){
        this.activitytypecode = activitytypecode ;
        this.modify("activitytypecode",activitytypecode);
    }

    /**
     * 设置 [SUCCESSCOUNT]
     */
    public void setSuccesscount(Integer  successcount){
        this.successcount = successcount ;
        this.modify("successcount",successcount);
    }

    /**
     * 设置 [CC]
     */
    public void setCc(String  cc){
        this.cc = cc ;
        this.modify("cc",cc);
    }

    /**
     * 设置 [OPTIONALATTENDEES]
     */
    public void setOptionalattendees(String  optionalattendees){
        this.optionalattendees = optionalattendees ;
        this.modify("optionalattendees",optionalattendees);
    }

    /**
     * 设置 [ACTUALSTART]
     */
    public void setActualstart(Timestamp  actualstart){
        this.actualstart = actualstart ;
        this.modify("actualstart",actualstart);
    }

    /**
     * 设置 [SORTDATE]
     */
    public void setSortdate(Timestamp  sortdate){
        this.sortdate = sortdate ;
        this.modify("sortdate",sortdate);
    }

    /**
     * 设置 [REGARDINGOBJECTTYPECODE]
     */
    public void setRegardingobjecttypecode(String  regardingobjecttypecode){
        this.regardingobjecttypecode = regardingobjecttypecode ;
        this.modify("regardingobjecttypecode",regardingobjecttypecode);
    }

    /**
     * 设置 [BILLED]
     */
    public void setBilled(Integer  billed){
        this.billed = billed ;
        this.modify("billed",billed);
    }

    /**
     * 设置 [PRIORITYCODE]
     */
    public void setPrioritycode(String  prioritycode){
        this.prioritycode = prioritycode ;
        this.modify("prioritycode",prioritycode);
    }

    /**
     * 设置 [VERSIONNUMBER]
     */
    public void setVersionnumber(BigInteger  versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [SUBJECT]
     */
    public void setSubject(String  subject){
        this.subject = subject ;
        this.modify("subject",subject);
    }

    /**
     * 设置 [RESOURCES]
     */
    public void setResources(String  resources){
        this.resources = resources ;
        this.modify("resources",resources);
    }

    /**
     * 设置 [TRANSACTIONCURRENCYID]
     */
    public void setTransactioncurrencyid(String  transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }

    /**
     * 设置 [SLAID]
     */
    public void setSlaid(String  slaid){
        this.slaid = slaid ;
        this.modify("slaid",slaid);
    }


}


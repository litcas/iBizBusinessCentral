package cn.ibizlab.businesscentral.centralapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[IncidentCustomerDTO]
 */
@Data
public class IncidentCustomerDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CUSTOMERID]
     *
     */
    @JSONField(name = "customerid")
    @JsonProperty("customerid")
    private String customerid;

    /**
     * 属性 [BUSINESSTYPECODE]
     *
     */
    @JSONField(name = "businesstypecode")
    @JsonProperty("businesstypecode")
    private String businesstypecode;

    /**
     * 属性 [CUSTOMERTYPE]
     *
     */
    @JSONField(name = "customertype")
    @JsonProperty("customertype")
    private String customertype;

    /**
     * 属性 [CUSTOMERNAME]
     *
     */
    @JSONField(name = "customername")
    @JsonProperty("customername")
    private String customername;



}


package cn.ibizlab.businesscentral.centralapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.marketing.domain.Campaign;
import cn.ibizlab.businesscentral.centralapi.dto.CampaignDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CentralApiCampaignMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface CampaignMapping extends MappingBase<CampaignDTO, Campaign> {


}


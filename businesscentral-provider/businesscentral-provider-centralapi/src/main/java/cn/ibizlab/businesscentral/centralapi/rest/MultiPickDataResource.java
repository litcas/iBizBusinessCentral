package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.base.domain.MultiPickData;
import cn.ibizlab.businesscentral.core.base.service.IMultiPickDataService;
import cn.ibizlab.businesscentral.core.base.filter.MultiPickDataSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"多类选择实体" })
@RestController("CentralApi-multipickdata")
@RequestMapping("")
public class MultiPickDataResource {

    @Autowired
    public IMultiPickDataService multipickdataService;

    @Autowired
    @Lazy
    public MultiPickDataMapping multipickdataMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-MultiPickData-Create-all')")
    @ApiOperation(value = "新建多类选择实体", tags = {"多类选择实体" },  notes = "新建多类选择实体")
	@RequestMapping(method = RequestMethod.POST, value = "/multipickdata")
    public ResponseEntity<MultiPickDataDTO> create(@RequestBody MultiPickDataDTO multipickdatadto) {
        MultiPickData domain = multipickdataMapping.toDomain(multipickdatadto);
		multipickdataService.create(domain);
        MultiPickDataDTO dto = multipickdataMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-MultiPickData-Create-all')")
    @ApiOperation(value = "批量新建多类选择实体", tags = {"多类选择实体" },  notes = "批量新建多类选择实体")
	@RequestMapping(method = RequestMethod.POST, value = "/multipickdata/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<MultiPickDataDTO> multipickdatadtos) {
        multipickdataService.createBatch(multipickdataMapping.toDomain(multipickdatadtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-MultiPickData-Update-all')")
    @ApiOperation(value = "更新多类选择实体", tags = {"多类选择实体" },  notes = "更新多类选择实体")
	@RequestMapping(method = RequestMethod.PUT, value = "/multipickdata/{multipickdata_id}")
    public ResponseEntity<MultiPickDataDTO> update(@PathVariable("multipickdata_id") String multipickdata_id, @RequestBody MultiPickDataDTO multipickdatadto) {
		MultiPickData domain  = multipickdataMapping.toDomain(multipickdatadto);
        domain .setPickdataid(multipickdata_id);
		multipickdataService.update(domain );
		MultiPickDataDTO dto = multipickdataMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-MultiPickData-Update-all')")
    @ApiOperation(value = "批量更新多类选择实体", tags = {"多类选择实体" },  notes = "批量更新多类选择实体")
	@RequestMapping(method = RequestMethod.PUT, value = "/multipickdata/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<MultiPickDataDTO> multipickdatadtos) {
        multipickdataService.updateBatch(multipickdataMapping.toDomain(multipickdatadtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-MultiPickData-Remove-all')")
    @ApiOperation(value = "删除多类选择实体", tags = {"多类选择实体" },  notes = "删除多类选择实体")
	@RequestMapping(method = RequestMethod.DELETE, value = "/multipickdata/{multipickdata_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("multipickdata_id") String multipickdata_id) {
         return ResponseEntity.status(HttpStatus.OK).body(multipickdataService.remove(multipickdata_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-MultiPickData-Remove-all')")
    @ApiOperation(value = "批量删除多类选择实体", tags = {"多类选择实体" },  notes = "批量删除多类选择实体")
	@RequestMapping(method = RequestMethod.DELETE, value = "/multipickdata/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        multipickdataService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-MultiPickData-Get-all')")
    @ApiOperation(value = "获取多类选择实体", tags = {"多类选择实体" },  notes = "获取多类选择实体")
	@RequestMapping(method = RequestMethod.GET, value = "/multipickdata/{multipickdata_id}")
    public ResponseEntity<MultiPickDataDTO> get(@PathVariable("multipickdata_id") String multipickdata_id) {
        MultiPickData domain = multipickdataService.get(multipickdata_id);
        MultiPickDataDTO dto = multipickdataMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取多类选择实体草稿", tags = {"多类选择实体" },  notes = "获取多类选择实体草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/multipickdata/getdraft")
    public ResponseEntity<MultiPickDataDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(multipickdataMapping.toDto(multipickdataService.getDraft(new MultiPickData())));
    }

    @ApiOperation(value = "检查多类选择实体", tags = {"多类选择实体" },  notes = "检查多类选择实体")
	@RequestMapping(method = RequestMethod.POST, value = "/multipickdata/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody MultiPickDataDTO multipickdatadto) {
        return  ResponseEntity.status(HttpStatus.OK).body(multipickdataService.checkKey(multipickdataMapping.toDomain(multipickdatadto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-MultiPickData-Save-all')")
    @ApiOperation(value = "保存多类选择实体", tags = {"多类选择实体" },  notes = "保存多类选择实体")
	@RequestMapping(method = RequestMethod.POST, value = "/multipickdata/save")
    public ResponseEntity<Boolean> save(@RequestBody MultiPickDataDTO multipickdatadto) {
        return ResponseEntity.status(HttpStatus.OK).body(multipickdataService.save(multipickdataMapping.toDomain(multipickdatadto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-MultiPickData-Save-all')")
    @ApiOperation(value = "批量保存多类选择实体", tags = {"多类选择实体" },  notes = "批量保存多类选择实体")
	@RequestMapping(method = RequestMethod.POST, value = "/multipickdata/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<MultiPickDataDTO> multipickdatadtos) {
        multipickdataService.saveBatch(multipickdataMapping.toDomain(multipickdatadtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-MultiPickData-searchAC-all')")
	@ApiOperation(value = "获取客户、联系人", tags = {"多类选择实体" } ,notes = "获取客户、联系人")
    @RequestMapping(method= RequestMethod.GET , value="/multipickdata/fetchac")
	public ResponseEntity<List<MultiPickDataDTO>> fetchAC(MultiPickDataSearchContext context) {
        Page<MultiPickData> domains = multipickdataService.searchAC(context) ;
        List<MultiPickDataDTO> list = multipickdataMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-MultiPickData-searchAC-all')")
	@ApiOperation(value = "查询客户、联系人", tags = {"多类选择实体" } ,notes = "查询客户、联系人")
    @RequestMapping(method= RequestMethod.POST , value="/multipickdata/searchac")
	public ResponseEntity<Page<MultiPickDataDTO>> searchAC(@RequestBody MultiPickDataSearchContext context) {
        Page<MultiPickData> domains = multipickdataService.searchAC(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(multipickdataMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-MultiPickData-searchACL-all')")
	@ApiOperation(value = "获取客户、联系人、潜在客户", tags = {"多类选择实体" } ,notes = "获取客户、联系人、潜在客户")
    @RequestMapping(method= RequestMethod.GET , value="/multipickdata/fetchacl")
	public ResponseEntity<List<MultiPickDataDTO>> fetchACL(MultiPickDataSearchContext context) {
        Page<MultiPickData> domains = multipickdataService.searchACL(context) ;
        List<MultiPickDataDTO> list = multipickdataMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-MultiPickData-searchACL-all')")
	@ApiOperation(value = "查询客户、联系人、潜在客户", tags = {"多类选择实体" } ,notes = "查询客户、联系人、潜在客户")
    @RequestMapping(method= RequestMethod.POST , value="/multipickdata/searchacl")
	public ResponseEntity<Page<MultiPickDataDTO>> searchACL(@RequestBody MultiPickDataSearchContext context) {
        Page<MultiPickData> domains = multipickdataService.searchACL(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(multipickdataMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-MultiPickData-searchDefault-all')")
	@ApiOperation(value = "获取DEFAULT", tags = {"多类选择实体" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/multipickdata/fetchdefault")
	public ResponseEntity<List<MultiPickDataDTO>> fetchDefault(MultiPickDataSearchContext context) {
        Page<MultiPickData> domains = multipickdataService.searchDefault(context) ;
        List<MultiPickDataDTO> list = multipickdataMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-MultiPickData-searchDefault-all')")
	@ApiOperation(value = "查询DEFAULT", tags = {"多类选择实体" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/multipickdata/searchdefault")
	public ResponseEntity<Page<MultiPickDataDTO>> searchDefault(@RequestBody MultiPickDataSearchContext context) {
        Page<MultiPickData> domains = multipickdataService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(multipickdataMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}


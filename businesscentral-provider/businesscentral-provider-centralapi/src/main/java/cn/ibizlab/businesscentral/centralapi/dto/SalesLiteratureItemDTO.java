package cn.ibizlab.businesscentral.centralapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[SalesLiteratureItemDTO]
 */
@Data
public class SalesLiteratureItemDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [AUTHORNAME]
     *
     */
    @JSONField(name = "authorname")
    @JsonProperty("authorname")
    private String authorname;

    /**
     * 属性 [TIMEZONERULEVERSIONNUMBER]
     *
     */
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;

    /**
     * 属性 [TITLE]
     *
     */
    @JSONField(name = "title")
    @JsonProperty("title")
    private String title;

    /**
     * 属性 [FILESIZE]
     *
     */
    @JSONField(name = "filesize")
    @JsonProperty("filesize")
    private Integer filesize;

    /**
     * 属性 [MODE]
     *
     */
    @JSONField(name = "mode")
    @JsonProperty("mode")
    private String mode;

    /**
     * 属性 [FILETYPECODE]
     *
     */
    @JSONField(name = "filetypecode")
    @JsonProperty("filetypecode")
    private String filetypecode;

    /**
     * 属性 [IMPORTSEQUENCENUMBER]
     *
     */
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;

    /**
     * 属性 [SALESLITERATUREITEMID]
     *
     */
    @JSONField(name = "salesliteratureitemid")
    @JsonProperty("salesliteratureitemid")
    private String salesliteratureitemid;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;

    /**
     * 属性 [MIMETYPE]
     *
     */
    @JSONField(name = "mimetype")
    @JsonProperty("mimetype")
    private String mimetype;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;

    /**
     * 属性 [KEYWORDS]
     *
     */
    @JSONField(name = "keywords")
    @JsonProperty("keywords")
    private String keywords;

    /**
     * 属性 [FILETYPE]
     *
     */
    @JSONField(name = "filetype")
    @JsonProperty("filetype")
    private Integer filetype;

    /**
     * 属性 [ATTACHEDDOCUMENTURL]
     *
     */
    @JSONField(name = "attacheddocumenturl")
    @JsonProperty("attacheddocumenturl")
    private String attacheddocumenturl;

    /**
     * 属性 [DOCUMENTBODY]
     *
     */
    @JSONField(name = "documentbody")
    @JsonProperty("documentbody")
    private String documentbody;

    /**
     * 属性 [VERSIONNUMBER]
     *
     */
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;

    /**
     * 属性 [ABSTRACT]
     *
     */
    @JSONField(name = "ibizabstract")
    @JsonProperty("ibizabstract")
    private String ibizabstract;

    /**
     * 属性 [OVERRIDDENCREATEDON]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;

    /**
     * 属性 [CUSTOMERVIEWABLE]
     *
     */
    @JSONField(name = "customerviewable")
    @JsonProperty("customerviewable")
    private Integer customerviewable;

    /**
     * 属性 [FILENAME]
     *
     */
    @JSONField(name = "filename")
    @JsonProperty("filename")
    private String filename;

    /**
     * 属性 [ORGANIZATIONID]
     *
     */
    @JSONField(name = "organizationid")
    @JsonProperty("organizationid")
    private String organizationid;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [UTCCONVERSIONTIMEZONECODE]
     *
     */
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;

    /**
     * 属性 [SALESLITERATUREID]
     *
     */
    @JSONField(name = "salesliteratureid")
    @JsonProperty("salesliteratureid")
    private String salesliteratureid;


    /**
     * 设置 [AUTHORNAME]
     */
    public void setAuthorname(String  authorname){
        this.authorname = authorname ;
        this.modify("authorname",authorname);
    }

    /**
     * 设置 [TIMEZONERULEVERSIONNUMBER]
     */
    public void setTimezoneruleversionnumber(Integer  timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [TITLE]
     */
    public void setTitle(String  title){
        this.title = title ;
        this.modify("title",title);
    }

    /**
     * 设置 [FILESIZE]
     */
    public void setFilesize(Integer  filesize){
        this.filesize = filesize ;
        this.modify("filesize",filesize);
    }

    /**
     * 设置 [MODE]
     */
    public void setMode(String  mode){
        this.mode = mode ;
        this.modify("mode",mode);
    }

    /**
     * 设置 [FILETYPECODE]
     */
    public void setFiletypecode(String  filetypecode){
        this.filetypecode = filetypecode ;
        this.modify("filetypecode",filetypecode);
    }

    /**
     * 设置 [IMPORTSEQUENCENUMBER]
     */
    public void setImportsequencenumber(Integer  importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [MIMETYPE]
     */
    public void setMimetype(String  mimetype){
        this.mimetype = mimetype ;
        this.modify("mimetype",mimetype);
    }

    /**
     * 设置 [KEYWORDS]
     */
    public void setKeywords(String  keywords){
        this.keywords = keywords ;
        this.modify("keywords",keywords);
    }

    /**
     * 设置 [FILETYPE]
     */
    public void setFiletype(Integer  filetype){
        this.filetype = filetype ;
        this.modify("filetype",filetype);
    }

    /**
     * 设置 [ATTACHEDDOCUMENTURL]
     */
    public void setAttacheddocumenturl(String  attacheddocumenturl){
        this.attacheddocumenturl = attacheddocumenturl ;
        this.modify("attacheddocumenturl",attacheddocumenturl);
    }

    /**
     * 设置 [DOCUMENTBODY]
     */
    public void setDocumentbody(String  documentbody){
        this.documentbody = documentbody ;
        this.modify("documentbody",documentbody);
    }

    /**
     * 设置 [VERSIONNUMBER]
     */
    public void setVersionnumber(BigInteger  versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [ABSTRACT]
     */
    public void setIbizabstract(String  ibizabstract){
        this.ibizabstract = ibizabstract ;
        this.modify("abstract",ibizabstract);
    }

    /**
     * 设置 [OVERRIDDENCREATEDON]
     */
    public void setOverriddencreatedon(Timestamp  overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 设置 [CUSTOMERVIEWABLE]
     */
    public void setCustomerviewable(Integer  customerviewable){
        this.customerviewable = customerviewable ;
        this.modify("customerviewable",customerviewable);
    }

    /**
     * 设置 [FILENAME]
     */
    public void setFilename(String  filename){
        this.filename = filename ;
        this.modify("filename",filename);
    }

    /**
     * 设置 [ORGANIZATIONID]
     */
    public void setOrganizationid(String  organizationid){
        this.organizationid = organizationid ;
        this.modify("organizationid",organizationid);
    }

    /**
     * 设置 [UTCCONVERSIONTIMEZONECODE]
     */
    public void setUtcconversiontimezonecode(Integer  utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [SALESLITERATUREID]
     */
    public void setSalesliteratureid(String  salesliteratureid){
        this.salesliteratureid = salesliteratureid ;
        this.modify("salesliteratureid",salesliteratureid);
    }


}


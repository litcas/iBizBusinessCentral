package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.base.domain.OMHierarchyCat;
import cn.ibizlab.businesscentral.core.base.service.IOMHierarchyCatService;
import cn.ibizlab.businesscentral.core.base.filter.OMHierarchyCatSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"结构层次类别" })
@RestController("CentralApi-omhierarchycat")
@RequestMapping("")
public class OMHierarchyCatResource {

    @Autowired
    public IOMHierarchyCatService omhierarchycatService;

    @Autowired
    @Lazy
    public OMHierarchyCatMapping omhierarchycatMapping;

    @PreAuthorize("hasPermission(this.omhierarchycatMapping.toDomain(#omhierarchycatdto),'iBizBusinessCentral-OMHierarchyCat-Create')")
    @ApiOperation(value = "新建结构层次类别", tags = {"结构层次类别" },  notes = "新建结构层次类别")
	@RequestMapping(method = RequestMethod.POST, value = "/omhierarchycats")
    public ResponseEntity<OMHierarchyCatDTO> create(@RequestBody OMHierarchyCatDTO omhierarchycatdto) {
        OMHierarchyCat domain = omhierarchycatMapping.toDomain(omhierarchycatdto);
		omhierarchycatService.create(domain);
        OMHierarchyCatDTO dto = omhierarchycatMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.omhierarchycatMapping.toDomain(#omhierarchycatdtos),'iBizBusinessCentral-OMHierarchyCat-Create')")
    @ApiOperation(value = "批量新建结构层次类别", tags = {"结构层次类别" },  notes = "批量新建结构层次类别")
	@RequestMapping(method = RequestMethod.POST, value = "/omhierarchycats/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<OMHierarchyCatDTO> omhierarchycatdtos) {
        omhierarchycatService.createBatch(omhierarchycatMapping.toDomain(omhierarchycatdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "omhierarchycat" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.omhierarchycatService.get(#omhierarchycat_id),'iBizBusinessCentral-OMHierarchyCat-Update')")
    @ApiOperation(value = "更新结构层次类别", tags = {"结构层次类别" },  notes = "更新结构层次类别")
	@RequestMapping(method = RequestMethod.PUT, value = "/omhierarchycats/{omhierarchycat_id}")
    public ResponseEntity<OMHierarchyCatDTO> update(@PathVariable("omhierarchycat_id") String omhierarchycat_id, @RequestBody OMHierarchyCatDTO omhierarchycatdto) {
		OMHierarchyCat domain  = omhierarchycatMapping.toDomain(omhierarchycatdto);
        domain .setOmhierarchycatid(omhierarchycat_id);
		omhierarchycatService.update(domain );
		OMHierarchyCatDTO dto = omhierarchycatMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.omhierarchycatService.getOmhierarchycatByEntities(this.omhierarchycatMapping.toDomain(#omhierarchycatdtos)),'iBizBusinessCentral-OMHierarchyCat-Update')")
    @ApiOperation(value = "批量更新结构层次类别", tags = {"结构层次类别" },  notes = "批量更新结构层次类别")
	@RequestMapping(method = RequestMethod.PUT, value = "/omhierarchycats/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<OMHierarchyCatDTO> omhierarchycatdtos) {
        omhierarchycatService.updateBatch(omhierarchycatMapping.toDomain(omhierarchycatdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.omhierarchycatService.get(#omhierarchycat_id),'iBizBusinessCentral-OMHierarchyCat-Remove')")
    @ApiOperation(value = "删除结构层次类别", tags = {"结构层次类别" },  notes = "删除结构层次类别")
	@RequestMapping(method = RequestMethod.DELETE, value = "/omhierarchycats/{omhierarchycat_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("omhierarchycat_id") String omhierarchycat_id) {
         return ResponseEntity.status(HttpStatus.OK).body(omhierarchycatService.remove(omhierarchycat_id));
    }

    @PreAuthorize("hasPermission(this.omhierarchycatService.getOmhierarchycatByIds(#ids),'iBizBusinessCentral-OMHierarchyCat-Remove')")
    @ApiOperation(value = "批量删除结构层次类别", tags = {"结构层次类别" },  notes = "批量删除结构层次类别")
	@RequestMapping(method = RequestMethod.DELETE, value = "/omhierarchycats/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        omhierarchycatService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.omhierarchycatMapping.toDomain(returnObject.body),'iBizBusinessCentral-OMHierarchyCat-Get')")
    @ApiOperation(value = "获取结构层次类别", tags = {"结构层次类别" },  notes = "获取结构层次类别")
	@RequestMapping(method = RequestMethod.GET, value = "/omhierarchycats/{omhierarchycat_id}")
    public ResponseEntity<OMHierarchyCatDTO> get(@PathVariable("omhierarchycat_id") String omhierarchycat_id) {
        OMHierarchyCat domain = omhierarchycatService.get(omhierarchycat_id);
        OMHierarchyCatDTO dto = omhierarchycatMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取结构层次类别草稿", tags = {"结构层次类别" },  notes = "获取结构层次类别草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/omhierarchycats/getdraft")
    public ResponseEntity<OMHierarchyCatDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(omhierarchycatMapping.toDto(omhierarchycatService.getDraft(new OMHierarchyCat())));
    }

    @ApiOperation(value = "检查结构层次类别", tags = {"结构层次类别" },  notes = "检查结构层次类别")
	@RequestMapping(method = RequestMethod.POST, value = "/omhierarchycats/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody OMHierarchyCatDTO omhierarchycatdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(omhierarchycatService.checkKey(omhierarchycatMapping.toDomain(omhierarchycatdto)));
    }

    @PreAuthorize("hasPermission(this.omhierarchycatMapping.toDomain(#omhierarchycatdto),'iBizBusinessCentral-OMHierarchyCat-Save')")
    @ApiOperation(value = "保存结构层次类别", tags = {"结构层次类别" },  notes = "保存结构层次类别")
	@RequestMapping(method = RequestMethod.POST, value = "/omhierarchycats/save")
    public ResponseEntity<Boolean> save(@RequestBody OMHierarchyCatDTO omhierarchycatdto) {
        return ResponseEntity.status(HttpStatus.OK).body(omhierarchycatService.save(omhierarchycatMapping.toDomain(omhierarchycatdto)));
    }

    @PreAuthorize("hasPermission(this.omhierarchycatMapping.toDomain(#omhierarchycatdtos),'iBizBusinessCentral-OMHierarchyCat-Save')")
    @ApiOperation(value = "批量保存结构层次类别", tags = {"结构层次类别" },  notes = "批量保存结构层次类别")
	@RequestMapping(method = RequestMethod.POST, value = "/omhierarchycats/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<OMHierarchyCatDTO> omhierarchycatdtos) {
        omhierarchycatService.saveBatch(omhierarchycatMapping.toDomain(omhierarchycatdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OMHierarchyCat-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OMHierarchyCat-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"结构层次类别" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/omhierarchycats/fetchdefault")
	public ResponseEntity<List<OMHierarchyCatDTO>> fetchDefault(OMHierarchyCatSearchContext context) {
        Page<OMHierarchyCat> domains = omhierarchycatService.searchDefault(context) ;
        List<OMHierarchyCatDTO> list = omhierarchycatMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OMHierarchyCat-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OMHierarchyCat-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"结构层次类别" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/omhierarchycats/searchdefault")
	public ResponseEntity<Page<OMHierarchyCatDTO>> searchDefault(@RequestBody OMHierarchyCatSearchContext context) {
        Page<OMHierarchyCat> domains = omhierarchycatService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(omhierarchycatMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}


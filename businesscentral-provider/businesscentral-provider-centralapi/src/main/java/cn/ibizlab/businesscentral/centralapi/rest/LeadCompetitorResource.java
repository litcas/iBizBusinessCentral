package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.sales.domain.LeadCompetitor;
import cn.ibizlab.businesscentral.core.sales.service.ILeadCompetitorService;
import cn.ibizlab.businesscentral.core.sales.filter.LeadCompetitorSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"潜在客户对手" })
@RestController("CentralApi-leadcompetitor")
@RequestMapping("")
public class LeadCompetitorResource {

    @Autowired
    public ILeadCompetitorService leadcompetitorService;

    @Autowired
    @Lazy
    public LeadCompetitorMapping leadcompetitorMapping;

    @PreAuthorize("hasPermission(this.leadcompetitorMapping.toDomain(#leadcompetitordto),'iBizBusinessCentral-LeadCompetitor-Create')")
    @ApiOperation(value = "新建潜在客户对手", tags = {"潜在客户对手" },  notes = "新建潜在客户对手")
	@RequestMapping(method = RequestMethod.POST, value = "/leadcompetitors")
    public ResponseEntity<LeadCompetitorDTO> create(@RequestBody LeadCompetitorDTO leadcompetitordto) {
        LeadCompetitor domain = leadcompetitorMapping.toDomain(leadcompetitordto);
		leadcompetitorService.create(domain);
        LeadCompetitorDTO dto = leadcompetitorMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.leadcompetitorMapping.toDomain(#leadcompetitordtos),'iBizBusinessCentral-LeadCompetitor-Create')")
    @ApiOperation(value = "批量新建潜在客户对手", tags = {"潜在客户对手" },  notes = "批量新建潜在客户对手")
	@RequestMapping(method = RequestMethod.POST, value = "/leadcompetitors/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<LeadCompetitorDTO> leadcompetitordtos) {
        leadcompetitorService.createBatch(leadcompetitorMapping.toDomain(leadcompetitordtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "leadcompetitor" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.leadcompetitorService.get(#leadcompetitor_id),'iBizBusinessCentral-LeadCompetitor-Update')")
    @ApiOperation(value = "更新潜在客户对手", tags = {"潜在客户对手" },  notes = "更新潜在客户对手")
	@RequestMapping(method = RequestMethod.PUT, value = "/leadcompetitors/{leadcompetitor_id}")
    public ResponseEntity<LeadCompetitorDTO> update(@PathVariable("leadcompetitor_id") String leadcompetitor_id, @RequestBody LeadCompetitorDTO leadcompetitordto) {
		LeadCompetitor domain  = leadcompetitorMapping.toDomain(leadcompetitordto);
        domain .setRelationshipsid(leadcompetitor_id);
		leadcompetitorService.update(domain );
		LeadCompetitorDTO dto = leadcompetitorMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.leadcompetitorService.getLeadcompetitorByEntities(this.leadcompetitorMapping.toDomain(#leadcompetitordtos)),'iBizBusinessCentral-LeadCompetitor-Update')")
    @ApiOperation(value = "批量更新潜在客户对手", tags = {"潜在客户对手" },  notes = "批量更新潜在客户对手")
	@RequestMapping(method = RequestMethod.PUT, value = "/leadcompetitors/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<LeadCompetitorDTO> leadcompetitordtos) {
        leadcompetitorService.updateBatch(leadcompetitorMapping.toDomain(leadcompetitordtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.leadcompetitorService.get(#leadcompetitor_id),'iBizBusinessCentral-LeadCompetitor-Remove')")
    @ApiOperation(value = "删除潜在客户对手", tags = {"潜在客户对手" },  notes = "删除潜在客户对手")
	@RequestMapping(method = RequestMethod.DELETE, value = "/leadcompetitors/{leadcompetitor_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("leadcompetitor_id") String leadcompetitor_id) {
         return ResponseEntity.status(HttpStatus.OK).body(leadcompetitorService.remove(leadcompetitor_id));
    }

    @PreAuthorize("hasPermission(this.leadcompetitorService.getLeadcompetitorByIds(#ids),'iBizBusinessCentral-LeadCompetitor-Remove')")
    @ApiOperation(value = "批量删除潜在客户对手", tags = {"潜在客户对手" },  notes = "批量删除潜在客户对手")
	@RequestMapping(method = RequestMethod.DELETE, value = "/leadcompetitors/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        leadcompetitorService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.leadcompetitorMapping.toDomain(returnObject.body),'iBizBusinessCentral-LeadCompetitor-Get')")
    @ApiOperation(value = "获取潜在客户对手", tags = {"潜在客户对手" },  notes = "获取潜在客户对手")
	@RequestMapping(method = RequestMethod.GET, value = "/leadcompetitors/{leadcompetitor_id}")
    public ResponseEntity<LeadCompetitorDTO> get(@PathVariable("leadcompetitor_id") String leadcompetitor_id) {
        LeadCompetitor domain = leadcompetitorService.get(leadcompetitor_id);
        LeadCompetitorDTO dto = leadcompetitorMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取潜在客户对手草稿", tags = {"潜在客户对手" },  notes = "获取潜在客户对手草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/leadcompetitors/getdraft")
    public ResponseEntity<LeadCompetitorDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(leadcompetitorMapping.toDto(leadcompetitorService.getDraft(new LeadCompetitor())));
    }

    @ApiOperation(value = "检查潜在客户对手", tags = {"潜在客户对手" },  notes = "检查潜在客户对手")
	@RequestMapping(method = RequestMethod.POST, value = "/leadcompetitors/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody LeadCompetitorDTO leadcompetitordto) {
        return  ResponseEntity.status(HttpStatus.OK).body(leadcompetitorService.checkKey(leadcompetitorMapping.toDomain(leadcompetitordto)));
    }

    @PreAuthorize("hasPermission(this.leadcompetitorMapping.toDomain(#leadcompetitordto),'iBizBusinessCentral-LeadCompetitor-Save')")
    @ApiOperation(value = "保存潜在客户对手", tags = {"潜在客户对手" },  notes = "保存潜在客户对手")
	@RequestMapping(method = RequestMethod.POST, value = "/leadcompetitors/save")
    public ResponseEntity<Boolean> save(@RequestBody LeadCompetitorDTO leadcompetitordto) {
        return ResponseEntity.status(HttpStatus.OK).body(leadcompetitorService.save(leadcompetitorMapping.toDomain(leadcompetitordto)));
    }

    @PreAuthorize("hasPermission(this.leadcompetitorMapping.toDomain(#leadcompetitordtos),'iBizBusinessCentral-LeadCompetitor-Save')")
    @ApiOperation(value = "批量保存潜在客户对手", tags = {"潜在客户对手" },  notes = "批量保存潜在客户对手")
	@RequestMapping(method = RequestMethod.POST, value = "/leadcompetitors/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<LeadCompetitorDTO> leadcompetitordtos) {
        leadcompetitorService.saveBatch(leadcompetitorMapping.toDomain(leadcompetitordtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-LeadCompetitor-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-LeadCompetitor-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"潜在客户对手" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/leadcompetitors/fetchdefault")
	public ResponseEntity<List<LeadCompetitorDTO>> fetchDefault(LeadCompetitorSearchContext context) {
        Page<LeadCompetitor> domains = leadcompetitorService.searchDefault(context) ;
        List<LeadCompetitorDTO> list = leadcompetitorMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-LeadCompetitor-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-LeadCompetitor-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"潜在客户对手" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/leadcompetitors/searchdefault")
	public ResponseEntity<Page<LeadCompetitorDTO>> searchDefault(@RequestBody LeadCompetitorSearchContext context) {
        Page<LeadCompetitor> domains = leadcompetitorService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(leadcompetitorMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.leadcompetitorMapping.toDomain(#leadcompetitordto),'iBizBusinessCentral-LeadCompetitor-Create')")
    @ApiOperation(value = "根据竞争对手建立潜在客户对手", tags = {"潜在客户对手" },  notes = "根据竞争对手建立潜在客户对手")
	@RequestMapping(method = RequestMethod.POST, value = "/competitors/{competitor_id}/leadcompetitors")
    public ResponseEntity<LeadCompetitorDTO> createByCompetitor(@PathVariable("competitor_id") String competitor_id, @RequestBody LeadCompetitorDTO leadcompetitordto) {
        LeadCompetitor domain = leadcompetitorMapping.toDomain(leadcompetitordto);
        domain.setEntity2id(competitor_id);
		leadcompetitorService.create(domain);
        LeadCompetitorDTO dto = leadcompetitorMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.leadcompetitorMapping.toDomain(#leadcompetitordtos),'iBizBusinessCentral-LeadCompetitor-Create')")
    @ApiOperation(value = "根据竞争对手批量建立潜在客户对手", tags = {"潜在客户对手" },  notes = "根据竞争对手批量建立潜在客户对手")
	@RequestMapping(method = RequestMethod.POST, value = "/competitors/{competitor_id}/leadcompetitors/batch")
    public ResponseEntity<Boolean> createBatchByCompetitor(@PathVariable("competitor_id") String competitor_id, @RequestBody List<LeadCompetitorDTO> leadcompetitordtos) {
        List<LeadCompetitor> domainlist=leadcompetitorMapping.toDomain(leadcompetitordtos);
        for(LeadCompetitor domain:domainlist){
            domain.setEntity2id(competitor_id);
        }
        leadcompetitorService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "leadcompetitor" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.leadcompetitorService.get(#leadcompetitor_id),'iBizBusinessCentral-LeadCompetitor-Update')")
    @ApiOperation(value = "根据竞争对手更新潜在客户对手", tags = {"潜在客户对手" },  notes = "根据竞争对手更新潜在客户对手")
	@RequestMapping(method = RequestMethod.PUT, value = "/competitors/{competitor_id}/leadcompetitors/{leadcompetitor_id}")
    public ResponseEntity<LeadCompetitorDTO> updateByCompetitor(@PathVariable("competitor_id") String competitor_id, @PathVariable("leadcompetitor_id") String leadcompetitor_id, @RequestBody LeadCompetitorDTO leadcompetitordto) {
        LeadCompetitor domain = leadcompetitorMapping.toDomain(leadcompetitordto);
        domain.setEntity2id(competitor_id);
        domain.setRelationshipsid(leadcompetitor_id);
		leadcompetitorService.update(domain);
        LeadCompetitorDTO dto = leadcompetitorMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.leadcompetitorService.getLeadcompetitorByEntities(this.leadcompetitorMapping.toDomain(#leadcompetitordtos)),'iBizBusinessCentral-LeadCompetitor-Update')")
    @ApiOperation(value = "根据竞争对手批量更新潜在客户对手", tags = {"潜在客户对手" },  notes = "根据竞争对手批量更新潜在客户对手")
	@RequestMapping(method = RequestMethod.PUT, value = "/competitors/{competitor_id}/leadcompetitors/batch")
    public ResponseEntity<Boolean> updateBatchByCompetitor(@PathVariable("competitor_id") String competitor_id, @RequestBody List<LeadCompetitorDTO> leadcompetitordtos) {
        List<LeadCompetitor> domainlist=leadcompetitorMapping.toDomain(leadcompetitordtos);
        for(LeadCompetitor domain:domainlist){
            domain.setEntity2id(competitor_id);
        }
        leadcompetitorService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.leadcompetitorService.get(#leadcompetitor_id),'iBizBusinessCentral-LeadCompetitor-Remove')")
    @ApiOperation(value = "根据竞争对手删除潜在客户对手", tags = {"潜在客户对手" },  notes = "根据竞争对手删除潜在客户对手")
	@RequestMapping(method = RequestMethod.DELETE, value = "/competitors/{competitor_id}/leadcompetitors/{leadcompetitor_id}")
    public ResponseEntity<Boolean> removeByCompetitor(@PathVariable("competitor_id") String competitor_id, @PathVariable("leadcompetitor_id") String leadcompetitor_id) {
		return ResponseEntity.status(HttpStatus.OK).body(leadcompetitorService.remove(leadcompetitor_id));
    }

    @PreAuthorize("hasPermission(this.leadcompetitorService.getLeadcompetitorByIds(#ids),'iBizBusinessCentral-LeadCompetitor-Remove')")
    @ApiOperation(value = "根据竞争对手批量删除潜在客户对手", tags = {"潜在客户对手" },  notes = "根据竞争对手批量删除潜在客户对手")
	@RequestMapping(method = RequestMethod.DELETE, value = "/competitors/{competitor_id}/leadcompetitors/batch")
    public ResponseEntity<Boolean> removeBatchByCompetitor(@RequestBody List<String> ids) {
        leadcompetitorService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.leadcompetitorMapping.toDomain(returnObject.body),'iBizBusinessCentral-LeadCompetitor-Get')")
    @ApiOperation(value = "根据竞争对手获取潜在客户对手", tags = {"潜在客户对手" },  notes = "根据竞争对手获取潜在客户对手")
	@RequestMapping(method = RequestMethod.GET, value = "/competitors/{competitor_id}/leadcompetitors/{leadcompetitor_id}")
    public ResponseEntity<LeadCompetitorDTO> getByCompetitor(@PathVariable("competitor_id") String competitor_id, @PathVariable("leadcompetitor_id") String leadcompetitor_id) {
        LeadCompetitor domain = leadcompetitorService.get(leadcompetitor_id);
        LeadCompetitorDTO dto = leadcompetitorMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据竞争对手获取潜在客户对手草稿", tags = {"潜在客户对手" },  notes = "根据竞争对手获取潜在客户对手草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/competitors/{competitor_id}/leadcompetitors/getdraft")
    public ResponseEntity<LeadCompetitorDTO> getDraftByCompetitor(@PathVariable("competitor_id") String competitor_id) {
        LeadCompetitor domain = new LeadCompetitor();
        domain.setEntity2id(competitor_id);
        return ResponseEntity.status(HttpStatus.OK).body(leadcompetitorMapping.toDto(leadcompetitorService.getDraft(domain)));
    }

    @ApiOperation(value = "根据竞争对手检查潜在客户对手", tags = {"潜在客户对手" },  notes = "根据竞争对手检查潜在客户对手")
	@RequestMapping(method = RequestMethod.POST, value = "/competitors/{competitor_id}/leadcompetitors/checkkey")
    public ResponseEntity<Boolean> checkKeyByCompetitor(@PathVariable("competitor_id") String competitor_id, @RequestBody LeadCompetitorDTO leadcompetitordto) {
        return  ResponseEntity.status(HttpStatus.OK).body(leadcompetitorService.checkKey(leadcompetitorMapping.toDomain(leadcompetitordto)));
    }

    @PreAuthorize("hasPermission(this.leadcompetitorMapping.toDomain(#leadcompetitordto),'iBizBusinessCentral-LeadCompetitor-Save')")
    @ApiOperation(value = "根据竞争对手保存潜在客户对手", tags = {"潜在客户对手" },  notes = "根据竞争对手保存潜在客户对手")
	@RequestMapping(method = RequestMethod.POST, value = "/competitors/{competitor_id}/leadcompetitors/save")
    public ResponseEntity<Boolean> saveByCompetitor(@PathVariable("competitor_id") String competitor_id, @RequestBody LeadCompetitorDTO leadcompetitordto) {
        LeadCompetitor domain = leadcompetitorMapping.toDomain(leadcompetitordto);
        domain.setEntity2id(competitor_id);
        return ResponseEntity.status(HttpStatus.OK).body(leadcompetitorService.save(domain));
    }

    @PreAuthorize("hasPermission(this.leadcompetitorMapping.toDomain(#leadcompetitordtos),'iBizBusinessCentral-LeadCompetitor-Save')")
    @ApiOperation(value = "根据竞争对手批量保存潜在客户对手", tags = {"潜在客户对手" },  notes = "根据竞争对手批量保存潜在客户对手")
	@RequestMapping(method = RequestMethod.POST, value = "/competitors/{competitor_id}/leadcompetitors/savebatch")
    public ResponseEntity<Boolean> saveBatchByCompetitor(@PathVariable("competitor_id") String competitor_id, @RequestBody List<LeadCompetitorDTO> leadcompetitordtos) {
        List<LeadCompetitor> domainlist=leadcompetitorMapping.toDomain(leadcompetitordtos);
        for(LeadCompetitor domain:domainlist){
             domain.setEntity2id(competitor_id);
        }
        leadcompetitorService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-LeadCompetitor-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-LeadCompetitor-Get')")
	@ApiOperation(value = "根据竞争对手获取DEFAULT", tags = {"潜在客户对手" } ,notes = "根据竞争对手获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/competitors/{competitor_id}/leadcompetitors/fetchdefault")
	public ResponseEntity<List<LeadCompetitorDTO>> fetchLeadCompetitorDefaultByCompetitor(@PathVariable("competitor_id") String competitor_id,LeadCompetitorSearchContext context) {
        context.setN_entity2id_eq(competitor_id);
        Page<LeadCompetitor> domains = leadcompetitorService.searchDefault(context) ;
        List<LeadCompetitorDTO> list = leadcompetitorMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-LeadCompetitor-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-LeadCompetitor-Get')")
	@ApiOperation(value = "根据竞争对手查询DEFAULT", tags = {"潜在客户对手" } ,notes = "根据竞争对手查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/competitors/{competitor_id}/leadcompetitors/searchdefault")
	public ResponseEntity<Page<LeadCompetitorDTO>> searchLeadCompetitorDefaultByCompetitor(@PathVariable("competitor_id") String competitor_id, @RequestBody LeadCompetitorSearchContext context) {
        context.setN_entity2id_eq(competitor_id);
        Page<LeadCompetitor> domains = leadcompetitorService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(leadcompetitorMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.leadcompetitorMapping.toDomain(#leadcompetitordto),'iBizBusinessCentral-LeadCompetitor-Create')")
    @ApiOperation(value = "根据潜在顾客建立潜在客户对手", tags = {"潜在客户对手" },  notes = "根据潜在顾客建立潜在客户对手")
	@RequestMapping(method = RequestMethod.POST, value = "/leads/{lead_id}/leadcompetitors")
    public ResponseEntity<LeadCompetitorDTO> createByLead(@PathVariable("lead_id") String lead_id, @RequestBody LeadCompetitorDTO leadcompetitordto) {
        LeadCompetitor domain = leadcompetitorMapping.toDomain(leadcompetitordto);
        domain.setEntityid(lead_id);
		leadcompetitorService.create(domain);
        LeadCompetitorDTO dto = leadcompetitorMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.leadcompetitorMapping.toDomain(#leadcompetitordtos),'iBizBusinessCentral-LeadCompetitor-Create')")
    @ApiOperation(value = "根据潜在顾客批量建立潜在客户对手", tags = {"潜在客户对手" },  notes = "根据潜在顾客批量建立潜在客户对手")
	@RequestMapping(method = RequestMethod.POST, value = "/leads/{lead_id}/leadcompetitors/batch")
    public ResponseEntity<Boolean> createBatchByLead(@PathVariable("lead_id") String lead_id, @RequestBody List<LeadCompetitorDTO> leadcompetitordtos) {
        List<LeadCompetitor> domainlist=leadcompetitorMapping.toDomain(leadcompetitordtos);
        for(LeadCompetitor domain:domainlist){
            domain.setEntityid(lead_id);
        }
        leadcompetitorService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "leadcompetitor" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.leadcompetitorService.get(#leadcompetitor_id),'iBizBusinessCentral-LeadCompetitor-Update')")
    @ApiOperation(value = "根据潜在顾客更新潜在客户对手", tags = {"潜在客户对手" },  notes = "根据潜在顾客更新潜在客户对手")
	@RequestMapping(method = RequestMethod.PUT, value = "/leads/{lead_id}/leadcompetitors/{leadcompetitor_id}")
    public ResponseEntity<LeadCompetitorDTO> updateByLead(@PathVariable("lead_id") String lead_id, @PathVariable("leadcompetitor_id") String leadcompetitor_id, @RequestBody LeadCompetitorDTO leadcompetitordto) {
        LeadCompetitor domain = leadcompetitorMapping.toDomain(leadcompetitordto);
        domain.setEntityid(lead_id);
        domain.setRelationshipsid(leadcompetitor_id);
		leadcompetitorService.update(domain);
        LeadCompetitorDTO dto = leadcompetitorMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.leadcompetitorService.getLeadcompetitorByEntities(this.leadcompetitorMapping.toDomain(#leadcompetitordtos)),'iBizBusinessCentral-LeadCompetitor-Update')")
    @ApiOperation(value = "根据潜在顾客批量更新潜在客户对手", tags = {"潜在客户对手" },  notes = "根据潜在顾客批量更新潜在客户对手")
	@RequestMapping(method = RequestMethod.PUT, value = "/leads/{lead_id}/leadcompetitors/batch")
    public ResponseEntity<Boolean> updateBatchByLead(@PathVariable("lead_id") String lead_id, @RequestBody List<LeadCompetitorDTO> leadcompetitordtos) {
        List<LeadCompetitor> domainlist=leadcompetitorMapping.toDomain(leadcompetitordtos);
        for(LeadCompetitor domain:domainlist){
            domain.setEntityid(lead_id);
        }
        leadcompetitorService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.leadcompetitorService.get(#leadcompetitor_id),'iBizBusinessCentral-LeadCompetitor-Remove')")
    @ApiOperation(value = "根据潜在顾客删除潜在客户对手", tags = {"潜在客户对手" },  notes = "根据潜在顾客删除潜在客户对手")
	@RequestMapping(method = RequestMethod.DELETE, value = "/leads/{lead_id}/leadcompetitors/{leadcompetitor_id}")
    public ResponseEntity<Boolean> removeByLead(@PathVariable("lead_id") String lead_id, @PathVariable("leadcompetitor_id") String leadcompetitor_id) {
		return ResponseEntity.status(HttpStatus.OK).body(leadcompetitorService.remove(leadcompetitor_id));
    }

    @PreAuthorize("hasPermission(this.leadcompetitorService.getLeadcompetitorByIds(#ids),'iBizBusinessCentral-LeadCompetitor-Remove')")
    @ApiOperation(value = "根据潜在顾客批量删除潜在客户对手", tags = {"潜在客户对手" },  notes = "根据潜在顾客批量删除潜在客户对手")
	@RequestMapping(method = RequestMethod.DELETE, value = "/leads/{lead_id}/leadcompetitors/batch")
    public ResponseEntity<Boolean> removeBatchByLead(@RequestBody List<String> ids) {
        leadcompetitorService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.leadcompetitorMapping.toDomain(returnObject.body),'iBizBusinessCentral-LeadCompetitor-Get')")
    @ApiOperation(value = "根据潜在顾客获取潜在客户对手", tags = {"潜在客户对手" },  notes = "根据潜在顾客获取潜在客户对手")
	@RequestMapping(method = RequestMethod.GET, value = "/leads/{lead_id}/leadcompetitors/{leadcompetitor_id}")
    public ResponseEntity<LeadCompetitorDTO> getByLead(@PathVariable("lead_id") String lead_id, @PathVariable("leadcompetitor_id") String leadcompetitor_id) {
        LeadCompetitor domain = leadcompetitorService.get(leadcompetitor_id);
        LeadCompetitorDTO dto = leadcompetitorMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据潜在顾客获取潜在客户对手草稿", tags = {"潜在客户对手" },  notes = "根据潜在顾客获取潜在客户对手草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/leads/{lead_id}/leadcompetitors/getdraft")
    public ResponseEntity<LeadCompetitorDTO> getDraftByLead(@PathVariable("lead_id") String lead_id) {
        LeadCompetitor domain = new LeadCompetitor();
        domain.setEntityid(lead_id);
        return ResponseEntity.status(HttpStatus.OK).body(leadcompetitorMapping.toDto(leadcompetitorService.getDraft(domain)));
    }

    @ApiOperation(value = "根据潜在顾客检查潜在客户对手", tags = {"潜在客户对手" },  notes = "根据潜在顾客检查潜在客户对手")
	@RequestMapping(method = RequestMethod.POST, value = "/leads/{lead_id}/leadcompetitors/checkkey")
    public ResponseEntity<Boolean> checkKeyByLead(@PathVariable("lead_id") String lead_id, @RequestBody LeadCompetitorDTO leadcompetitordto) {
        return  ResponseEntity.status(HttpStatus.OK).body(leadcompetitorService.checkKey(leadcompetitorMapping.toDomain(leadcompetitordto)));
    }

    @PreAuthorize("hasPermission(this.leadcompetitorMapping.toDomain(#leadcompetitordto),'iBizBusinessCentral-LeadCompetitor-Save')")
    @ApiOperation(value = "根据潜在顾客保存潜在客户对手", tags = {"潜在客户对手" },  notes = "根据潜在顾客保存潜在客户对手")
	@RequestMapping(method = RequestMethod.POST, value = "/leads/{lead_id}/leadcompetitors/save")
    public ResponseEntity<Boolean> saveByLead(@PathVariable("lead_id") String lead_id, @RequestBody LeadCompetitorDTO leadcompetitordto) {
        LeadCompetitor domain = leadcompetitorMapping.toDomain(leadcompetitordto);
        domain.setEntityid(lead_id);
        return ResponseEntity.status(HttpStatus.OK).body(leadcompetitorService.save(domain));
    }

    @PreAuthorize("hasPermission(this.leadcompetitorMapping.toDomain(#leadcompetitordtos),'iBizBusinessCentral-LeadCompetitor-Save')")
    @ApiOperation(value = "根据潜在顾客批量保存潜在客户对手", tags = {"潜在客户对手" },  notes = "根据潜在顾客批量保存潜在客户对手")
	@RequestMapping(method = RequestMethod.POST, value = "/leads/{lead_id}/leadcompetitors/savebatch")
    public ResponseEntity<Boolean> saveBatchByLead(@PathVariable("lead_id") String lead_id, @RequestBody List<LeadCompetitorDTO> leadcompetitordtos) {
        List<LeadCompetitor> domainlist=leadcompetitorMapping.toDomain(leadcompetitordtos);
        for(LeadCompetitor domain:domainlist){
             domain.setEntityid(lead_id);
        }
        leadcompetitorService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-LeadCompetitor-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-LeadCompetitor-Get')")
	@ApiOperation(value = "根据潜在顾客获取DEFAULT", tags = {"潜在客户对手" } ,notes = "根据潜在顾客获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/leads/{lead_id}/leadcompetitors/fetchdefault")
	public ResponseEntity<List<LeadCompetitorDTO>> fetchLeadCompetitorDefaultByLead(@PathVariable("lead_id") String lead_id,LeadCompetitorSearchContext context) {
        context.setN_entityid_eq(lead_id);
        Page<LeadCompetitor> domains = leadcompetitorService.searchDefault(context) ;
        List<LeadCompetitorDTO> list = leadcompetitorMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-LeadCompetitor-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-LeadCompetitor-Get')")
	@ApiOperation(value = "根据潜在顾客查询DEFAULT", tags = {"潜在客户对手" } ,notes = "根据潜在顾客查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/leads/{lead_id}/leadcompetitors/searchdefault")
	public ResponseEntity<Page<LeadCompetitorDTO>> searchLeadCompetitorDefaultByLead(@PathVariable("lead_id") String lead_id, @RequestBody LeadCompetitorSearchContext context) {
        context.setN_entityid_eq(lead_id);
        Page<LeadCompetitor> domains = leadcompetitorService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(leadcompetitorMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.leadcompetitorMapping.toDomain(#leadcompetitordto),'iBizBusinessCentral-LeadCompetitor-Create')")
    @ApiOperation(value = "根据客户潜在顾客建立潜在客户对手", tags = {"潜在客户对手" },  notes = "根据客户潜在顾客建立潜在客户对手")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/leads/{lead_id}/leadcompetitors")
    public ResponseEntity<LeadCompetitorDTO> createByAccountLead(@PathVariable("account_id") String account_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadCompetitorDTO leadcompetitordto) {
        LeadCompetitor domain = leadcompetitorMapping.toDomain(leadcompetitordto);
        domain.setEntityid(lead_id);
		leadcompetitorService.create(domain);
        LeadCompetitorDTO dto = leadcompetitorMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.leadcompetitorMapping.toDomain(#leadcompetitordtos),'iBizBusinessCentral-LeadCompetitor-Create')")
    @ApiOperation(value = "根据客户潜在顾客批量建立潜在客户对手", tags = {"潜在客户对手" },  notes = "根据客户潜在顾客批量建立潜在客户对手")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/leads/{lead_id}/leadcompetitors/batch")
    public ResponseEntity<Boolean> createBatchByAccountLead(@PathVariable("account_id") String account_id, @PathVariable("lead_id") String lead_id, @RequestBody List<LeadCompetitorDTO> leadcompetitordtos) {
        List<LeadCompetitor> domainlist=leadcompetitorMapping.toDomain(leadcompetitordtos);
        for(LeadCompetitor domain:domainlist){
            domain.setEntityid(lead_id);
        }
        leadcompetitorService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "leadcompetitor" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.leadcompetitorService.get(#leadcompetitor_id),'iBizBusinessCentral-LeadCompetitor-Update')")
    @ApiOperation(value = "根据客户潜在顾客更新潜在客户对手", tags = {"潜在客户对手" },  notes = "根据客户潜在顾客更新潜在客户对手")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/leads/{lead_id}/leadcompetitors/{leadcompetitor_id}")
    public ResponseEntity<LeadCompetitorDTO> updateByAccountLead(@PathVariable("account_id") String account_id, @PathVariable("lead_id") String lead_id, @PathVariable("leadcompetitor_id") String leadcompetitor_id, @RequestBody LeadCompetitorDTO leadcompetitordto) {
        LeadCompetitor domain = leadcompetitorMapping.toDomain(leadcompetitordto);
        domain.setEntityid(lead_id);
        domain.setRelationshipsid(leadcompetitor_id);
		leadcompetitorService.update(domain);
        LeadCompetitorDTO dto = leadcompetitorMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.leadcompetitorService.getLeadcompetitorByEntities(this.leadcompetitorMapping.toDomain(#leadcompetitordtos)),'iBizBusinessCentral-LeadCompetitor-Update')")
    @ApiOperation(value = "根据客户潜在顾客批量更新潜在客户对手", tags = {"潜在客户对手" },  notes = "根据客户潜在顾客批量更新潜在客户对手")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/leads/{lead_id}/leadcompetitors/batch")
    public ResponseEntity<Boolean> updateBatchByAccountLead(@PathVariable("account_id") String account_id, @PathVariable("lead_id") String lead_id, @RequestBody List<LeadCompetitorDTO> leadcompetitordtos) {
        List<LeadCompetitor> domainlist=leadcompetitorMapping.toDomain(leadcompetitordtos);
        for(LeadCompetitor domain:domainlist){
            domain.setEntityid(lead_id);
        }
        leadcompetitorService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.leadcompetitorService.get(#leadcompetitor_id),'iBizBusinessCentral-LeadCompetitor-Remove')")
    @ApiOperation(value = "根据客户潜在顾客删除潜在客户对手", tags = {"潜在客户对手" },  notes = "根据客户潜在顾客删除潜在客户对手")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/leads/{lead_id}/leadcompetitors/{leadcompetitor_id}")
    public ResponseEntity<Boolean> removeByAccountLead(@PathVariable("account_id") String account_id, @PathVariable("lead_id") String lead_id, @PathVariable("leadcompetitor_id") String leadcompetitor_id) {
		return ResponseEntity.status(HttpStatus.OK).body(leadcompetitorService.remove(leadcompetitor_id));
    }

    @PreAuthorize("hasPermission(this.leadcompetitorService.getLeadcompetitorByIds(#ids),'iBizBusinessCentral-LeadCompetitor-Remove')")
    @ApiOperation(value = "根据客户潜在顾客批量删除潜在客户对手", tags = {"潜在客户对手" },  notes = "根据客户潜在顾客批量删除潜在客户对手")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/leads/{lead_id}/leadcompetitors/batch")
    public ResponseEntity<Boolean> removeBatchByAccountLead(@RequestBody List<String> ids) {
        leadcompetitorService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.leadcompetitorMapping.toDomain(returnObject.body),'iBizBusinessCentral-LeadCompetitor-Get')")
    @ApiOperation(value = "根据客户潜在顾客获取潜在客户对手", tags = {"潜在客户对手" },  notes = "根据客户潜在顾客获取潜在客户对手")
	@RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/leads/{lead_id}/leadcompetitors/{leadcompetitor_id}")
    public ResponseEntity<LeadCompetitorDTO> getByAccountLead(@PathVariable("account_id") String account_id, @PathVariable("lead_id") String lead_id, @PathVariable("leadcompetitor_id") String leadcompetitor_id) {
        LeadCompetitor domain = leadcompetitorService.get(leadcompetitor_id);
        LeadCompetitorDTO dto = leadcompetitorMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据客户潜在顾客获取潜在客户对手草稿", tags = {"潜在客户对手" },  notes = "根据客户潜在顾客获取潜在客户对手草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/leads/{lead_id}/leadcompetitors/getdraft")
    public ResponseEntity<LeadCompetitorDTO> getDraftByAccountLead(@PathVariable("account_id") String account_id, @PathVariable("lead_id") String lead_id) {
        LeadCompetitor domain = new LeadCompetitor();
        domain.setEntityid(lead_id);
        return ResponseEntity.status(HttpStatus.OK).body(leadcompetitorMapping.toDto(leadcompetitorService.getDraft(domain)));
    }

    @ApiOperation(value = "根据客户潜在顾客检查潜在客户对手", tags = {"潜在客户对手" },  notes = "根据客户潜在顾客检查潜在客户对手")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/leads/{lead_id}/leadcompetitors/checkkey")
    public ResponseEntity<Boolean> checkKeyByAccountLead(@PathVariable("account_id") String account_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadCompetitorDTO leadcompetitordto) {
        return  ResponseEntity.status(HttpStatus.OK).body(leadcompetitorService.checkKey(leadcompetitorMapping.toDomain(leadcompetitordto)));
    }

    @PreAuthorize("hasPermission(this.leadcompetitorMapping.toDomain(#leadcompetitordto),'iBizBusinessCentral-LeadCompetitor-Save')")
    @ApiOperation(value = "根据客户潜在顾客保存潜在客户对手", tags = {"潜在客户对手" },  notes = "根据客户潜在顾客保存潜在客户对手")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/leads/{lead_id}/leadcompetitors/save")
    public ResponseEntity<Boolean> saveByAccountLead(@PathVariable("account_id") String account_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadCompetitorDTO leadcompetitordto) {
        LeadCompetitor domain = leadcompetitorMapping.toDomain(leadcompetitordto);
        domain.setEntityid(lead_id);
        return ResponseEntity.status(HttpStatus.OK).body(leadcompetitorService.save(domain));
    }

    @PreAuthorize("hasPermission(this.leadcompetitorMapping.toDomain(#leadcompetitordtos),'iBizBusinessCentral-LeadCompetitor-Save')")
    @ApiOperation(value = "根据客户潜在顾客批量保存潜在客户对手", tags = {"潜在客户对手" },  notes = "根据客户潜在顾客批量保存潜在客户对手")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/leads/{lead_id}/leadcompetitors/savebatch")
    public ResponseEntity<Boolean> saveBatchByAccountLead(@PathVariable("account_id") String account_id, @PathVariable("lead_id") String lead_id, @RequestBody List<LeadCompetitorDTO> leadcompetitordtos) {
        List<LeadCompetitor> domainlist=leadcompetitorMapping.toDomain(leadcompetitordtos);
        for(LeadCompetitor domain:domainlist){
             domain.setEntityid(lead_id);
        }
        leadcompetitorService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-LeadCompetitor-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-LeadCompetitor-Get')")
	@ApiOperation(value = "根据客户潜在顾客获取DEFAULT", tags = {"潜在客户对手" } ,notes = "根据客户潜在顾客获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/leads/{lead_id}/leadcompetitors/fetchdefault")
	public ResponseEntity<List<LeadCompetitorDTO>> fetchLeadCompetitorDefaultByAccountLead(@PathVariable("account_id") String account_id, @PathVariable("lead_id") String lead_id,LeadCompetitorSearchContext context) {
        context.setN_entityid_eq(lead_id);
        Page<LeadCompetitor> domains = leadcompetitorService.searchDefault(context) ;
        List<LeadCompetitorDTO> list = leadcompetitorMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-LeadCompetitor-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-LeadCompetitor-Get')")
	@ApiOperation(value = "根据客户潜在顾客查询DEFAULT", tags = {"潜在客户对手" } ,notes = "根据客户潜在顾客查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/leads/{lead_id}/leadcompetitors/searchdefault")
	public ResponseEntity<Page<LeadCompetitorDTO>> searchLeadCompetitorDefaultByAccountLead(@PathVariable("account_id") String account_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadCompetitorSearchContext context) {
        context.setN_entityid_eq(lead_id);
        Page<LeadCompetitor> domains = leadcompetitorService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(leadcompetitorMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.leadcompetitorMapping.toDomain(#leadcompetitordto),'iBizBusinessCentral-LeadCompetitor-Create')")
    @ApiOperation(value = "根据市场活动潜在顾客建立潜在客户对手", tags = {"潜在客户对手" },  notes = "根据市场活动潜在顾客建立潜在客户对手")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigns/{campaign_id}/leads/{lead_id}/leadcompetitors")
    public ResponseEntity<LeadCompetitorDTO> createByCampaignLead(@PathVariable("campaign_id") String campaign_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadCompetitorDTO leadcompetitordto) {
        LeadCompetitor domain = leadcompetitorMapping.toDomain(leadcompetitordto);
        domain.setEntityid(lead_id);
		leadcompetitorService.create(domain);
        LeadCompetitorDTO dto = leadcompetitorMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.leadcompetitorMapping.toDomain(#leadcompetitordtos),'iBizBusinessCentral-LeadCompetitor-Create')")
    @ApiOperation(value = "根据市场活动潜在顾客批量建立潜在客户对手", tags = {"潜在客户对手" },  notes = "根据市场活动潜在顾客批量建立潜在客户对手")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigns/{campaign_id}/leads/{lead_id}/leadcompetitors/batch")
    public ResponseEntity<Boolean> createBatchByCampaignLead(@PathVariable("campaign_id") String campaign_id, @PathVariable("lead_id") String lead_id, @RequestBody List<LeadCompetitorDTO> leadcompetitordtos) {
        List<LeadCompetitor> domainlist=leadcompetitorMapping.toDomain(leadcompetitordtos);
        for(LeadCompetitor domain:domainlist){
            domain.setEntityid(lead_id);
        }
        leadcompetitorService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "leadcompetitor" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.leadcompetitorService.get(#leadcompetitor_id),'iBizBusinessCentral-LeadCompetitor-Update')")
    @ApiOperation(value = "根据市场活动潜在顾客更新潜在客户对手", tags = {"潜在客户对手" },  notes = "根据市场活动潜在顾客更新潜在客户对手")
	@RequestMapping(method = RequestMethod.PUT, value = "/campaigns/{campaign_id}/leads/{lead_id}/leadcompetitors/{leadcompetitor_id}")
    public ResponseEntity<LeadCompetitorDTO> updateByCampaignLead(@PathVariable("campaign_id") String campaign_id, @PathVariable("lead_id") String lead_id, @PathVariable("leadcompetitor_id") String leadcompetitor_id, @RequestBody LeadCompetitorDTO leadcompetitordto) {
        LeadCompetitor domain = leadcompetitorMapping.toDomain(leadcompetitordto);
        domain.setEntityid(lead_id);
        domain.setRelationshipsid(leadcompetitor_id);
		leadcompetitorService.update(domain);
        LeadCompetitorDTO dto = leadcompetitorMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.leadcompetitorService.getLeadcompetitorByEntities(this.leadcompetitorMapping.toDomain(#leadcompetitordtos)),'iBizBusinessCentral-LeadCompetitor-Update')")
    @ApiOperation(value = "根据市场活动潜在顾客批量更新潜在客户对手", tags = {"潜在客户对手" },  notes = "根据市场活动潜在顾客批量更新潜在客户对手")
	@RequestMapping(method = RequestMethod.PUT, value = "/campaigns/{campaign_id}/leads/{lead_id}/leadcompetitors/batch")
    public ResponseEntity<Boolean> updateBatchByCampaignLead(@PathVariable("campaign_id") String campaign_id, @PathVariable("lead_id") String lead_id, @RequestBody List<LeadCompetitorDTO> leadcompetitordtos) {
        List<LeadCompetitor> domainlist=leadcompetitorMapping.toDomain(leadcompetitordtos);
        for(LeadCompetitor domain:domainlist){
            domain.setEntityid(lead_id);
        }
        leadcompetitorService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.leadcompetitorService.get(#leadcompetitor_id),'iBizBusinessCentral-LeadCompetitor-Remove')")
    @ApiOperation(value = "根据市场活动潜在顾客删除潜在客户对手", tags = {"潜在客户对手" },  notes = "根据市场活动潜在顾客删除潜在客户对手")
	@RequestMapping(method = RequestMethod.DELETE, value = "/campaigns/{campaign_id}/leads/{lead_id}/leadcompetitors/{leadcompetitor_id}")
    public ResponseEntity<Boolean> removeByCampaignLead(@PathVariable("campaign_id") String campaign_id, @PathVariable("lead_id") String lead_id, @PathVariable("leadcompetitor_id") String leadcompetitor_id) {
		return ResponseEntity.status(HttpStatus.OK).body(leadcompetitorService.remove(leadcompetitor_id));
    }

    @PreAuthorize("hasPermission(this.leadcompetitorService.getLeadcompetitorByIds(#ids),'iBizBusinessCentral-LeadCompetitor-Remove')")
    @ApiOperation(value = "根据市场活动潜在顾客批量删除潜在客户对手", tags = {"潜在客户对手" },  notes = "根据市场活动潜在顾客批量删除潜在客户对手")
	@RequestMapping(method = RequestMethod.DELETE, value = "/campaigns/{campaign_id}/leads/{lead_id}/leadcompetitors/batch")
    public ResponseEntity<Boolean> removeBatchByCampaignLead(@RequestBody List<String> ids) {
        leadcompetitorService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.leadcompetitorMapping.toDomain(returnObject.body),'iBizBusinessCentral-LeadCompetitor-Get')")
    @ApiOperation(value = "根据市场活动潜在顾客获取潜在客户对手", tags = {"潜在客户对手" },  notes = "根据市场活动潜在顾客获取潜在客户对手")
	@RequestMapping(method = RequestMethod.GET, value = "/campaigns/{campaign_id}/leads/{lead_id}/leadcompetitors/{leadcompetitor_id}")
    public ResponseEntity<LeadCompetitorDTO> getByCampaignLead(@PathVariable("campaign_id") String campaign_id, @PathVariable("lead_id") String lead_id, @PathVariable("leadcompetitor_id") String leadcompetitor_id) {
        LeadCompetitor domain = leadcompetitorService.get(leadcompetitor_id);
        LeadCompetitorDTO dto = leadcompetitorMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据市场活动潜在顾客获取潜在客户对手草稿", tags = {"潜在客户对手" },  notes = "根据市场活动潜在顾客获取潜在客户对手草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/campaigns/{campaign_id}/leads/{lead_id}/leadcompetitors/getdraft")
    public ResponseEntity<LeadCompetitorDTO> getDraftByCampaignLead(@PathVariable("campaign_id") String campaign_id, @PathVariable("lead_id") String lead_id) {
        LeadCompetitor domain = new LeadCompetitor();
        domain.setEntityid(lead_id);
        return ResponseEntity.status(HttpStatus.OK).body(leadcompetitorMapping.toDto(leadcompetitorService.getDraft(domain)));
    }

    @ApiOperation(value = "根据市场活动潜在顾客检查潜在客户对手", tags = {"潜在客户对手" },  notes = "根据市场活动潜在顾客检查潜在客户对手")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigns/{campaign_id}/leads/{lead_id}/leadcompetitors/checkkey")
    public ResponseEntity<Boolean> checkKeyByCampaignLead(@PathVariable("campaign_id") String campaign_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadCompetitorDTO leadcompetitordto) {
        return  ResponseEntity.status(HttpStatus.OK).body(leadcompetitorService.checkKey(leadcompetitorMapping.toDomain(leadcompetitordto)));
    }

    @PreAuthorize("hasPermission(this.leadcompetitorMapping.toDomain(#leadcompetitordto),'iBizBusinessCentral-LeadCompetitor-Save')")
    @ApiOperation(value = "根据市场活动潜在顾客保存潜在客户对手", tags = {"潜在客户对手" },  notes = "根据市场活动潜在顾客保存潜在客户对手")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigns/{campaign_id}/leads/{lead_id}/leadcompetitors/save")
    public ResponseEntity<Boolean> saveByCampaignLead(@PathVariable("campaign_id") String campaign_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadCompetitorDTO leadcompetitordto) {
        LeadCompetitor domain = leadcompetitorMapping.toDomain(leadcompetitordto);
        domain.setEntityid(lead_id);
        return ResponseEntity.status(HttpStatus.OK).body(leadcompetitorService.save(domain));
    }

    @PreAuthorize("hasPermission(this.leadcompetitorMapping.toDomain(#leadcompetitordtos),'iBizBusinessCentral-LeadCompetitor-Save')")
    @ApiOperation(value = "根据市场活动潜在顾客批量保存潜在客户对手", tags = {"潜在客户对手" },  notes = "根据市场活动潜在顾客批量保存潜在客户对手")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigns/{campaign_id}/leads/{lead_id}/leadcompetitors/savebatch")
    public ResponseEntity<Boolean> saveBatchByCampaignLead(@PathVariable("campaign_id") String campaign_id, @PathVariable("lead_id") String lead_id, @RequestBody List<LeadCompetitorDTO> leadcompetitordtos) {
        List<LeadCompetitor> domainlist=leadcompetitorMapping.toDomain(leadcompetitordtos);
        for(LeadCompetitor domain:domainlist){
             domain.setEntityid(lead_id);
        }
        leadcompetitorService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-LeadCompetitor-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-LeadCompetitor-Get')")
	@ApiOperation(value = "根据市场活动潜在顾客获取DEFAULT", tags = {"潜在客户对手" } ,notes = "根据市场活动潜在顾客获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/campaigns/{campaign_id}/leads/{lead_id}/leadcompetitors/fetchdefault")
	public ResponseEntity<List<LeadCompetitorDTO>> fetchLeadCompetitorDefaultByCampaignLead(@PathVariable("campaign_id") String campaign_id, @PathVariable("lead_id") String lead_id,LeadCompetitorSearchContext context) {
        context.setN_entityid_eq(lead_id);
        Page<LeadCompetitor> domains = leadcompetitorService.searchDefault(context) ;
        List<LeadCompetitorDTO> list = leadcompetitorMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-LeadCompetitor-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-LeadCompetitor-Get')")
	@ApiOperation(value = "根据市场活动潜在顾客查询DEFAULT", tags = {"潜在客户对手" } ,notes = "根据市场活动潜在顾客查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/campaigns/{campaign_id}/leads/{lead_id}/leadcompetitors/searchdefault")
	public ResponseEntity<Page<LeadCompetitorDTO>> searchLeadCompetitorDefaultByCampaignLead(@PathVariable("campaign_id") String campaign_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadCompetitorSearchContext context) {
        context.setN_entityid_eq(lead_id);
        Page<LeadCompetitor> domains = leadcompetitorService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(leadcompetitorMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.leadcompetitorMapping.toDomain(#leadcompetitordto),'iBizBusinessCentral-LeadCompetitor-Create')")
    @ApiOperation(value = "根据联系人潜在顾客建立潜在客户对手", tags = {"潜在客户对手" },  notes = "根据联系人潜在顾客建立潜在客户对手")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/leads/{lead_id}/leadcompetitors")
    public ResponseEntity<LeadCompetitorDTO> createByContactLead(@PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadCompetitorDTO leadcompetitordto) {
        LeadCompetitor domain = leadcompetitorMapping.toDomain(leadcompetitordto);
        domain.setEntityid(lead_id);
		leadcompetitorService.create(domain);
        LeadCompetitorDTO dto = leadcompetitorMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.leadcompetitorMapping.toDomain(#leadcompetitordtos),'iBizBusinessCentral-LeadCompetitor-Create')")
    @ApiOperation(value = "根据联系人潜在顾客批量建立潜在客户对手", tags = {"潜在客户对手" },  notes = "根据联系人潜在顾客批量建立潜在客户对手")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/leads/{lead_id}/leadcompetitors/batch")
    public ResponseEntity<Boolean> createBatchByContactLead(@PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody List<LeadCompetitorDTO> leadcompetitordtos) {
        List<LeadCompetitor> domainlist=leadcompetitorMapping.toDomain(leadcompetitordtos);
        for(LeadCompetitor domain:domainlist){
            domain.setEntityid(lead_id);
        }
        leadcompetitorService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "leadcompetitor" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.leadcompetitorService.get(#leadcompetitor_id),'iBizBusinessCentral-LeadCompetitor-Update')")
    @ApiOperation(value = "根据联系人潜在顾客更新潜在客户对手", tags = {"潜在客户对手" },  notes = "根据联系人潜在顾客更新潜在客户对手")
	@RequestMapping(method = RequestMethod.PUT, value = "/contacts/{contact_id}/leads/{lead_id}/leadcompetitors/{leadcompetitor_id}")
    public ResponseEntity<LeadCompetitorDTO> updateByContactLead(@PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @PathVariable("leadcompetitor_id") String leadcompetitor_id, @RequestBody LeadCompetitorDTO leadcompetitordto) {
        LeadCompetitor domain = leadcompetitorMapping.toDomain(leadcompetitordto);
        domain.setEntityid(lead_id);
        domain.setRelationshipsid(leadcompetitor_id);
		leadcompetitorService.update(domain);
        LeadCompetitorDTO dto = leadcompetitorMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.leadcompetitorService.getLeadcompetitorByEntities(this.leadcompetitorMapping.toDomain(#leadcompetitordtos)),'iBizBusinessCentral-LeadCompetitor-Update')")
    @ApiOperation(value = "根据联系人潜在顾客批量更新潜在客户对手", tags = {"潜在客户对手" },  notes = "根据联系人潜在顾客批量更新潜在客户对手")
	@RequestMapping(method = RequestMethod.PUT, value = "/contacts/{contact_id}/leads/{lead_id}/leadcompetitors/batch")
    public ResponseEntity<Boolean> updateBatchByContactLead(@PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody List<LeadCompetitorDTO> leadcompetitordtos) {
        List<LeadCompetitor> domainlist=leadcompetitorMapping.toDomain(leadcompetitordtos);
        for(LeadCompetitor domain:domainlist){
            domain.setEntityid(lead_id);
        }
        leadcompetitorService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.leadcompetitorService.get(#leadcompetitor_id),'iBizBusinessCentral-LeadCompetitor-Remove')")
    @ApiOperation(value = "根据联系人潜在顾客删除潜在客户对手", tags = {"潜在客户对手" },  notes = "根据联系人潜在顾客删除潜在客户对手")
	@RequestMapping(method = RequestMethod.DELETE, value = "/contacts/{contact_id}/leads/{lead_id}/leadcompetitors/{leadcompetitor_id}")
    public ResponseEntity<Boolean> removeByContactLead(@PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @PathVariable("leadcompetitor_id") String leadcompetitor_id) {
		return ResponseEntity.status(HttpStatus.OK).body(leadcompetitorService.remove(leadcompetitor_id));
    }

    @PreAuthorize("hasPermission(this.leadcompetitorService.getLeadcompetitorByIds(#ids),'iBizBusinessCentral-LeadCompetitor-Remove')")
    @ApiOperation(value = "根据联系人潜在顾客批量删除潜在客户对手", tags = {"潜在客户对手" },  notes = "根据联系人潜在顾客批量删除潜在客户对手")
	@RequestMapping(method = RequestMethod.DELETE, value = "/contacts/{contact_id}/leads/{lead_id}/leadcompetitors/batch")
    public ResponseEntity<Boolean> removeBatchByContactLead(@RequestBody List<String> ids) {
        leadcompetitorService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.leadcompetitorMapping.toDomain(returnObject.body),'iBizBusinessCentral-LeadCompetitor-Get')")
    @ApiOperation(value = "根据联系人潜在顾客获取潜在客户对手", tags = {"潜在客户对手" },  notes = "根据联系人潜在顾客获取潜在客户对手")
	@RequestMapping(method = RequestMethod.GET, value = "/contacts/{contact_id}/leads/{lead_id}/leadcompetitors/{leadcompetitor_id}")
    public ResponseEntity<LeadCompetitorDTO> getByContactLead(@PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @PathVariable("leadcompetitor_id") String leadcompetitor_id) {
        LeadCompetitor domain = leadcompetitorService.get(leadcompetitor_id);
        LeadCompetitorDTO dto = leadcompetitorMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据联系人潜在顾客获取潜在客户对手草稿", tags = {"潜在客户对手" },  notes = "根据联系人潜在顾客获取潜在客户对手草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/contacts/{contact_id}/leads/{lead_id}/leadcompetitors/getdraft")
    public ResponseEntity<LeadCompetitorDTO> getDraftByContactLead(@PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id) {
        LeadCompetitor domain = new LeadCompetitor();
        domain.setEntityid(lead_id);
        return ResponseEntity.status(HttpStatus.OK).body(leadcompetitorMapping.toDto(leadcompetitorService.getDraft(domain)));
    }

    @ApiOperation(value = "根据联系人潜在顾客检查潜在客户对手", tags = {"潜在客户对手" },  notes = "根据联系人潜在顾客检查潜在客户对手")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/leads/{lead_id}/leadcompetitors/checkkey")
    public ResponseEntity<Boolean> checkKeyByContactLead(@PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadCompetitorDTO leadcompetitordto) {
        return  ResponseEntity.status(HttpStatus.OK).body(leadcompetitorService.checkKey(leadcompetitorMapping.toDomain(leadcompetitordto)));
    }

    @PreAuthorize("hasPermission(this.leadcompetitorMapping.toDomain(#leadcompetitordto),'iBizBusinessCentral-LeadCompetitor-Save')")
    @ApiOperation(value = "根据联系人潜在顾客保存潜在客户对手", tags = {"潜在客户对手" },  notes = "根据联系人潜在顾客保存潜在客户对手")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/leads/{lead_id}/leadcompetitors/save")
    public ResponseEntity<Boolean> saveByContactLead(@PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadCompetitorDTO leadcompetitordto) {
        LeadCompetitor domain = leadcompetitorMapping.toDomain(leadcompetitordto);
        domain.setEntityid(lead_id);
        return ResponseEntity.status(HttpStatus.OK).body(leadcompetitorService.save(domain));
    }

    @PreAuthorize("hasPermission(this.leadcompetitorMapping.toDomain(#leadcompetitordtos),'iBizBusinessCentral-LeadCompetitor-Save')")
    @ApiOperation(value = "根据联系人潜在顾客批量保存潜在客户对手", tags = {"潜在客户对手" },  notes = "根据联系人潜在顾客批量保存潜在客户对手")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/leads/{lead_id}/leadcompetitors/savebatch")
    public ResponseEntity<Boolean> saveBatchByContactLead(@PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody List<LeadCompetitorDTO> leadcompetitordtos) {
        List<LeadCompetitor> domainlist=leadcompetitorMapping.toDomain(leadcompetitordtos);
        for(LeadCompetitor domain:domainlist){
             domain.setEntityid(lead_id);
        }
        leadcompetitorService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-LeadCompetitor-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-LeadCompetitor-Get')")
	@ApiOperation(value = "根据联系人潜在顾客获取DEFAULT", tags = {"潜在客户对手" } ,notes = "根据联系人潜在顾客获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/contacts/{contact_id}/leads/{lead_id}/leadcompetitors/fetchdefault")
	public ResponseEntity<List<LeadCompetitorDTO>> fetchLeadCompetitorDefaultByContactLead(@PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id,LeadCompetitorSearchContext context) {
        context.setN_entityid_eq(lead_id);
        Page<LeadCompetitor> domains = leadcompetitorService.searchDefault(context) ;
        List<LeadCompetitorDTO> list = leadcompetitorMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-LeadCompetitor-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-LeadCompetitor-Get')")
	@ApiOperation(value = "根据联系人潜在顾客查询DEFAULT", tags = {"潜在客户对手" } ,notes = "根据联系人潜在顾客查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/contacts/{contact_id}/leads/{lead_id}/leadcompetitors/searchdefault")
	public ResponseEntity<Page<LeadCompetitorDTO>> searchLeadCompetitorDefaultByContactLead(@PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadCompetitorSearchContext context) {
        context.setN_entityid_eq(lead_id);
        Page<LeadCompetitor> domains = leadcompetitorService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(leadcompetitorMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.leadcompetitorMapping.toDomain(#leadcompetitordto),'iBizBusinessCentral-LeadCompetitor-Create')")
    @ApiOperation(value = "根据客户联系人潜在顾客建立潜在客户对手", tags = {"潜在客户对手" },  notes = "根据客户联系人潜在顾客建立潜在客户对手")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}/leadcompetitors")
    public ResponseEntity<LeadCompetitorDTO> createByAccountContactLead(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadCompetitorDTO leadcompetitordto) {
        LeadCompetitor domain = leadcompetitorMapping.toDomain(leadcompetitordto);
        domain.setEntityid(lead_id);
		leadcompetitorService.create(domain);
        LeadCompetitorDTO dto = leadcompetitorMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.leadcompetitorMapping.toDomain(#leadcompetitordtos),'iBizBusinessCentral-LeadCompetitor-Create')")
    @ApiOperation(value = "根据客户联系人潜在顾客批量建立潜在客户对手", tags = {"潜在客户对手" },  notes = "根据客户联系人潜在顾客批量建立潜在客户对手")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}/leadcompetitors/batch")
    public ResponseEntity<Boolean> createBatchByAccountContactLead(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody List<LeadCompetitorDTO> leadcompetitordtos) {
        List<LeadCompetitor> domainlist=leadcompetitorMapping.toDomain(leadcompetitordtos);
        for(LeadCompetitor domain:domainlist){
            domain.setEntityid(lead_id);
        }
        leadcompetitorService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "leadcompetitor" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.leadcompetitorService.get(#leadcompetitor_id),'iBizBusinessCentral-LeadCompetitor-Update')")
    @ApiOperation(value = "根据客户联系人潜在顾客更新潜在客户对手", tags = {"潜在客户对手" },  notes = "根据客户联系人潜在顾客更新潜在客户对手")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}/leadcompetitors/{leadcompetitor_id}")
    public ResponseEntity<LeadCompetitorDTO> updateByAccountContactLead(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @PathVariable("leadcompetitor_id") String leadcompetitor_id, @RequestBody LeadCompetitorDTO leadcompetitordto) {
        LeadCompetitor domain = leadcompetitorMapping.toDomain(leadcompetitordto);
        domain.setEntityid(lead_id);
        domain.setRelationshipsid(leadcompetitor_id);
		leadcompetitorService.update(domain);
        LeadCompetitorDTO dto = leadcompetitorMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.leadcompetitorService.getLeadcompetitorByEntities(this.leadcompetitorMapping.toDomain(#leadcompetitordtos)),'iBizBusinessCentral-LeadCompetitor-Update')")
    @ApiOperation(value = "根据客户联系人潜在顾客批量更新潜在客户对手", tags = {"潜在客户对手" },  notes = "根据客户联系人潜在顾客批量更新潜在客户对手")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}/leadcompetitors/batch")
    public ResponseEntity<Boolean> updateBatchByAccountContactLead(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody List<LeadCompetitorDTO> leadcompetitordtos) {
        List<LeadCompetitor> domainlist=leadcompetitorMapping.toDomain(leadcompetitordtos);
        for(LeadCompetitor domain:domainlist){
            domain.setEntityid(lead_id);
        }
        leadcompetitorService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.leadcompetitorService.get(#leadcompetitor_id),'iBizBusinessCentral-LeadCompetitor-Remove')")
    @ApiOperation(value = "根据客户联系人潜在顾客删除潜在客户对手", tags = {"潜在客户对手" },  notes = "根据客户联系人潜在顾客删除潜在客户对手")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}/leadcompetitors/{leadcompetitor_id}")
    public ResponseEntity<Boolean> removeByAccountContactLead(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @PathVariable("leadcompetitor_id") String leadcompetitor_id) {
		return ResponseEntity.status(HttpStatus.OK).body(leadcompetitorService.remove(leadcompetitor_id));
    }

    @PreAuthorize("hasPermission(this.leadcompetitorService.getLeadcompetitorByIds(#ids),'iBizBusinessCentral-LeadCompetitor-Remove')")
    @ApiOperation(value = "根据客户联系人潜在顾客批量删除潜在客户对手", tags = {"潜在客户对手" },  notes = "根据客户联系人潜在顾客批量删除潜在客户对手")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}/leadcompetitors/batch")
    public ResponseEntity<Boolean> removeBatchByAccountContactLead(@RequestBody List<String> ids) {
        leadcompetitorService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.leadcompetitorMapping.toDomain(returnObject.body),'iBizBusinessCentral-LeadCompetitor-Get')")
    @ApiOperation(value = "根据客户联系人潜在顾客获取潜在客户对手", tags = {"潜在客户对手" },  notes = "根据客户联系人潜在顾客获取潜在客户对手")
	@RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}/leadcompetitors/{leadcompetitor_id}")
    public ResponseEntity<LeadCompetitorDTO> getByAccountContactLead(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @PathVariable("leadcompetitor_id") String leadcompetitor_id) {
        LeadCompetitor domain = leadcompetitorService.get(leadcompetitor_id);
        LeadCompetitorDTO dto = leadcompetitorMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据客户联系人潜在顾客获取潜在客户对手草稿", tags = {"潜在客户对手" },  notes = "根据客户联系人潜在顾客获取潜在客户对手草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}/leadcompetitors/getdraft")
    public ResponseEntity<LeadCompetitorDTO> getDraftByAccountContactLead(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id) {
        LeadCompetitor domain = new LeadCompetitor();
        domain.setEntityid(lead_id);
        return ResponseEntity.status(HttpStatus.OK).body(leadcompetitorMapping.toDto(leadcompetitorService.getDraft(domain)));
    }

    @ApiOperation(value = "根据客户联系人潜在顾客检查潜在客户对手", tags = {"潜在客户对手" },  notes = "根据客户联系人潜在顾客检查潜在客户对手")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}/leadcompetitors/checkkey")
    public ResponseEntity<Boolean> checkKeyByAccountContactLead(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadCompetitorDTO leadcompetitordto) {
        return  ResponseEntity.status(HttpStatus.OK).body(leadcompetitorService.checkKey(leadcompetitorMapping.toDomain(leadcompetitordto)));
    }

    @PreAuthorize("hasPermission(this.leadcompetitorMapping.toDomain(#leadcompetitordto),'iBizBusinessCentral-LeadCompetitor-Save')")
    @ApiOperation(value = "根据客户联系人潜在顾客保存潜在客户对手", tags = {"潜在客户对手" },  notes = "根据客户联系人潜在顾客保存潜在客户对手")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}/leadcompetitors/save")
    public ResponseEntity<Boolean> saveByAccountContactLead(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadCompetitorDTO leadcompetitordto) {
        LeadCompetitor domain = leadcompetitorMapping.toDomain(leadcompetitordto);
        domain.setEntityid(lead_id);
        return ResponseEntity.status(HttpStatus.OK).body(leadcompetitorService.save(domain));
    }

    @PreAuthorize("hasPermission(this.leadcompetitorMapping.toDomain(#leadcompetitordtos),'iBizBusinessCentral-LeadCompetitor-Save')")
    @ApiOperation(value = "根据客户联系人潜在顾客批量保存潜在客户对手", tags = {"潜在客户对手" },  notes = "根据客户联系人潜在顾客批量保存潜在客户对手")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}/leadcompetitors/savebatch")
    public ResponseEntity<Boolean> saveBatchByAccountContactLead(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody List<LeadCompetitorDTO> leadcompetitordtos) {
        List<LeadCompetitor> domainlist=leadcompetitorMapping.toDomain(leadcompetitordtos);
        for(LeadCompetitor domain:domainlist){
             domain.setEntityid(lead_id);
        }
        leadcompetitorService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-LeadCompetitor-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-LeadCompetitor-Get')")
	@ApiOperation(value = "根据客户联系人潜在顾客获取DEFAULT", tags = {"潜在客户对手" } ,notes = "根据客户联系人潜在顾客获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}/leadcompetitors/fetchdefault")
	public ResponseEntity<List<LeadCompetitorDTO>> fetchLeadCompetitorDefaultByAccountContactLead(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id,LeadCompetitorSearchContext context) {
        context.setN_entityid_eq(lead_id);
        Page<LeadCompetitor> domains = leadcompetitorService.searchDefault(context) ;
        List<LeadCompetitorDTO> list = leadcompetitorMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-LeadCompetitor-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-LeadCompetitor-Get')")
	@ApiOperation(value = "根据客户联系人潜在顾客查询DEFAULT", tags = {"潜在客户对手" } ,notes = "根据客户联系人潜在顾客查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}/leadcompetitors/searchdefault")
	public ResponseEntity<Page<LeadCompetitorDTO>> searchLeadCompetitorDefaultByAccountContactLead(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadCompetitorSearchContext context) {
        context.setN_entityid_eq(lead_id);
        Page<LeadCompetitor> domains = leadcompetitorService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(leadcompetitorMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}


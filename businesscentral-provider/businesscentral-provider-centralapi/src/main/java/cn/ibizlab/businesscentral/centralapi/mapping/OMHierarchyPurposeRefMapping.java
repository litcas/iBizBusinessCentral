package cn.ibizlab.businesscentral.centralapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.base.domain.OMHierarchyPurposeRef;
import cn.ibizlab.businesscentral.centralapi.dto.OMHierarchyPurposeRefDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CentralApiOMHierarchyPurposeRefMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface OMHierarchyPurposeRefMapping extends MappingBase<OMHierarchyPurposeRefDTO, OMHierarchyPurposeRef> {


}


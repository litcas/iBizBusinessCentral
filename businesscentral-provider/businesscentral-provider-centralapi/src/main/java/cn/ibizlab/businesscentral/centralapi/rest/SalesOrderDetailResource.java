package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.sales.domain.SalesOrderDetail;
import cn.ibizlab.businesscentral.core.sales.service.ISalesOrderDetailService;
import cn.ibizlab.businesscentral.core.sales.filter.SalesOrderDetailSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"订单产品" })
@RestController("CentralApi-salesorderdetail")
@RequestMapping("")
public class SalesOrderDetailResource {

    @Autowired
    public ISalesOrderDetailService salesorderdetailService;

    @Autowired
    @Lazy
    public SalesOrderDetailMapping salesorderdetailMapping;

    @PreAuthorize("hasPermission(this.salesorderdetailMapping.toDomain(#salesorderdetaildto),'iBizBusinessCentral-SalesOrderDetail-Create')")
    @ApiOperation(value = "新建订单产品", tags = {"订单产品" },  notes = "新建订单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/salesorderdetails")
    public ResponseEntity<SalesOrderDetailDTO> create(@RequestBody SalesOrderDetailDTO salesorderdetaildto) {
        SalesOrderDetail domain = salesorderdetailMapping.toDomain(salesorderdetaildto);
		salesorderdetailService.create(domain);
        SalesOrderDetailDTO dto = salesorderdetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.salesorderdetailMapping.toDomain(#salesorderdetaildtos),'iBizBusinessCentral-SalesOrderDetail-Create')")
    @ApiOperation(value = "批量新建订单产品", tags = {"订单产品" },  notes = "批量新建订单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/salesorderdetails/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<SalesOrderDetailDTO> salesorderdetaildtos) {
        salesorderdetailService.createBatch(salesorderdetailMapping.toDomain(salesorderdetaildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "salesorderdetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.salesorderdetailService.get(#salesorderdetail_id),'iBizBusinessCentral-SalesOrderDetail-Update')")
    @ApiOperation(value = "更新订单产品", tags = {"订单产品" },  notes = "更新订单产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/salesorderdetails/{salesorderdetail_id}")
    public ResponseEntity<SalesOrderDetailDTO> update(@PathVariable("salesorderdetail_id") String salesorderdetail_id, @RequestBody SalesOrderDetailDTO salesorderdetaildto) {
		SalesOrderDetail domain  = salesorderdetailMapping.toDomain(salesorderdetaildto);
        domain .setSalesorderdetailid(salesorderdetail_id);
		salesorderdetailService.update(domain );
		SalesOrderDetailDTO dto = salesorderdetailMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.salesorderdetailService.getSalesorderdetailByEntities(this.salesorderdetailMapping.toDomain(#salesorderdetaildtos)),'iBizBusinessCentral-SalesOrderDetail-Update')")
    @ApiOperation(value = "批量更新订单产品", tags = {"订单产品" },  notes = "批量更新订单产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/salesorderdetails/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<SalesOrderDetailDTO> salesorderdetaildtos) {
        salesorderdetailService.updateBatch(salesorderdetailMapping.toDomain(salesorderdetaildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.salesorderdetailService.get(#salesorderdetail_id),'iBizBusinessCentral-SalesOrderDetail-Remove')")
    @ApiOperation(value = "删除订单产品", tags = {"订单产品" },  notes = "删除订单产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/salesorderdetails/{salesorderdetail_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("salesorderdetail_id") String salesorderdetail_id) {
         return ResponseEntity.status(HttpStatus.OK).body(salesorderdetailService.remove(salesorderdetail_id));
    }

    @PreAuthorize("hasPermission(this.salesorderdetailService.getSalesorderdetailByIds(#ids),'iBizBusinessCentral-SalesOrderDetail-Remove')")
    @ApiOperation(value = "批量删除订单产品", tags = {"订单产品" },  notes = "批量删除订单产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/salesorderdetails/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        salesorderdetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.salesorderdetailMapping.toDomain(returnObject.body),'iBizBusinessCentral-SalesOrderDetail-Get')")
    @ApiOperation(value = "获取订单产品", tags = {"订单产品" },  notes = "获取订单产品")
	@RequestMapping(method = RequestMethod.GET, value = "/salesorderdetails/{salesorderdetail_id}")
    public ResponseEntity<SalesOrderDetailDTO> get(@PathVariable("salesorderdetail_id") String salesorderdetail_id) {
        SalesOrderDetail domain = salesorderdetailService.get(salesorderdetail_id);
        SalesOrderDetailDTO dto = salesorderdetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取订单产品草稿", tags = {"订单产品" },  notes = "获取订单产品草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/salesorderdetails/getdraft")
    public ResponseEntity<SalesOrderDetailDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(salesorderdetailMapping.toDto(salesorderdetailService.getDraft(new SalesOrderDetail())));
    }

    @ApiOperation(value = "检查订单产品", tags = {"订单产品" },  notes = "检查订单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/salesorderdetails/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody SalesOrderDetailDTO salesorderdetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(salesorderdetailService.checkKey(salesorderdetailMapping.toDomain(salesorderdetaildto)));
    }

    @PreAuthorize("hasPermission(this.salesorderdetailMapping.toDomain(#salesorderdetaildto),'iBizBusinessCentral-SalesOrderDetail-Save')")
    @ApiOperation(value = "保存订单产品", tags = {"订单产品" },  notes = "保存订单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/salesorderdetails/save")
    public ResponseEntity<Boolean> save(@RequestBody SalesOrderDetailDTO salesorderdetaildto) {
        return ResponseEntity.status(HttpStatus.OK).body(salesorderdetailService.save(salesorderdetailMapping.toDomain(salesorderdetaildto)));
    }

    @PreAuthorize("hasPermission(this.salesorderdetailMapping.toDomain(#salesorderdetaildtos),'iBizBusinessCentral-SalesOrderDetail-Save')")
    @ApiOperation(value = "批量保存订单产品", tags = {"订单产品" },  notes = "批量保存订单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/salesorderdetails/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<SalesOrderDetailDTO> salesorderdetaildtos) {
        salesorderdetailService.saveBatch(salesorderdetailMapping.toDomain(salesorderdetaildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrderDetail-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrderDetail-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"订单产品" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/salesorderdetails/fetchdefault")
	public ResponseEntity<List<SalesOrderDetailDTO>> fetchDefault(SalesOrderDetailSearchContext context) {
        Page<SalesOrderDetail> domains = salesorderdetailService.searchDefault(context) ;
        List<SalesOrderDetailDTO> list = salesorderdetailMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrderDetail-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrderDetail-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"订单产品" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/salesorderdetails/searchdefault")
	public ResponseEntity<Page<SalesOrderDetailDTO>> searchDefault(@RequestBody SalesOrderDetailSearchContext context) {
        Page<SalesOrderDetail> domains = salesorderdetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderdetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.salesorderdetailMapping.toDomain(#salesorderdetaildto),'iBizBusinessCentral-SalesOrderDetail-Create')")
    @ApiOperation(value = "根据订单建立订单产品", tags = {"订单产品" },  notes = "根据订单建立订单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/salesorders/{salesorder_id}/salesorderdetails")
    public ResponseEntity<SalesOrderDetailDTO> createBySalesOrder(@PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDetailDTO salesorderdetaildto) {
        SalesOrderDetail domain = salesorderdetailMapping.toDomain(salesorderdetaildto);
        domain.setSalesorderid(salesorder_id);
		salesorderdetailService.create(domain);
        SalesOrderDetailDTO dto = salesorderdetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.salesorderdetailMapping.toDomain(#salesorderdetaildtos),'iBizBusinessCentral-SalesOrderDetail-Create')")
    @ApiOperation(value = "根据订单批量建立订单产品", tags = {"订单产品" },  notes = "根据订单批量建立订单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/salesorders/{salesorder_id}/salesorderdetails/batch")
    public ResponseEntity<Boolean> createBatchBySalesOrder(@PathVariable("salesorder_id") String salesorder_id, @RequestBody List<SalesOrderDetailDTO> salesorderdetaildtos) {
        List<SalesOrderDetail> domainlist=salesorderdetailMapping.toDomain(salesorderdetaildtos);
        for(SalesOrderDetail domain:domainlist){
            domain.setSalesorderid(salesorder_id);
        }
        salesorderdetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "salesorderdetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.salesorderdetailService.get(#salesorderdetail_id),'iBizBusinessCentral-SalesOrderDetail-Update')")
    @ApiOperation(value = "根据订单更新订单产品", tags = {"订单产品" },  notes = "根据订单更新订单产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/salesorders/{salesorder_id}/salesorderdetails/{salesorderdetail_id}")
    public ResponseEntity<SalesOrderDetailDTO> updateBySalesOrder(@PathVariable("salesorder_id") String salesorder_id, @PathVariable("salesorderdetail_id") String salesorderdetail_id, @RequestBody SalesOrderDetailDTO salesorderdetaildto) {
        SalesOrderDetail domain = salesorderdetailMapping.toDomain(salesorderdetaildto);
        domain.setSalesorderid(salesorder_id);
        domain.setSalesorderdetailid(salesorderdetail_id);
		salesorderdetailService.update(domain);
        SalesOrderDetailDTO dto = salesorderdetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.salesorderdetailService.getSalesorderdetailByEntities(this.salesorderdetailMapping.toDomain(#salesorderdetaildtos)),'iBizBusinessCentral-SalesOrderDetail-Update')")
    @ApiOperation(value = "根据订单批量更新订单产品", tags = {"订单产品" },  notes = "根据订单批量更新订单产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/salesorders/{salesorder_id}/salesorderdetails/batch")
    public ResponseEntity<Boolean> updateBatchBySalesOrder(@PathVariable("salesorder_id") String salesorder_id, @RequestBody List<SalesOrderDetailDTO> salesorderdetaildtos) {
        List<SalesOrderDetail> domainlist=salesorderdetailMapping.toDomain(salesorderdetaildtos);
        for(SalesOrderDetail domain:domainlist){
            domain.setSalesorderid(salesorder_id);
        }
        salesorderdetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.salesorderdetailService.get(#salesorderdetail_id),'iBizBusinessCentral-SalesOrderDetail-Remove')")
    @ApiOperation(value = "根据订单删除订单产品", tags = {"订单产品" },  notes = "根据订单删除订单产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/salesorders/{salesorder_id}/salesorderdetails/{salesorderdetail_id}")
    public ResponseEntity<Boolean> removeBySalesOrder(@PathVariable("salesorder_id") String salesorder_id, @PathVariable("salesorderdetail_id") String salesorderdetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(salesorderdetailService.remove(salesorderdetail_id));
    }

    @PreAuthorize("hasPermission(this.salesorderdetailService.getSalesorderdetailByIds(#ids),'iBizBusinessCentral-SalesOrderDetail-Remove')")
    @ApiOperation(value = "根据订单批量删除订单产品", tags = {"订单产品" },  notes = "根据订单批量删除订单产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/salesorders/{salesorder_id}/salesorderdetails/batch")
    public ResponseEntity<Boolean> removeBatchBySalesOrder(@RequestBody List<String> ids) {
        salesorderdetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.salesorderdetailMapping.toDomain(returnObject.body),'iBizBusinessCentral-SalesOrderDetail-Get')")
    @ApiOperation(value = "根据订单获取订单产品", tags = {"订单产品" },  notes = "根据订单获取订单产品")
	@RequestMapping(method = RequestMethod.GET, value = "/salesorders/{salesorder_id}/salesorderdetails/{salesorderdetail_id}")
    public ResponseEntity<SalesOrderDetailDTO> getBySalesOrder(@PathVariable("salesorder_id") String salesorder_id, @PathVariable("salesorderdetail_id") String salesorderdetail_id) {
        SalesOrderDetail domain = salesorderdetailService.get(salesorderdetail_id);
        SalesOrderDetailDTO dto = salesorderdetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据订单获取订单产品草稿", tags = {"订单产品" },  notes = "根据订单获取订单产品草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/salesorders/{salesorder_id}/salesorderdetails/getdraft")
    public ResponseEntity<SalesOrderDetailDTO> getDraftBySalesOrder(@PathVariable("salesorder_id") String salesorder_id) {
        SalesOrderDetail domain = new SalesOrderDetail();
        domain.setSalesorderid(salesorder_id);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderdetailMapping.toDto(salesorderdetailService.getDraft(domain)));
    }

    @ApiOperation(value = "根据订单检查订单产品", tags = {"订单产品" },  notes = "根据订单检查订单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/salesorders/{salesorder_id}/salesorderdetails/checkkey")
    public ResponseEntity<Boolean> checkKeyBySalesOrder(@PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDetailDTO salesorderdetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(salesorderdetailService.checkKey(salesorderdetailMapping.toDomain(salesorderdetaildto)));
    }

    @PreAuthorize("hasPermission(this.salesorderdetailMapping.toDomain(#salesorderdetaildto),'iBizBusinessCentral-SalesOrderDetail-Save')")
    @ApiOperation(value = "根据订单保存订单产品", tags = {"订单产品" },  notes = "根据订单保存订单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/salesorders/{salesorder_id}/salesorderdetails/save")
    public ResponseEntity<Boolean> saveBySalesOrder(@PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDetailDTO salesorderdetaildto) {
        SalesOrderDetail domain = salesorderdetailMapping.toDomain(salesorderdetaildto);
        domain.setSalesorderid(salesorder_id);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderdetailService.save(domain));
    }

    @PreAuthorize("hasPermission(this.salesorderdetailMapping.toDomain(#salesorderdetaildtos),'iBizBusinessCentral-SalesOrderDetail-Save')")
    @ApiOperation(value = "根据订单批量保存订单产品", tags = {"订单产品" },  notes = "根据订单批量保存订单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/salesorders/{salesorder_id}/salesorderdetails/savebatch")
    public ResponseEntity<Boolean> saveBatchBySalesOrder(@PathVariable("salesorder_id") String salesorder_id, @RequestBody List<SalesOrderDetailDTO> salesorderdetaildtos) {
        List<SalesOrderDetail> domainlist=salesorderdetailMapping.toDomain(salesorderdetaildtos);
        for(SalesOrderDetail domain:domainlist){
             domain.setSalesorderid(salesorder_id);
        }
        salesorderdetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrderDetail-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrderDetail-Get')")
	@ApiOperation(value = "根据订单获取DEFAULT", tags = {"订单产品" } ,notes = "根据订单获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/salesorders/{salesorder_id}/salesorderdetails/fetchdefault")
	public ResponseEntity<List<SalesOrderDetailDTO>> fetchSalesOrderDetailDefaultBySalesOrder(@PathVariable("salesorder_id") String salesorder_id,SalesOrderDetailSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<SalesOrderDetail> domains = salesorderdetailService.searchDefault(context) ;
        List<SalesOrderDetailDTO> list = salesorderdetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrderDetail-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrderDetail-Get')")
	@ApiOperation(value = "根据订单查询DEFAULT", tags = {"订单产品" } ,notes = "根据订单查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/salesorders/{salesorder_id}/salesorderdetails/searchdefault")
	public ResponseEntity<Page<SalesOrderDetailDTO>> searchSalesOrderDetailDefaultBySalesOrder(@PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDetailSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<SalesOrderDetail> domains = salesorderdetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderdetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.salesorderdetailMapping.toDomain(#salesorderdetaildto),'iBizBusinessCentral-SalesOrderDetail-Create')")
    @ApiOperation(value = "根据报价单订单建立订单产品", tags = {"订单产品" },  notes = "根据报价单订单建立订单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails")
    public ResponseEntity<SalesOrderDetailDTO> createByQuoteSalesOrder(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDetailDTO salesorderdetaildto) {
        SalesOrderDetail domain = salesorderdetailMapping.toDomain(salesorderdetaildto);
        domain.setSalesorderid(salesorder_id);
		salesorderdetailService.create(domain);
        SalesOrderDetailDTO dto = salesorderdetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.salesorderdetailMapping.toDomain(#salesorderdetaildtos),'iBizBusinessCentral-SalesOrderDetail-Create')")
    @ApiOperation(value = "根据报价单订单批量建立订单产品", tags = {"订单产品" },  notes = "根据报价单订单批量建立订单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/batch")
    public ResponseEntity<Boolean> createBatchByQuoteSalesOrder(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody List<SalesOrderDetailDTO> salesorderdetaildtos) {
        List<SalesOrderDetail> domainlist=salesorderdetailMapping.toDomain(salesorderdetaildtos);
        for(SalesOrderDetail domain:domainlist){
            domain.setSalesorderid(salesorder_id);
        }
        salesorderdetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "salesorderdetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.salesorderdetailService.get(#salesorderdetail_id),'iBizBusinessCentral-SalesOrderDetail-Update')")
    @ApiOperation(value = "根据报价单订单更新订单产品", tags = {"订单产品" },  notes = "根据报价单订单更新订单产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/{salesorderdetail_id}")
    public ResponseEntity<SalesOrderDetailDTO> updateByQuoteSalesOrder(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("salesorderdetail_id") String salesorderdetail_id, @RequestBody SalesOrderDetailDTO salesorderdetaildto) {
        SalesOrderDetail domain = salesorderdetailMapping.toDomain(salesorderdetaildto);
        domain.setSalesorderid(salesorder_id);
        domain.setSalesorderdetailid(salesorderdetail_id);
		salesorderdetailService.update(domain);
        SalesOrderDetailDTO dto = salesorderdetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.salesorderdetailService.getSalesorderdetailByEntities(this.salesorderdetailMapping.toDomain(#salesorderdetaildtos)),'iBizBusinessCentral-SalesOrderDetail-Update')")
    @ApiOperation(value = "根据报价单订单批量更新订单产品", tags = {"订单产品" },  notes = "根据报价单订单批量更新订单产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/batch")
    public ResponseEntity<Boolean> updateBatchByQuoteSalesOrder(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody List<SalesOrderDetailDTO> salesorderdetaildtos) {
        List<SalesOrderDetail> domainlist=salesorderdetailMapping.toDomain(salesorderdetaildtos);
        for(SalesOrderDetail domain:domainlist){
            domain.setSalesorderid(salesorder_id);
        }
        salesorderdetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.salesorderdetailService.get(#salesorderdetail_id),'iBizBusinessCentral-SalesOrderDetail-Remove')")
    @ApiOperation(value = "根据报价单订单删除订单产品", tags = {"订单产品" },  notes = "根据报价单订单删除订单产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/{salesorderdetail_id}")
    public ResponseEntity<Boolean> removeByQuoteSalesOrder(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("salesorderdetail_id") String salesorderdetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(salesorderdetailService.remove(salesorderdetail_id));
    }

    @PreAuthorize("hasPermission(this.salesorderdetailService.getSalesorderdetailByIds(#ids),'iBizBusinessCentral-SalesOrderDetail-Remove')")
    @ApiOperation(value = "根据报价单订单批量删除订单产品", tags = {"订单产品" },  notes = "根据报价单订单批量删除订单产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/batch")
    public ResponseEntity<Boolean> removeBatchByQuoteSalesOrder(@RequestBody List<String> ids) {
        salesorderdetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.salesorderdetailMapping.toDomain(returnObject.body),'iBizBusinessCentral-SalesOrderDetail-Get')")
    @ApiOperation(value = "根据报价单订单获取订单产品", tags = {"订单产品" },  notes = "根据报价单订单获取订单产品")
	@RequestMapping(method = RequestMethod.GET, value = "/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/{salesorderdetail_id}")
    public ResponseEntity<SalesOrderDetailDTO> getByQuoteSalesOrder(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("salesorderdetail_id") String salesorderdetail_id) {
        SalesOrderDetail domain = salesorderdetailService.get(salesorderdetail_id);
        SalesOrderDetailDTO dto = salesorderdetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据报价单订单获取订单产品草稿", tags = {"订单产品" },  notes = "根据报价单订单获取订单产品草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/getdraft")
    public ResponseEntity<SalesOrderDetailDTO> getDraftByQuoteSalesOrder(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id) {
        SalesOrderDetail domain = new SalesOrderDetail();
        domain.setSalesorderid(salesorder_id);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderdetailMapping.toDto(salesorderdetailService.getDraft(domain)));
    }

    @ApiOperation(value = "根据报价单订单检查订单产品", tags = {"订单产品" },  notes = "根据报价单订单检查订单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByQuoteSalesOrder(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDetailDTO salesorderdetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(salesorderdetailService.checkKey(salesorderdetailMapping.toDomain(salesorderdetaildto)));
    }

    @PreAuthorize("hasPermission(this.salesorderdetailMapping.toDomain(#salesorderdetaildto),'iBizBusinessCentral-SalesOrderDetail-Save')")
    @ApiOperation(value = "根据报价单订单保存订单产品", tags = {"订单产品" },  notes = "根据报价单订单保存订单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/save")
    public ResponseEntity<Boolean> saveByQuoteSalesOrder(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDetailDTO salesorderdetaildto) {
        SalesOrderDetail domain = salesorderdetailMapping.toDomain(salesorderdetaildto);
        domain.setSalesorderid(salesorder_id);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderdetailService.save(domain));
    }

    @PreAuthorize("hasPermission(this.salesorderdetailMapping.toDomain(#salesorderdetaildtos),'iBizBusinessCentral-SalesOrderDetail-Save')")
    @ApiOperation(value = "根据报价单订单批量保存订单产品", tags = {"订单产品" },  notes = "根据报价单订单批量保存订单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByQuoteSalesOrder(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody List<SalesOrderDetailDTO> salesorderdetaildtos) {
        List<SalesOrderDetail> domainlist=salesorderdetailMapping.toDomain(salesorderdetaildtos);
        for(SalesOrderDetail domain:domainlist){
             domain.setSalesorderid(salesorder_id);
        }
        salesorderdetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrderDetail-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrderDetail-Get')")
	@ApiOperation(value = "根据报价单订单获取DEFAULT", tags = {"订单产品" } ,notes = "根据报价单订单获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/fetchdefault")
	public ResponseEntity<List<SalesOrderDetailDTO>> fetchSalesOrderDetailDefaultByQuoteSalesOrder(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id,SalesOrderDetailSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<SalesOrderDetail> domains = salesorderdetailService.searchDefault(context) ;
        List<SalesOrderDetailDTO> list = salesorderdetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrderDetail-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrderDetail-Get')")
	@ApiOperation(value = "根据报价单订单查询DEFAULT", tags = {"订单产品" } ,notes = "根据报价单订单查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/searchdefault")
	public ResponseEntity<Page<SalesOrderDetailDTO>> searchSalesOrderDetailDefaultByQuoteSalesOrder(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDetailSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<SalesOrderDetail> domains = salesorderdetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderdetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.salesorderdetailMapping.toDomain(#salesorderdetaildto),'iBizBusinessCentral-SalesOrderDetail-Create')")
    @ApiOperation(value = "根据商机报价单订单建立订单产品", tags = {"订单产品" },  notes = "根据商机报价单订单建立订单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails")
    public ResponseEntity<SalesOrderDetailDTO> createByOpportunityQuoteSalesOrder(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDetailDTO salesorderdetaildto) {
        SalesOrderDetail domain = salesorderdetailMapping.toDomain(salesorderdetaildto);
        domain.setSalesorderid(salesorder_id);
		salesorderdetailService.create(domain);
        SalesOrderDetailDTO dto = salesorderdetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.salesorderdetailMapping.toDomain(#salesorderdetaildtos),'iBizBusinessCentral-SalesOrderDetail-Create')")
    @ApiOperation(value = "根据商机报价单订单批量建立订单产品", tags = {"订单产品" },  notes = "根据商机报价单订单批量建立订单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/batch")
    public ResponseEntity<Boolean> createBatchByOpportunityQuoteSalesOrder(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody List<SalesOrderDetailDTO> salesorderdetaildtos) {
        List<SalesOrderDetail> domainlist=salesorderdetailMapping.toDomain(salesorderdetaildtos);
        for(SalesOrderDetail domain:domainlist){
            domain.setSalesorderid(salesorder_id);
        }
        salesorderdetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "salesorderdetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.salesorderdetailService.get(#salesorderdetail_id),'iBizBusinessCentral-SalesOrderDetail-Update')")
    @ApiOperation(value = "根据商机报价单订单更新订单产品", tags = {"订单产品" },  notes = "根据商机报价单订单更新订单产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/{salesorderdetail_id}")
    public ResponseEntity<SalesOrderDetailDTO> updateByOpportunityQuoteSalesOrder(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("salesorderdetail_id") String salesorderdetail_id, @RequestBody SalesOrderDetailDTO salesorderdetaildto) {
        SalesOrderDetail domain = salesorderdetailMapping.toDomain(salesorderdetaildto);
        domain.setSalesorderid(salesorder_id);
        domain.setSalesorderdetailid(salesorderdetail_id);
		salesorderdetailService.update(domain);
        SalesOrderDetailDTO dto = salesorderdetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.salesorderdetailService.getSalesorderdetailByEntities(this.salesorderdetailMapping.toDomain(#salesorderdetaildtos)),'iBizBusinessCentral-SalesOrderDetail-Update')")
    @ApiOperation(value = "根据商机报价单订单批量更新订单产品", tags = {"订单产品" },  notes = "根据商机报价单订单批量更新订单产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/batch")
    public ResponseEntity<Boolean> updateBatchByOpportunityQuoteSalesOrder(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody List<SalesOrderDetailDTO> salesorderdetaildtos) {
        List<SalesOrderDetail> domainlist=salesorderdetailMapping.toDomain(salesorderdetaildtos);
        for(SalesOrderDetail domain:domainlist){
            domain.setSalesorderid(salesorder_id);
        }
        salesorderdetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.salesorderdetailService.get(#salesorderdetail_id),'iBizBusinessCentral-SalesOrderDetail-Remove')")
    @ApiOperation(value = "根据商机报价单订单删除订单产品", tags = {"订单产品" },  notes = "根据商机报价单订单删除订单产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/{salesorderdetail_id}")
    public ResponseEntity<Boolean> removeByOpportunityQuoteSalesOrder(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("salesorderdetail_id") String salesorderdetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(salesorderdetailService.remove(salesorderdetail_id));
    }

    @PreAuthorize("hasPermission(this.salesorderdetailService.getSalesorderdetailByIds(#ids),'iBizBusinessCentral-SalesOrderDetail-Remove')")
    @ApiOperation(value = "根据商机报价单订单批量删除订单产品", tags = {"订单产品" },  notes = "根据商机报价单订单批量删除订单产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/batch")
    public ResponseEntity<Boolean> removeBatchByOpportunityQuoteSalesOrder(@RequestBody List<String> ids) {
        salesorderdetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.salesorderdetailMapping.toDomain(returnObject.body),'iBizBusinessCentral-SalesOrderDetail-Get')")
    @ApiOperation(value = "根据商机报价单订单获取订单产品", tags = {"订单产品" },  notes = "根据商机报价单订单获取订单产品")
	@RequestMapping(method = RequestMethod.GET, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/{salesorderdetail_id}")
    public ResponseEntity<SalesOrderDetailDTO> getByOpportunityQuoteSalesOrder(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("salesorderdetail_id") String salesorderdetail_id) {
        SalesOrderDetail domain = salesorderdetailService.get(salesorderdetail_id);
        SalesOrderDetailDTO dto = salesorderdetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据商机报价单订单获取订单产品草稿", tags = {"订单产品" },  notes = "根据商机报价单订单获取订单产品草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/getdraft")
    public ResponseEntity<SalesOrderDetailDTO> getDraftByOpportunityQuoteSalesOrder(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id) {
        SalesOrderDetail domain = new SalesOrderDetail();
        domain.setSalesorderid(salesorder_id);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderdetailMapping.toDto(salesorderdetailService.getDraft(domain)));
    }

    @ApiOperation(value = "根据商机报价单订单检查订单产品", tags = {"订单产品" },  notes = "根据商机报价单订单检查订单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByOpportunityQuoteSalesOrder(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDetailDTO salesorderdetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(salesorderdetailService.checkKey(salesorderdetailMapping.toDomain(salesorderdetaildto)));
    }

    @PreAuthorize("hasPermission(this.salesorderdetailMapping.toDomain(#salesorderdetaildto),'iBizBusinessCentral-SalesOrderDetail-Save')")
    @ApiOperation(value = "根据商机报价单订单保存订单产品", tags = {"订单产品" },  notes = "根据商机报价单订单保存订单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/save")
    public ResponseEntity<Boolean> saveByOpportunityQuoteSalesOrder(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDetailDTO salesorderdetaildto) {
        SalesOrderDetail domain = salesorderdetailMapping.toDomain(salesorderdetaildto);
        domain.setSalesorderid(salesorder_id);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderdetailService.save(domain));
    }

    @PreAuthorize("hasPermission(this.salesorderdetailMapping.toDomain(#salesorderdetaildtos),'iBizBusinessCentral-SalesOrderDetail-Save')")
    @ApiOperation(value = "根据商机报价单订单批量保存订单产品", tags = {"订单产品" },  notes = "根据商机报价单订单批量保存订单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByOpportunityQuoteSalesOrder(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody List<SalesOrderDetailDTO> salesorderdetaildtos) {
        List<SalesOrderDetail> domainlist=salesorderdetailMapping.toDomain(salesorderdetaildtos);
        for(SalesOrderDetail domain:domainlist){
             domain.setSalesorderid(salesorder_id);
        }
        salesorderdetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrderDetail-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrderDetail-Get')")
	@ApiOperation(value = "根据商机报价单订单获取DEFAULT", tags = {"订单产品" } ,notes = "根据商机报价单订单获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/fetchdefault")
	public ResponseEntity<List<SalesOrderDetailDTO>> fetchSalesOrderDetailDefaultByOpportunityQuoteSalesOrder(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id,SalesOrderDetailSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<SalesOrderDetail> domains = salesorderdetailService.searchDefault(context) ;
        List<SalesOrderDetailDTO> list = salesorderdetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrderDetail-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrderDetail-Get')")
	@ApiOperation(value = "根据商机报价单订单查询DEFAULT", tags = {"订单产品" } ,notes = "根据商机报价单订单查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/searchdefault")
	public ResponseEntity<Page<SalesOrderDetailDTO>> searchSalesOrderDetailDefaultByOpportunityQuoteSalesOrder(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDetailSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<SalesOrderDetail> domains = salesorderdetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderdetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.salesorderdetailMapping.toDomain(#salesorderdetaildto),'iBizBusinessCentral-SalesOrderDetail-Create')")
    @ApiOperation(value = "根据客户商机报价单订单建立订单产品", tags = {"订单产品" },  notes = "根据客户商机报价单订单建立订单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails")
    public ResponseEntity<SalesOrderDetailDTO> createByAccountOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDetailDTO salesorderdetaildto) {
        SalesOrderDetail domain = salesorderdetailMapping.toDomain(salesorderdetaildto);
        domain.setSalesorderid(salesorder_id);
		salesorderdetailService.create(domain);
        SalesOrderDetailDTO dto = salesorderdetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.salesorderdetailMapping.toDomain(#salesorderdetaildtos),'iBizBusinessCentral-SalesOrderDetail-Create')")
    @ApiOperation(value = "根据客户商机报价单订单批量建立订单产品", tags = {"订单产品" },  notes = "根据客户商机报价单订单批量建立订单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/batch")
    public ResponseEntity<Boolean> createBatchByAccountOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody List<SalesOrderDetailDTO> salesorderdetaildtos) {
        List<SalesOrderDetail> domainlist=salesorderdetailMapping.toDomain(salesorderdetaildtos);
        for(SalesOrderDetail domain:domainlist){
            domain.setSalesorderid(salesorder_id);
        }
        salesorderdetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "salesorderdetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.salesorderdetailService.get(#salesorderdetail_id),'iBizBusinessCentral-SalesOrderDetail-Update')")
    @ApiOperation(value = "根据客户商机报价单订单更新订单产品", tags = {"订单产品" },  notes = "根据客户商机报价单订单更新订单产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/{salesorderdetail_id}")
    public ResponseEntity<SalesOrderDetailDTO> updateByAccountOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("salesorderdetail_id") String salesorderdetail_id, @RequestBody SalesOrderDetailDTO salesorderdetaildto) {
        SalesOrderDetail domain = salesorderdetailMapping.toDomain(salesorderdetaildto);
        domain.setSalesorderid(salesorder_id);
        domain.setSalesorderdetailid(salesorderdetail_id);
		salesorderdetailService.update(domain);
        SalesOrderDetailDTO dto = salesorderdetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.salesorderdetailService.getSalesorderdetailByEntities(this.salesorderdetailMapping.toDomain(#salesorderdetaildtos)),'iBizBusinessCentral-SalesOrderDetail-Update')")
    @ApiOperation(value = "根据客户商机报价单订单批量更新订单产品", tags = {"订单产品" },  notes = "根据客户商机报价单订单批量更新订单产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/batch")
    public ResponseEntity<Boolean> updateBatchByAccountOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody List<SalesOrderDetailDTO> salesorderdetaildtos) {
        List<SalesOrderDetail> domainlist=salesorderdetailMapping.toDomain(salesorderdetaildtos);
        for(SalesOrderDetail domain:domainlist){
            domain.setSalesorderid(salesorder_id);
        }
        salesorderdetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.salesorderdetailService.get(#salesorderdetail_id),'iBizBusinessCentral-SalesOrderDetail-Remove')")
    @ApiOperation(value = "根据客户商机报价单订单删除订单产品", tags = {"订单产品" },  notes = "根据客户商机报价单订单删除订单产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/{salesorderdetail_id}")
    public ResponseEntity<Boolean> removeByAccountOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("salesorderdetail_id") String salesorderdetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(salesorderdetailService.remove(salesorderdetail_id));
    }

    @PreAuthorize("hasPermission(this.salesorderdetailService.getSalesorderdetailByIds(#ids),'iBizBusinessCentral-SalesOrderDetail-Remove')")
    @ApiOperation(value = "根据客户商机报价单订单批量删除订单产品", tags = {"订单产品" },  notes = "根据客户商机报价单订单批量删除订单产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/batch")
    public ResponseEntity<Boolean> removeBatchByAccountOpportunityQuoteSalesOrder(@RequestBody List<String> ids) {
        salesorderdetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.salesorderdetailMapping.toDomain(returnObject.body),'iBizBusinessCentral-SalesOrderDetail-Get')")
    @ApiOperation(value = "根据客户商机报价单订单获取订单产品", tags = {"订单产品" },  notes = "根据客户商机报价单订单获取订单产品")
	@RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/{salesorderdetail_id}")
    public ResponseEntity<SalesOrderDetailDTO> getByAccountOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("salesorderdetail_id") String salesorderdetail_id) {
        SalesOrderDetail domain = salesorderdetailService.get(salesorderdetail_id);
        SalesOrderDetailDTO dto = salesorderdetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据客户商机报价单订单获取订单产品草稿", tags = {"订单产品" },  notes = "根据客户商机报价单订单获取订单产品草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/getdraft")
    public ResponseEntity<SalesOrderDetailDTO> getDraftByAccountOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id) {
        SalesOrderDetail domain = new SalesOrderDetail();
        domain.setSalesorderid(salesorder_id);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderdetailMapping.toDto(salesorderdetailService.getDraft(domain)));
    }

    @ApiOperation(value = "根据客户商机报价单订单检查订单产品", tags = {"订单产品" },  notes = "根据客户商机报价单订单检查订单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByAccountOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDetailDTO salesorderdetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(salesorderdetailService.checkKey(salesorderdetailMapping.toDomain(salesorderdetaildto)));
    }

    @PreAuthorize("hasPermission(this.salesorderdetailMapping.toDomain(#salesorderdetaildto),'iBizBusinessCentral-SalesOrderDetail-Save')")
    @ApiOperation(value = "根据客户商机报价单订单保存订单产品", tags = {"订单产品" },  notes = "根据客户商机报价单订单保存订单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/save")
    public ResponseEntity<Boolean> saveByAccountOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDetailDTO salesorderdetaildto) {
        SalesOrderDetail domain = salesorderdetailMapping.toDomain(salesorderdetaildto);
        domain.setSalesorderid(salesorder_id);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderdetailService.save(domain));
    }

    @PreAuthorize("hasPermission(this.salesorderdetailMapping.toDomain(#salesorderdetaildtos),'iBizBusinessCentral-SalesOrderDetail-Save')")
    @ApiOperation(value = "根据客户商机报价单订单批量保存订单产品", tags = {"订单产品" },  notes = "根据客户商机报价单订单批量保存订单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByAccountOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody List<SalesOrderDetailDTO> salesorderdetaildtos) {
        List<SalesOrderDetail> domainlist=salesorderdetailMapping.toDomain(salesorderdetaildtos);
        for(SalesOrderDetail domain:domainlist){
             domain.setSalesorderid(salesorder_id);
        }
        salesorderdetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrderDetail-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrderDetail-Get')")
	@ApiOperation(value = "根据客户商机报价单订单获取DEFAULT", tags = {"订单产品" } ,notes = "根据客户商机报价单订单获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/fetchdefault")
	public ResponseEntity<List<SalesOrderDetailDTO>> fetchSalesOrderDetailDefaultByAccountOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id,SalesOrderDetailSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<SalesOrderDetail> domains = salesorderdetailService.searchDefault(context) ;
        List<SalesOrderDetailDTO> list = salesorderdetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrderDetail-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrderDetail-Get')")
	@ApiOperation(value = "根据客户商机报价单订单查询DEFAULT", tags = {"订单产品" } ,notes = "根据客户商机报价单订单查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/searchdefault")
	public ResponseEntity<Page<SalesOrderDetailDTO>> searchSalesOrderDetailDefaultByAccountOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDetailSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<SalesOrderDetail> domains = salesorderdetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderdetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.salesorderdetailMapping.toDomain(#salesorderdetaildto),'iBizBusinessCentral-SalesOrderDetail-Create')")
    @ApiOperation(value = "根据联系人商机报价单订单建立订单产品", tags = {"订单产品" },  notes = "根据联系人商机报价单订单建立订单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails")
    public ResponseEntity<SalesOrderDetailDTO> createByContactOpportunityQuoteSalesOrder(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDetailDTO salesorderdetaildto) {
        SalesOrderDetail domain = salesorderdetailMapping.toDomain(salesorderdetaildto);
        domain.setSalesorderid(salesorder_id);
		salesorderdetailService.create(domain);
        SalesOrderDetailDTO dto = salesorderdetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.salesorderdetailMapping.toDomain(#salesorderdetaildtos),'iBizBusinessCentral-SalesOrderDetail-Create')")
    @ApiOperation(value = "根据联系人商机报价单订单批量建立订单产品", tags = {"订单产品" },  notes = "根据联系人商机报价单订单批量建立订单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/batch")
    public ResponseEntity<Boolean> createBatchByContactOpportunityQuoteSalesOrder(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody List<SalesOrderDetailDTO> salesorderdetaildtos) {
        List<SalesOrderDetail> domainlist=salesorderdetailMapping.toDomain(salesorderdetaildtos);
        for(SalesOrderDetail domain:domainlist){
            domain.setSalesorderid(salesorder_id);
        }
        salesorderdetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "salesorderdetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.salesorderdetailService.get(#salesorderdetail_id),'iBizBusinessCentral-SalesOrderDetail-Update')")
    @ApiOperation(value = "根据联系人商机报价单订单更新订单产品", tags = {"订单产品" },  notes = "根据联系人商机报价单订单更新订单产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/{salesorderdetail_id}")
    public ResponseEntity<SalesOrderDetailDTO> updateByContactOpportunityQuoteSalesOrder(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("salesorderdetail_id") String salesorderdetail_id, @RequestBody SalesOrderDetailDTO salesorderdetaildto) {
        SalesOrderDetail domain = salesorderdetailMapping.toDomain(salesorderdetaildto);
        domain.setSalesorderid(salesorder_id);
        domain.setSalesorderdetailid(salesorderdetail_id);
		salesorderdetailService.update(domain);
        SalesOrderDetailDTO dto = salesorderdetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.salesorderdetailService.getSalesorderdetailByEntities(this.salesorderdetailMapping.toDomain(#salesorderdetaildtos)),'iBizBusinessCentral-SalesOrderDetail-Update')")
    @ApiOperation(value = "根据联系人商机报价单订单批量更新订单产品", tags = {"订单产品" },  notes = "根据联系人商机报价单订单批量更新订单产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/batch")
    public ResponseEntity<Boolean> updateBatchByContactOpportunityQuoteSalesOrder(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody List<SalesOrderDetailDTO> salesorderdetaildtos) {
        List<SalesOrderDetail> domainlist=salesorderdetailMapping.toDomain(salesorderdetaildtos);
        for(SalesOrderDetail domain:domainlist){
            domain.setSalesorderid(salesorder_id);
        }
        salesorderdetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.salesorderdetailService.get(#salesorderdetail_id),'iBizBusinessCentral-SalesOrderDetail-Remove')")
    @ApiOperation(value = "根据联系人商机报价单订单删除订单产品", tags = {"订单产品" },  notes = "根据联系人商机报价单订单删除订单产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/{salesorderdetail_id}")
    public ResponseEntity<Boolean> removeByContactOpportunityQuoteSalesOrder(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("salesorderdetail_id") String salesorderdetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(salesorderdetailService.remove(salesorderdetail_id));
    }

    @PreAuthorize("hasPermission(this.salesorderdetailService.getSalesorderdetailByIds(#ids),'iBizBusinessCentral-SalesOrderDetail-Remove')")
    @ApiOperation(value = "根据联系人商机报价单订单批量删除订单产品", tags = {"订单产品" },  notes = "根据联系人商机报价单订单批量删除订单产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/batch")
    public ResponseEntity<Boolean> removeBatchByContactOpportunityQuoteSalesOrder(@RequestBody List<String> ids) {
        salesorderdetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.salesorderdetailMapping.toDomain(returnObject.body),'iBizBusinessCentral-SalesOrderDetail-Get')")
    @ApiOperation(value = "根据联系人商机报价单订单获取订单产品", tags = {"订单产品" },  notes = "根据联系人商机报价单订单获取订单产品")
	@RequestMapping(method = RequestMethod.GET, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/{salesorderdetail_id}")
    public ResponseEntity<SalesOrderDetailDTO> getByContactOpportunityQuoteSalesOrder(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("salesorderdetail_id") String salesorderdetail_id) {
        SalesOrderDetail domain = salesorderdetailService.get(salesorderdetail_id);
        SalesOrderDetailDTO dto = salesorderdetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据联系人商机报价单订单获取订单产品草稿", tags = {"订单产品" },  notes = "根据联系人商机报价单订单获取订单产品草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/getdraft")
    public ResponseEntity<SalesOrderDetailDTO> getDraftByContactOpportunityQuoteSalesOrder(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id) {
        SalesOrderDetail domain = new SalesOrderDetail();
        domain.setSalesorderid(salesorder_id);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderdetailMapping.toDto(salesorderdetailService.getDraft(domain)));
    }

    @ApiOperation(value = "根据联系人商机报价单订单检查订单产品", tags = {"订单产品" },  notes = "根据联系人商机报价单订单检查订单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByContactOpportunityQuoteSalesOrder(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDetailDTO salesorderdetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(salesorderdetailService.checkKey(salesorderdetailMapping.toDomain(salesorderdetaildto)));
    }

    @PreAuthorize("hasPermission(this.salesorderdetailMapping.toDomain(#salesorderdetaildto),'iBizBusinessCentral-SalesOrderDetail-Save')")
    @ApiOperation(value = "根据联系人商机报价单订单保存订单产品", tags = {"订单产品" },  notes = "根据联系人商机报价单订单保存订单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/save")
    public ResponseEntity<Boolean> saveByContactOpportunityQuoteSalesOrder(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDetailDTO salesorderdetaildto) {
        SalesOrderDetail domain = salesorderdetailMapping.toDomain(salesorderdetaildto);
        domain.setSalesorderid(salesorder_id);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderdetailService.save(domain));
    }

    @PreAuthorize("hasPermission(this.salesorderdetailMapping.toDomain(#salesorderdetaildtos),'iBizBusinessCentral-SalesOrderDetail-Save')")
    @ApiOperation(value = "根据联系人商机报价单订单批量保存订单产品", tags = {"订单产品" },  notes = "根据联系人商机报价单订单批量保存订单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByContactOpportunityQuoteSalesOrder(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody List<SalesOrderDetailDTO> salesorderdetaildtos) {
        List<SalesOrderDetail> domainlist=salesorderdetailMapping.toDomain(salesorderdetaildtos);
        for(SalesOrderDetail domain:domainlist){
             domain.setSalesorderid(salesorder_id);
        }
        salesorderdetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrderDetail-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrderDetail-Get')")
	@ApiOperation(value = "根据联系人商机报价单订单获取DEFAULT", tags = {"订单产品" } ,notes = "根据联系人商机报价单订单获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/fetchdefault")
	public ResponseEntity<List<SalesOrderDetailDTO>> fetchSalesOrderDetailDefaultByContactOpportunityQuoteSalesOrder(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id,SalesOrderDetailSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<SalesOrderDetail> domains = salesorderdetailService.searchDefault(context) ;
        List<SalesOrderDetailDTO> list = salesorderdetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrderDetail-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrderDetail-Get')")
	@ApiOperation(value = "根据联系人商机报价单订单查询DEFAULT", tags = {"订单产品" } ,notes = "根据联系人商机报价单订单查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/searchdefault")
	public ResponseEntity<Page<SalesOrderDetailDTO>> searchSalesOrderDetailDefaultByContactOpportunityQuoteSalesOrder(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDetailSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<SalesOrderDetail> domains = salesorderdetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderdetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.salesorderdetailMapping.toDomain(#salesorderdetaildto),'iBizBusinessCentral-SalesOrderDetail-Create')")
    @ApiOperation(value = "根据客户联系人商机报价单订单建立订单产品", tags = {"订单产品" },  notes = "根据客户联系人商机报价单订单建立订单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails")
    public ResponseEntity<SalesOrderDetailDTO> createByAccountContactOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDetailDTO salesorderdetaildto) {
        SalesOrderDetail domain = salesorderdetailMapping.toDomain(salesorderdetaildto);
        domain.setSalesorderid(salesorder_id);
		salesorderdetailService.create(domain);
        SalesOrderDetailDTO dto = salesorderdetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.salesorderdetailMapping.toDomain(#salesorderdetaildtos),'iBizBusinessCentral-SalesOrderDetail-Create')")
    @ApiOperation(value = "根据客户联系人商机报价单订单批量建立订单产品", tags = {"订单产品" },  notes = "根据客户联系人商机报价单订单批量建立订单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/batch")
    public ResponseEntity<Boolean> createBatchByAccountContactOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody List<SalesOrderDetailDTO> salesorderdetaildtos) {
        List<SalesOrderDetail> domainlist=salesorderdetailMapping.toDomain(salesorderdetaildtos);
        for(SalesOrderDetail domain:domainlist){
            domain.setSalesorderid(salesorder_id);
        }
        salesorderdetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "salesorderdetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.salesorderdetailService.get(#salesorderdetail_id),'iBizBusinessCentral-SalesOrderDetail-Update')")
    @ApiOperation(value = "根据客户联系人商机报价单订单更新订单产品", tags = {"订单产品" },  notes = "根据客户联系人商机报价单订单更新订单产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/{salesorderdetail_id}")
    public ResponseEntity<SalesOrderDetailDTO> updateByAccountContactOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("salesorderdetail_id") String salesorderdetail_id, @RequestBody SalesOrderDetailDTO salesorderdetaildto) {
        SalesOrderDetail domain = salesorderdetailMapping.toDomain(salesorderdetaildto);
        domain.setSalesorderid(salesorder_id);
        domain.setSalesorderdetailid(salesorderdetail_id);
		salesorderdetailService.update(domain);
        SalesOrderDetailDTO dto = salesorderdetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.salesorderdetailService.getSalesorderdetailByEntities(this.salesorderdetailMapping.toDomain(#salesorderdetaildtos)),'iBizBusinessCentral-SalesOrderDetail-Update')")
    @ApiOperation(value = "根据客户联系人商机报价单订单批量更新订单产品", tags = {"订单产品" },  notes = "根据客户联系人商机报价单订单批量更新订单产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/batch")
    public ResponseEntity<Boolean> updateBatchByAccountContactOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody List<SalesOrderDetailDTO> salesorderdetaildtos) {
        List<SalesOrderDetail> domainlist=salesorderdetailMapping.toDomain(salesorderdetaildtos);
        for(SalesOrderDetail domain:domainlist){
            domain.setSalesorderid(salesorder_id);
        }
        salesorderdetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.salesorderdetailService.get(#salesorderdetail_id),'iBizBusinessCentral-SalesOrderDetail-Remove')")
    @ApiOperation(value = "根据客户联系人商机报价单订单删除订单产品", tags = {"订单产品" },  notes = "根据客户联系人商机报价单订单删除订单产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/{salesorderdetail_id}")
    public ResponseEntity<Boolean> removeByAccountContactOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("salesorderdetail_id") String salesorderdetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(salesorderdetailService.remove(salesorderdetail_id));
    }

    @PreAuthorize("hasPermission(this.salesorderdetailService.getSalesorderdetailByIds(#ids),'iBizBusinessCentral-SalesOrderDetail-Remove')")
    @ApiOperation(value = "根据客户联系人商机报价单订单批量删除订单产品", tags = {"订单产品" },  notes = "根据客户联系人商机报价单订单批量删除订单产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/batch")
    public ResponseEntity<Boolean> removeBatchByAccountContactOpportunityQuoteSalesOrder(@RequestBody List<String> ids) {
        salesorderdetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.salesorderdetailMapping.toDomain(returnObject.body),'iBizBusinessCentral-SalesOrderDetail-Get')")
    @ApiOperation(value = "根据客户联系人商机报价单订单获取订单产品", tags = {"订单产品" },  notes = "根据客户联系人商机报价单订单获取订单产品")
	@RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/{salesorderdetail_id}")
    public ResponseEntity<SalesOrderDetailDTO> getByAccountContactOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("salesorderdetail_id") String salesorderdetail_id) {
        SalesOrderDetail domain = salesorderdetailService.get(salesorderdetail_id);
        SalesOrderDetailDTO dto = salesorderdetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据客户联系人商机报价单订单获取订单产品草稿", tags = {"订单产品" },  notes = "根据客户联系人商机报价单订单获取订单产品草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/getdraft")
    public ResponseEntity<SalesOrderDetailDTO> getDraftByAccountContactOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id) {
        SalesOrderDetail domain = new SalesOrderDetail();
        domain.setSalesorderid(salesorder_id);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderdetailMapping.toDto(salesorderdetailService.getDraft(domain)));
    }

    @ApiOperation(value = "根据客户联系人商机报价单订单检查订单产品", tags = {"订单产品" },  notes = "根据客户联系人商机报价单订单检查订单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByAccountContactOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDetailDTO salesorderdetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(salesorderdetailService.checkKey(salesorderdetailMapping.toDomain(salesorderdetaildto)));
    }

    @PreAuthorize("hasPermission(this.salesorderdetailMapping.toDomain(#salesorderdetaildto),'iBizBusinessCentral-SalesOrderDetail-Save')")
    @ApiOperation(value = "根据客户联系人商机报价单订单保存订单产品", tags = {"订单产品" },  notes = "根据客户联系人商机报价单订单保存订单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/save")
    public ResponseEntity<Boolean> saveByAccountContactOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDetailDTO salesorderdetaildto) {
        SalesOrderDetail domain = salesorderdetailMapping.toDomain(salesorderdetaildto);
        domain.setSalesorderid(salesorder_id);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderdetailService.save(domain));
    }

    @PreAuthorize("hasPermission(this.salesorderdetailMapping.toDomain(#salesorderdetaildtos),'iBizBusinessCentral-SalesOrderDetail-Save')")
    @ApiOperation(value = "根据客户联系人商机报价单订单批量保存订单产品", tags = {"订单产品" },  notes = "根据客户联系人商机报价单订单批量保存订单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByAccountContactOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody List<SalesOrderDetailDTO> salesorderdetaildtos) {
        List<SalesOrderDetail> domainlist=salesorderdetailMapping.toDomain(salesorderdetaildtos);
        for(SalesOrderDetail domain:domainlist){
             domain.setSalesorderid(salesorder_id);
        }
        salesorderdetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrderDetail-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrderDetail-Get')")
	@ApiOperation(value = "根据客户联系人商机报价单订单获取DEFAULT", tags = {"订单产品" } ,notes = "根据客户联系人商机报价单订单获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/fetchdefault")
	public ResponseEntity<List<SalesOrderDetailDTO>> fetchSalesOrderDetailDefaultByAccountContactOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id,SalesOrderDetailSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<SalesOrderDetail> domains = salesorderdetailService.searchDefault(context) ;
        List<SalesOrderDetailDTO> list = salesorderdetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrderDetail-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrderDetail-Get')")
	@ApiOperation(value = "根据客户联系人商机报价单订单查询DEFAULT", tags = {"订单产品" } ,notes = "根据客户联系人商机报价单订单查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/salesorderdetails/searchdefault")
	public ResponseEntity<Page<SalesOrderDetailDTO>> searchSalesOrderDetailDefaultByAccountContactOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDetailSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<SalesOrderDetail> domains = salesorderdetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderdetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}


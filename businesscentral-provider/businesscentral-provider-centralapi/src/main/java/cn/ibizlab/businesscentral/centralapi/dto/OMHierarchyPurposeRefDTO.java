package cn.ibizlab.businesscentral.centralapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[OMHierarchyPurposeRefDTO]
 */
@Data
public class OMHierarchyPurposeRefDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;

    /**
     * 属性 [OMHIERARCHYPURPOSEREFNAME]
     *
     */
    @JSONField(name = "omhierarchypurposerefname")
    @JsonProperty("omhierarchypurposerefname")
    private String omhierarchypurposerefname;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [OMHIERARCHYPURPOSEREFID]
     *
     */
    @JSONField(name = "omhierarchypurposerefid")
    @JsonProperty("omhierarchypurposerefid")
    private String omhierarchypurposerefid;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;

    /**
     * 属性 [OMHIERARCHYCATID]
     *
     */
    @JSONField(name = "omhierarchycatid")
    @JsonProperty("omhierarchycatid")
    private String omhierarchycatid;

    /**
     * 属性 [OMHIERARCHYPURPOSEID]
     *
     */
    @JSONField(name = "omhierarchypurposeid")
    @JsonProperty("omhierarchypurposeid")
    private String omhierarchypurposeid;

    /**
     * 属性 [ISDEFAULT]
     *
     */
    @JSONField(name = "isdefault")
    @JsonProperty("isdefault")
    private Integer isdefault;

    /**
     * 属性 [ENDDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "enddate" , format="yyyy-MM-dd")
    @JsonProperty("enddate")
    private Timestamp enddate;

    /**
     * 属性 [BEGINDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "begindate" , format="yyyy-MM-dd")
    @JsonProperty("begindate")
    private Timestamp begindate;


    /**
     * 设置 [OMHIERARCHYPURPOSEREFNAME]
     */
    public void setOmhierarchypurposerefname(String  omhierarchypurposerefname){
        this.omhierarchypurposerefname = omhierarchypurposerefname ;
        this.modify("omhierarchypurposerefname",omhierarchypurposerefname);
    }

    /**
     * 设置 [OMHIERARCHYCATID]
     */
    public void setOmhierarchycatid(String  omhierarchycatid){
        this.omhierarchycatid = omhierarchycatid ;
        this.modify("omhierarchycatid",omhierarchycatid);
    }

    /**
     * 设置 [OMHIERARCHYPURPOSEID]
     */
    public void setOmhierarchypurposeid(String  omhierarchypurposeid){
        this.omhierarchypurposeid = omhierarchypurposeid ;
        this.modify("omhierarchypurposeid",omhierarchypurposeid);
    }

    /**
     * 设置 [ISDEFAULT]
     */
    public void setIsdefault(Integer  isdefault){
        this.isdefault = isdefault ;
        this.modify("isdefault",isdefault);
    }

    /**
     * 设置 [ENDDATE]
     */
    public void setEnddate(Timestamp  enddate){
        this.enddate = enddate ;
        this.modify("enddate",enddate);
    }

    /**
     * 设置 [BEGINDATE]
     */
    public void setBegindate(Timestamp  begindate){
        this.begindate = begindate ;
        this.modify("begindate",begindate);
    }


}


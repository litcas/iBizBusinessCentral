package cn.ibizlab.businesscentral.centralapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.base.domain.OMHierarchyCat;
import cn.ibizlab.businesscentral.centralapi.dto.OMHierarchyCatDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CentralApiOMHierarchyCatMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface OMHierarchyCatMapping extends MappingBase<OMHierarchyCatDTO, OMHierarchyCat> {


}


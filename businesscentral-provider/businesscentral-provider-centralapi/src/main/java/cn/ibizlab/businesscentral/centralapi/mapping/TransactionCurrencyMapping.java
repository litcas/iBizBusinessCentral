package cn.ibizlab.businesscentral.centralapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency;
import cn.ibizlab.businesscentral.centralapi.dto.TransactionCurrencyDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CentralApiTransactionCurrencyMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface TransactionCurrencyMapping extends MappingBase<TransactionCurrencyDTO, TransactionCurrency> {


}


package cn.ibizlab.businesscentral.centralapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[IncidentDTO]
 */
@Data
public class IncidentDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [LASTONHOLDTIME]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "lastonholdtime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("lastonholdtime")
    private Timestamp lastonholdtime;

    /**
     * 属性 [VERSIONNUMBER]
     *
     */
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;

    /**
     * 属性 [FIRSTRESPONSESLASTATUS]
     *
     */
    @JSONField(name = "firstresponseslastatus")
    @JsonProperty("firstresponseslastatus")
    private String firstresponseslastatus;

    /**
     * 属性 [INCIDENTSTAGECODE]
     *
     */
    @JSONField(name = "incidentstagecode")
    @JsonProperty("incidentstagecode")
    private String incidentstagecode;

    /**
     * 属性 [INFLUENCESCORE]
     *
     */
    @JSONField(name = "influencescore")
    @JsonProperty("influencescore")
    private Double influencescore;

    /**
     * 属性 [SOCIALPROFILEID]
     *
     */
    @JSONField(name = "socialprofileid")
    @JsonProperty("socialprofileid")
    private String socialprofileid;

    /**
     * 属性 [TICKETNUMBER]
     *
     */
    @JSONField(name = "ticketnumber")
    @JsonProperty("ticketnumber")
    private String ticketnumber;

    /**
     * 属性 [MERGED]
     *
     */
    @JSONField(name = "merged")
    @JsonProperty("merged")
    private Integer merged;

    /**
     * 属性 [PRODUCTSERIALNUMBER]
     *
     */
    @JSONField(name = "productserialnumber")
    @JsonProperty("productserialnumber")
    private String productserialnumber;

    /**
     * 属性 [ACCOUNTNAME]
     *
     */
    @JSONField(name = "accountname")
    @JsonProperty("accountname")
    private String accountname;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 属性 [ESCALATED]
     *
     */
    @JSONField(name = "escalated")
    @JsonProperty("escalated")
    private Integer escalated;

    /**
     * 属性 [STAGEID]
     *
     */
    @JSONField(name = "stageid")
    @JsonProperty("stageid")
    private String stageid;

    /**
     * 属性 [CASETYPECODE]
     *
     */
    @JSONField(name = "casetypecode")
    @JsonProperty("casetypecode")
    private String casetypecode;

    /**
     * 属性 [EMAILADDRESS]
     *
     */
    @JSONField(name = "emailaddress")
    @JsonProperty("emailaddress")
    private String emailaddress;

    /**
     * 属性 [CHECKEMAIL]
     *
     */
    @JSONField(name = "checkemail")
    @JsonProperty("checkemail")
    private Integer checkemail;

    /**
     * 属性 [SEVERITYCODE]
     *
     */
    @JSONField(name = "severitycode")
    @JsonProperty("severitycode")
    private String severitycode;

    /**
     * 属性 [ESCALATEDON]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "escalatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("escalatedon")
    private Timestamp escalatedon;

    /**
     * 属性 [UTCCONVERSIONTIMEZONECODE]
     *
     */
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;

    /**
     * 属性 [OWNERID]
     *
     */
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;

    /**
     * 属性 [BILLEDSERVICEUNITS]
     *
     */
    @JSONField(name = "billedserviceunits")
    @JsonProperty("billedserviceunits")
    private Integer billedserviceunits;

    /**
     * 属性 [PRIORITYCODE]
     *
     */
    @JSONField(name = "prioritycode")
    @JsonProperty("prioritycode")
    private String prioritycode;

    /**
     * 属性 [ENTITYIMAGE]
     *
     */
    @JSONField(name = "entityimage")
    @JsonProperty("entityimage")
    private String entityimage;

    /**
     * 属性 [ONHOLDTIME]
     *
     */
    @JSONField(name = "onholdtime")
    @JsonProperty("onholdtime")
    private Integer onholdtime;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;

    /**
     * 属性 [KBARTICLEID]
     *
     */
    @JSONField(name = "kbarticleid")
    @JsonProperty("kbarticleid")
    private String kbarticleid;

    /**
     * 属性 [CONTRACTSERVICELEVELCODE]
     *
     */
    @JSONField(name = "contractservicelevelcode")
    @JsonProperty("contractservicelevelcode")
    private String contractservicelevelcode;

    /**
     * 属性 [OWNERTYPE]
     *
     */
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;

    /**
     * 属性 [OWNERNAME]
     *
     */
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;

    /**
     * 属性 [ENTITYIMAGE_URL]
     *
     */
    @JSONField(name = "entityimage_url")
    @JsonProperty("entityimage_url")
    private String entityimageUrl;

    /**
     * 属性 [CUSTOMERCONTACTED]
     *
     */
    @JSONField(name = "customercontacted")
    @JsonProperty("customercontacted")
    private Integer customercontacted;

    /**
     * 属性 [PROCESSID]
     *
     */
    @JSONField(name = "processid")
    @JsonProperty("processid")
    private String processid;

    /**
     * 属性 [TIMEZONERULEVERSIONNUMBER]
     *
     */
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [DECREMENTING]
     *
     */
    @JSONField(name = "decrementing")
    @JsonProperty("decrementing")
    private Integer decrementing;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;

    /**
     * 属性 [STATUSCODE]
     *
     */
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;

    /**
     * 属性 [RESOLVEBY]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "resolveby" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("resolveby")
    private Timestamp resolveby;

    /**
     * 属性 [RESPONSEBY]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "responseby" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("responseby")
    private Timestamp responseby;

    /**
     * 属性 [ENTITYIMAGEID]
     *
     */
    @JSONField(name = "entityimageid")
    @JsonProperty("entityimageid")
    private String entityimageid;

    /**
     * 属性 [FOLLOWUPTASKCREATED]
     *
     */
    @JSONField(name = "followuptaskcreated")
    @JsonProperty("followuptaskcreated")
    private Integer followuptaskcreated;

    /**
     * 属性 [FIRSTRESPONSESENT]
     *
     */
    @JSONField(name = "firstresponsesent")
    @JsonProperty("firstresponsesent")
    private Integer firstresponsesent;

    /**
     * 属性 [FOLLOWUPBY]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "followupby" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("followupby")
    private Timestamp followupby;

    /**
     * 属性 [INCIDENTID]
     *
     */
    @JSONField(name = "incidentid")
    @JsonProperty("incidentid")
    private String incidentid;

    /**
     * 属性 [SERVICESTAGE]
     *
     */
    @JSONField(name = "servicestage")
    @JsonProperty("servicestage")
    private String servicestage;

    /**
     * 属性 [TRAVERSEDPATH]
     *
     */
    @JSONField(name = "traversedpath")
    @JsonProperty("traversedpath")
    private String traversedpath;

    /**
     * 属性 [NUMBEROFCHILDINCIDENTS]
     *
     */
    @JSONField(name = "numberofchildincidents")
    @JsonProperty("numberofchildincidents")
    private Integer numberofchildincidents;

    /**
     * 属性 [BLOCKEDPROFILE]
     *
     */
    @JSONField(name = "blockedprofile")
    @JsonProperty("blockedprofile")
    private Integer blockedprofile;

    /**
     * 属性 [OVERRIDDENCREATEDON]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;

    /**
     * 属性 [ACTUALSERVICEUNITS]
     *
     */
    @JSONField(name = "actualserviceunits")
    @JsonProperty("actualserviceunits")
    private Integer actualserviceunits;

    /**
     * 属性 [MESSAGETYPECODE]
     *
     */
    @JSONField(name = "messagetypecode")
    @JsonProperty("messagetypecode")
    private String messagetypecode;

    /**
     * 属性 [CONTACTNAME]
     *
     */
    @JSONField(name = "contactname")
    @JsonProperty("contactname")
    private String contactname;

    /**
     * 属性 [ROUTECASE]
     *
     */
    @JSONField(name = "routecase")
    @JsonProperty("routecase")
    private Integer routecase;

    /**
     * 属性 [STATECODE]
     *
     */
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;

    /**
     * 属性 [IMPORTSEQUENCENUMBER]
     *
     */
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;

    /**
     * 属性 [RESOLVEBYSLASTATUS]
     *
     */
    @JSONField(name = "resolvebyslastatus")
    @JsonProperty("resolvebyslastatus")
    private String resolvebyslastatus;

    /**
     * 属性 [CASEORIGINCODE]
     *
     */
    @JSONField(name = "caseorigincode")
    @JsonProperty("caseorigincode")
    private String caseorigincode;

    /**
     * 属性 [ACTIVITIESCOMPLETE]
     *
     */
    @JSONField(name = "activitiescomplete")
    @JsonProperty("activitiescomplete")
    private Integer activitiescomplete;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [DECREMENTENTITLEMENTTERM]
     *
     */
    @JSONField(name = "decremententitlementterm")
    @JsonProperty("decremententitlementterm")
    private Integer decremententitlementterm;

    /**
     * 属性 [CUSTOMERSATISFACTIONCODE]
     *
     */
    @JSONField(name = "customersatisfactioncode")
    @JsonProperty("customersatisfactioncode")
    private String customersatisfactioncode;

    /**
     * 属性 [SENTIMENTVALUE]
     *
     */
    @JSONField(name = "sentimentvalue")
    @JsonProperty("sentimentvalue")
    private Double sentimentvalue;

    /**
     * 属性 [ENTITYIMAGE_TIMESTAMP]
     *
     */
    @JSONField(name = "entityimage_timestamp")
    @JsonProperty("entityimage_timestamp")
    private BigInteger entityimageTimestamp;

    /**
     * 属性 [TITLE]
     *
     */
    @JSONField(name = "title")
    @JsonProperty("title")
    private String title;

    /**
     * 属性 [EXCHANGERATE]
     *
     */
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;

    /**
     * 属性 [EXISTINGCASENAME]
     *
     */
    @JSONField(name = "existingcasename")
    @JsonProperty("existingcasename")
    private String existingcasename;

    /**
     * 属性 [SUBJECTNAME]
     *
     */
    @JSONField(name = "subjectname")
    @JsonProperty("subjectname")
    private String subjectname;

    /**
     * 属性 [CONTRACTDETAILNAME]
     *
     */
    @JSONField(name = "contractdetailname")
    @JsonProperty("contractdetailname")
    private String contractdetailname;

    /**
     * 属性 [PARENTCASENAME]
     *
     */
    @JSONField(name = "parentcasename")
    @JsonProperty("parentcasename")
    private String parentcasename;

    /**
     * 属性 [ENTITLEMENTNAME]
     *
     */
    @JSONField(name = "entitlementname")
    @JsonProperty("entitlementname")
    private String entitlementname;

    /**
     * 属性 [MASTERNAME]
     *
     */
    @JSONField(name = "mastername")
    @JsonProperty("mastername")
    private String mastername;

    /**
     * 属性 [CONTRACTNAME]
     *
     */
    @JSONField(name = "contractname")
    @JsonProperty("contractname")
    private String contractname;

    /**
     * 属性 [CUSTOMERNAME]
     *
     */
    @JSONField(name = "customername")
    @JsonProperty("customername")
    private String customername;

    /**
     * 属性 [CUSTOMERTYPE]
     *
     */
    @JSONField(name = "customertype")
    @JsonProperty("customertype")
    private String customertype;

    /**
     * 属性 [CURRENCYNAME]
     *
     */
    @JSONField(name = "currencyname")
    @JsonProperty("currencyname")
    private String currencyname;

    /**
     * 属性 [PRODUCTNAME]
     *
     */
    @JSONField(name = "productname")
    @JsonProperty("productname")
    private String productname;

    /**
     * 属性 [SLANAME]
     *
     */
    @JSONField(name = "slaname")
    @JsonProperty("slaname")
    private String slaname;

    /**
     * 属性 [FIRSTRESPONSEBYKPINAME]
     *
     */
    @JSONField(name = "firstresponsebykpiname")
    @JsonProperty("firstresponsebykpiname")
    private String firstresponsebykpiname;

    /**
     * 属性 [RESOLVEBYKPINAME]
     *
     */
    @JSONField(name = "resolvebykpiname")
    @JsonProperty("resolvebykpiname")
    private String resolvebykpiname;

    /**
     * 属性 [PRIMARYCONTACTNAME]
     *
     */
    @JSONField(name = "primarycontactname")
    @JsonProperty("primarycontactname")
    private String primarycontactname;

    /**
     * 属性 [RESPONSIBLECONTACTNAME]
     *
     */
    @JSONField(name = "responsiblecontactname")
    @JsonProperty("responsiblecontactname")
    private String responsiblecontactname;

    /**
     * 属性 [ENTITLEMENTID]
     *
     */
    @JSONField(name = "entitlementid")
    @JsonProperty("entitlementid")
    private String entitlementid;

    /**
     * 属性 [CUSTOMERID]
     *
     */
    @JSONField(name = "customerid")
    @JsonProperty("customerid")
    private String customerid;

    /**
     * 属性 [FIRSTRESPONSEBYKPIID]
     *
     */
    @JSONField(name = "firstresponsebykpiid")
    @JsonProperty("firstresponsebykpiid")
    private String firstresponsebykpiid;

    /**
     * 属性 [SLAID]
     *
     */
    @JSONField(name = "slaid")
    @JsonProperty("slaid")
    private String slaid;

    /**
     * 属性 [EXISTINGCASE]
     *
     */
    @JSONField(name = "existingcase")
    @JsonProperty("existingcase")
    private String existingcase;

    /**
     * 属性 [PRODUCTID]
     *
     */
    @JSONField(name = "productid")
    @JsonProperty("productid")
    private String productid;

    /**
     * 属性 [CONTRACTID]
     *
     */
    @JSONField(name = "contractid")
    @JsonProperty("contractid")
    private String contractid;

    /**
     * 属性 [RESPONSIBLECONTACTID]
     *
     */
    @JSONField(name = "responsiblecontactid")
    @JsonProperty("responsiblecontactid")
    private String responsiblecontactid;

    /**
     * 属性 [RESOLVEBYKPIID]
     *
     */
    @JSONField(name = "resolvebykpiid")
    @JsonProperty("resolvebykpiid")
    private String resolvebykpiid;

    /**
     * 属性 [CONTRACTDETAILID]
     *
     */
    @JSONField(name = "contractdetailid")
    @JsonProperty("contractdetailid")
    private String contractdetailid;

    /**
     * 属性 [SUBJECTID]
     *
     */
    @JSONField(name = "subjectid")
    @JsonProperty("subjectid")
    private String subjectid;

    /**
     * 属性 [MASTERID]
     *
     */
    @JSONField(name = "masterid")
    @JsonProperty("masterid")
    private String masterid;

    /**
     * 属性 [PARENTCASEID]
     *
     */
    @JSONField(name = "parentcaseid")
    @JsonProperty("parentcaseid")
    private String parentcaseid;

    /**
     * 属性 [PRIMARYCONTACTID]
     *
     */
    @JSONField(name = "primarycontactid")
    @JsonProperty("primarycontactid")
    private String primarycontactid;

    /**
     * 属性 [TRANSACTIONCURRENCYID]
     *
     */
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;


    /**
     * 设置 [LASTONHOLDTIME]
     */
    public void setLastonholdtime(Timestamp  lastonholdtime){
        this.lastonholdtime = lastonholdtime ;
        this.modify("lastonholdtime",lastonholdtime);
    }

    /**
     * 设置 [VERSIONNUMBER]
     */
    public void setVersionnumber(BigInteger  versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [FIRSTRESPONSESLASTATUS]
     */
    public void setFirstresponseslastatus(String  firstresponseslastatus){
        this.firstresponseslastatus = firstresponseslastatus ;
        this.modify("firstresponseslastatus",firstresponseslastatus);
    }

    /**
     * 设置 [INCIDENTSTAGECODE]
     */
    public void setIncidentstagecode(String  incidentstagecode){
        this.incidentstagecode = incidentstagecode ;
        this.modify("incidentstagecode",incidentstagecode);
    }

    /**
     * 设置 [INFLUENCESCORE]
     */
    public void setInfluencescore(Double  influencescore){
        this.influencescore = influencescore ;
        this.modify("influencescore",influencescore);
    }

    /**
     * 设置 [SOCIALPROFILEID]
     */
    public void setSocialprofileid(String  socialprofileid){
        this.socialprofileid = socialprofileid ;
        this.modify("socialprofileid",socialprofileid);
    }

    /**
     * 设置 [TICKETNUMBER]
     */
    public void setTicketnumber(String  ticketnumber){
        this.ticketnumber = ticketnumber ;
        this.modify("ticketnumber",ticketnumber);
    }

    /**
     * 设置 [MERGED]
     */
    public void setMerged(Integer  merged){
        this.merged = merged ;
        this.modify("merged",merged);
    }

    /**
     * 设置 [PRODUCTSERIALNUMBER]
     */
    public void setProductserialnumber(String  productserialnumber){
        this.productserialnumber = productserialnumber ;
        this.modify("productserialnumber",productserialnumber);
    }

    /**
     * 设置 [ACCOUNTNAME]
     */
    public void setAccountname(String  accountname){
        this.accountname = accountname ;
        this.modify("accountname",accountname);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [ESCALATED]
     */
    public void setEscalated(Integer  escalated){
        this.escalated = escalated ;
        this.modify("escalated",escalated);
    }

    /**
     * 设置 [STAGEID]
     */
    public void setStageid(String  stageid){
        this.stageid = stageid ;
        this.modify("stageid",stageid);
    }

    /**
     * 设置 [CASETYPECODE]
     */
    public void setCasetypecode(String  casetypecode){
        this.casetypecode = casetypecode ;
        this.modify("casetypecode",casetypecode);
    }

    /**
     * 设置 [EMAILADDRESS]
     */
    public void setEmailaddress(String  emailaddress){
        this.emailaddress = emailaddress ;
        this.modify("emailaddress",emailaddress);
    }

    /**
     * 设置 [CHECKEMAIL]
     */
    public void setCheckemail(Integer  checkemail){
        this.checkemail = checkemail ;
        this.modify("checkemail",checkemail);
    }

    /**
     * 设置 [SEVERITYCODE]
     */
    public void setSeveritycode(String  severitycode){
        this.severitycode = severitycode ;
        this.modify("severitycode",severitycode);
    }

    /**
     * 设置 [ESCALATEDON]
     */
    public void setEscalatedon(Timestamp  escalatedon){
        this.escalatedon = escalatedon ;
        this.modify("escalatedon",escalatedon);
    }

    /**
     * 设置 [UTCCONVERSIONTIMEZONECODE]
     */
    public void setUtcconversiontimezonecode(Integer  utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [OWNERID]
     */
    public void setOwnerid(String  ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [BILLEDSERVICEUNITS]
     */
    public void setBilledserviceunits(Integer  billedserviceunits){
        this.billedserviceunits = billedserviceunits ;
        this.modify("billedserviceunits",billedserviceunits);
    }

    /**
     * 设置 [PRIORITYCODE]
     */
    public void setPrioritycode(String  prioritycode){
        this.prioritycode = prioritycode ;
        this.modify("prioritycode",prioritycode);
    }

    /**
     * 设置 [ENTITYIMAGE]
     */
    public void setEntityimage(String  entityimage){
        this.entityimage = entityimage ;
        this.modify("entityimage",entityimage);
    }

    /**
     * 设置 [ONHOLDTIME]
     */
    public void setOnholdtime(Integer  onholdtime){
        this.onholdtime = onholdtime ;
        this.modify("onholdtime",onholdtime);
    }

    /**
     * 设置 [KBARTICLEID]
     */
    public void setKbarticleid(String  kbarticleid){
        this.kbarticleid = kbarticleid ;
        this.modify("kbarticleid",kbarticleid);
    }

    /**
     * 设置 [CONTRACTSERVICELEVELCODE]
     */
    public void setContractservicelevelcode(String  contractservicelevelcode){
        this.contractservicelevelcode = contractservicelevelcode ;
        this.modify("contractservicelevelcode",contractservicelevelcode);
    }

    /**
     * 设置 [OWNERTYPE]
     */
    public void setOwnertype(String  ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [OWNERNAME]
     */
    public void setOwnername(String  ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [ENTITYIMAGE_URL]
     */
    public void setEntityimageUrl(String  entityimageUrl){
        this.entityimageUrl = entityimageUrl ;
        this.modify("entityimage_url",entityimageUrl);
    }

    /**
     * 设置 [CUSTOMERCONTACTED]
     */
    public void setCustomercontacted(Integer  customercontacted){
        this.customercontacted = customercontacted ;
        this.modify("customercontacted",customercontacted);
    }

    /**
     * 设置 [PROCESSID]
     */
    public void setProcessid(String  processid){
        this.processid = processid ;
        this.modify("processid",processid);
    }

    /**
     * 设置 [TIMEZONERULEVERSIONNUMBER]
     */
    public void setTimezoneruleversionnumber(Integer  timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [DECREMENTING]
     */
    public void setDecrementing(Integer  decrementing){
        this.decrementing = decrementing ;
        this.modify("decrementing",decrementing);
    }

    /**
     * 设置 [STATUSCODE]
     */
    public void setStatuscode(Integer  statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [RESOLVEBY]
     */
    public void setResolveby(Timestamp  resolveby){
        this.resolveby = resolveby ;
        this.modify("resolveby",resolveby);
    }

    /**
     * 设置 [RESPONSEBY]
     */
    public void setResponseby(Timestamp  responseby){
        this.responseby = responseby ;
        this.modify("responseby",responseby);
    }

    /**
     * 设置 [ENTITYIMAGEID]
     */
    public void setEntityimageid(String  entityimageid){
        this.entityimageid = entityimageid ;
        this.modify("entityimageid",entityimageid);
    }

    /**
     * 设置 [FOLLOWUPTASKCREATED]
     */
    public void setFollowuptaskcreated(Integer  followuptaskcreated){
        this.followuptaskcreated = followuptaskcreated ;
        this.modify("followuptaskcreated",followuptaskcreated);
    }

    /**
     * 设置 [FIRSTRESPONSESENT]
     */
    public void setFirstresponsesent(Integer  firstresponsesent){
        this.firstresponsesent = firstresponsesent ;
        this.modify("firstresponsesent",firstresponsesent);
    }

    /**
     * 设置 [FOLLOWUPBY]
     */
    public void setFollowupby(Timestamp  followupby){
        this.followupby = followupby ;
        this.modify("followupby",followupby);
    }

    /**
     * 设置 [SERVICESTAGE]
     */
    public void setServicestage(String  servicestage){
        this.servicestage = servicestage ;
        this.modify("servicestage",servicestage);
    }

    /**
     * 设置 [TRAVERSEDPATH]
     */
    public void setTraversedpath(String  traversedpath){
        this.traversedpath = traversedpath ;
        this.modify("traversedpath",traversedpath);
    }

    /**
     * 设置 [NUMBEROFCHILDINCIDENTS]
     */
    public void setNumberofchildincidents(Integer  numberofchildincidents){
        this.numberofchildincidents = numberofchildincidents ;
        this.modify("numberofchildincidents",numberofchildincidents);
    }

    /**
     * 设置 [BLOCKEDPROFILE]
     */
    public void setBlockedprofile(Integer  blockedprofile){
        this.blockedprofile = blockedprofile ;
        this.modify("blockedprofile",blockedprofile);
    }

    /**
     * 设置 [OVERRIDDENCREATEDON]
     */
    public void setOverriddencreatedon(Timestamp  overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 设置 [ACTUALSERVICEUNITS]
     */
    public void setActualserviceunits(Integer  actualserviceunits){
        this.actualserviceunits = actualserviceunits ;
        this.modify("actualserviceunits",actualserviceunits);
    }

    /**
     * 设置 [MESSAGETYPECODE]
     */
    public void setMessagetypecode(String  messagetypecode){
        this.messagetypecode = messagetypecode ;
        this.modify("messagetypecode",messagetypecode);
    }

    /**
     * 设置 [CONTACTNAME]
     */
    public void setContactname(String  contactname){
        this.contactname = contactname ;
        this.modify("contactname",contactname);
    }

    /**
     * 设置 [ROUTECASE]
     */
    public void setRoutecase(Integer  routecase){
        this.routecase = routecase ;
        this.modify("routecase",routecase);
    }

    /**
     * 设置 [STATECODE]
     */
    public void setStatecode(Integer  statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [IMPORTSEQUENCENUMBER]
     */
    public void setImportsequencenumber(Integer  importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [RESOLVEBYSLASTATUS]
     */
    public void setResolvebyslastatus(String  resolvebyslastatus){
        this.resolvebyslastatus = resolvebyslastatus ;
        this.modify("resolvebyslastatus",resolvebyslastatus);
    }

    /**
     * 设置 [CASEORIGINCODE]
     */
    public void setCaseorigincode(String  caseorigincode){
        this.caseorigincode = caseorigincode ;
        this.modify("caseorigincode",caseorigincode);
    }

    /**
     * 设置 [ACTIVITIESCOMPLETE]
     */
    public void setActivitiescomplete(Integer  activitiescomplete){
        this.activitiescomplete = activitiescomplete ;
        this.modify("activitiescomplete",activitiescomplete);
    }

    /**
     * 设置 [DECREMENTENTITLEMENTTERM]
     */
    public void setDecremententitlementterm(Integer  decremententitlementterm){
        this.decremententitlementterm = decremententitlementterm ;
        this.modify("decremententitlementterm",decremententitlementterm);
    }

    /**
     * 设置 [CUSTOMERSATISFACTIONCODE]
     */
    public void setCustomersatisfactioncode(String  customersatisfactioncode){
        this.customersatisfactioncode = customersatisfactioncode ;
        this.modify("customersatisfactioncode",customersatisfactioncode);
    }

    /**
     * 设置 [SENTIMENTVALUE]
     */
    public void setSentimentvalue(Double  sentimentvalue){
        this.sentimentvalue = sentimentvalue ;
        this.modify("sentimentvalue",sentimentvalue);
    }

    /**
     * 设置 [ENTITYIMAGE_TIMESTAMP]
     */
    public void setEntityimageTimestamp(BigInteger  entityimageTimestamp){
        this.entityimageTimestamp = entityimageTimestamp ;
        this.modify("entityimage_timestamp",entityimageTimestamp);
    }

    /**
     * 设置 [TITLE]
     */
    public void setTitle(String  title){
        this.title = title ;
        this.modify("title",title);
    }

    /**
     * 设置 [EXCHANGERATE]
     */
    public void setExchangerate(BigDecimal  exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [EXISTINGCASENAME]
     */
    public void setExistingcasename(String  existingcasename){
        this.existingcasename = existingcasename ;
        this.modify("existingcasename",existingcasename);
    }

    /**
     * 设置 [SUBJECTNAME]
     */
    public void setSubjectname(String  subjectname){
        this.subjectname = subjectname ;
        this.modify("subjectname",subjectname);
    }

    /**
     * 设置 [CONTRACTDETAILNAME]
     */
    public void setContractdetailname(String  contractdetailname){
        this.contractdetailname = contractdetailname ;
        this.modify("contractdetailname",contractdetailname);
    }

    /**
     * 设置 [PARENTCASENAME]
     */
    public void setParentcasename(String  parentcasename){
        this.parentcasename = parentcasename ;
        this.modify("parentcasename",parentcasename);
    }

    /**
     * 设置 [ENTITLEMENTNAME]
     */
    public void setEntitlementname(String  entitlementname){
        this.entitlementname = entitlementname ;
        this.modify("entitlementname",entitlementname);
    }

    /**
     * 设置 [MASTERNAME]
     */
    public void setMastername(String  mastername){
        this.mastername = mastername ;
        this.modify("mastername",mastername);
    }

    /**
     * 设置 [CONTRACTNAME]
     */
    public void setContractname(String  contractname){
        this.contractname = contractname ;
        this.modify("contractname",contractname);
    }

    /**
     * 设置 [CUSTOMERNAME]
     */
    public void setCustomername(String  customername){
        this.customername = customername ;
        this.modify("customername",customername);
    }

    /**
     * 设置 [CUSTOMERTYPE]
     */
    public void setCustomertype(String  customertype){
        this.customertype = customertype ;
        this.modify("customertype",customertype);
    }

    /**
     * 设置 [CURRENCYNAME]
     */
    public void setCurrencyname(String  currencyname){
        this.currencyname = currencyname ;
        this.modify("currencyname",currencyname);
    }

    /**
     * 设置 [PRODUCTNAME]
     */
    public void setProductname(String  productname){
        this.productname = productname ;
        this.modify("productname",productname);
    }

    /**
     * 设置 [SLANAME]
     */
    public void setSlaname(String  slaname){
        this.slaname = slaname ;
        this.modify("slaname",slaname);
    }

    /**
     * 设置 [FIRSTRESPONSEBYKPINAME]
     */
    public void setFirstresponsebykpiname(String  firstresponsebykpiname){
        this.firstresponsebykpiname = firstresponsebykpiname ;
        this.modify("firstresponsebykpiname",firstresponsebykpiname);
    }

    /**
     * 设置 [RESOLVEBYKPINAME]
     */
    public void setResolvebykpiname(String  resolvebykpiname){
        this.resolvebykpiname = resolvebykpiname ;
        this.modify("resolvebykpiname",resolvebykpiname);
    }

    /**
     * 设置 [PRIMARYCONTACTNAME]
     */
    public void setPrimarycontactname(String  primarycontactname){
        this.primarycontactname = primarycontactname ;
        this.modify("primarycontactname",primarycontactname);
    }

    /**
     * 设置 [RESPONSIBLECONTACTNAME]
     */
    public void setResponsiblecontactname(String  responsiblecontactname){
        this.responsiblecontactname = responsiblecontactname ;
        this.modify("responsiblecontactname",responsiblecontactname);
    }

    /**
     * 设置 [ENTITLEMENTID]
     */
    public void setEntitlementid(String  entitlementid){
        this.entitlementid = entitlementid ;
        this.modify("entitlementid",entitlementid);
    }

    /**
     * 设置 [CUSTOMERID]
     */
    public void setCustomerid(String  customerid){
        this.customerid = customerid ;
        this.modify("customerid",customerid);
    }

    /**
     * 设置 [FIRSTRESPONSEBYKPIID]
     */
    public void setFirstresponsebykpiid(String  firstresponsebykpiid){
        this.firstresponsebykpiid = firstresponsebykpiid ;
        this.modify("firstresponsebykpiid",firstresponsebykpiid);
    }

    /**
     * 设置 [SLAID]
     */
    public void setSlaid(String  slaid){
        this.slaid = slaid ;
        this.modify("slaid",slaid);
    }

    /**
     * 设置 [EXISTINGCASE]
     */
    public void setExistingcase(String  existingcase){
        this.existingcase = existingcase ;
        this.modify("existingcase",existingcase);
    }

    /**
     * 设置 [PRODUCTID]
     */
    public void setProductid(String  productid){
        this.productid = productid ;
        this.modify("productid",productid);
    }

    /**
     * 设置 [CONTRACTID]
     */
    public void setContractid(String  contractid){
        this.contractid = contractid ;
        this.modify("contractid",contractid);
    }

    /**
     * 设置 [RESPONSIBLECONTACTID]
     */
    public void setResponsiblecontactid(String  responsiblecontactid){
        this.responsiblecontactid = responsiblecontactid ;
        this.modify("responsiblecontactid",responsiblecontactid);
    }

    /**
     * 设置 [RESOLVEBYKPIID]
     */
    public void setResolvebykpiid(String  resolvebykpiid){
        this.resolvebykpiid = resolvebykpiid ;
        this.modify("resolvebykpiid",resolvebykpiid);
    }

    /**
     * 设置 [CONTRACTDETAILID]
     */
    public void setContractdetailid(String  contractdetailid){
        this.contractdetailid = contractdetailid ;
        this.modify("contractdetailid",contractdetailid);
    }

    /**
     * 设置 [SUBJECTID]
     */
    public void setSubjectid(String  subjectid){
        this.subjectid = subjectid ;
        this.modify("subjectid",subjectid);
    }

    /**
     * 设置 [MASTERID]
     */
    public void setMasterid(String  masterid){
        this.masterid = masterid ;
        this.modify("masterid",masterid);
    }

    /**
     * 设置 [PARENTCASEID]
     */
    public void setParentcaseid(String  parentcaseid){
        this.parentcaseid = parentcaseid ;
        this.modify("parentcaseid",parentcaseid);
    }

    /**
     * 设置 [PRIMARYCONTACTID]
     */
    public void setPrimarycontactid(String  primarycontactid){
        this.primarycontactid = primarycontactid ;
        this.modify("primarycontactid",primarycontactid);
    }

    /**
     * 设置 [TRANSACTIONCURRENCYID]
     */
    public void setTransactioncurrencyid(String  transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }


}


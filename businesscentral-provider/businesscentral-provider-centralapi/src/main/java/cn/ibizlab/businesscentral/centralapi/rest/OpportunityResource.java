package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.sales.domain.Opportunity;
import cn.ibizlab.businesscentral.core.sales.service.IOpportunityService;
import cn.ibizlab.businesscentral.core.sales.filter.OpportunitySearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"商机" })
@RestController("CentralApi-opportunity")
@RequestMapping("")
public class OpportunityResource {

    @Autowired
    public IOpportunityService opportunityService;

    @Autowired
    @Lazy
    public OpportunityMapping opportunityMapping;

    @PreAuthorize("hasPermission(this.opportunityMapping.toDomain(#opportunitydto),'iBizBusinessCentral-Opportunity-Create')")
    @ApiOperation(value = "新建商机", tags = {"商机" },  notes = "新建商机")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities")
    public ResponseEntity<OpportunityDTO> create(@RequestBody OpportunityDTO opportunitydto) {
        Opportunity domain = opportunityMapping.toDomain(opportunitydto);
		opportunityService.create(domain);
        OpportunityDTO dto = opportunityMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.opportunityMapping.toDomain(#opportunitydtos),'iBizBusinessCentral-Opportunity-Create')")
    @ApiOperation(value = "批量新建商机", tags = {"商机" },  notes = "批量新建商机")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<OpportunityDTO> opportunitydtos) {
        opportunityService.createBatch(opportunityMapping.toDomain(opportunitydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "opportunity" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.opportunityService.get(#opportunity_id),'iBizBusinessCentral-Opportunity-Update')")
    @ApiOperation(value = "更新商机", tags = {"商机" },  notes = "更新商机")
	@RequestMapping(method = RequestMethod.PUT, value = "/opportunities/{opportunity_id}")
    public ResponseEntity<OpportunityDTO> update(@PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityDTO opportunitydto) {
		Opportunity domain  = opportunityMapping.toDomain(opportunitydto);
        domain .setOpportunityid(opportunity_id);
		opportunityService.update(domain );
		OpportunityDTO dto = opportunityMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.opportunityService.getOpportunityByEntities(this.opportunityMapping.toDomain(#opportunitydtos)),'iBizBusinessCentral-Opportunity-Update')")
    @ApiOperation(value = "批量更新商机", tags = {"商机" },  notes = "批量更新商机")
	@RequestMapping(method = RequestMethod.PUT, value = "/opportunities/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<OpportunityDTO> opportunitydtos) {
        opportunityService.updateBatch(opportunityMapping.toDomain(opportunitydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.opportunityService.get(#opportunity_id),'iBizBusinessCentral-Opportunity-Remove')")
    @ApiOperation(value = "删除商机", tags = {"商机" },  notes = "删除商机")
	@RequestMapping(method = RequestMethod.DELETE, value = "/opportunities/{opportunity_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("opportunity_id") String opportunity_id) {
         return ResponseEntity.status(HttpStatus.OK).body(opportunityService.remove(opportunity_id));
    }

    @PreAuthorize("hasPermission(this.opportunityService.getOpportunityByIds(#ids),'iBizBusinessCentral-Opportunity-Remove')")
    @ApiOperation(value = "批量删除商机", tags = {"商机" },  notes = "批量删除商机")
	@RequestMapping(method = RequestMethod.DELETE, value = "/opportunities/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        opportunityService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.opportunityMapping.toDomain(returnObject.body),'iBizBusinessCentral-Opportunity-Get')")
    @ApiOperation(value = "获取商机", tags = {"商机" },  notes = "获取商机")
	@RequestMapping(method = RequestMethod.GET, value = "/opportunities/{opportunity_id}")
    public ResponseEntity<OpportunityDTO> get(@PathVariable("opportunity_id") String opportunity_id) {
        Opportunity domain = opportunityService.get(opportunity_id);
        OpportunityDTO dto = opportunityMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取商机草稿", tags = {"商机" },  notes = "获取商机草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/opportunities/getdraft")
    public ResponseEntity<OpportunityDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(opportunityMapping.toDto(opportunityService.getDraft(new Opportunity())));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-Active-all')")
    @ApiOperation(value = "重新开启商机", tags = {"商机" },  notes = "重新开启商机")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/active")
    public ResponseEntity<OpportunityDTO> active(@PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityDTO opportunitydto) {
        Opportunity domain = opportunityMapping.toDomain(opportunitydto);
domain.setOpportunityid(opportunity_id);
        domain = opportunityService.active(domain);
        opportunitydto = opportunityMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(opportunitydto);
    }

    @ApiOperation(value = "检查商机", tags = {"商机" },  notes = "检查商机")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody OpportunityDTO opportunitydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(opportunityService.checkKey(opportunityMapping.toDomain(opportunitydto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-Lose-all')")
    @ApiOperation(value = "作为弃单", tags = {"商机" },  notes = "作为弃单")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/lose")
    public ResponseEntity<OpportunityDTO> lose(@PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityDTO opportunitydto) {
        Opportunity domain = opportunityMapping.toDomain(opportunitydto);
domain.setOpportunityid(opportunity_id);
        domain = opportunityService.lose(domain);
        opportunitydto = opportunityMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(opportunitydto);
    }

    @PreAuthorize("hasPermission(this.opportunityMapping.toDomain(#opportunitydto),'iBizBusinessCentral-Opportunity-Save')")
    @ApiOperation(value = "保存商机", tags = {"商机" },  notes = "保存商机")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/save")
    public ResponseEntity<Boolean> save(@RequestBody OpportunityDTO opportunitydto) {
        return ResponseEntity.status(HttpStatus.OK).body(opportunityService.save(opportunityMapping.toDomain(opportunitydto)));
    }

    @PreAuthorize("hasPermission(this.opportunityMapping.toDomain(#opportunitydtos),'iBizBusinessCentral-Opportunity-Save')")
    @ApiOperation(value = "批量保存商机", tags = {"商机" },  notes = "批量保存商机")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<OpportunityDTO> opportunitydtos) {
        opportunityService.saveBatch(opportunityMapping.toDomain(opportunitydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-Win-all')")
    @ApiOperation(value = "作为赢单", tags = {"商机" },  notes = "作为赢单")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/win")
    public ResponseEntity<OpportunityDTO> win(@PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityDTO opportunitydto) {
        Opportunity domain = opportunityMapping.toDomain(opportunitydto);
domain.setOpportunityid(opportunity_id);
        domain = opportunityService.win(domain);
        opportunitydto = opportunityMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(opportunitydto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Opportunity-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"商机" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/opportunities/fetchdefault")
	public ResponseEntity<List<OpportunityDTO>> fetchDefault(OpportunitySearchContext context) {
        Page<Opportunity> domains = opportunityService.searchDefault(context) ;
        List<OpportunityDTO> list = opportunityMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Opportunity-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"商机" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/opportunities/searchdefault")
	public ResponseEntity<Page<OpportunityDTO>> searchDefault(@RequestBody OpportunitySearchContext context) {
        Page<Opportunity> domains = opportunityService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(opportunityMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-searchLost-all') and hasPermission(#context,'iBizBusinessCentral-Opportunity-Get')")
	@ApiOperation(value = "获取丢单商机", tags = {"商机" } ,notes = "获取丢单商机")
    @RequestMapping(method= RequestMethod.GET , value="/opportunities/fetchlost")
	public ResponseEntity<List<OpportunityDTO>> fetchLost(OpportunitySearchContext context) {
        Page<Opportunity> domains = opportunityService.searchLost(context) ;
        List<OpportunityDTO> list = opportunityMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-searchLost-all') and hasPermission(#context,'iBizBusinessCentral-Opportunity-Get')")
	@ApiOperation(value = "查询丢单商机", tags = {"商机" } ,notes = "查询丢单商机")
    @RequestMapping(method= RequestMethod.POST , value="/opportunities/searchlost")
	public ResponseEntity<Page<OpportunityDTO>> searchLost(@RequestBody OpportunitySearchContext context) {
        Page<Opportunity> domains = opportunityService.searchLost(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(opportunityMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-searchTop5-all') and hasPermission(#context,'iBizBusinessCentral-Opportunity-Get')")
	@ApiOperation(value = "获取Top5", tags = {"商机" } ,notes = "获取Top5")
    @RequestMapping(method= RequestMethod.GET , value="/opportunities/fetchtop5")
	public ResponseEntity<List<OpportunityDTO>> fetchTop5(OpportunitySearchContext context) {
        Page<Opportunity> domains = opportunityService.searchTop5(context) ;
        List<OpportunityDTO> list = opportunityMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-searchTop5-all') and hasPermission(#context,'iBizBusinessCentral-Opportunity-Get')")
	@ApiOperation(value = "查询Top5", tags = {"商机" } ,notes = "查询Top5")
    @RequestMapping(method= RequestMethod.POST , value="/opportunities/searchtop5")
	public ResponseEntity<Page<OpportunityDTO>> searchTop5(@RequestBody OpportunitySearchContext context) {
        Page<Opportunity> domains = opportunityService.searchTop5(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(opportunityMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-searchWin-all') and hasPermission(#context,'iBizBusinessCentral-Opportunity-Get')")
	@ApiOperation(value = "获取赢单商机", tags = {"商机" } ,notes = "获取赢单商机")
    @RequestMapping(method= RequestMethod.GET , value="/opportunities/fetchwin")
	public ResponseEntity<List<OpportunityDTO>> fetchWin(OpportunitySearchContext context) {
        Page<Opportunity> domains = opportunityService.searchWin(context) ;
        List<OpportunityDTO> list = opportunityMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-searchWin-all') and hasPermission(#context,'iBizBusinessCentral-Opportunity-Get')")
	@ApiOperation(value = "查询赢单商机", tags = {"商机" } ,notes = "查询赢单商机")
    @RequestMapping(method= RequestMethod.POST , value="/opportunities/searchwin")
	public ResponseEntity<Page<OpportunityDTO>> searchWin(@RequestBody OpportunitySearchContext context) {
        Page<Opportunity> domains = opportunityService.searchWin(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(opportunityMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.opportunityMapping.toDomain(#opportunitydto),'iBizBusinessCentral-Opportunity-Create')")
    @ApiOperation(value = "根据客户建立商机", tags = {"商机" },  notes = "根据客户建立商机")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities")
    public ResponseEntity<OpportunityDTO> createByAccount(@PathVariable("account_id") String account_id, @RequestBody OpportunityDTO opportunitydto) {
        Opportunity domain = opportunityMapping.toDomain(opportunitydto);
        domain.setParentaccountid(account_id);
		opportunityService.create(domain);
        OpportunityDTO dto = opportunityMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.opportunityMapping.toDomain(#opportunitydtos),'iBizBusinessCentral-Opportunity-Create')")
    @ApiOperation(value = "根据客户批量建立商机", tags = {"商机" },  notes = "根据客户批量建立商机")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/batch")
    public ResponseEntity<Boolean> createBatchByAccount(@PathVariable("account_id") String account_id, @RequestBody List<OpportunityDTO> opportunitydtos) {
        List<Opportunity> domainlist=opportunityMapping.toDomain(opportunitydtos);
        for(Opportunity domain:domainlist){
            domain.setParentaccountid(account_id);
        }
        opportunityService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "opportunity" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.opportunityService.get(#opportunity_id),'iBizBusinessCentral-Opportunity-Update')")
    @ApiOperation(value = "根据客户更新商机", tags = {"商机" },  notes = "根据客户更新商机")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/opportunities/{opportunity_id}")
    public ResponseEntity<OpportunityDTO> updateByAccount(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityDTO opportunitydto) {
        Opportunity domain = opportunityMapping.toDomain(opportunitydto);
        domain.setParentaccountid(account_id);
        domain.setOpportunityid(opportunity_id);
		opportunityService.update(domain);
        OpportunityDTO dto = opportunityMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.opportunityService.getOpportunityByEntities(this.opportunityMapping.toDomain(#opportunitydtos)),'iBizBusinessCentral-Opportunity-Update')")
    @ApiOperation(value = "根据客户批量更新商机", tags = {"商机" },  notes = "根据客户批量更新商机")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/opportunities/batch")
    public ResponseEntity<Boolean> updateBatchByAccount(@PathVariable("account_id") String account_id, @RequestBody List<OpportunityDTO> opportunitydtos) {
        List<Opportunity> domainlist=opportunityMapping.toDomain(opportunitydtos);
        for(Opportunity domain:domainlist){
            domain.setParentaccountid(account_id);
        }
        opportunityService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.opportunityService.get(#opportunity_id),'iBizBusinessCentral-Opportunity-Remove')")
    @ApiOperation(value = "根据客户删除商机", tags = {"商机" },  notes = "根据客户删除商机")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/opportunities/{opportunity_id}")
    public ResponseEntity<Boolean> removeByAccount(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id) {
		return ResponseEntity.status(HttpStatus.OK).body(opportunityService.remove(opportunity_id));
    }

    @PreAuthorize("hasPermission(this.opportunityService.getOpportunityByIds(#ids),'iBizBusinessCentral-Opportunity-Remove')")
    @ApiOperation(value = "根据客户批量删除商机", tags = {"商机" },  notes = "根据客户批量删除商机")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/opportunities/batch")
    public ResponseEntity<Boolean> removeBatchByAccount(@RequestBody List<String> ids) {
        opportunityService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.opportunityMapping.toDomain(returnObject.body),'iBizBusinessCentral-Opportunity-Get')")
    @ApiOperation(value = "根据客户获取商机", tags = {"商机" },  notes = "根据客户获取商机")
	@RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/opportunities/{opportunity_id}")
    public ResponseEntity<OpportunityDTO> getByAccount(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id) {
        Opportunity domain = opportunityService.get(opportunity_id);
        OpportunityDTO dto = opportunityMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据客户获取商机草稿", tags = {"商机" },  notes = "根据客户获取商机草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/opportunities/getdraft")
    public ResponseEntity<OpportunityDTO> getDraftByAccount(@PathVariable("account_id") String account_id) {
        Opportunity domain = new Opportunity();
        domain.setParentaccountid(account_id);
        return ResponseEntity.status(HttpStatus.OK).body(opportunityMapping.toDto(opportunityService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-Active-all')")
    @ApiOperation(value = "根据客户商机", tags = {"商机" },  notes = "根据客户商机")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/active")
    public ResponseEntity<OpportunityDTO> activeByAccount(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityDTO opportunitydto) {
        Opportunity domain = opportunityMapping.toDomain(opportunitydto);
        domain.setParentaccountid(account_id);
        domain = opportunityService.active(domain) ;
        opportunitydto = opportunityMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(opportunitydto);
    }

    @ApiOperation(value = "根据客户检查商机", tags = {"商机" },  notes = "根据客户检查商机")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/checkkey")
    public ResponseEntity<Boolean> checkKeyByAccount(@PathVariable("account_id") String account_id, @RequestBody OpportunityDTO opportunitydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(opportunityService.checkKey(opportunityMapping.toDomain(opportunitydto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-Lose-all')")
    @ApiOperation(value = "根据客户商机", tags = {"商机" },  notes = "根据客户商机")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/lose")
    public ResponseEntity<OpportunityDTO> loseByAccount(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityDTO opportunitydto) {
        Opportunity domain = opportunityMapping.toDomain(opportunitydto);
        domain.setParentaccountid(account_id);
        domain = opportunityService.lose(domain) ;
        opportunitydto = opportunityMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(opportunitydto);
    }

    @PreAuthorize("hasPermission(this.opportunityMapping.toDomain(#opportunitydto),'iBizBusinessCentral-Opportunity-Save')")
    @ApiOperation(value = "根据客户保存商机", tags = {"商机" },  notes = "根据客户保存商机")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/save")
    public ResponseEntity<Boolean> saveByAccount(@PathVariable("account_id") String account_id, @RequestBody OpportunityDTO opportunitydto) {
        Opportunity domain = opportunityMapping.toDomain(opportunitydto);
        domain.setParentaccountid(account_id);
        return ResponseEntity.status(HttpStatus.OK).body(opportunityService.save(domain));
    }

    @PreAuthorize("hasPermission(this.opportunityMapping.toDomain(#opportunitydtos),'iBizBusinessCentral-Opportunity-Save')")
    @ApiOperation(value = "根据客户批量保存商机", tags = {"商机" },  notes = "根据客户批量保存商机")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/savebatch")
    public ResponseEntity<Boolean> saveBatchByAccount(@PathVariable("account_id") String account_id, @RequestBody List<OpportunityDTO> opportunitydtos) {
        List<Opportunity> domainlist=opportunityMapping.toDomain(opportunitydtos);
        for(Opportunity domain:domainlist){
             domain.setParentaccountid(account_id);
        }
        opportunityService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-Win-all')")
    @ApiOperation(value = "根据客户商机", tags = {"商机" },  notes = "根据客户商机")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/win")
    public ResponseEntity<OpportunityDTO> winByAccount(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityDTO opportunitydto) {
        Opportunity domain = opportunityMapping.toDomain(opportunitydto);
        domain.setParentaccountid(account_id);
        domain = opportunityService.win(domain) ;
        opportunitydto = opportunityMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(opportunitydto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Opportunity-Get')")
	@ApiOperation(value = "根据客户获取DEFAULT", tags = {"商机" } ,notes = "根据客户获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/opportunities/fetchdefault")
	public ResponseEntity<List<OpportunityDTO>> fetchOpportunityDefaultByAccount(@PathVariable("account_id") String account_id,OpportunitySearchContext context) {
        context.setN_parentaccountid_eq(account_id);
        Page<Opportunity> domains = opportunityService.searchDefault(context) ;
        List<OpportunityDTO> list = opportunityMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Opportunity-Get')")
	@ApiOperation(value = "根据客户查询DEFAULT", tags = {"商机" } ,notes = "根据客户查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/opportunities/searchdefault")
	public ResponseEntity<Page<OpportunityDTO>> searchOpportunityDefaultByAccount(@PathVariable("account_id") String account_id, @RequestBody OpportunitySearchContext context) {
        context.setN_parentaccountid_eq(account_id);
        Page<Opportunity> domains = opportunityService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(opportunityMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-searchLost-all') and hasPermission(#context,'iBizBusinessCentral-Opportunity-Get')")
	@ApiOperation(value = "根据客户获取丢单商机", tags = {"商机" } ,notes = "根据客户获取丢单商机")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/opportunities/fetchlost")
	public ResponseEntity<List<OpportunityDTO>> fetchOpportunityLostByAccount(@PathVariable("account_id") String account_id,OpportunitySearchContext context) {
        context.setN_parentaccountid_eq(account_id);
        Page<Opportunity> domains = opportunityService.searchLost(context) ;
        List<OpportunityDTO> list = opportunityMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-searchLost-all') and hasPermission(#context,'iBizBusinessCentral-Opportunity-Get')")
	@ApiOperation(value = "根据客户查询丢单商机", tags = {"商机" } ,notes = "根据客户查询丢单商机")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/opportunities/searchlost")
	public ResponseEntity<Page<OpportunityDTO>> searchOpportunityLostByAccount(@PathVariable("account_id") String account_id, @RequestBody OpportunitySearchContext context) {
        context.setN_parentaccountid_eq(account_id);
        Page<Opportunity> domains = opportunityService.searchLost(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(opportunityMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-searchTop5-all') and hasPermission(#context,'iBizBusinessCentral-Opportunity-Get')")
	@ApiOperation(value = "根据客户获取Top5", tags = {"商机" } ,notes = "根据客户获取Top5")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/opportunities/fetchtop5")
	public ResponseEntity<List<OpportunityDTO>> fetchOpportunityTop5ByAccount(@PathVariable("account_id") String account_id,OpportunitySearchContext context) {
        context.setN_parentaccountid_eq(account_id);
        Page<Opportunity> domains = opportunityService.searchTop5(context) ;
        List<OpportunityDTO> list = opportunityMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-searchTop5-all') and hasPermission(#context,'iBizBusinessCentral-Opportunity-Get')")
	@ApiOperation(value = "根据客户查询Top5", tags = {"商机" } ,notes = "根据客户查询Top5")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/opportunities/searchtop5")
	public ResponseEntity<Page<OpportunityDTO>> searchOpportunityTop5ByAccount(@PathVariable("account_id") String account_id, @RequestBody OpportunitySearchContext context) {
        context.setN_parentaccountid_eq(account_id);
        Page<Opportunity> domains = opportunityService.searchTop5(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(opportunityMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-searchWin-all') and hasPermission(#context,'iBizBusinessCentral-Opportunity-Get')")
	@ApiOperation(value = "根据客户获取赢单商机", tags = {"商机" } ,notes = "根据客户获取赢单商机")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/opportunities/fetchwin")
	public ResponseEntity<List<OpportunityDTO>> fetchOpportunityWinByAccount(@PathVariable("account_id") String account_id,OpportunitySearchContext context) {
        context.setN_parentaccountid_eq(account_id);
        Page<Opportunity> domains = opportunityService.searchWin(context) ;
        List<OpportunityDTO> list = opportunityMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-searchWin-all') and hasPermission(#context,'iBizBusinessCentral-Opportunity-Get')")
	@ApiOperation(value = "根据客户查询赢单商机", tags = {"商机" } ,notes = "根据客户查询赢单商机")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/opportunities/searchwin")
	public ResponseEntity<Page<OpportunityDTO>> searchOpportunityWinByAccount(@PathVariable("account_id") String account_id, @RequestBody OpportunitySearchContext context) {
        context.setN_parentaccountid_eq(account_id);
        Page<Opportunity> domains = opportunityService.searchWin(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(opportunityMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.opportunityMapping.toDomain(#opportunitydto),'iBizBusinessCentral-Opportunity-Create')")
    @ApiOperation(value = "根据联系人建立商机", tags = {"商机" },  notes = "根据联系人建立商机")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities")
    public ResponseEntity<OpportunityDTO> createByContact(@PathVariable("contact_id") String contact_id, @RequestBody OpportunityDTO opportunitydto) {
        Opportunity domain = opportunityMapping.toDomain(opportunitydto);
        domain.setParentcontactid(contact_id);
		opportunityService.create(domain);
        OpportunityDTO dto = opportunityMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.opportunityMapping.toDomain(#opportunitydtos),'iBizBusinessCentral-Opportunity-Create')")
    @ApiOperation(value = "根据联系人批量建立商机", tags = {"商机" },  notes = "根据联系人批量建立商机")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/batch")
    public ResponseEntity<Boolean> createBatchByContact(@PathVariable("contact_id") String contact_id, @RequestBody List<OpportunityDTO> opportunitydtos) {
        List<Opportunity> domainlist=opportunityMapping.toDomain(opportunitydtos);
        for(Opportunity domain:domainlist){
            domain.setParentcontactid(contact_id);
        }
        opportunityService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "opportunity" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.opportunityService.get(#opportunity_id),'iBizBusinessCentral-Opportunity-Update')")
    @ApiOperation(value = "根据联系人更新商机", tags = {"商机" },  notes = "根据联系人更新商机")
	@RequestMapping(method = RequestMethod.PUT, value = "/contacts/{contact_id}/opportunities/{opportunity_id}")
    public ResponseEntity<OpportunityDTO> updateByContact(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityDTO opportunitydto) {
        Opportunity domain = opportunityMapping.toDomain(opportunitydto);
        domain.setParentcontactid(contact_id);
        domain.setOpportunityid(opportunity_id);
		opportunityService.update(domain);
        OpportunityDTO dto = opportunityMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.opportunityService.getOpportunityByEntities(this.opportunityMapping.toDomain(#opportunitydtos)),'iBizBusinessCentral-Opportunity-Update')")
    @ApiOperation(value = "根据联系人批量更新商机", tags = {"商机" },  notes = "根据联系人批量更新商机")
	@RequestMapping(method = RequestMethod.PUT, value = "/contacts/{contact_id}/opportunities/batch")
    public ResponseEntity<Boolean> updateBatchByContact(@PathVariable("contact_id") String contact_id, @RequestBody List<OpportunityDTO> opportunitydtos) {
        List<Opportunity> domainlist=opportunityMapping.toDomain(opportunitydtos);
        for(Opportunity domain:domainlist){
            domain.setParentcontactid(contact_id);
        }
        opportunityService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.opportunityService.get(#opportunity_id),'iBizBusinessCentral-Opportunity-Remove')")
    @ApiOperation(value = "根据联系人删除商机", tags = {"商机" },  notes = "根据联系人删除商机")
	@RequestMapping(method = RequestMethod.DELETE, value = "/contacts/{contact_id}/opportunities/{opportunity_id}")
    public ResponseEntity<Boolean> removeByContact(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id) {
		return ResponseEntity.status(HttpStatus.OK).body(opportunityService.remove(opportunity_id));
    }

    @PreAuthorize("hasPermission(this.opportunityService.getOpportunityByIds(#ids),'iBizBusinessCentral-Opportunity-Remove')")
    @ApiOperation(value = "根据联系人批量删除商机", tags = {"商机" },  notes = "根据联系人批量删除商机")
	@RequestMapping(method = RequestMethod.DELETE, value = "/contacts/{contact_id}/opportunities/batch")
    public ResponseEntity<Boolean> removeBatchByContact(@RequestBody List<String> ids) {
        opportunityService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.opportunityMapping.toDomain(returnObject.body),'iBizBusinessCentral-Opportunity-Get')")
    @ApiOperation(value = "根据联系人获取商机", tags = {"商机" },  notes = "根据联系人获取商机")
	@RequestMapping(method = RequestMethod.GET, value = "/contacts/{contact_id}/opportunities/{opportunity_id}")
    public ResponseEntity<OpportunityDTO> getByContact(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id) {
        Opportunity domain = opportunityService.get(opportunity_id);
        OpportunityDTO dto = opportunityMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据联系人获取商机草稿", tags = {"商机" },  notes = "根据联系人获取商机草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/contacts/{contact_id}/opportunities/getdraft")
    public ResponseEntity<OpportunityDTO> getDraftByContact(@PathVariable("contact_id") String contact_id) {
        Opportunity domain = new Opportunity();
        domain.setParentcontactid(contact_id);
        return ResponseEntity.status(HttpStatus.OK).body(opportunityMapping.toDto(opportunityService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-Active-all')")
    @ApiOperation(value = "根据联系人商机", tags = {"商机" },  notes = "根据联系人商机")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/active")
    public ResponseEntity<OpportunityDTO> activeByContact(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityDTO opportunitydto) {
        Opportunity domain = opportunityMapping.toDomain(opportunitydto);
        domain.setParentcontactid(contact_id);
        domain = opportunityService.active(domain) ;
        opportunitydto = opportunityMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(opportunitydto);
    }

    @ApiOperation(value = "根据联系人检查商机", tags = {"商机" },  notes = "根据联系人检查商机")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/checkkey")
    public ResponseEntity<Boolean> checkKeyByContact(@PathVariable("contact_id") String contact_id, @RequestBody OpportunityDTO opportunitydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(opportunityService.checkKey(opportunityMapping.toDomain(opportunitydto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-Lose-all')")
    @ApiOperation(value = "根据联系人商机", tags = {"商机" },  notes = "根据联系人商机")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/lose")
    public ResponseEntity<OpportunityDTO> loseByContact(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityDTO opportunitydto) {
        Opportunity domain = opportunityMapping.toDomain(opportunitydto);
        domain.setParentcontactid(contact_id);
        domain = opportunityService.lose(domain) ;
        opportunitydto = opportunityMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(opportunitydto);
    }

    @PreAuthorize("hasPermission(this.opportunityMapping.toDomain(#opportunitydto),'iBizBusinessCentral-Opportunity-Save')")
    @ApiOperation(value = "根据联系人保存商机", tags = {"商机" },  notes = "根据联系人保存商机")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/save")
    public ResponseEntity<Boolean> saveByContact(@PathVariable("contact_id") String contact_id, @RequestBody OpportunityDTO opportunitydto) {
        Opportunity domain = opportunityMapping.toDomain(opportunitydto);
        domain.setParentcontactid(contact_id);
        return ResponseEntity.status(HttpStatus.OK).body(opportunityService.save(domain));
    }

    @PreAuthorize("hasPermission(this.opportunityMapping.toDomain(#opportunitydtos),'iBizBusinessCentral-Opportunity-Save')")
    @ApiOperation(value = "根据联系人批量保存商机", tags = {"商机" },  notes = "根据联系人批量保存商机")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/savebatch")
    public ResponseEntity<Boolean> saveBatchByContact(@PathVariable("contact_id") String contact_id, @RequestBody List<OpportunityDTO> opportunitydtos) {
        List<Opportunity> domainlist=opportunityMapping.toDomain(opportunitydtos);
        for(Opportunity domain:domainlist){
             domain.setParentcontactid(contact_id);
        }
        opportunityService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-Win-all')")
    @ApiOperation(value = "根据联系人商机", tags = {"商机" },  notes = "根据联系人商机")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/win")
    public ResponseEntity<OpportunityDTO> winByContact(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityDTO opportunitydto) {
        Opportunity domain = opportunityMapping.toDomain(opportunitydto);
        domain.setParentcontactid(contact_id);
        domain = opportunityService.win(domain) ;
        opportunitydto = opportunityMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(opportunitydto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Opportunity-Get')")
	@ApiOperation(value = "根据联系人获取DEFAULT", tags = {"商机" } ,notes = "根据联系人获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/contacts/{contact_id}/opportunities/fetchdefault")
	public ResponseEntity<List<OpportunityDTO>> fetchOpportunityDefaultByContact(@PathVariable("contact_id") String contact_id,OpportunitySearchContext context) {
        context.setN_parentcontactid_eq(contact_id);
        Page<Opportunity> domains = opportunityService.searchDefault(context) ;
        List<OpportunityDTO> list = opportunityMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Opportunity-Get')")
	@ApiOperation(value = "根据联系人查询DEFAULT", tags = {"商机" } ,notes = "根据联系人查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/contacts/{contact_id}/opportunities/searchdefault")
	public ResponseEntity<Page<OpportunityDTO>> searchOpportunityDefaultByContact(@PathVariable("contact_id") String contact_id, @RequestBody OpportunitySearchContext context) {
        context.setN_parentcontactid_eq(contact_id);
        Page<Opportunity> domains = opportunityService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(opportunityMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-searchLost-all') and hasPermission(#context,'iBizBusinessCentral-Opportunity-Get')")
	@ApiOperation(value = "根据联系人获取丢单商机", tags = {"商机" } ,notes = "根据联系人获取丢单商机")
    @RequestMapping(method= RequestMethod.GET , value="/contacts/{contact_id}/opportunities/fetchlost")
	public ResponseEntity<List<OpportunityDTO>> fetchOpportunityLostByContact(@PathVariable("contact_id") String contact_id,OpportunitySearchContext context) {
        context.setN_parentcontactid_eq(contact_id);
        Page<Opportunity> domains = opportunityService.searchLost(context) ;
        List<OpportunityDTO> list = opportunityMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-searchLost-all') and hasPermission(#context,'iBizBusinessCentral-Opportunity-Get')")
	@ApiOperation(value = "根据联系人查询丢单商机", tags = {"商机" } ,notes = "根据联系人查询丢单商机")
    @RequestMapping(method= RequestMethod.POST , value="/contacts/{contact_id}/opportunities/searchlost")
	public ResponseEntity<Page<OpportunityDTO>> searchOpportunityLostByContact(@PathVariable("contact_id") String contact_id, @RequestBody OpportunitySearchContext context) {
        context.setN_parentcontactid_eq(contact_id);
        Page<Opportunity> domains = opportunityService.searchLost(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(opportunityMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-searchTop5-all') and hasPermission(#context,'iBizBusinessCentral-Opportunity-Get')")
	@ApiOperation(value = "根据联系人获取Top5", tags = {"商机" } ,notes = "根据联系人获取Top5")
    @RequestMapping(method= RequestMethod.GET , value="/contacts/{contact_id}/opportunities/fetchtop5")
	public ResponseEntity<List<OpportunityDTO>> fetchOpportunityTop5ByContact(@PathVariable("contact_id") String contact_id,OpportunitySearchContext context) {
        context.setN_parentcontactid_eq(contact_id);
        Page<Opportunity> domains = opportunityService.searchTop5(context) ;
        List<OpportunityDTO> list = opportunityMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-searchTop5-all') and hasPermission(#context,'iBizBusinessCentral-Opportunity-Get')")
	@ApiOperation(value = "根据联系人查询Top5", tags = {"商机" } ,notes = "根据联系人查询Top5")
    @RequestMapping(method= RequestMethod.POST , value="/contacts/{contact_id}/opportunities/searchtop5")
	public ResponseEntity<Page<OpportunityDTO>> searchOpportunityTop5ByContact(@PathVariable("contact_id") String contact_id, @RequestBody OpportunitySearchContext context) {
        context.setN_parentcontactid_eq(contact_id);
        Page<Opportunity> domains = opportunityService.searchTop5(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(opportunityMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-searchWin-all') and hasPermission(#context,'iBizBusinessCentral-Opportunity-Get')")
	@ApiOperation(value = "根据联系人获取赢单商机", tags = {"商机" } ,notes = "根据联系人获取赢单商机")
    @RequestMapping(method= RequestMethod.GET , value="/contacts/{contact_id}/opportunities/fetchwin")
	public ResponseEntity<List<OpportunityDTO>> fetchOpportunityWinByContact(@PathVariable("contact_id") String contact_id,OpportunitySearchContext context) {
        context.setN_parentcontactid_eq(contact_id);
        Page<Opportunity> domains = opportunityService.searchWin(context) ;
        List<OpportunityDTO> list = opportunityMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-searchWin-all') and hasPermission(#context,'iBizBusinessCentral-Opportunity-Get')")
	@ApiOperation(value = "根据联系人查询赢单商机", tags = {"商机" } ,notes = "根据联系人查询赢单商机")
    @RequestMapping(method= RequestMethod.POST , value="/contacts/{contact_id}/opportunities/searchwin")
	public ResponseEntity<Page<OpportunityDTO>> searchOpportunityWinByContact(@PathVariable("contact_id") String contact_id, @RequestBody OpportunitySearchContext context) {
        context.setN_parentcontactid_eq(contact_id);
        Page<Opportunity> domains = opportunityService.searchWin(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(opportunityMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.opportunityMapping.toDomain(#opportunitydto),'iBizBusinessCentral-Opportunity-Create')")
    @ApiOperation(value = "根据客户联系人建立商机", tags = {"商机" },  notes = "根据客户联系人建立商机")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities")
    public ResponseEntity<OpportunityDTO> createByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @RequestBody OpportunityDTO opportunitydto) {
        Opportunity domain = opportunityMapping.toDomain(opportunitydto);
        domain.setParentcontactid(contact_id);
		opportunityService.create(domain);
        OpportunityDTO dto = opportunityMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.opportunityMapping.toDomain(#opportunitydtos),'iBizBusinessCentral-Opportunity-Create')")
    @ApiOperation(value = "根据客户联系人批量建立商机", tags = {"商机" },  notes = "根据客户联系人批量建立商机")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/batch")
    public ResponseEntity<Boolean> createBatchByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @RequestBody List<OpportunityDTO> opportunitydtos) {
        List<Opportunity> domainlist=opportunityMapping.toDomain(opportunitydtos);
        for(Opportunity domain:domainlist){
            domain.setParentcontactid(contact_id);
        }
        opportunityService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "opportunity" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.opportunityService.get(#opportunity_id),'iBizBusinessCentral-Opportunity-Update')")
    @ApiOperation(value = "根据客户联系人更新商机", tags = {"商机" },  notes = "根据客户联系人更新商机")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}")
    public ResponseEntity<OpportunityDTO> updateByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityDTO opportunitydto) {
        Opportunity domain = opportunityMapping.toDomain(opportunitydto);
        domain.setParentcontactid(contact_id);
        domain.setOpportunityid(opportunity_id);
		opportunityService.update(domain);
        OpportunityDTO dto = opportunityMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.opportunityService.getOpportunityByEntities(this.opportunityMapping.toDomain(#opportunitydtos)),'iBizBusinessCentral-Opportunity-Update')")
    @ApiOperation(value = "根据客户联系人批量更新商机", tags = {"商机" },  notes = "根据客户联系人批量更新商机")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/batch")
    public ResponseEntity<Boolean> updateBatchByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @RequestBody List<OpportunityDTO> opportunitydtos) {
        List<Opportunity> domainlist=opportunityMapping.toDomain(opportunitydtos);
        for(Opportunity domain:domainlist){
            domain.setParentcontactid(contact_id);
        }
        opportunityService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.opportunityService.get(#opportunity_id),'iBizBusinessCentral-Opportunity-Remove')")
    @ApiOperation(value = "根据客户联系人删除商机", tags = {"商机" },  notes = "根据客户联系人删除商机")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}")
    public ResponseEntity<Boolean> removeByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id) {
		return ResponseEntity.status(HttpStatus.OK).body(opportunityService.remove(opportunity_id));
    }

    @PreAuthorize("hasPermission(this.opportunityService.getOpportunityByIds(#ids),'iBizBusinessCentral-Opportunity-Remove')")
    @ApiOperation(value = "根据客户联系人批量删除商机", tags = {"商机" },  notes = "根据客户联系人批量删除商机")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/batch")
    public ResponseEntity<Boolean> removeBatchByAccountContact(@RequestBody List<String> ids) {
        opportunityService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.opportunityMapping.toDomain(returnObject.body),'iBizBusinessCentral-Opportunity-Get')")
    @ApiOperation(value = "根据客户联系人获取商机", tags = {"商机" },  notes = "根据客户联系人获取商机")
	@RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}")
    public ResponseEntity<OpportunityDTO> getByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id) {
        Opportunity domain = opportunityService.get(opportunity_id);
        OpportunityDTO dto = opportunityMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据客户联系人获取商机草稿", tags = {"商机" },  notes = "根据客户联系人获取商机草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/getdraft")
    public ResponseEntity<OpportunityDTO> getDraftByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id) {
        Opportunity domain = new Opportunity();
        domain.setParentcontactid(contact_id);
        return ResponseEntity.status(HttpStatus.OK).body(opportunityMapping.toDto(opportunityService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-Active-all')")
    @ApiOperation(value = "根据客户联系人商机", tags = {"商机" },  notes = "根据客户联系人商机")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/active")
    public ResponseEntity<OpportunityDTO> activeByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityDTO opportunitydto) {
        Opportunity domain = opportunityMapping.toDomain(opportunitydto);
        domain.setParentcontactid(contact_id);
        domain = opportunityService.active(domain) ;
        opportunitydto = opportunityMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(opportunitydto);
    }

    @ApiOperation(value = "根据客户联系人检查商机", tags = {"商机" },  notes = "根据客户联系人检查商机")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/checkkey")
    public ResponseEntity<Boolean> checkKeyByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @RequestBody OpportunityDTO opportunitydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(opportunityService.checkKey(opportunityMapping.toDomain(opportunitydto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-Lose-all')")
    @ApiOperation(value = "根据客户联系人商机", tags = {"商机" },  notes = "根据客户联系人商机")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/lose")
    public ResponseEntity<OpportunityDTO> loseByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityDTO opportunitydto) {
        Opportunity domain = opportunityMapping.toDomain(opportunitydto);
        domain.setParentcontactid(contact_id);
        domain = opportunityService.lose(domain) ;
        opportunitydto = opportunityMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(opportunitydto);
    }

    @PreAuthorize("hasPermission(this.opportunityMapping.toDomain(#opportunitydto),'iBizBusinessCentral-Opportunity-Save')")
    @ApiOperation(value = "根据客户联系人保存商机", tags = {"商机" },  notes = "根据客户联系人保存商机")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/save")
    public ResponseEntity<Boolean> saveByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @RequestBody OpportunityDTO opportunitydto) {
        Opportunity domain = opportunityMapping.toDomain(opportunitydto);
        domain.setParentcontactid(contact_id);
        return ResponseEntity.status(HttpStatus.OK).body(opportunityService.save(domain));
    }

    @PreAuthorize("hasPermission(this.opportunityMapping.toDomain(#opportunitydtos),'iBizBusinessCentral-Opportunity-Save')")
    @ApiOperation(value = "根据客户联系人批量保存商机", tags = {"商机" },  notes = "根据客户联系人批量保存商机")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/savebatch")
    public ResponseEntity<Boolean> saveBatchByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @RequestBody List<OpportunityDTO> opportunitydtos) {
        List<Opportunity> domainlist=opportunityMapping.toDomain(opportunitydtos);
        for(Opportunity domain:domainlist){
             domain.setParentcontactid(contact_id);
        }
        opportunityService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-Win-all')")
    @ApiOperation(value = "根据客户联系人商机", tags = {"商机" },  notes = "根据客户联系人商机")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/win")
    public ResponseEntity<OpportunityDTO> winByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityDTO opportunitydto) {
        Opportunity domain = opportunityMapping.toDomain(opportunitydto);
        domain.setParentcontactid(contact_id);
        domain = opportunityService.win(domain) ;
        opportunitydto = opportunityMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(opportunitydto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Opportunity-Get')")
	@ApiOperation(value = "根据客户联系人获取DEFAULT", tags = {"商机" } ,notes = "根据客户联系人获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/fetchdefault")
	public ResponseEntity<List<OpportunityDTO>> fetchOpportunityDefaultByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id,OpportunitySearchContext context) {
        context.setN_parentcontactid_eq(contact_id);
        Page<Opportunity> domains = opportunityService.searchDefault(context) ;
        List<OpportunityDTO> list = opportunityMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Opportunity-Get')")
	@ApiOperation(value = "根据客户联系人查询DEFAULT", tags = {"商机" } ,notes = "根据客户联系人查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/searchdefault")
	public ResponseEntity<Page<OpportunityDTO>> searchOpportunityDefaultByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @RequestBody OpportunitySearchContext context) {
        context.setN_parentcontactid_eq(contact_id);
        Page<Opportunity> domains = opportunityService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(opportunityMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-searchLost-all') and hasPermission(#context,'iBizBusinessCentral-Opportunity-Get')")
	@ApiOperation(value = "根据客户联系人获取丢单商机", tags = {"商机" } ,notes = "根据客户联系人获取丢单商机")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/fetchlost")
	public ResponseEntity<List<OpportunityDTO>> fetchOpportunityLostByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id,OpportunitySearchContext context) {
        context.setN_parentcontactid_eq(contact_id);
        Page<Opportunity> domains = opportunityService.searchLost(context) ;
        List<OpportunityDTO> list = opportunityMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-searchLost-all') and hasPermission(#context,'iBizBusinessCentral-Opportunity-Get')")
	@ApiOperation(value = "根据客户联系人查询丢单商机", tags = {"商机" } ,notes = "根据客户联系人查询丢单商机")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/searchlost")
	public ResponseEntity<Page<OpportunityDTO>> searchOpportunityLostByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @RequestBody OpportunitySearchContext context) {
        context.setN_parentcontactid_eq(contact_id);
        Page<Opportunity> domains = opportunityService.searchLost(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(opportunityMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-searchTop5-all') and hasPermission(#context,'iBizBusinessCentral-Opportunity-Get')")
	@ApiOperation(value = "根据客户联系人获取Top5", tags = {"商机" } ,notes = "根据客户联系人获取Top5")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/fetchtop5")
	public ResponseEntity<List<OpportunityDTO>> fetchOpportunityTop5ByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id,OpportunitySearchContext context) {
        context.setN_parentcontactid_eq(contact_id);
        Page<Opportunity> domains = opportunityService.searchTop5(context) ;
        List<OpportunityDTO> list = opportunityMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-searchTop5-all') and hasPermission(#context,'iBizBusinessCentral-Opportunity-Get')")
	@ApiOperation(value = "根据客户联系人查询Top5", tags = {"商机" } ,notes = "根据客户联系人查询Top5")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/searchtop5")
	public ResponseEntity<Page<OpportunityDTO>> searchOpportunityTop5ByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @RequestBody OpportunitySearchContext context) {
        context.setN_parentcontactid_eq(contact_id);
        Page<Opportunity> domains = opportunityService.searchTop5(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(opportunityMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-searchWin-all') and hasPermission(#context,'iBizBusinessCentral-Opportunity-Get')")
	@ApiOperation(value = "根据客户联系人获取赢单商机", tags = {"商机" } ,notes = "根据客户联系人获取赢单商机")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/fetchwin")
	public ResponseEntity<List<OpportunityDTO>> fetchOpportunityWinByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id,OpportunitySearchContext context) {
        context.setN_parentcontactid_eq(contact_id);
        Page<Opportunity> domains = opportunityService.searchWin(context) ;
        List<OpportunityDTO> list = opportunityMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Opportunity-searchWin-all') and hasPermission(#context,'iBizBusinessCentral-Opportunity-Get')")
	@ApiOperation(value = "根据客户联系人查询赢单商机", tags = {"商机" } ,notes = "根据客户联系人查询赢单商机")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/searchwin")
	public ResponseEntity<Page<OpportunityDTO>> searchOpportunityWinByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @RequestBody OpportunitySearchContext context) {
        context.setN_parentcontactid_eq(contact_id);
        Page<Opportunity> domains = opportunityService.searchWin(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(opportunityMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}


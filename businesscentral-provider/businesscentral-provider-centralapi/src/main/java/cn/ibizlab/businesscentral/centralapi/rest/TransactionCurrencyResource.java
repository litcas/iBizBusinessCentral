package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency;
import cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService;
import cn.ibizlab.businesscentral.core.base.filter.TransactionCurrencySearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"货币" })
@RestController("CentralApi-transactioncurrency")
@RequestMapping("")
public class TransactionCurrencyResource {

    @Autowired
    public ITransactionCurrencyService transactioncurrencyService;

    @Autowired
    @Lazy
    public TransactionCurrencyMapping transactioncurrencyMapping;

    @PreAuthorize("hasPermission(this.transactioncurrencyMapping.toDomain(#transactioncurrencydto),'iBizBusinessCentral-TransactionCurrency-Create')")
    @ApiOperation(value = "新建货币", tags = {"货币" },  notes = "新建货币")
	@RequestMapping(method = RequestMethod.POST, value = "/transactioncurrencies")
    public ResponseEntity<TransactionCurrencyDTO> create(@RequestBody TransactionCurrencyDTO transactioncurrencydto) {
        TransactionCurrency domain = transactioncurrencyMapping.toDomain(transactioncurrencydto);
		transactioncurrencyService.create(domain);
        TransactionCurrencyDTO dto = transactioncurrencyMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.transactioncurrencyMapping.toDomain(#transactioncurrencydtos),'iBizBusinessCentral-TransactionCurrency-Create')")
    @ApiOperation(value = "批量新建货币", tags = {"货币" },  notes = "批量新建货币")
	@RequestMapping(method = RequestMethod.POST, value = "/transactioncurrencies/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<TransactionCurrencyDTO> transactioncurrencydtos) {
        transactioncurrencyService.createBatch(transactioncurrencyMapping.toDomain(transactioncurrencydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "transactioncurrency" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.transactioncurrencyService.get(#transactioncurrency_id),'iBizBusinessCentral-TransactionCurrency-Update')")
    @ApiOperation(value = "更新货币", tags = {"货币" },  notes = "更新货币")
	@RequestMapping(method = RequestMethod.PUT, value = "/transactioncurrencies/{transactioncurrency_id}")
    public ResponseEntity<TransactionCurrencyDTO> update(@PathVariable("transactioncurrency_id") String transactioncurrency_id, @RequestBody TransactionCurrencyDTO transactioncurrencydto) {
		TransactionCurrency domain  = transactioncurrencyMapping.toDomain(transactioncurrencydto);
        domain .setTransactioncurrencyid(transactioncurrency_id);
		transactioncurrencyService.update(domain );
		TransactionCurrencyDTO dto = transactioncurrencyMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.transactioncurrencyService.getTransactioncurrencyByEntities(this.transactioncurrencyMapping.toDomain(#transactioncurrencydtos)),'iBizBusinessCentral-TransactionCurrency-Update')")
    @ApiOperation(value = "批量更新货币", tags = {"货币" },  notes = "批量更新货币")
	@RequestMapping(method = RequestMethod.PUT, value = "/transactioncurrencies/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<TransactionCurrencyDTO> transactioncurrencydtos) {
        transactioncurrencyService.updateBatch(transactioncurrencyMapping.toDomain(transactioncurrencydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.transactioncurrencyService.get(#transactioncurrency_id),'iBizBusinessCentral-TransactionCurrency-Remove')")
    @ApiOperation(value = "删除货币", tags = {"货币" },  notes = "删除货币")
	@RequestMapping(method = RequestMethod.DELETE, value = "/transactioncurrencies/{transactioncurrency_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("transactioncurrency_id") String transactioncurrency_id) {
         return ResponseEntity.status(HttpStatus.OK).body(transactioncurrencyService.remove(transactioncurrency_id));
    }

    @PreAuthorize("hasPermission(this.transactioncurrencyService.getTransactioncurrencyByIds(#ids),'iBizBusinessCentral-TransactionCurrency-Remove')")
    @ApiOperation(value = "批量删除货币", tags = {"货币" },  notes = "批量删除货币")
	@RequestMapping(method = RequestMethod.DELETE, value = "/transactioncurrencies/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        transactioncurrencyService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.transactioncurrencyMapping.toDomain(returnObject.body),'iBizBusinessCentral-TransactionCurrency-Get')")
    @ApiOperation(value = "获取货币", tags = {"货币" },  notes = "获取货币")
	@RequestMapping(method = RequestMethod.GET, value = "/transactioncurrencies/{transactioncurrency_id}")
    public ResponseEntity<TransactionCurrencyDTO> get(@PathVariable("transactioncurrency_id") String transactioncurrency_id) {
        TransactionCurrency domain = transactioncurrencyService.get(transactioncurrency_id);
        TransactionCurrencyDTO dto = transactioncurrencyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取货币草稿", tags = {"货币" },  notes = "获取货币草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/transactioncurrencies/getdraft")
    public ResponseEntity<TransactionCurrencyDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(transactioncurrencyMapping.toDto(transactioncurrencyService.getDraft(new TransactionCurrency())));
    }

    @ApiOperation(value = "检查货币", tags = {"货币" },  notes = "检查货币")
	@RequestMapping(method = RequestMethod.POST, value = "/transactioncurrencies/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody TransactionCurrencyDTO transactioncurrencydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(transactioncurrencyService.checkKey(transactioncurrencyMapping.toDomain(transactioncurrencydto)));
    }

    @PreAuthorize("hasPermission(this.transactioncurrencyMapping.toDomain(#transactioncurrencydto),'iBizBusinessCentral-TransactionCurrency-Save')")
    @ApiOperation(value = "保存货币", tags = {"货币" },  notes = "保存货币")
	@RequestMapping(method = RequestMethod.POST, value = "/transactioncurrencies/save")
    public ResponseEntity<Boolean> save(@RequestBody TransactionCurrencyDTO transactioncurrencydto) {
        return ResponseEntity.status(HttpStatus.OK).body(transactioncurrencyService.save(transactioncurrencyMapping.toDomain(transactioncurrencydto)));
    }

    @PreAuthorize("hasPermission(this.transactioncurrencyMapping.toDomain(#transactioncurrencydtos),'iBizBusinessCentral-TransactionCurrency-Save')")
    @ApiOperation(value = "批量保存货币", tags = {"货币" },  notes = "批量保存货币")
	@RequestMapping(method = RequestMethod.POST, value = "/transactioncurrencies/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<TransactionCurrencyDTO> transactioncurrencydtos) {
        transactioncurrencyService.saveBatch(transactioncurrencyMapping.toDomain(transactioncurrencydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-TransactionCurrency-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-TransactionCurrency-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"货币" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/transactioncurrencies/fetchdefault")
	public ResponseEntity<List<TransactionCurrencyDTO>> fetchDefault(TransactionCurrencySearchContext context) {
        Page<TransactionCurrency> domains = transactioncurrencyService.searchDefault(context) ;
        List<TransactionCurrencyDTO> list = transactioncurrencyMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-TransactionCurrency-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-TransactionCurrency-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"货币" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/transactioncurrencies/searchdefault")
	public ResponseEntity<Page<TransactionCurrencyDTO>> searchDefault(@RequestBody TransactionCurrencySearchContext context) {
        Page<TransactionCurrency> domains = transactioncurrencyService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(transactioncurrencyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}


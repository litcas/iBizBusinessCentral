package cn.ibizlab.businesscentral.centralapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.sales.domain.SalesLiterature;
import cn.ibizlab.businesscentral.centralapi.dto.SalesLiteratureDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CentralApiSalesLiteratureMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface SalesLiteratureMapping extends MappingBase<SalesLiteratureDTO, SalesLiterature> {


}


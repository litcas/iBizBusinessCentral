package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.service.domain.ServiceAppointment;
import cn.ibizlab.businesscentral.core.service.service.IServiceAppointmentService;
import cn.ibizlab.businesscentral.core.service.filter.ServiceAppointmentSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"服务活动" })
@RestController("CentralApi-serviceappointment")
@RequestMapping("")
public class ServiceAppointmentResource {

    @Autowired
    public IServiceAppointmentService serviceappointmentService;

    @Autowired
    @Lazy
    public ServiceAppointmentMapping serviceappointmentMapping;

    @PreAuthorize("hasPermission(this.serviceappointmentMapping.toDomain(#serviceappointmentdto),'iBizBusinessCentral-ServiceAppointment-Create')")
    @ApiOperation(value = "新建服务活动", tags = {"服务活动" },  notes = "新建服务活动")
	@RequestMapping(method = RequestMethod.POST, value = "/serviceappointments")
    public ResponseEntity<ServiceAppointmentDTO> create(@RequestBody ServiceAppointmentDTO serviceappointmentdto) {
        ServiceAppointment domain = serviceappointmentMapping.toDomain(serviceappointmentdto);
		serviceappointmentService.create(domain);
        ServiceAppointmentDTO dto = serviceappointmentMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.serviceappointmentMapping.toDomain(#serviceappointmentdtos),'iBizBusinessCentral-ServiceAppointment-Create')")
    @ApiOperation(value = "批量新建服务活动", tags = {"服务活动" },  notes = "批量新建服务活动")
	@RequestMapping(method = RequestMethod.POST, value = "/serviceappointments/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<ServiceAppointmentDTO> serviceappointmentdtos) {
        serviceappointmentService.createBatch(serviceappointmentMapping.toDomain(serviceappointmentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "serviceappointment" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.serviceappointmentService.get(#serviceappointment_id),'iBizBusinessCentral-ServiceAppointment-Update')")
    @ApiOperation(value = "更新服务活动", tags = {"服务活动" },  notes = "更新服务活动")
	@RequestMapping(method = RequestMethod.PUT, value = "/serviceappointments/{serviceappointment_id}")
    public ResponseEntity<ServiceAppointmentDTO> update(@PathVariable("serviceappointment_id") String serviceappointment_id, @RequestBody ServiceAppointmentDTO serviceappointmentdto) {
		ServiceAppointment domain  = serviceappointmentMapping.toDomain(serviceappointmentdto);
        domain .setActivityid(serviceappointment_id);
		serviceappointmentService.update(domain );
		ServiceAppointmentDTO dto = serviceappointmentMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.serviceappointmentService.getServiceappointmentByEntities(this.serviceappointmentMapping.toDomain(#serviceappointmentdtos)),'iBizBusinessCentral-ServiceAppointment-Update')")
    @ApiOperation(value = "批量更新服务活动", tags = {"服务活动" },  notes = "批量更新服务活动")
	@RequestMapping(method = RequestMethod.PUT, value = "/serviceappointments/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<ServiceAppointmentDTO> serviceappointmentdtos) {
        serviceappointmentService.updateBatch(serviceappointmentMapping.toDomain(serviceappointmentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.serviceappointmentService.get(#serviceappointment_id),'iBizBusinessCentral-ServiceAppointment-Remove')")
    @ApiOperation(value = "删除服务活动", tags = {"服务活动" },  notes = "删除服务活动")
	@RequestMapping(method = RequestMethod.DELETE, value = "/serviceappointments/{serviceappointment_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("serviceappointment_id") String serviceappointment_id) {
         return ResponseEntity.status(HttpStatus.OK).body(serviceappointmentService.remove(serviceappointment_id));
    }

    @PreAuthorize("hasPermission(this.serviceappointmentService.getServiceappointmentByIds(#ids),'iBizBusinessCentral-ServiceAppointment-Remove')")
    @ApiOperation(value = "批量删除服务活动", tags = {"服务活动" },  notes = "批量删除服务活动")
	@RequestMapping(method = RequestMethod.DELETE, value = "/serviceappointments/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        serviceappointmentService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.serviceappointmentMapping.toDomain(returnObject.body),'iBizBusinessCentral-ServiceAppointment-Get')")
    @ApiOperation(value = "获取服务活动", tags = {"服务活动" },  notes = "获取服务活动")
	@RequestMapping(method = RequestMethod.GET, value = "/serviceappointments/{serviceappointment_id}")
    public ResponseEntity<ServiceAppointmentDTO> get(@PathVariable("serviceappointment_id") String serviceappointment_id) {
        ServiceAppointment domain = serviceappointmentService.get(serviceappointment_id);
        ServiceAppointmentDTO dto = serviceappointmentMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取服务活动草稿", tags = {"服务活动" },  notes = "获取服务活动草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/serviceappointments/getdraft")
    public ResponseEntity<ServiceAppointmentDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(serviceappointmentMapping.toDto(serviceappointmentService.getDraft(new ServiceAppointment())));
    }

    @ApiOperation(value = "检查服务活动", tags = {"服务活动" },  notes = "检查服务活动")
	@RequestMapping(method = RequestMethod.POST, value = "/serviceappointments/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody ServiceAppointmentDTO serviceappointmentdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(serviceappointmentService.checkKey(serviceappointmentMapping.toDomain(serviceappointmentdto)));
    }

    @PreAuthorize("hasPermission(this.serviceappointmentMapping.toDomain(#serviceappointmentdto),'iBizBusinessCentral-ServiceAppointment-Save')")
    @ApiOperation(value = "保存服务活动", tags = {"服务活动" },  notes = "保存服务活动")
	@RequestMapping(method = RequestMethod.POST, value = "/serviceappointments/save")
    public ResponseEntity<Boolean> save(@RequestBody ServiceAppointmentDTO serviceappointmentdto) {
        return ResponseEntity.status(HttpStatus.OK).body(serviceappointmentService.save(serviceappointmentMapping.toDomain(serviceappointmentdto)));
    }

    @PreAuthorize("hasPermission(this.serviceappointmentMapping.toDomain(#serviceappointmentdtos),'iBizBusinessCentral-ServiceAppointment-Save')")
    @ApiOperation(value = "批量保存服务活动", tags = {"服务活动" },  notes = "批量保存服务活动")
	@RequestMapping(method = RequestMethod.POST, value = "/serviceappointments/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<ServiceAppointmentDTO> serviceappointmentdtos) {
        serviceappointmentService.saveBatch(serviceappointmentMapping.toDomain(serviceappointmentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ServiceAppointment-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ServiceAppointment-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"服务活动" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/serviceappointments/fetchdefault")
	public ResponseEntity<List<ServiceAppointmentDTO>> fetchDefault(ServiceAppointmentSearchContext context) {
        Page<ServiceAppointment> domains = serviceappointmentService.searchDefault(context) ;
        List<ServiceAppointmentDTO> list = serviceappointmentMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ServiceAppointment-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ServiceAppointment-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"服务活动" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/serviceappointments/searchdefault")
	public ResponseEntity<Page<ServiceAppointmentDTO>> searchDefault(@RequestBody ServiceAppointmentSearchContext context) {
        Page<ServiceAppointment> domains = serviceappointmentService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(serviceappointmentMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}


package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.base.domain.PhoneCall;
import cn.ibizlab.businesscentral.core.base.service.IPhoneCallService;
import cn.ibizlab.businesscentral.core.base.filter.PhoneCallSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"电话联络" })
@RestController("CentralApi-phonecall")
@RequestMapping("")
public class PhoneCallResource {

    @Autowired
    public IPhoneCallService phonecallService;

    @Autowired
    @Lazy
    public PhoneCallMapping phonecallMapping;

    @PreAuthorize("hasPermission(this.phonecallMapping.toDomain(#phonecalldto),'iBizBusinessCentral-PhoneCall-Create')")
    @ApiOperation(value = "新建电话联络", tags = {"电话联络" },  notes = "新建电话联络")
	@RequestMapping(method = RequestMethod.POST, value = "/phonecalls")
    public ResponseEntity<PhoneCallDTO> create(@RequestBody PhoneCallDTO phonecalldto) {
        PhoneCall domain = phonecallMapping.toDomain(phonecalldto);
		phonecallService.create(domain);
        PhoneCallDTO dto = phonecallMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.phonecallMapping.toDomain(#phonecalldtos),'iBizBusinessCentral-PhoneCall-Create')")
    @ApiOperation(value = "批量新建电话联络", tags = {"电话联络" },  notes = "批量新建电话联络")
	@RequestMapping(method = RequestMethod.POST, value = "/phonecalls/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<PhoneCallDTO> phonecalldtos) {
        phonecallService.createBatch(phonecallMapping.toDomain(phonecalldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "phonecall" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.phonecallService.get(#phonecall_id),'iBizBusinessCentral-PhoneCall-Update')")
    @ApiOperation(value = "更新电话联络", tags = {"电话联络" },  notes = "更新电话联络")
	@RequestMapping(method = RequestMethod.PUT, value = "/phonecalls/{phonecall_id}")
    public ResponseEntity<PhoneCallDTO> update(@PathVariable("phonecall_id") String phonecall_id, @RequestBody PhoneCallDTO phonecalldto) {
		PhoneCall domain  = phonecallMapping.toDomain(phonecalldto);
        domain .setActivityid(phonecall_id);
		phonecallService.update(domain );
		PhoneCallDTO dto = phonecallMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.phonecallService.getPhonecallByEntities(this.phonecallMapping.toDomain(#phonecalldtos)),'iBizBusinessCentral-PhoneCall-Update')")
    @ApiOperation(value = "批量更新电话联络", tags = {"电话联络" },  notes = "批量更新电话联络")
	@RequestMapping(method = RequestMethod.PUT, value = "/phonecalls/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<PhoneCallDTO> phonecalldtos) {
        phonecallService.updateBatch(phonecallMapping.toDomain(phonecalldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.phonecallService.get(#phonecall_id),'iBizBusinessCentral-PhoneCall-Remove')")
    @ApiOperation(value = "删除电话联络", tags = {"电话联络" },  notes = "删除电话联络")
	@RequestMapping(method = RequestMethod.DELETE, value = "/phonecalls/{phonecall_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("phonecall_id") String phonecall_id) {
         return ResponseEntity.status(HttpStatus.OK).body(phonecallService.remove(phonecall_id));
    }

    @PreAuthorize("hasPermission(this.phonecallService.getPhonecallByIds(#ids),'iBizBusinessCentral-PhoneCall-Remove')")
    @ApiOperation(value = "批量删除电话联络", tags = {"电话联络" },  notes = "批量删除电话联络")
	@RequestMapping(method = RequestMethod.DELETE, value = "/phonecalls/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        phonecallService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.phonecallMapping.toDomain(returnObject.body),'iBizBusinessCentral-PhoneCall-Get')")
    @ApiOperation(value = "获取电话联络", tags = {"电话联络" },  notes = "获取电话联络")
	@RequestMapping(method = RequestMethod.GET, value = "/phonecalls/{phonecall_id}")
    public ResponseEntity<PhoneCallDTO> get(@PathVariable("phonecall_id") String phonecall_id) {
        PhoneCall domain = phonecallService.get(phonecall_id);
        PhoneCallDTO dto = phonecallMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取电话联络草稿", tags = {"电话联络" },  notes = "获取电话联络草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/phonecalls/getdraft")
    public ResponseEntity<PhoneCallDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(phonecallMapping.toDto(phonecallService.getDraft(new PhoneCall())));
    }

    @ApiOperation(value = "检查电话联络", tags = {"电话联络" },  notes = "检查电话联络")
	@RequestMapping(method = RequestMethod.POST, value = "/phonecalls/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody PhoneCallDTO phonecalldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(phonecallService.checkKey(phonecallMapping.toDomain(phonecalldto)));
    }

    @PreAuthorize("hasPermission(this.phonecallMapping.toDomain(#phonecalldto),'iBizBusinessCentral-PhoneCall-Save')")
    @ApiOperation(value = "保存电话联络", tags = {"电话联络" },  notes = "保存电话联络")
	@RequestMapping(method = RequestMethod.POST, value = "/phonecalls/save")
    public ResponseEntity<Boolean> save(@RequestBody PhoneCallDTO phonecalldto) {
        return ResponseEntity.status(HttpStatus.OK).body(phonecallService.save(phonecallMapping.toDomain(phonecalldto)));
    }

    @PreAuthorize("hasPermission(this.phonecallMapping.toDomain(#phonecalldtos),'iBizBusinessCentral-PhoneCall-Save')")
    @ApiOperation(value = "批量保存电话联络", tags = {"电话联络" },  notes = "批量保存电话联络")
	@RequestMapping(method = RequestMethod.POST, value = "/phonecalls/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<PhoneCallDTO> phonecalldtos) {
        phonecallService.saveBatch(phonecallMapping.toDomain(phonecalldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-PhoneCall-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-PhoneCall-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"电话联络" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/phonecalls/fetchdefault")
	public ResponseEntity<List<PhoneCallDTO>> fetchDefault(PhoneCallSearchContext context) {
        Page<PhoneCall> domains = phonecallService.searchDefault(context) ;
        List<PhoneCallDTO> list = phonecallMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-PhoneCall-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-PhoneCall-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"电话联络" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/phonecalls/searchdefault")
	public ResponseEntity<Page<PhoneCallDTO>> searchDefault(@RequestBody PhoneCallSearchContext context) {
        Page<PhoneCall> domains = phonecallService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(phonecallMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}


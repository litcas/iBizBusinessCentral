package cn.ibizlab.businesscentral.centralapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.product.domain.ProductPriceLevel;
import cn.ibizlab.businesscentral.centralapi.dto.ProductPriceLevelDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CentralApiProductPriceLevelMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface ProductPriceLevelMapping extends MappingBase<ProductPriceLevelDTO, ProductPriceLevel> {


}


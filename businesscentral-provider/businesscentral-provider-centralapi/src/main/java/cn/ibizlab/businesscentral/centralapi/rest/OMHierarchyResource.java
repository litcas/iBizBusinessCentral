package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.base.domain.OMHierarchy;
import cn.ibizlab.businesscentral.core.base.service.IOMHierarchyService;
import cn.ibizlab.businesscentral.core.base.filter.OMHierarchySearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"组织层次结构" })
@RestController("CentralApi-omhierarchy")
@RequestMapping("")
public class OMHierarchyResource {

    @Autowired
    public IOMHierarchyService omhierarchyService;

    @Autowired
    @Lazy
    public OMHierarchyMapping omhierarchyMapping;

    @PreAuthorize("hasPermission(this.omhierarchyMapping.toDomain(#omhierarchydto),'iBizBusinessCentral-OMHierarchy-Create')")
    @ApiOperation(value = "新建组织层次结构", tags = {"组织层次结构" },  notes = "新建组织层次结构")
	@RequestMapping(method = RequestMethod.POST, value = "/omhierarchies")
    public ResponseEntity<OMHierarchyDTO> create(@RequestBody OMHierarchyDTO omhierarchydto) {
        OMHierarchy domain = omhierarchyMapping.toDomain(omhierarchydto);
		omhierarchyService.create(domain);
        OMHierarchyDTO dto = omhierarchyMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.omhierarchyMapping.toDomain(#omhierarchydtos),'iBizBusinessCentral-OMHierarchy-Create')")
    @ApiOperation(value = "批量新建组织层次结构", tags = {"组织层次结构" },  notes = "批量新建组织层次结构")
	@RequestMapping(method = RequestMethod.POST, value = "/omhierarchies/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<OMHierarchyDTO> omhierarchydtos) {
        omhierarchyService.createBatch(omhierarchyMapping.toDomain(omhierarchydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "omhierarchy" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.omhierarchyService.get(#omhierarchy_id),'iBizBusinessCentral-OMHierarchy-Update')")
    @ApiOperation(value = "更新组织层次结构", tags = {"组织层次结构" },  notes = "更新组织层次结构")
	@RequestMapping(method = RequestMethod.PUT, value = "/omhierarchies/{omhierarchy_id}")
    public ResponseEntity<OMHierarchyDTO> update(@PathVariable("omhierarchy_id") String omhierarchy_id, @RequestBody OMHierarchyDTO omhierarchydto) {
		OMHierarchy domain  = omhierarchyMapping.toDomain(omhierarchydto);
        domain .setOmhierarchyid(omhierarchy_id);
		omhierarchyService.update(domain );
		OMHierarchyDTO dto = omhierarchyMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.omhierarchyService.getOmhierarchyByEntities(this.omhierarchyMapping.toDomain(#omhierarchydtos)),'iBizBusinessCentral-OMHierarchy-Update')")
    @ApiOperation(value = "批量更新组织层次结构", tags = {"组织层次结构" },  notes = "批量更新组织层次结构")
	@RequestMapping(method = RequestMethod.PUT, value = "/omhierarchies/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<OMHierarchyDTO> omhierarchydtos) {
        omhierarchyService.updateBatch(omhierarchyMapping.toDomain(omhierarchydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.omhierarchyService.get(#omhierarchy_id),'iBizBusinessCentral-OMHierarchy-Remove')")
    @ApiOperation(value = "删除组织层次结构", tags = {"组织层次结构" },  notes = "删除组织层次结构")
	@RequestMapping(method = RequestMethod.DELETE, value = "/omhierarchies/{omhierarchy_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("omhierarchy_id") String omhierarchy_id) {
         return ResponseEntity.status(HttpStatus.OK).body(omhierarchyService.remove(omhierarchy_id));
    }

    @PreAuthorize("hasPermission(this.omhierarchyService.getOmhierarchyByIds(#ids),'iBizBusinessCentral-OMHierarchy-Remove')")
    @ApiOperation(value = "批量删除组织层次结构", tags = {"组织层次结构" },  notes = "批量删除组织层次结构")
	@RequestMapping(method = RequestMethod.DELETE, value = "/omhierarchies/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        omhierarchyService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.omhierarchyMapping.toDomain(returnObject.body),'iBizBusinessCentral-OMHierarchy-Get')")
    @ApiOperation(value = "获取组织层次结构", tags = {"组织层次结构" },  notes = "获取组织层次结构")
	@RequestMapping(method = RequestMethod.GET, value = "/omhierarchies/{omhierarchy_id}")
    public ResponseEntity<OMHierarchyDTO> get(@PathVariable("omhierarchy_id") String omhierarchy_id) {
        OMHierarchy domain = omhierarchyService.get(omhierarchy_id);
        OMHierarchyDTO dto = omhierarchyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取组织层次结构草稿", tags = {"组织层次结构" },  notes = "获取组织层次结构草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/omhierarchies/getdraft")
    public ResponseEntity<OMHierarchyDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(omhierarchyMapping.toDto(omhierarchyService.getDraft(new OMHierarchy())));
    }

    @ApiOperation(value = "检查组织层次结构", tags = {"组织层次结构" },  notes = "检查组织层次结构")
	@RequestMapping(method = RequestMethod.POST, value = "/omhierarchies/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody OMHierarchyDTO omhierarchydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(omhierarchyService.checkKey(omhierarchyMapping.toDomain(omhierarchydto)));
    }

    @PreAuthorize("hasPermission(this.omhierarchyMapping.toDomain(#omhierarchydto),'iBizBusinessCentral-OMHierarchy-Save')")
    @ApiOperation(value = "保存组织层次结构", tags = {"组织层次结构" },  notes = "保存组织层次结构")
	@RequestMapping(method = RequestMethod.POST, value = "/omhierarchies/save")
    public ResponseEntity<Boolean> save(@RequestBody OMHierarchyDTO omhierarchydto) {
        return ResponseEntity.status(HttpStatus.OK).body(omhierarchyService.save(omhierarchyMapping.toDomain(omhierarchydto)));
    }

    @PreAuthorize("hasPermission(this.omhierarchyMapping.toDomain(#omhierarchydtos),'iBizBusinessCentral-OMHierarchy-Save')")
    @ApiOperation(value = "批量保存组织层次结构", tags = {"组织层次结构" },  notes = "批量保存组织层次结构")
	@RequestMapping(method = RequestMethod.POST, value = "/omhierarchies/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<OMHierarchyDTO> omhierarchydtos) {
        omhierarchyService.saveBatch(omhierarchyMapping.toDomain(omhierarchydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OMHierarchy-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OMHierarchy-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"组织层次结构" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/omhierarchies/fetchdefault")
	public ResponseEntity<List<OMHierarchyDTO>> fetchDefault(OMHierarchySearchContext context) {
        Page<OMHierarchy> domains = omhierarchyService.searchDefault(context) ;
        List<OMHierarchyDTO> list = omhierarchyMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OMHierarchy-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OMHierarchy-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"组织层次结构" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/omhierarchies/searchdefault")
	public ResponseEntity<Page<OMHierarchyDTO>> searchDefault(@RequestBody OMHierarchySearchContext context) {
        Page<OMHierarchy> domains = omhierarchyService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(omhierarchyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OMHierarchy-searchRootOrg-all') and hasPermission(#context,'iBizBusinessCentral-OMHierarchy-Get')")
	@ApiOperation(value = "获取顶级组织", tags = {"组织层次结构" } ,notes = "获取顶级组织")
    @RequestMapping(method= RequestMethod.GET , value="/omhierarchies/fetchrootorg")
	public ResponseEntity<List<OMHierarchyDTO>> fetchRootOrg(OMHierarchySearchContext context) {
        Page<OMHierarchy> domains = omhierarchyService.searchRootOrg(context) ;
        List<OMHierarchyDTO> list = omhierarchyMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OMHierarchy-searchRootOrg-all') and hasPermission(#context,'iBizBusinessCentral-OMHierarchy-Get')")
	@ApiOperation(value = "查询顶级组织", tags = {"组织层次结构" } ,notes = "查询顶级组织")
    @RequestMapping(method= RequestMethod.POST , value="/omhierarchies/searchrootorg")
	public ResponseEntity<Page<OMHierarchyDTO>> searchRootOrg(@RequestBody OMHierarchySearchContext context) {
        Page<OMHierarchy> domains = omhierarchyService.searchRootOrg(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(omhierarchyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.omhierarchyMapping.toDomain(#omhierarchydto),'iBizBusinessCentral-OMHierarchy-Create')")
    @ApiOperation(value = "根据法人建立组织层次结构", tags = {"组织层次结构" },  notes = "根据法人建立组织层次结构")
	@RequestMapping(method = RequestMethod.POST, value = "/legals/{legal_id}/omhierarchies")
    public ResponseEntity<OMHierarchyDTO> createByLegal(@PathVariable("legal_id") String legal_id, @RequestBody OMHierarchyDTO omhierarchydto) {
        OMHierarchy domain = omhierarchyMapping.toDomain(omhierarchydto);
        domain.setOrganizationid(legal_id);
		omhierarchyService.create(domain);
        OMHierarchyDTO dto = omhierarchyMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.omhierarchyMapping.toDomain(#omhierarchydtos),'iBizBusinessCentral-OMHierarchy-Create')")
    @ApiOperation(value = "根据法人批量建立组织层次结构", tags = {"组织层次结构" },  notes = "根据法人批量建立组织层次结构")
	@RequestMapping(method = RequestMethod.POST, value = "/legals/{legal_id}/omhierarchies/batch")
    public ResponseEntity<Boolean> createBatchByLegal(@PathVariable("legal_id") String legal_id, @RequestBody List<OMHierarchyDTO> omhierarchydtos) {
        List<OMHierarchy> domainlist=omhierarchyMapping.toDomain(omhierarchydtos);
        for(OMHierarchy domain:domainlist){
            domain.setOrganizationid(legal_id);
        }
        omhierarchyService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "omhierarchy" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.omhierarchyService.get(#omhierarchy_id),'iBizBusinessCentral-OMHierarchy-Update')")
    @ApiOperation(value = "根据法人更新组织层次结构", tags = {"组织层次结构" },  notes = "根据法人更新组织层次结构")
	@RequestMapping(method = RequestMethod.PUT, value = "/legals/{legal_id}/omhierarchies/{omhierarchy_id}")
    public ResponseEntity<OMHierarchyDTO> updateByLegal(@PathVariable("legal_id") String legal_id, @PathVariable("omhierarchy_id") String omhierarchy_id, @RequestBody OMHierarchyDTO omhierarchydto) {
        OMHierarchy domain = omhierarchyMapping.toDomain(omhierarchydto);
        domain.setOrganizationid(legal_id);
        domain.setOmhierarchyid(omhierarchy_id);
		omhierarchyService.update(domain);
        OMHierarchyDTO dto = omhierarchyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.omhierarchyService.getOmhierarchyByEntities(this.omhierarchyMapping.toDomain(#omhierarchydtos)),'iBizBusinessCentral-OMHierarchy-Update')")
    @ApiOperation(value = "根据法人批量更新组织层次结构", tags = {"组织层次结构" },  notes = "根据法人批量更新组织层次结构")
	@RequestMapping(method = RequestMethod.PUT, value = "/legals/{legal_id}/omhierarchies/batch")
    public ResponseEntity<Boolean> updateBatchByLegal(@PathVariable("legal_id") String legal_id, @RequestBody List<OMHierarchyDTO> omhierarchydtos) {
        List<OMHierarchy> domainlist=omhierarchyMapping.toDomain(omhierarchydtos);
        for(OMHierarchy domain:domainlist){
            domain.setOrganizationid(legal_id);
        }
        omhierarchyService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.omhierarchyService.get(#omhierarchy_id),'iBizBusinessCentral-OMHierarchy-Remove')")
    @ApiOperation(value = "根据法人删除组织层次结构", tags = {"组织层次结构" },  notes = "根据法人删除组织层次结构")
	@RequestMapping(method = RequestMethod.DELETE, value = "/legals/{legal_id}/omhierarchies/{omhierarchy_id}")
    public ResponseEntity<Boolean> removeByLegal(@PathVariable("legal_id") String legal_id, @PathVariable("omhierarchy_id") String omhierarchy_id) {
		return ResponseEntity.status(HttpStatus.OK).body(omhierarchyService.remove(omhierarchy_id));
    }

    @PreAuthorize("hasPermission(this.omhierarchyService.getOmhierarchyByIds(#ids),'iBizBusinessCentral-OMHierarchy-Remove')")
    @ApiOperation(value = "根据法人批量删除组织层次结构", tags = {"组织层次结构" },  notes = "根据法人批量删除组织层次结构")
	@RequestMapping(method = RequestMethod.DELETE, value = "/legals/{legal_id}/omhierarchies/batch")
    public ResponseEntity<Boolean> removeBatchByLegal(@RequestBody List<String> ids) {
        omhierarchyService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.omhierarchyMapping.toDomain(returnObject.body),'iBizBusinessCentral-OMHierarchy-Get')")
    @ApiOperation(value = "根据法人获取组织层次结构", tags = {"组织层次结构" },  notes = "根据法人获取组织层次结构")
	@RequestMapping(method = RequestMethod.GET, value = "/legals/{legal_id}/omhierarchies/{omhierarchy_id}")
    public ResponseEntity<OMHierarchyDTO> getByLegal(@PathVariable("legal_id") String legal_id, @PathVariable("omhierarchy_id") String omhierarchy_id) {
        OMHierarchy domain = omhierarchyService.get(omhierarchy_id);
        OMHierarchyDTO dto = omhierarchyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据法人获取组织层次结构草稿", tags = {"组织层次结构" },  notes = "根据法人获取组织层次结构草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/legals/{legal_id}/omhierarchies/getdraft")
    public ResponseEntity<OMHierarchyDTO> getDraftByLegal(@PathVariable("legal_id") String legal_id) {
        OMHierarchy domain = new OMHierarchy();
        domain.setOrganizationid(legal_id);
        return ResponseEntity.status(HttpStatus.OK).body(omhierarchyMapping.toDto(omhierarchyService.getDraft(domain)));
    }

    @ApiOperation(value = "根据法人检查组织层次结构", tags = {"组织层次结构" },  notes = "根据法人检查组织层次结构")
	@RequestMapping(method = RequestMethod.POST, value = "/legals/{legal_id}/omhierarchies/checkkey")
    public ResponseEntity<Boolean> checkKeyByLegal(@PathVariable("legal_id") String legal_id, @RequestBody OMHierarchyDTO omhierarchydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(omhierarchyService.checkKey(omhierarchyMapping.toDomain(omhierarchydto)));
    }

    @PreAuthorize("hasPermission(this.omhierarchyMapping.toDomain(#omhierarchydto),'iBizBusinessCentral-OMHierarchy-Save')")
    @ApiOperation(value = "根据法人保存组织层次结构", tags = {"组织层次结构" },  notes = "根据法人保存组织层次结构")
	@RequestMapping(method = RequestMethod.POST, value = "/legals/{legal_id}/omhierarchies/save")
    public ResponseEntity<Boolean> saveByLegal(@PathVariable("legal_id") String legal_id, @RequestBody OMHierarchyDTO omhierarchydto) {
        OMHierarchy domain = omhierarchyMapping.toDomain(omhierarchydto);
        domain.setOrganizationid(legal_id);
        return ResponseEntity.status(HttpStatus.OK).body(omhierarchyService.save(domain));
    }

    @PreAuthorize("hasPermission(this.omhierarchyMapping.toDomain(#omhierarchydtos),'iBizBusinessCentral-OMHierarchy-Save')")
    @ApiOperation(value = "根据法人批量保存组织层次结构", tags = {"组织层次结构" },  notes = "根据法人批量保存组织层次结构")
	@RequestMapping(method = RequestMethod.POST, value = "/legals/{legal_id}/omhierarchies/savebatch")
    public ResponseEntity<Boolean> saveBatchByLegal(@PathVariable("legal_id") String legal_id, @RequestBody List<OMHierarchyDTO> omhierarchydtos) {
        List<OMHierarchy> domainlist=omhierarchyMapping.toDomain(omhierarchydtos);
        for(OMHierarchy domain:domainlist){
             domain.setOrganizationid(legal_id);
        }
        omhierarchyService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OMHierarchy-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OMHierarchy-Get')")
	@ApiOperation(value = "根据法人获取DEFAULT", tags = {"组织层次结构" } ,notes = "根据法人获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/legals/{legal_id}/omhierarchies/fetchdefault")
	public ResponseEntity<List<OMHierarchyDTO>> fetchOMHierarchyDefaultByLegal(@PathVariable("legal_id") String legal_id,OMHierarchySearchContext context) {
        context.setN_organizationid_eq(legal_id);
        Page<OMHierarchy> domains = omhierarchyService.searchDefault(context) ;
        List<OMHierarchyDTO> list = omhierarchyMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OMHierarchy-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OMHierarchy-Get')")
	@ApiOperation(value = "根据法人查询DEFAULT", tags = {"组织层次结构" } ,notes = "根据法人查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/legals/{legal_id}/omhierarchies/searchdefault")
	public ResponseEntity<Page<OMHierarchyDTO>> searchOMHierarchyDefaultByLegal(@PathVariable("legal_id") String legal_id, @RequestBody OMHierarchySearchContext context) {
        context.setN_organizationid_eq(legal_id);
        Page<OMHierarchy> domains = omhierarchyService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(omhierarchyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OMHierarchy-searchRootOrg-all') and hasPermission(#context,'iBizBusinessCentral-OMHierarchy-Get')")
	@ApiOperation(value = "根据法人获取顶级组织", tags = {"组织层次结构" } ,notes = "根据法人获取顶级组织")
    @RequestMapping(method= RequestMethod.GET , value="/legals/{legal_id}/omhierarchies/fetchrootorg")
	public ResponseEntity<List<OMHierarchyDTO>> fetchOMHierarchyRootOrgByLegal(@PathVariable("legal_id") String legal_id,OMHierarchySearchContext context) {
        context.setN_organizationid_eq(legal_id);
        Page<OMHierarchy> domains = omhierarchyService.searchRootOrg(context) ;
        List<OMHierarchyDTO> list = omhierarchyMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OMHierarchy-searchRootOrg-all') and hasPermission(#context,'iBizBusinessCentral-OMHierarchy-Get')")
	@ApiOperation(value = "根据法人查询顶级组织", tags = {"组织层次结构" } ,notes = "根据法人查询顶级组织")
    @RequestMapping(method= RequestMethod.POST , value="/legals/{legal_id}/omhierarchies/searchrootorg")
	public ResponseEntity<Page<OMHierarchyDTO>> searchOMHierarchyRootOrgByLegal(@PathVariable("legal_id") String legal_id, @RequestBody OMHierarchySearchContext context) {
        context.setN_organizationid_eq(legal_id);
        Page<OMHierarchy> domains = omhierarchyService.searchRootOrg(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(omhierarchyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.omhierarchyMapping.toDomain(#omhierarchydto),'iBizBusinessCentral-OMHierarchy-Create')")
    @ApiOperation(value = "根据结构层次类别建立组织层次结构", tags = {"组织层次结构" },  notes = "根据结构层次类别建立组织层次结构")
	@RequestMapping(method = RequestMethod.POST, value = "/omhierarchycats/{omhierarchycat_id}/omhierarchies")
    public ResponseEntity<OMHierarchyDTO> createByOMHierarchyCat(@PathVariable("omhierarchycat_id") String omhierarchycat_id, @RequestBody OMHierarchyDTO omhierarchydto) {
        OMHierarchy domain = omhierarchyMapping.toDomain(omhierarchydto);
        domain.setOmhierarchycatid(omhierarchycat_id);
		omhierarchyService.create(domain);
        OMHierarchyDTO dto = omhierarchyMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.omhierarchyMapping.toDomain(#omhierarchydtos),'iBizBusinessCentral-OMHierarchy-Create')")
    @ApiOperation(value = "根据结构层次类别批量建立组织层次结构", tags = {"组织层次结构" },  notes = "根据结构层次类别批量建立组织层次结构")
	@RequestMapping(method = RequestMethod.POST, value = "/omhierarchycats/{omhierarchycat_id}/omhierarchies/batch")
    public ResponseEntity<Boolean> createBatchByOMHierarchyCat(@PathVariable("omhierarchycat_id") String omhierarchycat_id, @RequestBody List<OMHierarchyDTO> omhierarchydtos) {
        List<OMHierarchy> domainlist=omhierarchyMapping.toDomain(omhierarchydtos);
        for(OMHierarchy domain:domainlist){
            domain.setOmhierarchycatid(omhierarchycat_id);
        }
        omhierarchyService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "omhierarchy" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.omhierarchyService.get(#omhierarchy_id),'iBizBusinessCentral-OMHierarchy-Update')")
    @ApiOperation(value = "根据结构层次类别更新组织层次结构", tags = {"组织层次结构" },  notes = "根据结构层次类别更新组织层次结构")
	@RequestMapping(method = RequestMethod.PUT, value = "/omhierarchycats/{omhierarchycat_id}/omhierarchies/{omhierarchy_id}")
    public ResponseEntity<OMHierarchyDTO> updateByOMHierarchyCat(@PathVariable("omhierarchycat_id") String omhierarchycat_id, @PathVariable("omhierarchy_id") String omhierarchy_id, @RequestBody OMHierarchyDTO omhierarchydto) {
        OMHierarchy domain = omhierarchyMapping.toDomain(omhierarchydto);
        domain.setOmhierarchycatid(omhierarchycat_id);
        domain.setOmhierarchyid(omhierarchy_id);
		omhierarchyService.update(domain);
        OMHierarchyDTO dto = omhierarchyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.omhierarchyService.getOmhierarchyByEntities(this.omhierarchyMapping.toDomain(#omhierarchydtos)),'iBizBusinessCentral-OMHierarchy-Update')")
    @ApiOperation(value = "根据结构层次类别批量更新组织层次结构", tags = {"组织层次结构" },  notes = "根据结构层次类别批量更新组织层次结构")
	@RequestMapping(method = RequestMethod.PUT, value = "/omhierarchycats/{omhierarchycat_id}/omhierarchies/batch")
    public ResponseEntity<Boolean> updateBatchByOMHierarchyCat(@PathVariable("omhierarchycat_id") String omhierarchycat_id, @RequestBody List<OMHierarchyDTO> omhierarchydtos) {
        List<OMHierarchy> domainlist=omhierarchyMapping.toDomain(omhierarchydtos);
        for(OMHierarchy domain:domainlist){
            domain.setOmhierarchycatid(omhierarchycat_id);
        }
        omhierarchyService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.omhierarchyService.get(#omhierarchy_id),'iBizBusinessCentral-OMHierarchy-Remove')")
    @ApiOperation(value = "根据结构层次类别删除组织层次结构", tags = {"组织层次结构" },  notes = "根据结构层次类别删除组织层次结构")
	@RequestMapping(method = RequestMethod.DELETE, value = "/omhierarchycats/{omhierarchycat_id}/omhierarchies/{omhierarchy_id}")
    public ResponseEntity<Boolean> removeByOMHierarchyCat(@PathVariable("omhierarchycat_id") String omhierarchycat_id, @PathVariable("omhierarchy_id") String omhierarchy_id) {
		return ResponseEntity.status(HttpStatus.OK).body(omhierarchyService.remove(omhierarchy_id));
    }

    @PreAuthorize("hasPermission(this.omhierarchyService.getOmhierarchyByIds(#ids),'iBizBusinessCentral-OMHierarchy-Remove')")
    @ApiOperation(value = "根据结构层次类别批量删除组织层次结构", tags = {"组织层次结构" },  notes = "根据结构层次类别批量删除组织层次结构")
	@RequestMapping(method = RequestMethod.DELETE, value = "/omhierarchycats/{omhierarchycat_id}/omhierarchies/batch")
    public ResponseEntity<Boolean> removeBatchByOMHierarchyCat(@RequestBody List<String> ids) {
        omhierarchyService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.omhierarchyMapping.toDomain(returnObject.body),'iBizBusinessCentral-OMHierarchy-Get')")
    @ApiOperation(value = "根据结构层次类别获取组织层次结构", tags = {"组织层次结构" },  notes = "根据结构层次类别获取组织层次结构")
	@RequestMapping(method = RequestMethod.GET, value = "/omhierarchycats/{omhierarchycat_id}/omhierarchies/{omhierarchy_id}")
    public ResponseEntity<OMHierarchyDTO> getByOMHierarchyCat(@PathVariable("omhierarchycat_id") String omhierarchycat_id, @PathVariable("omhierarchy_id") String omhierarchy_id) {
        OMHierarchy domain = omhierarchyService.get(omhierarchy_id);
        OMHierarchyDTO dto = omhierarchyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据结构层次类别获取组织层次结构草稿", tags = {"组织层次结构" },  notes = "根据结构层次类别获取组织层次结构草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/omhierarchycats/{omhierarchycat_id}/omhierarchies/getdraft")
    public ResponseEntity<OMHierarchyDTO> getDraftByOMHierarchyCat(@PathVariable("omhierarchycat_id") String omhierarchycat_id) {
        OMHierarchy domain = new OMHierarchy();
        domain.setOmhierarchycatid(omhierarchycat_id);
        return ResponseEntity.status(HttpStatus.OK).body(omhierarchyMapping.toDto(omhierarchyService.getDraft(domain)));
    }

    @ApiOperation(value = "根据结构层次类别检查组织层次结构", tags = {"组织层次结构" },  notes = "根据结构层次类别检查组织层次结构")
	@RequestMapping(method = RequestMethod.POST, value = "/omhierarchycats/{omhierarchycat_id}/omhierarchies/checkkey")
    public ResponseEntity<Boolean> checkKeyByOMHierarchyCat(@PathVariable("omhierarchycat_id") String omhierarchycat_id, @RequestBody OMHierarchyDTO omhierarchydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(omhierarchyService.checkKey(omhierarchyMapping.toDomain(omhierarchydto)));
    }

    @PreAuthorize("hasPermission(this.omhierarchyMapping.toDomain(#omhierarchydto),'iBizBusinessCentral-OMHierarchy-Save')")
    @ApiOperation(value = "根据结构层次类别保存组织层次结构", tags = {"组织层次结构" },  notes = "根据结构层次类别保存组织层次结构")
	@RequestMapping(method = RequestMethod.POST, value = "/omhierarchycats/{omhierarchycat_id}/omhierarchies/save")
    public ResponseEntity<Boolean> saveByOMHierarchyCat(@PathVariable("omhierarchycat_id") String omhierarchycat_id, @RequestBody OMHierarchyDTO omhierarchydto) {
        OMHierarchy domain = omhierarchyMapping.toDomain(omhierarchydto);
        domain.setOmhierarchycatid(omhierarchycat_id);
        return ResponseEntity.status(HttpStatus.OK).body(omhierarchyService.save(domain));
    }

    @PreAuthorize("hasPermission(this.omhierarchyMapping.toDomain(#omhierarchydtos),'iBizBusinessCentral-OMHierarchy-Save')")
    @ApiOperation(value = "根据结构层次类别批量保存组织层次结构", tags = {"组织层次结构" },  notes = "根据结构层次类别批量保存组织层次结构")
	@RequestMapping(method = RequestMethod.POST, value = "/omhierarchycats/{omhierarchycat_id}/omhierarchies/savebatch")
    public ResponseEntity<Boolean> saveBatchByOMHierarchyCat(@PathVariable("omhierarchycat_id") String omhierarchycat_id, @RequestBody List<OMHierarchyDTO> omhierarchydtos) {
        List<OMHierarchy> domainlist=omhierarchyMapping.toDomain(omhierarchydtos);
        for(OMHierarchy domain:domainlist){
             domain.setOmhierarchycatid(omhierarchycat_id);
        }
        omhierarchyService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OMHierarchy-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OMHierarchy-Get')")
	@ApiOperation(value = "根据结构层次类别获取DEFAULT", tags = {"组织层次结构" } ,notes = "根据结构层次类别获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/omhierarchycats/{omhierarchycat_id}/omhierarchies/fetchdefault")
	public ResponseEntity<List<OMHierarchyDTO>> fetchOMHierarchyDefaultByOMHierarchyCat(@PathVariable("omhierarchycat_id") String omhierarchycat_id,OMHierarchySearchContext context) {
        context.setN_omhierarchycatid_eq(omhierarchycat_id);
        Page<OMHierarchy> domains = omhierarchyService.searchDefault(context) ;
        List<OMHierarchyDTO> list = omhierarchyMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OMHierarchy-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OMHierarchy-Get')")
	@ApiOperation(value = "根据结构层次类别查询DEFAULT", tags = {"组织层次结构" } ,notes = "根据结构层次类别查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/omhierarchycats/{omhierarchycat_id}/omhierarchies/searchdefault")
	public ResponseEntity<Page<OMHierarchyDTO>> searchOMHierarchyDefaultByOMHierarchyCat(@PathVariable("omhierarchycat_id") String omhierarchycat_id, @RequestBody OMHierarchySearchContext context) {
        context.setN_omhierarchycatid_eq(omhierarchycat_id);
        Page<OMHierarchy> domains = omhierarchyService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(omhierarchyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OMHierarchy-searchRootOrg-all') and hasPermission(#context,'iBizBusinessCentral-OMHierarchy-Get')")
	@ApiOperation(value = "根据结构层次类别获取顶级组织", tags = {"组织层次结构" } ,notes = "根据结构层次类别获取顶级组织")
    @RequestMapping(method= RequestMethod.GET , value="/omhierarchycats/{omhierarchycat_id}/omhierarchies/fetchrootorg")
	public ResponseEntity<List<OMHierarchyDTO>> fetchOMHierarchyRootOrgByOMHierarchyCat(@PathVariable("omhierarchycat_id") String omhierarchycat_id,OMHierarchySearchContext context) {
        context.setN_omhierarchycatid_eq(omhierarchycat_id);
        Page<OMHierarchy> domains = omhierarchyService.searchRootOrg(context) ;
        List<OMHierarchyDTO> list = omhierarchyMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OMHierarchy-searchRootOrg-all') and hasPermission(#context,'iBizBusinessCentral-OMHierarchy-Get')")
	@ApiOperation(value = "根据结构层次类别查询顶级组织", tags = {"组织层次结构" } ,notes = "根据结构层次类别查询顶级组织")
    @RequestMapping(method= RequestMethod.POST , value="/omhierarchycats/{omhierarchycat_id}/omhierarchies/searchrootorg")
	public ResponseEntity<Page<OMHierarchyDTO>> searchOMHierarchyRootOrgByOMHierarchyCat(@PathVariable("omhierarchycat_id") String omhierarchycat_id, @RequestBody OMHierarchySearchContext context) {
        context.setN_omhierarchycatid_eq(omhierarchycat_id);
        Page<OMHierarchy> domains = omhierarchyService.searchRootOrg(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(omhierarchyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.omhierarchyMapping.toDomain(#omhierarchydto),'iBizBusinessCentral-OMHierarchy-Create')")
    @ApiOperation(value = "根据运营单位建立组织层次结构", tags = {"组织层次结构" },  notes = "根据运营单位建立组织层次结构")
	@RequestMapping(method = RequestMethod.POST, value = "/operationunits/{operationunit_id}/omhierarchies")
    public ResponseEntity<OMHierarchyDTO> createByOperationUnit(@PathVariable("operationunit_id") String operationunit_id, @RequestBody OMHierarchyDTO omhierarchydto) {
        OMHierarchy domain = omhierarchyMapping.toDomain(omhierarchydto);
        domain.setOrganizationid(operationunit_id);
		omhierarchyService.create(domain);
        OMHierarchyDTO dto = omhierarchyMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.omhierarchyMapping.toDomain(#omhierarchydtos),'iBizBusinessCentral-OMHierarchy-Create')")
    @ApiOperation(value = "根据运营单位批量建立组织层次结构", tags = {"组织层次结构" },  notes = "根据运营单位批量建立组织层次结构")
	@RequestMapping(method = RequestMethod.POST, value = "/operationunits/{operationunit_id}/omhierarchies/batch")
    public ResponseEntity<Boolean> createBatchByOperationUnit(@PathVariable("operationunit_id") String operationunit_id, @RequestBody List<OMHierarchyDTO> omhierarchydtos) {
        List<OMHierarchy> domainlist=omhierarchyMapping.toDomain(omhierarchydtos);
        for(OMHierarchy domain:domainlist){
            domain.setOrganizationid(operationunit_id);
        }
        omhierarchyService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "omhierarchy" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.omhierarchyService.get(#omhierarchy_id),'iBizBusinessCentral-OMHierarchy-Update')")
    @ApiOperation(value = "根据运营单位更新组织层次结构", tags = {"组织层次结构" },  notes = "根据运营单位更新组织层次结构")
	@RequestMapping(method = RequestMethod.PUT, value = "/operationunits/{operationunit_id}/omhierarchies/{omhierarchy_id}")
    public ResponseEntity<OMHierarchyDTO> updateByOperationUnit(@PathVariable("operationunit_id") String operationunit_id, @PathVariable("omhierarchy_id") String omhierarchy_id, @RequestBody OMHierarchyDTO omhierarchydto) {
        OMHierarchy domain = omhierarchyMapping.toDomain(omhierarchydto);
        domain.setOrganizationid(operationunit_id);
        domain.setOmhierarchyid(omhierarchy_id);
		omhierarchyService.update(domain);
        OMHierarchyDTO dto = omhierarchyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.omhierarchyService.getOmhierarchyByEntities(this.omhierarchyMapping.toDomain(#omhierarchydtos)),'iBizBusinessCentral-OMHierarchy-Update')")
    @ApiOperation(value = "根据运营单位批量更新组织层次结构", tags = {"组织层次结构" },  notes = "根据运营单位批量更新组织层次结构")
	@RequestMapping(method = RequestMethod.PUT, value = "/operationunits/{operationunit_id}/omhierarchies/batch")
    public ResponseEntity<Boolean> updateBatchByOperationUnit(@PathVariable("operationunit_id") String operationunit_id, @RequestBody List<OMHierarchyDTO> omhierarchydtos) {
        List<OMHierarchy> domainlist=omhierarchyMapping.toDomain(omhierarchydtos);
        for(OMHierarchy domain:domainlist){
            domain.setOrganizationid(operationunit_id);
        }
        omhierarchyService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.omhierarchyService.get(#omhierarchy_id),'iBizBusinessCentral-OMHierarchy-Remove')")
    @ApiOperation(value = "根据运营单位删除组织层次结构", tags = {"组织层次结构" },  notes = "根据运营单位删除组织层次结构")
	@RequestMapping(method = RequestMethod.DELETE, value = "/operationunits/{operationunit_id}/omhierarchies/{omhierarchy_id}")
    public ResponseEntity<Boolean> removeByOperationUnit(@PathVariable("operationunit_id") String operationunit_id, @PathVariable("omhierarchy_id") String omhierarchy_id) {
		return ResponseEntity.status(HttpStatus.OK).body(omhierarchyService.remove(omhierarchy_id));
    }

    @PreAuthorize("hasPermission(this.omhierarchyService.getOmhierarchyByIds(#ids),'iBizBusinessCentral-OMHierarchy-Remove')")
    @ApiOperation(value = "根据运营单位批量删除组织层次结构", tags = {"组织层次结构" },  notes = "根据运营单位批量删除组织层次结构")
	@RequestMapping(method = RequestMethod.DELETE, value = "/operationunits/{operationunit_id}/omhierarchies/batch")
    public ResponseEntity<Boolean> removeBatchByOperationUnit(@RequestBody List<String> ids) {
        omhierarchyService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.omhierarchyMapping.toDomain(returnObject.body),'iBizBusinessCentral-OMHierarchy-Get')")
    @ApiOperation(value = "根据运营单位获取组织层次结构", tags = {"组织层次结构" },  notes = "根据运营单位获取组织层次结构")
	@RequestMapping(method = RequestMethod.GET, value = "/operationunits/{operationunit_id}/omhierarchies/{omhierarchy_id}")
    public ResponseEntity<OMHierarchyDTO> getByOperationUnit(@PathVariable("operationunit_id") String operationunit_id, @PathVariable("omhierarchy_id") String omhierarchy_id) {
        OMHierarchy domain = omhierarchyService.get(omhierarchy_id);
        OMHierarchyDTO dto = omhierarchyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据运营单位获取组织层次结构草稿", tags = {"组织层次结构" },  notes = "根据运营单位获取组织层次结构草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/operationunits/{operationunit_id}/omhierarchies/getdraft")
    public ResponseEntity<OMHierarchyDTO> getDraftByOperationUnit(@PathVariable("operationunit_id") String operationunit_id) {
        OMHierarchy domain = new OMHierarchy();
        domain.setOrganizationid(operationunit_id);
        return ResponseEntity.status(HttpStatus.OK).body(omhierarchyMapping.toDto(omhierarchyService.getDraft(domain)));
    }

    @ApiOperation(value = "根据运营单位检查组织层次结构", tags = {"组织层次结构" },  notes = "根据运营单位检查组织层次结构")
	@RequestMapping(method = RequestMethod.POST, value = "/operationunits/{operationunit_id}/omhierarchies/checkkey")
    public ResponseEntity<Boolean> checkKeyByOperationUnit(@PathVariable("operationunit_id") String operationunit_id, @RequestBody OMHierarchyDTO omhierarchydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(omhierarchyService.checkKey(omhierarchyMapping.toDomain(omhierarchydto)));
    }

    @PreAuthorize("hasPermission(this.omhierarchyMapping.toDomain(#omhierarchydto),'iBizBusinessCentral-OMHierarchy-Save')")
    @ApiOperation(value = "根据运营单位保存组织层次结构", tags = {"组织层次结构" },  notes = "根据运营单位保存组织层次结构")
	@RequestMapping(method = RequestMethod.POST, value = "/operationunits/{operationunit_id}/omhierarchies/save")
    public ResponseEntity<Boolean> saveByOperationUnit(@PathVariable("operationunit_id") String operationunit_id, @RequestBody OMHierarchyDTO omhierarchydto) {
        OMHierarchy domain = omhierarchyMapping.toDomain(omhierarchydto);
        domain.setOrganizationid(operationunit_id);
        return ResponseEntity.status(HttpStatus.OK).body(omhierarchyService.save(domain));
    }

    @PreAuthorize("hasPermission(this.omhierarchyMapping.toDomain(#omhierarchydtos),'iBizBusinessCentral-OMHierarchy-Save')")
    @ApiOperation(value = "根据运营单位批量保存组织层次结构", tags = {"组织层次结构" },  notes = "根据运营单位批量保存组织层次结构")
	@RequestMapping(method = RequestMethod.POST, value = "/operationunits/{operationunit_id}/omhierarchies/savebatch")
    public ResponseEntity<Boolean> saveBatchByOperationUnit(@PathVariable("operationunit_id") String operationunit_id, @RequestBody List<OMHierarchyDTO> omhierarchydtos) {
        List<OMHierarchy> domainlist=omhierarchyMapping.toDomain(omhierarchydtos);
        for(OMHierarchy domain:domainlist){
             domain.setOrganizationid(operationunit_id);
        }
        omhierarchyService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OMHierarchy-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OMHierarchy-Get')")
	@ApiOperation(value = "根据运营单位获取DEFAULT", tags = {"组织层次结构" } ,notes = "根据运营单位获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/operationunits/{operationunit_id}/omhierarchies/fetchdefault")
	public ResponseEntity<List<OMHierarchyDTO>> fetchOMHierarchyDefaultByOperationUnit(@PathVariable("operationunit_id") String operationunit_id,OMHierarchySearchContext context) {
        context.setN_organizationid_eq(operationunit_id);
        Page<OMHierarchy> domains = omhierarchyService.searchDefault(context) ;
        List<OMHierarchyDTO> list = omhierarchyMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OMHierarchy-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OMHierarchy-Get')")
	@ApiOperation(value = "根据运营单位查询DEFAULT", tags = {"组织层次结构" } ,notes = "根据运营单位查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/operationunits/{operationunit_id}/omhierarchies/searchdefault")
	public ResponseEntity<Page<OMHierarchyDTO>> searchOMHierarchyDefaultByOperationUnit(@PathVariable("operationunit_id") String operationunit_id, @RequestBody OMHierarchySearchContext context) {
        context.setN_organizationid_eq(operationunit_id);
        Page<OMHierarchy> domains = omhierarchyService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(omhierarchyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OMHierarchy-searchRootOrg-all') and hasPermission(#context,'iBizBusinessCentral-OMHierarchy-Get')")
	@ApiOperation(value = "根据运营单位获取顶级组织", tags = {"组织层次结构" } ,notes = "根据运营单位获取顶级组织")
    @RequestMapping(method= RequestMethod.GET , value="/operationunits/{operationunit_id}/omhierarchies/fetchrootorg")
	public ResponseEntity<List<OMHierarchyDTO>> fetchOMHierarchyRootOrgByOperationUnit(@PathVariable("operationunit_id") String operationunit_id,OMHierarchySearchContext context) {
        context.setN_organizationid_eq(operationunit_id);
        Page<OMHierarchy> domains = omhierarchyService.searchRootOrg(context) ;
        List<OMHierarchyDTO> list = omhierarchyMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OMHierarchy-searchRootOrg-all') and hasPermission(#context,'iBizBusinessCentral-OMHierarchy-Get')")
	@ApiOperation(value = "根据运营单位查询顶级组织", tags = {"组织层次结构" } ,notes = "根据运营单位查询顶级组织")
    @RequestMapping(method= RequestMethod.POST , value="/operationunits/{operationunit_id}/omhierarchies/searchrootorg")
	public ResponseEntity<Page<OMHierarchyDTO>> searchOMHierarchyRootOrgByOperationUnit(@PathVariable("operationunit_id") String operationunit_id, @RequestBody OMHierarchySearchContext context) {
        context.setN_organizationid_eq(operationunit_id);
        Page<OMHierarchy> domains = omhierarchyService.searchRootOrg(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(omhierarchyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}


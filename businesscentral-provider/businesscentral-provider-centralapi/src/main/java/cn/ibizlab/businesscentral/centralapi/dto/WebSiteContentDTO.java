package cn.ibizlab.businesscentral.centralapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[WebSiteContentDTO]
 */
@Data
public class WebSiteContentDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CONTENT]
     *
     */
    @JSONField(name = "content")
    @JsonProperty("content")
    private String content;

    /**
     * 属性 [WEBSITECONTENTID]
     *
     */
    @JSONField(name = "websitecontentid")
    @JsonProperty("websitecontentid")
    private String websitecontentid;

    /**
     * 属性 [CONTENTCODE]
     *
     */
    @JSONField(name = "contentcode")
    @JsonProperty("contentcode")
    private String contentcode;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;

    /**
     * 属性 [MEMO]
     *
     */
    @JSONField(name = "memo")
    @JsonProperty("memo")
    private String memo;

    /**
     * 属性 [AUTHOR]
     *
     */
    @JSONField(name = "author")
    @JsonProperty("author")
    private String author;

    /**
     * 属性 [VALIDFLAG]
     *
     */
    @JSONField(name = "validflag")
    @JsonProperty("validflag")
    private Integer validflag;

    /**
     * 属性 [TITLE]
     *
     */
    @JSONField(name = "title")
    @JsonProperty("title")
    private String title;

    /**
     * 属性 [LINK]
     *
     */
    @JSONField(name = "link")
    @JsonProperty("link")
    private String link;

    /**
     * 属性 [SN]
     *
     */
    @JSONField(name = "sn")
    @JsonProperty("sn")
    private Integer sn;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [SUBTITLE]
     *
     */
    @JSONField(name = "subtitle")
    @JsonProperty("subtitle")
    private String subtitle;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;

    /**
     * 属性 [WEBSITECONTENTNAME]
     *
     */
    @JSONField(name = "websitecontentname")
    @JsonProperty("websitecontentname")
    private String websitecontentname;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [CONTENTTYPE]
     *
     */
    @JSONField(name = "contenttype")
    @JsonProperty("contenttype")
    private String contenttype;

    /**
     * 属性 [WEBSITECHANNELNAME]
     *
     */
    @JSONField(name = "websitechannelname")
    @JsonProperty("websitechannelname")
    private String websitechannelname;

    /**
     * 属性 [WEBSITENAME]
     *
     */
    @JSONField(name = "websitename")
    @JsonProperty("websitename")
    private String websitename;

    /**
     * 属性 [WEBSITEID]
     *
     */
    @JSONField(name = "websiteid")
    @JsonProperty("websiteid")
    private String websiteid;

    /**
     * 属性 [WEBSITECHANNELID]
     *
     */
    @JSONField(name = "websitechannelid")
    @JsonProperty("websitechannelid")
    private String websitechannelid;


    /**
     * 设置 [CONTENT]
     */
    public void setContent(String  content){
        this.content = content ;
        this.modify("content",content);
    }

    /**
     * 设置 [CONTENTCODE]
     */
    public void setContentcode(String  contentcode){
        this.contentcode = contentcode ;
        this.modify("contentcode",contentcode);
    }

    /**
     * 设置 [MEMO]
     */
    public void setMemo(String  memo){
        this.memo = memo ;
        this.modify("memo",memo);
    }

    /**
     * 设置 [AUTHOR]
     */
    public void setAuthor(String  author){
        this.author = author ;
        this.modify("author",author);
    }

    /**
     * 设置 [VALIDFLAG]
     */
    public void setValidflag(Integer  validflag){
        this.validflag = validflag ;
        this.modify("validflag",validflag);
    }

    /**
     * 设置 [TITLE]
     */
    public void setTitle(String  title){
        this.title = title ;
        this.modify("title",title);
    }

    /**
     * 设置 [LINK]
     */
    public void setLink(String  link){
        this.link = link ;
        this.modify("link",link);
    }

    /**
     * 设置 [SN]
     */
    public void setSn(Integer  sn){
        this.sn = sn ;
        this.modify("sn",sn);
    }

    /**
     * 设置 [SUBTITLE]
     */
    public void setSubtitle(String  subtitle){
        this.subtitle = subtitle ;
        this.modify("subtitle",subtitle);
    }

    /**
     * 设置 [WEBSITECONTENTNAME]
     */
    public void setWebsitecontentname(String  websitecontentname){
        this.websitecontentname = websitecontentname ;
        this.modify("websitecontentname",websitecontentname);
    }

    /**
     * 设置 [CONTENTTYPE]
     */
    public void setContenttype(String  contenttype){
        this.contenttype = contenttype ;
        this.modify("contenttype",contenttype);
    }

    /**
     * 设置 [WEBSITEID]
     */
    public void setWebsiteid(String  websiteid){
        this.websiteid = websiteid ;
        this.modify("websiteid",websiteid);
    }

    /**
     * 设置 [WEBSITECHANNELID]
     */
    public void setWebsitechannelid(String  websitechannelid){
        this.websitechannelid = websitechannelid ;
        this.modify("websitechannelid",websitechannelid);
    }


}


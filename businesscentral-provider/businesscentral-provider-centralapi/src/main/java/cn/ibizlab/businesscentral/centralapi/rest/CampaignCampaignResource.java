package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.marketing.domain.CampaignCampaign;
import cn.ibizlab.businesscentral.core.marketing.service.ICampaignCampaignService;
import cn.ibizlab.businesscentral.core.marketing.filter.CampaignCampaignSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"市场活动-市场活动" })
@RestController("CentralApi-campaigncampaign")
@RequestMapping("")
public class CampaignCampaignResource {

    @Autowired
    public ICampaignCampaignService campaigncampaignService;

    @Autowired
    @Lazy
    public CampaignCampaignMapping campaigncampaignMapping;

    @PreAuthorize("hasPermission(this.campaigncampaignMapping.toDomain(#campaigncampaigndto),'iBizBusinessCentral-CampaignCampaign-Create')")
    @ApiOperation(value = "新建市场活动-市场活动", tags = {"市场活动-市场活动" },  notes = "新建市场活动-市场活动")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigncampaigns")
    public ResponseEntity<CampaignCampaignDTO> create(@RequestBody CampaignCampaignDTO campaigncampaigndto) {
        CampaignCampaign domain = campaigncampaignMapping.toDomain(campaigncampaigndto);
		campaigncampaignService.create(domain);
        CampaignCampaignDTO dto = campaigncampaignMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.campaigncampaignMapping.toDomain(#campaigncampaigndtos),'iBizBusinessCentral-CampaignCampaign-Create')")
    @ApiOperation(value = "批量新建市场活动-市场活动", tags = {"市场活动-市场活动" },  notes = "批量新建市场活动-市场活动")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigncampaigns/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<CampaignCampaignDTO> campaigncampaigndtos) {
        campaigncampaignService.createBatch(campaigncampaignMapping.toDomain(campaigncampaigndtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "campaigncampaign" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.campaigncampaignService.get(#campaigncampaign_id),'iBizBusinessCentral-CampaignCampaign-Update')")
    @ApiOperation(value = "更新市场活动-市场活动", tags = {"市场活动-市场活动" },  notes = "更新市场活动-市场活动")
	@RequestMapping(method = RequestMethod.PUT, value = "/campaigncampaigns/{campaigncampaign_id}")
    public ResponseEntity<CampaignCampaignDTO> update(@PathVariable("campaigncampaign_id") String campaigncampaign_id, @RequestBody CampaignCampaignDTO campaigncampaigndto) {
		CampaignCampaign domain  = campaigncampaignMapping.toDomain(campaigncampaigndto);
        domain .setRelationshipsid(campaigncampaign_id);
		campaigncampaignService.update(domain );
		CampaignCampaignDTO dto = campaigncampaignMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.campaigncampaignService.getCampaigncampaignByEntities(this.campaigncampaignMapping.toDomain(#campaigncampaigndtos)),'iBizBusinessCentral-CampaignCampaign-Update')")
    @ApiOperation(value = "批量更新市场活动-市场活动", tags = {"市场活动-市场活动" },  notes = "批量更新市场活动-市场活动")
	@RequestMapping(method = RequestMethod.PUT, value = "/campaigncampaigns/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<CampaignCampaignDTO> campaigncampaigndtos) {
        campaigncampaignService.updateBatch(campaigncampaignMapping.toDomain(campaigncampaigndtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.campaigncampaignService.get(#campaigncampaign_id),'iBizBusinessCentral-CampaignCampaign-Remove')")
    @ApiOperation(value = "删除市场活动-市场活动", tags = {"市场活动-市场活动" },  notes = "删除市场活动-市场活动")
	@RequestMapping(method = RequestMethod.DELETE, value = "/campaigncampaigns/{campaigncampaign_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("campaigncampaign_id") String campaigncampaign_id) {
         return ResponseEntity.status(HttpStatus.OK).body(campaigncampaignService.remove(campaigncampaign_id));
    }

    @PreAuthorize("hasPermission(this.campaigncampaignService.getCampaigncampaignByIds(#ids),'iBizBusinessCentral-CampaignCampaign-Remove')")
    @ApiOperation(value = "批量删除市场活动-市场活动", tags = {"市场活动-市场活动" },  notes = "批量删除市场活动-市场活动")
	@RequestMapping(method = RequestMethod.DELETE, value = "/campaigncampaigns/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        campaigncampaignService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.campaigncampaignMapping.toDomain(returnObject.body),'iBizBusinessCentral-CampaignCampaign-Get')")
    @ApiOperation(value = "获取市场活动-市场活动", tags = {"市场活动-市场活动" },  notes = "获取市场活动-市场活动")
	@RequestMapping(method = RequestMethod.GET, value = "/campaigncampaigns/{campaigncampaign_id}")
    public ResponseEntity<CampaignCampaignDTO> get(@PathVariable("campaigncampaign_id") String campaigncampaign_id) {
        CampaignCampaign domain = campaigncampaignService.get(campaigncampaign_id);
        CampaignCampaignDTO dto = campaigncampaignMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取市场活动-市场活动草稿", tags = {"市场活动-市场活动" },  notes = "获取市场活动-市场活动草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/campaigncampaigns/getdraft")
    public ResponseEntity<CampaignCampaignDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(campaigncampaignMapping.toDto(campaigncampaignService.getDraft(new CampaignCampaign())));
    }

    @ApiOperation(value = "检查市场活动-市场活动", tags = {"市场活动-市场活动" },  notes = "检查市场活动-市场活动")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigncampaigns/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody CampaignCampaignDTO campaigncampaigndto) {
        return  ResponseEntity.status(HttpStatus.OK).body(campaigncampaignService.checkKey(campaigncampaignMapping.toDomain(campaigncampaigndto)));
    }

    @PreAuthorize("hasPermission(this.campaigncampaignMapping.toDomain(#campaigncampaigndto),'iBizBusinessCentral-CampaignCampaign-Save')")
    @ApiOperation(value = "保存市场活动-市场活动", tags = {"市场活动-市场活动" },  notes = "保存市场活动-市场活动")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigncampaigns/save")
    public ResponseEntity<Boolean> save(@RequestBody CampaignCampaignDTO campaigncampaigndto) {
        return ResponseEntity.status(HttpStatus.OK).body(campaigncampaignService.save(campaigncampaignMapping.toDomain(campaigncampaigndto)));
    }

    @PreAuthorize("hasPermission(this.campaigncampaignMapping.toDomain(#campaigncampaigndtos),'iBizBusinessCentral-CampaignCampaign-Save')")
    @ApiOperation(value = "批量保存市场活动-市场活动", tags = {"市场活动-市场活动" },  notes = "批量保存市场活动-市场活动")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigncampaigns/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<CampaignCampaignDTO> campaigncampaigndtos) {
        campaigncampaignService.saveBatch(campaigncampaignMapping.toDomain(campaigncampaigndtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-CampaignCampaign-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-CampaignCampaign-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"市场活动-市场活动" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/campaigncampaigns/fetchdefault")
	public ResponseEntity<List<CampaignCampaignDTO>> fetchDefault(CampaignCampaignSearchContext context) {
        Page<CampaignCampaign> domains = campaigncampaignService.searchDefault(context) ;
        List<CampaignCampaignDTO> list = campaigncampaignMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-CampaignCampaign-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-CampaignCampaign-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"市场活动-市场活动" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/campaigncampaigns/searchdefault")
	public ResponseEntity<Page<CampaignCampaignDTO>> searchDefault(@RequestBody CampaignCampaignSearchContext context) {
        Page<CampaignCampaign> domains = campaigncampaignService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(campaigncampaignMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.campaigncampaignMapping.toDomain(#campaigncampaigndto),'iBizBusinessCentral-CampaignCampaign-Create')")
    @ApiOperation(value = "根据市场活动建立市场活动-市场活动", tags = {"市场活动-市场活动" },  notes = "根据市场活动建立市场活动-市场活动")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigns/{campaign_id}/campaigncampaigns")
    public ResponseEntity<CampaignCampaignDTO> createByCampaign(@PathVariable("campaign_id") String campaign_id, @RequestBody CampaignCampaignDTO campaigncampaigndto) {
        CampaignCampaign domain = campaigncampaignMapping.toDomain(campaigncampaigndto);
        domain.setEntity2id(campaign_id);
		campaigncampaignService.create(domain);
        CampaignCampaignDTO dto = campaigncampaignMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.campaigncampaignMapping.toDomain(#campaigncampaigndtos),'iBizBusinessCentral-CampaignCampaign-Create')")
    @ApiOperation(value = "根据市场活动批量建立市场活动-市场活动", tags = {"市场活动-市场活动" },  notes = "根据市场活动批量建立市场活动-市场活动")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigns/{campaign_id}/campaigncampaigns/batch")
    public ResponseEntity<Boolean> createBatchByCampaign(@PathVariable("campaign_id") String campaign_id, @RequestBody List<CampaignCampaignDTO> campaigncampaigndtos) {
        List<CampaignCampaign> domainlist=campaigncampaignMapping.toDomain(campaigncampaigndtos);
        for(CampaignCampaign domain:domainlist){
            domain.setEntity2id(campaign_id);
        }
        campaigncampaignService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "campaigncampaign" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.campaigncampaignService.get(#campaigncampaign_id),'iBizBusinessCentral-CampaignCampaign-Update')")
    @ApiOperation(value = "根据市场活动更新市场活动-市场活动", tags = {"市场活动-市场活动" },  notes = "根据市场活动更新市场活动-市场活动")
	@RequestMapping(method = RequestMethod.PUT, value = "/campaigns/{campaign_id}/campaigncampaigns/{campaigncampaign_id}")
    public ResponseEntity<CampaignCampaignDTO> updateByCampaign(@PathVariable("campaign_id") String campaign_id, @PathVariable("campaigncampaign_id") String campaigncampaign_id, @RequestBody CampaignCampaignDTO campaigncampaigndto) {
        CampaignCampaign domain = campaigncampaignMapping.toDomain(campaigncampaigndto);
        domain.setEntity2id(campaign_id);
        domain.setRelationshipsid(campaigncampaign_id);
		campaigncampaignService.update(domain);
        CampaignCampaignDTO dto = campaigncampaignMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.campaigncampaignService.getCampaigncampaignByEntities(this.campaigncampaignMapping.toDomain(#campaigncampaigndtos)),'iBizBusinessCentral-CampaignCampaign-Update')")
    @ApiOperation(value = "根据市场活动批量更新市场活动-市场活动", tags = {"市场活动-市场活动" },  notes = "根据市场活动批量更新市场活动-市场活动")
	@RequestMapping(method = RequestMethod.PUT, value = "/campaigns/{campaign_id}/campaigncampaigns/batch")
    public ResponseEntity<Boolean> updateBatchByCampaign(@PathVariable("campaign_id") String campaign_id, @RequestBody List<CampaignCampaignDTO> campaigncampaigndtos) {
        List<CampaignCampaign> domainlist=campaigncampaignMapping.toDomain(campaigncampaigndtos);
        for(CampaignCampaign domain:domainlist){
            domain.setEntity2id(campaign_id);
        }
        campaigncampaignService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.campaigncampaignService.get(#campaigncampaign_id),'iBizBusinessCentral-CampaignCampaign-Remove')")
    @ApiOperation(value = "根据市场活动删除市场活动-市场活动", tags = {"市场活动-市场活动" },  notes = "根据市场活动删除市场活动-市场活动")
	@RequestMapping(method = RequestMethod.DELETE, value = "/campaigns/{campaign_id}/campaigncampaigns/{campaigncampaign_id}")
    public ResponseEntity<Boolean> removeByCampaign(@PathVariable("campaign_id") String campaign_id, @PathVariable("campaigncampaign_id") String campaigncampaign_id) {
		return ResponseEntity.status(HttpStatus.OK).body(campaigncampaignService.remove(campaigncampaign_id));
    }

    @PreAuthorize("hasPermission(this.campaigncampaignService.getCampaigncampaignByIds(#ids),'iBizBusinessCentral-CampaignCampaign-Remove')")
    @ApiOperation(value = "根据市场活动批量删除市场活动-市场活动", tags = {"市场活动-市场活动" },  notes = "根据市场活动批量删除市场活动-市场活动")
	@RequestMapping(method = RequestMethod.DELETE, value = "/campaigns/{campaign_id}/campaigncampaigns/batch")
    public ResponseEntity<Boolean> removeBatchByCampaign(@RequestBody List<String> ids) {
        campaigncampaignService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.campaigncampaignMapping.toDomain(returnObject.body),'iBizBusinessCentral-CampaignCampaign-Get')")
    @ApiOperation(value = "根据市场活动获取市场活动-市场活动", tags = {"市场活动-市场活动" },  notes = "根据市场活动获取市场活动-市场活动")
	@RequestMapping(method = RequestMethod.GET, value = "/campaigns/{campaign_id}/campaigncampaigns/{campaigncampaign_id}")
    public ResponseEntity<CampaignCampaignDTO> getByCampaign(@PathVariable("campaign_id") String campaign_id, @PathVariable("campaigncampaign_id") String campaigncampaign_id) {
        CampaignCampaign domain = campaigncampaignService.get(campaigncampaign_id);
        CampaignCampaignDTO dto = campaigncampaignMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据市场活动获取市场活动-市场活动草稿", tags = {"市场活动-市场活动" },  notes = "根据市场活动获取市场活动-市场活动草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/campaigns/{campaign_id}/campaigncampaigns/getdraft")
    public ResponseEntity<CampaignCampaignDTO> getDraftByCampaign(@PathVariable("campaign_id") String campaign_id) {
        CampaignCampaign domain = new CampaignCampaign();
        domain.setEntity2id(campaign_id);
        return ResponseEntity.status(HttpStatus.OK).body(campaigncampaignMapping.toDto(campaigncampaignService.getDraft(domain)));
    }

    @ApiOperation(value = "根据市场活动检查市场活动-市场活动", tags = {"市场活动-市场活动" },  notes = "根据市场活动检查市场活动-市场活动")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigns/{campaign_id}/campaigncampaigns/checkkey")
    public ResponseEntity<Boolean> checkKeyByCampaign(@PathVariable("campaign_id") String campaign_id, @RequestBody CampaignCampaignDTO campaigncampaigndto) {
        return  ResponseEntity.status(HttpStatus.OK).body(campaigncampaignService.checkKey(campaigncampaignMapping.toDomain(campaigncampaigndto)));
    }

    @PreAuthorize("hasPermission(this.campaigncampaignMapping.toDomain(#campaigncampaigndto),'iBizBusinessCentral-CampaignCampaign-Save')")
    @ApiOperation(value = "根据市场活动保存市场活动-市场活动", tags = {"市场活动-市场活动" },  notes = "根据市场活动保存市场活动-市场活动")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigns/{campaign_id}/campaigncampaigns/save")
    public ResponseEntity<Boolean> saveByCampaign(@PathVariable("campaign_id") String campaign_id, @RequestBody CampaignCampaignDTO campaigncampaigndto) {
        CampaignCampaign domain = campaigncampaignMapping.toDomain(campaigncampaigndto);
        domain.setEntity2id(campaign_id);
        return ResponseEntity.status(HttpStatus.OK).body(campaigncampaignService.save(domain));
    }

    @PreAuthorize("hasPermission(this.campaigncampaignMapping.toDomain(#campaigncampaigndtos),'iBizBusinessCentral-CampaignCampaign-Save')")
    @ApiOperation(value = "根据市场活动批量保存市场活动-市场活动", tags = {"市场活动-市场活动" },  notes = "根据市场活动批量保存市场活动-市场活动")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigns/{campaign_id}/campaigncampaigns/savebatch")
    public ResponseEntity<Boolean> saveBatchByCampaign(@PathVariable("campaign_id") String campaign_id, @RequestBody List<CampaignCampaignDTO> campaigncampaigndtos) {
        List<CampaignCampaign> domainlist=campaigncampaignMapping.toDomain(campaigncampaigndtos);
        for(CampaignCampaign domain:domainlist){
             domain.setEntity2id(campaign_id);
        }
        campaigncampaignService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-CampaignCampaign-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-CampaignCampaign-Get')")
	@ApiOperation(value = "根据市场活动获取DEFAULT", tags = {"市场活动-市场活动" } ,notes = "根据市场活动获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/campaigns/{campaign_id}/campaigncampaigns/fetchdefault")
	public ResponseEntity<List<CampaignCampaignDTO>> fetchCampaignCampaignDefaultByCampaign(@PathVariable("campaign_id") String campaign_id,CampaignCampaignSearchContext context) {
        context.setN_entity2id_eq(campaign_id);
        Page<CampaignCampaign> domains = campaigncampaignService.searchDefault(context) ;
        List<CampaignCampaignDTO> list = campaigncampaignMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-CampaignCampaign-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-CampaignCampaign-Get')")
	@ApiOperation(value = "根据市场活动查询DEFAULT", tags = {"市场活动-市场活动" } ,notes = "根据市场活动查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/campaigns/{campaign_id}/campaigncampaigns/searchdefault")
	public ResponseEntity<Page<CampaignCampaignDTO>> searchCampaignCampaignDefaultByCampaign(@PathVariable("campaign_id") String campaign_id, @RequestBody CampaignCampaignSearchContext context) {
        context.setN_entity2id_eq(campaign_id);
        Page<CampaignCampaign> domains = campaigncampaignService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(campaigncampaignMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}


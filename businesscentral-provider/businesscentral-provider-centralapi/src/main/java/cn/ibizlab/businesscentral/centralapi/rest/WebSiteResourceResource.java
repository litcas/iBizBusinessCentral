package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.website.domain.WebSiteResource;
import cn.ibizlab.businesscentral.core.website.service.IWebSiteResourceService;
import cn.ibizlab.businesscentral.core.website.filter.WebSiteResourceSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"站点资源" })
@RestController("CentralApi-websiteresource")
@RequestMapping("")
public class WebSiteResourceResource {

    @Autowired
    public IWebSiteResourceService websiteresourceService;

    @Autowired
    @Lazy
    public WebSiteResourceMapping websiteresourceMapping;

    @PreAuthorize("hasPermission(this.websiteresourceMapping.toDomain(#websiteresourcedto),'iBizBusinessCentral-WebSiteResource-Create')")
    @ApiOperation(value = "新建站点资源", tags = {"站点资源" },  notes = "新建站点资源")
	@RequestMapping(method = RequestMethod.POST, value = "/websiteresources")
    public ResponseEntity<WebSiteResourceDTO> create(@RequestBody WebSiteResourceDTO websiteresourcedto) {
        WebSiteResource domain = websiteresourceMapping.toDomain(websiteresourcedto);
		websiteresourceService.create(domain);
        WebSiteResourceDTO dto = websiteresourceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.websiteresourceMapping.toDomain(#websiteresourcedtos),'iBizBusinessCentral-WebSiteResource-Create')")
    @ApiOperation(value = "批量新建站点资源", tags = {"站点资源" },  notes = "批量新建站点资源")
	@RequestMapping(method = RequestMethod.POST, value = "/websiteresources/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<WebSiteResourceDTO> websiteresourcedtos) {
        websiteresourceService.createBatch(websiteresourceMapping.toDomain(websiteresourcedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "websiteresource" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.websiteresourceService.get(#websiteresource_id),'iBizBusinessCentral-WebSiteResource-Update')")
    @ApiOperation(value = "更新站点资源", tags = {"站点资源" },  notes = "更新站点资源")
	@RequestMapping(method = RequestMethod.PUT, value = "/websiteresources/{websiteresource_id}")
    public ResponseEntity<WebSiteResourceDTO> update(@PathVariable("websiteresource_id") String websiteresource_id, @RequestBody WebSiteResourceDTO websiteresourcedto) {
		WebSiteResource domain  = websiteresourceMapping.toDomain(websiteresourcedto);
        domain .setWebsiteresourceid(websiteresource_id);
		websiteresourceService.update(domain );
		WebSiteResourceDTO dto = websiteresourceMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.websiteresourceService.getWebsiteresourceByEntities(this.websiteresourceMapping.toDomain(#websiteresourcedtos)),'iBizBusinessCentral-WebSiteResource-Update')")
    @ApiOperation(value = "批量更新站点资源", tags = {"站点资源" },  notes = "批量更新站点资源")
	@RequestMapping(method = RequestMethod.PUT, value = "/websiteresources/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<WebSiteResourceDTO> websiteresourcedtos) {
        websiteresourceService.updateBatch(websiteresourceMapping.toDomain(websiteresourcedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.websiteresourceService.get(#websiteresource_id),'iBizBusinessCentral-WebSiteResource-Remove')")
    @ApiOperation(value = "删除站点资源", tags = {"站点资源" },  notes = "删除站点资源")
	@RequestMapping(method = RequestMethod.DELETE, value = "/websiteresources/{websiteresource_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("websiteresource_id") String websiteresource_id) {
         return ResponseEntity.status(HttpStatus.OK).body(websiteresourceService.remove(websiteresource_id));
    }

    @PreAuthorize("hasPermission(this.websiteresourceService.getWebsiteresourceByIds(#ids),'iBizBusinessCentral-WebSiteResource-Remove')")
    @ApiOperation(value = "批量删除站点资源", tags = {"站点资源" },  notes = "批量删除站点资源")
	@RequestMapping(method = RequestMethod.DELETE, value = "/websiteresources/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        websiteresourceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.websiteresourceMapping.toDomain(returnObject.body),'iBizBusinessCentral-WebSiteResource-Get')")
    @ApiOperation(value = "获取站点资源", tags = {"站点资源" },  notes = "获取站点资源")
	@RequestMapping(method = RequestMethod.GET, value = "/websiteresources/{websiteresource_id}")
    public ResponseEntity<WebSiteResourceDTO> get(@PathVariable("websiteresource_id") String websiteresource_id) {
        WebSiteResource domain = websiteresourceService.get(websiteresource_id);
        WebSiteResourceDTO dto = websiteresourceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取站点资源草稿", tags = {"站点资源" },  notes = "获取站点资源草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/websiteresources/getdraft")
    public ResponseEntity<WebSiteResourceDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(websiteresourceMapping.toDto(websiteresourceService.getDraft(new WebSiteResource())));
    }

    @ApiOperation(value = "检查站点资源", tags = {"站点资源" },  notes = "检查站点资源")
	@RequestMapping(method = RequestMethod.POST, value = "/websiteresources/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody WebSiteResourceDTO websiteresourcedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(websiteresourceService.checkKey(websiteresourceMapping.toDomain(websiteresourcedto)));
    }

    @PreAuthorize("hasPermission(this.websiteresourceMapping.toDomain(#websiteresourcedto),'iBizBusinessCentral-WebSiteResource-Save')")
    @ApiOperation(value = "保存站点资源", tags = {"站点资源" },  notes = "保存站点资源")
	@RequestMapping(method = RequestMethod.POST, value = "/websiteresources/save")
    public ResponseEntity<Boolean> save(@RequestBody WebSiteResourceDTO websiteresourcedto) {
        return ResponseEntity.status(HttpStatus.OK).body(websiteresourceService.save(websiteresourceMapping.toDomain(websiteresourcedto)));
    }

    @PreAuthorize("hasPermission(this.websiteresourceMapping.toDomain(#websiteresourcedtos),'iBizBusinessCentral-WebSiteResource-Save')")
    @ApiOperation(value = "批量保存站点资源", tags = {"站点资源" },  notes = "批量保存站点资源")
	@RequestMapping(method = RequestMethod.POST, value = "/websiteresources/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<WebSiteResourceDTO> websiteresourcedtos) {
        websiteresourceService.saveBatch(websiteresourceMapping.toDomain(websiteresourcedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-WebSiteResource-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-WebSiteResource-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"站点资源" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/websiteresources/fetchdefault")
	public ResponseEntity<List<WebSiteResourceDTO>> fetchDefault(WebSiteResourceSearchContext context) {
        Page<WebSiteResource> domains = websiteresourceService.searchDefault(context) ;
        List<WebSiteResourceDTO> list = websiteresourceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-WebSiteResource-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-WebSiteResource-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"站点资源" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/websiteresources/searchdefault")
	public ResponseEntity<Page<WebSiteResourceDTO>> searchDefault(@RequestBody WebSiteResourceSearchContext context) {
        Page<WebSiteResource> domains = websiteresourceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(websiteresourceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.websiteresourceMapping.toDomain(#websiteresourcedto),'iBizBusinessCentral-WebSiteResource-Create')")
    @ApiOperation(value = "根据站点建立站点资源", tags = {"站点资源" },  notes = "根据站点建立站点资源")
	@RequestMapping(method = RequestMethod.POST, value = "/websites/{website_id}/websiteresources")
    public ResponseEntity<WebSiteResourceDTO> createByWebSite(@PathVariable("website_id") String website_id, @RequestBody WebSiteResourceDTO websiteresourcedto) {
        WebSiteResource domain = websiteresourceMapping.toDomain(websiteresourcedto);
        domain.setWebsiteid(website_id);
		websiteresourceService.create(domain);
        WebSiteResourceDTO dto = websiteresourceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.websiteresourceMapping.toDomain(#websiteresourcedtos),'iBizBusinessCentral-WebSiteResource-Create')")
    @ApiOperation(value = "根据站点批量建立站点资源", tags = {"站点资源" },  notes = "根据站点批量建立站点资源")
	@RequestMapping(method = RequestMethod.POST, value = "/websites/{website_id}/websiteresources/batch")
    public ResponseEntity<Boolean> createBatchByWebSite(@PathVariable("website_id") String website_id, @RequestBody List<WebSiteResourceDTO> websiteresourcedtos) {
        List<WebSiteResource> domainlist=websiteresourceMapping.toDomain(websiteresourcedtos);
        for(WebSiteResource domain:domainlist){
            domain.setWebsiteid(website_id);
        }
        websiteresourceService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "websiteresource" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.websiteresourceService.get(#websiteresource_id),'iBizBusinessCentral-WebSiteResource-Update')")
    @ApiOperation(value = "根据站点更新站点资源", tags = {"站点资源" },  notes = "根据站点更新站点资源")
	@RequestMapping(method = RequestMethod.PUT, value = "/websites/{website_id}/websiteresources/{websiteresource_id}")
    public ResponseEntity<WebSiteResourceDTO> updateByWebSite(@PathVariable("website_id") String website_id, @PathVariable("websiteresource_id") String websiteresource_id, @RequestBody WebSiteResourceDTO websiteresourcedto) {
        WebSiteResource domain = websiteresourceMapping.toDomain(websiteresourcedto);
        domain.setWebsiteid(website_id);
        domain.setWebsiteresourceid(websiteresource_id);
		websiteresourceService.update(domain);
        WebSiteResourceDTO dto = websiteresourceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.websiteresourceService.getWebsiteresourceByEntities(this.websiteresourceMapping.toDomain(#websiteresourcedtos)),'iBizBusinessCentral-WebSiteResource-Update')")
    @ApiOperation(value = "根据站点批量更新站点资源", tags = {"站点资源" },  notes = "根据站点批量更新站点资源")
	@RequestMapping(method = RequestMethod.PUT, value = "/websites/{website_id}/websiteresources/batch")
    public ResponseEntity<Boolean> updateBatchByWebSite(@PathVariable("website_id") String website_id, @RequestBody List<WebSiteResourceDTO> websiteresourcedtos) {
        List<WebSiteResource> domainlist=websiteresourceMapping.toDomain(websiteresourcedtos);
        for(WebSiteResource domain:domainlist){
            domain.setWebsiteid(website_id);
        }
        websiteresourceService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.websiteresourceService.get(#websiteresource_id),'iBizBusinessCentral-WebSiteResource-Remove')")
    @ApiOperation(value = "根据站点删除站点资源", tags = {"站点资源" },  notes = "根据站点删除站点资源")
	@RequestMapping(method = RequestMethod.DELETE, value = "/websites/{website_id}/websiteresources/{websiteresource_id}")
    public ResponseEntity<Boolean> removeByWebSite(@PathVariable("website_id") String website_id, @PathVariable("websiteresource_id") String websiteresource_id) {
		return ResponseEntity.status(HttpStatus.OK).body(websiteresourceService.remove(websiteresource_id));
    }

    @PreAuthorize("hasPermission(this.websiteresourceService.getWebsiteresourceByIds(#ids),'iBizBusinessCentral-WebSiteResource-Remove')")
    @ApiOperation(value = "根据站点批量删除站点资源", tags = {"站点资源" },  notes = "根据站点批量删除站点资源")
	@RequestMapping(method = RequestMethod.DELETE, value = "/websites/{website_id}/websiteresources/batch")
    public ResponseEntity<Boolean> removeBatchByWebSite(@RequestBody List<String> ids) {
        websiteresourceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.websiteresourceMapping.toDomain(returnObject.body),'iBizBusinessCentral-WebSiteResource-Get')")
    @ApiOperation(value = "根据站点获取站点资源", tags = {"站点资源" },  notes = "根据站点获取站点资源")
	@RequestMapping(method = RequestMethod.GET, value = "/websites/{website_id}/websiteresources/{websiteresource_id}")
    public ResponseEntity<WebSiteResourceDTO> getByWebSite(@PathVariable("website_id") String website_id, @PathVariable("websiteresource_id") String websiteresource_id) {
        WebSiteResource domain = websiteresourceService.get(websiteresource_id);
        WebSiteResourceDTO dto = websiteresourceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据站点获取站点资源草稿", tags = {"站点资源" },  notes = "根据站点获取站点资源草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/websites/{website_id}/websiteresources/getdraft")
    public ResponseEntity<WebSiteResourceDTO> getDraftByWebSite(@PathVariable("website_id") String website_id) {
        WebSiteResource domain = new WebSiteResource();
        domain.setWebsiteid(website_id);
        return ResponseEntity.status(HttpStatus.OK).body(websiteresourceMapping.toDto(websiteresourceService.getDraft(domain)));
    }

    @ApiOperation(value = "根据站点检查站点资源", tags = {"站点资源" },  notes = "根据站点检查站点资源")
	@RequestMapping(method = RequestMethod.POST, value = "/websites/{website_id}/websiteresources/checkkey")
    public ResponseEntity<Boolean> checkKeyByWebSite(@PathVariable("website_id") String website_id, @RequestBody WebSiteResourceDTO websiteresourcedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(websiteresourceService.checkKey(websiteresourceMapping.toDomain(websiteresourcedto)));
    }

    @PreAuthorize("hasPermission(this.websiteresourceMapping.toDomain(#websiteresourcedto),'iBizBusinessCentral-WebSiteResource-Save')")
    @ApiOperation(value = "根据站点保存站点资源", tags = {"站点资源" },  notes = "根据站点保存站点资源")
	@RequestMapping(method = RequestMethod.POST, value = "/websites/{website_id}/websiteresources/save")
    public ResponseEntity<Boolean> saveByWebSite(@PathVariable("website_id") String website_id, @RequestBody WebSiteResourceDTO websiteresourcedto) {
        WebSiteResource domain = websiteresourceMapping.toDomain(websiteresourcedto);
        domain.setWebsiteid(website_id);
        return ResponseEntity.status(HttpStatus.OK).body(websiteresourceService.save(domain));
    }

    @PreAuthorize("hasPermission(this.websiteresourceMapping.toDomain(#websiteresourcedtos),'iBizBusinessCentral-WebSiteResource-Save')")
    @ApiOperation(value = "根据站点批量保存站点资源", tags = {"站点资源" },  notes = "根据站点批量保存站点资源")
	@RequestMapping(method = RequestMethod.POST, value = "/websites/{website_id}/websiteresources/savebatch")
    public ResponseEntity<Boolean> saveBatchByWebSite(@PathVariable("website_id") String website_id, @RequestBody List<WebSiteResourceDTO> websiteresourcedtos) {
        List<WebSiteResource> domainlist=websiteresourceMapping.toDomain(websiteresourcedtos);
        for(WebSiteResource domain:domainlist){
             domain.setWebsiteid(website_id);
        }
        websiteresourceService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-WebSiteResource-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-WebSiteResource-Get')")
	@ApiOperation(value = "根据站点获取DEFAULT", tags = {"站点资源" } ,notes = "根据站点获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/websites/{website_id}/websiteresources/fetchdefault")
	public ResponseEntity<List<WebSiteResourceDTO>> fetchWebSiteResourceDefaultByWebSite(@PathVariable("website_id") String website_id,WebSiteResourceSearchContext context) {
        context.setN_websiteid_eq(website_id);
        Page<WebSiteResource> domains = websiteresourceService.searchDefault(context) ;
        List<WebSiteResourceDTO> list = websiteresourceMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-WebSiteResource-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-WebSiteResource-Get')")
	@ApiOperation(value = "根据站点查询DEFAULT", tags = {"站点资源" } ,notes = "根据站点查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/websites/{website_id}/websiteresources/searchdefault")
	public ResponseEntity<Page<WebSiteResourceDTO>> searchWebSiteResourceDefaultByWebSite(@PathVariable("website_id") String website_id, @RequestBody WebSiteResourceSearchContext context) {
        context.setN_websiteid_eq(website_id);
        Page<WebSiteResource> domains = websiteresourceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(websiteresourceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}


package cn.ibizlab.businesscentral.centralapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[ProductSubstituteDTO]
 */
@Data
public class ProductSubstituteDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [PRODUCTSUBSTITUTENAME]
     *
     */
    @JSONField(name = "productsubstitutename")
    @JsonProperty("productsubstitutename")
    private String productsubstitutename;

    /**
     * 属性 [STATUSCODE]
     *
     */
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;

    /**
     * 属性 [VERSIONNUMBER]
     *
     */
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;

    /**
     * 属性 [IMPORTSEQUENCENUMBER]
     *
     */
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;

    /**
     * 属性 [EXCHANGERATE]
     *
     */
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;

    /**
     * 属性 [UTCCONVERSIONTIMEZONECODE]
     *
     */
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;

    /**
     * 属性 [OVERRIDDENCREATEDON]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [DIRECTION]
     *
     */
    @JSONField(name = "direction")
    @JsonProperty("direction")
    private String direction;

    /**
     * 属性 [SALESRELATIONSHIPTYPE]
     *
     */
    @JSONField(name = "salesrelationshiptype")
    @JsonProperty("salesrelationshiptype")
    private String salesrelationshiptype;

    /**
     * 属性 [TIMEZONERULEVERSIONNUMBER]
     *
     */
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;

    /**
     * 属性 [PRODUCTSUBSTITUTEID]
     *
     */
    @JSONField(name = "productsubstituteid")
    @JsonProperty("productsubstituteid")
    private String productsubstituteid;

    /**
     * 属性 [STATECODE]
     *
     */
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [PRODUCTNAME]
     *
     */
    @JSONField(name = "productname")
    @JsonProperty("productname")
    private String productname;

    /**
     * 属性 [SUBSTITUTEDPRODUCTNAME]
     *
     */
    @JSONField(name = "substitutedproductname")
    @JsonProperty("substitutedproductname")
    private String substitutedproductname;

    /**
     * 属性 [CURRENCYNAME]
     *
     */
    @JSONField(name = "currencyname")
    @JsonProperty("currencyname")
    private String currencyname;

    /**
     * 属性 [PRODUCTID]
     *
     */
    @JSONField(name = "productid")
    @JsonProperty("productid")
    private String productid;

    /**
     * 属性 [SUBSTITUTEDPRODUCTID]
     *
     */
    @JSONField(name = "substitutedproductid")
    @JsonProperty("substitutedproductid")
    private String substitutedproductid;

    /**
     * 属性 [TRANSACTIONCURRENCYID]
     *
     */
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;


    /**
     * 设置 [PRODUCTSUBSTITUTENAME]
     */
    public void setProductsubstitutename(String  productsubstitutename){
        this.productsubstitutename = productsubstitutename ;
        this.modify("productsubstitutename",productsubstitutename);
    }

    /**
     * 设置 [STATUSCODE]
     */
    public void setStatuscode(Integer  statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [VERSIONNUMBER]
     */
    public void setVersionnumber(BigInteger  versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [IMPORTSEQUENCENUMBER]
     */
    public void setImportsequencenumber(Integer  importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [EXCHANGERATE]
     */
    public void setExchangerate(BigDecimal  exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [UTCCONVERSIONTIMEZONECODE]
     */
    public void setUtcconversiontimezonecode(Integer  utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [OVERRIDDENCREATEDON]
     */
    public void setOverriddencreatedon(Timestamp  overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 设置 [DIRECTION]
     */
    public void setDirection(String  direction){
        this.direction = direction ;
        this.modify("direction",direction);
    }

    /**
     * 设置 [SALESRELATIONSHIPTYPE]
     */
    public void setSalesrelationshiptype(String  salesrelationshiptype){
        this.salesrelationshiptype = salesrelationshiptype ;
        this.modify("salesrelationshiptype",salesrelationshiptype);
    }

    /**
     * 设置 [TIMEZONERULEVERSIONNUMBER]
     */
    public void setTimezoneruleversionnumber(Integer  timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [STATECODE]
     */
    public void setStatecode(Integer  statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [PRODUCTNAME]
     */
    public void setProductname(String  productname){
        this.productname = productname ;
        this.modify("productname",productname);
    }

    /**
     * 设置 [SUBSTITUTEDPRODUCTNAME]
     */
    public void setSubstitutedproductname(String  substitutedproductname){
        this.substitutedproductname = substitutedproductname ;
        this.modify("substitutedproductname",substitutedproductname);
    }

    /**
     * 设置 [CURRENCYNAME]
     */
    public void setCurrencyname(String  currencyname){
        this.currencyname = currencyname ;
        this.modify("currencyname",currencyname);
    }

    /**
     * 设置 [PRODUCTID]
     */
    public void setProductid(String  productid){
        this.productid = productid ;
        this.modify("productid",productid);
    }

    /**
     * 设置 [SUBSTITUTEDPRODUCTID]
     */
    public void setSubstitutedproductid(String  substitutedproductid){
        this.substitutedproductid = substitutedproductid ;
        this.modify("substitutedproductid",substitutedproductid);
    }

    /**
     * 设置 [TRANSACTIONCURRENCYID]
     */
    public void setTransactioncurrencyid(String  transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }


}


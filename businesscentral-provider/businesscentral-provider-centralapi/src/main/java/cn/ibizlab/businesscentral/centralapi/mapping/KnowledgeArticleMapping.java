package cn.ibizlab.businesscentral.centralapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.base.domain.KnowledgeArticle;
import cn.ibizlab.businesscentral.centralapi.dto.KnowledgeArticleDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CentralApiKnowledgeArticleMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface KnowledgeArticleMapping extends MappingBase<KnowledgeArticleDTO, KnowledgeArticle> {


}


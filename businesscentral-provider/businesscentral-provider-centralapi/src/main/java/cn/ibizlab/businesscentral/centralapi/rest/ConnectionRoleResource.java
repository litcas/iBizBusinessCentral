package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.runtime.domain.ConnectionRole;
import cn.ibizlab.businesscentral.core.runtime.service.IConnectionRoleService;
import cn.ibizlab.businesscentral.core.runtime.filter.ConnectionRoleSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"连接角色" })
@RestController("CentralApi-connectionrole")
@RequestMapping("")
public class ConnectionRoleResource {

    @Autowired
    public IConnectionRoleService connectionroleService;

    @Autowired
    @Lazy
    public ConnectionRoleMapping connectionroleMapping;

    @PreAuthorize("hasPermission(this.connectionroleMapping.toDomain(#connectionroledto),'iBizBusinessCentral-ConnectionRole-Create')")
    @ApiOperation(value = "新建连接角色", tags = {"连接角色" },  notes = "新建连接角色")
	@RequestMapping(method = RequestMethod.POST, value = "/connectionroles")
    public ResponseEntity<ConnectionRoleDTO> create(@RequestBody ConnectionRoleDTO connectionroledto) {
        ConnectionRole domain = connectionroleMapping.toDomain(connectionroledto);
		connectionroleService.create(domain);
        ConnectionRoleDTO dto = connectionroleMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.connectionroleMapping.toDomain(#connectionroledtos),'iBizBusinessCentral-ConnectionRole-Create')")
    @ApiOperation(value = "批量新建连接角色", tags = {"连接角色" },  notes = "批量新建连接角色")
	@RequestMapping(method = RequestMethod.POST, value = "/connectionroles/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<ConnectionRoleDTO> connectionroledtos) {
        connectionroleService.createBatch(connectionroleMapping.toDomain(connectionroledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "connectionrole" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.connectionroleService.get(#connectionrole_id),'iBizBusinessCentral-ConnectionRole-Update')")
    @ApiOperation(value = "更新连接角色", tags = {"连接角色" },  notes = "更新连接角色")
	@RequestMapping(method = RequestMethod.PUT, value = "/connectionroles/{connectionrole_id}")
    public ResponseEntity<ConnectionRoleDTO> update(@PathVariable("connectionrole_id") String connectionrole_id, @RequestBody ConnectionRoleDTO connectionroledto) {
		ConnectionRole domain  = connectionroleMapping.toDomain(connectionroledto);
        domain .setConnectionroleid(connectionrole_id);
		connectionroleService.update(domain );
		ConnectionRoleDTO dto = connectionroleMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.connectionroleService.getConnectionroleByEntities(this.connectionroleMapping.toDomain(#connectionroledtos)),'iBizBusinessCentral-ConnectionRole-Update')")
    @ApiOperation(value = "批量更新连接角色", tags = {"连接角色" },  notes = "批量更新连接角色")
	@RequestMapping(method = RequestMethod.PUT, value = "/connectionroles/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<ConnectionRoleDTO> connectionroledtos) {
        connectionroleService.updateBatch(connectionroleMapping.toDomain(connectionroledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.connectionroleService.get(#connectionrole_id),'iBizBusinessCentral-ConnectionRole-Remove')")
    @ApiOperation(value = "删除连接角色", tags = {"连接角色" },  notes = "删除连接角色")
	@RequestMapping(method = RequestMethod.DELETE, value = "/connectionroles/{connectionrole_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("connectionrole_id") String connectionrole_id) {
         return ResponseEntity.status(HttpStatus.OK).body(connectionroleService.remove(connectionrole_id));
    }

    @PreAuthorize("hasPermission(this.connectionroleService.getConnectionroleByIds(#ids),'iBizBusinessCentral-ConnectionRole-Remove')")
    @ApiOperation(value = "批量删除连接角色", tags = {"连接角色" },  notes = "批量删除连接角色")
	@RequestMapping(method = RequestMethod.DELETE, value = "/connectionroles/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        connectionroleService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.connectionroleMapping.toDomain(returnObject.body),'iBizBusinessCentral-ConnectionRole-Get')")
    @ApiOperation(value = "获取连接角色", tags = {"连接角色" },  notes = "获取连接角色")
	@RequestMapping(method = RequestMethod.GET, value = "/connectionroles/{connectionrole_id}")
    public ResponseEntity<ConnectionRoleDTO> get(@PathVariable("connectionrole_id") String connectionrole_id) {
        ConnectionRole domain = connectionroleService.get(connectionrole_id);
        ConnectionRoleDTO dto = connectionroleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取连接角色草稿", tags = {"连接角色" },  notes = "获取连接角色草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/connectionroles/getdraft")
    public ResponseEntity<ConnectionRoleDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(connectionroleMapping.toDto(connectionroleService.getDraft(new ConnectionRole())));
    }

    @ApiOperation(value = "检查连接角色", tags = {"连接角色" },  notes = "检查连接角色")
	@RequestMapping(method = RequestMethod.POST, value = "/connectionroles/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody ConnectionRoleDTO connectionroledto) {
        return  ResponseEntity.status(HttpStatus.OK).body(connectionroleService.checkKey(connectionroleMapping.toDomain(connectionroledto)));
    }

    @PreAuthorize("hasPermission(this.connectionroleMapping.toDomain(#connectionroledto),'iBizBusinessCentral-ConnectionRole-Save')")
    @ApiOperation(value = "保存连接角色", tags = {"连接角色" },  notes = "保存连接角色")
	@RequestMapping(method = RequestMethod.POST, value = "/connectionroles/save")
    public ResponseEntity<Boolean> save(@RequestBody ConnectionRoleDTO connectionroledto) {
        return ResponseEntity.status(HttpStatus.OK).body(connectionroleService.save(connectionroleMapping.toDomain(connectionroledto)));
    }

    @PreAuthorize("hasPermission(this.connectionroleMapping.toDomain(#connectionroledtos),'iBizBusinessCentral-ConnectionRole-Save')")
    @ApiOperation(value = "批量保存连接角色", tags = {"连接角色" },  notes = "批量保存连接角色")
	@RequestMapping(method = RequestMethod.POST, value = "/connectionroles/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<ConnectionRoleDTO> connectionroledtos) {
        connectionroleService.saveBatch(connectionroleMapping.toDomain(connectionroledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ConnectionRole-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ConnectionRole-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"连接角色" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/connectionroles/fetchdefault")
	public ResponseEntity<List<ConnectionRoleDTO>> fetchDefault(ConnectionRoleSearchContext context) {
        Page<ConnectionRole> domains = connectionroleService.searchDefault(context) ;
        List<ConnectionRoleDTO> list = connectionroleMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ConnectionRole-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ConnectionRole-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"连接角色" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/connectionroles/searchdefault")
	public ResponseEntity<Page<ConnectionRoleDTO>> searchDefault(@RequestBody ConnectionRoleSearchContext context) {
        Page<ConnectionRole> domains = connectionroleService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(connectionroleMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}


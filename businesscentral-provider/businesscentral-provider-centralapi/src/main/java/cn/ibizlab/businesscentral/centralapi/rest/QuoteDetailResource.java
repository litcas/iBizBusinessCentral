package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.sales.domain.QuoteDetail;
import cn.ibizlab.businesscentral.core.sales.service.IQuoteDetailService;
import cn.ibizlab.businesscentral.core.sales.filter.QuoteDetailSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"报价单产品" })
@RestController("CentralApi-quotedetail")
@RequestMapping("")
public class QuoteDetailResource {

    @Autowired
    public IQuoteDetailService quotedetailService;

    @Autowired
    @Lazy
    public QuoteDetailMapping quotedetailMapping;

    @PreAuthorize("hasPermission(this.quotedetailMapping.toDomain(#quotedetaildto),'iBizBusinessCentral-QuoteDetail-Create')")
    @ApiOperation(value = "新建报价单产品", tags = {"报价单产品" },  notes = "新建报价单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/quotedetails")
    public ResponseEntity<QuoteDetailDTO> create(@RequestBody QuoteDetailDTO quotedetaildto) {
        QuoteDetail domain = quotedetailMapping.toDomain(quotedetaildto);
		quotedetailService.create(domain);
        QuoteDetailDTO dto = quotedetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.quotedetailMapping.toDomain(#quotedetaildtos),'iBizBusinessCentral-QuoteDetail-Create')")
    @ApiOperation(value = "批量新建报价单产品", tags = {"报价单产品" },  notes = "批量新建报价单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/quotedetails/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<QuoteDetailDTO> quotedetaildtos) {
        quotedetailService.createBatch(quotedetailMapping.toDomain(quotedetaildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "quotedetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.quotedetailService.get(#quotedetail_id),'iBizBusinessCentral-QuoteDetail-Update')")
    @ApiOperation(value = "更新报价单产品", tags = {"报价单产品" },  notes = "更新报价单产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/quotedetails/{quotedetail_id}")
    public ResponseEntity<QuoteDetailDTO> update(@PathVariable("quotedetail_id") String quotedetail_id, @RequestBody QuoteDetailDTO quotedetaildto) {
		QuoteDetail domain  = quotedetailMapping.toDomain(quotedetaildto);
        domain .setQuotedetailid(quotedetail_id);
		quotedetailService.update(domain );
		QuoteDetailDTO dto = quotedetailMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.quotedetailService.getQuotedetailByEntities(this.quotedetailMapping.toDomain(#quotedetaildtos)),'iBizBusinessCentral-QuoteDetail-Update')")
    @ApiOperation(value = "批量更新报价单产品", tags = {"报价单产品" },  notes = "批量更新报价单产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/quotedetails/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<QuoteDetailDTO> quotedetaildtos) {
        quotedetailService.updateBatch(quotedetailMapping.toDomain(quotedetaildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.quotedetailService.get(#quotedetail_id),'iBizBusinessCentral-QuoteDetail-Remove')")
    @ApiOperation(value = "删除报价单产品", tags = {"报价单产品" },  notes = "删除报价单产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/quotedetails/{quotedetail_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("quotedetail_id") String quotedetail_id) {
         return ResponseEntity.status(HttpStatus.OK).body(quotedetailService.remove(quotedetail_id));
    }

    @PreAuthorize("hasPermission(this.quotedetailService.getQuotedetailByIds(#ids),'iBizBusinessCentral-QuoteDetail-Remove')")
    @ApiOperation(value = "批量删除报价单产品", tags = {"报价单产品" },  notes = "批量删除报价单产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/quotedetails/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        quotedetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.quotedetailMapping.toDomain(returnObject.body),'iBizBusinessCentral-QuoteDetail-Get')")
    @ApiOperation(value = "获取报价单产品", tags = {"报价单产品" },  notes = "获取报价单产品")
	@RequestMapping(method = RequestMethod.GET, value = "/quotedetails/{quotedetail_id}")
    public ResponseEntity<QuoteDetailDTO> get(@PathVariable("quotedetail_id") String quotedetail_id) {
        QuoteDetail domain = quotedetailService.get(quotedetail_id);
        QuoteDetailDTO dto = quotedetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取报价单产品草稿", tags = {"报价单产品" },  notes = "获取报价单产品草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/quotedetails/getdraft")
    public ResponseEntity<QuoteDetailDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(quotedetailMapping.toDto(quotedetailService.getDraft(new QuoteDetail())));
    }

    @ApiOperation(value = "检查报价单产品", tags = {"报价单产品" },  notes = "检查报价单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/quotedetails/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody QuoteDetailDTO quotedetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(quotedetailService.checkKey(quotedetailMapping.toDomain(quotedetaildto)));
    }

    @PreAuthorize("hasPermission(this.quotedetailMapping.toDomain(#quotedetaildto),'iBizBusinessCentral-QuoteDetail-Save')")
    @ApiOperation(value = "保存报价单产品", tags = {"报价单产品" },  notes = "保存报价单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/quotedetails/save")
    public ResponseEntity<Boolean> save(@RequestBody QuoteDetailDTO quotedetaildto) {
        return ResponseEntity.status(HttpStatus.OK).body(quotedetailService.save(quotedetailMapping.toDomain(quotedetaildto)));
    }

    @PreAuthorize("hasPermission(this.quotedetailMapping.toDomain(#quotedetaildtos),'iBizBusinessCentral-QuoteDetail-Save')")
    @ApiOperation(value = "批量保存报价单产品", tags = {"报价单产品" },  notes = "批量保存报价单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/quotedetails/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<QuoteDetailDTO> quotedetaildtos) {
        quotedetailService.saveBatch(quotedetailMapping.toDomain(quotedetaildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-QuoteDetail-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-QuoteDetail-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"报价单产品" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/quotedetails/fetchdefault")
	public ResponseEntity<List<QuoteDetailDTO>> fetchDefault(QuoteDetailSearchContext context) {
        Page<QuoteDetail> domains = quotedetailService.searchDefault(context) ;
        List<QuoteDetailDTO> list = quotedetailMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-QuoteDetail-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-QuoteDetail-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"报价单产品" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/quotedetails/searchdefault")
	public ResponseEntity<Page<QuoteDetailDTO>> searchDefault(@RequestBody QuoteDetailSearchContext context) {
        Page<QuoteDetail> domains = quotedetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quotedetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.quotedetailMapping.toDomain(#quotedetaildto),'iBizBusinessCentral-QuoteDetail-Create')")
    @ApiOperation(value = "根据报价单建立报价单产品", tags = {"报价单产品" },  notes = "根据报价单建立报价单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/quotes/{quote_id}/quotedetails")
    public ResponseEntity<QuoteDetailDTO> createByQuote(@PathVariable("quote_id") String quote_id, @RequestBody QuoteDetailDTO quotedetaildto) {
        QuoteDetail domain = quotedetailMapping.toDomain(quotedetaildto);
        domain.setQuoteid(quote_id);
		quotedetailService.create(domain);
        QuoteDetailDTO dto = quotedetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.quotedetailMapping.toDomain(#quotedetaildtos),'iBizBusinessCentral-QuoteDetail-Create')")
    @ApiOperation(value = "根据报价单批量建立报价单产品", tags = {"报价单产品" },  notes = "根据报价单批量建立报价单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/quotes/{quote_id}/quotedetails/batch")
    public ResponseEntity<Boolean> createBatchByQuote(@PathVariable("quote_id") String quote_id, @RequestBody List<QuoteDetailDTO> quotedetaildtos) {
        List<QuoteDetail> domainlist=quotedetailMapping.toDomain(quotedetaildtos);
        for(QuoteDetail domain:domainlist){
            domain.setQuoteid(quote_id);
        }
        quotedetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "quotedetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.quotedetailService.get(#quotedetail_id),'iBizBusinessCentral-QuoteDetail-Update')")
    @ApiOperation(value = "根据报价单更新报价单产品", tags = {"报价单产品" },  notes = "根据报价单更新报价单产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/quotes/{quote_id}/quotedetails/{quotedetail_id}")
    public ResponseEntity<QuoteDetailDTO> updateByQuote(@PathVariable("quote_id") String quote_id, @PathVariable("quotedetail_id") String quotedetail_id, @RequestBody QuoteDetailDTO quotedetaildto) {
        QuoteDetail domain = quotedetailMapping.toDomain(quotedetaildto);
        domain.setQuoteid(quote_id);
        domain.setQuotedetailid(quotedetail_id);
		quotedetailService.update(domain);
        QuoteDetailDTO dto = quotedetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.quotedetailService.getQuotedetailByEntities(this.quotedetailMapping.toDomain(#quotedetaildtos)),'iBizBusinessCentral-QuoteDetail-Update')")
    @ApiOperation(value = "根据报价单批量更新报价单产品", tags = {"报价单产品" },  notes = "根据报价单批量更新报价单产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/quotes/{quote_id}/quotedetails/batch")
    public ResponseEntity<Boolean> updateBatchByQuote(@PathVariable("quote_id") String quote_id, @RequestBody List<QuoteDetailDTO> quotedetaildtos) {
        List<QuoteDetail> domainlist=quotedetailMapping.toDomain(quotedetaildtos);
        for(QuoteDetail domain:domainlist){
            domain.setQuoteid(quote_id);
        }
        quotedetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.quotedetailService.get(#quotedetail_id),'iBizBusinessCentral-QuoteDetail-Remove')")
    @ApiOperation(value = "根据报价单删除报价单产品", tags = {"报价单产品" },  notes = "根据报价单删除报价单产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/quotes/{quote_id}/quotedetails/{quotedetail_id}")
    public ResponseEntity<Boolean> removeByQuote(@PathVariable("quote_id") String quote_id, @PathVariable("quotedetail_id") String quotedetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(quotedetailService.remove(quotedetail_id));
    }

    @PreAuthorize("hasPermission(this.quotedetailService.getQuotedetailByIds(#ids),'iBizBusinessCentral-QuoteDetail-Remove')")
    @ApiOperation(value = "根据报价单批量删除报价单产品", tags = {"报价单产品" },  notes = "根据报价单批量删除报价单产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/quotes/{quote_id}/quotedetails/batch")
    public ResponseEntity<Boolean> removeBatchByQuote(@RequestBody List<String> ids) {
        quotedetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.quotedetailMapping.toDomain(returnObject.body),'iBizBusinessCentral-QuoteDetail-Get')")
    @ApiOperation(value = "根据报价单获取报价单产品", tags = {"报价单产品" },  notes = "根据报价单获取报价单产品")
	@RequestMapping(method = RequestMethod.GET, value = "/quotes/{quote_id}/quotedetails/{quotedetail_id}")
    public ResponseEntity<QuoteDetailDTO> getByQuote(@PathVariable("quote_id") String quote_id, @PathVariable("quotedetail_id") String quotedetail_id) {
        QuoteDetail domain = quotedetailService.get(quotedetail_id);
        QuoteDetailDTO dto = quotedetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据报价单获取报价单产品草稿", tags = {"报价单产品" },  notes = "根据报价单获取报价单产品草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/quotes/{quote_id}/quotedetails/getdraft")
    public ResponseEntity<QuoteDetailDTO> getDraftByQuote(@PathVariable("quote_id") String quote_id) {
        QuoteDetail domain = new QuoteDetail();
        domain.setQuoteid(quote_id);
        return ResponseEntity.status(HttpStatus.OK).body(quotedetailMapping.toDto(quotedetailService.getDraft(domain)));
    }

    @ApiOperation(value = "根据报价单检查报价单产品", tags = {"报价单产品" },  notes = "根据报价单检查报价单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/quotes/{quote_id}/quotedetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByQuote(@PathVariable("quote_id") String quote_id, @RequestBody QuoteDetailDTO quotedetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(quotedetailService.checkKey(quotedetailMapping.toDomain(quotedetaildto)));
    }

    @PreAuthorize("hasPermission(this.quotedetailMapping.toDomain(#quotedetaildto),'iBizBusinessCentral-QuoteDetail-Save')")
    @ApiOperation(value = "根据报价单保存报价单产品", tags = {"报价单产品" },  notes = "根据报价单保存报价单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/quotes/{quote_id}/quotedetails/save")
    public ResponseEntity<Boolean> saveByQuote(@PathVariable("quote_id") String quote_id, @RequestBody QuoteDetailDTO quotedetaildto) {
        QuoteDetail domain = quotedetailMapping.toDomain(quotedetaildto);
        domain.setQuoteid(quote_id);
        return ResponseEntity.status(HttpStatus.OK).body(quotedetailService.save(domain));
    }

    @PreAuthorize("hasPermission(this.quotedetailMapping.toDomain(#quotedetaildtos),'iBizBusinessCentral-QuoteDetail-Save')")
    @ApiOperation(value = "根据报价单批量保存报价单产品", tags = {"报价单产品" },  notes = "根据报价单批量保存报价单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/quotes/{quote_id}/quotedetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByQuote(@PathVariable("quote_id") String quote_id, @RequestBody List<QuoteDetailDTO> quotedetaildtos) {
        List<QuoteDetail> domainlist=quotedetailMapping.toDomain(quotedetaildtos);
        for(QuoteDetail domain:domainlist){
             domain.setQuoteid(quote_id);
        }
        quotedetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-QuoteDetail-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-QuoteDetail-Get')")
	@ApiOperation(value = "根据报价单获取DEFAULT", tags = {"报价单产品" } ,notes = "根据报价单获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/quotes/{quote_id}/quotedetails/fetchdefault")
	public ResponseEntity<List<QuoteDetailDTO>> fetchQuoteDetailDefaultByQuote(@PathVariable("quote_id") String quote_id,QuoteDetailSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<QuoteDetail> domains = quotedetailService.searchDefault(context) ;
        List<QuoteDetailDTO> list = quotedetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-QuoteDetail-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-QuoteDetail-Get')")
	@ApiOperation(value = "根据报价单查询DEFAULT", tags = {"报价单产品" } ,notes = "根据报价单查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/quotes/{quote_id}/quotedetails/searchdefault")
	public ResponseEntity<Page<QuoteDetailDTO>> searchQuoteDetailDefaultByQuote(@PathVariable("quote_id") String quote_id, @RequestBody QuoteDetailSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<QuoteDetail> domains = quotedetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quotedetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.quotedetailMapping.toDomain(#quotedetaildto),'iBizBusinessCentral-QuoteDetail-Create')")
    @ApiOperation(value = "根据商机报价单建立报价单产品", tags = {"报价单产品" },  notes = "根据商机报价单建立报价单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails")
    public ResponseEntity<QuoteDetailDTO> createByOpportunityQuote(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDetailDTO quotedetaildto) {
        QuoteDetail domain = quotedetailMapping.toDomain(quotedetaildto);
        domain.setQuoteid(quote_id);
		quotedetailService.create(domain);
        QuoteDetailDTO dto = quotedetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.quotedetailMapping.toDomain(#quotedetaildtos),'iBizBusinessCentral-QuoteDetail-Create')")
    @ApiOperation(value = "根据商机报价单批量建立报价单产品", tags = {"报价单产品" },  notes = "根据商机报价单批量建立报价单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/batch")
    public ResponseEntity<Boolean> createBatchByOpportunityQuote(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody List<QuoteDetailDTO> quotedetaildtos) {
        List<QuoteDetail> domainlist=quotedetailMapping.toDomain(quotedetaildtos);
        for(QuoteDetail domain:domainlist){
            domain.setQuoteid(quote_id);
        }
        quotedetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "quotedetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.quotedetailService.get(#quotedetail_id),'iBizBusinessCentral-QuoteDetail-Update')")
    @ApiOperation(value = "根据商机报价单更新报价单产品", tags = {"报价单产品" },  notes = "根据商机报价单更新报价单产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/{quotedetail_id}")
    public ResponseEntity<QuoteDetailDTO> updateByOpportunityQuote(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("quotedetail_id") String quotedetail_id, @RequestBody QuoteDetailDTO quotedetaildto) {
        QuoteDetail domain = quotedetailMapping.toDomain(quotedetaildto);
        domain.setQuoteid(quote_id);
        domain.setQuotedetailid(quotedetail_id);
		quotedetailService.update(domain);
        QuoteDetailDTO dto = quotedetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.quotedetailService.getQuotedetailByEntities(this.quotedetailMapping.toDomain(#quotedetaildtos)),'iBizBusinessCentral-QuoteDetail-Update')")
    @ApiOperation(value = "根据商机报价单批量更新报价单产品", tags = {"报价单产品" },  notes = "根据商机报价单批量更新报价单产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/batch")
    public ResponseEntity<Boolean> updateBatchByOpportunityQuote(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody List<QuoteDetailDTO> quotedetaildtos) {
        List<QuoteDetail> domainlist=quotedetailMapping.toDomain(quotedetaildtos);
        for(QuoteDetail domain:domainlist){
            domain.setQuoteid(quote_id);
        }
        quotedetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.quotedetailService.get(#quotedetail_id),'iBizBusinessCentral-QuoteDetail-Remove')")
    @ApiOperation(value = "根据商机报价单删除报价单产品", tags = {"报价单产品" },  notes = "根据商机报价单删除报价单产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/{quotedetail_id}")
    public ResponseEntity<Boolean> removeByOpportunityQuote(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("quotedetail_id") String quotedetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(quotedetailService.remove(quotedetail_id));
    }

    @PreAuthorize("hasPermission(this.quotedetailService.getQuotedetailByIds(#ids),'iBizBusinessCentral-QuoteDetail-Remove')")
    @ApiOperation(value = "根据商机报价单批量删除报价单产品", tags = {"报价单产品" },  notes = "根据商机报价单批量删除报价单产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/batch")
    public ResponseEntity<Boolean> removeBatchByOpportunityQuote(@RequestBody List<String> ids) {
        quotedetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.quotedetailMapping.toDomain(returnObject.body),'iBizBusinessCentral-QuoteDetail-Get')")
    @ApiOperation(value = "根据商机报价单获取报价单产品", tags = {"报价单产品" },  notes = "根据商机报价单获取报价单产品")
	@RequestMapping(method = RequestMethod.GET, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/{quotedetail_id}")
    public ResponseEntity<QuoteDetailDTO> getByOpportunityQuote(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("quotedetail_id") String quotedetail_id) {
        QuoteDetail domain = quotedetailService.get(quotedetail_id);
        QuoteDetailDTO dto = quotedetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据商机报价单获取报价单产品草稿", tags = {"报价单产品" },  notes = "根据商机报价单获取报价单产品草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/getdraft")
    public ResponseEntity<QuoteDetailDTO> getDraftByOpportunityQuote(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id) {
        QuoteDetail domain = new QuoteDetail();
        domain.setQuoteid(quote_id);
        return ResponseEntity.status(HttpStatus.OK).body(quotedetailMapping.toDto(quotedetailService.getDraft(domain)));
    }

    @ApiOperation(value = "根据商机报价单检查报价单产品", tags = {"报价单产品" },  notes = "根据商机报价单检查报价单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByOpportunityQuote(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDetailDTO quotedetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(quotedetailService.checkKey(quotedetailMapping.toDomain(quotedetaildto)));
    }

    @PreAuthorize("hasPermission(this.quotedetailMapping.toDomain(#quotedetaildto),'iBizBusinessCentral-QuoteDetail-Save')")
    @ApiOperation(value = "根据商机报价单保存报价单产品", tags = {"报价单产品" },  notes = "根据商机报价单保存报价单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/save")
    public ResponseEntity<Boolean> saveByOpportunityQuote(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDetailDTO quotedetaildto) {
        QuoteDetail domain = quotedetailMapping.toDomain(quotedetaildto);
        domain.setQuoteid(quote_id);
        return ResponseEntity.status(HttpStatus.OK).body(quotedetailService.save(domain));
    }

    @PreAuthorize("hasPermission(this.quotedetailMapping.toDomain(#quotedetaildtos),'iBizBusinessCentral-QuoteDetail-Save')")
    @ApiOperation(value = "根据商机报价单批量保存报价单产品", tags = {"报价单产品" },  notes = "根据商机报价单批量保存报价单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByOpportunityQuote(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody List<QuoteDetailDTO> quotedetaildtos) {
        List<QuoteDetail> domainlist=quotedetailMapping.toDomain(quotedetaildtos);
        for(QuoteDetail domain:domainlist){
             domain.setQuoteid(quote_id);
        }
        quotedetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-QuoteDetail-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-QuoteDetail-Get')")
	@ApiOperation(value = "根据商机报价单获取DEFAULT", tags = {"报价单产品" } ,notes = "根据商机报价单获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/fetchdefault")
	public ResponseEntity<List<QuoteDetailDTO>> fetchQuoteDetailDefaultByOpportunityQuote(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id,QuoteDetailSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<QuoteDetail> domains = quotedetailService.searchDefault(context) ;
        List<QuoteDetailDTO> list = quotedetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-QuoteDetail-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-QuoteDetail-Get')")
	@ApiOperation(value = "根据商机报价单查询DEFAULT", tags = {"报价单产品" } ,notes = "根据商机报价单查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/searchdefault")
	public ResponseEntity<Page<QuoteDetailDTO>> searchQuoteDetailDefaultByOpportunityQuote(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDetailSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<QuoteDetail> domains = quotedetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quotedetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.quotedetailMapping.toDomain(#quotedetaildto),'iBizBusinessCentral-QuoteDetail-Create')")
    @ApiOperation(value = "根据客户商机报价单建立报价单产品", tags = {"报价单产品" },  notes = "根据客户商机报价单建立报价单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails")
    public ResponseEntity<QuoteDetailDTO> createByAccountOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDetailDTO quotedetaildto) {
        QuoteDetail domain = quotedetailMapping.toDomain(quotedetaildto);
        domain.setQuoteid(quote_id);
		quotedetailService.create(domain);
        QuoteDetailDTO dto = quotedetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.quotedetailMapping.toDomain(#quotedetaildtos),'iBizBusinessCentral-QuoteDetail-Create')")
    @ApiOperation(value = "根据客户商机报价单批量建立报价单产品", tags = {"报价单产品" },  notes = "根据客户商机报价单批量建立报价单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/batch")
    public ResponseEntity<Boolean> createBatchByAccountOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody List<QuoteDetailDTO> quotedetaildtos) {
        List<QuoteDetail> domainlist=quotedetailMapping.toDomain(quotedetaildtos);
        for(QuoteDetail domain:domainlist){
            domain.setQuoteid(quote_id);
        }
        quotedetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "quotedetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.quotedetailService.get(#quotedetail_id),'iBizBusinessCentral-QuoteDetail-Update')")
    @ApiOperation(value = "根据客户商机报价单更新报价单产品", tags = {"报价单产品" },  notes = "根据客户商机报价单更新报价单产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/{quotedetail_id}")
    public ResponseEntity<QuoteDetailDTO> updateByAccountOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("quotedetail_id") String quotedetail_id, @RequestBody QuoteDetailDTO quotedetaildto) {
        QuoteDetail domain = quotedetailMapping.toDomain(quotedetaildto);
        domain.setQuoteid(quote_id);
        domain.setQuotedetailid(quotedetail_id);
		quotedetailService.update(domain);
        QuoteDetailDTO dto = quotedetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.quotedetailService.getQuotedetailByEntities(this.quotedetailMapping.toDomain(#quotedetaildtos)),'iBizBusinessCentral-QuoteDetail-Update')")
    @ApiOperation(value = "根据客户商机报价单批量更新报价单产品", tags = {"报价单产品" },  notes = "根据客户商机报价单批量更新报价单产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/batch")
    public ResponseEntity<Boolean> updateBatchByAccountOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody List<QuoteDetailDTO> quotedetaildtos) {
        List<QuoteDetail> domainlist=quotedetailMapping.toDomain(quotedetaildtos);
        for(QuoteDetail domain:domainlist){
            domain.setQuoteid(quote_id);
        }
        quotedetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.quotedetailService.get(#quotedetail_id),'iBizBusinessCentral-QuoteDetail-Remove')")
    @ApiOperation(value = "根据客户商机报价单删除报价单产品", tags = {"报价单产品" },  notes = "根据客户商机报价单删除报价单产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/{quotedetail_id}")
    public ResponseEntity<Boolean> removeByAccountOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("quotedetail_id") String quotedetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(quotedetailService.remove(quotedetail_id));
    }

    @PreAuthorize("hasPermission(this.quotedetailService.getQuotedetailByIds(#ids),'iBizBusinessCentral-QuoteDetail-Remove')")
    @ApiOperation(value = "根据客户商机报价单批量删除报价单产品", tags = {"报价单产品" },  notes = "根据客户商机报价单批量删除报价单产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/batch")
    public ResponseEntity<Boolean> removeBatchByAccountOpportunityQuote(@RequestBody List<String> ids) {
        quotedetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.quotedetailMapping.toDomain(returnObject.body),'iBizBusinessCentral-QuoteDetail-Get')")
    @ApiOperation(value = "根据客户商机报价单获取报价单产品", tags = {"报价单产品" },  notes = "根据客户商机报价单获取报价单产品")
	@RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/{quotedetail_id}")
    public ResponseEntity<QuoteDetailDTO> getByAccountOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("quotedetail_id") String quotedetail_id) {
        QuoteDetail domain = quotedetailService.get(quotedetail_id);
        QuoteDetailDTO dto = quotedetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据客户商机报价单获取报价单产品草稿", tags = {"报价单产品" },  notes = "根据客户商机报价单获取报价单产品草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/getdraft")
    public ResponseEntity<QuoteDetailDTO> getDraftByAccountOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id) {
        QuoteDetail domain = new QuoteDetail();
        domain.setQuoteid(quote_id);
        return ResponseEntity.status(HttpStatus.OK).body(quotedetailMapping.toDto(quotedetailService.getDraft(domain)));
    }

    @ApiOperation(value = "根据客户商机报价单检查报价单产品", tags = {"报价单产品" },  notes = "根据客户商机报价单检查报价单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByAccountOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDetailDTO quotedetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(quotedetailService.checkKey(quotedetailMapping.toDomain(quotedetaildto)));
    }

    @PreAuthorize("hasPermission(this.quotedetailMapping.toDomain(#quotedetaildto),'iBizBusinessCentral-QuoteDetail-Save')")
    @ApiOperation(value = "根据客户商机报价单保存报价单产品", tags = {"报价单产品" },  notes = "根据客户商机报价单保存报价单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/save")
    public ResponseEntity<Boolean> saveByAccountOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDetailDTO quotedetaildto) {
        QuoteDetail domain = quotedetailMapping.toDomain(quotedetaildto);
        domain.setQuoteid(quote_id);
        return ResponseEntity.status(HttpStatus.OK).body(quotedetailService.save(domain));
    }

    @PreAuthorize("hasPermission(this.quotedetailMapping.toDomain(#quotedetaildtos),'iBizBusinessCentral-QuoteDetail-Save')")
    @ApiOperation(value = "根据客户商机报价单批量保存报价单产品", tags = {"报价单产品" },  notes = "根据客户商机报价单批量保存报价单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByAccountOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody List<QuoteDetailDTO> quotedetaildtos) {
        List<QuoteDetail> domainlist=quotedetailMapping.toDomain(quotedetaildtos);
        for(QuoteDetail domain:domainlist){
             domain.setQuoteid(quote_id);
        }
        quotedetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-QuoteDetail-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-QuoteDetail-Get')")
	@ApiOperation(value = "根据客户商机报价单获取DEFAULT", tags = {"报价单产品" } ,notes = "根据客户商机报价单获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/fetchdefault")
	public ResponseEntity<List<QuoteDetailDTO>> fetchQuoteDetailDefaultByAccountOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id,QuoteDetailSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<QuoteDetail> domains = quotedetailService.searchDefault(context) ;
        List<QuoteDetailDTO> list = quotedetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-QuoteDetail-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-QuoteDetail-Get')")
	@ApiOperation(value = "根据客户商机报价单查询DEFAULT", tags = {"报价单产品" } ,notes = "根据客户商机报价单查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/searchdefault")
	public ResponseEntity<Page<QuoteDetailDTO>> searchQuoteDetailDefaultByAccountOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDetailSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<QuoteDetail> domains = quotedetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quotedetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.quotedetailMapping.toDomain(#quotedetaildto),'iBizBusinessCentral-QuoteDetail-Create')")
    @ApiOperation(value = "根据联系人商机报价单建立报价单产品", tags = {"报价单产品" },  notes = "根据联系人商机报价单建立报价单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails")
    public ResponseEntity<QuoteDetailDTO> createByContactOpportunityQuote(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDetailDTO quotedetaildto) {
        QuoteDetail domain = quotedetailMapping.toDomain(quotedetaildto);
        domain.setQuoteid(quote_id);
		quotedetailService.create(domain);
        QuoteDetailDTO dto = quotedetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.quotedetailMapping.toDomain(#quotedetaildtos),'iBizBusinessCentral-QuoteDetail-Create')")
    @ApiOperation(value = "根据联系人商机报价单批量建立报价单产品", tags = {"报价单产品" },  notes = "根据联系人商机报价单批量建立报价单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/batch")
    public ResponseEntity<Boolean> createBatchByContactOpportunityQuote(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody List<QuoteDetailDTO> quotedetaildtos) {
        List<QuoteDetail> domainlist=quotedetailMapping.toDomain(quotedetaildtos);
        for(QuoteDetail domain:domainlist){
            domain.setQuoteid(quote_id);
        }
        quotedetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "quotedetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.quotedetailService.get(#quotedetail_id),'iBizBusinessCentral-QuoteDetail-Update')")
    @ApiOperation(value = "根据联系人商机报价单更新报价单产品", tags = {"报价单产品" },  notes = "根据联系人商机报价单更新报价单产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/{quotedetail_id}")
    public ResponseEntity<QuoteDetailDTO> updateByContactOpportunityQuote(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("quotedetail_id") String quotedetail_id, @RequestBody QuoteDetailDTO quotedetaildto) {
        QuoteDetail domain = quotedetailMapping.toDomain(quotedetaildto);
        domain.setQuoteid(quote_id);
        domain.setQuotedetailid(quotedetail_id);
		quotedetailService.update(domain);
        QuoteDetailDTO dto = quotedetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.quotedetailService.getQuotedetailByEntities(this.quotedetailMapping.toDomain(#quotedetaildtos)),'iBizBusinessCentral-QuoteDetail-Update')")
    @ApiOperation(value = "根据联系人商机报价单批量更新报价单产品", tags = {"报价单产品" },  notes = "根据联系人商机报价单批量更新报价单产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/batch")
    public ResponseEntity<Boolean> updateBatchByContactOpportunityQuote(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody List<QuoteDetailDTO> quotedetaildtos) {
        List<QuoteDetail> domainlist=quotedetailMapping.toDomain(quotedetaildtos);
        for(QuoteDetail domain:domainlist){
            domain.setQuoteid(quote_id);
        }
        quotedetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.quotedetailService.get(#quotedetail_id),'iBizBusinessCentral-QuoteDetail-Remove')")
    @ApiOperation(value = "根据联系人商机报价单删除报价单产品", tags = {"报价单产品" },  notes = "根据联系人商机报价单删除报价单产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/{quotedetail_id}")
    public ResponseEntity<Boolean> removeByContactOpportunityQuote(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("quotedetail_id") String quotedetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(quotedetailService.remove(quotedetail_id));
    }

    @PreAuthorize("hasPermission(this.quotedetailService.getQuotedetailByIds(#ids),'iBizBusinessCentral-QuoteDetail-Remove')")
    @ApiOperation(value = "根据联系人商机报价单批量删除报价单产品", tags = {"报价单产品" },  notes = "根据联系人商机报价单批量删除报价单产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/batch")
    public ResponseEntity<Boolean> removeBatchByContactOpportunityQuote(@RequestBody List<String> ids) {
        quotedetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.quotedetailMapping.toDomain(returnObject.body),'iBizBusinessCentral-QuoteDetail-Get')")
    @ApiOperation(value = "根据联系人商机报价单获取报价单产品", tags = {"报价单产品" },  notes = "根据联系人商机报价单获取报价单产品")
	@RequestMapping(method = RequestMethod.GET, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/{quotedetail_id}")
    public ResponseEntity<QuoteDetailDTO> getByContactOpportunityQuote(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("quotedetail_id") String quotedetail_id) {
        QuoteDetail domain = quotedetailService.get(quotedetail_id);
        QuoteDetailDTO dto = quotedetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据联系人商机报价单获取报价单产品草稿", tags = {"报价单产品" },  notes = "根据联系人商机报价单获取报价单产品草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/getdraft")
    public ResponseEntity<QuoteDetailDTO> getDraftByContactOpportunityQuote(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id) {
        QuoteDetail domain = new QuoteDetail();
        domain.setQuoteid(quote_id);
        return ResponseEntity.status(HttpStatus.OK).body(quotedetailMapping.toDto(quotedetailService.getDraft(domain)));
    }

    @ApiOperation(value = "根据联系人商机报价单检查报价单产品", tags = {"报价单产品" },  notes = "根据联系人商机报价单检查报价单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByContactOpportunityQuote(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDetailDTO quotedetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(quotedetailService.checkKey(quotedetailMapping.toDomain(quotedetaildto)));
    }

    @PreAuthorize("hasPermission(this.quotedetailMapping.toDomain(#quotedetaildto),'iBizBusinessCentral-QuoteDetail-Save')")
    @ApiOperation(value = "根据联系人商机报价单保存报价单产品", tags = {"报价单产品" },  notes = "根据联系人商机报价单保存报价单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/save")
    public ResponseEntity<Boolean> saveByContactOpportunityQuote(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDetailDTO quotedetaildto) {
        QuoteDetail domain = quotedetailMapping.toDomain(quotedetaildto);
        domain.setQuoteid(quote_id);
        return ResponseEntity.status(HttpStatus.OK).body(quotedetailService.save(domain));
    }

    @PreAuthorize("hasPermission(this.quotedetailMapping.toDomain(#quotedetaildtos),'iBizBusinessCentral-QuoteDetail-Save')")
    @ApiOperation(value = "根据联系人商机报价单批量保存报价单产品", tags = {"报价单产品" },  notes = "根据联系人商机报价单批量保存报价单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByContactOpportunityQuote(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody List<QuoteDetailDTO> quotedetaildtos) {
        List<QuoteDetail> domainlist=quotedetailMapping.toDomain(quotedetaildtos);
        for(QuoteDetail domain:domainlist){
             domain.setQuoteid(quote_id);
        }
        quotedetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-QuoteDetail-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-QuoteDetail-Get')")
	@ApiOperation(value = "根据联系人商机报价单获取DEFAULT", tags = {"报价单产品" } ,notes = "根据联系人商机报价单获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/fetchdefault")
	public ResponseEntity<List<QuoteDetailDTO>> fetchQuoteDetailDefaultByContactOpportunityQuote(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id,QuoteDetailSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<QuoteDetail> domains = quotedetailService.searchDefault(context) ;
        List<QuoteDetailDTO> list = quotedetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-QuoteDetail-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-QuoteDetail-Get')")
	@ApiOperation(value = "根据联系人商机报价单查询DEFAULT", tags = {"报价单产品" } ,notes = "根据联系人商机报价单查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/searchdefault")
	public ResponseEntity<Page<QuoteDetailDTO>> searchQuoteDetailDefaultByContactOpportunityQuote(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDetailSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<QuoteDetail> domains = quotedetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quotedetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.quotedetailMapping.toDomain(#quotedetaildto),'iBizBusinessCentral-QuoteDetail-Create')")
    @ApiOperation(value = "根据客户联系人商机报价单建立报价单产品", tags = {"报价单产品" },  notes = "根据客户联系人商机报价单建立报价单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails")
    public ResponseEntity<QuoteDetailDTO> createByAccountContactOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDetailDTO quotedetaildto) {
        QuoteDetail domain = quotedetailMapping.toDomain(quotedetaildto);
        domain.setQuoteid(quote_id);
		quotedetailService.create(domain);
        QuoteDetailDTO dto = quotedetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.quotedetailMapping.toDomain(#quotedetaildtos),'iBizBusinessCentral-QuoteDetail-Create')")
    @ApiOperation(value = "根据客户联系人商机报价单批量建立报价单产品", tags = {"报价单产品" },  notes = "根据客户联系人商机报价单批量建立报价单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/batch")
    public ResponseEntity<Boolean> createBatchByAccountContactOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody List<QuoteDetailDTO> quotedetaildtos) {
        List<QuoteDetail> domainlist=quotedetailMapping.toDomain(quotedetaildtos);
        for(QuoteDetail domain:domainlist){
            domain.setQuoteid(quote_id);
        }
        quotedetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "quotedetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.quotedetailService.get(#quotedetail_id),'iBizBusinessCentral-QuoteDetail-Update')")
    @ApiOperation(value = "根据客户联系人商机报价单更新报价单产品", tags = {"报价单产品" },  notes = "根据客户联系人商机报价单更新报价单产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/{quotedetail_id}")
    public ResponseEntity<QuoteDetailDTO> updateByAccountContactOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("quotedetail_id") String quotedetail_id, @RequestBody QuoteDetailDTO quotedetaildto) {
        QuoteDetail domain = quotedetailMapping.toDomain(quotedetaildto);
        domain.setQuoteid(quote_id);
        domain.setQuotedetailid(quotedetail_id);
		quotedetailService.update(domain);
        QuoteDetailDTO dto = quotedetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.quotedetailService.getQuotedetailByEntities(this.quotedetailMapping.toDomain(#quotedetaildtos)),'iBizBusinessCentral-QuoteDetail-Update')")
    @ApiOperation(value = "根据客户联系人商机报价单批量更新报价单产品", tags = {"报价单产品" },  notes = "根据客户联系人商机报价单批量更新报价单产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/batch")
    public ResponseEntity<Boolean> updateBatchByAccountContactOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody List<QuoteDetailDTO> quotedetaildtos) {
        List<QuoteDetail> domainlist=quotedetailMapping.toDomain(quotedetaildtos);
        for(QuoteDetail domain:domainlist){
            domain.setQuoteid(quote_id);
        }
        quotedetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.quotedetailService.get(#quotedetail_id),'iBizBusinessCentral-QuoteDetail-Remove')")
    @ApiOperation(value = "根据客户联系人商机报价单删除报价单产品", tags = {"报价单产品" },  notes = "根据客户联系人商机报价单删除报价单产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/{quotedetail_id}")
    public ResponseEntity<Boolean> removeByAccountContactOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("quotedetail_id") String quotedetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(quotedetailService.remove(quotedetail_id));
    }

    @PreAuthorize("hasPermission(this.quotedetailService.getQuotedetailByIds(#ids),'iBizBusinessCentral-QuoteDetail-Remove')")
    @ApiOperation(value = "根据客户联系人商机报价单批量删除报价单产品", tags = {"报价单产品" },  notes = "根据客户联系人商机报价单批量删除报价单产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/batch")
    public ResponseEntity<Boolean> removeBatchByAccountContactOpportunityQuote(@RequestBody List<String> ids) {
        quotedetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.quotedetailMapping.toDomain(returnObject.body),'iBizBusinessCentral-QuoteDetail-Get')")
    @ApiOperation(value = "根据客户联系人商机报价单获取报价单产品", tags = {"报价单产品" },  notes = "根据客户联系人商机报价单获取报价单产品")
	@RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/{quotedetail_id}")
    public ResponseEntity<QuoteDetailDTO> getByAccountContactOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("quotedetail_id") String quotedetail_id) {
        QuoteDetail domain = quotedetailService.get(quotedetail_id);
        QuoteDetailDTO dto = quotedetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据客户联系人商机报价单获取报价单产品草稿", tags = {"报价单产品" },  notes = "根据客户联系人商机报价单获取报价单产品草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/getdraft")
    public ResponseEntity<QuoteDetailDTO> getDraftByAccountContactOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id) {
        QuoteDetail domain = new QuoteDetail();
        domain.setQuoteid(quote_id);
        return ResponseEntity.status(HttpStatus.OK).body(quotedetailMapping.toDto(quotedetailService.getDraft(domain)));
    }

    @ApiOperation(value = "根据客户联系人商机报价单检查报价单产品", tags = {"报价单产品" },  notes = "根据客户联系人商机报价单检查报价单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByAccountContactOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDetailDTO quotedetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(quotedetailService.checkKey(quotedetailMapping.toDomain(quotedetaildto)));
    }

    @PreAuthorize("hasPermission(this.quotedetailMapping.toDomain(#quotedetaildto),'iBizBusinessCentral-QuoteDetail-Save')")
    @ApiOperation(value = "根据客户联系人商机报价单保存报价单产品", tags = {"报价单产品" },  notes = "根据客户联系人商机报价单保存报价单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/save")
    public ResponseEntity<Boolean> saveByAccountContactOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDetailDTO quotedetaildto) {
        QuoteDetail domain = quotedetailMapping.toDomain(quotedetaildto);
        domain.setQuoteid(quote_id);
        return ResponseEntity.status(HttpStatus.OK).body(quotedetailService.save(domain));
    }

    @PreAuthorize("hasPermission(this.quotedetailMapping.toDomain(#quotedetaildtos),'iBizBusinessCentral-QuoteDetail-Save')")
    @ApiOperation(value = "根据客户联系人商机报价单批量保存报价单产品", tags = {"报价单产品" },  notes = "根据客户联系人商机报价单批量保存报价单产品")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByAccountContactOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody List<QuoteDetailDTO> quotedetaildtos) {
        List<QuoteDetail> domainlist=quotedetailMapping.toDomain(quotedetaildtos);
        for(QuoteDetail domain:domainlist){
             domain.setQuoteid(quote_id);
        }
        quotedetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-QuoteDetail-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-QuoteDetail-Get')")
	@ApiOperation(value = "根据客户联系人商机报价单获取DEFAULT", tags = {"报价单产品" } ,notes = "根据客户联系人商机报价单获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/fetchdefault")
	public ResponseEntity<List<QuoteDetailDTO>> fetchQuoteDetailDefaultByAccountContactOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id,QuoteDetailSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<QuoteDetail> domains = quotedetailService.searchDefault(context) ;
        List<QuoteDetailDTO> list = quotedetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-QuoteDetail-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-QuoteDetail-Get')")
	@ApiOperation(value = "根据客户联系人商机报价单查询DEFAULT", tags = {"报价单产品" } ,notes = "根据客户联系人商机报价单查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/quotedetails/searchdefault")
	public ResponseEntity<Page<QuoteDetailDTO>> searchQuoteDetailDefaultByAccountContactOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDetailSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<QuoteDetail> domains = quotedetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quotedetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}


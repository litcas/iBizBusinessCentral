package cn.ibizlab.businesscentral.centralapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.marketing.domain.CampaignCampaign;
import cn.ibizlab.businesscentral.centralapi.dto.CampaignCampaignDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CentralApiCampaignCampaignMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface CampaignCampaignMapping extends MappingBase<CampaignCampaignDTO, CampaignCampaign> {


}


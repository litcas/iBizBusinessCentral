package cn.ibizlab.businesscentral.centralapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.uaa.domain.SysUser;
import cn.ibizlab.businesscentral.centralapi.dto.SysUserDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CentralApiSysUserMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface SysUserMapping extends MappingBase<SysUserDTO, SysUser> {


}


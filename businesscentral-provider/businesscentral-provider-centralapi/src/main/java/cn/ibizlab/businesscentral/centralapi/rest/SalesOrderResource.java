package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.sales.domain.SalesOrder;
import cn.ibizlab.businesscentral.core.sales.service.ISalesOrderService;
import cn.ibizlab.businesscentral.core.sales.filter.SalesOrderSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"订单" })
@RestController("CentralApi-salesorder")
@RequestMapping("")
public class SalesOrderResource {

    @Autowired
    public ISalesOrderService salesorderService;

    @Autowired
    @Lazy
    public SalesOrderMapping salesorderMapping;

    @PreAuthorize("hasPermission(this.salesorderMapping.toDomain(#salesorderdto),'iBizBusinessCentral-SalesOrder-Create')")
    @ApiOperation(value = "新建订单", tags = {"订单" },  notes = "新建订单")
	@RequestMapping(method = RequestMethod.POST, value = "/salesorders")
    public ResponseEntity<SalesOrderDTO> create(@RequestBody SalesOrderDTO salesorderdto) {
        SalesOrder domain = salesorderMapping.toDomain(salesorderdto);
		salesorderService.create(domain);
        SalesOrderDTO dto = salesorderMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.salesorderMapping.toDomain(#salesorderdtos),'iBizBusinessCentral-SalesOrder-Create')")
    @ApiOperation(value = "批量新建订单", tags = {"订单" },  notes = "批量新建订单")
	@RequestMapping(method = RequestMethod.POST, value = "/salesorders/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<SalesOrderDTO> salesorderdtos) {
        salesorderService.createBatch(salesorderMapping.toDomain(salesorderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "salesorder" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.salesorderService.get(#salesorder_id),'iBizBusinessCentral-SalesOrder-Update')")
    @ApiOperation(value = "更新订单", tags = {"订单" },  notes = "更新订单")
	@RequestMapping(method = RequestMethod.PUT, value = "/salesorders/{salesorder_id}")
    public ResponseEntity<SalesOrderDTO> update(@PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDTO salesorderdto) {
		SalesOrder domain  = salesorderMapping.toDomain(salesorderdto);
        domain .setSalesorderid(salesorder_id);
		salesorderService.update(domain );
		SalesOrderDTO dto = salesorderMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.salesorderService.getSalesorderByEntities(this.salesorderMapping.toDomain(#salesorderdtos)),'iBizBusinessCentral-SalesOrder-Update')")
    @ApiOperation(value = "批量更新订单", tags = {"订单" },  notes = "批量更新订单")
	@RequestMapping(method = RequestMethod.PUT, value = "/salesorders/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<SalesOrderDTO> salesorderdtos) {
        salesorderService.updateBatch(salesorderMapping.toDomain(salesorderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.salesorderService.get(#salesorder_id),'iBizBusinessCentral-SalesOrder-Remove')")
    @ApiOperation(value = "删除订单", tags = {"订单" },  notes = "删除订单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/salesorders/{salesorder_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("salesorder_id") String salesorder_id) {
         return ResponseEntity.status(HttpStatus.OK).body(salesorderService.remove(salesorder_id));
    }

    @PreAuthorize("hasPermission(this.salesorderService.getSalesorderByIds(#ids),'iBizBusinessCentral-SalesOrder-Remove')")
    @ApiOperation(value = "批量删除订单", tags = {"订单" },  notes = "批量删除订单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/salesorders/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        salesorderService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.salesorderMapping.toDomain(returnObject.body),'iBizBusinessCentral-SalesOrder-Get')")
    @ApiOperation(value = "获取订单", tags = {"订单" },  notes = "获取订单")
	@RequestMapping(method = RequestMethod.GET, value = "/salesorders/{salesorder_id}")
    public ResponseEntity<SalesOrderDTO> get(@PathVariable("salesorder_id") String salesorder_id) {
        SalesOrder domain = salesorderService.get(salesorder_id);
        SalesOrderDTO dto = salesorderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取订单草稿", tags = {"订单" },  notes = "获取订单草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/salesorders/getdraft")
    public ResponseEntity<SalesOrderDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(salesorderMapping.toDto(salesorderService.getDraft(new SalesOrder())));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-Cancel-all')")
    @ApiOperation(value = "取消订单", tags = {"订单" },  notes = "取消订单")
	@RequestMapping(method = RequestMethod.POST, value = "/salesorders/{salesorder_id}/cancel")
    public ResponseEntity<SalesOrderDTO> cancel(@PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDTO salesorderdto) {
        SalesOrder domain = salesorderMapping.toDomain(salesorderdto);
domain.setSalesorderid(salesorder_id);
        domain = salesorderService.cancel(domain);
        salesorderdto = salesorderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderdto);
    }

    @ApiOperation(value = "检查订单", tags = {"订单" },  notes = "检查订单")
	@RequestMapping(method = RequestMethod.POST, value = "/salesorders/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody SalesOrderDTO salesorderdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(salesorderService.checkKey(salesorderMapping.toDomain(salesorderdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-Finish-all')")
    @ApiOperation(value = "完成订单", tags = {"订单" },  notes = "完成订单")
	@RequestMapping(method = RequestMethod.POST, value = "/salesorders/{salesorder_id}/finish")
    public ResponseEntity<SalesOrderDTO> finish(@PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDTO salesorderdto) {
        SalesOrder domain = salesorderMapping.toDomain(salesorderdto);
domain.setSalesorderid(salesorder_id);
        domain = salesorderService.finish(domain);
        salesorderdto = salesorderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-GenInvoice-all')")
    @ApiOperation(value = "创建发票", tags = {"订单" },  notes = "创建发票")
	@RequestMapping(method = RequestMethod.POST, value = "/salesorders/{salesorder_id}/geninvoice")
    public ResponseEntity<SalesOrderDTO> genInvoice(@PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDTO salesorderdto) {
        SalesOrder domain = salesorderMapping.toDomain(salesorderdto);
domain.setSalesorderid(salesorder_id);
        domain = salesorderService.genInvoice(domain);
        salesorderdto = salesorderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderdto);
    }

    @PreAuthorize("hasPermission(this.salesorderMapping.toDomain(#salesorderdto),'iBizBusinessCentral-SalesOrder-Save')")
    @ApiOperation(value = "保存订单", tags = {"订单" },  notes = "保存订单")
	@RequestMapping(method = RequestMethod.POST, value = "/salesorders/save")
    public ResponseEntity<Boolean> save(@RequestBody SalesOrderDTO salesorderdto) {
        return ResponseEntity.status(HttpStatus.OK).body(salesorderService.save(salesorderMapping.toDomain(salesorderdto)));
    }

    @PreAuthorize("hasPermission(this.salesorderMapping.toDomain(#salesorderdtos),'iBizBusinessCentral-SalesOrder-Save')")
    @ApiOperation(value = "批量保存订单", tags = {"订单" },  notes = "批量保存订单")
	@RequestMapping(method = RequestMethod.POST, value = "/salesorders/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<SalesOrderDTO> salesorderdtos) {
        salesorderService.saveBatch(salesorderMapping.toDomain(salesorderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "获取ByParentKey", tags = {"订单" } ,notes = "获取ByParentKey")
    @RequestMapping(method= RequestMethod.GET , value="/salesorders/fetchbyparentkey")
	public ResponseEntity<List<SalesOrderDTO>> fetchByParentKey(SalesOrderSearchContext context) {
        Page<SalesOrder> domains = salesorderService.searchByParentKey(context) ;
        List<SalesOrderDTO> list = salesorderMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "查询ByParentKey", tags = {"订单" } ,notes = "查询ByParentKey")
    @RequestMapping(method= RequestMethod.POST , value="/salesorders/searchbyparentkey")
	public ResponseEntity<Page<SalesOrderDTO>> searchByParentKey(@RequestBody SalesOrderSearchContext context) {
        Page<SalesOrder> domains = salesorderService.searchByParentKey(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchCancel-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "获取已取消", tags = {"订单" } ,notes = "获取已取消")
    @RequestMapping(method= RequestMethod.GET , value="/salesorders/fetchcancel")
	public ResponseEntity<List<SalesOrderDTO>> fetchCancel(SalesOrderSearchContext context) {
        Page<SalesOrder> domains = salesorderService.searchCancel(context) ;
        List<SalesOrderDTO> list = salesorderMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchCancel-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "查询已取消", tags = {"订单" } ,notes = "查询已取消")
    @RequestMapping(method= RequestMethod.POST , value="/salesorders/searchcancel")
	public ResponseEntity<Page<SalesOrderDTO>> searchCancel(@RequestBody SalesOrderSearchContext context) {
        Page<SalesOrder> domains = salesorderService.searchCancel(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"订单" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/salesorders/fetchdefault")
	public ResponseEntity<List<SalesOrderDTO>> fetchDefault(SalesOrderSearchContext context) {
        Page<SalesOrder> domains = salesorderService.searchDefault(context) ;
        List<SalesOrderDTO> list = salesorderMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"订单" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/salesorders/searchdefault")
	public ResponseEntity<Page<SalesOrderDTO>> searchDefault(@RequestBody SalesOrderSearchContext context) {
        Page<SalesOrder> domains = salesorderService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchFinish-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "获取已完成", tags = {"订单" } ,notes = "获取已完成")
    @RequestMapping(method= RequestMethod.GET , value="/salesorders/fetchfinish")
	public ResponseEntity<List<SalesOrderDTO>> fetchFinish(SalesOrderSearchContext context) {
        Page<SalesOrder> domains = salesorderService.searchFinish(context) ;
        List<SalesOrderDTO> list = salesorderMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchFinish-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "查询已完成", tags = {"订单" } ,notes = "查询已完成")
    @RequestMapping(method= RequestMethod.POST , value="/salesorders/searchfinish")
	public ResponseEntity<Page<SalesOrderDTO>> searchFinish(@RequestBody SalesOrderSearchContext context) {
        Page<SalesOrder> domains = salesorderService.searchFinish(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchInvoiced-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "获取已开发票", tags = {"订单" } ,notes = "获取已开发票")
    @RequestMapping(method= RequestMethod.GET , value="/salesorders/fetchinvoiced")
	public ResponseEntity<List<SalesOrderDTO>> fetchInvoiced(SalesOrderSearchContext context) {
        Page<SalesOrder> domains = salesorderService.searchInvoiced(context) ;
        List<SalesOrderDTO> list = salesorderMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchInvoiced-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "查询已开发票", tags = {"订单" } ,notes = "查询已开发票")
    @RequestMapping(method= RequestMethod.POST , value="/salesorders/searchinvoiced")
	public ResponseEntity<Page<SalesOrderDTO>> searchInvoiced(@RequestBody SalesOrderSearchContext context) {
        Page<SalesOrder> domains = salesorderService.searchInvoiced(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.salesorderMapping.toDomain(#salesorderdto),'iBizBusinessCentral-SalesOrder-Create')")
    @ApiOperation(value = "根据报价单建立订单", tags = {"订单" },  notes = "根据报价单建立订单")
	@RequestMapping(method = RequestMethod.POST, value = "/quotes/{quote_id}/salesorders")
    public ResponseEntity<SalesOrderDTO> createByQuote(@PathVariable("quote_id") String quote_id, @RequestBody SalesOrderDTO salesorderdto) {
        SalesOrder domain = salesorderMapping.toDomain(salesorderdto);
        domain.setQuoteid(quote_id);
		salesorderService.create(domain);
        SalesOrderDTO dto = salesorderMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.salesorderMapping.toDomain(#salesorderdtos),'iBizBusinessCentral-SalesOrder-Create')")
    @ApiOperation(value = "根据报价单批量建立订单", tags = {"订单" },  notes = "根据报价单批量建立订单")
	@RequestMapping(method = RequestMethod.POST, value = "/quotes/{quote_id}/salesorders/batch")
    public ResponseEntity<Boolean> createBatchByQuote(@PathVariable("quote_id") String quote_id, @RequestBody List<SalesOrderDTO> salesorderdtos) {
        List<SalesOrder> domainlist=salesorderMapping.toDomain(salesorderdtos);
        for(SalesOrder domain:domainlist){
            domain.setQuoteid(quote_id);
        }
        salesorderService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "salesorder" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.salesorderService.get(#salesorder_id),'iBizBusinessCentral-SalesOrder-Update')")
    @ApiOperation(value = "根据报价单更新订单", tags = {"订单" },  notes = "根据报价单更新订单")
	@RequestMapping(method = RequestMethod.PUT, value = "/quotes/{quote_id}/salesorders/{salesorder_id}")
    public ResponseEntity<SalesOrderDTO> updateByQuote(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDTO salesorderdto) {
        SalesOrder domain = salesorderMapping.toDomain(salesorderdto);
        domain.setQuoteid(quote_id);
        domain.setSalesorderid(salesorder_id);
		salesorderService.update(domain);
        SalesOrderDTO dto = salesorderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.salesorderService.getSalesorderByEntities(this.salesorderMapping.toDomain(#salesorderdtos)),'iBizBusinessCentral-SalesOrder-Update')")
    @ApiOperation(value = "根据报价单批量更新订单", tags = {"订单" },  notes = "根据报价单批量更新订单")
	@RequestMapping(method = RequestMethod.PUT, value = "/quotes/{quote_id}/salesorders/batch")
    public ResponseEntity<Boolean> updateBatchByQuote(@PathVariable("quote_id") String quote_id, @RequestBody List<SalesOrderDTO> salesorderdtos) {
        List<SalesOrder> domainlist=salesorderMapping.toDomain(salesorderdtos);
        for(SalesOrder domain:domainlist){
            domain.setQuoteid(quote_id);
        }
        salesorderService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.salesorderService.get(#salesorder_id),'iBizBusinessCentral-SalesOrder-Remove')")
    @ApiOperation(value = "根据报价单删除订单", tags = {"订单" },  notes = "根据报价单删除订单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/quotes/{quote_id}/salesorders/{salesorder_id}")
    public ResponseEntity<Boolean> removeByQuote(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id) {
		return ResponseEntity.status(HttpStatus.OK).body(salesorderService.remove(salesorder_id));
    }

    @PreAuthorize("hasPermission(this.salesorderService.getSalesorderByIds(#ids),'iBizBusinessCentral-SalesOrder-Remove')")
    @ApiOperation(value = "根据报价单批量删除订单", tags = {"订单" },  notes = "根据报价单批量删除订单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/quotes/{quote_id}/salesorders/batch")
    public ResponseEntity<Boolean> removeBatchByQuote(@RequestBody List<String> ids) {
        salesorderService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.salesorderMapping.toDomain(returnObject.body),'iBizBusinessCentral-SalesOrder-Get')")
    @ApiOperation(value = "根据报价单获取订单", tags = {"订单" },  notes = "根据报价单获取订单")
	@RequestMapping(method = RequestMethod.GET, value = "/quotes/{quote_id}/salesorders/{salesorder_id}")
    public ResponseEntity<SalesOrderDTO> getByQuote(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id) {
        SalesOrder domain = salesorderService.get(salesorder_id);
        SalesOrderDTO dto = salesorderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据报价单获取订单草稿", tags = {"订单" },  notes = "根据报价单获取订单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/quotes/{quote_id}/salesorders/getdraft")
    public ResponseEntity<SalesOrderDTO> getDraftByQuote(@PathVariable("quote_id") String quote_id) {
        SalesOrder domain = new SalesOrder();
        domain.setQuoteid(quote_id);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderMapping.toDto(salesorderService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-Cancel-all')")
    @ApiOperation(value = "根据报价单订单", tags = {"订单" },  notes = "根据报价单订单")
	@RequestMapping(method = RequestMethod.POST, value = "/quotes/{quote_id}/salesorders/{salesorder_id}/cancel")
    public ResponseEntity<SalesOrderDTO> cancelByQuote(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDTO salesorderdto) {
        SalesOrder domain = salesorderMapping.toDomain(salesorderdto);
        domain.setQuoteid(quote_id);
        domain = salesorderService.cancel(domain) ;
        salesorderdto = salesorderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderdto);
    }

    @ApiOperation(value = "根据报价单检查订单", tags = {"订单" },  notes = "根据报价单检查订单")
	@RequestMapping(method = RequestMethod.POST, value = "/quotes/{quote_id}/salesorders/checkkey")
    public ResponseEntity<Boolean> checkKeyByQuote(@PathVariable("quote_id") String quote_id, @RequestBody SalesOrderDTO salesorderdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(salesorderService.checkKey(salesorderMapping.toDomain(salesorderdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-Finish-all')")
    @ApiOperation(value = "根据报价单订单", tags = {"订单" },  notes = "根据报价单订单")
	@RequestMapping(method = RequestMethod.POST, value = "/quotes/{quote_id}/salesorders/{salesorder_id}/finish")
    public ResponseEntity<SalesOrderDTO> finishByQuote(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDTO salesorderdto) {
        SalesOrder domain = salesorderMapping.toDomain(salesorderdto);
        domain.setQuoteid(quote_id);
        domain = salesorderService.finish(domain) ;
        salesorderdto = salesorderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-GenInvoice-all')")
    @ApiOperation(value = "根据报价单订单", tags = {"订单" },  notes = "根据报价单订单")
	@RequestMapping(method = RequestMethod.POST, value = "/quotes/{quote_id}/salesorders/{salesorder_id}/geninvoice")
    public ResponseEntity<SalesOrderDTO> genInvoiceByQuote(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDTO salesorderdto) {
        SalesOrder domain = salesorderMapping.toDomain(salesorderdto);
        domain.setQuoteid(quote_id);
        domain = salesorderService.genInvoice(domain) ;
        salesorderdto = salesorderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderdto);
    }

    @PreAuthorize("hasPermission(this.salesorderMapping.toDomain(#salesorderdto),'iBizBusinessCentral-SalesOrder-Save')")
    @ApiOperation(value = "根据报价单保存订单", tags = {"订单" },  notes = "根据报价单保存订单")
	@RequestMapping(method = RequestMethod.POST, value = "/quotes/{quote_id}/salesorders/save")
    public ResponseEntity<Boolean> saveByQuote(@PathVariable("quote_id") String quote_id, @RequestBody SalesOrderDTO salesorderdto) {
        SalesOrder domain = salesorderMapping.toDomain(salesorderdto);
        domain.setQuoteid(quote_id);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderService.save(domain));
    }

    @PreAuthorize("hasPermission(this.salesorderMapping.toDomain(#salesorderdtos),'iBizBusinessCentral-SalesOrder-Save')")
    @ApiOperation(value = "根据报价单批量保存订单", tags = {"订单" },  notes = "根据报价单批量保存订单")
	@RequestMapping(method = RequestMethod.POST, value = "/quotes/{quote_id}/salesorders/savebatch")
    public ResponseEntity<Boolean> saveBatchByQuote(@PathVariable("quote_id") String quote_id, @RequestBody List<SalesOrderDTO> salesorderdtos) {
        List<SalesOrder> domainlist=salesorderMapping.toDomain(salesorderdtos);
        for(SalesOrder domain:domainlist){
             domain.setQuoteid(quote_id);
        }
        salesorderService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据报价单获取ByParentKey", tags = {"订单" } ,notes = "根据报价单获取ByParentKey")
    @RequestMapping(method= RequestMethod.GET , value="/quotes/{quote_id}/salesorders/fetchbyparentkey")
	public ResponseEntity<List<SalesOrderDTO>> fetchSalesOrderByParentKeyByQuote(@PathVariable("quote_id") String quote_id,SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchByParentKey(context) ;
        List<SalesOrderDTO> list = salesorderMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据报价单查询ByParentKey", tags = {"订单" } ,notes = "根据报价单查询ByParentKey")
    @RequestMapping(method= RequestMethod.POST , value="/quotes/{quote_id}/salesorders/searchbyparentkey")
	public ResponseEntity<Page<SalesOrderDTO>> searchSalesOrderByParentKeyByQuote(@PathVariable("quote_id") String quote_id, @RequestBody SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchByParentKey(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchCancel-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据报价单获取已取消", tags = {"订单" } ,notes = "根据报价单获取已取消")
    @RequestMapping(method= RequestMethod.GET , value="/quotes/{quote_id}/salesorders/fetchcancel")
	public ResponseEntity<List<SalesOrderDTO>> fetchSalesOrderCancelByQuote(@PathVariable("quote_id") String quote_id,SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchCancel(context) ;
        List<SalesOrderDTO> list = salesorderMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchCancel-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据报价单查询已取消", tags = {"订单" } ,notes = "根据报价单查询已取消")
    @RequestMapping(method= RequestMethod.POST , value="/quotes/{quote_id}/salesorders/searchcancel")
	public ResponseEntity<Page<SalesOrderDTO>> searchSalesOrderCancelByQuote(@PathVariable("quote_id") String quote_id, @RequestBody SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchCancel(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据报价单获取DEFAULT", tags = {"订单" } ,notes = "根据报价单获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/quotes/{quote_id}/salesorders/fetchdefault")
	public ResponseEntity<List<SalesOrderDTO>> fetchSalesOrderDefaultByQuote(@PathVariable("quote_id") String quote_id,SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchDefault(context) ;
        List<SalesOrderDTO> list = salesorderMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据报价单查询DEFAULT", tags = {"订单" } ,notes = "根据报价单查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/quotes/{quote_id}/salesorders/searchdefault")
	public ResponseEntity<Page<SalesOrderDTO>> searchSalesOrderDefaultByQuote(@PathVariable("quote_id") String quote_id, @RequestBody SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchFinish-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据报价单获取已完成", tags = {"订单" } ,notes = "根据报价单获取已完成")
    @RequestMapping(method= RequestMethod.GET , value="/quotes/{quote_id}/salesorders/fetchfinish")
	public ResponseEntity<List<SalesOrderDTO>> fetchSalesOrderFinishByQuote(@PathVariable("quote_id") String quote_id,SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchFinish(context) ;
        List<SalesOrderDTO> list = salesorderMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchFinish-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据报价单查询已完成", tags = {"订单" } ,notes = "根据报价单查询已完成")
    @RequestMapping(method= RequestMethod.POST , value="/quotes/{quote_id}/salesorders/searchfinish")
	public ResponseEntity<Page<SalesOrderDTO>> searchSalesOrderFinishByQuote(@PathVariable("quote_id") String quote_id, @RequestBody SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchFinish(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchInvoiced-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据报价单获取已开发票", tags = {"订单" } ,notes = "根据报价单获取已开发票")
    @RequestMapping(method= RequestMethod.GET , value="/quotes/{quote_id}/salesorders/fetchinvoiced")
	public ResponseEntity<List<SalesOrderDTO>> fetchSalesOrderInvoicedByQuote(@PathVariable("quote_id") String quote_id,SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchInvoiced(context) ;
        List<SalesOrderDTO> list = salesorderMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchInvoiced-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据报价单查询已开发票", tags = {"订单" } ,notes = "根据报价单查询已开发票")
    @RequestMapping(method= RequestMethod.POST , value="/quotes/{quote_id}/salesorders/searchinvoiced")
	public ResponseEntity<Page<SalesOrderDTO>> searchSalesOrderInvoicedByQuote(@PathVariable("quote_id") String quote_id, @RequestBody SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchInvoiced(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.salesorderMapping.toDomain(#salesorderdto),'iBizBusinessCentral-SalesOrder-Create')")
    @ApiOperation(value = "根据商机报价单建立订单", tags = {"订单" },  notes = "根据商机报价单建立订单")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders")
    public ResponseEntity<SalesOrderDTO> createByOpportunityQuote(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody SalesOrderDTO salesorderdto) {
        SalesOrder domain = salesorderMapping.toDomain(salesorderdto);
        domain.setQuoteid(quote_id);
		salesorderService.create(domain);
        SalesOrderDTO dto = salesorderMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.salesorderMapping.toDomain(#salesorderdtos),'iBizBusinessCentral-SalesOrder-Create')")
    @ApiOperation(value = "根据商机报价单批量建立订单", tags = {"订单" },  notes = "根据商机报价单批量建立订单")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/batch")
    public ResponseEntity<Boolean> createBatchByOpportunityQuote(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody List<SalesOrderDTO> salesorderdtos) {
        List<SalesOrder> domainlist=salesorderMapping.toDomain(salesorderdtos);
        for(SalesOrder domain:domainlist){
            domain.setQuoteid(quote_id);
        }
        salesorderService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "salesorder" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.salesorderService.get(#salesorder_id),'iBizBusinessCentral-SalesOrder-Update')")
    @ApiOperation(value = "根据商机报价单更新订单", tags = {"订单" },  notes = "根据商机报价单更新订单")
	@RequestMapping(method = RequestMethod.PUT, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}")
    public ResponseEntity<SalesOrderDTO> updateByOpportunityQuote(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDTO salesorderdto) {
        SalesOrder domain = salesorderMapping.toDomain(salesorderdto);
        domain.setQuoteid(quote_id);
        domain.setSalesorderid(salesorder_id);
		salesorderService.update(domain);
        SalesOrderDTO dto = salesorderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.salesorderService.getSalesorderByEntities(this.salesorderMapping.toDomain(#salesorderdtos)),'iBizBusinessCentral-SalesOrder-Update')")
    @ApiOperation(value = "根据商机报价单批量更新订单", tags = {"订单" },  notes = "根据商机报价单批量更新订单")
	@RequestMapping(method = RequestMethod.PUT, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/batch")
    public ResponseEntity<Boolean> updateBatchByOpportunityQuote(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody List<SalesOrderDTO> salesorderdtos) {
        List<SalesOrder> domainlist=salesorderMapping.toDomain(salesorderdtos);
        for(SalesOrder domain:domainlist){
            domain.setQuoteid(quote_id);
        }
        salesorderService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.salesorderService.get(#salesorder_id),'iBizBusinessCentral-SalesOrder-Remove')")
    @ApiOperation(value = "根据商机报价单删除订单", tags = {"订单" },  notes = "根据商机报价单删除订单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}")
    public ResponseEntity<Boolean> removeByOpportunityQuote(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id) {
		return ResponseEntity.status(HttpStatus.OK).body(salesorderService.remove(salesorder_id));
    }

    @PreAuthorize("hasPermission(this.salesorderService.getSalesorderByIds(#ids),'iBizBusinessCentral-SalesOrder-Remove')")
    @ApiOperation(value = "根据商机报价单批量删除订单", tags = {"订单" },  notes = "根据商机报价单批量删除订单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/batch")
    public ResponseEntity<Boolean> removeBatchByOpportunityQuote(@RequestBody List<String> ids) {
        salesorderService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.salesorderMapping.toDomain(returnObject.body),'iBizBusinessCentral-SalesOrder-Get')")
    @ApiOperation(value = "根据商机报价单获取订单", tags = {"订单" },  notes = "根据商机报价单获取订单")
	@RequestMapping(method = RequestMethod.GET, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}")
    public ResponseEntity<SalesOrderDTO> getByOpportunityQuote(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id) {
        SalesOrder domain = salesorderService.get(salesorder_id);
        SalesOrderDTO dto = salesorderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据商机报价单获取订单草稿", tags = {"订单" },  notes = "根据商机报价单获取订单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/getdraft")
    public ResponseEntity<SalesOrderDTO> getDraftByOpportunityQuote(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id) {
        SalesOrder domain = new SalesOrder();
        domain.setQuoteid(quote_id);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderMapping.toDto(salesorderService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-Cancel-all')")
    @ApiOperation(value = "根据商机报价单订单", tags = {"订单" },  notes = "根据商机报价单订单")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/cancel")
    public ResponseEntity<SalesOrderDTO> cancelByOpportunityQuote(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDTO salesorderdto) {
        SalesOrder domain = salesorderMapping.toDomain(salesorderdto);
        domain.setQuoteid(quote_id);
        domain = salesorderService.cancel(domain) ;
        salesorderdto = salesorderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderdto);
    }

    @ApiOperation(value = "根据商机报价单检查订单", tags = {"订单" },  notes = "根据商机报价单检查订单")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/checkkey")
    public ResponseEntity<Boolean> checkKeyByOpportunityQuote(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody SalesOrderDTO salesorderdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(salesorderService.checkKey(salesorderMapping.toDomain(salesorderdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-Finish-all')")
    @ApiOperation(value = "根据商机报价单订单", tags = {"订单" },  notes = "根据商机报价单订单")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/finish")
    public ResponseEntity<SalesOrderDTO> finishByOpportunityQuote(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDTO salesorderdto) {
        SalesOrder domain = salesorderMapping.toDomain(salesorderdto);
        domain.setQuoteid(quote_id);
        domain = salesorderService.finish(domain) ;
        salesorderdto = salesorderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-GenInvoice-all')")
    @ApiOperation(value = "根据商机报价单订单", tags = {"订单" },  notes = "根据商机报价单订单")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/geninvoice")
    public ResponseEntity<SalesOrderDTO> genInvoiceByOpportunityQuote(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDTO salesorderdto) {
        SalesOrder domain = salesorderMapping.toDomain(salesorderdto);
        domain.setQuoteid(quote_id);
        domain = salesorderService.genInvoice(domain) ;
        salesorderdto = salesorderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderdto);
    }

    @PreAuthorize("hasPermission(this.salesorderMapping.toDomain(#salesorderdto),'iBizBusinessCentral-SalesOrder-Save')")
    @ApiOperation(value = "根据商机报价单保存订单", tags = {"订单" },  notes = "根据商机报价单保存订单")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/save")
    public ResponseEntity<Boolean> saveByOpportunityQuote(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody SalesOrderDTO salesorderdto) {
        SalesOrder domain = salesorderMapping.toDomain(salesorderdto);
        domain.setQuoteid(quote_id);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderService.save(domain));
    }

    @PreAuthorize("hasPermission(this.salesorderMapping.toDomain(#salesorderdtos),'iBizBusinessCentral-SalesOrder-Save')")
    @ApiOperation(value = "根据商机报价单批量保存订单", tags = {"订单" },  notes = "根据商机报价单批量保存订单")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/savebatch")
    public ResponseEntity<Boolean> saveBatchByOpportunityQuote(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody List<SalesOrderDTO> salesorderdtos) {
        List<SalesOrder> domainlist=salesorderMapping.toDomain(salesorderdtos);
        for(SalesOrder domain:domainlist){
             domain.setQuoteid(quote_id);
        }
        salesorderService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据商机报价单获取ByParentKey", tags = {"订单" } ,notes = "根据商机报价单获取ByParentKey")
    @RequestMapping(method= RequestMethod.GET , value="/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/fetchbyparentkey")
	public ResponseEntity<List<SalesOrderDTO>> fetchSalesOrderByParentKeyByOpportunityQuote(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id,SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchByParentKey(context) ;
        List<SalesOrderDTO> list = salesorderMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据商机报价单查询ByParentKey", tags = {"订单" } ,notes = "根据商机报价单查询ByParentKey")
    @RequestMapping(method= RequestMethod.POST , value="/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/searchbyparentkey")
	public ResponseEntity<Page<SalesOrderDTO>> searchSalesOrderByParentKeyByOpportunityQuote(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchByParentKey(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchCancel-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据商机报价单获取已取消", tags = {"订单" } ,notes = "根据商机报价单获取已取消")
    @RequestMapping(method= RequestMethod.GET , value="/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/fetchcancel")
	public ResponseEntity<List<SalesOrderDTO>> fetchSalesOrderCancelByOpportunityQuote(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id,SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchCancel(context) ;
        List<SalesOrderDTO> list = salesorderMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchCancel-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据商机报价单查询已取消", tags = {"订单" } ,notes = "根据商机报价单查询已取消")
    @RequestMapping(method= RequestMethod.POST , value="/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/searchcancel")
	public ResponseEntity<Page<SalesOrderDTO>> searchSalesOrderCancelByOpportunityQuote(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchCancel(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据商机报价单获取DEFAULT", tags = {"订单" } ,notes = "根据商机报价单获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/fetchdefault")
	public ResponseEntity<List<SalesOrderDTO>> fetchSalesOrderDefaultByOpportunityQuote(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id,SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchDefault(context) ;
        List<SalesOrderDTO> list = salesorderMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据商机报价单查询DEFAULT", tags = {"订单" } ,notes = "根据商机报价单查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/searchdefault")
	public ResponseEntity<Page<SalesOrderDTO>> searchSalesOrderDefaultByOpportunityQuote(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchFinish-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据商机报价单获取已完成", tags = {"订单" } ,notes = "根据商机报价单获取已完成")
    @RequestMapping(method= RequestMethod.GET , value="/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/fetchfinish")
	public ResponseEntity<List<SalesOrderDTO>> fetchSalesOrderFinishByOpportunityQuote(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id,SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchFinish(context) ;
        List<SalesOrderDTO> list = salesorderMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchFinish-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据商机报价单查询已完成", tags = {"订单" } ,notes = "根据商机报价单查询已完成")
    @RequestMapping(method= RequestMethod.POST , value="/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/searchfinish")
	public ResponseEntity<Page<SalesOrderDTO>> searchSalesOrderFinishByOpportunityQuote(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchFinish(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchInvoiced-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据商机报价单获取已开发票", tags = {"订单" } ,notes = "根据商机报价单获取已开发票")
    @RequestMapping(method= RequestMethod.GET , value="/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/fetchinvoiced")
	public ResponseEntity<List<SalesOrderDTO>> fetchSalesOrderInvoicedByOpportunityQuote(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id,SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchInvoiced(context) ;
        List<SalesOrderDTO> list = salesorderMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchInvoiced-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据商机报价单查询已开发票", tags = {"订单" } ,notes = "根据商机报价单查询已开发票")
    @RequestMapping(method= RequestMethod.POST , value="/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/searchinvoiced")
	public ResponseEntity<Page<SalesOrderDTO>> searchSalesOrderInvoicedByOpportunityQuote(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchInvoiced(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.salesorderMapping.toDomain(#salesorderdto),'iBizBusinessCentral-SalesOrder-Create')")
    @ApiOperation(value = "根据客户商机报价单建立订单", tags = {"订单" },  notes = "根据客户商机报价单建立订单")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders")
    public ResponseEntity<SalesOrderDTO> createByAccountOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody SalesOrderDTO salesorderdto) {
        SalesOrder domain = salesorderMapping.toDomain(salesorderdto);
        domain.setQuoteid(quote_id);
		salesorderService.create(domain);
        SalesOrderDTO dto = salesorderMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.salesorderMapping.toDomain(#salesorderdtos),'iBizBusinessCentral-SalesOrder-Create')")
    @ApiOperation(value = "根据客户商机报价单批量建立订单", tags = {"订单" },  notes = "根据客户商机报价单批量建立订单")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/batch")
    public ResponseEntity<Boolean> createBatchByAccountOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody List<SalesOrderDTO> salesorderdtos) {
        List<SalesOrder> domainlist=salesorderMapping.toDomain(salesorderdtos);
        for(SalesOrder domain:domainlist){
            domain.setQuoteid(quote_id);
        }
        salesorderService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "salesorder" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.salesorderService.get(#salesorder_id),'iBizBusinessCentral-SalesOrder-Update')")
    @ApiOperation(value = "根据客户商机报价单更新订单", tags = {"订单" },  notes = "根据客户商机报价单更新订单")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}")
    public ResponseEntity<SalesOrderDTO> updateByAccountOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDTO salesorderdto) {
        SalesOrder domain = salesorderMapping.toDomain(salesorderdto);
        domain.setQuoteid(quote_id);
        domain.setSalesorderid(salesorder_id);
		salesorderService.update(domain);
        SalesOrderDTO dto = salesorderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.salesorderService.getSalesorderByEntities(this.salesorderMapping.toDomain(#salesorderdtos)),'iBizBusinessCentral-SalesOrder-Update')")
    @ApiOperation(value = "根据客户商机报价单批量更新订单", tags = {"订单" },  notes = "根据客户商机报价单批量更新订单")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/batch")
    public ResponseEntity<Boolean> updateBatchByAccountOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody List<SalesOrderDTO> salesorderdtos) {
        List<SalesOrder> domainlist=salesorderMapping.toDomain(salesorderdtos);
        for(SalesOrder domain:domainlist){
            domain.setQuoteid(quote_id);
        }
        salesorderService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.salesorderService.get(#salesorder_id),'iBizBusinessCentral-SalesOrder-Remove')")
    @ApiOperation(value = "根据客户商机报价单删除订单", tags = {"订单" },  notes = "根据客户商机报价单删除订单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}")
    public ResponseEntity<Boolean> removeByAccountOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id) {
		return ResponseEntity.status(HttpStatus.OK).body(salesorderService.remove(salesorder_id));
    }

    @PreAuthorize("hasPermission(this.salesorderService.getSalesorderByIds(#ids),'iBizBusinessCentral-SalesOrder-Remove')")
    @ApiOperation(value = "根据客户商机报价单批量删除订单", tags = {"订单" },  notes = "根据客户商机报价单批量删除订单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/batch")
    public ResponseEntity<Boolean> removeBatchByAccountOpportunityQuote(@RequestBody List<String> ids) {
        salesorderService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.salesorderMapping.toDomain(returnObject.body),'iBizBusinessCentral-SalesOrder-Get')")
    @ApiOperation(value = "根据客户商机报价单获取订单", tags = {"订单" },  notes = "根据客户商机报价单获取订单")
	@RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}")
    public ResponseEntity<SalesOrderDTO> getByAccountOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id) {
        SalesOrder domain = salesorderService.get(salesorder_id);
        SalesOrderDTO dto = salesorderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据客户商机报价单获取订单草稿", tags = {"订单" },  notes = "根据客户商机报价单获取订单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/getdraft")
    public ResponseEntity<SalesOrderDTO> getDraftByAccountOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id) {
        SalesOrder domain = new SalesOrder();
        domain.setQuoteid(quote_id);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderMapping.toDto(salesorderService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-Cancel-all')")
    @ApiOperation(value = "根据客户商机报价单订单", tags = {"订单" },  notes = "根据客户商机报价单订单")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/cancel")
    public ResponseEntity<SalesOrderDTO> cancelByAccountOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDTO salesorderdto) {
        SalesOrder domain = salesorderMapping.toDomain(salesorderdto);
        domain.setQuoteid(quote_id);
        domain = salesorderService.cancel(domain) ;
        salesorderdto = salesorderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderdto);
    }

    @ApiOperation(value = "根据客户商机报价单检查订单", tags = {"订单" },  notes = "根据客户商机报价单检查订单")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/checkkey")
    public ResponseEntity<Boolean> checkKeyByAccountOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody SalesOrderDTO salesorderdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(salesorderService.checkKey(salesorderMapping.toDomain(salesorderdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-Finish-all')")
    @ApiOperation(value = "根据客户商机报价单订单", tags = {"订单" },  notes = "根据客户商机报价单订单")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/finish")
    public ResponseEntity<SalesOrderDTO> finishByAccountOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDTO salesorderdto) {
        SalesOrder domain = salesorderMapping.toDomain(salesorderdto);
        domain.setQuoteid(quote_id);
        domain = salesorderService.finish(domain) ;
        salesorderdto = salesorderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-GenInvoice-all')")
    @ApiOperation(value = "根据客户商机报价单订单", tags = {"订单" },  notes = "根据客户商机报价单订单")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/geninvoice")
    public ResponseEntity<SalesOrderDTO> genInvoiceByAccountOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDTO salesorderdto) {
        SalesOrder domain = salesorderMapping.toDomain(salesorderdto);
        domain.setQuoteid(quote_id);
        domain = salesorderService.genInvoice(domain) ;
        salesorderdto = salesorderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderdto);
    }

    @PreAuthorize("hasPermission(this.salesorderMapping.toDomain(#salesorderdto),'iBizBusinessCentral-SalesOrder-Save')")
    @ApiOperation(value = "根据客户商机报价单保存订单", tags = {"订单" },  notes = "根据客户商机报价单保存订单")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/save")
    public ResponseEntity<Boolean> saveByAccountOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody SalesOrderDTO salesorderdto) {
        SalesOrder domain = salesorderMapping.toDomain(salesorderdto);
        domain.setQuoteid(quote_id);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderService.save(domain));
    }

    @PreAuthorize("hasPermission(this.salesorderMapping.toDomain(#salesorderdtos),'iBizBusinessCentral-SalesOrder-Save')")
    @ApiOperation(value = "根据客户商机报价单批量保存订单", tags = {"订单" },  notes = "根据客户商机报价单批量保存订单")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/savebatch")
    public ResponseEntity<Boolean> saveBatchByAccountOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody List<SalesOrderDTO> salesorderdtos) {
        List<SalesOrder> domainlist=salesorderMapping.toDomain(salesorderdtos);
        for(SalesOrder domain:domainlist){
             domain.setQuoteid(quote_id);
        }
        salesorderService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据客户商机报价单获取ByParentKey", tags = {"订单" } ,notes = "根据客户商机报价单获取ByParentKey")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/fetchbyparentkey")
	public ResponseEntity<List<SalesOrderDTO>> fetchSalesOrderByParentKeyByAccountOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id,SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchByParentKey(context) ;
        List<SalesOrderDTO> list = salesorderMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据客户商机报价单查询ByParentKey", tags = {"订单" } ,notes = "根据客户商机报价单查询ByParentKey")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/searchbyparentkey")
	public ResponseEntity<Page<SalesOrderDTO>> searchSalesOrderByParentKeyByAccountOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchByParentKey(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchCancel-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据客户商机报价单获取已取消", tags = {"订单" } ,notes = "根据客户商机报价单获取已取消")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/fetchcancel")
	public ResponseEntity<List<SalesOrderDTO>> fetchSalesOrderCancelByAccountOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id,SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchCancel(context) ;
        List<SalesOrderDTO> list = salesorderMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchCancel-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据客户商机报价单查询已取消", tags = {"订单" } ,notes = "根据客户商机报价单查询已取消")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/searchcancel")
	public ResponseEntity<Page<SalesOrderDTO>> searchSalesOrderCancelByAccountOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchCancel(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据客户商机报价单获取DEFAULT", tags = {"订单" } ,notes = "根据客户商机报价单获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/fetchdefault")
	public ResponseEntity<List<SalesOrderDTO>> fetchSalesOrderDefaultByAccountOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id,SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchDefault(context) ;
        List<SalesOrderDTO> list = salesorderMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据客户商机报价单查询DEFAULT", tags = {"订单" } ,notes = "根据客户商机报价单查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/searchdefault")
	public ResponseEntity<Page<SalesOrderDTO>> searchSalesOrderDefaultByAccountOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchFinish-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据客户商机报价单获取已完成", tags = {"订单" } ,notes = "根据客户商机报价单获取已完成")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/fetchfinish")
	public ResponseEntity<List<SalesOrderDTO>> fetchSalesOrderFinishByAccountOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id,SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchFinish(context) ;
        List<SalesOrderDTO> list = salesorderMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchFinish-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据客户商机报价单查询已完成", tags = {"订单" } ,notes = "根据客户商机报价单查询已完成")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/searchfinish")
	public ResponseEntity<Page<SalesOrderDTO>> searchSalesOrderFinishByAccountOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchFinish(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchInvoiced-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据客户商机报价单获取已开发票", tags = {"订单" } ,notes = "根据客户商机报价单获取已开发票")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/fetchinvoiced")
	public ResponseEntity<List<SalesOrderDTO>> fetchSalesOrderInvoicedByAccountOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id,SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchInvoiced(context) ;
        List<SalesOrderDTO> list = salesorderMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchInvoiced-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据客户商机报价单查询已开发票", tags = {"订单" } ,notes = "根据客户商机报价单查询已开发票")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/searchinvoiced")
	public ResponseEntity<Page<SalesOrderDTO>> searchSalesOrderInvoicedByAccountOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchInvoiced(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.salesorderMapping.toDomain(#salesorderdto),'iBizBusinessCentral-SalesOrder-Create')")
    @ApiOperation(value = "根据联系人商机报价单建立订单", tags = {"订单" },  notes = "根据联系人商机报价单建立订单")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders")
    public ResponseEntity<SalesOrderDTO> createByContactOpportunityQuote(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody SalesOrderDTO salesorderdto) {
        SalesOrder domain = salesorderMapping.toDomain(salesorderdto);
        domain.setQuoteid(quote_id);
		salesorderService.create(domain);
        SalesOrderDTO dto = salesorderMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.salesorderMapping.toDomain(#salesorderdtos),'iBizBusinessCentral-SalesOrder-Create')")
    @ApiOperation(value = "根据联系人商机报价单批量建立订单", tags = {"订单" },  notes = "根据联系人商机报价单批量建立订单")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/batch")
    public ResponseEntity<Boolean> createBatchByContactOpportunityQuote(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody List<SalesOrderDTO> salesorderdtos) {
        List<SalesOrder> domainlist=salesorderMapping.toDomain(salesorderdtos);
        for(SalesOrder domain:domainlist){
            domain.setQuoteid(quote_id);
        }
        salesorderService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "salesorder" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.salesorderService.get(#salesorder_id),'iBizBusinessCentral-SalesOrder-Update')")
    @ApiOperation(value = "根据联系人商机报价单更新订单", tags = {"订单" },  notes = "根据联系人商机报价单更新订单")
	@RequestMapping(method = RequestMethod.PUT, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}")
    public ResponseEntity<SalesOrderDTO> updateByContactOpportunityQuote(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDTO salesorderdto) {
        SalesOrder domain = salesorderMapping.toDomain(salesorderdto);
        domain.setQuoteid(quote_id);
        domain.setSalesorderid(salesorder_id);
		salesorderService.update(domain);
        SalesOrderDTO dto = salesorderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.salesorderService.getSalesorderByEntities(this.salesorderMapping.toDomain(#salesorderdtos)),'iBizBusinessCentral-SalesOrder-Update')")
    @ApiOperation(value = "根据联系人商机报价单批量更新订单", tags = {"订单" },  notes = "根据联系人商机报价单批量更新订单")
	@RequestMapping(method = RequestMethod.PUT, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/batch")
    public ResponseEntity<Boolean> updateBatchByContactOpportunityQuote(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody List<SalesOrderDTO> salesorderdtos) {
        List<SalesOrder> domainlist=salesorderMapping.toDomain(salesorderdtos);
        for(SalesOrder domain:domainlist){
            domain.setQuoteid(quote_id);
        }
        salesorderService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.salesorderService.get(#salesorder_id),'iBizBusinessCentral-SalesOrder-Remove')")
    @ApiOperation(value = "根据联系人商机报价单删除订单", tags = {"订单" },  notes = "根据联系人商机报价单删除订单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}")
    public ResponseEntity<Boolean> removeByContactOpportunityQuote(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id) {
		return ResponseEntity.status(HttpStatus.OK).body(salesorderService.remove(salesorder_id));
    }

    @PreAuthorize("hasPermission(this.salesorderService.getSalesorderByIds(#ids),'iBizBusinessCentral-SalesOrder-Remove')")
    @ApiOperation(value = "根据联系人商机报价单批量删除订单", tags = {"订单" },  notes = "根据联系人商机报价单批量删除订单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/batch")
    public ResponseEntity<Boolean> removeBatchByContactOpportunityQuote(@RequestBody List<String> ids) {
        salesorderService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.salesorderMapping.toDomain(returnObject.body),'iBizBusinessCentral-SalesOrder-Get')")
    @ApiOperation(value = "根据联系人商机报价单获取订单", tags = {"订单" },  notes = "根据联系人商机报价单获取订单")
	@RequestMapping(method = RequestMethod.GET, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}")
    public ResponseEntity<SalesOrderDTO> getByContactOpportunityQuote(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id) {
        SalesOrder domain = salesorderService.get(salesorder_id);
        SalesOrderDTO dto = salesorderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据联系人商机报价单获取订单草稿", tags = {"订单" },  notes = "根据联系人商机报价单获取订单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/getdraft")
    public ResponseEntity<SalesOrderDTO> getDraftByContactOpportunityQuote(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id) {
        SalesOrder domain = new SalesOrder();
        domain.setQuoteid(quote_id);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderMapping.toDto(salesorderService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-Cancel-all')")
    @ApiOperation(value = "根据联系人商机报价单订单", tags = {"订单" },  notes = "根据联系人商机报价单订单")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/cancel")
    public ResponseEntity<SalesOrderDTO> cancelByContactOpportunityQuote(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDTO salesorderdto) {
        SalesOrder domain = salesorderMapping.toDomain(salesorderdto);
        domain.setQuoteid(quote_id);
        domain = salesorderService.cancel(domain) ;
        salesorderdto = salesorderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderdto);
    }

    @ApiOperation(value = "根据联系人商机报价单检查订单", tags = {"订单" },  notes = "根据联系人商机报价单检查订单")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/checkkey")
    public ResponseEntity<Boolean> checkKeyByContactOpportunityQuote(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody SalesOrderDTO salesorderdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(salesorderService.checkKey(salesorderMapping.toDomain(salesorderdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-Finish-all')")
    @ApiOperation(value = "根据联系人商机报价单订单", tags = {"订单" },  notes = "根据联系人商机报价单订单")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/finish")
    public ResponseEntity<SalesOrderDTO> finishByContactOpportunityQuote(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDTO salesorderdto) {
        SalesOrder domain = salesorderMapping.toDomain(salesorderdto);
        domain.setQuoteid(quote_id);
        domain = salesorderService.finish(domain) ;
        salesorderdto = salesorderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-GenInvoice-all')")
    @ApiOperation(value = "根据联系人商机报价单订单", tags = {"订单" },  notes = "根据联系人商机报价单订单")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/geninvoice")
    public ResponseEntity<SalesOrderDTO> genInvoiceByContactOpportunityQuote(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDTO salesorderdto) {
        SalesOrder domain = salesorderMapping.toDomain(salesorderdto);
        domain.setQuoteid(quote_id);
        domain = salesorderService.genInvoice(domain) ;
        salesorderdto = salesorderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderdto);
    }

    @PreAuthorize("hasPermission(this.salesorderMapping.toDomain(#salesorderdto),'iBizBusinessCentral-SalesOrder-Save')")
    @ApiOperation(value = "根据联系人商机报价单保存订单", tags = {"订单" },  notes = "根据联系人商机报价单保存订单")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/save")
    public ResponseEntity<Boolean> saveByContactOpportunityQuote(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody SalesOrderDTO salesorderdto) {
        SalesOrder domain = salesorderMapping.toDomain(salesorderdto);
        domain.setQuoteid(quote_id);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderService.save(domain));
    }

    @PreAuthorize("hasPermission(this.salesorderMapping.toDomain(#salesorderdtos),'iBizBusinessCentral-SalesOrder-Save')")
    @ApiOperation(value = "根据联系人商机报价单批量保存订单", tags = {"订单" },  notes = "根据联系人商机报价单批量保存订单")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/savebatch")
    public ResponseEntity<Boolean> saveBatchByContactOpportunityQuote(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody List<SalesOrderDTO> salesorderdtos) {
        List<SalesOrder> domainlist=salesorderMapping.toDomain(salesorderdtos);
        for(SalesOrder domain:domainlist){
             domain.setQuoteid(quote_id);
        }
        salesorderService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据联系人商机报价单获取ByParentKey", tags = {"订单" } ,notes = "根据联系人商机报价单获取ByParentKey")
    @RequestMapping(method= RequestMethod.GET , value="/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/fetchbyparentkey")
	public ResponseEntity<List<SalesOrderDTO>> fetchSalesOrderByParentKeyByContactOpportunityQuote(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id,SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchByParentKey(context) ;
        List<SalesOrderDTO> list = salesorderMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据联系人商机报价单查询ByParentKey", tags = {"订单" } ,notes = "根据联系人商机报价单查询ByParentKey")
    @RequestMapping(method= RequestMethod.POST , value="/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/searchbyparentkey")
	public ResponseEntity<Page<SalesOrderDTO>> searchSalesOrderByParentKeyByContactOpportunityQuote(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchByParentKey(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchCancel-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据联系人商机报价单获取已取消", tags = {"订单" } ,notes = "根据联系人商机报价单获取已取消")
    @RequestMapping(method= RequestMethod.GET , value="/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/fetchcancel")
	public ResponseEntity<List<SalesOrderDTO>> fetchSalesOrderCancelByContactOpportunityQuote(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id,SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchCancel(context) ;
        List<SalesOrderDTO> list = salesorderMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchCancel-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据联系人商机报价单查询已取消", tags = {"订单" } ,notes = "根据联系人商机报价单查询已取消")
    @RequestMapping(method= RequestMethod.POST , value="/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/searchcancel")
	public ResponseEntity<Page<SalesOrderDTO>> searchSalesOrderCancelByContactOpportunityQuote(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchCancel(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据联系人商机报价单获取DEFAULT", tags = {"订单" } ,notes = "根据联系人商机报价单获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/fetchdefault")
	public ResponseEntity<List<SalesOrderDTO>> fetchSalesOrderDefaultByContactOpportunityQuote(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id,SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchDefault(context) ;
        List<SalesOrderDTO> list = salesorderMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据联系人商机报价单查询DEFAULT", tags = {"订单" } ,notes = "根据联系人商机报价单查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/searchdefault")
	public ResponseEntity<Page<SalesOrderDTO>> searchSalesOrderDefaultByContactOpportunityQuote(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchFinish-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据联系人商机报价单获取已完成", tags = {"订单" } ,notes = "根据联系人商机报价单获取已完成")
    @RequestMapping(method= RequestMethod.GET , value="/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/fetchfinish")
	public ResponseEntity<List<SalesOrderDTO>> fetchSalesOrderFinishByContactOpportunityQuote(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id,SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchFinish(context) ;
        List<SalesOrderDTO> list = salesorderMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchFinish-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据联系人商机报价单查询已完成", tags = {"订单" } ,notes = "根据联系人商机报价单查询已完成")
    @RequestMapping(method= RequestMethod.POST , value="/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/searchfinish")
	public ResponseEntity<Page<SalesOrderDTO>> searchSalesOrderFinishByContactOpportunityQuote(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchFinish(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchInvoiced-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据联系人商机报价单获取已开发票", tags = {"订单" } ,notes = "根据联系人商机报价单获取已开发票")
    @RequestMapping(method= RequestMethod.GET , value="/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/fetchinvoiced")
	public ResponseEntity<List<SalesOrderDTO>> fetchSalesOrderInvoicedByContactOpportunityQuote(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id,SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchInvoiced(context) ;
        List<SalesOrderDTO> list = salesorderMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchInvoiced-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据联系人商机报价单查询已开发票", tags = {"订单" } ,notes = "根据联系人商机报价单查询已开发票")
    @RequestMapping(method= RequestMethod.POST , value="/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/searchinvoiced")
	public ResponseEntity<Page<SalesOrderDTO>> searchSalesOrderInvoicedByContactOpportunityQuote(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchInvoiced(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.salesorderMapping.toDomain(#salesorderdto),'iBizBusinessCentral-SalesOrder-Create')")
    @ApiOperation(value = "根据客户联系人商机报价单建立订单", tags = {"订单" },  notes = "根据客户联系人商机报价单建立订单")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders")
    public ResponseEntity<SalesOrderDTO> createByAccountContactOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody SalesOrderDTO salesorderdto) {
        SalesOrder domain = salesorderMapping.toDomain(salesorderdto);
        domain.setQuoteid(quote_id);
		salesorderService.create(domain);
        SalesOrderDTO dto = salesorderMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.salesorderMapping.toDomain(#salesorderdtos),'iBizBusinessCentral-SalesOrder-Create')")
    @ApiOperation(value = "根据客户联系人商机报价单批量建立订单", tags = {"订单" },  notes = "根据客户联系人商机报价单批量建立订单")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/batch")
    public ResponseEntity<Boolean> createBatchByAccountContactOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody List<SalesOrderDTO> salesorderdtos) {
        List<SalesOrder> domainlist=salesorderMapping.toDomain(salesorderdtos);
        for(SalesOrder domain:domainlist){
            domain.setQuoteid(quote_id);
        }
        salesorderService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "salesorder" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.salesorderService.get(#salesorder_id),'iBizBusinessCentral-SalesOrder-Update')")
    @ApiOperation(value = "根据客户联系人商机报价单更新订单", tags = {"订单" },  notes = "根据客户联系人商机报价单更新订单")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}")
    public ResponseEntity<SalesOrderDTO> updateByAccountContactOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDTO salesorderdto) {
        SalesOrder domain = salesorderMapping.toDomain(salesorderdto);
        domain.setQuoteid(quote_id);
        domain.setSalesorderid(salesorder_id);
		salesorderService.update(domain);
        SalesOrderDTO dto = salesorderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.salesorderService.getSalesorderByEntities(this.salesorderMapping.toDomain(#salesorderdtos)),'iBizBusinessCentral-SalesOrder-Update')")
    @ApiOperation(value = "根据客户联系人商机报价单批量更新订单", tags = {"订单" },  notes = "根据客户联系人商机报价单批量更新订单")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/batch")
    public ResponseEntity<Boolean> updateBatchByAccountContactOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody List<SalesOrderDTO> salesorderdtos) {
        List<SalesOrder> domainlist=salesorderMapping.toDomain(salesorderdtos);
        for(SalesOrder domain:domainlist){
            domain.setQuoteid(quote_id);
        }
        salesorderService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.salesorderService.get(#salesorder_id),'iBizBusinessCentral-SalesOrder-Remove')")
    @ApiOperation(value = "根据客户联系人商机报价单删除订单", tags = {"订单" },  notes = "根据客户联系人商机报价单删除订单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}")
    public ResponseEntity<Boolean> removeByAccountContactOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id) {
		return ResponseEntity.status(HttpStatus.OK).body(salesorderService.remove(salesorder_id));
    }

    @PreAuthorize("hasPermission(this.salesorderService.getSalesorderByIds(#ids),'iBizBusinessCentral-SalesOrder-Remove')")
    @ApiOperation(value = "根据客户联系人商机报价单批量删除订单", tags = {"订单" },  notes = "根据客户联系人商机报价单批量删除订单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/batch")
    public ResponseEntity<Boolean> removeBatchByAccountContactOpportunityQuote(@RequestBody List<String> ids) {
        salesorderService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.salesorderMapping.toDomain(returnObject.body),'iBizBusinessCentral-SalesOrder-Get')")
    @ApiOperation(value = "根据客户联系人商机报价单获取订单", tags = {"订单" },  notes = "根据客户联系人商机报价单获取订单")
	@RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}")
    public ResponseEntity<SalesOrderDTO> getByAccountContactOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id) {
        SalesOrder domain = salesorderService.get(salesorder_id);
        SalesOrderDTO dto = salesorderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据客户联系人商机报价单获取订单草稿", tags = {"订单" },  notes = "根据客户联系人商机报价单获取订单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/getdraft")
    public ResponseEntity<SalesOrderDTO> getDraftByAccountContactOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id) {
        SalesOrder domain = new SalesOrder();
        domain.setQuoteid(quote_id);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderMapping.toDto(salesorderService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-Cancel-all')")
    @ApiOperation(value = "根据客户联系人商机报价单订单", tags = {"订单" },  notes = "根据客户联系人商机报价单订单")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/cancel")
    public ResponseEntity<SalesOrderDTO> cancelByAccountContactOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDTO salesorderdto) {
        SalesOrder domain = salesorderMapping.toDomain(salesorderdto);
        domain.setQuoteid(quote_id);
        domain = salesorderService.cancel(domain) ;
        salesorderdto = salesorderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderdto);
    }

    @ApiOperation(value = "根据客户联系人商机报价单检查订单", tags = {"订单" },  notes = "根据客户联系人商机报价单检查订单")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/checkkey")
    public ResponseEntity<Boolean> checkKeyByAccountContactOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody SalesOrderDTO salesorderdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(salesorderService.checkKey(salesorderMapping.toDomain(salesorderdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-Finish-all')")
    @ApiOperation(value = "根据客户联系人商机报价单订单", tags = {"订单" },  notes = "根据客户联系人商机报价单订单")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/finish")
    public ResponseEntity<SalesOrderDTO> finishByAccountContactOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDTO salesorderdto) {
        SalesOrder domain = salesorderMapping.toDomain(salesorderdto);
        domain.setQuoteid(quote_id);
        domain = salesorderService.finish(domain) ;
        salesorderdto = salesorderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-GenInvoice-all')")
    @ApiOperation(value = "根据客户联系人商机报价单订单", tags = {"订单" },  notes = "根据客户联系人商机报价单订单")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/geninvoice")
    public ResponseEntity<SalesOrderDTO> genInvoiceByAccountContactOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody SalesOrderDTO salesorderdto) {
        SalesOrder domain = salesorderMapping.toDomain(salesorderdto);
        domain.setQuoteid(quote_id);
        domain = salesorderService.genInvoice(domain) ;
        salesorderdto = salesorderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderdto);
    }

    @PreAuthorize("hasPermission(this.salesorderMapping.toDomain(#salesorderdto),'iBizBusinessCentral-SalesOrder-Save')")
    @ApiOperation(value = "根据客户联系人商机报价单保存订单", tags = {"订单" },  notes = "根据客户联系人商机报价单保存订单")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/save")
    public ResponseEntity<Boolean> saveByAccountContactOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody SalesOrderDTO salesorderdto) {
        SalesOrder domain = salesorderMapping.toDomain(salesorderdto);
        domain.setQuoteid(quote_id);
        return ResponseEntity.status(HttpStatus.OK).body(salesorderService.save(domain));
    }

    @PreAuthorize("hasPermission(this.salesorderMapping.toDomain(#salesorderdtos),'iBizBusinessCentral-SalesOrder-Save')")
    @ApiOperation(value = "根据客户联系人商机报价单批量保存订单", tags = {"订单" },  notes = "根据客户联系人商机报价单批量保存订单")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/savebatch")
    public ResponseEntity<Boolean> saveBatchByAccountContactOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody List<SalesOrderDTO> salesorderdtos) {
        List<SalesOrder> domainlist=salesorderMapping.toDomain(salesorderdtos);
        for(SalesOrder domain:domainlist){
             domain.setQuoteid(quote_id);
        }
        salesorderService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据客户联系人商机报价单获取ByParentKey", tags = {"订单" } ,notes = "根据客户联系人商机报价单获取ByParentKey")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/fetchbyparentkey")
	public ResponseEntity<List<SalesOrderDTO>> fetchSalesOrderByParentKeyByAccountContactOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id,SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchByParentKey(context) ;
        List<SalesOrderDTO> list = salesorderMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据客户联系人商机报价单查询ByParentKey", tags = {"订单" } ,notes = "根据客户联系人商机报价单查询ByParentKey")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/searchbyparentkey")
	public ResponseEntity<Page<SalesOrderDTO>> searchSalesOrderByParentKeyByAccountContactOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchByParentKey(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchCancel-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据客户联系人商机报价单获取已取消", tags = {"订单" } ,notes = "根据客户联系人商机报价单获取已取消")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/fetchcancel")
	public ResponseEntity<List<SalesOrderDTO>> fetchSalesOrderCancelByAccountContactOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id,SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchCancel(context) ;
        List<SalesOrderDTO> list = salesorderMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchCancel-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据客户联系人商机报价单查询已取消", tags = {"订单" } ,notes = "根据客户联系人商机报价单查询已取消")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/searchcancel")
	public ResponseEntity<Page<SalesOrderDTO>> searchSalesOrderCancelByAccountContactOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchCancel(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据客户联系人商机报价单获取DEFAULT", tags = {"订单" } ,notes = "根据客户联系人商机报价单获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/fetchdefault")
	public ResponseEntity<List<SalesOrderDTO>> fetchSalesOrderDefaultByAccountContactOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id,SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchDefault(context) ;
        List<SalesOrderDTO> list = salesorderMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据客户联系人商机报价单查询DEFAULT", tags = {"订单" } ,notes = "根据客户联系人商机报价单查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/searchdefault")
	public ResponseEntity<Page<SalesOrderDTO>> searchSalesOrderDefaultByAccountContactOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchFinish-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据客户联系人商机报价单获取已完成", tags = {"订单" } ,notes = "根据客户联系人商机报价单获取已完成")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/fetchfinish")
	public ResponseEntity<List<SalesOrderDTO>> fetchSalesOrderFinishByAccountContactOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id,SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchFinish(context) ;
        List<SalesOrderDTO> list = salesorderMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchFinish-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据客户联系人商机报价单查询已完成", tags = {"订单" } ,notes = "根据客户联系人商机报价单查询已完成")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/searchfinish")
	public ResponseEntity<Page<SalesOrderDTO>> searchSalesOrderFinishByAccountContactOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchFinish(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchInvoiced-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据客户联系人商机报价单获取已开发票", tags = {"订单" } ,notes = "根据客户联系人商机报价单获取已开发票")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/fetchinvoiced")
	public ResponseEntity<List<SalesOrderDTO>> fetchSalesOrderInvoicedByAccountContactOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id,SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchInvoiced(context) ;
        List<SalesOrderDTO> list = salesorderMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesOrder-searchInvoiced-all') and hasPermission(#context,'iBizBusinessCentral-SalesOrder-Get')")
	@ApiOperation(value = "根据客户联系人商机报价单查询已开发票", tags = {"订单" } ,notes = "根据客户联系人商机报价单查询已开发票")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/searchinvoiced")
	public ResponseEntity<Page<SalesOrderDTO>> searchSalesOrderInvoicedByAccountContactOpportunityQuote(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody SalesOrderSearchContext context) {
        context.setN_quoteid_eq(quote_id);
        Page<SalesOrder> domains = salesorderService.searchInvoiced(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesorderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}


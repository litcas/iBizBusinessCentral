package cn.ibizlab.businesscentral.centralapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[IBizServiceDTO]
 */
@Data
public class IBizServiceDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [TIMEZONERULEVERSIONNUMBER]
     *
     */
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;

    /**
     * 属性 [OVERRIDDENCREATEDON]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;

    /**
     * 属性 [ANCHOROFFSET]
     *
     */
    @JSONField(name = "anchoroffset")
    @JsonProperty("anchoroffset")
    private Integer anchoroffset;

    /**
     * 属性 [INITIALSTATUSCODE]
     *
     */
    @JSONField(name = "initialstatuscode")
    @JsonProperty("initialstatuscode")
    private Integer initialstatuscode;

    /**
     * 属性 [GRANULARITY]
     *
     */
    @JSONField(name = "granularity")
    @JsonProperty("granularity")
    private String granularity;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 属性 [SHOWRESOURCES]
     *
     */
    @JSONField(name = "showresources")
    @JsonProperty("showresources")
    private Integer showresources;

    /**
     * 属性 [SERVICENAME]
     *
     */
    @JSONField(name = "servicename")
    @JsonProperty("servicename")
    private String servicename;

    /**
     * 属性 [IMPORTSEQUENCENUMBER]
     *
     */
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;

    /**
     * 属性 [VISIBLE]
     *
     */
    @JSONField(name = "visible")
    @JsonProperty("visible")
    private Integer visible;

    /**
     * 属性 [CALENDARID]
     *
     */
    @JSONField(name = "calendarid")
    @JsonProperty("calendarid")
    private String calendarid;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;

    /**
     * 属性 [SCHEDULABLE]
     *
     */
    @JSONField(name = "schedulable")
    @JsonProperty("schedulable")
    private Integer schedulable;

    /**
     * 属性 [STRATEGYID]
     *
     */
    @JSONField(name = "strategyid")
    @JsonProperty("strategyid")
    private String strategyid;

    /**
     * 属性 [VERSIONNUMBER]
     *
     */
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;

    /**
     * 属性 [SERVICEID]
     *
     */
    @JSONField(name = "serviceid")
    @JsonProperty("serviceid")
    private String serviceid;

    /**
     * 属性 [DURATION]
     *
     */
    @JSONField(name = "duration")
    @JsonProperty("duration")
    private Integer duration;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [UTCCONVERSIONTIMEZONECODE]
     *
     */
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;

    /**
     * 属性 [RESOURCESPECID]
     *
     */
    @JSONField(name = "resourcespecid")
    @JsonProperty("resourcespecid")
    private String resourcespecid;


    /**
     * 设置 [TIMEZONERULEVERSIONNUMBER]
     */
    public void setTimezoneruleversionnumber(Integer  timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [OVERRIDDENCREATEDON]
     */
    public void setOverriddencreatedon(Timestamp  overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 设置 [ANCHOROFFSET]
     */
    public void setAnchoroffset(Integer  anchoroffset){
        this.anchoroffset = anchoroffset ;
        this.modify("anchoroffset",anchoroffset);
    }

    /**
     * 设置 [INITIALSTATUSCODE]
     */
    public void setInitialstatuscode(Integer  initialstatuscode){
        this.initialstatuscode = initialstatuscode ;
        this.modify("initialstatuscode",initialstatuscode);
    }

    /**
     * 设置 [GRANULARITY]
     */
    public void setGranularity(String  granularity){
        this.granularity = granularity ;
        this.modify("granularity",granularity);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [SHOWRESOURCES]
     */
    public void setShowresources(Integer  showresources){
        this.showresources = showresources ;
        this.modify("showresources",showresources);
    }

    /**
     * 设置 [SERVICENAME]
     */
    public void setServicename(String  servicename){
        this.servicename = servicename ;
        this.modify("servicename",servicename);
    }

    /**
     * 设置 [IMPORTSEQUENCENUMBER]
     */
    public void setImportsequencenumber(Integer  importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [VISIBLE]
     */
    public void setVisible(Integer  visible){
        this.visible = visible ;
        this.modify("visible",visible);
    }

    /**
     * 设置 [CALENDARID]
     */
    public void setCalendarid(String  calendarid){
        this.calendarid = calendarid ;
        this.modify("calendarid",calendarid);
    }

    /**
     * 设置 [SCHEDULABLE]
     */
    public void setSchedulable(Integer  schedulable){
        this.schedulable = schedulable ;
        this.modify("schedulable",schedulable);
    }

    /**
     * 设置 [STRATEGYID]
     */
    public void setStrategyid(String  strategyid){
        this.strategyid = strategyid ;
        this.modify("strategyid",strategyid);
    }

    /**
     * 设置 [VERSIONNUMBER]
     */
    public void setVersionnumber(BigInteger  versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [DURATION]
     */
    public void setDuration(Integer  duration){
        this.duration = duration ;
        this.modify("duration",duration);
    }

    /**
     * 设置 [UTCCONVERSIONTIMEZONECODE]
     */
    public void setUtcconversiontimezonecode(Integer  utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [RESOURCESPECID]
     */
    public void setResourcespecid(String  resourcespecid){
        this.resourcespecid = resourcespecid ;
        this.modify("resourcespecid",resourcespecid);
    }


}


package cn.ibizlab.businesscentral.centralapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[TaskDTO]
 */
@Data
public class TaskDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [WORKFLOWCREATED]
     *
     */
    @JSONField(name = "workflowcreated")
    @JsonProperty("workflowcreated")
    private Integer workflowcreated;

    /**
     * 属性 [REGARDINGOBJECTNAME]
     *
     */
    @JSONField(name = "regardingobjectname")
    @JsonProperty("regardingobjectname")
    private String regardingobjectname;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [ACTUALEND]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "actualend" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("actualend")
    private Timestamp actualend;

    /**
     * 属性 [STATECODE]
     *
     */
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;

    /**
     * 属性 [PRIORITYCODE]
     *
     */
    @JSONField(name = "prioritycode")
    @JsonProperty("prioritycode")
    private String prioritycode;

    /**
     * 属性 [ACTIVITYADDITIONALPARAMS]
     *
     */
    @JSONField(name = "activityadditionalparams")
    @JsonProperty("activityadditionalparams")
    private String activityadditionalparams;

    /**
     * 属性 [SUBSCRIPTIONID]
     *
     */
    @JSONField(name = "subscriptionid")
    @JsonProperty("subscriptionid")
    private String subscriptionid;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;

    /**
     * 属性 [SCHEDULEDDURATIONMINUTES]
     *
     */
    @JSONField(name = "scheduleddurationminutes")
    @JsonProperty("scheduleddurationminutes")
    private Integer scheduleddurationminutes;

    /**
     * 属性 [SORTDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "sortdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("sortdate")
    private Timestamp sortdate;

    /**
     * 属性 [ACTIVITYTYPECODE]
     *
     */
    @JSONField(name = "activitytypecode")
    @JsonProperty("activitytypecode")
    private String activitytypecode;

    /**
     * 属性 [REGARDINGOBJECTTYPECODE]
     *
     */
    @JSONField(name = "regardingobjecttypecode")
    @JsonProperty("regardingobjecttypecode")
    private String regardingobjecttypecode;

    /**
     * 属性 [LASTONHOLDTIME]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "lastonholdtime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("lastonholdtime")
    private Timestamp lastonholdtime;

    /**
     * 属性 [SCHEDULEDSTART]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "scheduledstart" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("scheduledstart")
    private Timestamp scheduledstart;

    /**
     * 属性 [VERSIONNUMBER]
     *
     */
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;

    /**
     * 属性 [OVERRIDDENCREATEDON]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;

    /**
     * 属性 [ONHOLDTIME]
     *
     */
    @JSONField(name = "onholdtime")
    @JsonProperty("onholdtime")
    private Integer onholdtime;

    /**
     * 属性 [ACTIVITYID]
     *
     */
    @JSONField(name = "activityid")
    @JsonProperty("activityid")
    private String activityid;

    /**
     * 属性 [SUBJECT]
     *
     */
    @JSONField(name = "subject")
    @JsonProperty("subject")
    private String subject;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [TRAVERSEDPATH]
     *
     */
    @JSONField(name = "traversedpath")
    @JsonProperty("traversedpath")
    private String traversedpath;

    /**
     * 属性 [REGULARACTIVITY]
     *
     */
    @JSONField(name = "regularactivity")
    @JsonProperty("regularactivity")
    private Integer regularactivity;

    /**
     * 属性 [UTCCONVERSIONTIMEZONECODE]
     *
     */
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;

    /**
     * 属性 [CRMTASKASSIGNEDUNIQUEID]
     *
     */
    @JSONField(name = "crmtaskassigneduniqueid")
    @JsonProperty("crmtaskassigneduniqueid")
    private String crmtaskassigneduniqueid;

    /**
     * 属性 [SLANAME]
     *
     */
    @JSONField(name = "slaname")
    @JsonProperty("slaname")
    private String slaname;

    /**
     * 属性 [ACTUALSTART]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "actualstart" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("actualstart")
    private Timestamp actualstart;

    /**
     * 属性 [SUBCATEGORY]
     *
     */
    @JSONField(name = "subcategory")
    @JsonProperty("subcategory")
    private String subcategory;

    /**
     * 属性 [STATUSCODE]
     *
     */
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;

    /**
     * 属性 [PERCENTCOMPLETE]
     *
     */
    @JSONField(name = "percentcomplete")
    @JsonProperty("percentcomplete")
    private Integer percentcomplete;

    /**
     * 属性 [REGARDINGOBJECTID]
     *
     */
    @JSONField(name = "regardingobjectid")
    @JsonProperty("regardingobjectid")
    private String regardingobjectid;

    /**
     * 属性 [OWNERTYPE]
     *
     */
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;

    /**
     * 属性 [BILLED]
     *
     */
    @JSONField(name = "billed")
    @JsonProperty("billed")
    private Integer billed;

    /**
     * 属性 [TIMEZONERULEVERSIONNUMBER]
     *
     */
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;

    /**
     * 属性 [SCHEDULEDEND]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "scheduledend" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("scheduledend")
    private Timestamp scheduledend;

    /**
     * 属性 [OWNERNAME]
     *
     */
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;

    /**
     * 属性 [PROCESSID]
     *
     */
    @JSONField(name = "processid")
    @JsonProperty("processid")
    private String processid;

    /**
     * 属性 [STAGEID]
     *
     */
    @JSONField(name = "stageid")
    @JsonProperty("stageid")
    private String stageid;

    /**
     * 属性 [OWNERID]
     *
     */
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;

    /**
     * 属性 [ACTUALDURATIONMINUTES]
     *
     */
    @JSONField(name = "actualdurationminutes")
    @JsonProperty("actualdurationminutes")
    private Integer actualdurationminutes;

    /**
     * 属性 [EXCHANGERATE]
     *
     */
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;

    /**
     * 属性 [CATEGORY]
     *
     */
    @JSONField(name = "category")
    @JsonProperty("category")
    private String category;

    /**
     * 属性 [IMPORTSEQUENCENUMBER]
     *
     */
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;

    /**
     * 属性 [SLAID]
     *
     */
    @JSONField(name = "slaid")
    @JsonProperty("slaid")
    private String slaid;

    /**
     * 属性 [TRANSACTIONCURRENCYID]
     *
     */
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;

    /**
     * 属性 [SERVICEID]
     *
     */
    @JSONField(name = "serviceid")
    @JsonProperty("serviceid")
    private String serviceid;


    /**
     * 设置 [WORKFLOWCREATED]
     */
    public void setWorkflowcreated(Integer  workflowcreated){
        this.workflowcreated = workflowcreated ;
        this.modify("workflowcreated",workflowcreated);
    }

    /**
     * 设置 [REGARDINGOBJECTNAME]
     */
    public void setRegardingobjectname(String  regardingobjectname){
        this.regardingobjectname = regardingobjectname ;
        this.modify("regardingobjectname",regardingobjectname);
    }

    /**
     * 设置 [ACTUALEND]
     */
    public void setActualend(Timestamp  actualend){
        this.actualend = actualend ;
        this.modify("actualend",actualend);
    }

    /**
     * 设置 [STATECODE]
     */
    public void setStatecode(Integer  statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [PRIORITYCODE]
     */
    public void setPrioritycode(String  prioritycode){
        this.prioritycode = prioritycode ;
        this.modify("prioritycode",prioritycode);
    }

    /**
     * 设置 [ACTIVITYADDITIONALPARAMS]
     */
    public void setActivityadditionalparams(String  activityadditionalparams){
        this.activityadditionalparams = activityadditionalparams ;
        this.modify("activityadditionalparams",activityadditionalparams);
    }

    /**
     * 设置 [SUBSCRIPTIONID]
     */
    public void setSubscriptionid(String  subscriptionid){
        this.subscriptionid = subscriptionid ;
        this.modify("subscriptionid",subscriptionid);
    }

    /**
     * 设置 [SCHEDULEDDURATIONMINUTES]
     */
    public void setScheduleddurationminutes(Integer  scheduleddurationminutes){
        this.scheduleddurationminutes = scheduleddurationminutes ;
        this.modify("scheduleddurationminutes",scheduleddurationminutes);
    }

    /**
     * 设置 [SORTDATE]
     */
    public void setSortdate(Timestamp  sortdate){
        this.sortdate = sortdate ;
        this.modify("sortdate",sortdate);
    }

    /**
     * 设置 [ACTIVITYTYPECODE]
     */
    public void setActivitytypecode(String  activitytypecode){
        this.activitytypecode = activitytypecode ;
        this.modify("activitytypecode",activitytypecode);
    }

    /**
     * 设置 [REGARDINGOBJECTTYPECODE]
     */
    public void setRegardingobjecttypecode(String  regardingobjecttypecode){
        this.regardingobjecttypecode = regardingobjecttypecode ;
        this.modify("regardingobjecttypecode",regardingobjecttypecode);
    }

    /**
     * 设置 [LASTONHOLDTIME]
     */
    public void setLastonholdtime(Timestamp  lastonholdtime){
        this.lastonholdtime = lastonholdtime ;
        this.modify("lastonholdtime",lastonholdtime);
    }

    /**
     * 设置 [SCHEDULEDSTART]
     */
    public void setScheduledstart(Timestamp  scheduledstart){
        this.scheduledstart = scheduledstart ;
        this.modify("scheduledstart",scheduledstart);
    }

    /**
     * 设置 [VERSIONNUMBER]
     */
    public void setVersionnumber(BigInteger  versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [OVERRIDDENCREATEDON]
     */
    public void setOverriddencreatedon(Timestamp  overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 设置 [ONHOLDTIME]
     */
    public void setOnholdtime(Integer  onholdtime){
        this.onholdtime = onholdtime ;
        this.modify("onholdtime",onholdtime);
    }

    /**
     * 设置 [SUBJECT]
     */
    public void setSubject(String  subject){
        this.subject = subject ;
        this.modify("subject",subject);
    }

    /**
     * 设置 [TRAVERSEDPATH]
     */
    public void setTraversedpath(String  traversedpath){
        this.traversedpath = traversedpath ;
        this.modify("traversedpath",traversedpath);
    }

    /**
     * 设置 [REGULARACTIVITY]
     */
    public void setRegularactivity(Integer  regularactivity){
        this.regularactivity = regularactivity ;
        this.modify("regularactivity",regularactivity);
    }

    /**
     * 设置 [UTCCONVERSIONTIMEZONECODE]
     */
    public void setUtcconversiontimezonecode(Integer  utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [CRMTASKASSIGNEDUNIQUEID]
     */
    public void setCrmtaskassigneduniqueid(String  crmtaskassigneduniqueid){
        this.crmtaskassigneduniqueid = crmtaskassigneduniqueid ;
        this.modify("crmtaskassigneduniqueid",crmtaskassigneduniqueid);
    }

    /**
     * 设置 [SLANAME]
     */
    public void setSlaname(String  slaname){
        this.slaname = slaname ;
        this.modify("slaname",slaname);
    }

    /**
     * 设置 [ACTUALSTART]
     */
    public void setActualstart(Timestamp  actualstart){
        this.actualstart = actualstart ;
        this.modify("actualstart",actualstart);
    }

    /**
     * 设置 [SUBCATEGORY]
     */
    public void setSubcategory(String  subcategory){
        this.subcategory = subcategory ;
        this.modify("subcategory",subcategory);
    }

    /**
     * 设置 [STATUSCODE]
     */
    public void setStatuscode(Integer  statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [PERCENTCOMPLETE]
     */
    public void setPercentcomplete(Integer  percentcomplete){
        this.percentcomplete = percentcomplete ;
        this.modify("percentcomplete",percentcomplete);
    }

    /**
     * 设置 [REGARDINGOBJECTID]
     */
    public void setRegardingobjectid(String  regardingobjectid){
        this.regardingobjectid = regardingobjectid ;
        this.modify("regardingobjectid",regardingobjectid);
    }

    /**
     * 设置 [OWNERTYPE]
     */
    public void setOwnertype(String  ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [BILLED]
     */
    public void setBilled(Integer  billed){
        this.billed = billed ;
        this.modify("billed",billed);
    }

    /**
     * 设置 [TIMEZONERULEVERSIONNUMBER]
     */
    public void setTimezoneruleversionnumber(Integer  timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [SCHEDULEDEND]
     */
    public void setScheduledend(Timestamp  scheduledend){
        this.scheduledend = scheduledend ;
        this.modify("scheduledend",scheduledend);
    }

    /**
     * 设置 [OWNERNAME]
     */
    public void setOwnername(String  ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [PROCESSID]
     */
    public void setProcessid(String  processid){
        this.processid = processid ;
        this.modify("processid",processid);
    }

    /**
     * 设置 [STAGEID]
     */
    public void setStageid(String  stageid){
        this.stageid = stageid ;
        this.modify("stageid",stageid);
    }

    /**
     * 设置 [OWNERID]
     */
    public void setOwnerid(String  ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [ACTUALDURATIONMINUTES]
     */
    public void setActualdurationminutes(Integer  actualdurationminutes){
        this.actualdurationminutes = actualdurationminutes ;
        this.modify("actualdurationminutes",actualdurationminutes);
    }

    /**
     * 设置 [EXCHANGERATE]
     */
    public void setExchangerate(BigDecimal  exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [CATEGORY]
     */
    public void setCategory(String  category){
        this.category = category ;
        this.modify("category",category);
    }

    /**
     * 设置 [IMPORTSEQUENCENUMBER]
     */
    public void setImportsequencenumber(Integer  importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [SLAID]
     */
    public void setSlaid(String  slaid){
        this.slaid = slaid ;
        this.modify("slaid",slaid);
    }

    /**
     * 设置 [TRANSACTIONCURRENCYID]
     */
    public void setTransactioncurrencyid(String  transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }

    /**
     * 设置 [SERVICEID]
     */
    public void setServiceid(String  serviceid){
        this.serviceid = serviceid ;
        this.modify("serviceid",serviceid);
    }


}


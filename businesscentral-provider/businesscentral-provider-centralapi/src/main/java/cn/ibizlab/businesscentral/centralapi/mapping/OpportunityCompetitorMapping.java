package cn.ibizlab.businesscentral.centralapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.sales.domain.OpportunityCompetitor;
import cn.ibizlab.businesscentral.centralapi.dto.OpportunityCompetitorDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CentralApiOpportunityCompetitorMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface OpportunityCompetitorMapping extends MappingBase<OpportunityCompetitorDTO, OpportunityCompetitor> {


}


package cn.ibizlab.businesscentral.centralapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[InvoiceDTO]
 */
@Data
public class InvoiceDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [OWNERNAME]
     *
     */
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;

    /**
     * 属性 [BILLTO_LINE1]
     *
     */
    @JSONField(name = "billto_line1")
    @JsonProperty("billto_line1")
    private String billtoLine1;

    /**
     * 属性 [BILLTO_TELEPHONE]
     *
     */
    @JSONField(name = "billto_telephone")
    @JsonProperty("billto_telephone")
    private String billtoTelephone;

    /**
     * 属性 [LASTONHOLDTIME]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "lastonholdtime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("lastonholdtime")
    private Timestamp lastonholdtime;

    /**
     * 属性 [BILLTO_STATEORPROVINCE]
     *
     */
    @JSONField(name = "billto_stateorprovince")
    @JsonProperty("billto_stateorprovince")
    private String billtoStateorprovince;

    /**
     * 属性 [DUEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "duedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("duedate")
    private Timestamp duedate;

    /**
     * 属性 [PRICINGERRORCODE]
     *
     */
    @JSONField(name = "pricingerrorcode")
    @JsonProperty("pricingerrorcode")
    private String pricingerrorcode;

    /**
     * 属性 [TRAVERSEDPATH]
     *
     */
    @JSONField(name = "traversedpath")
    @JsonProperty("traversedpath")
    private String traversedpath;

    /**
     * 属性 [TOTALDISCOUNTAMOUNT_BASE]
     *
     */
    @JSONField(name = "totaldiscountamount_base")
    @JsonProperty("totaldiscountamount_base")
    private BigDecimal totaldiscountamountBase;

    /**
     * 属性 [TOTALAMOUNTLESSFREIGHT_BASE]
     *
     */
    @JSONField(name = "totalamountlessfreight_base")
    @JsonProperty("totalamountlessfreight_base")
    private BigDecimal totalamountlessfreightBase;

    /**
     * 属性 [LASTBACKOFFICESUBMIT]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "lastbackofficesubmit" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("lastbackofficesubmit")
    private Timestamp lastbackofficesubmit;

    /**
     * 属性 [SHIPTO_TELEPHONE]
     *
     */
    @JSONField(name = "shipto_telephone")
    @JsonProperty("shipto_telephone")
    private String shiptoTelephone;

    /**
     * 属性 [OWNERTYPE]
     *
     */
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;

    /**
     * 属性 [BILLTO_CITY]
     *
     */
    @JSONField(name = "billto_city")
    @JsonProperty("billto_city")
    private String billtoCity;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;

    /**
     * 属性 [SHIPTO_COMPOSITE]
     *
     */
    @JSONField(name = "shipto_composite")
    @JsonProperty("shipto_composite")
    private String shiptoComposite;

    /**
     * 属性 [OVERRIDDENCREATEDON]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;

    /**
     * 属性 [BILLTO_POSTALCODE]
     *
     */
    @JSONField(name = "billto_postalcode")
    @JsonProperty("billto_postalcode")
    private String billtoPostalcode;

    /**
     * 属性 [FREIGHTAMOUNT_BASE]
     *
     */
    @JSONField(name = "freightamount_base")
    @JsonProperty("freightamount_base")
    private BigDecimal freightamountBase;

    /**
     * 属性 [INVOICENAME]
     *
     */
    @JSONField(name = "invoicename")
    @JsonProperty("invoicename")
    private String invoicename;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [SHIPTO_CITY]
     *
     */
    @JSONField(name = "shipto_city")
    @JsonProperty("shipto_city")
    private String shiptoCity;

    /**
     * 属性 [TOTALAMOUNT]
     *
     */
    @JSONField(name = "totalamount")
    @JsonProperty("totalamount")
    private BigDecimal totalamount;

    /**
     * 属性 [CUSTOMERID]
     *
     */
    @JSONField(name = "customerid")
    @JsonProperty("customerid")
    private String customerid;

    /**
     * 属性 [TOTALAMOUNT_BASE]
     *
     */
    @JSONField(name = "totalamount_base")
    @JsonProperty("totalamount_base")
    private BigDecimal totalamountBase;

    /**
     * 属性 [BILLTO_NAME]
     *
     */
    @JSONField(name = "billto_name")
    @JsonProperty("billto_name")
    private String billtoName;

    /**
     * 属性 [TIMEZONERULEVERSIONNUMBER]
     *
     */
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 属性 [UTCCONVERSIONTIMEZONECODE]
     *
     */
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;

    /**
     * 属性 [FREIGHTAMOUNT]
     *
     */
    @JSONField(name = "freightamount")
    @JsonProperty("freightamount")
    private BigDecimal freightamount;

    /**
     * 属性 [PRIORITYCODE]
     *
     */
    @JSONField(name = "prioritycode")
    @JsonProperty("prioritycode")
    private String prioritycode;

    /**
     * 属性 [DISCOUNTAMOUNT_BASE]
     *
     */
    @JSONField(name = "discountamount_base")
    @JsonProperty("discountamount_base")
    private BigDecimal discountamountBase;

    /**
     * 属性 [ONHOLDTIME]
     *
     */
    @JSONField(name = "onholdtime")
    @JsonProperty("onholdtime")
    private Integer onholdtime;

    /**
     * 属性 [SHIPTO_COUNTRY]
     *
     */
    @JSONField(name = "shipto_country")
    @JsonProperty("shipto_country")
    private String shiptoCountry;

    /**
     * 属性 [PAYMENTTERMSCODE]
     *
     */
    @JSONField(name = "paymenttermscode")
    @JsonProperty("paymenttermscode")
    private String paymenttermscode;

    /**
     * 属性 [SHIPTO_FREIGHTTERMSCODE]
     *
     */
    @JSONField(name = "shipto_freighttermscode")
    @JsonProperty("shipto_freighttermscode")
    private String shiptoFreighttermscode;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;

    /**
     * 属性 [SHIPTO_LINE3]
     *
     */
    @JSONField(name = "shipto_line3")
    @JsonProperty("shipto_line3")
    private String shiptoLine3;

    /**
     * 属性 [TOTALTAX]
     *
     */
    @JSONField(name = "totaltax")
    @JsonProperty("totaltax")
    private BigDecimal totaltax;

    /**
     * 属性 [BILLTO_LINE2]
     *
     */
    @JSONField(name = "billto_line2")
    @JsonProperty("billto_line2")
    private String billtoLine2;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [EXCHANGERATE]
     *
     */
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;

    /**
     * 属性 [SHIPTO_LINE1]
     *
     */
    @JSONField(name = "shipto_line1")
    @JsonProperty("shipto_line1")
    private String shiptoLine1;

    /**
     * 属性 [INVOICEID]
     *
     */
    @JSONField(name = "invoiceid")
    @JsonProperty("invoiceid")
    private String invoiceid;

    /**
     * 属性 [CUSTOMERNAME]
     *
     */
    @JSONField(name = "customername")
    @JsonProperty("customername")
    private String customername;

    /**
     * 属性 [DISCOUNTPERCENTAGE]
     *
     */
    @JSONField(name = "discountpercentage")
    @JsonProperty("discountpercentage")
    private BigDecimal discountpercentage;

    /**
     * 属性 [TOTALLINEITEMAMOUNT_BASE]
     *
     */
    @JSONField(name = "totallineitemamount_base")
    @JsonProperty("totallineitemamount_base")
    private BigDecimal totallineitemamountBase;

    /**
     * 属性 [STAGEID]
     *
     */
    @JSONField(name = "stageid")
    @JsonProperty("stageid")
    private String stageid;

    /**
     * 属性 [ENTITYIMAGE_URL]
     *
     */
    @JSONField(name = "entityimage_url")
    @JsonProperty("entityimage_url")
    private String entityimageUrl;

    /**
     * 属性 [STATECODE]
     *
     */
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;

    /**
     * 属性 [TOTALAMOUNTLESSFREIGHT]
     *
     */
    @JSONField(name = "totalamountlessfreight")
    @JsonProperty("totalamountlessfreight")
    private BigDecimal totalamountlessfreight;

    /**
     * 属性 [TOTALTAX_BASE]
     *
     */
    @JSONField(name = "totaltax_base")
    @JsonProperty("totaltax_base")
    private BigDecimal totaltaxBase;

    /**
     * 属性 [SHIPTO_LINE2]
     *
     */
    @JSONField(name = "shipto_line2")
    @JsonProperty("shipto_line2")
    private String shiptoLine2;

    /**
     * 属性 [BILLTO_COUNTRY]
     *
     */
    @JSONField(name = "billto_country")
    @JsonProperty("billto_country")
    private String billtoCountry;

    /**
     * 属性 [ENTITYIMAGE]
     *
     */
    @JSONField(name = "entityimage")
    @JsonProperty("entityimage")
    private String entityimage;

    /**
     * 属性 [IMPORTSEQUENCENUMBER]
     *
     */
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;

    /**
     * 属性 [SHIPTO_FAX]
     *
     */
    @JSONField(name = "shipto_fax")
    @JsonProperty("shipto_fax")
    private String shiptoFax;

    /**
     * 属性 [OWNERID]
     *
     */
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;

    /**
     * 属性 [DATEDELIVERED]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "datedelivered" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("datedelivered")
    private Timestamp datedelivered;

    /**
     * 属性 [CONTACTNAME]
     *
     */
    @JSONField(name = "contactname")
    @JsonProperty("contactname")
    private String contactname;

    /**
     * 属性 [ENTITYIMAGE_TIMESTAMP]
     *
     */
    @JSONField(name = "entityimage_timestamp")
    @JsonProperty("entityimage_timestamp")
    private BigInteger entityimageTimestamp;

    /**
     * 属性 [PROCESSID]
     *
     */
    @JSONField(name = "processid")
    @JsonProperty("processid")
    private String processid;

    /**
     * 属性 [TOTALDISCOUNTAMOUNT]
     *
     */
    @JSONField(name = "totaldiscountamount")
    @JsonProperty("totaldiscountamount")
    private BigDecimal totaldiscountamount;

    /**
     * 属性 [PRICELOCKED]
     *
     */
    @JSONField(name = "pricelocked")
    @JsonProperty("pricelocked")
    private Integer pricelocked;

    /**
     * 属性 [WILLCALL]
     *
     */
    @JSONField(name = "willcall")
    @JsonProperty("willcall")
    private Integer willcall;

    /**
     * 属性 [BILLTO_LINE3]
     *
     */
    @JSONField(name = "billto_line3")
    @JsonProperty("billto_line3")
    private String billtoLine3;

    /**
     * 属性 [SHIPTO_NAME]
     *
     */
    @JSONField(name = "shipto_name")
    @JsonProperty("shipto_name")
    private String shiptoName;

    /**
     * 属性 [STATUSCODE]
     *
     */
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;

    /**
     * 属性 [TOTALLINEITEMAMOUNT]
     *
     */
    @JSONField(name = "totallineitemamount")
    @JsonProperty("totallineitemamount")
    private BigDecimal totallineitemamount;

    /**
     * 属性 [DISCOUNTAMOUNT]
     *
     */
    @JSONField(name = "discountamount")
    @JsonProperty("discountamount")
    private BigDecimal discountamount;

    /**
     * 属性 [SHIPTO_STATEORPROVINCE]
     *
     */
    @JSONField(name = "shipto_stateorprovince")
    @JsonProperty("shipto_stateorprovince")
    private String shiptoStateorprovince;

    /**
     * 属性 [ENTITYIMAGEID]
     *
     */
    @JSONField(name = "entityimageid")
    @JsonProperty("entityimageid")
    private String entityimageid;

    /**
     * 属性 [VERSIONNUMBER]
     *
     */
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;

    /**
     * 属性 [SHIPTO_POSTALCODE]
     *
     */
    @JSONField(name = "shipto_postalcode")
    @JsonProperty("shipto_postalcode")
    private String shiptoPostalcode;

    /**
     * 属性 [INVOICENUMBER]
     *
     */
    @JSONField(name = "invoicenumber")
    @JsonProperty("invoicenumber")
    private String invoicenumber;

    /**
     * 属性 [BILLTO_COMPOSITE]
     *
     */
    @JSONField(name = "billto_composite")
    @JsonProperty("billto_composite")
    private String billtoComposite;

    /**
     * 属性 [EMAILADDRESS]
     *
     */
    @JSONField(name = "emailaddress")
    @JsonProperty("emailaddress")
    private String emailaddress;

    /**
     * 属性 [SHIPPINGMETHODCODE]
     *
     */
    @JSONField(name = "shippingmethodcode")
    @JsonProperty("shippingmethodcode")
    private String shippingmethodcode;

    /**
     * 属性 [CUSTOMERTYPE]
     *
     */
    @JSONField(name = "customertype")
    @JsonProperty("customertype")
    private String customertype;

    /**
     * 属性 [TOTALLINEITEMDISCOUNTAMOUNT]
     *
     */
    @JSONField(name = "totallineitemdiscountamount")
    @JsonProperty("totallineitemdiscountamount")
    private BigDecimal totallineitemdiscountamount;

    /**
     * 属性 [ACCOUNTNAME]
     *
     */
    @JSONField(name = "accountname")
    @JsonProperty("accountname")
    private String accountname;

    /**
     * 属性 [BILLTO_FAX]
     *
     */
    @JSONField(name = "billto_fax")
    @JsonProperty("billto_fax")
    private String billtoFax;

    /**
     * 属性 [OPPORTUNITYNAME]
     *
     */
    @JSONField(name = "opportunityname")
    @JsonProperty("opportunityname")
    private String opportunityname;

    /**
     * 属性 [CURRENCYNAME]
     *
     */
    @JSONField(name = "currencyname")
    @JsonProperty("currencyname")
    private String currencyname;

    /**
     * 属性 [PRICELEVELNAME]
     *
     */
    @JSONField(name = "pricelevelname")
    @JsonProperty("pricelevelname")
    private String pricelevelname;

    /**
     * 属性 [SLANAME]
     *
     */
    @JSONField(name = "slaname")
    @JsonProperty("slaname")
    private String slaname;

    /**
     * 属性 [SALESORDERNAME]
     *
     */
    @JSONField(name = "salesordername")
    @JsonProperty("salesordername")
    private String salesordername;

    /**
     * 属性 [TRANSACTIONCURRENCYID]
     *
     */
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;

    /**
     * 属性 [PRICELEVELID]
     *
     */
    @JSONField(name = "pricelevelid")
    @JsonProperty("pricelevelid")
    private String pricelevelid;

    /**
     * 属性 [SALESORDERID]
     *
     */
    @JSONField(name = "salesorderid")
    @JsonProperty("salesorderid")
    private String salesorderid;

    /**
     * 属性 [OPPORTUNITYID]
     *
     */
    @JSONField(name = "opportunityid")
    @JsonProperty("opportunityid")
    private String opportunityid;

    /**
     * 属性 [SLAID]
     *
     */
    @JSONField(name = "slaid")
    @JsonProperty("slaid")
    private String slaid;


    /**
     * 设置 [OWNERNAME]
     */
    public void setOwnername(String  ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [BILLTO_LINE1]
     */
    public void setBilltoLine1(String  billtoLine1){
        this.billtoLine1 = billtoLine1 ;
        this.modify("billto_line1",billtoLine1);
    }

    /**
     * 设置 [BILLTO_TELEPHONE]
     */
    public void setBilltoTelephone(String  billtoTelephone){
        this.billtoTelephone = billtoTelephone ;
        this.modify("billto_telephone",billtoTelephone);
    }

    /**
     * 设置 [LASTONHOLDTIME]
     */
    public void setLastonholdtime(Timestamp  lastonholdtime){
        this.lastonholdtime = lastonholdtime ;
        this.modify("lastonholdtime",lastonholdtime);
    }

    /**
     * 设置 [BILLTO_STATEORPROVINCE]
     */
    public void setBilltoStateorprovince(String  billtoStateorprovince){
        this.billtoStateorprovince = billtoStateorprovince ;
        this.modify("billto_stateorprovince",billtoStateorprovince);
    }

    /**
     * 设置 [DUEDATE]
     */
    public void setDuedate(Timestamp  duedate){
        this.duedate = duedate ;
        this.modify("duedate",duedate);
    }

    /**
     * 设置 [PRICINGERRORCODE]
     */
    public void setPricingerrorcode(String  pricingerrorcode){
        this.pricingerrorcode = pricingerrorcode ;
        this.modify("pricingerrorcode",pricingerrorcode);
    }

    /**
     * 设置 [TRAVERSEDPATH]
     */
    public void setTraversedpath(String  traversedpath){
        this.traversedpath = traversedpath ;
        this.modify("traversedpath",traversedpath);
    }

    /**
     * 设置 [TOTALDISCOUNTAMOUNT_BASE]
     */
    public void setTotaldiscountamountBase(BigDecimal  totaldiscountamountBase){
        this.totaldiscountamountBase = totaldiscountamountBase ;
        this.modify("totaldiscountamount_base",totaldiscountamountBase);
    }

    /**
     * 设置 [TOTALAMOUNTLESSFREIGHT_BASE]
     */
    public void setTotalamountlessfreightBase(BigDecimal  totalamountlessfreightBase){
        this.totalamountlessfreightBase = totalamountlessfreightBase ;
        this.modify("totalamountlessfreight_base",totalamountlessfreightBase);
    }

    /**
     * 设置 [LASTBACKOFFICESUBMIT]
     */
    public void setLastbackofficesubmit(Timestamp  lastbackofficesubmit){
        this.lastbackofficesubmit = lastbackofficesubmit ;
        this.modify("lastbackofficesubmit",lastbackofficesubmit);
    }

    /**
     * 设置 [SHIPTO_TELEPHONE]
     */
    public void setShiptoTelephone(String  shiptoTelephone){
        this.shiptoTelephone = shiptoTelephone ;
        this.modify("shipto_telephone",shiptoTelephone);
    }

    /**
     * 设置 [OWNERTYPE]
     */
    public void setOwnertype(String  ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [BILLTO_CITY]
     */
    public void setBilltoCity(String  billtoCity){
        this.billtoCity = billtoCity ;
        this.modify("billto_city",billtoCity);
    }

    /**
     * 设置 [SHIPTO_COMPOSITE]
     */
    public void setShiptoComposite(String  shiptoComposite){
        this.shiptoComposite = shiptoComposite ;
        this.modify("shipto_composite",shiptoComposite);
    }

    /**
     * 设置 [OVERRIDDENCREATEDON]
     */
    public void setOverriddencreatedon(Timestamp  overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 设置 [BILLTO_POSTALCODE]
     */
    public void setBilltoPostalcode(String  billtoPostalcode){
        this.billtoPostalcode = billtoPostalcode ;
        this.modify("billto_postalcode",billtoPostalcode);
    }

    /**
     * 设置 [FREIGHTAMOUNT_BASE]
     */
    public void setFreightamountBase(BigDecimal  freightamountBase){
        this.freightamountBase = freightamountBase ;
        this.modify("freightamount_base",freightamountBase);
    }

    /**
     * 设置 [INVOICENAME]
     */
    public void setInvoicename(String  invoicename){
        this.invoicename = invoicename ;
        this.modify("invoicename",invoicename);
    }

    /**
     * 设置 [SHIPTO_CITY]
     */
    public void setShiptoCity(String  shiptoCity){
        this.shiptoCity = shiptoCity ;
        this.modify("shipto_city",shiptoCity);
    }

    /**
     * 设置 [TOTALAMOUNT]
     */
    public void setTotalamount(BigDecimal  totalamount){
        this.totalamount = totalamount ;
        this.modify("totalamount",totalamount);
    }

    /**
     * 设置 [CUSTOMERID]
     */
    public void setCustomerid(String  customerid){
        this.customerid = customerid ;
        this.modify("customerid",customerid);
    }

    /**
     * 设置 [TOTALAMOUNT_BASE]
     */
    public void setTotalamountBase(BigDecimal  totalamountBase){
        this.totalamountBase = totalamountBase ;
        this.modify("totalamount_base",totalamountBase);
    }

    /**
     * 设置 [BILLTO_NAME]
     */
    public void setBilltoName(String  billtoName){
        this.billtoName = billtoName ;
        this.modify("billto_name",billtoName);
    }

    /**
     * 设置 [TIMEZONERULEVERSIONNUMBER]
     */
    public void setTimezoneruleversionnumber(Integer  timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [UTCCONVERSIONTIMEZONECODE]
     */
    public void setUtcconversiontimezonecode(Integer  utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [FREIGHTAMOUNT]
     */
    public void setFreightamount(BigDecimal  freightamount){
        this.freightamount = freightamount ;
        this.modify("freightamount",freightamount);
    }

    /**
     * 设置 [PRIORITYCODE]
     */
    public void setPrioritycode(String  prioritycode){
        this.prioritycode = prioritycode ;
        this.modify("prioritycode",prioritycode);
    }

    /**
     * 设置 [DISCOUNTAMOUNT_BASE]
     */
    public void setDiscountamountBase(BigDecimal  discountamountBase){
        this.discountamountBase = discountamountBase ;
        this.modify("discountamount_base",discountamountBase);
    }

    /**
     * 设置 [ONHOLDTIME]
     */
    public void setOnholdtime(Integer  onholdtime){
        this.onholdtime = onholdtime ;
        this.modify("onholdtime",onholdtime);
    }

    /**
     * 设置 [SHIPTO_COUNTRY]
     */
    public void setShiptoCountry(String  shiptoCountry){
        this.shiptoCountry = shiptoCountry ;
        this.modify("shipto_country",shiptoCountry);
    }

    /**
     * 设置 [PAYMENTTERMSCODE]
     */
    public void setPaymenttermscode(String  paymenttermscode){
        this.paymenttermscode = paymenttermscode ;
        this.modify("paymenttermscode",paymenttermscode);
    }

    /**
     * 设置 [SHIPTO_FREIGHTTERMSCODE]
     */
    public void setShiptoFreighttermscode(String  shiptoFreighttermscode){
        this.shiptoFreighttermscode = shiptoFreighttermscode ;
        this.modify("shipto_freighttermscode",shiptoFreighttermscode);
    }

    /**
     * 设置 [SHIPTO_LINE3]
     */
    public void setShiptoLine3(String  shiptoLine3){
        this.shiptoLine3 = shiptoLine3 ;
        this.modify("shipto_line3",shiptoLine3);
    }

    /**
     * 设置 [TOTALTAX]
     */
    public void setTotaltax(BigDecimal  totaltax){
        this.totaltax = totaltax ;
        this.modify("totaltax",totaltax);
    }

    /**
     * 设置 [BILLTO_LINE2]
     */
    public void setBilltoLine2(String  billtoLine2){
        this.billtoLine2 = billtoLine2 ;
        this.modify("billto_line2",billtoLine2);
    }

    /**
     * 设置 [EXCHANGERATE]
     */
    public void setExchangerate(BigDecimal  exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [SHIPTO_LINE1]
     */
    public void setShiptoLine1(String  shiptoLine1){
        this.shiptoLine1 = shiptoLine1 ;
        this.modify("shipto_line1",shiptoLine1);
    }

    /**
     * 设置 [CUSTOMERNAME]
     */
    public void setCustomername(String  customername){
        this.customername = customername ;
        this.modify("customername",customername);
    }

    /**
     * 设置 [DISCOUNTPERCENTAGE]
     */
    public void setDiscountpercentage(BigDecimal  discountpercentage){
        this.discountpercentage = discountpercentage ;
        this.modify("discountpercentage",discountpercentage);
    }

    /**
     * 设置 [TOTALLINEITEMAMOUNT_BASE]
     */
    public void setTotallineitemamountBase(BigDecimal  totallineitemamountBase){
        this.totallineitemamountBase = totallineitemamountBase ;
        this.modify("totallineitemamount_base",totallineitemamountBase);
    }

    /**
     * 设置 [STAGEID]
     */
    public void setStageid(String  stageid){
        this.stageid = stageid ;
        this.modify("stageid",stageid);
    }

    /**
     * 设置 [ENTITYIMAGE_URL]
     */
    public void setEntityimageUrl(String  entityimageUrl){
        this.entityimageUrl = entityimageUrl ;
        this.modify("entityimage_url",entityimageUrl);
    }

    /**
     * 设置 [STATECODE]
     */
    public void setStatecode(Integer  statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [TOTALAMOUNTLESSFREIGHT]
     */
    public void setTotalamountlessfreight(BigDecimal  totalamountlessfreight){
        this.totalamountlessfreight = totalamountlessfreight ;
        this.modify("totalamountlessfreight",totalamountlessfreight);
    }

    /**
     * 设置 [TOTALTAX_BASE]
     */
    public void setTotaltaxBase(BigDecimal  totaltaxBase){
        this.totaltaxBase = totaltaxBase ;
        this.modify("totaltax_base",totaltaxBase);
    }

    /**
     * 设置 [SHIPTO_LINE2]
     */
    public void setShiptoLine2(String  shiptoLine2){
        this.shiptoLine2 = shiptoLine2 ;
        this.modify("shipto_line2",shiptoLine2);
    }

    /**
     * 设置 [BILLTO_COUNTRY]
     */
    public void setBilltoCountry(String  billtoCountry){
        this.billtoCountry = billtoCountry ;
        this.modify("billto_country",billtoCountry);
    }

    /**
     * 设置 [ENTITYIMAGE]
     */
    public void setEntityimage(String  entityimage){
        this.entityimage = entityimage ;
        this.modify("entityimage",entityimage);
    }

    /**
     * 设置 [IMPORTSEQUENCENUMBER]
     */
    public void setImportsequencenumber(Integer  importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [SHIPTO_FAX]
     */
    public void setShiptoFax(String  shiptoFax){
        this.shiptoFax = shiptoFax ;
        this.modify("shipto_fax",shiptoFax);
    }

    /**
     * 设置 [OWNERID]
     */
    public void setOwnerid(String  ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [DATEDELIVERED]
     */
    public void setDatedelivered(Timestamp  datedelivered){
        this.datedelivered = datedelivered ;
        this.modify("datedelivered",datedelivered);
    }

    /**
     * 设置 [CONTACTNAME]
     */
    public void setContactname(String  contactname){
        this.contactname = contactname ;
        this.modify("contactname",contactname);
    }

    /**
     * 设置 [ENTITYIMAGE_TIMESTAMP]
     */
    public void setEntityimageTimestamp(BigInteger  entityimageTimestamp){
        this.entityimageTimestamp = entityimageTimestamp ;
        this.modify("entityimage_timestamp",entityimageTimestamp);
    }

    /**
     * 设置 [PROCESSID]
     */
    public void setProcessid(String  processid){
        this.processid = processid ;
        this.modify("processid",processid);
    }

    /**
     * 设置 [TOTALDISCOUNTAMOUNT]
     */
    public void setTotaldiscountamount(BigDecimal  totaldiscountamount){
        this.totaldiscountamount = totaldiscountamount ;
        this.modify("totaldiscountamount",totaldiscountamount);
    }

    /**
     * 设置 [PRICELOCKED]
     */
    public void setPricelocked(Integer  pricelocked){
        this.pricelocked = pricelocked ;
        this.modify("pricelocked",pricelocked);
    }

    /**
     * 设置 [WILLCALL]
     */
    public void setWillcall(Integer  willcall){
        this.willcall = willcall ;
        this.modify("willcall",willcall);
    }

    /**
     * 设置 [BILLTO_LINE3]
     */
    public void setBilltoLine3(String  billtoLine3){
        this.billtoLine3 = billtoLine3 ;
        this.modify("billto_line3",billtoLine3);
    }

    /**
     * 设置 [SHIPTO_NAME]
     */
    public void setShiptoName(String  shiptoName){
        this.shiptoName = shiptoName ;
        this.modify("shipto_name",shiptoName);
    }

    /**
     * 设置 [STATUSCODE]
     */
    public void setStatuscode(Integer  statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [TOTALLINEITEMAMOUNT]
     */
    public void setTotallineitemamount(BigDecimal  totallineitemamount){
        this.totallineitemamount = totallineitemamount ;
        this.modify("totallineitemamount",totallineitemamount);
    }

    /**
     * 设置 [DISCOUNTAMOUNT]
     */
    public void setDiscountamount(BigDecimal  discountamount){
        this.discountamount = discountamount ;
        this.modify("discountamount",discountamount);
    }

    /**
     * 设置 [SHIPTO_STATEORPROVINCE]
     */
    public void setShiptoStateorprovince(String  shiptoStateorprovince){
        this.shiptoStateorprovince = shiptoStateorprovince ;
        this.modify("shipto_stateorprovince",shiptoStateorprovince);
    }

    /**
     * 设置 [ENTITYIMAGEID]
     */
    public void setEntityimageid(String  entityimageid){
        this.entityimageid = entityimageid ;
        this.modify("entityimageid",entityimageid);
    }

    /**
     * 设置 [VERSIONNUMBER]
     */
    public void setVersionnumber(BigInteger  versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [SHIPTO_POSTALCODE]
     */
    public void setShiptoPostalcode(String  shiptoPostalcode){
        this.shiptoPostalcode = shiptoPostalcode ;
        this.modify("shipto_postalcode",shiptoPostalcode);
    }

    /**
     * 设置 [INVOICENUMBER]
     */
    public void setInvoicenumber(String  invoicenumber){
        this.invoicenumber = invoicenumber ;
        this.modify("invoicenumber",invoicenumber);
    }

    /**
     * 设置 [BILLTO_COMPOSITE]
     */
    public void setBilltoComposite(String  billtoComposite){
        this.billtoComposite = billtoComposite ;
        this.modify("billto_composite",billtoComposite);
    }

    /**
     * 设置 [EMAILADDRESS]
     */
    public void setEmailaddress(String  emailaddress){
        this.emailaddress = emailaddress ;
        this.modify("emailaddress",emailaddress);
    }

    /**
     * 设置 [SHIPPINGMETHODCODE]
     */
    public void setShippingmethodcode(String  shippingmethodcode){
        this.shippingmethodcode = shippingmethodcode ;
        this.modify("shippingmethodcode",shippingmethodcode);
    }

    /**
     * 设置 [CUSTOMERTYPE]
     */
    public void setCustomertype(String  customertype){
        this.customertype = customertype ;
        this.modify("customertype",customertype);
    }

    /**
     * 设置 [TOTALLINEITEMDISCOUNTAMOUNT]
     */
    public void setTotallineitemdiscountamount(BigDecimal  totallineitemdiscountamount){
        this.totallineitemdiscountamount = totallineitemdiscountamount ;
        this.modify("totallineitemdiscountamount",totallineitemdiscountamount);
    }

    /**
     * 设置 [ACCOUNTNAME]
     */
    public void setAccountname(String  accountname){
        this.accountname = accountname ;
        this.modify("accountname",accountname);
    }

    /**
     * 设置 [BILLTO_FAX]
     */
    public void setBilltoFax(String  billtoFax){
        this.billtoFax = billtoFax ;
        this.modify("billto_fax",billtoFax);
    }

    /**
     * 设置 [OPPORTUNITYNAME]
     */
    public void setOpportunityname(String  opportunityname){
        this.opportunityname = opportunityname ;
        this.modify("opportunityname",opportunityname);
    }

    /**
     * 设置 [CURRENCYNAME]
     */
    public void setCurrencyname(String  currencyname){
        this.currencyname = currencyname ;
        this.modify("currencyname",currencyname);
    }

    /**
     * 设置 [PRICELEVELNAME]
     */
    public void setPricelevelname(String  pricelevelname){
        this.pricelevelname = pricelevelname ;
        this.modify("pricelevelname",pricelevelname);
    }

    /**
     * 设置 [SLANAME]
     */
    public void setSlaname(String  slaname){
        this.slaname = slaname ;
        this.modify("slaname",slaname);
    }

    /**
     * 设置 [SALESORDERNAME]
     */
    public void setSalesordername(String  salesordername){
        this.salesordername = salesordername ;
        this.modify("salesordername",salesordername);
    }

    /**
     * 设置 [TRANSACTIONCURRENCYID]
     */
    public void setTransactioncurrencyid(String  transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }

    /**
     * 设置 [PRICELEVELID]
     */
    public void setPricelevelid(String  pricelevelid){
        this.pricelevelid = pricelevelid ;
        this.modify("pricelevelid",pricelevelid);
    }

    /**
     * 设置 [SALESORDERID]
     */
    public void setSalesorderid(String  salesorderid){
        this.salesorderid = salesorderid ;
        this.modify("salesorderid",salesorderid);
    }

    /**
     * 设置 [OPPORTUNITYID]
     */
    public void setOpportunityid(String  opportunityid){
        this.opportunityid = opportunityid ;
        this.modify("opportunityid",opportunityid);
    }

    /**
     * 设置 [SLAID]
     */
    public void setSlaid(String  slaid){
        this.slaid = slaid ;
        this.modify("slaid",slaid);
    }


}


package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.base.domain.OMHierarchyPurposeRef;
import cn.ibizlab.businesscentral.core.base.service.IOMHierarchyPurposeRefService;
import cn.ibizlab.businesscentral.core.base.filter.OMHierarchyPurposeRefSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"组织层次结构分配" })
@RestController("CentralApi-omhierarchypurposeref")
@RequestMapping("")
public class OMHierarchyPurposeRefResource {

    @Autowired
    public IOMHierarchyPurposeRefService omhierarchypurposerefService;

    @Autowired
    @Lazy
    public OMHierarchyPurposeRefMapping omhierarchypurposerefMapping;

    @PreAuthorize("hasPermission(this.omhierarchypurposerefMapping.toDomain(#omhierarchypurposerefdto),'iBizBusinessCentral-OMHierarchyPurposeRef-Create')")
    @ApiOperation(value = "新建组织层次结构分配", tags = {"组织层次结构分配" },  notes = "新建组织层次结构分配")
	@RequestMapping(method = RequestMethod.POST, value = "/omhierarchypurposerefs")
    public ResponseEntity<OMHierarchyPurposeRefDTO> create(@RequestBody OMHierarchyPurposeRefDTO omhierarchypurposerefdto) {
        OMHierarchyPurposeRef domain = omhierarchypurposerefMapping.toDomain(omhierarchypurposerefdto);
		omhierarchypurposerefService.create(domain);
        OMHierarchyPurposeRefDTO dto = omhierarchypurposerefMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.omhierarchypurposerefMapping.toDomain(#omhierarchypurposerefdtos),'iBizBusinessCentral-OMHierarchyPurposeRef-Create')")
    @ApiOperation(value = "批量新建组织层次结构分配", tags = {"组织层次结构分配" },  notes = "批量新建组织层次结构分配")
	@RequestMapping(method = RequestMethod.POST, value = "/omhierarchypurposerefs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<OMHierarchyPurposeRefDTO> omhierarchypurposerefdtos) {
        omhierarchypurposerefService.createBatch(omhierarchypurposerefMapping.toDomain(omhierarchypurposerefdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "omhierarchypurposeref" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.omhierarchypurposerefService.get(#omhierarchypurposeref_id),'iBizBusinessCentral-OMHierarchyPurposeRef-Update')")
    @ApiOperation(value = "更新组织层次结构分配", tags = {"组织层次结构分配" },  notes = "更新组织层次结构分配")
	@RequestMapping(method = RequestMethod.PUT, value = "/omhierarchypurposerefs/{omhierarchypurposeref_id}")
    public ResponseEntity<OMHierarchyPurposeRefDTO> update(@PathVariable("omhierarchypurposeref_id") String omhierarchypurposeref_id, @RequestBody OMHierarchyPurposeRefDTO omhierarchypurposerefdto) {
		OMHierarchyPurposeRef domain  = omhierarchypurposerefMapping.toDomain(omhierarchypurposerefdto);
        domain .setOmhierarchypurposerefid(omhierarchypurposeref_id);
		omhierarchypurposerefService.update(domain );
		OMHierarchyPurposeRefDTO dto = omhierarchypurposerefMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.omhierarchypurposerefService.getOmhierarchypurposerefByEntities(this.omhierarchypurposerefMapping.toDomain(#omhierarchypurposerefdtos)),'iBizBusinessCentral-OMHierarchyPurposeRef-Update')")
    @ApiOperation(value = "批量更新组织层次结构分配", tags = {"组织层次结构分配" },  notes = "批量更新组织层次结构分配")
	@RequestMapping(method = RequestMethod.PUT, value = "/omhierarchypurposerefs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<OMHierarchyPurposeRefDTO> omhierarchypurposerefdtos) {
        omhierarchypurposerefService.updateBatch(omhierarchypurposerefMapping.toDomain(omhierarchypurposerefdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.omhierarchypurposerefService.get(#omhierarchypurposeref_id),'iBizBusinessCentral-OMHierarchyPurposeRef-Remove')")
    @ApiOperation(value = "删除组织层次结构分配", tags = {"组织层次结构分配" },  notes = "删除组织层次结构分配")
	@RequestMapping(method = RequestMethod.DELETE, value = "/omhierarchypurposerefs/{omhierarchypurposeref_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("omhierarchypurposeref_id") String omhierarchypurposeref_id) {
         return ResponseEntity.status(HttpStatus.OK).body(omhierarchypurposerefService.remove(omhierarchypurposeref_id));
    }

    @PreAuthorize("hasPermission(this.omhierarchypurposerefService.getOmhierarchypurposerefByIds(#ids),'iBizBusinessCentral-OMHierarchyPurposeRef-Remove')")
    @ApiOperation(value = "批量删除组织层次结构分配", tags = {"组织层次结构分配" },  notes = "批量删除组织层次结构分配")
	@RequestMapping(method = RequestMethod.DELETE, value = "/omhierarchypurposerefs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        omhierarchypurposerefService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.omhierarchypurposerefMapping.toDomain(returnObject.body),'iBizBusinessCentral-OMHierarchyPurposeRef-Get')")
    @ApiOperation(value = "获取组织层次结构分配", tags = {"组织层次结构分配" },  notes = "获取组织层次结构分配")
	@RequestMapping(method = RequestMethod.GET, value = "/omhierarchypurposerefs/{omhierarchypurposeref_id}")
    public ResponseEntity<OMHierarchyPurposeRefDTO> get(@PathVariable("omhierarchypurposeref_id") String omhierarchypurposeref_id) {
        OMHierarchyPurposeRef domain = omhierarchypurposerefService.get(omhierarchypurposeref_id);
        OMHierarchyPurposeRefDTO dto = omhierarchypurposerefMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取组织层次结构分配草稿", tags = {"组织层次结构分配" },  notes = "获取组织层次结构分配草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/omhierarchypurposerefs/getdraft")
    public ResponseEntity<OMHierarchyPurposeRefDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(omhierarchypurposerefMapping.toDto(omhierarchypurposerefService.getDraft(new OMHierarchyPurposeRef())));
    }

    @ApiOperation(value = "检查组织层次结构分配", tags = {"组织层次结构分配" },  notes = "检查组织层次结构分配")
	@RequestMapping(method = RequestMethod.POST, value = "/omhierarchypurposerefs/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody OMHierarchyPurposeRefDTO omhierarchypurposerefdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(omhierarchypurposerefService.checkKey(omhierarchypurposerefMapping.toDomain(omhierarchypurposerefdto)));
    }

    @PreAuthorize("hasPermission(this.omhierarchypurposerefMapping.toDomain(#omhierarchypurposerefdto),'iBizBusinessCentral-OMHierarchyPurposeRef-Save')")
    @ApiOperation(value = "保存组织层次结构分配", tags = {"组织层次结构分配" },  notes = "保存组织层次结构分配")
	@RequestMapping(method = RequestMethod.POST, value = "/omhierarchypurposerefs/save")
    public ResponseEntity<Boolean> save(@RequestBody OMHierarchyPurposeRefDTO omhierarchypurposerefdto) {
        return ResponseEntity.status(HttpStatus.OK).body(omhierarchypurposerefService.save(omhierarchypurposerefMapping.toDomain(omhierarchypurposerefdto)));
    }

    @PreAuthorize("hasPermission(this.omhierarchypurposerefMapping.toDomain(#omhierarchypurposerefdtos),'iBizBusinessCentral-OMHierarchyPurposeRef-Save')")
    @ApiOperation(value = "批量保存组织层次结构分配", tags = {"组织层次结构分配" },  notes = "批量保存组织层次结构分配")
	@RequestMapping(method = RequestMethod.POST, value = "/omhierarchypurposerefs/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<OMHierarchyPurposeRefDTO> omhierarchypurposerefdtos) {
        omhierarchypurposerefService.saveBatch(omhierarchypurposerefMapping.toDomain(omhierarchypurposerefdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OMHierarchyPurposeRef-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OMHierarchyPurposeRef-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"组织层次结构分配" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/omhierarchypurposerefs/fetchdefault")
	public ResponseEntity<List<OMHierarchyPurposeRefDTO>> fetchDefault(OMHierarchyPurposeRefSearchContext context) {
        Page<OMHierarchyPurposeRef> domains = omhierarchypurposerefService.searchDefault(context) ;
        List<OMHierarchyPurposeRefDTO> list = omhierarchypurposerefMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OMHierarchyPurposeRef-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OMHierarchyPurposeRef-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"组织层次结构分配" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/omhierarchypurposerefs/searchdefault")
	public ResponseEntity<Page<OMHierarchyPurposeRefDTO>> searchDefault(@RequestBody OMHierarchyPurposeRefSearchContext context) {
        Page<OMHierarchyPurposeRef> domains = omhierarchypurposerefService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(omhierarchypurposerefMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}


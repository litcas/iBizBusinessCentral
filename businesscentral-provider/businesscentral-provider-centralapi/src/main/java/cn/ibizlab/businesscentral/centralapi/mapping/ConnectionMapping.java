package cn.ibizlab.businesscentral.centralapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.runtime.domain.Connection;
import cn.ibizlab.businesscentral.centralapi.dto.ConnectionDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CentralApiConnectionMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface ConnectionMapping extends MappingBase<ConnectionDTO, Connection> {


}


package cn.ibizlab.businesscentral.centralapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[WebSiteResourceDTO]
 */
@Data
public class WebSiteResourceDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [WEBSITERESOURCEID]
     *
     */
    @JSONField(name = "websiteresourceid")
    @JsonProperty("websiteresourceid")
    private String websiteresourceid;

    /**
     * 属性 [WEBSITERESOURCENAME]
     *
     */
    @JSONField(name = "websiteresourcename")
    @JsonProperty("websiteresourcename")
    private String websiteresourcename;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;

    /**
     * 属性 [WEBSITENAME]
     *
     */
    @JSONField(name = "websitename")
    @JsonProperty("websitename")
    private String websitename;

    /**
     * 属性 [WEBSITEID]
     *
     */
    @JSONField(name = "websiteid")
    @JsonProperty("websiteid")
    private String websiteid;


    /**
     * 设置 [WEBSITERESOURCENAME]
     */
    public void setWebsiteresourcename(String  websiteresourcename){
        this.websiteresourcename = websiteresourcename ;
        this.modify("websiteresourcename",websiteresourcename);
    }

    /**
     * 设置 [WEBSITEID]
     */
    public void setWebsiteid(String  websiteid){
        this.websiteid = websiteid ;
        this.modify("websiteid",websiteid);
    }


}


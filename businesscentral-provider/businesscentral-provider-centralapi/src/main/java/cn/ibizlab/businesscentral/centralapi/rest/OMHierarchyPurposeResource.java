package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.base.domain.OMHierarchyPurpose;
import cn.ibizlab.businesscentral.core.base.service.IOMHierarchyPurposeService;
import cn.ibizlab.businesscentral.core.base.filter.OMHierarchyPurposeSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"组织层次结构应用" })
@RestController("CentralApi-omhierarchypurpose")
@RequestMapping("")
public class OMHierarchyPurposeResource {

    @Autowired
    public IOMHierarchyPurposeService omhierarchypurposeService;

    @Autowired
    @Lazy
    public OMHierarchyPurposeMapping omhierarchypurposeMapping;

    @PreAuthorize("hasPermission(this.omhierarchypurposeMapping.toDomain(#omhierarchypurposedto),'iBizBusinessCentral-OMHierarchyPurpose-Create')")
    @ApiOperation(value = "新建组织层次结构应用", tags = {"组织层次结构应用" },  notes = "新建组织层次结构应用")
	@RequestMapping(method = RequestMethod.POST, value = "/omhierarchypurposes")
    public ResponseEntity<OMHierarchyPurposeDTO> create(@RequestBody OMHierarchyPurposeDTO omhierarchypurposedto) {
        OMHierarchyPurpose domain = omhierarchypurposeMapping.toDomain(omhierarchypurposedto);
		omhierarchypurposeService.create(domain);
        OMHierarchyPurposeDTO dto = omhierarchypurposeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.omhierarchypurposeMapping.toDomain(#omhierarchypurposedtos),'iBizBusinessCentral-OMHierarchyPurpose-Create')")
    @ApiOperation(value = "批量新建组织层次结构应用", tags = {"组织层次结构应用" },  notes = "批量新建组织层次结构应用")
	@RequestMapping(method = RequestMethod.POST, value = "/omhierarchypurposes/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<OMHierarchyPurposeDTO> omhierarchypurposedtos) {
        omhierarchypurposeService.createBatch(omhierarchypurposeMapping.toDomain(omhierarchypurposedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "omhierarchypurpose" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.omhierarchypurposeService.get(#omhierarchypurpose_id),'iBizBusinessCentral-OMHierarchyPurpose-Update')")
    @ApiOperation(value = "更新组织层次结构应用", tags = {"组织层次结构应用" },  notes = "更新组织层次结构应用")
	@RequestMapping(method = RequestMethod.PUT, value = "/omhierarchypurposes/{omhierarchypurpose_id}")
    public ResponseEntity<OMHierarchyPurposeDTO> update(@PathVariable("omhierarchypurpose_id") String omhierarchypurpose_id, @RequestBody OMHierarchyPurposeDTO omhierarchypurposedto) {
		OMHierarchyPurpose domain  = omhierarchypurposeMapping.toDomain(omhierarchypurposedto);
        domain .setOmhierarchypurposeid(omhierarchypurpose_id);
		omhierarchypurposeService.update(domain );
		OMHierarchyPurposeDTO dto = omhierarchypurposeMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.omhierarchypurposeService.getOmhierarchypurposeByEntities(this.omhierarchypurposeMapping.toDomain(#omhierarchypurposedtos)),'iBizBusinessCentral-OMHierarchyPurpose-Update')")
    @ApiOperation(value = "批量更新组织层次结构应用", tags = {"组织层次结构应用" },  notes = "批量更新组织层次结构应用")
	@RequestMapping(method = RequestMethod.PUT, value = "/omhierarchypurposes/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<OMHierarchyPurposeDTO> omhierarchypurposedtos) {
        omhierarchypurposeService.updateBatch(omhierarchypurposeMapping.toDomain(omhierarchypurposedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.omhierarchypurposeService.get(#omhierarchypurpose_id),'iBizBusinessCentral-OMHierarchyPurpose-Remove')")
    @ApiOperation(value = "删除组织层次结构应用", tags = {"组织层次结构应用" },  notes = "删除组织层次结构应用")
	@RequestMapping(method = RequestMethod.DELETE, value = "/omhierarchypurposes/{omhierarchypurpose_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("omhierarchypurpose_id") String omhierarchypurpose_id) {
         return ResponseEntity.status(HttpStatus.OK).body(omhierarchypurposeService.remove(omhierarchypurpose_id));
    }

    @PreAuthorize("hasPermission(this.omhierarchypurposeService.getOmhierarchypurposeByIds(#ids),'iBizBusinessCentral-OMHierarchyPurpose-Remove')")
    @ApiOperation(value = "批量删除组织层次结构应用", tags = {"组织层次结构应用" },  notes = "批量删除组织层次结构应用")
	@RequestMapping(method = RequestMethod.DELETE, value = "/omhierarchypurposes/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        omhierarchypurposeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.omhierarchypurposeMapping.toDomain(returnObject.body),'iBizBusinessCentral-OMHierarchyPurpose-Get')")
    @ApiOperation(value = "获取组织层次结构应用", tags = {"组织层次结构应用" },  notes = "获取组织层次结构应用")
	@RequestMapping(method = RequestMethod.GET, value = "/omhierarchypurposes/{omhierarchypurpose_id}")
    public ResponseEntity<OMHierarchyPurposeDTO> get(@PathVariable("omhierarchypurpose_id") String omhierarchypurpose_id) {
        OMHierarchyPurpose domain = omhierarchypurposeService.get(omhierarchypurpose_id);
        OMHierarchyPurposeDTO dto = omhierarchypurposeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取组织层次结构应用草稿", tags = {"组织层次结构应用" },  notes = "获取组织层次结构应用草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/omhierarchypurposes/getdraft")
    public ResponseEntity<OMHierarchyPurposeDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(omhierarchypurposeMapping.toDto(omhierarchypurposeService.getDraft(new OMHierarchyPurpose())));
    }

    @ApiOperation(value = "检查组织层次结构应用", tags = {"组织层次结构应用" },  notes = "检查组织层次结构应用")
	@RequestMapping(method = RequestMethod.POST, value = "/omhierarchypurposes/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody OMHierarchyPurposeDTO omhierarchypurposedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(omhierarchypurposeService.checkKey(omhierarchypurposeMapping.toDomain(omhierarchypurposedto)));
    }

    @PreAuthorize("hasPermission(this.omhierarchypurposeMapping.toDomain(#omhierarchypurposedto),'iBizBusinessCentral-OMHierarchyPurpose-Save')")
    @ApiOperation(value = "保存组织层次结构应用", tags = {"组织层次结构应用" },  notes = "保存组织层次结构应用")
	@RequestMapping(method = RequestMethod.POST, value = "/omhierarchypurposes/save")
    public ResponseEntity<Boolean> save(@RequestBody OMHierarchyPurposeDTO omhierarchypurposedto) {
        return ResponseEntity.status(HttpStatus.OK).body(omhierarchypurposeService.save(omhierarchypurposeMapping.toDomain(omhierarchypurposedto)));
    }

    @PreAuthorize("hasPermission(this.omhierarchypurposeMapping.toDomain(#omhierarchypurposedtos),'iBizBusinessCentral-OMHierarchyPurpose-Save')")
    @ApiOperation(value = "批量保存组织层次结构应用", tags = {"组织层次结构应用" },  notes = "批量保存组织层次结构应用")
	@RequestMapping(method = RequestMethod.POST, value = "/omhierarchypurposes/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<OMHierarchyPurposeDTO> omhierarchypurposedtos) {
        omhierarchypurposeService.saveBatch(omhierarchypurposeMapping.toDomain(omhierarchypurposedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OMHierarchyPurpose-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OMHierarchyPurpose-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"组织层次结构应用" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/omhierarchypurposes/fetchdefault")
	public ResponseEntity<List<OMHierarchyPurposeDTO>> fetchDefault(OMHierarchyPurposeSearchContext context) {
        Page<OMHierarchyPurpose> domains = omhierarchypurposeService.searchDefault(context) ;
        List<OMHierarchyPurposeDTO> list = omhierarchypurposeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OMHierarchyPurpose-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OMHierarchyPurpose-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"组织层次结构应用" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/omhierarchypurposes/searchdefault")
	public ResponseEntity<Page<OMHierarchyPurposeDTO>> searchDefault(@RequestBody OMHierarchyPurposeSearchContext context) {
        Page<OMHierarchyPurpose> domains = omhierarchypurposeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(omhierarchypurposeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}


package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.sales.domain.SalesLiterature;
import cn.ibizlab.businesscentral.core.sales.service.ISalesLiteratureService;
import cn.ibizlab.businesscentral.core.sales.filter.SalesLiteratureSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"销售宣传资料" })
@RestController("CentralApi-salesliterature")
@RequestMapping("")
public class SalesLiteratureResource {

    @Autowired
    public ISalesLiteratureService salesliteratureService;

    @Autowired
    @Lazy
    public SalesLiteratureMapping salesliteratureMapping;

    @PreAuthorize("hasPermission(this.salesliteratureMapping.toDomain(#salesliteraturedto),'iBizBusinessCentral-SalesLiterature-Create')")
    @ApiOperation(value = "新建销售宣传资料", tags = {"销售宣传资料" },  notes = "新建销售宣传资料")
	@RequestMapping(method = RequestMethod.POST, value = "/salesliteratures")
    public ResponseEntity<SalesLiteratureDTO> create(@RequestBody SalesLiteratureDTO salesliteraturedto) {
        SalesLiterature domain = salesliteratureMapping.toDomain(salesliteraturedto);
		salesliteratureService.create(domain);
        SalesLiteratureDTO dto = salesliteratureMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.salesliteratureMapping.toDomain(#salesliteraturedtos),'iBizBusinessCentral-SalesLiterature-Create')")
    @ApiOperation(value = "批量新建销售宣传资料", tags = {"销售宣传资料" },  notes = "批量新建销售宣传资料")
	@RequestMapping(method = RequestMethod.POST, value = "/salesliteratures/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<SalesLiteratureDTO> salesliteraturedtos) {
        salesliteratureService.createBatch(salesliteratureMapping.toDomain(salesliteraturedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "salesliterature" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.salesliteratureService.get(#salesliterature_id),'iBizBusinessCentral-SalesLiterature-Update')")
    @ApiOperation(value = "更新销售宣传资料", tags = {"销售宣传资料" },  notes = "更新销售宣传资料")
	@RequestMapping(method = RequestMethod.PUT, value = "/salesliteratures/{salesliterature_id}")
    public ResponseEntity<SalesLiteratureDTO> update(@PathVariable("salesliterature_id") String salesliterature_id, @RequestBody SalesLiteratureDTO salesliteraturedto) {
		SalesLiterature domain  = salesliteratureMapping.toDomain(salesliteraturedto);
        domain .setSalesliteratureid(salesliterature_id);
		salesliteratureService.update(domain );
		SalesLiteratureDTO dto = salesliteratureMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.salesliteratureService.getSalesliteratureByEntities(this.salesliteratureMapping.toDomain(#salesliteraturedtos)),'iBizBusinessCentral-SalesLiterature-Update')")
    @ApiOperation(value = "批量更新销售宣传资料", tags = {"销售宣传资料" },  notes = "批量更新销售宣传资料")
	@RequestMapping(method = RequestMethod.PUT, value = "/salesliteratures/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<SalesLiteratureDTO> salesliteraturedtos) {
        salesliteratureService.updateBatch(salesliteratureMapping.toDomain(salesliteraturedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.salesliteratureService.get(#salesliterature_id),'iBizBusinessCentral-SalesLiterature-Remove')")
    @ApiOperation(value = "删除销售宣传资料", tags = {"销售宣传资料" },  notes = "删除销售宣传资料")
	@RequestMapping(method = RequestMethod.DELETE, value = "/salesliteratures/{salesliterature_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("salesliterature_id") String salesliterature_id) {
         return ResponseEntity.status(HttpStatus.OK).body(salesliteratureService.remove(salesliterature_id));
    }

    @PreAuthorize("hasPermission(this.salesliteratureService.getSalesliteratureByIds(#ids),'iBizBusinessCentral-SalesLiterature-Remove')")
    @ApiOperation(value = "批量删除销售宣传资料", tags = {"销售宣传资料" },  notes = "批量删除销售宣传资料")
	@RequestMapping(method = RequestMethod.DELETE, value = "/salesliteratures/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        salesliteratureService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.salesliteratureMapping.toDomain(returnObject.body),'iBizBusinessCentral-SalesLiterature-Get')")
    @ApiOperation(value = "获取销售宣传资料", tags = {"销售宣传资料" },  notes = "获取销售宣传资料")
	@RequestMapping(method = RequestMethod.GET, value = "/salesliteratures/{salesliterature_id}")
    public ResponseEntity<SalesLiteratureDTO> get(@PathVariable("salesliterature_id") String salesliterature_id) {
        SalesLiterature domain = salesliteratureService.get(salesliterature_id);
        SalesLiteratureDTO dto = salesliteratureMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取销售宣传资料草稿", tags = {"销售宣传资料" },  notes = "获取销售宣传资料草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/salesliteratures/getdraft")
    public ResponseEntity<SalesLiteratureDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(salesliteratureMapping.toDto(salesliteratureService.getDraft(new SalesLiterature())));
    }

    @ApiOperation(value = "检查销售宣传资料", tags = {"销售宣传资料" },  notes = "检查销售宣传资料")
	@RequestMapping(method = RequestMethod.POST, value = "/salesliteratures/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody SalesLiteratureDTO salesliteraturedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(salesliteratureService.checkKey(salesliteratureMapping.toDomain(salesliteraturedto)));
    }

    @PreAuthorize("hasPermission(this.salesliteratureMapping.toDomain(#salesliteraturedto),'iBizBusinessCentral-SalesLiterature-Save')")
    @ApiOperation(value = "保存销售宣传资料", tags = {"销售宣传资料" },  notes = "保存销售宣传资料")
	@RequestMapping(method = RequestMethod.POST, value = "/salesliteratures/save")
    public ResponseEntity<Boolean> save(@RequestBody SalesLiteratureDTO salesliteraturedto) {
        return ResponseEntity.status(HttpStatus.OK).body(salesliteratureService.save(salesliteratureMapping.toDomain(salesliteraturedto)));
    }

    @PreAuthorize("hasPermission(this.salesliteratureMapping.toDomain(#salesliteraturedtos),'iBizBusinessCentral-SalesLiterature-Save')")
    @ApiOperation(value = "批量保存销售宣传资料", tags = {"销售宣传资料" },  notes = "批量保存销售宣传资料")
	@RequestMapping(method = RequestMethod.POST, value = "/salesliteratures/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<SalesLiteratureDTO> salesliteraturedtos) {
        salesliteratureService.saveBatch(salesliteratureMapping.toDomain(salesliteraturedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesLiterature-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-SalesLiterature-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"销售宣传资料" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/salesliteratures/fetchdefault")
	public ResponseEntity<List<SalesLiteratureDTO>> fetchDefault(SalesLiteratureSearchContext context) {
        Page<SalesLiterature> domains = salesliteratureService.searchDefault(context) ;
        List<SalesLiteratureDTO> list = salesliteratureMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesLiterature-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-SalesLiterature-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"销售宣传资料" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/salesliteratures/searchdefault")
	public ResponseEntity<Page<SalesLiteratureDTO>> searchDefault(@RequestBody SalesLiteratureSearchContext context) {
        Page<SalesLiterature> domains = salesliteratureService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesliteratureMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}


package cn.ibizlab.businesscentral.centralapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[IBizListDTO]
 */
@Data
public class IBizListDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [QUERY]
     *
     */
    @JSONField(name = "query")
    @JsonProperty("query")
    private String query;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;

    /**
     * 属性 [LOCKSTATUS]
     *
     */
    @JSONField(name = "lockstatus")
    @JsonProperty("lockstatus")
    private Integer lockstatus;

    /**
     * 属性 [PURPOSE]
     *
     */
    @JSONField(name = "purpose")
    @JsonProperty("purpose")
    private String purpose;

    /**
     * 属性 [LASTUSEDON]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "lastusedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("lastusedon")
    private Timestamp lastusedon;

    /**
     * 属性 [OWNERID]
     *
     */
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;

    /**
     * 属性 [OWNERNAME]
     *
     */
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;

    /**
     * 属性 [LISTID]
     *
     */
    @JSONField(name = "listid")
    @JsonProperty("listid")
    private String listid;

    /**
     * 属性 [MEMBERTYPE]
     *
     */
    @JSONField(name = "membertype")
    @JsonProperty("membertype")
    private Integer membertype;

    /**
     * 属性 [DONOTSENDONOPTOUT]
     *
     */
    @JSONField(name = "donotsendonoptout")
    @JsonProperty("donotsendonoptout")
    private Integer donotsendonoptout;

    /**
     * 属性 [VERSIONNUMBER]
     *
     */
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;

    /**
     * 属性 [COST]
     *
     */
    @JSONField(name = "cost")
    @JsonProperty("cost")
    private BigDecimal cost;

    /**
     * 属性 [STATECODE]
     *
     */
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;

    /**
     * 属性 [EXCHANGERATE]
     *
     */
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;

    /**
     * 属性 [OWNERTYPE]
     *
     */
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;

    /**
     * 属性 [LISTNAME]
     *
     */
    @JSONField(name = "listname")
    @JsonProperty("listname")
    private String listname;

    /**
     * 属性 [STATUSCODE]
     *
     */
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 属性 [TYPE]
     *
     */
    @JSONField(name = "type")
    @JsonProperty("type")
    private Integer type;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [IMPORTSEQUENCENUMBER]
     *
     */
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;

    /**
     * 属性 [PROCESSID]
     *
     */
    @JSONField(name = "processid")
    @JsonProperty("processid")
    private String processid;

    /**
     * 属性 [COST_BASE]
     *
     */
    @JSONField(name = "cost_base")
    @JsonProperty("cost_base")
    private BigDecimal costBase;

    /**
     * 属性 [MEMBERCOUNT]
     *
     */
    @JSONField(name = "membercount")
    @JsonProperty("membercount")
    private Integer membercount;

    /**
     * 属性 [IGNOREINACTIVELISTMEMBERS]
     *
     */
    @JSONField(name = "ignoreinactivelistmembers")
    @JsonProperty("ignoreinactivelistmembers")
    private Integer ignoreinactivelistmembers;

    /**
     * 属性 [STAGEID]
     *
     */
    @JSONField(name = "stageid")
    @JsonProperty("stageid")
    private String stageid;

    /**
     * 属性 [SOURCE]
     *
     */
    @JSONField(name = "source")
    @JsonProperty("source")
    private String source;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [UTCCONVERSIONTIMEZONECODE]
     *
     */
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;

    /**
     * 属性 [CREATEDFROMCODE]
     *
     */
    @JSONField(name = "createdfromcode")
    @JsonProperty("createdfromcode")
    private String createdfromcode;

    /**
     * 属性 [OVERRIDDENCREATEDON]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;

    /**
     * 属性 [TIMEZONERULEVERSIONNUMBER]
     *
     */
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;

    /**
     * 属性 [TRAVERSEDPATH]
     *
     */
    @JSONField(name = "traversedpath")
    @JsonProperty("traversedpath")
    private String traversedpath;

    /**
     * 属性 [CURRENCYNAME]
     *
     */
    @JSONField(name = "currencyname")
    @JsonProperty("currencyname")
    private String currencyname;

    /**
     * 属性 [TRANSACTIONCURRENCYID]
     *
     */
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;


    /**
     * 设置 [QUERY]
     */
    public void setQuery(String  query){
        this.query = query ;
        this.modify("query",query);
    }

    /**
     * 设置 [LOCKSTATUS]
     */
    public void setLockstatus(Integer  lockstatus){
        this.lockstatus = lockstatus ;
        this.modify("lockstatus",lockstatus);
    }

    /**
     * 设置 [PURPOSE]
     */
    public void setPurpose(String  purpose){
        this.purpose = purpose ;
        this.modify("purpose",purpose);
    }

    /**
     * 设置 [LASTUSEDON]
     */
    public void setLastusedon(Timestamp  lastusedon){
        this.lastusedon = lastusedon ;
        this.modify("lastusedon",lastusedon);
    }

    /**
     * 设置 [OWNERID]
     */
    public void setOwnerid(String  ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [OWNERNAME]
     */
    public void setOwnername(String  ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [MEMBERTYPE]
     */
    public void setMembertype(Integer  membertype){
        this.membertype = membertype ;
        this.modify("membertype",membertype);
    }

    /**
     * 设置 [DONOTSENDONOPTOUT]
     */
    public void setDonotsendonoptout(Integer  donotsendonoptout){
        this.donotsendonoptout = donotsendonoptout ;
        this.modify("donotsendonoptout",donotsendonoptout);
    }

    /**
     * 设置 [VERSIONNUMBER]
     */
    public void setVersionnumber(BigInteger  versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [COST]
     */
    public void setCost(BigDecimal  cost){
        this.cost = cost ;
        this.modify("cost",cost);
    }

    /**
     * 设置 [STATECODE]
     */
    public void setStatecode(Integer  statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [EXCHANGERATE]
     */
    public void setExchangerate(BigDecimal  exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [OWNERTYPE]
     */
    public void setOwnertype(String  ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [LISTNAME]
     */
    public void setListname(String  listname){
        this.listname = listname ;
        this.modify("listname",listname);
    }

    /**
     * 设置 [STATUSCODE]
     */
    public void setStatuscode(Integer  statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [TYPE]
     */
    public void setType(Integer  type){
        this.type = type ;
        this.modify("type",type);
    }

    /**
     * 设置 [IMPORTSEQUENCENUMBER]
     */
    public void setImportsequencenumber(Integer  importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [PROCESSID]
     */
    public void setProcessid(String  processid){
        this.processid = processid ;
        this.modify("processid",processid);
    }

    /**
     * 设置 [COST_BASE]
     */
    public void setCostBase(BigDecimal  costBase){
        this.costBase = costBase ;
        this.modify("cost_base",costBase);
    }

    /**
     * 设置 [MEMBERCOUNT]
     */
    public void setMembercount(Integer  membercount){
        this.membercount = membercount ;
        this.modify("membercount",membercount);
    }

    /**
     * 设置 [IGNOREINACTIVELISTMEMBERS]
     */
    public void setIgnoreinactivelistmembers(Integer  ignoreinactivelistmembers){
        this.ignoreinactivelistmembers = ignoreinactivelistmembers ;
        this.modify("ignoreinactivelistmembers",ignoreinactivelistmembers);
    }

    /**
     * 设置 [STAGEID]
     */
    public void setStageid(String  stageid){
        this.stageid = stageid ;
        this.modify("stageid",stageid);
    }

    /**
     * 设置 [SOURCE]
     */
    public void setSource(String  source){
        this.source = source ;
        this.modify("source",source);
    }

    /**
     * 设置 [UTCCONVERSIONTIMEZONECODE]
     */
    public void setUtcconversiontimezonecode(Integer  utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [CREATEDFROMCODE]
     */
    public void setCreatedfromcode(String  createdfromcode){
        this.createdfromcode = createdfromcode ;
        this.modify("createdfromcode",createdfromcode);
    }

    /**
     * 设置 [OVERRIDDENCREATEDON]
     */
    public void setOverriddencreatedon(Timestamp  overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 设置 [TIMEZONERULEVERSIONNUMBER]
     */
    public void setTimezoneruleversionnumber(Integer  timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [TRAVERSEDPATH]
     */
    public void setTraversedpath(String  traversedpath){
        this.traversedpath = traversedpath ;
        this.modify("traversedpath",traversedpath);
    }

    /**
     * 设置 [TRANSACTIONCURRENCYID]
     */
    public void setTransactioncurrencyid(String  transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }


}


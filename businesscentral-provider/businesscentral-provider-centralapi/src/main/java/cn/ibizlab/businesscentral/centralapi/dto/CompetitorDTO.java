package cn.ibizlab.businesscentral.centralapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[CompetitorDTO]
 */
@Data
public class CompetitorDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ENTITYIMAGE]
     *
     */
    @JSONField(name = "entityimage")
    @JsonProperty("entityimage")
    private String entityimage;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [ADDRESS2_SHIPPINGMETHODCODE]
     *
     */
    @JSONField(name = "address2_shippingmethodcode")
    @JsonProperty("address2_shippingmethodcode")
    private String address2Shippingmethodcode;

    /**
     * 属性 [ADDRESS2_COUNTRY]
     *
     */
    @JSONField(name = "address2_country")
    @JsonProperty("address2_country")
    private String address2Country;

    /**
     * 属性 [ADDRESS2_TELEPHONE2]
     *
     */
    @JSONField(name = "address2_telephone2")
    @JsonProperty("address2_telephone2")
    private String address2Telephone2;

    /**
     * 属性 [WINPERCENTAGE]
     *
     */
    @JSONField(name = "winpercentage")
    @JsonProperty("winpercentage")
    private Double winpercentage;

    /**
     * 属性 [ADDRESS1_SHIPPINGMETHODCODE]
     *
     */
    @JSONField(name = "address1_shippingmethodcode")
    @JsonProperty("address1_shippingmethodcode")
    private String address1Shippingmethodcode;

    /**
     * 属性 [REFERENCEINFOURL]
     *
     */
    @JSONField(name = "referenceinfourl")
    @JsonProperty("referenceinfourl")
    private String referenceinfourl;

    /**
     * 属性 [ADDRESS2_LATITUDE]
     *
     */
    @JSONField(name = "address2_latitude")
    @JsonProperty("address2_latitude")
    private Double address2Latitude;

    /**
     * 属性 [VERSIONNUMBER]
     *
     */
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;

    /**
     * 属性 [ADDRESS2_COUNTY]
     *
     */
    @JSONField(name = "address2_county")
    @JsonProperty("address2_county")
    private String address2County;

    /**
     * 属性 [IMPORTSEQUENCENUMBER]
     *
     */
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;

    /**
     * 属性 [ADDRESS2_TELEPHONE3]
     *
     */
    @JSONField(name = "address2_telephone3")
    @JsonProperty("address2_telephone3")
    private String address2Telephone3;

    /**
     * 属性 [TRAVERSEDPATH]
     *
     */
    @JSONField(name = "traversedpath")
    @JsonProperty("traversedpath")
    private String traversedpath;

    /**
     * 属性 [ADDRESS1_CITY]
     *
     */
    @JSONField(name = "address1_city")
    @JsonProperty("address1_city")
    private String address1City;

    /**
     * 属性 [REPORTEDREVENUE_BASE]
     *
     */
    @JSONField(name = "reportedrevenue_base")
    @JsonProperty("reportedrevenue_base")
    private BigDecimal reportedrevenueBase;

    /**
     * 属性 [COMPETITORID]
     *
     */
    @JSONField(name = "competitorid")
    @JsonProperty("competitorid")
    private String competitorid;

    /**
     * 属性 [ADDRESS1_FAX]
     *
     */
    @JSONField(name = "address1_fax")
    @JsonProperty("address1_fax")
    private String address1Fax;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;

    /**
     * 属性 [OVERVIEW]
     *
     */
    @JSONField(name = "overview")
    @JsonProperty("overview")
    private String overview;

    /**
     * 属性 [ADDRESS2_STATEORPROVINCE]
     *
     */
    @JSONField(name = "address2_stateorprovince")
    @JsonProperty("address2_stateorprovince")
    private String address2Stateorprovince;

    /**
     * 属性 [ENTITYIMAGEID]
     *
     */
    @JSONField(name = "entityimageid")
    @JsonProperty("entityimageid")
    private String entityimageid;

    /**
     * 属性 [TIMEZONERULEVERSIONNUMBER]
     *
     */
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;

    /**
     * 属性 [ADDRESS2_UPSZONE]
     *
     */
    @JSONField(name = "address2_upszone")
    @JsonProperty("address2_upszone")
    private String address2Upszone;

    /**
     * 属性 [ADDRESS1_LINE3]
     *
     */
    @JSONField(name = "address1_line3")
    @JsonProperty("address1_line3")
    private String address1Line3;

    /**
     * 属性 [STOCKEXCHANGE]
     *
     */
    @JSONField(name = "stockexchange")
    @JsonProperty("stockexchange")
    private String stockexchange;

    /**
     * 属性 [STAGEID]
     *
     */
    @JSONField(name = "stageid")
    @JsonProperty("stageid")
    private String stageid;

    /**
     * 属性 [REPORTINGYEAR]
     *
     */
    @JSONField(name = "reportingyear")
    @JsonProperty("reportingyear")
    private Integer reportingyear;

    /**
     * 属性 [REPORTINGQUARTER]
     *
     */
    @JSONField(name = "reportingquarter")
    @JsonProperty("reportingquarter")
    private Integer reportingquarter;

    /**
     * 属性 [ADDRESS1_STATEORPROVINCE]
     *
     */
    @JSONField(name = "address1_stateorprovince")
    @JsonProperty("address1_stateorprovince")
    private String address1Stateorprovince;

    /**
     * 属性 [ADDRESS1_UTCOFFSET]
     *
     */
    @JSONField(name = "address1_utcoffset")
    @JsonProperty("address1_utcoffset")
    private Integer address1Utcoffset;

    /**
     * 属性 [ADDRESS1_LATITUDE]
     *
     */
    @JSONField(name = "address1_latitude")
    @JsonProperty("address1_latitude")
    private Double address1Latitude;

    /**
     * 属性 [ADDRESS1_COMPOSITE]
     *
     */
    @JSONField(name = "address1_composite")
    @JsonProperty("address1_composite")
    private String address1Composite;

    /**
     * 属性 [ADDRESS1_COUNTY]
     *
     */
    @JSONField(name = "address1_county")
    @JsonProperty("address1_county")
    private String address1County;

    /**
     * 属性 [ADDRESS1_NAME]
     *
     */
    @JSONField(name = "address1_name")
    @JsonProperty("address1_name")
    private String address1Name;

    /**
     * 属性 [ADDRESS1_POSTALCODE]
     *
     */
    @JSONField(name = "address1_postalcode")
    @JsonProperty("address1_postalcode")
    private String address1Postalcode;

    /**
     * 属性 [OPPORTUNITIES]
     *
     */
    @JSONField(name = "opportunities")
    @JsonProperty("opportunities")
    private String opportunities;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [ADDRESS1_ADDRESSID]
     *
     */
    @JSONField(name = "address1_addressid")
    @JsonProperty("address1_addressid")
    private String address1Addressid;

    /**
     * 属性 [ADDRESS2_LINE3]
     *
     */
    @JSONField(name = "address2_line3")
    @JsonProperty("address2_line3")
    private String address2Line3;

    /**
     * 属性 [ADDRESS2_FAX]
     *
     */
    @JSONField(name = "address2_fax")
    @JsonProperty("address2_fax")
    private String address2Fax;

    /**
     * 属性 [PROCESSID]
     *
     */
    @JSONField(name = "processid")
    @JsonProperty("processid")
    private String processid;

    /**
     * 属性 [ADDRESS2_POSTOFFICEBOX]
     *
     */
    @JSONField(name = "address2_postofficebox")
    @JsonProperty("address2_postofficebox")
    private String address2Postofficebox;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;

    /**
     * 属性 [EXCHANGERATE]
     *
     */
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;

    /**
     * 属性 [OVERRIDDENCREATEDON]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;

    /**
     * 属性 [ADDRESS1_TELEPHONE2]
     *
     */
    @JSONField(name = "address1_telephone2")
    @JsonProperty("address1_telephone2")
    private String address1Telephone2;

    /**
     * 属性 [STRENGTHS]
     *
     */
    @JSONField(name = "strengths")
    @JsonProperty("strengths")
    private String strengths;

    /**
     * 属性 [ADDRESS2_ADDRESSTYPECODE]
     *
     */
    @JSONField(name = "address2_addresstypecode")
    @JsonProperty("address2_addresstypecode")
    private String address2Addresstypecode;

    /**
     * 属性 [ADDRESS1_POSTOFFICEBOX]
     *
     */
    @JSONField(name = "address1_postofficebox")
    @JsonProperty("address1_postofficebox")
    private String address1Postofficebox;

    /**
     * 属性 [WEAKNESSES]
     *
     */
    @JSONField(name = "weaknesses")
    @JsonProperty("weaknesses")
    private String weaknesses;

    /**
     * 属性 [ADDRESS1_LINE2]
     *
     */
    @JSONField(name = "address1_line2")
    @JsonProperty("address1_line2")
    private String address1Line2;

    /**
     * 属性 [ADDRESS2_LONGITUDE]
     *
     */
    @JSONField(name = "address2_longitude")
    @JsonProperty("address2_longitude")
    private Double address2Longitude;

    /**
     * 属性 [THREATS]
     *
     */
    @JSONField(name = "threats")
    @JsonProperty("threats")
    private String threats;

    /**
     * 属性 [ADDRESS2_COMPOSITE]
     *
     */
    @JSONField(name = "address2_composite")
    @JsonProperty("address2_composite")
    private String address2Composite;

    /**
     * 属性 [ENTITYIMAGE_TIMESTAMP]
     *
     */
    @JSONField(name = "entityimage_timestamp")
    @JsonProperty("entityimage_timestamp")
    private BigInteger entityimageTimestamp;

    /**
     * 属性 [ADDRESS2_UTCOFFSET]
     *
     */
    @JSONField(name = "address2_utcoffset")
    @JsonProperty("address2_utcoffset")
    private Integer address2Utcoffset;

    /**
     * 属性 [ADDRESS1_COUNTRY]
     *
     */
    @JSONField(name = "address1_country")
    @JsonProperty("address1_country")
    private String address1Country;

    /**
     * 属性 [UTCCONVERSIONTIMEZONECODE]
     *
     */
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;

    /**
     * 属性 [REPORTEDREVENUE]
     *
     */
    @JSONField(name = "reportedrevenue")
    @JsonProperty("reportedrevenue")
    private BigDecimal reportedrevenue;

    /**
     * 属性 [ADDRESS1_LONGITUDE]
     *
     */
    @JSONField(name = "address1_longitude")
    @JsonProperty("address1_longitude")
    private Double address1Longitude;

    /**
     * 属性 [TICKERSYMBOL]
     *
     */
    @JSONField(name = "tickersymbol")
    @JsonProperty("tickersymbol")
    private String tickersymbol;

    /**
     * 属性 [ADDRESS2_TELEPHONE1]
     *
     */
    @JSONField(name = "address2_telephone1")
    @JsonProperty("address2_telephone1")
    private String address2Telephone1;

    /**
     * 属性 [KEYPRODUCT]
     *
     */
    @JSONField(name = "keyproduct")
    @JsonProperty("keyproduct")
    private String keyproduct;

    /**
     * 属性 [ENTITYIMAGE_URL]
     *
     */
    @JSONField(name = "entityimage_url")
    @JsonProperty("entityimage_url")
    private String entityimageUrl;

    /**
     * 属性 [ADDRESS2_CITY]
     *
     */
    @JSONField(name = "address2_city")
    @JsonProperty("address2_city")
    private String address2City;

    /**
     * 属性 [WEBSITEURL]
     *
     */
    @JSONField(name = "websiteurl")
    @JsonProperty("websiteurl")
    private String websiteurl;

    /**
     * 属性 [ADDRESS2_ADDRESSID]
     *
     */
    @JSONField(name = "address2_addressid")
    @JsonProperty("address2_addressid")
    private String address2Addressid;

    /**
     * 属性 [ADDRESS1_TELEPHONE1]
     *
     */
    @JSONField(name = "address1_telephone1")
    @JsonProperty("address1_telephone1")
    private String address1Telephone1;

    /**
     * 属性 [ADDRESS2_POSTALCODE]
     *
     */
    @JSONField(name = "address2_postalcode")
    @JsonProperty("address2_postalcode")
    private String address2Postalcode;

    /**
     * 属性 [ADDRESS1_LINE1]
     *
     */
    @JSONField(name = "address1_line1")
    @JsonProperty("address1_line1")
    private String address1Line1;

    /**
     * 属性 [ADDRESS1_TELEPHONE3]
     *
     */
    @JSONField(name = "address1_telephone3")
    @JsonProperty("address1_telephone3")
    private String address1Telephone3;

    /**
     * 属性 [ADDRESS2_LINE1]
     *
     */
    @JSONField(name = "address2_line1")
    @JsonProperty("address2_line1")
    private String address2Line1;

    /**
     * 属性 [ADDRESS1_ADDRESSTYPECODE]
     *
     */
    @JSONField(name = "address1_addresstypecode")
    @JsonProperty("address1_addresstypecode")
    private String address1Addresstypecode;

    /**
     * 属性 [ADDRESS2_NAME]
     *
     */
    @JSONField(name = "address2_name")
    @JsonProperty("address2_name")
    private String address2Name;

    /**
     * 属性 [COMPETITORNAME]
     *
     */
    @JSONField(name = "competitorname")
    @JsonProperty("competitorname")
    private String competitorname;

    /**
     * 属性 [ADDRESS1_UPSZONE]
     *
     */
    @JSONField(name = "address1_upszone")
    @JsonProperty("address1_upszone")
    private String address1Upszone;

    /**
     * 属性 [ADDRESS2_LINE2]
     *
     */
    @JSONField(name = "address2_line2")
    @JsonProperty("address2_line2")
    private String address2Line2;

    /**
     * 属性 [CURRENCYNAME]
     *
     */
    @JSONField(name = "currencyname")
    @JsonProperty("currencyname")
    private String currencyname;

    /**
     * 属性 [TRANSACTIONCURRENCYID]
     *
     */
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;


    /**
     * 设置 [ENTITYIMAGE]
     */
    public void setEntityimage(String  entityimage){
        this.entityimage = entityimage ;
        this.modify("entityimage",entityimage);
    }

    /**
     * 设置 [ADDRESS2_SHIPPINGMETHODCODE]
     */
    public void setAddress2Shippingmethodcode(String  address2Shippingmethodcode){
        this.address2Shippingmethodcode = address2Shippingmethodcode ;
        this.modify("address2_shippingmethodcode",address2Shippingmethodcode);
    }

    /**
     * 设置 [ADDRESS2_COUNTRY]
     */
    public void setAddress2Country(String  address2Country){
        this.address2Country = address2Country ;
        this.modify("address2_country",address2Country);
    }

    /**
     * 设置 [ADDRESS2_TELEPHONE2]
     */
    public void setAddress2Telephone2(String  address2Telephone2){
        this.address2Telephone2 = address2Telephone2 ;
        this.modify("address2_telephone2",address2Telephone2);
    }

    /**
     * 设置 [WINPERCENTAGE]
     */
    public void setWinpercentage(Double  winpercentage){
        this.winpercentage = winpercentage ;
        this.modify("winpercentage",winpercentage);
    }

    /**
     * 设置 [ADDRESS1_SHIPPINGMETHODCODE]
     */
    public void setAddress1Shippingmethodcode(String  address1Shippingmethodcode){
        this.address1Shippingmethodcode = address1Shippingmethodcode ;
        this.modify("address1_shippingmethodcode",address1Shippingmethodcode);
    }

    /**
     * 设置 [REFERENCEINFOURL]
     */
    public void setReferenceinfourl(String  referenceinfourl){
        this.referenceinfourl = referenceinfourl ;
        this.modify("referenceinfourl",referenceinfourl);
    }

    /**
     * 设置 [ADDRESS2_LATITUDE]
     */
    public void setAddress2Latitude(Double  address2Latitude){
        this.address2Latitude = address2Latitude ;
        this.modify("address2_latitude",address2Latitude);
    }

    /**
     * 设置 [VERSIONNUMBER]
     */
    public void setVersionnumber(BigInteger  versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [ADDRESS2_COUNTY]
     */
    public void setAddress2County(String  address2County){
        this.address2County = address2County ;
        this.modify("address2_county",address2County);
    }

    /**
     * 设置 [IMPORTSEQUENCENUMBER]
     */
    public void setImportsequencenumber(Integer  importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [ADDRESS2_TELEPHONE3]
     */
    public void setAddress2Telephone3(String  address2Telephone3){
        this.address2Telephone3 = address2Telephone3 ;
        this.modify("address2_telephone3",address2Telephone3);
    }

    /**
     * 设置 [TRAVERSEDPATH]
     */
    public void setTraversedpath(String  traversedpath){
        this.traversedpath = traversedpath ;
        this.modify("traversedpath",traversedpath);
    }

    /**
     * 设置 [ADDRESS1_CITY]
     */
    public void setAddress1City(String  address1City){
        this.address1City = address1City ;
        this.modify("address1_city",address1City);
    }

    /**
     * 设置 [REPORTEDREVENUE_BASE]
     */
    public void setReportedrevenueBase(BigDecimal  reportedrevenueBase){
        this.reportedrevenueBase = reportedrevenueBase ;
        this.modify("reportedrevenue_base",reportedrevenueBase);
    }

    /**
     * 设置 [ADDRESS1_FAX]
     */
    public void setAddress1Fax(String  address1Fax){
        this.address1Fax = address1Fax ;
        this.modify("address1_fax",address1Fax);
    }

    /**
     * 设置 [OVERVIEW]
     */
    public void setOverview(String  overview){
        this.overview = overview ;
        this.modify("overview",overview);
    }

    /**
     * 设置 [ADDRESS2_STATEORPROVINCE]
     */
    public void setAddress2Stateorprovince(String  address2Stateorprovince){
        this.address2Stateorprovince = address2Stateorprovince ;
        this.modify("address2_stateorprovince",address2Stateorprovince);
    }

    /**
     * 设置 [ENTITYIMAGEID]
     */
    public void setEntityimageid(String  entityimageid){
        this.entityimageid = entityimageid ;
        this.modify("entityimageid",entityimageid);
    }

    /**
     * 设置 [TIMEZONERULEVERSIONNUMBER]
     */
    public void setTimezoneruleversionnumber(Integer  timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [ADDRESS2_UPSZONE]
     */
    public void setAddress2Upszone(String  address2Upszone){
        this.address2Upszone = address2Upszone ;
        this.modify("address2_upszone",address2Upszone);
    }

    /**
     * 设置 [ADDRESS1_LINE3]
     */
    public void setAddress1Line3(String  address1Line3){
        this.address1Line3 = address1Line3 ;
        this.modify("address1_line3",address1Line3);
    }

    /**
     * 设置 [STOCKEXCHANGE]
     */
    public void setStockexchange(String  stockexchange){
        this.stockexchange = stockexchange ;
        this.modify("stockexchange",stockexchange);
    }

    /**
     * 设置 [STAGEID]
     */
    public void setStageid(String  stageid){
        this.stageid = stageid ;
        this.modify("stageid",stageid);
    }

    /**
     * 设置 [REPORTINGYEAR]
     */
    public void setReportingyear(Integer  reportingyear){
        this.reportingyear = reportingyear ;
        this.modify("reportingyear",reportingyear);
    }

    /**
     * 设置 [REPORTINGQUARTER]
     */
    public void setReportingquarter(Integer  reportingquarter){
        this.reportingquarter = reportingquarter ;
        this.modify("reportingquarter",reportingquarter);
    }

    /**
     * 设置 [ADDRESS1_STATEORPROVINCE]
     */
    public void setAddress1Stateorprovince(String  address1Stateorprovince){
        this.address1Stateorprovince = address1Stateorprovince ;
        this.modify("address1_stateorprovince",address1Stateorprovince);
    }

    /**
     * 设置 [ADDRESS1_UTCOFFSET]
     */
    public void setAddress1Utcoffset(Integer  address1Utcoffset){
        this.address1Utcoffset = address1Utcoffset ;
        this.modify("address1_utcoffset",address1Utcoffset);
    }

    /**
     * 设置 [ADDRESS1_LATITUDE]
     */
    public void setAddress1Latitude(Double  address1Latitude){
        this.address1Latitude = address1Latitude ;
        this.modify("address1_latitude",address1Latitude);
    }

    /**
     * 设置 [ADDRESS1_COMPOSITE]
     */
    public void setAddress1Composite(String  address1Composite){
        this.address1Composite = address1Composite ;
        this.modify("address1_composite",address1Composite);
    }

    /**
     * 设置 [ADDRESS1_COUNTY]
     */
    public void setAddress1County(String  address1County){
        this.address1County = address1County ;
        this.modify("address1_county",address1County);
    }

    /**
     * 设置 [ADDRESS1_NAME]
     */
    public void setAddress1Name(String  address1Name){
        this.address1Name = address1Name ;
        this.modify("address1_name",address1Name);
    }

    /**
     * 设置 [ADDRESS1_POSTALCODE]
     */
    public void setAddress1Postalcode(String  address1Postalcode){
        this.address1Postalcode = address1Postalcode ;
        this.modify("address1_postalcode",address1Postalcode);
    }

    /**
     * 设置 [OPPORTUNITIES]
     */
    public void setOpportunities(String  opportunities){
        this.opportunities = opportunities ;
        this.modify("opportunities",opportunities);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [ADDRESS1_ADDRESSID]
     */
    public void setAddress1Addressid(String  address1Addressid){
        this.address1Addressid = address1Addressid ;
        this.modify("address1_addressid",address1Addressid);
    }

    /**
     * 设置 [ADDRESS2_LINE3]
     */
    public void setAddress2Line3(String  address2Line3){
        this.address2Line3 = address2Line3 ;
        this.modify("address2_line3",address2Line3);
    }

    /**
     * 设置 [ADDRESS2_FAX]
     */
    public void setAddress2Fax(String  address2Fax){
        this.address2Fax = address2Fax ;
        this.modify("address2_fax",address2Fax);
    }

    /**
     * 设置 [PROCESSID]
     */
    public void setProcessid(String  processid){
        this.processid = processid ;
        this.modify("processid",processid);
    }

    /**
     * 设置 [ADDRESS2_POSTOFFICEBOX]
     */
    public void setAddress2Postofficebox(String  address2Postofficebox){
        this.address2Postofficebox = address2Postofficebox ;
        this.modify("address2_postofficebox",address2Postofficebox);
    }

    /**
     * 设置 [EXCHANGERATE]
     */
    public void setExchangerate(BigDecimal  exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [OVERRIDDENCREATEDON]
     */
    public void setOverriddencreatedon(Timestamp  overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 设置 [ADDRESS1_TELEPHONE2]
     */
    public void setAddress1Telephone2(String  address1Telephone2){
        this.address1Telephone2 = address1Telephone2 ;
        this.modify("address1_telephone2",address1Telephone2);
    }

    /**
     * 设置 [STRENGTHS]
     */
    public void setStrengths(String  strengths){
        this.strengths = strengths ;
        this.modify("strengths",strengths);
    }

    /**
     * 设置 [ADDRESS2_ADDRESSTYPECODE]
     */
    public void setAddress2Addresstypecode(String  address2Addresstypecode){
        this.address2Addresstypecode = address2Addresstypecode ;
        this.modify("address2_addresstypecode",address2Addresstypecode);
    }

    /**
     * 设置 [ADDRESS1_POSTOFFICEBOX]
     */
    public void setAddress1Postofficebox(String  address1Postofficebox){
        this.address1Postofficebox = address1Postofficebox ;
        this.modify("address1_postofficebox",address1Postofficebox);
    }

    /**
     * 设置 [WEAKNESSES]
     */
    public void setWeaknesses(String  weaknesses){
        this.weaknesses = weaknesses ;
        this.modify("weaknesses",weaknesses);
    }

    /**
     * 设置 [ADDRESS1_LINE2]
     */
    public void setAddress1Line2(String  address1Line2){
        this.address1Line2 = address1Line2 ;
        this.modify("address1_line2",address1Line2);
    }

    /**
     * 设置 [ADDRESS2_LONGITUDE]
     */
    public void setAddress2Longitude(Double  address2Longitude){
        this.address2Longitude = address2Longitude ;
        this.modify("address2_longitude",address2Longitude);
    }

    /**
     * 设置 [THREATS]
     */
    public void setThreats(String  threats){
        this.threats = threats ;
        this.modify("threats",threats);
    }

    /**
     * 设置 [ADDRESS2_COMPOSITE]
     */
    public void setAddress2Composite(String  address2Composite){
        this.address2Composite = address2Composite ;
        this.modify("address2_composite",address2Composite);
    }

    /**
     * 设置 [ENTITYIMAGE_TIMESTAMP]
     */
    public void setEntityimageTimestamp(BigInteger  entityimageTimestamp){
        this.entityimageTimestamp = entityimageTimestamp ;
        this.modify("entityimage_timestamp",entityimageTimestamp);
    }

    /**
     * 设置 [ADDRESS2_UTCOFFSET]
     */
    public void setAddress2Utcoffset(Integer  address2Utcoffset){
        this.address2Utcoffset = address2Utcoffset ;
        this.modify("address2_utcoffset",address2Utcoffset);
    }

    /**
     * 设置 [ADDRESS1_COUNTRY]
     */
    public void setAddress1Country(String  address1Country){
        this.address1Country = address1Country ;
        this.modify("address1_country",address1Country);
    }

    /**
     * 设置 [UTCCONVERSIONTIMEZONECODE]
     */
    public void setUtcconversiontimezonecode(Integer  utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [REPORTEDREVENUE]
     */
    public void setReportedrevenue(BigDecimal  reportedrevenue){
        this.reportedrevenue = reportedrevenue ;
        this.modify("reportedrevenue",reportedrevenue);
    }

    /**
     * 设置 [ADDRESS1_LONGITUDE]
     */
    public void setAddress1Longitude(Double  address1Longitude){
        this.address1Longitude = address1Longitude ;
        this.modify("address1_longitude",address1Longitude);
    }

    /**
     * 设置 [TICKERSYMBOL]
     */
    public void setTickersymbol(String  tickersymbol){
        this.tickersymbol = tickersymbol ;
        this.modify("tickersymbol",tickersymbol);
    }

    /**
     * 设置 [ADDRESS2_TELEPHONE1]
     */
    public void setAddress2Telephone1(String  address2Telephone1){
        this.address2Telephone1 = address2Telephone1 ;
        this.modify("address2_telephone1",address2Telephone1);
    }

    /**
     * 设置 [KEYPRODUCT]
     */
    public void setKeyproduct(String  keyproduct){
        this.keyproduct = keyproduct ;
        this.modify("keyproduct",keyproduct);
    }

    /**
     * 设置 [ENTITYIMAGE_URL]
     */
    public void setEntityimageUrl(String  entityimageUrl){
        this.entityimageUrl = entityimageUrl ;
        this.modify("entityimage_url",entityimageUrl);
    }

    /**
     * 设置 [ADDRESS2_CITY]
     */
    public void setAddress2City(String  address2City){
        this.address2City = address2City ;
        this.modify("address2_city",address2City);
    }

    /**
     * 设置 [WEBSITEURL]
     */
    public void setWebsiteurl(String  websiteurl){
        this.websiteurl = websiteurl ;
        this.modify("websiteurl",websiteurl);
    }

    /**
     * 设置 [ADDRESS2_ADDRESSID]
     */
    public void setAddress2Addressid(String  address2Addressid){
        this.address2Addressid = address2Addressid ;
        this.modify("address2_addressid",address2Addressid);
    }

    /**
     * 设置 [ADDRESS1_TELEPHONE1]
     */
    public void setAddress1Telephone1(String  address1Telephone1){
        this.address1Telephone1 = address1Telephone1 ;
        this.modify("address1_telephone1",address1Telephone1);
    }

    /**
     * 设置 [ADDRESS2_POSTALCODE]
     */
    public void setAddress2Postalcode(String  address2Postalcode){
        this.address2Postalcode = address2Postalcode ;
        this.modify("address2_postalcode",address2Postalcode);
    }

    /**
     * 设置 [ADDRESS1_LINE1]
     */
    public void setAddress1Line1(String  address1Line1){
        this.address1Line1 = address1Line1 ;
        this.modify("address1_line1",address1Line1);
    }

    /**
     * 设置 [ADDRESS1_TELEPHONE3]
     */
    public void setAddress1Telephone3(String  address1Telephone3){
        this.address1Telephone3 = address1Telephone3 ;
        this.modify("address1_telephone3",address1Telephone3);
    }

    /**
     * 设置 [ADDRESS2_LINE1]
     */
    public void setAddress2Line1(String  address2Line1){
        this.address2Line1 = address2Line1 ;
        this.modify("address2_line1",address2Line1);
    }

    /**
     * 设置 [ADDRESS1_ADDRESSTYPECODE]
     */
    public void setAddress1Addresstypecode(String  address1Addresstypecode){
        this.address1Addresstypecode = address1Addresstypecode ;
        this.modify("address1_addresstypecode",address1Addresstypecode);
    }

    /**
     * 设置 [ADDRESS2_NAME]
     */
    public void setAddress2Name(String  address2Name){
        this.address2Name = address2Name ;
        this.modify("address2_name",address2Name);
    }

    /**
     * 设置 [COMPETITORNAME]
     */
    public void setCompetitorname(String  competitorname){
        this.competitorname = competitorname ;
        this.modify("competitorname",competitorname);
    }

    /**
     * 设置 [ADDRESS1_UPSZONE]
     */
    public void setAddress1Upszone(String  address1Upszone){
        this.address1Upszone = address1Upszone ;
        this.modify("address1_upszone",address1Upszone);
    }

    /**
     * 设置 [ADDRESS2_LINE2]
     */
    public void setAddress2Line2(String  address2Line2){
        this.address2Line2 = address2Line2 ;
        this.modify("address2_line2",address2Line2);
    }

    /**
     * 设置 [CURRENCYNAME]
     */
    public void setCurrencyname(String  currencyname){
        this.currencyname = currencyname ;
        this.modify("currencyname",currencyname);
    }

    /**
     * 设置 [TRANSACTIONCURRENCYID]
     */
    public void setTransactioncurrencyid(String  transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }


}


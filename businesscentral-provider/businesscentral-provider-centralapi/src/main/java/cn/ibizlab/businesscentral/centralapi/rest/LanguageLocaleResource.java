package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.base.domain.LanguageLocale;
import cn.ibizlab.businesscentral.core.base.service.ILanguageLocaleService;
import cn.ibizlab.businesscentral.core.base.filter.LanguageLocaleSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"语言" })
@RestController("CentralApi-languagelocale")
@RequestMapping("")
public class LanguageLocaleResource {

    @Autowired
    public ILanguageLocaleService languagelocaleService;

    @Autowired
    @Lazy
    public LanguageLocaleMapping languagelocaleMapping;

    @PreAuthorize("hasPermission(this.languagelocaleMapping.toDomain(#languagelocaledto),'iBizBusinessCentral-LanguageLocale-Create')")
    @ApiOperation(value = "新建语言", tags = {"语言" },  notes = "新建语言")
	@RequestMapping(method = RequestMethod.POST, value = "/languagelocales")
    public ResponseEntity<LanguageLocaleDTO> create(@RequestBody LanguageLocaleDTO languagelocaledto) {
        LanguageLocale domain = languagelocaleMapping.toDomain(languagelocaledto);
		languagelocaleService.create(domain);
        LanguageLocaleDTO dto = languagelocaleMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.languagelocaleMapping.toDomain(#languagelocaledtos),'iBizBusinessCentral-LanguageLocale-Create')")
    @ApiOperation(value = "批量新建语言", tags = {"语言" },  notes = "批量新建语言")
	@RequestMapping(method = RequestMethod.POST, value = "/languagelocales/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<LanguageLocaleDTO> languagelocaledtos) {
        languagelocaleService.createBatch(languagelocaleMapping.toDomain(languagelocaledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "languagelocale" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.languagelocaleService.get(#languagelocale_id),'iBizBusinessCentral-LanguageLocale-Update')")
    @ApiOperation(value = "更新语言", tags = {"语言" },  notes = "更新语言")
	@RequestMapping(method = RequestMethod.PUT, value = "/languagelocales/{languagelocale_id}")
    public ResponseEntity<LanguageLocaleDTO> update(@PathVariable("languagelocale_id") String languagelocale_id, @RequestBody LanguageLocaleDTO languagelocaledto) {
		LanguageLocale domain  = languagelocaleMapping.toDomain(languagelocaledto);
        domain .setLanguagelocaleid(languagelocale_id);
		languagelocaleService.update(domain );
		LanguageLocaleDTO dto = languagelocaleMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.languagelocaleService.getLanguagelocaleByEntities(this.languagelocaleMapping.toDomain(#languagelocaledtos)),'iBizBusinessCentral-LanguageLocale-Update')")
    @ApiOperation(value = "批量更新语言", tags = {"语言" },  notes = "批量更新语言")
	@RequestMapping(method = RequestMethod.PUT, value = "/languagelocales/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<LanguageLocaleDTO> languagelocaledtos) {
        languagelocaleService.updateBatch(languagelocaleMapping.toDomain(languagelocaledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.languagelocaleService.get(#languagelocale_id),'iBizBusinessCentral-LanguageLocale-Remove')")
    @ApiOperation(value = "删除语言", tags = {"语言" },  notes = "删除语言")
	@RequestMapping(method = RequestMethod.DELETE, value = "/languagelocales/{languagelocale_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("languagelocale_id") String languagelocale_id) {
         return ResponseEntity.status(HttpStatus.OK).body(languagelocaleService.remove(languagelocale_id));
    }

    @PreAuthorize("hasPermission(this.languagelocaleService.getLanguagelocaleByIds(#ids),'iBizBusinessCentral-LanguageLocale-Remove')")
    @ApiOperation(value = "批量删除语言", tags = {"语言" },  notes = "批量删除语言")
	@RequestMapping(method = RequestMethod.DELETE, value = "/languagelocales/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        languagelocaleService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.languagelocaleMapping.toDomain(returnObject.body),'iBizBusinessCentral-LanguageLocale-Get')")
    @ApiOperation(value = "获取语言", tags = {"语言" },  notes = "获取语言")
	@RequestMapping(method = RequestMethod.GET, value = "/languagelocales/{languagelocale_id}")
    public ResponseEntity<LanguageLocaleDTO> get(@PathVariable("languagelocale_id") String languagelocale_id) {
        LanguageLocale domain = languagelocaleService.get(languagelocale_id);
        LanguageLocaleDTO dto = languagelocaleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取语言草稿", tags = {"语言" },  notes = "获取语言草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/languagelocales/getdraft")
    public ResponseEntity<LanguageLocaleDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(languagelocaleMapping.toDto(languagelocaleService.getDraft(new LanguageLocale())));
    }

    @ApiOperation(value = "检查语言", tags = {"语言" },  notes = "检查语言")
	@RequestMapping(method = RequestMethod.POST, value = "/languagelocales/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody LanguageLocaleDTO languagelocaledto) {
        return  ResponseEntity.status(HttpStatus.OK).body(languagelocaleService.checkKey(languagelocaleMapping.toDomain(languagelocaledto)));
    }

    @PreAuthorize("hasPermission(this.languagelocaleMapping.toDomain(#languagelocaledto),'iBizBusinessCentral-LanguageLocale-Save')")
    @ApiOperation(value = "保存语言", tags = {"语言" },  notes = "保存语言")
	@RequestMapping(method = RequestMethod.POST, value = "/languagelocales/save")
    public ResponseEntity<Boolean> save(@RequestBody LanguageLocaleDTO languagelocaledto) {
        return ResponseEntity.status(HttpStatus.OK).body(languagelocaleService.save(languagelocaleMapping.toDomain(languagelocaledto)));
    }

    @PreAuthorize("hasPermission(this.languagelocaleMapping.toDomain(#languagelocaledtos),'iBizBusinessCentral-LanguageLocale-Save')")
    @ApiOperation(value = "批量保存语言", tags = {"语言" },  notes = "批量保存语言")
	@RequestMapping(method = RequestMethod.POST, value = "/languagelocales/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<LanguageLocaleDTO> languagelocaledtos) {
        languagelocaleService.saveBatch(languagelocaleMapping.toDomain(languagelocaledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-LanguageLocale-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-LanguageLocale-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"语言" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/languagelocales/fetchdefault")
	public ResponseEntity<List<LanguageLocaleDTO>> fetchDefault(LanguageLocaleSearchContext context) {
        Page<LanguageLocale> domains = languagelocaleService.searchDefault(context) ;
        List<LanguageLocaleDTO> list = languagelocaleMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-LanguageLocale-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-LanguageLocale-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"语言" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/languagelocales/searchdefault")
	public ResponseEntity<Page<LanguageLocaleDTO>> searchDefault(@RequestBody LanguageLocaleSearchContext context) {
        Page<LanguageLocale> domains = languagelocaleService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(languagelocaleMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}


package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.sales.domain.CompetitorSalesLiterature;
import cn.ibizlab.businesscentral.core.sales.service.ICompetitorSalesLiteratureService;
import cn.ibizlab.businesscentral.core.sales.filter.CompetitorSalesLiteratureSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"竞争对手宣传资料" })
@RestController("CentralApi-competitorsalesliterature")
@RequestMapping("")
public class CompetitorSalesLiteratureResource {

    @Autowired
    public ICompetitorSalesLiteratureService competitorsalesliteratureService;

    @Autowired
    @Lazy
    public CompetitorSalesLiteratureMapping competitorsalesliteratureMapping;

    @PreAuthorize("hasPermission(this.competitorsalesliteratureMapping.toDomain(#competitorsalesliteraturedto),'iBizBusinessCentral-CompetitorSalesLiterature-Create')")
    @ApiOperation(value = "新建竞争对手宣传资料", tags = {"竞争对手宣传资料" },  notes = "新建竞争对手宣传资料")
	@RequestMapping(method = RequestMethod.POST, value = "/competitorsalesliteratures")
    public ResponseEntity<CompetitorSalesLiteratureDTO> create(@RequestBody CompetitorSalesLiteratureDTO competitorsalesliteraturedto) {
        CompetitorSalesLiterature domain = competitorsalesliteratureMapping.toDomain(competitorsalesliteraturedto);
		competitorsalesliteratureService.create(domain);
        CompetitorSalesLiteratureDTO dto = competitorsalesliteratureMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.competitorsalesliteratureMapping.toDomain(#competitorsalesliteraturedtos),'iBizBusinessCentral-CompetitorSalesLiterature-Create')")
    @ApiOperation(value = "批量新建竞争对手宣传资料", tags = {"竞争对手宣传资料" },  notes = "批量新建竞争对手宣传资料")
	@RequestMapping(method = RequestMethod.POST, value = "/competitorsalesliteratures/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<CompetitorSalesLiteratureDTO> competitorsalesliteraturedtos) {
        competitorsalesliteratureService.createBatch(competitorsalesliteratureMapping.toDomain(competitorsalesliteraturedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "competitorsalesliterature" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.competitorsalesliteratureService.get(#competitorsalesliterature_id),'iBizBusinessCentral-CompetitorSalesLiterature-Update')")
    @ApiOperation(value = "更新竞争对手宣传资料", tags = {"竞争对手宣传资料" },  notes = "更新竞争对手宣传资料")
	@RequestMapping(method = RequestMethod.PUT, value = "/competitorsalesliteratures/{competitorsalesliterature_id}")
    public ResponseEntity<CompetitorSalesLiteratureDTO> update(@PathVariable("competitorsalesliterature_id") String competitorsalesliterature_id, @RequestBody CompetitorSalesLiteratureDTO competitorsalesliteraturedto) {
		CompetitorSalesLiterature domain  = competitorsalesliteratureMapping.toDomain(competitorsalesliteraturedto);
        domain .setRelationshipsid(competitorsalesliterature_id);
		competitorsalesliteratureService.update(domain );
		CompetitorSalesLiteratureDTO dto = competitorsalesliteratureMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.competitorsalesliteratureService.getCompetitorsalesliteratureByEntities(this.competitorsalesliteratureMapping.toDomain(#competitorsalesliteraturedtos)),'iBizBusinessCentral-CompetitorSalesLiterature-Update')")
    @ApiOperation(value = "批量更新竞争对手宣传资料", tags = {"竞争对手宣传资料" },  notes = "批量更新竞争对手宣传资料")
	@RequestMapping(method = RequestMethod.PUT, value = "/competitorsalesliteratures/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<CompetitorSalesLiteratureDTO> competitorsalesliteraturedtos) {
        competitorsalesliteratureService.updateBatch(competitorsalesliteratureMapping.toDomain(competitorsalesliteraturedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.competitorsalesliteratureService.get(#competitorsalesliterature_id),'iBizBusinessCentral-CompetitorSalesLiterature-Remove')")
    @ApiOperation(value = "删除竞争对手宣传资料", tags = {"竞争对手宣传资料" },  notes = "删除竞争对手宣传资料")
	@RequestMapping(method = RequestMethod.DELETE, value = "/competitorsalesliteratures/{competitorsalesliterature_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("competitorsalesliterature_id") String competitorsalesliterature_id) {
         return ResponseEntity.status(HttpStatus.OK).body(competitorsalesliteratureService.remove(competitorsalesliterature_id));
    }

    @PreAuthorize("hasPermission(this.competitorsalesliteratureService.getCompetitorsalesliteratureByIds(#ids),'iBizBusinessCentral-CompetitorSalesLiterature-Remove')")
    @ApiOperation(value = "批量删除竞争对手宣传资料", tags = {"竞争对手宣传资料" },  notes = "批量删除竞争对手宣传资料")
	@RequestMapping(method = RequestMethod.DELETE, value = "/competitorsalesliteratures/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        competitorsalesliteratureService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.competitorsalesliteratureMapping.toDomain(returnObject.body),'iBizBusinessCentral-CompetitorSalesLiterature-Get')")
    @ApiOperation(value = "获取竞争对手宣传资料", tags = {"竞争对手宣传资料" },  notes = "获取竞争对手宣传资料")
	@RequestMapping(method = RequestMethod.GET, value = "/competitorsalesliteratures/{competitorsalesliterature_id}")
    public ResponseEntity<CompetitorSalesLiteratureDTO> get(@PathVariable("competitorsalesliterature_id") String competitorsalesliterature_id) {
        CompetitorSalesLiterature domain = competitorsalesliteratureService.get(competitorsalesliterature_id);
        CompetitorSalesLiteratureDTO dto = competitorsalesliteratureMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取竞争对手宣传资料草稿", tags = {"竞争对手宣传资料" },  notes = "获取竞争对手宣传资料草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/competitorsalesliteratures/getdraft")
    public ResponseEntity<CompetitorSalesLiteratureDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(competitorsalesliteratureMapping.toDto(competitorsalesliteratureService.getDraft(new CompetitorSalesLiterature())));
    }

    @ApiOperation(value = "检查竞争对手宣传资料", tags = {"竞争对手宣传资料" },  notes = "检查竞争对手宣传资料")
	@RequestMapping(method = RequestMethod.POST, value = "/competitorsalesliteratures/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody CompetitorSalesLiteratureDTO competitorsalesliteraturedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(competitorsalesliteratureService.checkKey(competitorsalesliteratureMapping.toDomain(competitorsalesliteraturedto)));
    }

    @PreAuthorize("hasPermission(this.competitorsalesliteratureMapping.toDomain(#competitorsalesliteraturedto),'iBizBusinessCentral-CompetitorSalesLiterature-Save')")
    @ApiOperation(value = "保存竞争对手宣传资料", tags = {"竞争对手宣传资料" },  notes = "保存竞争对手宣传资料")
	@RequestMapping(method = RequestMethod.POST, value = "/competitorsalesliteratures/save")
    public ResponseEntity<Boolean> save(@RequestBody CompetitorSalesLiteratureDTO competitorsalesliteraturedto) {
        return ResponseEntity.status(HttpStatus.OK).body(competitorsalesliteratureService.save(competitorsalesliteratureMapping.toDomain(competitorsalesliteraturedto)));
    }

    @PreAuthorize("hasPermission(this.competitorsalesliteratureMapping.toDomain(#competitorsalesliteraturedtos),'iBizBusinessCentral-CompetitorSalesLiterature-Save')")
    @ApiOperation(value = "批量保存竞争对手宣传资料", tags = {"竞争对手宣传资料" },  notes = "批量保存竞争对手宣传资料")
	@RequestMapping(method = RequestMethod.POST, value = "/competitorsalesliteratures/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<CompetitorSalesLiteratureDTO> competitorsalesliteraturedtos) {
        competitorsalesliteratureService.saveBatch(competitorsalesliteratureMapping.toDomain(competitorsalesliteraturedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-CompetitorSalesLiterature-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-CompetitorSalesLiterature-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"竞争对手宣传资料" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/competitorsalesliteratures/fetchdefault")
	public ResponseEntity<List<CompetitorSalesLiteratureDTO>> fetchDefault(CompetitorSalesLiteratureSearchContext context) {
        Page<CompetitorSalesLiterature> domains = competitorsalesliteratureService.searchDefault(context) ;
        List<CompetitorSalesLiteratureDTO> list = competitorsalesliteratureMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-CompetitorSalesLiterature-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-CompetitorSalesLiterature-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"竞争对手宣传资料" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/competitorsalesliteratures/searchdefault")
	public ResponseEntity<Page<CompetitorSalesLiteratureDTO>> searchDefault(@RequestBody CompetitorSalesLiteratureSearchContext context) {
        Page<CompetitorSalesLiterature> domains = competitorsalesliteratureService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(competitorsalesliteratureMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.competitorsalesliteratureMapping.toDomain(#competitorsalesliteraturedto),'iBizBusinessCentral-CompetitorSalesLiterature-Create')")
    @ApiOperation(value = "根据销售宣传资料建立竞争对手宣传资料", tags = {"竞争对手宣传资料" },  notes = "根据销售宣传资料建立竞争对手宣传资料")
	@RequestMapping(method = RequestMethod.POST, value = "/salesliteratures/{salesliterature_id}/competitorsalesliteratures")
    public ResponseEntity<CompetitorSalesLiteratureDTO> createBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id, @RequestBody CompetitorSalesLiteratureDTO competitorsalesliteraturedto) {
        CompetitorSalesLiterature domain = competitorsalesliteratureMapping.toDomain(competitorsalesliteraturedto);
        domain.setEntity2id(salesliterature_id);
		competitorsalesliteratureService.create(domain);
        CompetitorSalesLiteratureDTO dto = competitorsalesliteratureMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.competitorsalesliteratureMapping.toDomain(#competitorsalesliteraturedtos),'iBizBusinessCentral-CompetitorSalesLiterature-Create')")
    @ApiOperation(value = "根据销售宣传资料批量建立竞争对手宣传资料", tags = {"竞争对手宣传资料" },  notes = "根据销售宣传资料批量建立竞争对手宣传资料")
	@RequestMapping(method = RequestMethod.POST, value = "/salesliteratures/{salesliterature_id}/competitorsalesliteratures/batch")
    public ResponseEntity<Boolean> createBatchBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id, @RequestBody List<CompetitorSalesLiteratureDTO> competitorsalesliteraturedtos) {
        List<CompetitorSalesLiterature> domainlist=competitorsalesliteratureMapping.toDomain(competitorsalesliteraturedtos);
        for(CompetitorSalesLiterature domain:domainlist){
            domain.setEntity2id(salesliterature_id);
        }
        competitorsalesliteratureService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "competitorsalesliterature" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.competitorsalesliteratureService.get(#competitorsalesliterature_id),'iBizBusinessCentral-CompetitorSalesLiterature-Update')")
    @ApiOperation(value = "根据销售宣传资料更新竞争对手宣传资料", tags = {"竞争对手宣传资料" },  notes = "根据销售宣传资料更新竞争对手宣传资料")
	@RequestMapping(method = RequestMethod.PUT, value = "/salesliteratures/{salesliterature_id}/competitorsalesliteratures/{competitorsalesliterature_id}")
    public ResponseEntity<CompetitorSalesLiteratureDTO> updateBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id, @PathVariable("competitorsalesliterature_id") String competitorsalesliterature_id, @RequestBody CompetitorSalesLiteratureDTO competitorsalesliteraturedto) {
        CompetitorSalesLiterature domain = competitorsalesliteratureMapping.toDomain(competitorsalesliteraturedto);
        domain.setEntity2id(salesliterature_id);
        domain.setRelationshipsid(competitorsalesliterature_id);
		competitorsalesliteratureService.update(domain);
        CompetitorSalesLiteratureDTO dto = competitorsalesliteratureMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.competitorsalesliteratureService.getCompetitorsalesliteratureByEntities(this.competitorsalesliteratureMapping.toDomain(#competitorsalesliteraturedtos)),'iBizBusinessCentral-CompetitorSalesLiterature-Update')")
    @ApiOperation(value = "根据销售宣传资料批量更新竞争对手宣传资料", tags = {"竞争对手宣传资料" },  notes = "根据销售宣传资料批量更新竞争对手宣传资料")
	@RequestMapping(method = RequestMethod.PUT, value = "/salesliteratures/{salesliterature_id}/competitorsalesliteratures/batch")
    public ResponseEntity<Boolean> updateBatchBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id, @RequestBody List<CompetitorSalesLiteratureDTO> competitorsalesliteraturedtos) {
        List<CompetitorSalesLiterature> domainlist=competitorsalesliteratureMapping.toDomain(competitorsalesliteraturedtos);
        for(CompetitorSalesLiterature domain:domainlist){
            domain.setEntity2id(salesliterature_id);
        }
        competitorsalesliteratureService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.competitorsalesliteratureService.get(#competitorsalesliterature_id),'iBizBusinessCentral-CompetitorSalesLiterature-Remove')")
    @ApiOperation(value = "根据销售宣传资料删除竞争对手宣传资料", tags = {"竞争对手宣传资料" },  notes = "根据销售宣传资料删除竞争对手宣传资料")
	@RequestMapping(method = RequestMethod.DELETE, value = "/salesliteratures/{salesliterature_id}/competitorsalesliteratures/{competitorsalesliterature_id}")
    public ResponseEntity<Boolean> removeBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id, @PathVariable("competitorsalesliterature_id") String competitorsalesliterature_id) {
		return ResponseEntity.status(HttpStatus.OK).body(competitorsalesliteratureService.remove(competitorsalesliterature_id));
    }

    @PreAuthorize("hasPermission(this.competitorsalesliteratureService.getCompetitorsalesliteratureByIds(#ids),'iBizBusinessCentral-CompetitorSalesLiterature-Remove')")
    @ApiOperation(value = "根据销售宣传资料批量删除竞争对手宣传资料", tags = {"竞争对手宣传资料" },  notes = "根据销售宣传资料批量删除竞争对手宣传资料")
	@RequestMapping(method = RequestMethod.DELETE, value = "/salesliteratures/{salesliterature_id}/competitorsalesliteratures/batch")
    public ResponseEntity<Boolean> removeBatchBySalesLiterature(@RequestBody List<String> ids) {
        competitorsalesliteratureService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.competitorsalesliteratureMapping.toDomain(returnObject.body),'iBizBusinessCentral-CompetitorSalesLiterature-Get')")
    @ApiOperation(value = "根据销售宣传资料获取竞争对手宣传资料", tags = {"竞争对手宣传资料" },  notes = "根据销售宣传资料获取竞争对手宣传资料")
	@RequestMapping(method = RequestMethod.GET, value = "/salesliteratures/{salesliterature_id}/competitorsalesliteratures/{competitorsalesliterature_id}")
    public ResponseEntity<CompetitorSalesLiteratureDTO> getBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id, @PathVariable("competitorsalesliterature_id") String competitorsalesliterature_id) {
        CompetitorSalesLiterature domain = competitorsalesliteratureService.get(competitorsalesliterature_id);
        CompetitorSalesLiteratureDTO dto = competitorsalesliteratureMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据销售宣传资料获取竞争对手宣传资料草稿", tags = {"竞争对手宣传资料" },  notes = "根据销售宣传资料获取竞争对手宣传资料草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/salesliteratures/{salesliterature_id}/competitorsalesliteratures/getdraft")
    public ResponseEntity<CompetitorSalesLiteratureDTO> getDraftBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id) {
        CompetitorSalesLiterature domain = new CompetitorSalesLiterature();
        domain.setEntity2id(salesliterature_id);
        return ResponseEntity.status(HttpStatus.OK).body(competitorsalesliteratureMapping.toDto(competitorsalesliteratureService.getDraft(domain)));
    }

    @ApiOperation(value = "根据销售宣传资料检查竞争对手宣传资料", tags = {"竞争对手宣传资料" },  notes = "根据销售宣传资料检查竞争对手宣传资料")
	@RequestMapping(method = RequestMethod.POST, value = "/salesliteratures/{salesliterature_id}/competitorsalesliteratures/checkkey")
    public ResponseEntity<Boolean> checkKeyBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id, @RequestBody CompetitorSalesLiteratureDTO competitorsalesliteraturedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(competitorsalesliteratureService.checkKey(competitorsalesliteratureMapping.toDomain(competitorsalesliteraturedto)));
    }

    @PreAuthorize("hasPermission(this.competitorsalesliteratureMapping.toDomain(#competitorsalesliteraturedto),'iBizBusinessCentral-CompetitorSalesLiterature-Save')")
    @ApiOperation(value = "根据销售宣传资料保存竞争对手宣传资料", tags = {"竞争对手宣传资料" },  notes = "根据销售宣传资料保存竞争对手宣传资料")
	@RequestMapping(method = RequestMethod.POST, value = "/salesliteratures/{salesliterature_id}/competitorsalesliteratures/save")
    public ResponseEntity<Boolean> saveBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id, @RequestBody CompetitorSalesLiteratureDTO competitorsalesliteraturedto) {
        CompetitorSalesLiterature domain = competitorsalesliteratureMapping.toDomain(competitorsalesliteraturedto);
        domain.setEntity2id(salesliterature_id);
        return ResponseEntity.status(HttpStatus.OK).body(competitorsalesliteratureService.save(domain));
    }

    @PreAuthorize("hasPermission(this.competitorsalesliteratureMapping.toDomain(#competitorsalesliteraturedtos),'iBizBusinessCentral-CompetitorSalesLiterature-Save')")
    @ApiOperation(value = "根据销售宣传资料批量保存竞争对手宣传资料", tags = {"竞争对手宣传资料" },  notes = "根据销售宣传资料批量保存竞争对手宣传资料")
	@RequestMapping(method = RequestMethod.POST, value = "/salesliteratures/{salesliterature_id}/competitorsalesliteratures/savebatch")
    public ResponseEntity<Boolean> saveBatchBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id, @RequestBody List<CompetitorSalesLiteratureDTO> competitorsalesliteraturedtos) {
        List<CompetitorSalesLiterature> domainlist=competitorsalesliteratureMapping.toDomain(competitorsalesliteraturedtos);
        for(CompetitorSalesLiterature domain:domainlist){
             domain.setEntity2id(salesliterature_id);
        }
        competitorsalesliteratureService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-CompetitorSalesLiterature-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-CompetitorSalesLiterature-Get')")
	@ApiOperation(value = "根据销售宣传资料获取DEFAULT", tags = {"竞争对手宣传资料" } ,notes = "根据销售宣传资料获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/salesliteratures/{salesliterature_id}/competitorsalesliteratures/fetchdefault")
	public ResponseEntity<List<CompetitorSalesLiteratureDTO>> fetchCompetitorSalesLiteratureDefaultBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id,CompetitorSalesLiteratureSearchContext context) {
        context.setN_entity2id_eq(salesliterature_id);
        Page<CompetitorSalesLiterature> domains = competitorsalesliteratureService.searchDefault(context) ;
        List<CompetitorSalesLiteratureDTO> list = competitorsalesliteratureMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-CompetitorSalesLiterature-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-CompetitorSalesLiterature-Get')")
	@ApiOperation(value = "根据销售宣传资料查询DEFAULT", tags = {"竞争对手宣传资料" } ,notes = "根据销售宣传资料查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/salesliteratures/{salesliterature_id}/competitorsalesliteratures/searchdefault")
	public ResponseEntity<Page<CompetitorSalesLiteratureDTO>> searchCompetitorSalesLiteratureDefaultBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id, @RequestBody CompetitorSalesLiteratureSearchContext context) {
        context.setN_entity2id_eq(salesliterature_id);
        Page<CompetitorSalesLiterature> domains = competitorsalesliteratureService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(competitorsalesliteratureMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}


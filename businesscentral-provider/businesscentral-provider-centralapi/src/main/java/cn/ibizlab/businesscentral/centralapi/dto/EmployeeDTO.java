package cn.ibizlab.businesscentral.centralapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EmployeeDTO]
 */
@Data
public class EmployeeDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [EMPLOYEENAME]
     *
     */
    @JSONField(name = "employeename")
    @JsonProperty("employeename")
    private String employeename;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;

    /**
     * 属性 [EMPLOYEEID]
     *
     */
    @JSONField(name = "employeeid")
    @JsonProperty("employeeid")
    private String employeeid;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [ORGANIZATIONID]
     *
     */
    @JSONField(name = "organizationid")
    @JsonProperty("organizationid")
    private String organizationid;

    /**
     * 属性 [ORGCODE]
     *
     */
    @JSONField(name = "orgcode")
    @JsonProperty("orgcode")
    private String orgcode;

    /**
     * 属性 [ORGLEVEL]
     *
     */
    @JSONField(name = "orglevel")
    @JsonProperty("orglevel")
    private BigInteger orglevel;

    /**
     * 属性 [SHOWORDER]
     *
     */
    @JSONField(name = "showorder")
    @JsonProperty("showorder")
    private BigInteger showorder;

    /**
     * 属性 [SHORTNAME]
     *
     */
    @JSONField(name = "shortname")
    @JsonProperty("shortname")
    private String shortname;

    /**
     * 属性 [EMPLOYEECODE]
     *
     */
    @JSONField(name = "employeecode")
    @JsonProperty("employeecode")
    private String employeecode;

    /**
     * 属性 [BIRTHDAY]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "birthday" , format="yyyy-MM-dd")
    @JsonProperty("birthday")
    private Timestamp birthday;

    /**
     * 属性 [CERTNUM]
     *
     */
    @JSONField(name = "certnum")
    @JsonProperty("certnum")
    private String certnum;

    /**
     * 属性 [AGE]
     *
     */
    @JSONField(name = "age")
    @JsonProperty("age")
    private String age;

    /**
     * 属性 [MOBILE]
     *
     */
    @JSONField(name = "mobile")
    @JsonProperty("mobile")
    private String mobile;

    /**
     * 属性 [EMAIL]
     *
     */
    @JSONField(name = "email")
    @JsonProperty("email")
    private String email;

    /**
     * 属性 [BIRTHADDRESS]
     *
     */
    @JSONField(name = "birthaddress")
    @JsonProperty("birthaddress")
    private String birthaddress;

    /**
     * 属性 [POSTALADDRESS]
     *
     */
    @JSONField(name = "postaladdress")
    @JsonProperty("postaladdress")
    private String postaladdress;

    /**
     * 属性 [HOBBY]
     *
     */
    @JSONField(name = "hobby")
    @JsonProperty("hobby")
    private String hobby;

    /**
     * 属性 [NATIVEADDRESS]
     *
     */
    @JSONField(name = "nativeaddress")
    @JsonProperty("nativeaddress")
    private String nativeaddress;

    /**
     * 属性 [STARTORGTIME]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "startorgtime" , format="yyyy-MM-dd")
    @JsonProperty("startorgtime")
    private Timestamp startorgtime;

    /**
     * 属性 [HEALTH]
     *
     */
    @JSONField(name = "health")
    @JsonProperty("health")
    private String health;

    /**
     * 属性 [STARTWORKTIME]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "startworktime" , format="yyyy-MM-dd")
    @JsonProperty("startworktime")
    private Timestamp startworktime;

    /**
     * 属性 [PHOTO]
     *
     */
    @JSONField(name = "photo")
    @JsonProperty("photo")
    private String photo;

    /**
     * 属性 [TECHNICALTITLE]
     *
     */
    @JSONField(name = "technicaltitle")
    @JsonProperty("technicaltitle")
    private String technicaltitle;

    /**
     * 属性 [NATIVEPLACE]
     *
     */
    @JSONField(name = "nativeplace")
    @JsonProperty("nativeplace")
    private String nativeplace;

    /**
     * 属性 [CERTIFICATES]
     *
     */
    @JSONField(name = "certificates")
    @JsonProperty("certificates")
    private String certificates;

    /**
     * 属性 [TELEPHONE]
     *
     */
    @JSONField(name = "telephone")
    @JsonProperty("telephone")
    private String telephone;

    /**
     * 属性 [BLOODTYPE]
     *
     */
    @JSONField(name = "bloodtype")
    @JsonProperty("bloodtype")
    private String bloodtype;

    /**
     * 属性 [NATION]
     *
     */
    @JSONField(name = "nation")
    @JsonProperty("nation")
    private String nation;

    /**
     * 属性 [CERTTYPE]
     *
     */
    @JSONField(name = "certtype")
    @JsonProperty("certtype")
    private String certtype;

    /**
     * 属性 [SEX]
     *
     */
    @JSONField(name = "sex")
    @JsonProperty("sex")
    private String sex;

    /**
     * 属性 [MARRIAGE]
     *
     */
    @JSONField(name = "marriage")
    @JsonProperty("marriage")
    private String marriage;

    /**
     * 属性 [NATIVETYPE]
     *
     */
    @JSONField(name = "nativetype")
    @JsonProperty("nativetype")
    private String nativetype;

    /**
     * 属性 [POLITICAL]
     *
     */
    @JSONField(name = "political")
    @JsonProperty("political")
    private String political;

    /**
     * 属性 [ORGANIZATIONNAME]
     *
     */
    @JSONField(name = "organizationname")
    @JsonProperty("organizationname")
    private String organizationname;


    /**
     * 设置 [EMPLOYEENAME]
     */
    public void setEmployeename(String  employeename){
        this.employeename = employeename ;
        this.modify("employeename",employeename);
    }

    /**
     * 设置 [ORGANIZATIONID]
     */
    public void setOrganizationid(String  organizationid){
        this.organizationid = organizationid ;
        this.modify("organizationid",organizationid);
    }

    /**
     * 设置 [ORGCODE]
     */
    public void setOrgcode(String  orgcode){
        this.orgcode = orgcode ;
        this.modify("orgcode",orgcode);
    }

    /**
     * 设置 [ORGLEVEL]
     */
    public void setOrglevel(BigInteger  orglevel){
        this.orglevel = orglevel ;
        this.modify("orglevel",orglevel);
    }

    /**
     * 设置 [SHOWORDER]
     */
    public void setShoworder(BigInteger  showorder){
        this.showorder = showorder ;
        this.modify("showorder",showorder);
    }

    /**
     * 设置 [SHORTNAME]
     */
    public void setShortname(String  shortname){
        this.shortname = shortname ;
        this.modify("shortname",shortname);
    }

    /**
     * 设置 [EMPLOYEECODE]
     */
    public void setEmployeecode(String  employeecode){
        this.employeecode = employeecode ;
        this.modify("employeecode",employeecode);
    }

    /**
     * 设置 [BIRTHDAY]
     */
    public void setBirthday(Timestamp  birthday){
        this.birthday = birthday ;
        this.modify("birthday",birthday);
    }

    /**
     * 设置 [CERTNUM]
     */
    public void setCertnum(String  certnum){
        this.certnum = certnum ;
        this.modify("certnum",certnum);
    }

    /**
     * 设置 [MOBILE]
     */
    public void setMobile(String  mobile){
        this.mobile = mobile ;
        this.modify("mobile",mobile);
    }

    /**
     * 设置 [EMAIL]
     */
    public void setEmail(String  email){
        this.email = email ;
        this.modify("email",email);
    }

    /**
     * 设置 [BIRTHADDRESS]
     */
    public void setBirthaddress(String  birthaddress){
        this.birthaddress = birthaddress ;
        this.modify("birthaddress",birthaddress);
    }

    /**
     * 设置 [POSTALADDRESS]
     */
    public void setPostaladdress(String  postaladdress){
        this.postaladdress = postaladdress ;
        this.modify("postaladdress",postaladdress);
    }

    /**
     * 设置 [HOBBY]
     */
    public void setHobby(String  hobby){
        this.hobby = hobby ;
        this.modify("hobby",hobby);
    }

    /**
     * 设置 [NATIVEADDRESS]
     */
    public void setNativeaddress(String  nativeaddress){
        this.nativeaddress = nativeaddress ;
        this.modify("nativeaddress",nativeaddress);
    }

    /**
     * 设置 [STARTORGTIME]
     */
    public void setStartorgtime(Timestamp  startorgtime){
        this.startorgtime = startorgtime ;
        this.modify("startorgtime",startorgtime);
    }

    /**
     * 设置 [HEALTH]
     */
    public void setHealth(String  health){
        this.health = health ;
        this.modify("health",health);
    }

    /**
     * 设置 [STARTWORKTIME]
     */
    public void setStartworktime(Timestamp  startworktime){
        this.startworktime = startworktime ;
        this.modify("startworktime",startworktime);
    }

    /**
     * 设置 [PHOTO]
     */
    public void setPhoto(String  photo){
        this.photo = photo ;
        this.modify("photo",photo);
    }

    /**
     * 设置 [TECHNICALTITLE]
     */
    public void setTechnicaltitle(String  technicaltitle){
        this.technicaltitle = technicaltitle ;
        this.modify("technicaltitle",technicaltitle);
    }

    /**
     * 设置 [NATIVEPLACE]
     */
    public void setNativeplace(String  nativeplace){
        this.nativeplace = nativeplace ;
        this.modify("nativeplace",nativeplace);
    }

    /**
     * 设置 [CERTIFICATES]
     */
    public void setCertificates(String  certificates){
        this.certificates = certificates ;
        this.modify("certificates",certificates);
    }

    /**
     * 设置 [TELEPHONE]
     */
    public void setTelephone(String  telephone){
        this.telephone = telephone ;
        this.modify("telephone",telephone);
    }

    /**
     * 设置 [BLOODTYPE]
     */
    public void setBloodtype(String  bloodtype){
        this.bloodtype = bloodtype ;
        this.modify("bloodtype",bloodtype);
    }

    /**
     * 设置 [NATION]
     */
    public void setNation(String  nation){
        this.nation = nation ;
        this.modify("nation",nation);
    }

    /**
     * 设置 [CERTTYPE]
     */
    public void setCerttype(String  certtype){
        this.certtype = certtype ;
        this.modify("certtype",certtype);
    }

    /**
     * 设置 [SEX]
     */
    public void setSex(String  sex){
        this.sex = sex ;
        this.modify("sex",sex);
    }

    /**
     * 设置 [MARRIAGE]
     */
    public void setMarriage(String  marriage){
        this.marriage = marriage ;
        this.modify("marriage",marriage);
    }

    /**
     * 设置 [NATIVETYPE]
     */
    public void setNativetype(String  nativetype){
        this.nativetype = nativetype ;
        this.modify("nativetype",nativetype);
    }

    /**
     * 设置 [POLITICAL]
     */
    public void setPolitical(String  political){
        this.political = political ;
        this.modify("political",political);
    }


}


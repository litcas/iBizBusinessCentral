package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.product.domain.PriceLevel;
import cn.ibizlab.businesscentral.core.product.service.IPriceLevelService;
import cn.ibizlab.businesscentral.core.product.filter.PriceLevelSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"价目表" })
@RestController("CentralApi-pricelevel")
@RequestMapping("")
public class PriceLevelResource {

    @Autowired
    public IPriceLevelService pricelevelService;

    @Autowired
    @Lazy
    public PriceLevelMapping pricelevelMapping;

    @PreAuthorize("hasPermission(this.pricelevelMapping.toDomain(#priceleveldto),'iBizBusinessCentral-PriceLevel-Create')")
    @ApiOperation(value = "新建价目表", tags = {"价目表" },  notes = "新建价目表")
	@RequestMapping(method = RequestMethod.POST, value = "/pricelevels")
    public ResponseEntity<PriceLevelDTO> create(@RequestBody PriceLevelDTO priceleveldto) {
        PriceLevel domain = pricelevelMapping.toDomain(priceleveldto);
		pricelevelService.create(domain);
        PriceLevelDTO dto = pricelevelMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.pricelevelMapping.toDomain(#priceleveldtos),'iBizBusinessCentral-PriceLevel-Create')")
    @ApiOperation(value = "批量新建价目表", tags = {"价目表" },  notes = "批量新建价目表")
	@RequestMapping(method = RequestMethod.POST, value = "/pricelevels/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<PriceLevelDTO> priceleveldtos) {
        pricelevelService.createBatch(pricelevelMapping.toDomain(priceleveldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "pricelevel" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.pricelevelService.get(#pricelevel_id),'iBizBusinessCentral-PriceLevel-Update')")
    @ApiOperation(value = "更新价目表", tags = {"价目表" },  notes = "更新价目表")
	@RequestMapping(method = RequestMethod.PUT, value = "/pricelevels/{pricelevel_id}")
    public ResponseEntity<PriceLevelDTO> update(@PathVariable("pricelevel_id") String pricelevel_id, @RequestBody PriceLevelDTO priceleveldto) {
		PriceLevel domain  = pricelevelMapping.toDomain(priceleveldto);
        domain .setPricelevelid(pricelevel_id);
		pricelevelService.update(domain );
		PriceLevelDTO dto = pricelevelMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.pricelevelService.getPricelevelByEntities(this.pricelevelMapping.toDomain(#priceleveldtos)),'iBizBusinessCentral-PriceLevel-Update')")
    @ApiOperation(value = "批量更新价目表", tags = {"价目表" },  notes = "批量更新价目表")
	@RequestMapping(method = RequestMethod.PUT, value = "/pricelevels/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<PriceLevelDTO> priceleveldtos) {
        pricelevelService.updateBatch(pricelevelMapping.toDomain(priceleveldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.pricelevelService.get(#pricelevel_id),'iBizBusinessCentral-PriceLevel-Remove')")
    @ApiOperation(value = "删除价目表", tags = {"价目表" },  notes = "删除价目表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pricelevels/{pricelevel_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("pricelevel_id") String pricelevel_id) {
         return ResponseEntity.status(HttpStatus.OK).body(pricelevelService.remove(pricelevel_id));
    }

    @PreAuthorize("hasPermission(this.pricelevelService.getPricelevelByIds(#ids),'iBizBusinessCentral-PriceLevel-Remove')")
    @ApiOperation(value = "批量删除价目表", tags = {"价目表" },  notes = "批量删除价目表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pricelevels/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        pricelevelService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.pricelevelMapping.toDomain(returnObject.body),'iBizBusinessCentral-PriceLevel-Get')")
    @ApiOperation(value = "获取价目表", tags = {"价目表" },  notes = "获取价目表")
	@RequestMapping(method = RequestMethod.GET, value = "/pricelevels/{pricelevel_id}")
    public ResponseEntity<PriceLevelDTO> get(@PathVariable("pricelevel_id") String pricelevel_id) {
        PriceLevel domain = pricelevelService.get(pricelevel_id);
        PriceLevelDTO dto = pricelevelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取价目表草稿", tags = {"价目表" },  notes = "获取价目表草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/pricelevels/getdraft")
    public ResponseEntity<PriceLevelDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(pricelevelMapping.toDto(pricelevelService.getDraft(new PriceLevel())));
    }

    @ApiOperation(value = "检查价目表", tags = {"价目表" },  notes = "检查价目表")
	@RequestMapping(method = RequestMethod.POST, value = "/pricelevels/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody PriceLevelDTO priceleveldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(pricelevelService.checkKey(pricelevelMapping.toDomain(priceleveldto)));
    }

    @PreAuthorize("hasPermission(this.pricelevelMapping.toDomain(#priceleveldto),'iBizBusinessCentral-PriceLevel-Save')")
    @ApiOperation(value = "保存价目表", tags = {"价目表" },  notes = "保存价目表")
	@RequestMapping(method = RequestMethod.POST, value = "/pricelevels/save")
    public ResponseEntity<Boolean> save(@RequestBody PriceLevelDTO priceleveldto) {
        return ResponseEntity.status(HttpStatus.OK).body(pricelevelService.save(pricelevelMapping.toDomain(priceleveldto)));
    }

    @PreAuthorize("hasPermission(this.pricelevelMapping.toDomain(#priceleveldtos),'iBizBusinessCentral-PriceLevel-Save')")
    @ApiOperation(value = "批量保存价目表", tags = {"价目表" },  notes = "批量保存价目表")
	@RequestMapping(method = RequestMethod.POST, value = "/pricelevels/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<PriceLevelDTO> priceleveldtos) {
        pricelevelService.saveBatch(pricelevelMapping.toDomain(priceleveldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-PriceLevel-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-PriceLevel-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"价目表" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pricelevels/fetchdefault")
	public ResponseEntity<List<PriceLevelDTO>> fetchDefault(PriceLevelSearchContext context) {
        Page<PriceLevel> domains = pricelevelService.searchDefault(context) ;
        List<PriceLevelDTO> list = pricelevelMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-PriceLevel-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-PriceLevel-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"价目表" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pricelevels/searchdefault")
	public ResponseEntity<Page<PriceLevelDTO>> searchDefault(@RequestBody PriceLevelSearchContext context) {
        Page<PriceLevel> domains = pricelevelService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(pricelevelMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}


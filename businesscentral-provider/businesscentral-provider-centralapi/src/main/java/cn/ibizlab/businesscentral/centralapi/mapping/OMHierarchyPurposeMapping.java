package cn.ibizlab.businesscentral.centralapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.base.domain.OMHierarchyPurpose;
import cn.ibizlab.businesscentral.centralapi.dto.OMHierarchyPurposeDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CentralApiOMHierarchyPurposeMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface OMHierarchyPurposeMapping extends MappingBase<OMHierarchyPurposeDTO, OMHierarchyPurpose> {


}


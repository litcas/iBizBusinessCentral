package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.marketing.domain.CampaignList;
import cn.ibizlab.businesscentral.core.marketing.service.ICampaignListService;
import cn.ibizlab.businesscentral.core.marketing.filter.CampaignListSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"市场活动-营销列表" })
@RestController("CentralApi-campaignlist")
@RequestMapping("")
public class CampaignListResource {

    @Autowired
    public ICampaignListService campaignlistService;

    @Autowired
    @Lazy
    public CampaignListMapping campaignlistMapping;

    @PreAuthorize("hasPermission(this.campaignlistMapping.toDomain(#campaignlistdto),'iBizBusinessCentral-CampaignList-Create')")
    @ApiOperation(value = "新建市场活动-营销列表", tags = {"市场活动-营销列表" },  notes = "新建市场活动-营销列表")
	@RequestMapping(method = RequestMethod.POST, value = "/campaignlists")
    public ResponseEntity<CampaignListDTO> create(@RequestBody CampaignListDTO campaignlistdto) {
        CampaignList domain = campaignlistMapping.toDomain(campaignlistdto);
		campaignlistService.create(domain);
        CampaignListDTO dto = campaignlistMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.campaignlistMapping.toDomain(#campaignlistdtos),'iBizBusinessCentral-CampaignList-Create')")
    @ApiOperation(value = "批量新建市场活动-营销列表", tags = {"市场活动-营销列表" },  notes = "批量新建市场活动-营销列表")
	@RequestMapping(method = RequestMethod.POST, value = "/campaignlists/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<CampaignListDTO> campaignlistdtos) {
        campaignlistService.createBatch(campaignlistMapping.toDomain(campaignlistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "campaignlist" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.campaignlistService.get(#campaignlist_id),'iBizBusinessCentral-CampaignList-Update')")
    @ApiOperation(value = "更新市场活动-营销列表", tags = {"市场活动-营销列表" },  notes = "更新市场活动-营销列表")
	@RequestMapping(method = RequestMethod.PUT, value = "/campaignlists/{campaignlist_id}")
    public ResponseEntity<CampaignListDTO> update(@PathVariable("campaignlist_id") String campaignlist_id, @RequestBody CampaignListDTO campaignlistdto) {
		CampaignList domain  = campaignlistMapping.toDomain(campaignlistdto);
        domain .setRelationshipsid(campaignlist_id);
		campaignlistService.update(domain );
		CampaignListDTO dto = campaignlistMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.campaignlistService.getCampaignlistByEntities(this.campaignlistMapping.toDomain(#campaignlistdtos)),'iBizBusinessCentral-CampaignList-Update')")
    @ApiOperation(value = "批量更新市场活动-营销列表", tags = {"市场活动-营销列表" },  notes = "批量更新市场活动-营销列表")
	@RequestMapping(method = RequestMethod.PUT, value = "/campaignlists/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<CampaignListDTO> campaignlistdtos) {
        campaignlistService.updateBatch(campaignlistMapping.toDomain(campaignlistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.campaignlistService.get(#campaignlist_id),'iBizBusinessCentral-CampaignList-Remove')")
    @ApiOperation(value = "删除市场活动-营销列表", tags = {"市场活动-营销列表" },  notes = "删除市场活动-营销列表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/campaignlists/{campaignlist_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("campaignlist_id") String campaignlist_id) {
         return ResponseEntity.status(HttpStatus.OK).body(campaignlistService.remove(campaignlist_id));
    }

    @PreAuthorize("hasPermission(this.campaignlistService.getCampaignlistByIds(#ids),'iBizBusinessCentral-CampaignList-Remove')")
    @ApiOperation(value = "批量删除市场活动-营销列表", tags = {"市场活动-营销列表" },  notes = "批量删除市场活动-营销列表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/campaignlists/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        campaignlistService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.campaignlistMapping.toDomain(returnObject.body),'iBizBusinessCentral-CampaignList-Get')")
    @ApiOperation(value = "获取市场活动-营销列表", tags = {"市场活动-营销列表" },  notes = "获取市场活动-营销列表")
	@RequestMapping(method = RequestMethod.GET, value = "/campaignlists/{campaignlist_id}")
    public ResponseEntity<CampaignListDTO> get(@PathVariable("campaignlist_id") String campaignlist_id) {
        CampaignList domain = campaignlistService.get(campaignlist_id);
        CampaignListDTO dto = campaignlistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取市场活动-营销列表草稿", tags = {"市场活动-营销列表" },  notes = "获取市场活动-营销列表草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/campaignlists/getdraft")
    public ResponseEntity<CampaignListDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(campaignlistMapping.toDto(campaignlistService.getDraft(new CampaignList())));
    }

    @ApiOperation(value = "检查市场活动-营销列表", tags = {"市场活动-营销列表" },  notes = "检查市场活动-营销列表")
	@RequestMapping(method = RequestMethod.POST, value = "/campaignlists/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody CampaignListDTO campaignlistdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(campaignlistService.checkKey(campaignlistMapping.toDomain(campaignlistdto)));
    }

    @PreAuthorize("hasPermission(this.campaignlistMapping.toDomain(#campaignlistdto),'iBizBusinessCentral-CampaignList-Save')")
    @ApiOperation(value = "保存市场活动-营销列表", tags = {"市场活动-营销列表" },  notes = "保存市场活动-营销列表")
	@RequestMapping(method = RequestMethod.POST, value = "/campaignlists/save")
    public ResponseEntity<Boolean> save(@RequestBody CampaignListDTO campaignlistdto) {
        return ResponseEntity.status(HttpStatus.OK).body(campaignlistService.save(campaignlistMapping.toDomain(campaignlistdto)));
    }

    @PreAuthorize("hasPermission(this.campaignlistMapping.toDomain(#campaignlistdtos),'iBizBusinessCentral-CampaignList-Save')")
    @ApiOperation(value = "批量保存市场活动-营销列表", tags = {"市场活动-营销列表" },  notes = "批量保存市场活动-营销列表")
	@RequestMapping(method = RequestMethod.POST, value = "/campaignlists/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<CampaignListDTO> campaignlistdtos) {
        campaignlistService.saveBatch(campaignlistMapping.toDomain(campaignlistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-CampaignList-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-CampaignList-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"市场活动-营销列表" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/campaignlists/fetchdefault")
	public ResponseEntity<List<CampaignListDTO>> fetchDefault(CampaignListSearchContext context) {
        Page<CampaignList> domains = campaignlistService.searchDefault(context) ;
        List<CampaignListDTO> list = campaignlistMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-CampaignList-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-CampaignList-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"市场活动-营销列表" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/campaignlists/searchdefault")
	public ResponseEntity<Page<CampaignListDTO>> searchDefault(@RequestBody CampaignListSearchContext context) {
        Page<CampaignList> domains = campaignlistService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(campaignlistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.campaignlistMapping.toDomain(#campaignlistdto),'iBizBusinessCentral-CampaignList-Create')")
    @ApiOperation(value = "根据市场活动建立市场活动-营销列表", tags = {"市场活动-营销列表" },  notes = "根据市场活动建立市场活动-营销列表")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigns/{campaign_id}/campaignlists")
    public ResponseEntity<CampaignListDTO> createByCampaign(@PathVariable("campaign_id") String campaign_id, @RequestBody CampaignListDTO campaignlistdto) {
        CampaignList domain = campaignlistMapping.toDomain(campaignlistdto);
        domain.setEntityid(campaign_id);
		campaignlistService.create(domain);
        CampaignListDTO dto = campaignlistMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.campaignlistMapping.toDomain(#campaignlistdtos),'iBizBusinessCentral-CampaignList-Create')")
    @ApiOperation(value = "根据市场活动批量建立市场活动-营销列表", tags = {"市场活动-营销列表" },  notes = "根据市场活动批量建立市场活动-营销列表")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigns/{campaign_id}/campaignlists/batch")
    public ResponseEntity<Boolean> createBatchByCampaign(@PathVariable("campaign_id") String campaign_id, @RequestBody List<CampaignListDTO> campaignlistdtos) {
        List<CampaignList> domainlist=campaignlistMapping.toDomain(campaignlistdtos);
        for(CampaignList domain:domainlist){
            domain.setEntityid(campaign_id);
        }
        campaignlistService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "campaignlist" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.campaignlistService.get(#campaignlist_id),'iBizBusinessCentral-CampaignList-Update')")
    @ApiOperation(value = "根据市场活动更新市场活动-营销列表", tags = {"市场活动-营销列表" },  notes = "根据市场活动更新市场活动-营销列表")
	@RequestMapping(method = RequestMethod.PUT, value = "/campaigns/{campaign_id}/campaignlists/{campaignlist_id}")
    public ResponseEntity<CampaignListDTO> updateByCampaign(@PathVariable("campaign_id") String campaign_id, @PathVariable("campaignlist_id") String campaignlist_id, @RequestBody CampaignListDTO campaignlistdto) {
        CampaignList domain = campaignlistMapping.toDomain(campaignlistdto);
        domain.setEntityid(campaign_id);
        domain.setRelationshipsid(campaignlist_id);
		campaignlistService.update(domain);
        CampaignListDTO dto = campaignlistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.campaignlistService.getCampaignlistByEntities(this.campaignlistMapping.toDomain(#campaignlistdtos)),'iBizBusinessCentral-CampaignList-Update')")
    @ApiOperation(value = "根据市场活动批量更新市场活动-营销列表", tags = {"市场活动-营销列表" },  notes = "根据市场活动批量更新市场活动-营销列表")
	@RequestMapping(method = RequestMethod.PUT, value = "/campaigns/{campaign_id}/campaignlists/batch")
    public ResponseEntity<Boolean> updateBatchByCampaign(@PathVariable("campaign_id") String campaign_id, @RequestBody List<CampaignListDTO> campaignlistdtos) {
        List<CampaignList> domainlist=campaignlistMapping.toDomain(campaignlistdtos);
        for(CampaignList domain:domainlist){
            domain.setEntityid(campaign_id);
        }
        campaignlistService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.campaignlistService.get(#campaignlist_id),'iBizBusinessCentral-CampaignList-Remove')")
    @ApiOperation(value = "根据市场活动删除市场活动-营销列表", tags = {"市场活动-营销列表" },  notes = "根据市场活动删除市场活动-营销列表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/campaigns/{campaign_id}/campaignlists/{campaignlist_id}")
    public ResponseEntity<Boolean> removeByCampaign(@PathVariable("campaign_id") String campaign_id, @PathVariable("campaignlist_id") String campaignlist_id) {
		return ResponseEntity.status(HttpStatus.OK).body(campaignlistService.remove(campaignlist_id));
    }

    @PreAuthorize("hasPermission(this.campaignlistService.getCampaignlistByIds(#ids),'iBizBusinessCentral-CampaignList-Remove')")
    @ApiOperation(value = "根据市场活动批量删除市场活动-营销列表", tags = {"市场活动-营销列表" },  notes = "根据市场活动批量删除市场活动-营销列表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/campaigns/{campaign_id}/campaignlists/batch")
    public ResponseEntity<Boolean> removeBatchByCampaign(@RequestBody List<String> ids) {
        campaignlistService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.campaignlistMapping.toDomain(returnObject.body),'iBizBusinessCentral-CampaignList-Get')")
    @ApiOperation(value = "根据市场活动获取市场活动-营销列表", tags = {"市场活动-营销列表" },  notes = "根据市场活动获取市场活动-营销列表")
	@RequestMapping(method = RequestMethod.GET, value = "/campaigns/{campaign_id}/campaignlists/{campaignlist_id}")
    public ResponseEntity<CampaignListDTO> getByCampaign(@PathVariable("campaign_id") String campaign_id, @PathVariable("campaignlist_id") String campaignlist_id) {
        CampaignList domain = campaignlistService.get(campaignlist_id);
        CampaignListDTO dto = campaignlistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据市场活动获取市场活动-营销列表草稿", tags = {"市场活动-营销列表" },  notes = "根据市场活动获取市场活动-营销列表草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/campaigns/{campaign_id}/campaignlists/getdraft")
    public ResponseEntity<CampaignListDTO> getDraftByCampaign(@PathVariable("campaign_id") String campaign_id) {
        CampaignList domain = new CampaignList();
        domain.setEntityid(campaign_id);
        return ResponseEntity.status(HttpStatus.OK).body(campaignlistMapping.toDto(campaignlistService.getDraft(domain)));
    }

    @ApiOperation(value = "根据市场活动检查市场活动-营销列表", tags = {"市场活动-营销列表" },  notes = "根据市场活动检查市场活动-营销列表")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigns/{campaign_id}/campaignlists/checkkey")
    public ResponseEntity<Boolean> checkKeyByCampaign(@PathVariable("campaign_id") String campaign_id, @RequestBody CampaignListDTO campaignlistdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(campaignlistService.checkKey(campaignlistMapping.toDomain(campaignlistdto)));
    }

    @PreAuthorize("hasPermission(this.campaignlistMapping.toDomain(#campaignlistdto),'iBizBusinessCentral-CampaignList-Save')")
    @ApiOperation(value = "根据市场活动保存市场活动-营销列表", tags = {"市场活动-营销列表" },  notes = "根据市场活动保存市场活动-营销列表")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigns/{campaign_id}/campaignlists/save")
    public ResponseEntity<Boolean> saveByCampaign(@PathVariable("campaign_id") String campaign_id, @RequestBody CampaignListDTO campaignlistdto) {
        CampaignList domain = campaignlistMapping.toDomain(campaignlistdto);
        domain.setEntityid(campaign_id);
        return ResponseEntity.status(HttpStatus.OK).body(campaignlistService.save(domain));
    }

    @PreAuthorize("hasPermission(this.campaignlistMapping.toDomain(#campaignlistdtos),'iBizBusinessCentral-CampaignList-Save')")
    @ApiOperation(value = "根据市场活动批量保存市场活动-营销列表", tags = {"市场活动-营销列表" },  notes = "根据市场活动批量保存市场活动-营销列表")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigns/{campaign_id}/campaignlists/savebatch")
    public ResponseEntity<Boolean> saveBatchByCampaign(@PathVariable("campaign_id") String campaign_id, @RequestBody List<CampaignListDTO> campaignlistdtos) {
        List<CampaignList> domainlist=campaignlistMapping.toDomain(campaignlistdtos);
        for(CampaignList domain:domainlist){
             domain.setEntityid(campaign_id);
        }
        campaignlistService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-CampaignList-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-CampaignList-Get')")
	@ApiOperation(value = "根据市场活动获取DEFAULT", tags = {"市场活动-营销列表" } ,notes = "根据市场活动获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/campaigns/{campaign_id}/campaignlists/fetchdefault")
	public ResponseEntity<List<CampaignListDTO>> fetchCampaignListDefaultByCampaign(@PathVariable("campaign_id") String campaign_id,CampaignListSearchContext context) {
        context.setN_entityid_eq(campaign_id);
        Page<CampaignList> domains = campaignlistService.searchDefault(context) ;
        List<CampaignListDTO> list = campaignlistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-CampaignList-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-CampaignList-Get')")
	@ApiOperation(value = "根据市场活动查询DEFAULT", tags = {"市场活动-营销列表" } ,notes = "根据市场活动查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/campaigns/{campaign_id}/campaignlists/searchdefault")
	public ResponseEntity<Page<CampaignListDTO>> searchCampaignListDefaultByCampaign(@PathVariable("campaign_id") String campaign_id, @RequestBody CampaignListSearchContext context) {
        context.setN_entityid_eq(campaign_id);
        Page<CampaignList> domains = campaignlistService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(campaignlistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.campaignlistMapping.toDomain(#campaignlistdto),'iBizBusinessCentral-CampaignList-Create')")
    @ApiOperation(value = "根据市场营销列表建立市场活动-营销列表", tags = {"市场活动-营销列表" },  notes = "根据市场营销列表建立市场活动-营销列表")
	@RequestMapping(method = RequestMethod.POST, value = "/ibizlists/{ibizlist_id}/campaignlists")
    public ResponseEntity<CampaignListDTO> createByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @RequestBody CampaignListDTO campaignlistdto) {
        CampaignList domain = campaignlistMapping.toDomain(campaignlistdto);
        domain.setEntity2id(ibizlist_id);
		campaignlistService.create(domain);
        CampaignListDTO dto = campaignlistMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.campaignlistMapping.toDomain(#campaignlistdtos),'iBizBusinessCentral-CampaignList-Create')")
    @ApiOperation(value = "根据市场营销列表批量建立市场活动-营销列表", tags = {"市场活动-营销列表" },  notes = "根据市场营销列表批量建立市场活动-营销列表")
	@RequestMapping(method = RequestMethod.POST, value = "/ibizlists/{ibizlist_id}/campaignlists/batch")
    public ResponseEntity<Boolean> createBatchByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @RequestBody List<CampaignListDTO> campaignlistdtos) {
        List<CampaignList> domainlist=campaignlistMapping.toDomain(campaignlistdtos);
        for(CampaignList domain:domainlist){
            domain.setEntity2id(ibizlist_id);
        }
        campaignlistService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "campaignlist" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.campaignlistService.get(#campaignlist_id),'iBizBusinessCentral-CampaignList-Update')")
    @ApiOperation(value = "根据市场营销列表更新市场活动-营销列表", tags = {"市场活动-营销列表" },  notes = "根据市场营销列表更新市场活动-营销列表")
	@RequestMapping(method = RequestMethod.PUT, value = "/ibizlists/{ibizlist_id}/campaignlists/{campaignlist_id}")
    public ResponseEntity<CampaignListDTO> updateByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @PathVariable("campaignlist_id") String campaignlist_id, @RequestBody CampaignListDTO campaignlistdto) {
        CampaignList domain = campaignlistMapping.toDomain(campaignlistdto);
        domain.setEntity2id(ibizlist_id);
        domain.setRelationshipsid(campaignlist_id);
		campaignlistService.update(domain);
        CampaignListDTO dto = campaignlistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.campaignlistService.getCampaignlistByEntities(this.campaignlistMapping.toDomain(#campaignlistdtos)),'iBizBusinessCentral-CampaignList-Update')")
    @ApiOperation(value = "根据市场营销列表批量更新市场活动-营销列表", tags = {"市场活动-营销列表" },  notes = "根据市场营销列表批量更新市场活动-营销列表")
	@RequestMapping(method = RequestMethod.PUT, value = "/ibizlists/{ibizlist_id}/campaignlists/batch")
    public ResponseEntity<Boolean> updateBatchByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @RequestBody List<CampaignListDTO> campaignlistdtos) {
        List<CampaignList> domainlist=campaignlistMapping.toDomain(campaignlistdtos);
        for(CampaignList domain:domainlist){
            domain.setEntity2id(ibizlist_id);
        }
        campaignlistService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.campaignlistService.get(#campaignlist_id),'iBizBusinessCentral-CampaignList-Remove')")
    @ApiOperation(value = "根据市场营销列表删除市场活动-营销列表", tags = {"市场活动-营销列表" },  notes = "根据市场营销列表删除市场活动-营销列表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/ibizlists/{ibizlist_id}/campaignlists/{campaignlist_id}")
    public ResponseEntity<Boolean> removeByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @PathVariable("campaignlist_id") String campaignlist_id) {
		return ResponseEntity.status(HttpStatus.OK).body(campaignlistService.remove(campaignlist_id));
    }

    @PreAuthorize("hasPermission(this.campaignlistService.getCampaignlistByIds(#ids),'iBizBusinessCentral-CampaignList-Remove')")
    @ApiOperation(value = "根据市场营销列表批量删除市场活动-营销列表", tags = {"市场活动-营销列表" },  notes = "根据市场营销列表批量删除市场活动-营销列表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/ibizlists/{ibizlist_id}/campaignlists/batch")
    public ResponseEntity<Boolean> removeBatchByIBizList(@RequestBody List<String> ids) {
        campaignlistService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.campaignlistMapping.toDomain(returnObject.body),'iBizBusinessCentral-CampaignList-Get')")
    @ApiOperation(value = "根据市场营销列表获取市场活动-营销列表", tags = {"市场活动-营销列表" },  notes = "根据市场营销列表获取市场活动-营销列表")
	@RequestMapping(method = RequestMethod.GET, value = "/ibizlists/{ibizlist_id}/campaignlists/{campaignlist_id}")
    public ResponseEntity<CampaignListDTO> getByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @PathVariable("campaignlist_id") String campaignlist_id) {
        CampaignList domain = campaignlistService.get(campaignlist_id);
        CampaignListDTO dto = campaignlistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据市场营销列表获取市场活动-营销列表草稿", tags = {"市场活动-营销列表" },  notes = "根据市场营销列表获取市场活动-营销列表草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/ibizlists/{ibizlist_id}/campaignlists/getdraft")
    public ResponseEntity<CampaignListDTO> getDraftByIBizList(@PathVariable("ibizlist_id") String ibizlist_id) {
        CampaignList domain = new CampaignList();
        domain.setEntity2id(ibizlist_id);
        return ResponseEntity.status(HttpStatus.OK).body(campaignlistMapping.toDto(campaignlistService.getDraft(domain)));
    }

    @ApiOperation(value = "根据市场营销列表检查市场活动-营销列表", tags = {"市场活动-营销列表" },  notes = "根据市场营销列表检查市场活动-营销列表")
	@RequestMapping(method = RequestMethod.POST, value = "/ibizlists/{ibizlist_id}/campaignlists/checkkey")
    public ResponseEntity<Boolean> checkKeyByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @RequestBody CampaignListDTO campaignlistdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(campaignlistService.checkKey(campaignlistMapping.toDomain(campaignlistdto)));
    }

    @PreAuthorize("hasPermission(this.campaignlistMapping.toDomain(#campaignlistdto),'iBizBusinessCentral-CampaignList-Save')")
    @ApiOperation(value = "根据市场营销列表保存市场活动-营销列表", tags = {"市场活动-营销列表" },  notes = "根据市场营销列表保存市场活动-营销列表")
	@RequestMapping(method = RequestMethod.POST, value = "/ibizlists/{ibizlist_id}/campaignlists/save")
    public ResponseEntity<Boolean> saveByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @RequestBody CampaignListDTO campaignlistdto) {
        CampaignList domain = campaignlistMapping.toDomain(campaignlistdto);
        domain.setEntity2id(ibizlist_id);
        return ResponseEntity.status(HttpStatus.OK).body(campaignlistService.save(domain));
    }

    @PreAuthorize("hasPermission(this.campaignlistMapping.toDomain(#campaignlistdtos),'iBizBusinessCentral-CampaignList-Save')")
    @ApiOperation(value = "根据市场营销列表批量保存市场活动-营销列表", tags = {"市场活动-营销列表" },  notes = "根据市场营销列表批量保存市场活动-营销列表")
	@RequestMapping(method = RequestMethod.POST, value = "/ibizlists/{ibizlist_id}/campaignlists/savebatch")
    public ResponseEntity<Boolean> saveBatchByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @RequestBody List<CampaignListDTO> campaignlistdtos) {
        List<CampaignList> domainlist=campaignlistMapping.toDomain(campaignlistdtos);
        for(CampaignList domain:domainlist){
             domain.setEntity2id(ibizlist_id);
        }
        campaignlistService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-CampaignList-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-CampaignList-Get')")
	@ApiOperation(value = "根据市场营销列表获取DEFAULT", tags = {"市场活动-营销列表" } ,notes = "根据市场营销列表获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/ibizlists/{ibizlist_id}/campaignlists/fetchdefault")
	public ResponseEntity<List<CampaignListDTO>> fetchCampaignListDefaultByIBizList(@PathVariable("ibizlist_id") String ibizlist_id,CampaignListSearchContext context) {
        context.setN_entity2id_eq(ibizlist_id);
        Page<CampaignList> domains = campaignlistService.searchDefault(context) ;
        List<CampaignListDTO> list = campaignlistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-CampaignList-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-CampaignList-Get')")
	@ApiOperation(value = "根据市场营销列表查询DEFAULT", tags = {"市场活动-营销列表" } ,notes = "根据市场营销列表查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/ibizlists/{ibizlist_id}/campaignlists/searchdefault")
	public ResponseEntity<Page<CampaignListDTO>> searchCampaignListDefaultByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @RequestBody CampaignListSearchContext context) {
        context.setN_entity2id_eq(ibizlist_id);
        Page<CampaignList> domains = campaignlistService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(campaignlistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}


package cn.ibizlab.businesscentral.centralapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[OpportunityDTO]
 */
@Data
public class OpportunityDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DECISIONMAKER]
     *
     */
    @JSONField(name = "decisionmaker")
    @JsonProperty("decisionmaker")
    private Integer decisionmaker;

    /**
     * 属性 [TOTALLINEITEMAMOUNT]
     *
     */
    @JSONField(name = "totallineitemamount")
    @JsonProperty("totallineitemamount")
    private BigDecimal totallineitemamount;

    /**
     * 属性 [PROCESSID]
     *
     */
    @JSONField(name = "processid")
    @JsonProperty("processid")
    private String processid;

    /**
     * 属性 [DISCOUNTPERCENTAGE]
     *
     */
    @JSONField(name = "discountpercentage")
    @JsonProperty("discountpercentage")
    private BigDecimal discountpercentage;

    /**
     * 属性 [OWNERTYPE]
     *
     */
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;

    /**
     * 属性 [PRICINGERRORCODE]
     *
     */
    @JSONField(name = "pricingerrorcode")
    @JsonProperty("pricingerrorcode")
    private String pricingerrorcode;

    /**
     * 属性 [TOTALAMOUNT]
     *
     */
    @JSONField(name = "totalamount")
    @JsonProperty("totalamount")
    private BigDecimal totalamount;

    /**
     * 属性 [TOTALTAX_BASE]
     *
     */
    @JSONField(name = "totaltax_base")
    @JsonProperty("totaltax_base")
    private BigDecimal totaltaxBase;

    /**
     * 属性 [TOTALTAX]
     *
     */
    @JSONField(name = "totaltax")
    @JsonProperty("totaltax")
    private BigDecimal totaltax;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;

    /**
     * 属性 [ESTIMATEDVALUE_BASE]
     *
     */
    @JSONField(name = "estimatedvalue_base")
    @JsonProperty("estimatedvalue_base")
    private BigDecimal estimatedvalueBase;

    /**
     * 属性 [OWNERNAME]
     *
     */
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;

    /**
     * 属性 [PRESENTFINALPROPOSAL]
     *
     */
    @JSONField(name = "presentfinalproposal")
    @JsonProperty("presentfinalproposal")
    private Integer presentfinalproposal;

    /**
     * 属性 [DEVELOPPROPOSAL]
     *
     */
    @JSONField(name = "developproposal")
    @JsonProperty("developproposal")
    private Integer developproposal;

    /**
     * 属性 [OPPORTUNITYRATINGCODE]
     *
     */
    @JSONField(name = "opportunityratingcode")
    @JsonProperty("opportunityratingcode")
    private String opportunityratingcode;

    /**
     * 属性 [TOTALAMOUNT_BASE]
     *
     */
    @JSONField(name = "totalamount_base")
    @JsonProperty("totalamount_base")
    private BigDecimal totalamountBase;

    /**
     * 属性 [TOTALAMOUNTLESSFREIGHT]
     *
     */
    @JSONField(name = "totalamountlessfreight")
    @JsonProperty("totalamountlessfreight")
    private BigDecimal totalamountlessfreight;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 属性 [IDENTIFYCOMPETITORS]
     *
     */
    @JSONField(name = "identifycompetitors")
    @JsonProperty("identifycompetitors")
    private Integer identifycompetitors;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [SENDTHANKYOUNOTE]
     *
     */
    @JSONField(name = "sendthankyounote")
    @JsonProperty("sendthankyounote")
    private Integer sendthankyounote;

    /**
     * 属性 [TOTALDISCOUNTAMOUNT_BASE]
     *
     */
    @JSONField(name = "totaldiscountamount_base")
    @JsonProperty("totaldiscountamount_base")
    private BigDecimal totaldiscountamountBase;

    /**
     * 属性 [TOTALLINEITEMDISCOUNTAMOUNT]
     *
     */
    @JSONField(name = "totallineitemdiscountamount")
    @JsonProperty("totallineitemdiscountamount")
    private BigDecimal totallineitemdiscountamount;

    /**
     * 属性 [PURCHASEPROCESS]
     *
     */
    @JSONField(name = "purchaseprocess")
    @JsonProperty("purchaseprocess")
    private String purchaseprocess;

    /**
     * 属性 [TRAVERSEDPATH]
     *
     */
    @JSONField(name = "traversedpath")
    @JsonProperty("traversedpath")
    private String traversedpath;

    /**
     * 属性 [CAPTUREPROPOSALFEEDBACK]
     *
     */
    @JSONField(name = "captureproposalfeedback")
    @JsonProperty("captureproposalfeedback")
    private Integer captureproposalfeedback;

    /**
     * 属性 [FINALDECISIONDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "finaldecisiondate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("finaldecisiondate")
    private Timestamp finaldecisiondate;

    /**
     * 属性 [STEPID]
     *
     */
    @JSONField(name = "stepid")
    @JsonProperty("stepid")
    private String stepid;

    /**
     * 属性 [RESOLVEFEEDBACK]
     *
     */
    @JSONField(name = "resolvefeedback")
    @JsonProperty("resolvefeedback")
    private Integer resolvefeedback;

    /**
     * 属性 [BUDGETAMOUNT_BASE]
     *
     */
    @JSONField(name = "budgetamount_base")
    @JsonProperty("budgetamount_base")
    private BigDecimal budgetamountBase;

    /**
     * 属性 [PURSUITDECISION]
     *
     */
    @JSONField(name = "pursuitdecision")
    @JsonProperty("pursuitdecision")
    private Integer pursuitdecision;

    /**
     * 属性 [REVENUESYSTEMCALCULATED]
     *
     */
    @JSONField(name = "revenuesystemcalculated")
    @JsonProperty("revenuesystemcalculated")
    private Integer revenuesystemcalculated;

    /**
     * 属性 [SCHEDULEPROPOSALMEETING]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "scheduleproposalmeeting" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("scheduleproposalmeeting")
    private Timestamp scheduleproposalmeeting;

    /**
     * 属性 [CUSTOMERTYPE]
     *
     */
    @JSONField(name = "customertype")
    @JsonProperty("customertype")
    private String customertype;

    /**
     * 属性 [CLOSEPROBABILITY]
     *
     */
    @JSONField(name = "closeprobability")
    @JsonProperty("closeprobability")
    private Integer closeprobability;

    /**
     * 属性 [UTCCONVERSIONTIMEZONECODE]
     *
     */
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;

    /**
     * 属性 [PRESENTPROPOSAL]
     *
     */
    @JSONField(name = "presentproposal")
    @JsonProperty("presentproposal")
    private Integer presentproposal;

    /**
     * 属性 [SCHEDULEFOLLOWUP_PROSPECT]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "schedulefollowup_prospect" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("schedulefollowup_prospect")
    private Timestamp schedulefollowupProspect;

    /**
     * 属性 [STATUSCODE]
     *
     */
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;

    /**
     * 属性 [ACTUALCLOSEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "actualclosedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("actualclosedate")
    private Timestamp actualclosedate;

    /**
     * 属性 [CONFIRMINTEREST]
     *
     */
    @JSONField(name = "confirminterest")
    @JsonProperty("confirminterest")
    private Integer confirminterest;

    /**
     * 属性 [CUSTOMERID]
     *
     */
    @JSONField(name = "customerid")
    @JsonProperty("customerid")
    private String customerid;

    /**
     * 属性 [SALESSTAGE]
     *
     */
    @JSONField(name = "salesstage")
    @JsonProperty("salesstage")
    private String salesstage;

    /**
     * 属性 [OPPORTUNITYID]
     *
     */
    @JSONField(name = "opportunityid")
    @JsonProperty("opportunityid")
    private String opportunityid;

    /**
     * 属性 [ONHOLDTIME]
     *
     */
    @JSONField(name = "onholdtime")
    @JsonProperty("onholdtime")
    private Integer onholdtime;

    /**
     * 属性 [QUALIFICATIONCOMMENTS]
     *
     */
    @JSONField(name = "qualificationcomments")
    @JsonProperty("qualificationcomments")
    private String qualificationcomments;

    /**
     * 属性 [SCHEDULEFOLLOWUP_QUALIFY]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "schedulefollowup_qualify" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("schedulefollowup_qualify")
    private Timestamp schedulefollowupQualify;

    /**
     * 属性 [PARTICIPATESINWORKFLOW]
     *
     */
    @JSONField(name = "participatesinworkflow")
    @JsonProperty("participatesinworkflow")
    private Integer participatesinworkflow;

    /**
     * 属性 [EXCHANGERATE]
     *
     */
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;

    /**
     * 属性 [STEPNAME]
     *
     */
    @JSONField(name = "stepname")
    @JsonProperty("stepname")
    private String stepname;

    /**
     * 属性 [CUSTOMERNEED]
     *
     */
    @JSONField(name = "customerneed")
    @JsonProperty("customerneed")
    private String customerneed;

    /**
     * 属性 [IMPORTSEQUENCENUMBER]
     *
     */
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;

    /**
     * 属性 [COMPLETEINTERNALREVIEW]
     *
     */
    @JSONField(name = "completeinternalreview")
    @JsonProperty("completeinternalreview")
    private Integer completeinternalreview;

    /**
     * 属性 [EVALUATEFIT]
     *
     */
    @JSONField(name = "evaluatefit")
    @JsonProperty("evaluatefit")
    private Integer evaluatefit;

    /**
     * 属性 [SALESSTAGECODE]
     *
     */
    @JSONField(name = "salesstagecode")
    @JsonProperty("salesstagecode")
    private String salesstagecode;

    /**
     * 属性 [IDENTIFYPURSUITTEAM]
     *
     */
    @JSONField(name = "identifypursuitteam")
    @JsonProperty("identifypursuitteam")
    private Integer identifypursuitteam;

    /**
     * 属性 [INITIALCOMMUNICATION]
     *
     */
    @JSONField(name = "initialcommunication")
    @JsonProperty("initialcommunication")
    private String initialcommunication;

    /**
     * 属性 [VERSIONNUMBER]
     *
     */
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;

    /**
     * 属性 [FREIGHTAMOUNT_BASE]
     *
     */
    @JSONField(name = "freightamount_base")
    @JsonProperty("freightamount_base")
    private BigDecimal freightamountBase;

    /**
     * 属性 [ACCOUNTNAME]
     *
     */
    @JSONField(name = "accountname")
    @JsonProperty("accountname")
    private String accountname;

    /**
     * 属性 [TEAMSFOLLOWED]
     *
     */
    @JSONField(name = "teamsfollowed")
    @JsonProperty("teamsfollowed")
    private Integer teamsfollowed;

    /**
     * 属性 [PROPOSEDSOLUTION]
     *
     */
    @JSONField(name = "proposedsolution")
    @JsonProperty("proposedsolution")
    private String proposedsolution;

    /**
     * 属性 [FILEDEBRIEF]
     *
     */
    @JSONField(name = "filedebrief")
    @JsonProperty("filedebrief")
    private Integer filedebrief;

    /**
     * 属性 [BUDGETAMOUNT]
     *
     */
    @JSONField(name = "budgetamount")
    @JsonProperty("budgetamount")
    private BigDecimal budgetamount;

    /**
     * 属性 [BUDGETSTATUS]
     *
     */
    @JSONField(name = "budgetstatus")
    @JsonProperty("budgetstatus")
    private String budgetstatus;

    /**
     * 属性 [DISCOUNTAMOUNT_BASE]
     *
     */
    @JSONField(name = "discountamount_base")
    @JsonProperty("discountamount_base")
    private BigDecimal discountamountBase;

    /**
     * 属性 [COMPLETEFINALPROPOSAL]
     *
     */
    @JSONField(name = "completefinalproposal")
    @JsonProperty("completefinalproposal")
    private Integer completefinalproposal;

    /**
     * 属性 [DISCOUNTAMOUNT]
     *
     */
    @JSONField(name = "discountamount")
    @JsonProperty("discountamount")
    private BigDecimal discountamount;

    /**
     * 属性 [NEED]
     *
     */
    @JSONField(name = "need")
    @JsonProperty("need")
    private String need;

    /**
     * 属性 [STAGEID]
     *
     */
    @JSONField(name = "stageid")
    @JsonProperty("stageid")
    private String stageid;

    /**
     * 属性 [FREIGHTAMOUNT]
     *
     */
    @JSONField(name = "freightamount")
    @JsonProperty("freightamount")
    private BigDecimal freightamount;

    /**
     * 属性 [ACTUALVALUE]
     *
     */
    @JSONField(name = "actualvalue")
    @JsonProperty("actualvalue")
    private BigDecimal actualvalue;

    /**
     * 属性 [PRIVATE]
     *
     */
    @JSONField(name = "ibizprivate")
    @JsonProperty("ibizprivate")
    private Integer ibizprivate;

    /**
     * 属性 [TIMEZONERULEVERSIONNUMBER]
     *
     */
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [PRIORITYCODE]
     *
     */
    @JSONField(name = "prioritycode")
    @JsonProperty("prioritycode")
    private String prioritycode;

    /**
     * 属性 [TOTALLINEITEMAMOUNT_BASE]
     *
     */
    @JSONField(name = "totallineitemamount_base")
    @JsonProperty("totallineitemamount_base")
    private BigDecimal totallineitemamountBase;

    /**
     * 属性 [CUSTOMERNAME]
     *
     */
    @JSONField(name = "customername")
    @JsonProperty("customername")
    private String customername;

    /**
     * 属性 [ESTIMATEDCLOSEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "estimatedclosedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("estimatedclosedate")
    private Timestamp estimatedclosedate;

    /**
     * 属性 [CURRENTSITUATION]
     *
     */
    @JSONField(name = "currentsituation")
    @JsonProperty("currentsituation")
    private String currentsituation;

    /**
     * 属性 [ACTUALVALUE_BASE]
     *
     */
    @JSONField(name = "actualvalue_base")
    @JsonProperty("actualvalue_base")
    private BigDecimal actualvalueBase;

    /**
     * 属性 [CUSTOMERPAINPOINTS]
     *
     */
    @JSONField(name = "customerpainpoints")
    @JsonProperty("customerpainpoints")
    private String customerpainpoints;

    /**
     * 属性 [TOTALDISCOUNTAMOUNT]
     *
     */
    @JSONField(name = "totaldiscountamount")
    @JsonProperty("totaldiscountamount")
    private BigDecimal totaldiscountamount;

    /**
     * 属性 [OPPORTUNITYNAME]
     *
     */
    @JSONField(name = "opportunityname")
    @JsonProperty("opportunityname")
    private String opportunityname;

    /**
     * 属性 [IDENTIFYCUSTOMERCONTACTS]
     *
     */
    @JSONField(name = "identifycustomercontacts")
    @JsonProperty("identifycustomercontacts")
    private Integer identifycustomercontacts;

    /**
     * 属性 [TIMELINE]
     *
     */
    @JSONField(name = "timeline")
    @JsonProperty("timeline")
    private String timeline;

    /**
     * 属性 [CONTACTNAME]
     *
     */
    @JSONField(name = "contactname")
    @JsonProperty("contactname")
    private String contactname;

    /**
     * 属性 [LASTONHOLDTIME]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "lastonholdtime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("lastonholdtime")
    private Timestamp lastonholdtime;

    /**
     * 属性 [TOTALAMOUNTLESSFREIGHT_BASE]
     *
     */
    @JSONField(name = "totalamountlessfreight_base")
    @JsonProperty("totalamountlessfreight_base")
    private BigDecimal totalamountlessfreightBase;

    /**
     * 属性 [ESTIMATEDVALUE]
     *
     */
    @JSONField(name = "estimatedvalue")
    @JsonProperty("estimatedvalue")
    private BigDecimal estimatedvalue;

    /**
     * 属性 [EMAILADDRESS]
     *
     */
    @JSONField(name = "emailaddress")
    @JsonProperty("emailaddress")
    private String emailaddress;

    /**
     * 属性 [STATECODE]
     *
     */
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;

    /**
     * 属性 [PURCHASETIMEFRAME]
     *
     */
    @JSONField(name = "purchasetimeframe")
    @JsonProperty("purchasetimeframe")
    private String purchasetimeframe;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;

    /**
     * 属性 [QUOTECOMMENTS]
     *
     */
    @JSONField(name = "quotecomments")
    @JsonProperty("quotecomments")
    private String quotecomments;

    /**
     * 属性 [OVERRIDDENCREATEDON]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;

    /**
     * 属性 [OWNERID]
     *
     */
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;

    /**
     * 属性 [PARENTACCOUNTNAME]
     *
     */
    @JSONField(name = "parentaccountname")
    @JsonProperty("parentaccountname")
    private String parentaccountname;

    /**
     * 属性 [ORIGINATINGLEADNAME]
     *
     */
    @JSONField(name = "originatingleadname")
    @JsonProperty("originatingleadname")
    private String originatingleadname;

    /**
     * 属性 [PARENTCONTACTNAME]
     *
     */
    @JSONField(name = "parentcontactname")
    @JsonProperty("parentcontactname")
    private String parentcontactname;

    /**
     * 属性 [CURRENCYNAME]
     *
     */
    @JSONField(name = "currencyname")
    @JsonProperty("currencyname")
    private String currencyname;

    /**
     * 属性 [PRICELEVELNAME]
     *
     */
    @JSONField(name = "pricelevelname")
    @JsonProperty("pricelevelname")
    private String pricelevelname;

    /**
     * 属性 [SLANAME]
     *
     */
    @JSONField(name = "slaname")
    @JsonProperty("slaname")
    private String slaname;

    /**
     * 属性 [CAMPAIGNNAME]
     *
     */
    @JSONField(name = "campaignname")
    @JsonProperty("campaignname")
    private String campaignname;

    /**
     * 属性 [PARENTCONTACTID]
     *
     */
    @JSONField(name = "parentcontactid")
    @JsonProperty("parentcontactid")
    private String parentcontactid;

    /**
     * 属性 [CAMPAIGNID]
     *
     */
    @JSONField(name = "campaignid")
    @JsonProperty("campaignid")
    private String campaignid;

    /**
     * 属性 [TRANSACTIONCURRENCYID]
     *
     */
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;

    /**
     * 属性 [PARENTACCOUNTID]
     *
     */
    @JSONField(name = "parentaccountid")
    @JsonProperty("parentaccountid")
    private String parentaccountid;

    /**
     * 属性 [PRICELEVELID]
     *
     */
    @JSONField(name = "pricelevelid")
    @JsonProperty("pricelevelid")
    private String pricelevelid;

    /**
     * 属性 [ORIGINATINGLEADID]
     *
     */
    @JSONField(name = "originatingleadid")
    @JsonProperty("originatingleadid")
    private String originatingleadid;

    /**
     * 属性 [SLAID]
     *
     */
    @JSONField(name = "slaid")
    @JsonProperty("slaid")
    private String slaid;


    /**
     * 设置 [DECISIONMAKER]
     */
    public void setDecisionmaker(Integer  decisionmaker){
        this.decisionmaker = decisionmaker ;
        this.modify("decisionmaker",decisionmaker);
    }

    /**
     * 设置 [TOTALLINEITEMAMOUNT]
     */
    public void setTotallineitemamount(BigDecimal  totallineitemamount){
        this.totallineitemamount = totallineitemamount ;
        this.modify("totallineitemamount",totallineitemamount);
    }

    /**
     * 设置 [PROCESSID]
     */
    public void setProcessid(String  processid){
        this.processid = processid ;
        this.modify("processid",processid);
    }

    /**
     * 设置 [DISCOUNTPERCENTAGE]
     */
    public void setDiscountpercentage(BigDecimal  discountpercentage){
        this.discountpercentage = discountpercentage ;
        this.modify("discountpercentage",discountpercentage);
    }

    /**
     * 设置 [OWNERTYPE]
     */
    public void setOwnertype(String  ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [PRICINGERRORCODE]
     */
    public void setPricingerrorcode(String  pricingerrorcode){
        this.pricingerrorcode = pricingerrorcode ;
        this.modify("pricingerrorcode",pricingerrorcode);
    }

    /**
     * 设置 [TOTALAMOUNT]
     */
    public void setTotalamount(BigDecimal  totalamount){
        this.totalamount = totalamount ;
        this.modify("totalamount",totalamount);
    }

    /**
     * 设置 [TOTALTAX_BASE]
     */
    public void setTotaltaxBase(BigDecimal  totaltaxBase){
        this.totaltaxBase = totaltaxBase ;
        this.modify("totaltax_base",totaltaxBase);
    }

    /**
     * 设置 [TOTALTAX]
     */
    public void setTotaltax(BigDecimal  totaltax){
        this.totaltax = totaltax ;
        this.modify("totaltax",totaltax);
    }

    /**
     * 设置 [ESTIMATEDVALUE_BASE]
     */
    public void setEstimatedvalueBase(BigDecimal  estimatedvalueBase){
        this.estimatedvalueBase = estimatedvalueBase ;
        this.modify("estimatedvalue_base",estimatedvalueBase);
    }

    /**
     * 设置 [OWNERNAME]
     */
    public void setOwnername(String  ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [PRESENTFINALPROPOSAL]
     */
    public void setPresentfinalproposal(Integer  presentfinalproposal){
        this.presentfinalproposal = presentfinalproposal ;
        this.modify("presentfinalproposal",presentfinalproposal);
    }

    /**
     * 设置 [DEVELOPPROPOSAL]
     */
    public void setDevelopproposal(Integer  developproposal){
        this.developproposal = developproposal ;
        this.modify("developproposal",developproposal);
    }

    /**
     * 设置 [OPPORTUNITYRATINGCODE]
     */
    public void setOpportunityratingcode(String  opportunityratingcode){
        this.opportunityratingcode = opportunityratingcode ;
        this.modify("opportunityratingcode",opportunityratingcode);
    }

    /**
     * 设置 [TOTALAMOUNT_BASE]
     */
    public void setTotalamountBase(BigDecimal  totalamountBase){
        this.totalamountBase = totalamountBase ;
        this.modify("totalamount_base",totalamountBase);
    }

    /**
     * 设置 [TOTALAMOUNTLESSFREIGHT]
     */
    public void setTotalamountlessfreight(BigDecimal  totalamountlessfreight){
        this.totalamountlessfreight = totalamountlessfreight ;
        this.modify("totalamountlessfreight",totalamountlessfreight);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [IDENTIFYCOMPETITORS]
     */
    public void setIdentifycompetitors(Integer  identifycompetitors){
        this.identifycompetitors = identifycompetitors ;
        this.modify("identifycompetitors",identifycompetitors);
    }

    /**
     * 设置 [SENDTHANKYOUNOTE]
     */
    public void setSendthankyounote(Integer  sendthankyounote){
        this.sendthankyounote = sendthankyounote ;
        this.modify("sendthankyounote",sendthankyounote);
    }

    /**
     * 设置 [TOTALDISCOUNTAMOUNT_BASE]
     */
    public void setTotaldiscountamountBase(BigDecimal  totaldiscountamountBase){
        this.totaldiscountamountBase = totaldiscountamountBase ;
        this.modify("totaldiscountamount_base",totaldiscountamountBase);
    }

    /**
     * 设置 [TOTALLINEITEMDISCOUNTAMOUNT]
     */
    public void setTotallineitemdiscountamount(BigDecimal  totallineitemdiscountamount){
        this.totallineitemdiscountamount = totallineitemdiscountamount ;
        this.modify("totallineitemdiscountamount",totallineitemdiscountamount);
    }

    /**
     * 设置 [PURCHASEPROCESS]
     */
    public void setPurchaseprocess(String  purchaseprocess){
        this.purchaseprocess = purchaseprocess ;
        this.modify("purchaseprocess",purchaseprocess);
    }

    /**
     * 设置 [TRAVERSEDPATH]
     */
    public void setTraversedpath(String  traversedpath){
        this.traversedpath = traversedpath ;
        this.modify("traversedpath",traversedpath);
    }

    /**
     * 设置 [CAPTUREPROPOSALFEEDBACK]
     */
    public void setCaptureproposalfeedback(Integer  captureproposalfeedback){
        this.captureproposalfeedback = captureproposalfeedback ;
        this.modify("captureproposalfeedback",captureproposalfeedback);
    }

    /**
     * 设置 [FINALDECISIONDATE]
     */
    public void setFinaldecisiondate(Timestamp  finaldecisiondate){
        this.finaldecisiondate = finaldecisiondate ;
        this.modify("finaldecisiondate",finaldecisiondate);
    }

    /**
     * 设置 [STEPID]
     */
    public void setStepid(String  stepid){
        this.stepid = stepid ;
        this.modify("stepid",stepid);
    }

    /**
     * 设置 [RESOLVEFEEDBACK]
     */
    public void setResolvefeedback(Integer  resolvefeedback){
        this.resolvefeedback = resolvefeedback ;
        this.modify("resolvefeedback",resolvefeedback);
    }

    /**
     * 设置 [BUDGETAMOUNT_BASE]
     */
    public void setBudgetamountBase(BigDecimal  budgetamountBase){
        this.budgetamountBase = budgetamountBase ;
        this.modify("budgetamount_base",budgetamountBase);
    }

    /**
     * 设置 [PURSUITDECISION]
     */
    public void setPursuitdecision(Integer  pursuitdecision){
        this.pursuitdecision = pursuitdecision ;
        this.modify("pursuitdecision",pursuitdecision);
    }

    /**
     * 设置 [REVENUESYSTEMCALCULATED]
     */
    public void setRevenuesystemcalculated(Integer  revenuesystemcalculated){
        this.revenuesystemcalculated = revenuesystemcalculated ;
        this.modify("revenuesystemcalculated",revenuesystemcalculated);
    }

    /**
     * 设置 [SCHEDULEPROPOSALMEETING]
     */
    public void setScheduleproposalmeeting(Timestamp  scheduleproposalmeeting){
        this.scheduleproposalmeeting = scheduleproposalmeeting ;
        this.modify("scheduleproposalmeeting",scheduleproposalmeeting);
    }

    /**
     * 设置 [CUSTOMERTYPE]
     */
    public void setCustomertype(String  customertype){
        this.customertype = customertype ;
        this.modify("customertype",customertype);
    }

    /**
     * 设置 [CLOSEPROBABILITY]
     */
    public void setCloseprobability(Integer  closeprobability){
        this.closeprobability = closeprobability ;
        this.modify("closeprobability",closeprobability);
    }

    /**
     * 设置 [UTCCONVERSIONTIMEZONECODE]
     */
    public void setUtcconversiontimezonecode(Integer  utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [PRESENTPROPOSAL]
     */
    public void setPresentproposal(Integer  presentproposal){
        this.presentproposal = presentproposal ;
        this.modify("presentproposal",presentproposal);
    }

    /**
     * 设置 [SCHEDULEFOLLOWUP_PROSPECT]
     */
    public void setSchedulefollowupProspect(Timestamp  schedulefollowupProspect){
        this.schedulefollowupProspect = schedulefollowupProspect ;
        this.modify("schedulefollowup_prospect",schedulefollowupProspect);
    }

    /**
     * 设置 [STATUSCODE]
     */
    public void setStatuscode(Integer  statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [ACTUALCLOSEDATE]
     */
    public void setActualclosedate(Timestamp  actualclosedate){
        this.actualclosedate = actualclosedate ;
        this.modify("actualclosedate",actualclosedate);
    }

    /**
     * 设置 [CONFIRMINTEREST]
     */
    public void setConfirminterest(Integer  confirminterest){
        this.confirminterest = confirminterest ;
        this.modify("confirminterest",confirminterest);
    }

    /**
     * 设置 [CUSTOMERID]
     */
    public void setCustomerid(String  customerid){
        this.customerid = customerid ;
        this.modify("customerid",customerid);
    }

    /**
     * 设置 [SALESSTAGE]
     */
    public void setSalesstage(String  salesstage){
        this.salesstage = salesstage ;
        this.modify("salesstage",salesstage);
    }

    /**
     * 设置 [ONHOLDTIME]
     */
    public void setOnholdtime(Integer  onholdtime){
        this.onholdtime = onholdtime ;
        this.modify("onholdtime",onholdtime);
    }

    /**
     * 设置 [QUALIFICATIONCOMMENTS]
     */
    public void setQualificationcomments(String  qualificationcomments){
        this.qualificationcomments = qualificationcomments ;
        this.modify("qualificationcomments",qualificationcomments);
    }

    /**
     * 设置 [SCHEDULEFOLLOWUP_QUALIFY]
     */
    public void setSchedulefollowupQualify(Timestamp  schedulefollowupQualify){
        this.schedulefollowupQualify = schedulefollowupQualify ;
        this.modify("schedulefollowup_qualify",schedulefollowupQualify);
    }

    /**
     * 设置 [PARTICIPATESINWORKFLOW]
     */
    public void setParticipatesinworkflow(Integer  participatesinworkflow){
        this.participatesinworkflow = participatesinworkflow ;
        this.modify("participatesinworkflow",participatesinworkflow);
    }

    /**
     * 设置 [EXCHANGERATE]
     */
    public void setExchangerate(BigDecimal  exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [STEPNAME]
     */
    public void setStepname(String  stepname){
        this.stepname = stepname ;
        this.modify("stepname",stepname);
    }

    /**
     * 设置 [CUSTOMERNEED]
     */
    public void setCustomerneed(String  customerneed){
        this.customerneed = customerneed ;
        this.modify("customerneed",customerneed);
    }

    /**
     * 设置 [IMPORTSEQUENCENUMBER]
     */
    public void setImportsequencenumber(Integer  importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [COMPLETEINTERNALREVIEW]
     */
    public void setCompleteinternalreview(Integer  completeinternalreview){
        this.completeinternalreview = completeinternalreview ;
        this.modify("completeinternalreview",completeinternalreview);
    }

    /**
     * 设置 [EVALUATEFIT]
     */
    public void setEvaluatefit(Integer  evaluatefit){
        this.evaluatefit = evaluatefit ;
        this.modify("evaluatefit",evaluatefit);
    }

    /**
     * 设置 [SALESSTAGECODE]
     */
    public void setSalesstagecode(String  salesstagecode){
        this.salesstagecode = salesstagecode ;
        this.modify("salesstagecode",salesstagecode);
    }

    /**
     * 设置 [IDENTIFYPURSUITTEAM]
     */
    public void setIdentifypursuitteam(Integer  identifypursuitteam){
        this.identifypursuitteam = identifypursuitteam ;
        this.modify("identifypursuitteam",identifypursuitteam);
    }

    /**
     * 设置 [INITIALCOMMUNICATION]
     */
    public void setInitialcommunication(String  initialcommunication){
        this.initialcommunication = initialcommunication ;
        this.modify("initialcommunication",initialcommunication);
    }

    /**
     * 设置 [VERSIONNUMBER]
     */
    public void setVersionnumber(BigInteger  versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [FREIGHTAMOUNT_BASE]
     */
    public void setFreightamountBase(BigDecimal  freightamountBase){
        this.freightamountBase = freightamountBase ;
        this.modify("freightamount_base",freightamountBase);
    }

    /**
     * 设置 [ACCOUNTNAME]
     */
    public void setAccountname(String  accountname){
        this.accountname = accountname ;
        this.modify("accountname",accountname);
    }

    /**
     * 设置 [TEAMSFOLLOWED]
     */
    public void setTeamsfollowed(Integer  teamsfollowed){
        this.teamsfollowed = teamsfollowed ;
        this.modify("teamsfollowed",teamsfollowed);
    }

    /**
     * 设置 [PROPOSEDSOLUTION]
     */
    public void setProposedsolution(String  proposedsolution){
        this.proposedsolution = proposedsolution ;
        this.modify("proposedsolution",proposedsolution);
    }

    /**
     * 设置 [FILEDEBRIEF]
     */
    public void setFiledebrief(Integer  filedebrief){
        this.filedebrief = filedebrief ;
        this.modify("filedebrief",filedebrief);
    }

    /**
     * 设置 [BUDGETAMOUNT]
     */
    public void setBudgetamount(BigDecimal  budgetamount){
        this.budgetamount = budgetamount ;
        this.modify("budgetamount",budgetamount);
    }

    /**
     * 设置 [BUDGETSTATUS]
     */
    public void setBudgetstatus(String  budgetstatus){
        this.budgetstatus = budgetstatus ;
        this.modify("budgetstatus",budgetstatus);
    }

    /**
     * 设置 [DISCOUNTAMOUNT_BASE]
     */
    public void setDiscountamountBase(BigDecimal  discountamountBase){
        this.discountamountBase = discountamountBase ;
        this.modify("discountamount_base",discountamountBase);
    }

    /**
     * 设置 [COMPLETEFINALPROPOSAL]
     */
    public void setCompletefinalproposal(Integer  completefinalproposal){
        this.completefinalproposal = completefinalproposal ;
        this.modify("completefinalproposal",completefinalproposal);
    }

    /**
     * 设置 [DISCOUNTAMOUNT]
     */
    public void setDiscountamount(BigDecimal  discountamount){
        this.discountamount = discountamount ;
        this.modify("discountamount",discountamount);
    }

    /**
     * 设置 [NEED]
     */
    public void setNeed(String  need){
        this.need = need ;
        this.modify("need",need);
    }

    /**
     * 设置 [STAGEID]
     */
    public void setStageid(String  stageid){
        this.stageid = stageid ;
        this.modify("stageid",stageid);
    }

    /**
     * 设置 [FREIGHTAMOUNT]
     */
    public void setFreightamount(BigDecimal  freightamount){
        this.freightamount = freightamount ;
        this.modify("freightamount",freightamount);
    }

    /**
     * 设置 [ACTUALVALUE]
     */
    public void setActualvalue(BigDecimal  actualvalue){
        this.actualvalue = actualvalue ;
        this.modify("actualvalue",actualvalue);
    }

    /**
     * 设置 [PRIVATE]
     */
    public void setIbizprivate(Integer  ibizprivate){
        this.ibizprivate = ibizprivate ;
        this.modify("private",ibizprivate);
    }

    /**
     * 设置 [TIMEZONERULEVERSIONNUMBER]
     */
    public void setTimezoneruleversionnumber(Integer  timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [PRIORITYCODE]
     */
    public void setPrioritycode(String  prioritycode){
        this.prioritycode = prioritycode ;
        this.modify("prioritycode",prioritycode);
    }

    /**
     * 设置 [TOTALLINEITEMAMOUNT_BASE]
     */
    public void setTotallineitemamountBase(BigDecimal  totallineitemamountBase){
        this.totallineitemamountBase = totallineitemamountBase ;
        this.modify("totallineitemamount_base",totallineitemamountBase);
    }

    /**
     * 设置 [CUSTOMERNAME]
     */
    public void setCustomername(String  customername){
        this.customername = customername ;
        this.modify("customername",customername);
    }

    /**
     * 设置 [ESTIMATEDCLOSEDATE]
     */
    public void setEstimatedclosedate(Timestamp  estimatedclosedate){
        this.estimatedclosedate = estimatedclosedate ;
        this.modify("estimatedclosedate",estimatedclosedate);
    }

    /**
     * 设置 [CURRENTSITUATION]
     */
    public void setCurrentsituation(String  currentsituation){
        this.currentsituation = currentsituation ;
        this.modify("currentsituation",currentsituation);
    }

    /**
     * 设置 [ACTUALVALUE_BASE]
     */
    public void setActualvalueBase(BigDecimal  actualvalueBase){
        this.actualvalueBase = actualvalueBase ;
        this.modify("actualvalue_base",actualvalueBase);
    }

    /**
     * 设置 [CUSTOMERPAINPOINTS]
     */
    public void setCustomerpainpoints(String  customerpainpoints){
        this.customerpainpoints = customerpainpoints ;
        this.modify("customerpainpoints",customerpainpoints);
    }

    /**
     * 设置 [TOTALDISCOUNTAMOUNT]
     */
    public void setTotaldiscountamount(BigDecimal  totaldiscountamount){
        this.totaldiscountamount = totaldiscountamount ;
        this.modify("totaldiscountamount",totaldiscountamount);
    }

    /**
     * 设置 [OPPORTUNITYNAME]
     */
    public void setOpportunityname(String  opportunityname){
        this.opportunityname = opportunityname ;
        this.modify("opportunityname",opportunityname);
    }

    /**
     * 设置 [IDENTIFYCUSTOMERCONTACTS]
     */
    public void setIdentifycustomercontacts(Integer  identifycustomercontacts){
        this.identifycustomercontacts = identifycustomercontacts ;
        this.modify("identifycustomercontacts",identifycustomercontacts);
    }

    /**
     * 设置 [TIMELINE]
     */
    public void setTimeline(String  timeline){
        this.timeline = timeline ;
        this.modify("timeline",timeline);
    }

    /**
     * 设置 [CONTACTNAME]
     */
    public void setContactname(String  contactname){
        this.contactname = contactname ;
        this.modify("contactname",contactname);
    }

    /**
     * 设置 [LASTONHOLDTIME]
     */
    public void setLastonholdtime(Timestamp  lastonholdtime){
        this.lastonholdtime = lastonholdtime ;
        this.modify("lastonholdtime",lastonholdtime);
    }

    /**
     * 设置 [TOTALAMOUNTLESSFREIGHT_BASE]
     */
    public void setTotalamountlessfreightBase(BigDecimal  totalamountlessfreightBase){
        this.totalamountlessfreightBase = totalamountlessfreightBase ;
        this.modify("totalamountlessfreight_base",totalamountlessfreightBase);
    }

    /**
     * 设置 [ESTIMATEDVALUE]
     */
    public void setEstimatedvalue(BigDecimal  estimatedvalue){
        this.estimatedvalue = estimatedvalue ;
        this.modify("estimatedvalue",estimatedvalue);
    }

    /**
     * 设置 [EMAILADDRESS]
     */
    public void setEmailaddress(String  emailaddress){
        this.emailaddress = emailaddress ;
        this.modify("emailaddress",emailaddress);
    }

    /**
     * 设置 [STATECODE]
     */
    public void setStatecode(Integer  statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [PURCHASETIMEFRAME]
     */
    public void setPurchasetimeframe(String  purchasetimeframe){
        this.purchasetimeframe = purchasetimeframe ;
        this.modify("purchasetimeframe",purchasetimeframe);
    }

    /**
     * 设置 [QUOTECOMMENTS]
     */
    public void setQuotecomments(String  quotecomments){
        this.quotecomments = quotecomments ;
        this.modify("quotecomments",quotecomments);
    }

    /**
     * 设置 [OVERRIDDENCREATEDON]
     */
    public void setOverriddencreatedon(Timestamp  overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 设置 [OWNERID]
     */
    public void setOwnerid(String  ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [PARENTACCOUNTNAME]
     */
    public void setParentaccountname(String  parentaccountname){
        this.parentaccountname = parentaccountname ;
        this.modify("parentaccountname",parentaccountname);
    }

    /**
     * 设置 [ORIGINATINGLEADNAME]
     */
    public void setOriginatingleadname(String  originatingleadname){
        this.originatingleadname = originatingleadname ;
        this.modify("originatingleadname",originatingleadname);
    }

    /**
     * 设置 [PARENTCONTACTNAME]
     */
    public void setParentcontactname(String  parentcontactname){
        this.parentcontactname = parentcontactname ;
        this.modify("parentcontactname",parentcontactname);
    }

    /**
     * 设置 [CURRENCYNAME]
     */
    public void setCurrencyname(String  currencyname){
        this.currencyname = currencyname ;
        this.modify("currencyname",currencyname);
    }

    /**
     * 设置 [PRICELEVELNAME]
     */
    public void setPricelevelname(String  pricelevelname){
        this.pricelevelname = pricelevelname ;
        this.modify("pricelevelname",pricelevelname);
    }

    /**
     * 设置 [SLANAME]
     */
    public void setSlaname(String  slaname){
        this.slaname = slaname ;
        this.modify("slaname",slaname);
    }

    /**
     * 设置 [CAMPAIGNNAME]
     */
    public void setCampaignname(String  campaignname){
        this.campaignname = campaignname ;
        this.modify("campaignname",campaignname);
    }

    /**
     * 设置 [PARENTCONTACTID]
     */
    public void setParentcontactid(String  parentcontactid){
        this.parentcontactid = parentcontactid ;
        this.modify("parentcontactid",parentcontactid);
    }

    /**
     * 设置 [CAMPAIGNID]
     */
    public void setCampaignid(String  campaignid){
        this.campaignid = campaignid ;
        this.modify("campaignid",campaignid);
    }

    /**
     * 设置 [TRANSACTIONCURRENCYID]
     */
    public void setTransactioncurrencyid(String  transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }

    /**
     * 设置 [PARENTACCOUNTID]
     */
    public void setParentaccountid(String  parentaccountid){
        this.parentaccountid = parentaccountid ;
        this.modify("parentaccountid",parentaccountid);
    }

    /**
     * 设置 [PRICELEVELID]
     */
    public void setPricelevelid(String  pricelevelid){
        this.pricelevelid = pricelevelid ;
        this.modify("pricelevelid",pricelevelid);
    }

    /**
     * 设置 [ORIGINATINGLEADID]
     */
    public void setOriginatingleadid(String  originatingleadid){
        this.originatingleadid = originatingleadid ;
        this.modify("originatingleadid",originatingleadid);
    }

    /**
     * 设置 [SLAID]
     */
    public void setSlaid(String  slaid){
        this.slaid = slaid ;
        this.modify("slaid",slaid);
    }


}

